--liquibase formatted sql

--changeset postgres:hm_002_add_hm_harvest_use_case_behavior_config context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM platform.config WHERE abbrev = 'HM_HARVEST_USE_CASE_BEHAVIOR') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-29 BDS-4849 - CB-HM: Insert new config for Harvest Use Case Requirements Configuration



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_HARVEST_USE_CASE_BEHAVIOR',
        'Harvest Manager - Harvest Use Case Requirements Configuration',
        $$			
            [
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "harvestMethodAbbrev": [
                        "BULK",
                        "SINGLE_SEED_DESCENT"
                    ],
                    "configDescription": "This harvest use case applies to the RICE crop, for selfing crosses OR plots, where the germplasm state is not_fixed. For each selfing cross OR plot, it creates ONE new germplasm, ONE new seed, and ONE new package, as well as other related records such as germplasm names, attributes, and relations records.",
                    "config": {
                        "harvestType": "single",
                        "entities": [
                            {
                                "entity": "germplasm",
                                "identifier": "germplasmDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "germplasm",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "existingGermplasmDbId",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm-names-search",
                                            "apiParams": {
                                                "nameValue": {
                                                    "type": "recordInfo",
                                                    "subType": "buildData",
                                                    "fieldName": "designation"
                                                }
                                            },
                                            "apiResultFieldName": "germplasmDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasm": [
                                            {
                                                "condition": "existingGermplasmDbId is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "existingGermplasmDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "existingGermplasmDbId"
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "generation": {
                                        "type": "generationAdvancement",
                                        "base": "F"
                                    },
                                    "parentage" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmParentage"
                                    },
                                    "state" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmState"
                                    },
                                    "germplasmType" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmType"
                                    },
                                    "nameType" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmNameType"
                                    },
                                    "taxonomyDbId" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmTaxonomyId"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_name",
                                "identifier": "germplasmNameDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "germplasm-names",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmName": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "nameValue": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "designation"
                                    },
                                    "germplasmNameType": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "germplasmNameType"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_name",
                                "identifier": "germplasmNameDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "germplasm-names",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "plantedGermplasmBCDerivName",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm-names-search",
                                            "apiParams": {
                                                "germplasmDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                },
                                                "germplasmNameType": {
                                                    "type": "recordInfo",
                                                    "subType": "buildData",
                                                    "fieldName": "germplasmNameType"
                                                }
                                            },
                                            "apiResultFieldName": "nameValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmName": [
                                            {
                                                "condition": "germplasmCreated is true and plantedGermplasmBCDerivName is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "nameValue": {
                                                            "type": "nameBuilder",
                                                            "specialType": "BACKCROSS_DERIVATIVE_NAME"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "germplasmNameType": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_NAME_TYPE",
                                        "scaleValueAbbrev": "BACKCROSS_DERIVATIVE"
                                    },
                                    "germplasmNameStatus": {
                                        "type": "plainText",
                                        "textValue": "active"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_relation",
                                "identifier": "germplasmRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 3,
                                "action": "create",
                                "apiEndpoint": "germplasm-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmRelation": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentGermplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "germplasmYear",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "GERMPLASM_YEAR"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and germplasmYear is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "germplasmYear"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "GERMPLASM_YEAR"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "mtaStatus",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "MTA_STATUS"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and mtaStatus is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "mtaStatus"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "MTA_STATUS"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "origin",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "ORIGIN"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and origin is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "origin"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "ORIGIN"
                                    }
                                }
                            },
                            {
                                "entity": "family_member",
                                "identifier": "familyMemberDbId",
                                "retrieveRecord": false,
                                "orderNumber": 5,
                                "action": "create",
                                "apiEndpoint": "family-members",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "familyDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "familyDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createFamilyMember": [
                                            {
                                                "condition": "germplasmCreated is true and familyDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "familyDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "familyDbId"
                                    }
                                }
                            },
                            {
                                "entity": "cross",
                                "identifier": "crossDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "crosses",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "existingCrossDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "crossDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCross": [
                                            {
                                                "condition": "existingCrossDbId is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "existingCrossDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "existingCrossDbId"
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "crossMethod": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "CROSS_METHOD",
                                        "scaleValueAbbrev": "SELFING"
                                    }
                                }
                            },
                            {
                                "entity": "cross_parent",
                                "identifier": "crossParentDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "cross-parents",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "crossCreated",
                                            "type": "previousDecision",
                                            "entity": "cross",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCrossParent": [
                                            {
                                                "condition": "crossCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentRole": {
                                        "type": "plainText",
                                        "textValue": "female-and-male"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1" 
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed",
                                "identifier": "seedDbId",
                                "retrieveRecord": true,
                                "orderNumber": 6,
                                "action": "create",
                                "apiEndpoint": "seeds",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "existingGermplasmUsed",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "useExisting"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeed": [
                                            {
                                                "condition": "germplasmCreated is true or existingGermplasmUsed is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "sourceEntryDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "entryDbId"
                                    },
                                    "sourcePlotDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "plotDbId"
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 7,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "package",
                                "identifier": "packageDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "packages",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createPackage": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {}
                            }
                        ]
                    }
                },
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "harvestMethodAbbrev": [
                        "SINGLE_PLANT_SELECTION",
                        "PANICLE_SELECTION",
                        "PLANT_SPECIFIC"
                    ],
                    "configDescription": "This harvest use case applies to the RICE crop, for selfing crosses OR plots, where the germplasm state is not_fixed. This creates multiple sets of germplasm, seed, and package records, as well as other related records such as germplasm names, attributes, and relations records. The number of germplasm records created is based on a given numeric variable, which is defined in the `numericVariable` property of the config. For example, if the numeric variable is `noOfPlant`, and the value is 5, then 5 germplasm records will be created, with each germplasm having its own seed and package records.",
                    "config": {
                        "harvestType": "multiple",
                        "numericVariable": {
                            "SINGLE_PLANT_SELECTION": {
                                "variableName": "noOfPlant",
                                "variableType": "integer"
                            },
                            "PANICLE_SELECTION": {
                                "variableName": "noOfPanicle",
                                "variableType": "integer"
                            },
                            "PLANT_SPECIFIC": {
                                "variableName": "specificPlantNo",
                                "variableType": "commaSeparatedNumber"
                            }
                        },
                        "entities": [
                            {
                                "entity": "germplasm",
                                "identifier": "germplasmDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "germplasm",
                                "data": {
                                    "generation": {
                                        "type": "generationAdvancement",
                                        "base": "F"
                                    },
                                    "parentage" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmParentage"
                                    },
                                    "state" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmState"
                                    },
                                    "germplasmType" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmType"
                                    },
                                    "nameType" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmNameType"
                                    },
                                    "taxonomyDbId" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmTaxonomyId"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_name",
                                "identifier": "germplasmNameDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "germplasm-names",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmName": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "nameValue": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "designation"
                                    },
                                    "germplasmNameType": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "germplasmNameType"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_name",
                                "identifier": "germplasmNameDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "germplasm-names",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "plantedGermplasmBCDerivName",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm-names-search",
                                            "apiParams": {
                                                "germplasmDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                },
                                                "germplasmNameType": {
                                                    "type": "recordInfo",
                                                    "subType": "buildData",
                                                    "fieldName": "germplasmNameType"
                                                }
                                            },
                                            "apiResultFieldName": "nameValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmName": [
                                            {
                                                "condition": "germplasmCreated is true and plantedGermplasmBCDerivName is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "nameValue": {
                                                            "type": "nameBuilder",
                                                            "specialType": "BACKCROSS_DERIVATIVE_NAME"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "germplasmNameType": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_NAME_TYPE",
                                        "scaleValueAbbrev": "BACKCROSS_DERIVATIVE"
                                    },
                                    "germplasmNameStatus": {
                                        "type": "plainText",
                                        "textValue": "active"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_relation",
                                "identifier": "germplasmRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 3,
                                "action": "create",
                                "apiEndpoint": "germplasm-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmRelation": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentGermplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "germplasmYear",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "GERMPLASM_YEAR"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and germplasmYear is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "germplasmYear"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "GERMPLASM_YEAR"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "mtaStatus",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "MTA_STATUS"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and mtaStatus is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "mtaStatus"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "MTA_STATUS"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "origin",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "ORIGIN"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and origin is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "origin"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "ORIGIN"
                                    }
                                }
                            },
                            {
                                "entity": "family_member",
                                "identifier": "familyMemberDbId",
                                "retrieveRecord": false,
                                "orderNumber": 5,
                                "action": "create",
                                "apiEndpoint": "family-members",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "familyDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "familyDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createFamilyMember": [
                                            {
                                                "condition": "germplasmCreated is true and familyDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "familyDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "familyDbId"
                                    }
                                }
                            },
                            {
                                "entity": "cross",
                                "identifier": "crossDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "crosses",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "existingCrossDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "crossDbId"
                                        },
                                        {
                                            "referenceName": "newCrossDbId",
                                            "type": "recordInfo",
                                            "subType": "new",
                                            "fieldName": "crossDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCross": [
                                            {
                                                "condition": "existingCrossDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "existingCrossDbId"
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "newCrossDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "newCrossDbId"
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "existingCrossDbId is null and newCrossDbId is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "crossMethod": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "CROSS_METHOD",
                                        "scaleValueAbbrev": "SELFING"
                                    }
                                }
                            },
                            {
                                "entity": "cross_parent",
                                "identifier": "crossParentDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "cross-parents",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "crossCreated",
                                            "type": "previousDecision",
                                            "entity": "cross",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCrossParent": [
                                            {
                                                "condition": "crossCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentRole": {
                                        "type": "plainText",
                                        "textValue": "female-and-male"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1" 
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed",
                                "identifier": "seedDbId",
                                "retrieveRecord": true,
                                "orderNumber": 6,
                                "action": "create",
                                "apiEndpoint": "seeds",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "existingGermplasmUsed",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "useExisting"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeed": [
                                            {
                                                "condition": "germplasmCreated is true or existingGermplasmUsed is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "sourceEntryDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "entryDbId"
                                    },
                                    "sourcePlotDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "plotDbId"
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 7,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "package",
                                "identifier": "packageDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "packages",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createPackage": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }

                                        ]
                                    }
                                },
                                "data": {}
                            }
                        ]
                    }
                },
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "harvestMethodAbbrev": "SINGLE_PLANT_SELECTION_AND_BULK",
                    "configDescription": "This harvest use case applies to the RICE crop, for selfing crosses OR plots, where the germplasm state is not_fixed. This is a combination of two harvest methods, SINGLE PLANT SELECTION and BULK. This creates multiple sets of germplasm, seed, and package records, as well as other related records such as germplasm names, attributes, and relations records. The number of germplasm records created is based on the given `noOfPlant` value. For example, if the `noOfPlant` value is 5, then 5 germplasm records will be created, with each germplasm having its own seed and package records. It also creates another set of germplasm, seed, and package records for the bulk harvest.",
                    "config": {
                        "harvestType": "combination",
                        "harvestMethodAbbrevs": [
                            "SINGLE_PLANT_SELECTION",
                            "BULK"
                        ]
                    }
                },
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "harvestMethodAbbrev": "PLANT_SPECIFIC_AND_BULK",
                    "configDescription": "This harvest use case applies to the RICE crop, for selfing crosses OR plots, where the germplasm state is not_fixed. This is a combination of two harvest methods, PLANT SPECIFIC and BULK. This creates multiple sets of germplasm, seed, and package records, as well as other related records such as germplasm names, attributes, and relations records. The number of germplasm records created is based on the given `specificPlantNo` value. For example, if the `specificPlantNo` value is 5, then 5 germplasm records will be created, with each germplasm having its own seed and package records. It also creates another set of germplasm, seed, and package records for the bulk harvest.",
                    "config": {
                        "harvestType": "combination",
                        "harvestMethodAbbrevs": [
                            "PLANT_SPECIFIC",
                            "BULK"
                        ]
                    }
                },
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": "SELFING",
                    "state": "fixed",
                    "harvestMethodAbbrev": "BULK",
                    "configDescription": "This harvest use case applies to the RICE crop, for selfing crosses OR plots, where the germplasm state is fixed. This creates a single set of seed and package records that are linked to the planted germplasm. No new germplasm is created. This also creates seed relations records, which link the parent seed to the child seed.",
                    "config": {
                        "harvestType": "single",
                        "entities": [
                            {
                                "entity": "cross",
                                "identifier": "crossDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "crosses",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "existingCrossDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "crossDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCross": [
                                            {
                                                "condition": "existingCrossDbId is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "existingCrossDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "existingCrossDbId"
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "crossMethod": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "CROSS_METHOD",
                                        "scaleValueAbbrev": "SELFING"
                                    }
                                }
                            },
                            {
                                "entity": "cross_parent",
                                "identifier": "crossParentDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "cross-parents",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "crossCreated",
                                            "type": "previousDecision",
                                            "entity": "cross",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCrossParent": [
                                            {
                                                "condition": "crossCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentRole": {
                                        "type": "plainText",
                                        "textValue": "female-and-male"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1" 
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed",
                                "identifier": "seedDbId",
                                "retrieveRecord": true,
                                "orderNumber": 6,
                                "action": "create",
                                "apiEndpoint": "seeds",
                                "data": {
                                    "germplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "sourceEntryDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "entryDbId"
                                    },
                                    "sourcePlotDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "plotDbId"
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 7,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "package",
                                "identifier": "packageDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "packages",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createPackage": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {}
                            }
                        ]
                    }
                },
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": "SELFING",
                    "state": "fixed",
                    "type": "fixed_line",
                    "harvestMethodAbbrev": "SINGLE_PLANT_SEED_INCREASE",
                    "configDescription": "This harvest use case applies to the RICE crop, for selfing crosses OR plots, where the germplasm state is fixed and the germplasm type is fixed_line. This creates multiple sets of germplasm, seed, and package records, as well as other related records such as germplasm names, attributes, and relations records. The number of germplasm records created is based on the given `noOfPlant` value. For example, if the `noOfPlant` value is 5, then 5 germplasm records will be created, with each germplasm having its own seed and package records.",
                    "config": {
                        "harvestType": "multiple",
                        "numericVariable": {
                            "variableName": "noOfPlant",
                            "variableType": "integer"
                        },
                        "entities": [
                            {
                                "entity": "germplasm",
                                "identifier": "germplasmDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "germplasm",
                                "data": {
                                    "generation": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmGeneration"
                                    },
                                    "parentage" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmParentage"
                                    },
                                    "state" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmState"
                                    },
                                    "germplasmType" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmType"
                                    },
                                    "nameType": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_NAME_TYPE",
                                        "scaleValueAbbrev": "LINE_NAME"
                                    },
                                    "taxonomyDbId" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmTaxonomyId"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_name",
                                "identifier": "germplasmNameDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "germplasm-names",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmName": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "nameValue": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "designation"
                                    },
                                    "germplasmNameType": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "germplasmNameType"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_name",
                                "identifier": "germplasmNameDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "germplasm-names",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "plantedGermplasmBCDerivName",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm-names-search",
                                            "apiParams": {
                                                "germplasmDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                },
                                                "germplasmNameType": {
                                                    "type": "recordInfo",
                                                    "subType": "buildData",
                                                    "fieldName": "germplasmNameType"
                                                }
                                            },
                                            "apiResultFieldName": "nameValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmName": [
                                            {
                                                "condition": "germplasmCreated is true and plantedGermplasmBCDerivName is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "nameValue": {
                                                            "type": "nameBuilder",
                                                            "specialType": "BACKCROSS_DERIVATIVE_NAME"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "germplasmNameType": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_NAME_TYPE",
                                        "scaleValueAbbrev": "BACKCROSS_DERIVATIVE"
                                    },
                                    "germplasmNameStatus": {
                                        "type": "plainText",
                                        "textValue": "active"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_relation",
                                "identifier": "germplasmRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 3,
                                "action": "create",
                                "apiEndpoint": "germplasm-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmRelation": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentGermplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "germplasmYear",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "GERMPLASM_YEAR"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and germplasmYear is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "germplasmYear"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "GERMPLASM_YEAR"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "mtaStatus",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "MTA_STATUS"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and mtaStatus is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "mtaStatus"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "MTA_STATUS"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "origin",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "ORIGIN"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and origin is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "origin"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "ORIGIN"
                                    }
                                }
                            },
                            {
                                "entity": "family_member",
                                "identifier": "familyMemberDbId",
                                "retrieveRecord": false,
                                "orderNumber": 5,
                                "action": "create",
                                "apiEndpoint": "family-members",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "familyDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "familyDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createFamilyMember": [
                                            {
                                                "condition": "germplasmCreated is true and familyDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "familyDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "familyDbId"
                                    }
                                }
                            },
                            {
                                "entity": "cross",
                                "identifier": "crossDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "crosses",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "existingCrossDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "crossDbId"
                                        },
                                        {
                                            "referenceName": "newCrossDbId",
                                            "type": "recordInfo",
                                            "subType": "new",
                                            "fieldName": "crossDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCross": [
                                            {
                                                "condition": "existingCrossDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "existingCrossDbId"
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "newCrossDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "newCrossDbId"
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "existingCrossDbId is null and newCrossDbId is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "crossMethod": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "CROSS_METHOD",
                                        "scaleValueAbbrev": "SELFING"
                                    }
                                }
                            },
                            {
                                "entity": "cross_parent",
                                "identifier": "crossParentDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "cross-parents",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "crossCreated",
                                            "type": "previousDecision",
                                            "entity": "cross",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCrossParent": [
                                            {
                                                "condition": "crossCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentRole": {
                                        "type": "plainText",
                                        "textValue": "female-and-male"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1" 
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed",
                                "identifier": "seedDbId",
                                "retrieveRecord": true,
                                "orderNumber": 6,
                                "action": "create",
                                "apiEndpoint": "seeds",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "existingGermplasmUsed",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "useExisting"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeed": [
                                            {
                                                "condition": "germplasmCreated is true or existingGermplasmUsed is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "sourceEntryDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "entryDbId"
                                    },
                                    "sourcePlotDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "plotDbId"
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 7,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "package",
                                "identifier": "packageDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "packages",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createPackage": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {}
                            }
                        ]
                    }
                },
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": "SELFING",
                    "state": [
                        "fixed", "not_fixed"
                    ],
                    "type": [
                        "transgenic", "genome_edited"
                    ],
                    "harvestMethodAbbrev": "BULK",
                    "configDescription": "This harvest use case applies to the RICE crop, for selfing crosses OR plots, where the germplasm state is fixed or not_fixed, and the germplasm type is transgenic or genome_edited. Via the BULK harvest method, this creates a new germplasm record, with its own seed and package records. It also creates other related records such as germplasm names, attributes, and relations records. The germplasm state and type is inherited from the planted germplasm.",
                    "config": {
                        "harvestType": "single",
                        "entities": [
                            {
                                "entity": "germplasm",
                                "identifier": "germplasmDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "germplasm",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "existingGermplasmDbId",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm-names-search",
                                            "apiParams": {
                                                "nameValue": {
                                                    "type": "recordInfo",
                                                    "subType": "buildData",
                                                    "fieldName": "designation"
                                                }
                                            },
                                            "apiResultFieldName": "germplasmDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasm": [
                                            {
                                                "condition": "existingGermplasmDbId is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "existingGermplasmDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "existingGermplasmDbId"
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "generation": {
                                        "type": "generationAdvancement",
                                        "base": "F"
                                    },
                                    "parentage" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmParentage"
                                    },
                                    "state" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmState"
                                    },
                                    "germplasmType" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmType"
                                    },
                                    "nameType": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_NAME_TYPE",
                                        "scaleValueAbbrev": "DERIVATIVE_NAME"
                                    },
                                    "taxonomyDbId" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmTaxonomyId"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_name",
                                "identifier": "germplasmNameDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "germplasm-names",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmName": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "nameValue": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "designation"
                                    },
                                    "germplasmNameType": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "germplasmNameType"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_relation",
                                "identifier": "germplasmRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 3,
                                "action": "create",
                                "apiEndpoint": "germplasm-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmRelation": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentGermplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "germplasmYear",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "GERMPLASM_YEAR"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and germplasmYear is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "germplasmYear"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "GERMPLASM_YEAR"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "mtaStatus",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "MTA_STATUS"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and mtaStatus is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "mtaStatus"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "MTA_STATUS"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "origin",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "ORIGIN"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and origin is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "origin"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "ORIGIN"
                                    }
                                }
                            },
                            {
                                "entity": "family_member",
                                "identifier": "familyMemberDbId",
                                "retrieveRecord": false,
                                "orderNumber": 5,
                                "action": "create",
                                "apiEndpoint": "family-members",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "familyDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "familyDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createFamilyMember": [
                                            {
                                                "condition": "germplasmCreated is true and familyDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "familyDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "familyDbId"
                                    }
                                }
                            },
                            {
                                "entity": "cross",
                                "identifier": "crossDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "crosses",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "existingCrossDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "crossDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCross": [
                                            {
                                                "condition": "existingCrossDbId is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "existingCrossDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "existingCrossDbId"
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "crossMethod": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "CROSS_METHOD",
                                        "scaleValueAbbrev": "SELFING"
                                    }
                                }
                            },
                            {
                                "entity": "seed",
                                "identifier": "seedDbId",
                                "retrieveRecord": true,
                                "orderNumber": 6,
                                "action": "create",
                                "apiEndpoint": "seeds",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "existingGermplasmUsed",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "useExisting"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeed": [
                                            {
                                                "condition": "germplasmCreated is true or existingGermplasmUsed is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "sourceEntryDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "entryDbId"
                                    },
                                    "sourcePlotDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "plotDbId"
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 7,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "package",
                                "identifier": "packageDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "packages",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createPackage": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {}
                            }
                        ]
                    }
                },
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": "SELFING",
                    "state": [
                        "fixed", "not_fixed"
                    ],
                    "type": [
                        "transgenic", "genome_edited"
                    ],
                    "harvestMethodAbbrev": "SINGLE_PLANT_SELECTION",
                    "configDescription": "This harvest use case applies to the RICE crop, for selfing crosses OR plots, where the germplasm state is fixed or not_fixed, and the germplasm type is transgenic or genome_edited. Via the SINGLE_PLANT_SELECTION harvest method, this creates N new germplasm records, where N is the number of plants value. Each germplasm has its own seed and package records. It also creates other related records such as germplasm names, attributes, and relations records. The germplasm state and type is inherited from the planted germplasm.",
                    "config": {
                        "harvestType": "multiple",
                        "numericVariable": {
                            "variableName": "noOfPlant",
                            "variableType": "integer"
                        },
                        "entities": [
                            {
                                "entity": "germplasm",
                                "identifier": "germplasmDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "germplasm",
                                "data": {
                                    "generation": {
                                        "type": "generationAdvancement",
                                        "base": "F"
                                    },
                                    "parentage" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmParentage"
                                    },
                                    "state" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmState"
                                    },
                                    "germplasmType" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmType"
                                    },
                                    "nameType": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_NAME_TYPE",
                                        "scaleValueAbbrev": "DERIVATIVE_NAME"
                                    },
                                    "taxonomyDbId" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmTaxonomyId"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_name",
                                "identifier": "germplasmNameDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "germplasm-names",
                                "data": {
                                    "nameValue": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "designation"
                                    },
                                    "germplasmNameType": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "germplasmNameType"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_relation",
                                "identifier": "germplasmRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 3,
                                "action": "create",
                                "apiEndpoint": "germplasm-relations",
                                "data": {
                                    "parentGermplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "germplasmYear",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "GERMPLASM_YEAR"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and germplasmYear is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "germplasmYear"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "GERMPLASM_YEAR"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "mtaStatus",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "MTA_STATUS"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and mtaStatus is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "mtaStatus"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "MTA_STATUS"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "origin",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "ORIGIN"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and origin is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "origin"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "ORIGIN"
                                    }
                                }
                            },
                            {
                                "entity": "family_member",
                                "identifier": "familyMemberDbId",
                                "retrieveRecord": false,
                                "orderNumber": 5,
                                "action": "create",
                                "apiEndpoint": "family-members",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "familyDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "familyDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createFamilyMember": [
                                            {
                                                "condition": "germplasmCreated is true and familyDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "familyDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "familyDbId"
                                    }
                                }
                            },
                            {
                                "entity": "cross",
                                "identifier": "crossDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "crosses",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "existingCrossDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "crossDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCross": [
                                            {
                                                "condition": "existingCrossDbId is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "existingCrossDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "existingCrossDbId"
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "crossMethod": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "CROSS_METHOD",
                                        "scaleValueAbbrev": "SELFING"
                                    }
                                }
                            },
                            {
                                "entity": "seed",
                                "identifier": "seedDbId",
                                "retrieveRecord": true,
                                "orderNumber": 6,
                                "action": "create",
                                "apiEndpoint": "seeds",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "existingGermplasmUsed",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "useExisting"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeed": [
                                            {
                                                "condition": "germplasmCreated is true or existingGermplasmUsed is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "sourceEntryDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "entryDbId"
                                    },
                                    "sourcePlotDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "plotDbId"
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 7,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "package",
                                "identifier": "packageDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "packages",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createPackage": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {}
                            }
                        ]
                    }
                },
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": "SELFING",
                    "stageCode": "TCV",
                    "state": "not_fixed",
                    "config": {
                        "harvestType": "single",
                        "entities": []
                    }
                },
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": "SELFING",
                    "stageCode": "TCV",
                    "state": "fixed",
                    "config": {
                        "harvestType": "single",
                        "entities": [
                            {
                                "entity": "germplasm",
                                "identifier": "germplasmDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "germplasm",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "hybridMaterialType",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "plots/:id/data-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "plotDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HYB_MATERIAL_TYPE"
                                                }
                                            },
                                            "apiResultInnerFieldName": "data",
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasm": [
                                            {
                                                "condition": "hybridMaterialType is null",
                                                "decision": {
                                                    "flags": {
                                                        "terminateValidation": true,
                                                        "terminateUseCase": true,
                                                        "useCaseFailed": true
                                                    },
                                                    "messages": [
                                                        {
                                                            "type": "ERROR",
                                                            "message": "The hybrid material type is not specified for this plot. Please upload the correct data via Data Collection, and try again."
                                                        }
                                                    ]
                                                }
                                            },
                                            {
                                                "condition": "hybridMaterialType != R",
                                                "decision": {
                                                    "flags": {
                                                        "terminateValidation": true,
                                                        "terminateUseCase": true,
                                                        "useCaseFailed": true
                                                    },
                                                    "messages": [
                                                        {
                                                            "type": "ERROR",
                                                            "message": "The hybrid material type specified for this plot is not equal to 'R'. Please upload the correct data via Data Collection, and try again."
                                                        }
                                                    ]
                                                }
                                            },
                                            {
                                                "condition": "hybridMaterialType = R",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "generation": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmGeneration"
                                    },
                                    "parentage" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmParentage"
                                    },
                                    "state" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmState"
                                    },
                                    "germplasmType" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmType"
                                    },
                                    "nameType": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_NAME_TYPE",
                                        "scaleValueAbbrev": "LINE_NAME"
                                    },
                                    "taxonomyDbId" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmTaxonomyId"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_name",
                                "identifier": "germplasmNameDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "germplasm-names",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmName": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "nameValue": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "designation"
                                    },
                                    "germplasmNameType": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "germplasmNameType"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_relation",
                                "identifier": "germplasmRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 3,
                                "action": "create",
                                "apiEndpoint": "germplasm-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmRelation": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentGermplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "family_member",
                                "identifier": "familyMemberDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "family-members",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "familyDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "familyDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createFamilyMember": [
                                            {
                                                "condition": "germplasmCreated is true and familyDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "familyDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "familyDbId"
                                    }
                                }
                            },
                            {
                                "entity": "cross",
                                "identifier": "crossDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "crosses",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "existingCrossDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "crossDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCross": [
                                            {
                                                "condition": "existingCrossDbId is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "existingCrossDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "existingCrossDbId"
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "crossMethod": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "CROSS_METHOD",
                                        "scaleValueAbbrev": "SELFING"
                                    }
                                }
                            },
                            {
                                "entity": "cross_parent",
                                "identifier": "crossParentDbId",
                                "retrieveRecord": false,
                                "orderNumber": 5,
                                "action": "create",
                                "apiEndpoint": "cross-parents",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "crossDbId",
                                            "type": "previousDecision",
                                            "entity": "cross",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCrossParent": [
                                            {
                                                "condition": "germplasmCreated is true and crossDbId is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    },
                                    "germplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "parentType": {
                                        "type": "plainText",
                                        "textValue": "PARENT"
                                    }
                                }
                            },
                            {
                                "entity": "seed",
                                "identifier": "seedDbId",
                                "retrieveRecord": true,
                                "orderNumber": 6,
                                "action": "create",
                                "apiEndpoint": "seeds",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "existingGermplasmUsed",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "useExisting"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeed": [
                                            {
                                                "condition": "germplasmCreated is true or existingGermplasmUsed is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "sourceEntryDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "entryDbId"
                                    },
                                    "sourcePlotDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "plotDbId"
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 7,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "package",
                                "identifier": "packageDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "packages",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createPackage": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {}
                            }
                        ]
                    }
                },
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": [
                        "SINGLE_CROSS",
                        "DOUBLE_CROSS",
                        "THREE_WAY_CROSS",
                        "COMPLEX_CROSS"
                    ],
                    "harvestMethodAbbrev": "BULK",
                    "configDescription": "This harvest use case applies to the RICE crop, for CROSSES where the cross method is single cross, double cross, three-way cross, or complex cross. This creates one new germplasm, seed, and package record for each cross. It also creates other related records such as germplasm names, attributes, and relations records.",
                    "config": {
                        "harvestType": "single",
                        "entities": [
                            {
                                "entity": "cross",
                                "identifier": "crossDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "update",
                                "apiEndpoint": "crosses",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "crossDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "crossDbId"
                                        },
                                        {
                                            "type": "apiRetrieval",
                                            "subType": "multiValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm-children-search?femaleParentGermplasmDbId=:femaleId&maleParentGermplasmDbId=:maleId&ordered=true&includeChildInfo=true&includeCrossInfo=true&limit=1",
                                            "urlReplacements": {
                                                "femaleId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "femaleGermplasmDbId"
                                                },
                                                "maleId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "maleGermplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "programDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "programDbId"
                                                }
                                            },
                                            "apiResultFieldNames": [
                                                {
                                                    "apiResultFieldName": "childGermplasmDbId",
                                                    "referenceName": "existingChildGermplasmDbId"
                                                },
                                                {
                                                    "apiResultFieldName": "crossName",
                                                    "referenceName": "existingCrossName"
                                                }
                                            ]
                                        }
                                    ],
                                    "decisions": {
                                        "1_updateCross": [
                                            {
                                                "condition": "existingChildGermplasmDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "update": true,
                                                        "buildNewCrossName": false
                                                    },
                                                    "options": {
                                                        "identifier": "crossDbId"
                                                    },
                                                    "data": {
                                                        "crossName": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "existingCrossName"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "existingChildGermplasmDbId is null",
                                                "decision": {
                                                    "flags": {
                                                        "update": true,
                                                        "buildNewCrossName": true
                                                    },
                                                    "options": {
                                                        "identifier": "crossDbId"
                                                    },
                                                    "data": {
                                                        "crossName": {
                                                            "type": "nameBuilder"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                }
                            },
                            {
                                "entity": "germplasm",
                                "identifier": "germplasmDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "germplasm",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "noOfSeed",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "noOfSeed"
                                        },
                                        {
                                            "referenceName": "newCrossNameGenerated",
                                            "type": "previousDecision",
                                            "entity": "cross",
                                            "decisionName": "buildNewCrossName"
                                        },
                                        {
                                            "referenceName": "femaleGermplasmGeneration",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "femaleGermplasmGeneration"
                                        },
                                        {
                                            "referenceName": "maleGermplasmGeneration",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "maleGermplasmGeneration"
                                        },
                                        {
                                            "referenceName": "existingChildGermplasmDbId",
                                            "type": "recordInfo",
                                            "subType": "retrieved",
                                            "fieldName": "existingChildGermplasmDbId"
                                        },
                                        {
                                            "type": "apiRetrieval",
                                            "subType": "multiValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "taxonomies-search",
                                            "apiParams": {
                                                "taxonomyDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "femaleGermplasmTaxonomyDbId"
                                                }
                                            },
                                            "apiResultFieldNames": [
                                                {
                                                    "apiResultFieldName": "taxonomyDbId",
                                                    "referenceName": "femaleGermplasmTaxonomyDbId"
                                                },
                                                {
                                                    "apiResultFieldName": "taxonomyName",
                                                    "referenceName": "femaleGermplasmTaxonomyName"
                                                }
                                            ]
                                        },
                                        {
                                            "type": "apiRetrieval",
                                            "subType": "multiValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "taxonomies-search",
                                            "apiParams": {
                                                "taxonomyDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "maleGermplasmTaxonomyDbId"
                                                }
                                            },
                                            "apiResultFieldNames": [
                                                {
                                                    "apiResultFieldName": "taxonomyDbId",
                                                    "referenceName": "maleGermplasmTaxonomyDbId"
                                                },
                                                {
                                                    "apiResultFieldName": "taxonomyName",
                                                    "referenceName": "maleGermplasmTaxonomyName"
                                                }   
                                            ]
                                        },
                                        {
                                            "referenceName": "femaleGermplasmType",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "femaleGermplasmType"
                                        },
                                        {
                                            "referenceName": "maleGermplasmType",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "maleGermplasmType"
                                        }
                                    ],
                                    "decisions": {
                                        "1_noOfSeedCheck": [
                                            {
                                                "condition": "noOfSeed = 0",
                                                "decision": {
                                                    "flags": {
                                                        "skip": true,
                                                        "terminateValidation": true,
                                                        "terminateUseCase": true
                                                    },
                                                    "messages": [
                                                        {
                                                            "type": "INFO",
                                                            "message": "No records were created because the number of seeds value is 0."
                                                        }
                                                    ]
                                                }
                                            }
                                        ],
                                        "2_crossNameCheck": [
                                            {
                                                "condition": "newCrossNameGenerated is false",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "existingChildGermplasmDbId"
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "newCrossNameGenerated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ],
                                        "3_parentageLogic": [
                                            {
                                                "condition": "femaleGermplasmGeneration contains F1 and maleGermplasmGeneration contains F1",
                                                "decision": {
                                                    "data": {
                                                        "parentage": {
                                                            "type": "parentageBuilder",
                                                            "subType": "delimiterNotation",
                                                            "delimiterPrimary": "/",
                                                            "leftHandSide": {
                                                                "type": "recordInfo",
                                                                "subType": "planted",
                                                                "fieldName": "femaleParentage"
                                                            },
                                                            "rightHandSide": {
                                                                "type": "recordInfo",
                                                                "subType": "planted",
                                                                "fieldName": "maleParentage"
                                                            }
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "femaleGermplasmGeneration contains F1",
                                                "decision": {
                                                    "data": {
                                                        "parentage": {
                                                            "type": "parentageBuilder",
                                                            "subType": "delimiterNotation",
                                                            "delimiterPrimary": "/",
                                                            "leftHandSide": {
                                                                "type": "recordInfo",
                                                                "subType": "planted",
                                                                "fieldName": "femaleParentage"
                                                            },
                                                            "rightHandSide": {
                                                                "type": "recordInfo",
                                                                "subType": "planted",
                                                                "fieldName": "maleGermplasmName"
                                                            }
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "maleGermplasmGeneration contains F1",
                                                "decision": {
                                                    "data": {
                                                        "parentage": {
                                                            "type": "parentageBuilder",
                                                            "subType": "delimiterNotation",
                                                            "delimiterPrimary": "/",
                                                            "leftHandSide": {
                                                                "type": "recordInfo",
                                                                "subType": "planted",
                                                                "fieldName": "femaleGermplasmName"
                                                            },
                                                            "rightHandSide": {
                                                                "type": "recordInfo",
                                                                "subType": "planted",
                                                                "fieldName": "maleParentage"
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        ],
                                        "4_taxonomyLogic": [
                                            {
                                                "condition": "femaleGermplasmTaxonomyDbId = maleGermplasmTaxonomyDbId",
                                                "decision": {
                                                    "data": {
                                                        "taxonomyDbId": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "femaleGermplasmTaxonomyDbId"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "femaleGermplasmTaxonomyDbId != maleGermplasmTaxonomyDbId and femaleGermplasmTaxonomyName = Oryza sativa",
                                                "decision": {
                                                    "data": {
                                                        "taxonomyDbId": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "femaleGermplasmTaxonomyDbId"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "femaleGermplasmTaxonomyDbId != maleGermplasmTaxonomyDbId and maleGermplasmTaxonomyName = Oryza sativa",
                                                "decision": {
                                                    "data": {
                                                        "taxonomyDbId": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "maleGermplasmTaxonomyDbId"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "femaleGermplasmTaxonomyDbId!= maleGermplasmTaxonomyDbId",
                                                "decision": {
                                                    "data": {
                                                        "taxonomyDbId": {
                                                            "type": "apiRetrieval",
                                                            "subType": "singleValue",
                                                            "apiMethod": "POST",
                                                            "apiEndpoint": "taxonomies-search",
                                                            "apiParams": {
                                                                "taxonId": {
                                                                    "type": "plainText",
                                                                    "textValue": "unknown"
                                                                }
                                                            },
                                                            "apiResultFieldName": "taxonomyDbId"
                                                        }
                                                    }
                                                }
                                            }
                                        ],
                                        "5_germplasmTypeLogic": [
                                            {
                                                "condition": "if femaleGermplasmType is null and maleGermplasmType or femaleGermplasmType is not in array ['transgenic', 'genome_edited'] and maleGermplasmType is not in array ['transgenic', 'genome_edited'] or femaleGermplasmType is in array ['transgenic', 'genome_edited'] and maleGermplasmType is in array ['transgenic', 'genome_edited'] and femaleGermplasmType != maleGermplasmType",
                                                "decision": {
                                                    "data": {
                                                        "germplasmType": {
                                                            "type": "scaleValue",
                                                            "variableAbbrev": "GERMPLASM_TYPE",
                                                            "scaleValueAbbrev": "PROGENY"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "femaleGermplasmType = transgenic and maleGermplasmType != genome_edited or femaleGermplasmType != genome_edited and maleGermplasmType = transgenic",
                                                "decision": {
                                                    "data": {
                                                        "germplasmType": {
                                                            "type": "scaleValue",
                                                            "variableAbbrev": "GERMPLASM_TYPE",
                                                            "scaleValueAbbrev": "TRANSGENIC"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "femaleGermplasmType = genome_edited and maleGermplasmType != transgenic or femaleGermplasmType != transgenic and maleGermplasmType = genome_edited",
                                                "decision": {
                                                    "data": {
                                                        "germplasmType": {
                                                            "type": "scaleValue",
                                                            "variableAbbrev": "GERMPLASM_TYPE",
                                                            "scaleValueAbbrev": "GENOME_EDITED"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "generation": {
                                        "type": "plainText",
                                        "textValue": "F1"
                                    },
                                    "state" : {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_STATE",
                                        "scaleValueAbbrev": "NOT_FIXED"
                                    },
                                    "nameType" : {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_NAME_TYPE",
                                        "scaleValueAbbrev": "CROSS_NAME"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_name",
                                "identifier": "germplasmNameDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "germplasm-names",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmName": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "nameValue": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "designation"
                                    },
                                    "germplasmNameType": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "germplasmNameType"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_name",
                                "identifier": "germplasmNameDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "germplasm-names",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmName": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "nameValue": {
                                        "type": "nameBuilder",
                                        "specialType": "BACKCROSS_DERIVATIVE_NAME"
                                    },
                                    "germplasmNameType": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_NAME_TYPE",
                                        "scaleValueAbbrev": "BACKCROSS_DERIVATIVE"
                                    },
                                    "germplasmNameStatus": {
                                        "type": "plainText",
                                        "textValue": "active"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_relation",
                                "identifier": "germplasmRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 3,
                                "action": "create",
                                "apiEndpoint": "germplasm-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmRelation": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentGermplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "femaleGermplasmDbId"
                                    },
                                    "childGermplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_relation",
                                "identifier": "germplasmRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 3,
                                "action": "create",
                                "apiEndpoint": "germplasm-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmRelation": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentGermplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "maleGermplasmDbId"
                                    },
                                    "childGermplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "2"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "femaleGermplasmTaxonomyName",
                                            "type": "recordInfo",
                                            "subType": "retrieved",
                                            "fieldName": "femaleGermplasmTaxonomyName"
                                        },
                                        {
                                            "referenceName": "maleGermplasmTaxonomyName",
                                            "type": "recordInfo",
                                            "subType": "retrieved",
                                            "fieldName": "maleGermplasmTaxonomyName"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and femaleGermplasmTaxonomyName != maleGermplasmTaxonomyName",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "nameBuilder",
                                                            "specialType": "TAXONOMY_NAME"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "TAXONOMY_NAME"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "MTA_STATUS"
                                    },
                                    "dataValue": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "MTA_STATUS",
                                        "scaleValueAbbrev": "PUD2"
                                    }
                                }
                            },
                            {
                                "entity": "family",
                                "identifier": "familyDbId",
                                "retrieveRecord": true,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "families",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createFamily": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {}
                            },
                            {
                                "entity": "family_member",
                                "identifier": "familyMemberDbId",
                                "retrieveRecord": false,
                                "orderNumber": 5,
                                "action": "create",
                                "apiEndpoint": "family-members",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "familyCreated",
                                            "type": "previousDecision",
                                            "entity": "family",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createFamilyMember": [
                                            {
                                                "condition": "germplasmCreated is true and familyCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "familyDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "familyDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed",
                                "identifier": "seedDbId",
                                "retrieveRecord": true,
                                "orderNumber": 6,
                                "action": "create",
                                "apiEndpoint": "seeds",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "existingGermplasmUsed",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "useExisting"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeed": [
                                            {
                                                "condition": "germplasmCreated is true or existingGermplasmUsed is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 7,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "femaleSeedDbId"
                                    },
                                    "childSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 7,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "maleSeedDbId"
                                    },
                                    "childSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "2"
                                    }
                                }
                            },
                            {
                                "entity": "seed_attribute",
                                "identifier": "seedAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "seed-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedAttribute": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "USE"
                                    },
                                    "dataValue": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "USE",
                                        "scaleValueAbbrev": "ACTIVE"
                                    }
                                }
                            },
                            {
                                "entity": "package",
                                "identifier": "packageDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "packages",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createPackage": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "packageUnit": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "PACKAGE_UNIT",
                                        "scaleValueAbbrev": "SEEDS"
                                    },
                                    "packageQuantity": {
                                        "type": "plainText",
                                        "textValue": "0"
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    "cropCode": "MAIZE",
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "harvestMethodAbbrev": "BULK",
                    "configDescription": "This harvest use case applies to the MAIZE crop, for selfing crosses OR plots, where the germplasm state is not_fixed. For each selfing cross OR plot, it creates ONE new germplasm, ONE new seed, and ONE new package, as well as other related records such as germplasm names, attributes, and relations records.",
                    "config": {
                        "harvestType": "single",
                        "entities": [
                            {
                                "entity": "germplasm",
                                "identifier": "germplasmDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "germplasm",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "noOfEar",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "noOfEar"
                                        },
                                        {
                                            "referenceName": "existingGermplasmDbId",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm-names-search",
                                            "apiParams": {
                                                "nameValue": {
                                                    "type": "recordInfo",
                                                    "subType": "buildData",
                                                    "fieldName": "designation"
                                                }
                                            },
                                            "apiResultFieldName": "germplasmDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_noOfEarCheck": [
                                            {
                                                "condition": "noOfEar is not null and noOfEar = 0",
                                                "decision": {
                                                    "flags": {
                                                        "skip": true,
                                                        "terminateValidation": true,
                                                        "terminateUseCase": true
                                                    },
                                                    "messages": [
                                                        {
                                                            "type": "INFO",
                                                            "message": "No records were created because the number of ears value is 0."
                                                        }
                                                    ]
                                                }
                                            }
                                        ],
                                        "1_createGermplasm": [
                                            {
                                                "condition": "existingGermplasmDbId is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "existingGermplasmDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "existingGermplasmDbId"
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "germplasmCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "MGE"
                                    },
                                    "generation": {
                                        "type": "generationAdvancement",
                                        "base": "F"
                                    },
                                    "parentage" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmParentage"
                                    },
                                    "state" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmState"
                                    },
                                    "germplasmType" : {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_TYPE",
                                        "scaleValueAbbrev": "SEGREGATING"
                                    },
                                    "nameType" : {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_NAME_TYPE",
                                        "scaleValueAbbrev": "PEDIGREE"
                                    },
                                    "taxonomyDbId" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmTaxonomyId"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_name",
                                "identifier": "germplasmNameDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "germplasm-names",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmName": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "nameValue": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "designation"
                                    },
                                    "germplasmNameType": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "germplasmNameType"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_relation",
                                "identifier": "germplasmRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 3,
                                "action": "create",
                                "apiEndpoint": "germplasm-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmRelation": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentGermplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "growthHabit",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "GROWTH_HABIT"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and growthHabit is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "growthHabit"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "GROWTH_HABIT"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "heteroticGroup",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HETEROTIC_GROUP"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and heteroticGroup is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "heteroticGroup"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "HETEROTIC_GROUP"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "grainColor",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "GRAIN_COLOR"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and grainColor is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "grainColor"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "GRAIN_COLOR"
                                    }
                                }
                            },
                            {
                                "entity": "family_member",
                                "identifier": "familyMemberDbId",
                                "retrieveRecord": false,
                                "orderNumber": 5,
                                "action": "create",
                                "apiEndpoint": "family-members",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "familyDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "familyDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createFamilyMember": [
                                            {
                                                "condition": "germplasmCreated is true and familyDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "familyDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "familyDbId"
                                    },
                                    "familyCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "MFM"
                                    }
                                }
                            },
                            {
                                "entity": "cross",
                                "identifier": "crossDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "crosses",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "existingCrossDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "crossDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCross": [
                                            {
                                                "condition": "existingCrossDbId is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "existingCrossDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "existingCrossDbId"
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "crossMethod": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "CROSS_METHOD",
                                        "scaleValueAbbrev": "SELFING"
                                    }
                                }
                            },
                            {
                                "entity": "cross_parent",
                                "identifier": "crossParentDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "cross-parents",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "crossCreated",
                                            "type": "previousDecision",
                                            "entity": "cross",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCrossParent": [
                                            {
                                                "condition": "crossCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentRole": {
                                        "type": "plainText",
                                        "textValue": "female-and-male"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1" 
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed",
                                "identifier": "seedDbId",
                                "retrieveRecord": true,
                                "orderNumber": 6,
                                "action": "create",
                                "apiEndpoint": "seeds",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "existingGermplasmUsed",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "useExisting"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeed": [
                                            {
                                                "condition": "germplasmCreated is true or existingGermplasmUsed is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "seedCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "MSD"
                                    },
                                    "sourceEntryDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "entryDbId"
                                    },
                                    "sourcePlotDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "plotDbId"
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 7,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "occurrence_data",
                                "identifier": "occurrenceDataDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "occurrence-data",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createOccurrenceData": [
                                            {
                                                "condition": "harvestedPackagePrefix is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "HARVESTED_PACKAGE_PREFIX"
                                    },
                                    "dataValue": {
                                        "type": "nameBuilder",
                                        "specialType": "HARVESTED_PACKAGE_PREFIX"
                                    }
                                }
                            },
                            {
                                "entity": "package",
                                "identifier": "packageDbId",
                                "retrieveRecord": false,
                                "orderNumber": 9,
                                "action": "create",
                                "apiEndpoint": "packages",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createPackage": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "packageLabel": {
                                                            "type": "nameBuilder"
                                                        }
                                                    }
                                                }   
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "packageCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "MPK"
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    "cropCode": "MAIZE",
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "harvestMethodAbbrev": "INDIVIDUAL_EAR",
                    "configDescription": "This harvest use case applies to the MAIZE crop, for selfing crosses OR plots, where the germplasm state is not_fixed. Via the INDIVIDUAL EAR harvest method, it creates N germplasm records, where N is equal to the number of ears input. For each germplasm created, it also creates ONE new seed and ONE new package, as well as other related records such as germplasm names, attributes, and relations records.",
                    "config": {
                        "harvestType": "multiple",
                        "numericVariable": {
                            "variableName": "noOfEar",
                            "variableType": "integer"
                        },
                        "entities": [
                            {
                                "entity": "germplasm",
                                "identifier": "germplasmDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "germplasm",
                                "data": {
                                    "germplasmCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "MGE"
                                    },
                                    "generation": {
                                        "type": "generationAdvancement",
                                        "base": "F"
                                    },
                                    "parentage" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmParentage"
                                    },
                                    "state" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmState"
                                    },
                                    "germplasmType" : {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_TYPE",
                                        "scaleValueAbbrev": "SEGREGATING"
                                    },
                                    "nameType" : {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_NAME_TYPE",
                                        "scaleValueAbbrev": "PEDIGREE"
                                    },
                                    "taxonomyDbId" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmTaxonomyId"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_name",
                                "identifier": "germplasmNameDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "germplasm-names",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmName": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "nameValue": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "designation"
                                    },
                                    "germplasmNameType": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "germplasmNameType"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_relation",
                                "identifier": "germplasmRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 3,
                                "action": "create",
                                "apiEndpoint": "germplasm-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmRelation": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentGermplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "growthHabit",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "GROWTH_HABIT"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and growthHabit is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "growthHabit"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "GROWTH_HABIT"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "heteroticGroup",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HETEROTIC_GROUP"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and heteroticGroup is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "heteroticGroup"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "HETEROTIC_GROUP"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "grainColor",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "GRAIN_COLOR"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and grainColor is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "grainColor"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "GRAIN_COLOR"
                                    }
                                }
                            },
                            {
                                "entity": "family_member",
                                "identifier": "familyMemberDbId",
                                "retrieveRecord": false,
                                "orderNumber": 5,
                                "action": "create",
                                "apiEndpoint": "family-members",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "familyDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "familyDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createFamilyMember": [
                                            {
                                                "condition": "germplasmCreated is true and familyDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "familyDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "familyDbId"
                                    },
                                    "familyCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "MFM"
                                    }
                                }
                            },
                            {
                                "entity": "cross",
                                "identifier": "crossDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "crosses",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "existingCrossDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "crossDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCross": [
                                            {
                                                "condition": "existingCrossDbId is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "existingCrossDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "existingCrossDbId"
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "crossMethod": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "CROSS_METHOD",
                                        "scaleValueAbbrev": "SELFING"
                                    }
                                }
                            },
                            {
                                "entity": "cross_parent",
                                "identifier": "crossParentDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "cross-parents",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "crossCreated",
                                            "type": "previousDecision",
                                            "entity": "cross",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCrossParent": [
                                            {
                                                "condition": "crossCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentRole": {
                                        "type": "plainText",
                                        "textValue": "female-and-male"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1" 
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed",
                                "identifier": "seedDbId",
                                "retrieveRecord": true,
                                "orderNumber": 6,
                                "action": "create",
                                "apiEndpoint": "seeds",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "existingGermplasmUsed",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "useExisting"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeed": [
                                            {
                                                "condition": "germplasmCreated is true or existingGermplasmUsed is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "seedCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "MSD"
                                    },
                                    "sourceEntryDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "entryDbId"
                                    },
                                    "sourcePlotDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "plotDbId"
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 7,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "occurrence_data",
                                "identifier": "occurrenceDataDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "occurrence-data",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createOccurrenceData": [
                                            {
                                                "condition": "harvestedPackagePrefix is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "HARVESTED_PACKAGE_PREFIX"
                                    },
                                    "dataValue": {
                                        "type": "nameBuilder",
                                        "specialType": "HARVESTED_PACKAGE_PREFIX"
                                    }
                                }
                            },
                            {
                                "entity": "package",
                                "identifier": "packageDbId",
                                "retrieveRecord": false,
                                "orderNumber": 9,
                                "action": "create",
                                "apiEndpoint": "packages",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createPackage": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "packageLabel": {
                                                            "type": "nameBuilder"
                                                        }
                                                    }
                                                }   
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "packageCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "MPK"
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    "cropCode": "MAIZE",
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "type": [
                        "landrace", "composite",
                        "synthetic", "population"
                    ],
                    "harvestMethodAbbrev": "MAINTAIN_AND_BULK",
                    "configDescription": "This harvest use case applies to the MAIZE crop, for selfing crosses OR plots, where the germplasm state is not_fixed and the germplasm type is landrace, composite, synthetic, or population. Via the MAINTAIN_AND_BULK harvest method, it creates ONE new seed, and ONE new package, as well as other related records such as seed relation records.",
                    "config": {
                        "harvestType": "single",
                        "entities": [
                            {
                                "entity": "cross",
                                "identifier": "crossDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "crosses",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "existingCrossDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "crossDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCross": [
                                            {
                                                "condition": "existingCrossDbId is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "existingCrossDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "existingCrossDbId"
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "crossMethod": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "CROSS_METHOD",
                                        "scaleValueAbbrev": "SELFING"
                                    }
                                }
                            },
                            {
                                "entity": "cross_parent",
                                "identifier": "crossParentDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "cross-parents",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "crossCreated",
                                            "type": "previousDecision",
                                            "entity": "cross",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCrossParent": [
                                            {
                                                "condition": "crossCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentRole": {
                                        "type": "plainText",
                                        "textValue": "female-and-male"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1" 
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed",
                                "identifier": "seedDbId",
                                "retrieveRecord": true,
                                "orderNumber": 6,
                                "action": "create",
                                "apiEndpoint": "seeds",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "noOfEar",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "noOfEar"
                                        }
                                    ],
                                    "decisions": {
                                        "1_noOfEarCheck": [
                                            {
                                                "condition": "noOfEar is not null and noOfEar = 0",
                                                "decision": {
                                                    "flags": {
                                                        "skip": true,
                                                        "terminateValidation": true,
                                                        "terminateUseCase": true
                                                    },
                                                    "messages": [
                                                        {
                                                            "type": "INFO",
                                                            "message": "No records were created because the number of ears value is 0."
                                                        }
                                                    ]
                                                }
                                            },
                                            {
                                                "default": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "germplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "seedCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "MSD"
                                    },
                                    "sourceEntryDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "entryDbId"
                                    },
                                    "sourcePlotDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "plotDbId"
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 7,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "occurrence_data",
                                "identifier": "occurrenceDataDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "occurrence-data",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createOccurrenceData": [
                                            {
                                                "condition": "harvestedPackagePrefix is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "HARVESTED_PACKAGE_PREFIX"
                                    },
                                    "dataValue": {
                                        "type": "nameBuilder",
                                        "specialType": "HARVESTED_PACKAGE_PREFIX"
                                    }
                                }
                            },
                            {
                                "entity": "package",
                                "identifier": "packageDbId",
                                "retrieveRecord": false,
                                "orderNumber": 9,
                                "action": "create",
                                "apiEndpoint": "packages",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createPackage": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "packageLabel": {
                                                            "type": "nameBuilder"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "packageCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "MPK"
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    "cropCode": "MAIZE",
                    "crossMethodAbbrev": "SELFING",
                    "state": "fixed",
                    "harvestMethodAbbrev": "BULK",
                    "configDescription": "This harvest use case applies to the MAIZE crop, for selfing crosses OR plots, where the germplasm state is fixed. Via the BULK harvest method, it creates ONE new seed, and ONE new package, as well as other related records such as seed relation records. If the germplasm type is 'doubled_haploid', this use case also appends '-B' to the planted germplasm's designation.",
                    "config": {
                        "harvestType": "single",
                        "entities": [
                            {
                                "entity": "germplasm",
                                "identifier": "germplasmDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "update",
                                "apiEndpoint": "germplasm",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "noOfEar",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "noOfEar"
                                        },
                                        {
                                            "referenceName": "germplasmDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "germplasmDbId"
                                        },
                                        {
                                            "referenceName": "germplasmType",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "type"
                                        },
                                        {
                                            "type": "apiRetrieval",
                                            "subType": "multiValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "DH_FIRST_INCREASE_COMPLETED"
                                                }
                                            },
                                            "apiResultFieldNames": [
                                                {
                                                    "apiResultFieldName": "dataValue",
                                                    "referenceName": "dhFirstIncreaseCompleted"
                                                },
                                                {
                                                    "apiResultFieldName": "germplasmAttributeDbId",
                                                    "referenceName": "dhficAttributeDbId"
                                                }
                                            ]
                                        }
                                    ],
                                    "decisions": {
                                        "1_noOfEarCheck": [
                                            {
                                                "condition": "noOfEar is not null and noOfEar = 0",
                                                "decision": {
                                                    "flags": {
                                                        "skip": true,
                                                        "terminateValidation": true,
                                                        "terminateUseCase": true
                                                    },
                                                    "messages": [
                                                        {
                                                            "type": "INFO",
                                                            "message": "No records were created because the number of ears value is 0."
                                                        }
                                                    ]
                                                }
                                            }
                                        ],
                                        "2_updateGermplasm": [
                                            {
                                                "condition": "germplasmType is not null and germplasmType = 'doubled_haploid' and dhFirstIncreaseCompleted is false",
                                                "decision": {
                                                    "flags": {
                                                        "update": true
                                                    },
                                                    "options": {
                                                        "identifier": "germplasmDbId"
                                                    },
                                                    "data": {
                                                        "designation": {
                                                            "type": "nameBuilder"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {}
                            },
                            {
                                "entity": "germplasm_name",
                                "identifier": "germplasmNameDbId",
                                "retrieveRecord": false,
                                "orderNumber": 1,
                                "action": "update",
                                "apiEndpoint": "germplasm-names",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmUpdated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "update"
                                        },
                                        {
                                            "referenceName": "currentStandardNameDbId",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm-names-search",
                                            "apiParams": {
                                                "germplasmDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                },
                                                "germplasmNameStatus": {
                                                    "type": "plainText",
                                                    "textValue": "standard"
                                                }
                                            },
                                            "apiResultFieldName": "germplasmNameDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_updateGermplasmName": [
                                            {
                                                "condition": "germplasmUpdated is true and currentStandardNameDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "update": true
                                                    },
                                                    "options": {
                                                        "identifier": "currentStandardNameDbId"
                                                    },
                                                    "data": {
                                                        "germplasmNameStatus": {
                                                            "type": "plainText",
                                                            "textValue": "active"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_name",
                                "identifier": "germplasmNameDbId",
                                "retrieveRecord": false,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "germplasm-names",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmUpdated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "update"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmName": [
                                            {
                                                "condition": "germplasmUpdated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "nameValue": {
                                                            "type": "recordInfo",
                                                            "subType": "updated",
                                                            "fieldName": "designation"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "germplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "germplasmNameType": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_NAME_TYPE",
                                        "scaleValueAbbrev": "PEDIGREE"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "update",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmUpdated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "update"
                                        },
                                        {
                                            "referenceName": "dhficAttributeDbId",
                                            "type": "recordInfo",
                                            "subType": "retrieved",
                                            "fieldName": "dhficAttributeDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_updateGermplasmAttribute": [
                                            {
                                                "condition": "germplasmUpdated is true",
                                                "decision": {
                                                    "flags": {
                                                        "update": true
                                                    },
                                                    "options": {
                                                        "identifier": "dhficAttributeDbId"
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "plainValue",
                                                            "value": "true"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {}
                            },
                            {
                                "entity": "cross",
                                "identifier": "crossDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "crosses",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "existingCrossDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "crossDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCross": [
                                            {
                                                "condition": "existingCrossDbId is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "existingCrossDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "existingCrossDbId"
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "crossMethod": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "CROSS_METHOD",
                                        "scaleValueAbbrev": "SELFING"
                                    }
                                }
                            },
                            {
                                "entity": "cross_parent",
                                "identifier": "crossParentDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "cross-parents",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "crossCreated",
                                            "type": "previousDecision",
                                            "entity": "cross",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCrossParent": [
                                            {
                                                "condition": "crossCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentRole": {
                                        "type": "plainText",
                                        "textValue": "female-and-male"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1" 
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed",
                                "identifier": "seedDbId",
                                "retrieveRecord": true,
                                "orderNumber": 6,
                                "action": "create",
                                "apiEndpoint": "seeds",
                                "data": {
                                    "germplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "seedCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "MSD"
                                    },
                                    "sourceEntryDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "entryDbId"
                                    },
                                    "sourcePlotDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "plotDbId"
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 7,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "occurrence_data",
                                "identifier": "occurrenceDataDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "occurrence-data",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createOccurrenceData": [
                                            {
                                                "condition": "harvestedPackagePrefix is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "HARVESTED_PACKAGE_PREFIX"
                                    },
                                    "dataValue": {
                                        "type": "nameBuilder",
                                        "specialType": "HARVESTED_PACKAGE_PREFIX"
                                    }
                                }
                            },
                            {
                                "entity": "package",
                                "identifier": "packageDbId",
                                "retrieveRecord": false,
                                "orderNumber": 9,
                                "action": "create",
                                "apiEndpoint": "packages",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createPackage": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "packageLabel": {
                                                            "type": "nameBuilder"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "packageCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "MPK"
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    "cropCode": "MAIZE",
                    "crossMethodAbbrev": "SELFING",
                    "state": "fixed",
                    "harvestMethodAbbrev": "INDIVIDUAL_EAR",
                    "configDescription": "This harvest use case applies to the MAIZE crop, for selfing crosses OR plots, where the germplasm state is fixed. Via the INDIVIDUAL_EAR harvest method, it creates N new seed and package records (N = number of ears), as well as other related records such as seed relation records. If the germplasm type is 'doubled_haploid', this use case also appends '-B' to the planted germplasm's designation.",
                    "config": {
                        "harvestType": "multiple",
                        "numericVariable": {
                            "variableName": "noOfEar",
                            "variableType": "integer"
                        },
                        "entities": [
                            {
                                "entity": "germplasm",
                                "identifier": "germplasmDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "update",
                                "apiEndpoint": "germplasm",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "noOfEar",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "noOfEar"
                                        },
                                        {
                                            "referenceName": "germplasmDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "germplasmDbId"
                                        },
                                        {
                                            "referenceName": "germplasmType",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "type"
                                        },
                                        {
                                            "type": "apiRetrieval",
                                            "subType": "multiValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "DH_FIRST_INCREASE_COMPLETED"
                                                }
                                            },
                                            "apiResultFieldNames": [
                                                {
                                                    "apiResultFieldName": "dataValue",
                                                    "referenceName": "dhFirstIncreaseCompleted"
                                                },
                                                {
                                                    "apiResultFieldName": "germplasmAttributeDbId",
                                                    "referenceName": "dhficAttributeDbId"
                                                }
                                            ]
                                        }
                                    ],
                                    "decisions": {
                                        "1_noOfEarCheck": [
                                            {
                                                "condition": "noOfEar is not null and noOfEar = 0",
                                                "decision": {
                                                    "flags": {
                                                        "skip": true,
                                                        "terminateValidation": true,
                                                        "terminateUseCase": true
                                                    },
                                                    "messages": [
                                                        {
                                                            "type": "INFO",
                                                            "message": "No records were created because the number of ears value is 0."
                                                        }
                                                    ]
                                                }
                                            }
                                        ],
                                        "2_updateGermplasm": [
                                            {
                                                "condition": "germplasmType is not null and germplasmType = 'doubled_haploid' and dhFirstIncreaseCompleted is false",
                                                "decision": {
                                                    "flags": {
                                                        "update": true
                                                    },
                                                    "options": {
                                                        "identifier": "germplasmDbId"
                                                    },
                                                    "data": {
                                                        "designation": {
                                                            "type": "nameBuilder"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {}
                            },
                            {
                                "entity": "germplasm_name",
                                "identifier": "germplasmNameDbId",
                                "retrieveRecord": false,
                                "orderNumber": 1,
                                "action": "update",
                                "apiEndpoint": "germplasm-names",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmUpdated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "update"
                                        },
                                        {
                                            "referenceName": "currentStandardNameDbId",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm-names-search",
                                            "apiParams": {
                                                "germplasmDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                },
                                                "germplasmNameStatus": {
                                                    "type": "plainText",
                                                    "textValue": "standard"
                                                }
                                            },
                                            "apiResultFieldName": "germplasmNameDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_updateGermplasmName": [
                                            {
                                                "condition": "germplasmUpdated is true and currentStandardNameDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "update": true
                                                    },
                                                    "options": {
                                                        "identifier": "currentStandardNameDbId"
                                                    },
                                                    "data": {
                                                        "germplasmNameStatus": {
                                                            "type": "plainText",
                                                            "textValue": "active"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_name",
                                "identifier": "germplasmNameDbId",
                                "retrieveRecord": false,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "germplasm-names",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmUpdated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "update"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmName": [
                                            {
                                                "condition": "germplasmUpdated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "nameValue": {
                                                            "type": "recordInfo",
                                                            "subType": "updated",
                                                            "fieldName": "designation"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "germplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "germplasmNameType": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_NAME_TYPE",
                                        "scaleValueAbbrev": "PEDIGREE"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "update",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmUpdated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "update"
                                        },
                                        {
                                            "referenceName": "dhficAttributeDbId",
                                            "type": "recordInfo",
                                            "subType": "retrieved",
                                            "fieldName": "dhficAttributeDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_updateGermplasmAttribute": [
                                            {
                                                "condition": "germplasmUpdated is true",
                                                "decision": {
                                                    "flags": {
                                                        "update": true
                                                    },
                                                    "options": {
                                                        "identifier": "dhficAttributeDbId"
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "plainValue",
                                                            "value": "true"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {}
                            },
                            {
                                "entity": "cross",
                                "identifier": "crossDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "crosses",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "existingCrossDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "crossDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCross": [
                                            {
                                                "condition": "existingCrossDbId is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "existingCrossDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "existingCrossDbId"
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "crossMethod": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "CROSS_METHOD",
                                        "scaleValueAbbrev": "SELFING"
                                    }
                                }
                            },
                            {
                                "entity": "cross_parent",
                                "identifier": "crossParentDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "cross-parents",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "crossCreated",
                                            "type": "previousDecision",
                                            "entity": "cross",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCrossParent": [
                                            {
                                                "condition": "crossCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentRole": {
                                        "type": "plainText",
                                        "textValue": "female-and-male"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1" 
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed",
                                "identifier": "seedDbId",
                                "retrieveRecord": true,
                                "orderNumber": 6,
                                "action": "create",
                                "apiEndpoint": "seeds",
                                "data": {
                                    "germplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "seedCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "MSD"
                                    },
                                    "sourceEntryDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "entryDbId"
                                    },
                                    "sourcePlotDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "plotDbId"
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 7,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "occurrence_data",
                                "identifier": "occurrenceDataDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "occurrence-data",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createOccurrenceData": [
                                            {
                                                "condition": "harvestedPackagePrefix is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "HARVESTED_PACKAGE_PREFIX"
                                    },
                                    "dataValue": {
                                        "type": "nameBuilder",
                                        "specialType": "HARVESTED_PACKAGE_PREFIX"
                                    }
                                }
                            },
                            {
                                "entity": "package",
                                "identifier": "packageDbId",
                                "retrieveRecord": false,
                                "orderNumber": 9,
                                "action": "create",
                                "apiEndpoint": "packages",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createPackage": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "packageLabel": {
                                                            "type": "nameBuilder"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "packageCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "MPK"
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    "cropCode": "WHEAT",
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "harvestMethodAbbrev": [
                        "BULK",
                        "SELECTED_BULK"
                    ],
                    "configDescription": "This harvest use case applies to the WHEAT crop, for selfing crosses OR plots, where the germplasm state is not fixed. Via the BULK or SELECTED_BULK harvest method, it creates ONE new seed, and ONE new package, as well as other related records such as seed relation records.",
                    "config": {
                        "harvestType": "single",
                        "entities": [
                            {
                                "entity": "germplasm",
                                "identifier": "germplasmDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "germplasm",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "existingGermplasmDbId",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm-names-search",
                                            "apiParams": {
                                                "nameValue": {
                                                    "type": "recordInfo",
                                                    "subType": "buildData",
                                                    "fieldName": "designation"
                                                }
                                            },
                                            "apiResultFieldName": "germplasmDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasm": [
                                            {
                                                "condition": "existingGermplasmDbId is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "existingGermplasmDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "existingGermplasmDbId"
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "germplasmCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "WGE"
                                    },
                                    "generation": {
                                        "type": "generationAdvancement",
                                        "base": "F"
                                    },
                                    "state" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmState"
                                    },
                                    "germplasmType": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_TYPE",
                                        "scaleValueAbbrev": "SEGREGATING"
                                    },
                                    "nameType": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_NAME_TYPE",
                                        "scaleValueAbbrev": "SELECTION_HISTORY"
                                    },
                                    "parentage" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmParentage"
                                    },
                                    "taxonomyDbId" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmTaxonomyId"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_name",
                                "identifier": "germplasmNameDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "germplasm-names",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmName": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "nameValue": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "designation"
                                    },
                                    "germplasmNameType": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "germplasmNameType"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_relation",
                                "identifier": "germplasmRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 3,
                                "action": "create",
                                "apiEndpoint": "germplasm-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmRelation": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentGermplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "growthHabit",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "GROWTH_HABIT"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and growthHabit is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "growthHabit"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "GROWTH_HABIT"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "crossNumber",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "CROSS_NUMBER"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and crossNumber is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "crossNumber"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "CROSS_NUMBER"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "grainColor",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "grainColor"
                                        },
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and grainColor is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "apiRetrieval",
                                                            "subType": "singleValue",
                                                            "apiMethod": "POST",
                                                            "apiEndpoint": "scale-values-search",
                                                            "apiParams": {
                                                                "variableAbbrev": {
                                                                    "type": "plainText",
                                                                    "textValue": "equals GRAIN_COLOR"
                                                                },
                                                                "scaleValue": {
                                                                    "type": "recordInfo",
                                                                    "subType": "planted",
                                                                    "fieldName": "grainColor"
                                                                }
                                                            },
                                                            "apiResultFieldName": "displayName",
                                                            "referenceName": "grainColorDisplayName"
                                                        },
                                                        "bypassScaleCheck": {
                                                            "type": "plainText",
                                                            "textValue": "true"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "GRAIN_COLOR"
                                    }
                                }
                            },
                            {
                                "entity": "family_member",
                                "identifier": "familyMemberDbId",
                                "retrieveRecord": false,
                                "orderNumber": 5,
                                "action": "create",
                                "apiEndpoint": "family-members",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "familyDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "familyDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createFamilyMember": [
                                            {
                                                "condition": "germplasmCreated is true and familyDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "familyDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "familyDbId"
                                    },
                                    "familyCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "WFM"
                                    }
                                }
                            },
                            {
                                "entity": "cross",
                                "identifier": "crossDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "crosses",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "existingCrossDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "crossDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCross": [
                                            {
                                                "condition": "existingCrossDbId is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "existingCrossDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "existingCrossDbId"
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "crossMethod": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "CROSS_METHOD",
                                        "scaleValueAbbrev": "SELFING"
                                    }
                                }
                            },
                            {
                                "entity": "cross_parent",
                                "identifier": "crossParentDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "cross-parents",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "crossCreated",
                                            "type": "previousDecision",
                                            "entity": "cross",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCrossParent": [
                                            {
                                                "condition": "crossCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentRole": {
                                        "type": "plainText",
                                        "textValue": "female-and-male"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1" 
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed",
                                "identifier": "seedDbId",
                                "retrieveRecord": true,
                                "orderNumber": 6,
                                "action": "create",
                                "apiEndpoint": "seeds",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "existingGermplasmUsed",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "useExisting"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeed": [
                                            {
                                                "condition": "germplasmCreated is true or existingGermplasmUsed is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "sourceEntryDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "entryDbId"
                                    },
                                    "sourcePlotDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "plotDbId"
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    },
                                    "seedCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "WSD"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 7,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "occurrence_data",
                                "identifier": "occurrenceDataDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "occurrence-data",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createOccurrenceData": [
                                            {
                                                "condition": "harvestedPackagePrefix is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "HARVESTED_PACKAGE_PREFIX"
                                    },
                                    "dataValue": {
                                        "type": "nameBuilder",
                                        "specialType": "HARVESTED_PACKAGE_PREFIX"
                                    }
                                }
                            },
                            {
                                "entity": "package",
                                "identifier": "packageDbId",
                                "retrieveRecord": false,
                                "orderNumber": 9,
                                "action": "create",
                                "apiEndpoint": "packages",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createPackage": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "packageLabel": {
                                                            "type": "nameBuilder"
                                                        }
                                                    }
                                                }   
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "packageCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "WPK"
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    "cropCode": "WHEAT",
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "harvestMethodAbbrev": [
                        "SINGLE_PLANT_SELECTION",
                        "INDIVIDUAL_SPIKE"
                    ],
                    "configDescription": "This harvest use case applies to the WHEAT crop, for selfing crosses OR plots, where the germplasm state is not_fixed. Via the SINGLE PLANT SELECTION or INDIVIDUAL SPIKE harvest method, it creates N germplasm records, where N is equal to the number of ears input. For each germplasm created, it also creates ONE new seed and ONE new package, as well as other related records such as germplasm names, attributes, and relations records.",
                    "config": {
                        "harvestType": "multiple",
                        "numericVariable": {
                            "variableName": "noOfPlant",
                            "variableType": "integer"
                        },
                        "entities": [
                            {
                                "entity": "germplasm",
                                "identifier": "germplasmDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "germplasm",
                                "data": {
                                    "germplasmCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "WGE"
                                    },
                                    "generation": {
                                        "type": "generationAdvancement",
                                        "base": "F"
                                    },
                                    "state" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmState"
                                    },
                                    "germplasmType": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_TYPE",
                                        "scaleValueAbbrev": "SEGREGATING"
                                    },
                                    "nameType": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_NAME_TYPE",
                                        "scaleValueAbbrev": "SELECTION_HISTORY"
                                    },
                                    "parentage" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmParentage"
                                    },
                                    "taxonomyDbId" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmTaxonomyId"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_name",
                                "identifier": "germplasmNameDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "germplasm-names",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmName": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "nameValue": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "designation"
                                    },
                                    "germplasmNameType": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "germplasmNameType"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_relation",
                                "identifier": "germplasmRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 3,
                                "action": "create",
                                "apiEndpoint": "germplasm-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmRelation": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentGermplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "growthHabit",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "GROWTH_HABIT"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and growthHabit is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "growthHabit"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "GROWTH_HABIT"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "crossNumber",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "CROSS_NUMBER"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and crossNumber is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "crossNumber"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "CROSS_NUMBER"
                                    }
                                }
                            },
                            {
                                "entity": "family_member",
                                "identifier": "familyMemberDbId",
                                "retrieveRecord": false,
                                "orderNumber": 5,
                                "action": "create",
                                "apiEndpoint": "family-members",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "familyDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "familyDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createFamilyMember": [
                                            {
                                                "condition": "germplasmCreated is true and familyDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "familyDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "familyDbId"
                                    },
                                    "familyCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "WFM"
                                    }
                                }
                            },
                            {
                                "entity": "cross",
                                "identifier": "crossDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "crosses",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "existingCrossDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "crossDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCross": [
                                            {
                                                "condition": "existingCrossDbId is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "existingCrossDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "existingCrossDbId"
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "crossMethod": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "CROSS_METHOD",
                                        "scaleValueAbbrev": "SELFING"
                                    }
                                }
                            },
                            {
                                "entity": "cross_parent",
                                "identifier": "crossParentDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "cross-parents",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "crossCreated",
                                            "type": "previousDecision",
                                            "entity": "cross",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCrossParent": [
                                            {
                                                "condition": "crossCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentRole": {
                                        "type": "plainText",
                                        "textValue": "female-and-male"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1" 
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed",
                                "identifier": "seedDbId",
                                "retrieveRecord": true,
                                "orderNumber": 6,
                                "action": "create",
                                "apiEndpoint": "seeds",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "existingGermplasmUsed",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "useExisting"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeed": [
                                            {
                                                "condition": "germplasmCreated is true or existingGermplasmUsed is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "sourceEntryDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "entryDbId"
                                    },
                                    "sourcePlotDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "plotDbId"
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    },
                                    "seedCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "WSD"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 7,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "occurrence_data",
                                "identifier": "occurrenceDataDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "occurrence-data",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createOccurrenceData": [
                                            {
                                                "condition": "harvestedPackagePrefix is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "HARVESTED_PACKAGE_PREFIX"
                                    },
                                    "dataValue": {
                                        "type": "nameBuilder",
                                        "specialType": "HARVESTED_PACKAGE_PREFIX"
                                    }
                                }
                            },
                            {
                                "entity": "package",
                                "identifier": "packageDbId",
                                "retrieveRecord": false,
                                "orderNumber": 9,
                                "action": "create",
                                "apiEndpoint": "packages",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createPackage": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "packageLabel": {
                                                            "type": "nameBuilder"
                                                        }
                                                    }
                                                }   
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "packageCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "WPK"
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    "cropCode": "WHEAT",
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "harvestMethodAbbrev": "SINGLE_PLANT_SELECTION_AND_BULK",
                    "configDescription": "This harvest use case applies to the WHEAT crop, for selfing crosses OR plots, where the germplasm state is not_fixed. Via the SINGLE PLANT SELECTION AND BULK harvest method, it creates N germplasm records, where N is equal to the number of ears input. For each germplasm created, it also creates ONE new seed and ONE new package, as well as other related records such as germplasm names, attributes, and relations records.",
                    "config": {
                        "harvestType": "combination",
                        "harvestMethodAbbrevs": [
                            "SINGLE_PLANT_SELECTION",
                            "BULK"
                        ]
                    }
                },
                {
                    "cropCode": "WHEAT",
                    "crossMethodAbbrev": "SELFING",
                    "state": "fixed",
                    "harvestMethodAbbrev": [
                        "BULK",
                        "SELECTED_BULK"
                    ],
                    "configDescription": "This harvest use case applies to the WHEAT crop, for selfing crosses OR plots, where the germplasm state is fixed. Via the BULK or SELECTED BULK harvest method, it creates ONE seed record and ONE package record. It also creates a seed relation record, linking the planted seed record to the new seed record.",
                    "config": {
                        "harvestType": "single",
                        "entities": [
                            {
                                "entity": "cross",
                                "identifier": "crossDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "crosses",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "existingCrossDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "crossDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCross": [
                                            {
                                                "condition": "existingCrossDbId is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "existingCrossDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "existingCrossDbId"
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "crossMethod": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "CROSS_METHOD",
                                        "scaleValueAbbrev": "SELFING"
                                    }
                                }
                            },
                            {
                                "entity": "cross_parent",
                                "identifier": "crossParentDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "cross-parents",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "crossCreated",
                                            "type": "previousDecision",
                                            "entity": "cross",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCrossParent": [
                                            {
                                                "condition": "crossCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentRole": {
                                        "type": "plainText",
                                        "textValue": "female-and-male"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1" 
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed",
                                "identifier": "seedDbId",
                                "retrieveRecord": true,
                                "orderNumber": 3,
                                "action": "create",
                                "apiEndpoint": "seeds",
                                "data": {
                                    "germplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "sourceEntryDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "entryDbId"
                                    },
                                    "sourcePlotDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "plotDbId"
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    },
                                    "seedCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "WSD"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "occurrence_data",
                                "identifier": "occurrenceDataDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "occurrence-data",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createOccurrenceData": [
                                            {
                                                "condition": "harvestedPackagePrefix is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "HARVESTED_PACKAGE_PREFIX"
                                    },
                                    "dataValue": {
                                        "type": "nameBuilder",
                                        "specialType": "HARVESTED_PACKAGE_PREFIX"
                                    }
                                }
                            },
                            {
                                "entity": "package",
                                "identifier": "packageDbId",
                                "retrieveRecord": false,
                                "orderNumber": 9,
                                "action": "create",
                                "apiEndpoint": "packages",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createPackage": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "packageLabel": {
                                                            "type": "nameBuilder"
                                                        }
                                                    }
                                                }   
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "packageCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "WPK"
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    "cropCode": "WHEAT",
                    "crossMethodAbbrev": "SELFING",
                    "state": "fixed",
                    "harvestMethodAbbrev": [
                        "SINGLE_PLANT_SELECTION",
                        "INDIVIDUAL_SPIKE"
                    ],
                    "configDescription": "This harvest use case applies to the WHEAT crop, for selfing crosses OR plots, where the germplasm state is fixed. Via the BULK or SELECTED BULK harvest method, it creates ONE seed record and ONE package record. It also creates a seed relation record, linking the planted seed record to the new seed record.",
                    "config": {
                        "harvestType": "multiple",
                        "numericVariable": {
                            "variableName": "noOfPlant",
                            "variableType": "integer"
                        },
                        "entities": [
                            {
                                "entity": "cross",
                                "identifier": "crossDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "crosses",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "existingCrossDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "crossDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCross": [
                                            {
                                                "condition": "existingCrossDbId is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "existingCrossDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "existingCrossDbId"
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "crossMethod": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "CROSS_METHOD",
                                        "scaleValueAbbrev": "SELFING"
                                    }
                                }
                            },
                            {
                                "entity": "cross_parent",
                                "identifier": "crossParentDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "cross-parents",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "crossCreated",
                                            "type": "previousDecision",
                                            "entity": "cross",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createCrossParent": [
                                            {
                                                "condition": "crossCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentRole": {
                                        "type": "plainText",
                                        "textValue": "female-and-male"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1" 
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed",
                                "identifier": "seedDbId",
                                "retrieveRecord": true,
                                "orderNumber": 3,
                                "action": "create",
                                "apiEndpoint": "seeds",
                                "data": {
                                    "germplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "sourceEntryDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "entryDbId"
                                    },
                                    "sourcePlotDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "plotDbId"
                                    },
                                    "crossDbId": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "crossDbId"
                                    },
                                    "seedCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "WSD"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "occurrence_data",
                                "identifier": "occurrenceDataDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "occurrence-data",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createOccurrenceData": [
                                            {
                                                "condition": "harvestedPackagePrefix is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "HARVESTED_PACKAGE_PREFIX"
                                    },
                                    "dataValue": {
                                        "type": "nameBuilder",
                                        "specialType": "HARVESTED_PACKAGE_PREFIX"
                                    }
                                }
                            },
                            {
                                "entity": "package",
                                "identifier": "packageDbId",
                                "retrieveRecord": false,
                                "orderNumber": 9,
                                "action": "create",
                                "apiEndpoint": "packages",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createPackage": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "packageLabel": {
                                                            "type": "nameBuilder"
                                                        }
                                                    }
                                                }   
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "packageCodePrefix": {
                                        "type": "plainText",
                                        "textValue": "WPK"
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    "cropCode": [
                        "COWPEA", "SOYBEAN"
                    ],
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "generation": [
                        "contains F1", "contains F3", "contains F4", "contains F5", "contains F6"
                    ],
                    "harvestMethodAbbrev": "BULK",
                    "configDescription": "This harvest use case applies to the COWPEA and SOYBEAN crops, for selfing crosses OR plots, where the germplasm state is not_fixed and the germplasm generation contains F1, F3, F4, F5, or F6 (eg. F1, F3, F4, F5, F6, BC1F1, BC1F3, BC3F5, etc.). Via the BULK harvest method, it creates ONE new germplasm record, with its own seed and package record, as well as other related records like names, attributes, and relation records.",
                    "config": {
                        "harvestType": "single",
                        "entities": [
                            {
                                "entity": "germplasm",
                                "identifier": "germplasmDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "germplasm",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "existingGermplasmDbId",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm-names-search",
                                            "apiParams": {
                                                "nameValue": {
                                                    "type": "recordInfo",
                                                    "subType": "buildData",
                                                    "fieldName": "designation"
                                                }
                                            },
                                            "apiResultFieldName": "germplasmDbId"
                                        },
                                        {
                                            "referenceName": "germplasmGeneration",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "generation"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasm": [
                                            {
                                                "condition": "existingGermplasmDbId is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "existingGermplasmDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "existingGermplasmDbId"
                                                    }
                                                }
                                            }
                                        ],
                                        "2_germplasmGenerationLogic": [
                                            {
                                                "condition": "germplasmGeneration contains F1",
                                                "decision": {
                                                    "data": {
                                                        "state": {
                                                            "type": "scaleValue",
                                                            "variableAbbrev": "GERMPLASM_STATE",
                                                            "scaleValueAbbrev": "NOT_FIXED"
                                                        },
                                                        "germplasmType": {
                                                            "type": "scaleValue",
                                                            "variableAbbrev": "GERMPLASM_TYPE",
                                                            "scaleValueAbbrev": "SEGREGATING"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "default": {
                                                    "data": {
                                                        "state": {
                                                            "type": "scaleValue",
                                                            "variableAbbrev": "GERMPLASM_STATE",
                                                            "scaleValueAbbrev": "FIXED"
                                                        },
                                                        "germplasmType": {
                                                            "type": "scaleValue",
                                                            "variableAbbrev": "GERMPLASM_TYPE",
                                                            "scaleValueAbbrev": "INBRED_LINE"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "generation": {
                                        "type": "generationAdvancement",
                                        "base": "F"
                                    },
                                    "nameType": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_NAME_TYPE",
                                        "scaleValueAbbrev": "CROSS_CODE"
                                    },
                                    "parentage" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmParentage"
                                    },
                                    "taxonomyDbId" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmTaxonomyId"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_name",
                                "identifier": "germplasmNameDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "germplasm-names",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmName": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "nameValue": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "designation"
                                    },
                                    "germplasmNameType": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "germplasmNameType"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_relation",
                                "identifier": "germplasmRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 3,
                                "action": "create",
                                "apiEndpoint": "germplasm-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmRelation": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentGermplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "family_member",
                                "identifier": "familyMemberDbId",
                                "retrieveRecord": false,
                                "orderNumber": 5,
                                "action": "create",
                                "apiEndpoint": "family-members",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "familyDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "familyDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createFamilyMember": [
                                            {
                                                "condition": "germplasmCreated is true and familyDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "familyDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "familyDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed",
                                "identifier": "seedDbId",
                                "retrieveRecord": true,
                                "orderNumber": 6,
                                "action": "create",
                                "apiEndpoint": "seeds",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "existingGermplasmUsed",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "useExisting"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeed": [
                                            {
                                                "condition": "germplasmCreated is true or existingGermplasmUsed is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "sourceEntryDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "entryDbId"
                                    },
                                    "sourcePlotDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "plotDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 7,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "occurrence_data",
                                "identifier": "occurrenceDataDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "occurrence-data",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createOccurrenceData": [
                                            {
                                                "condition": "harvestedPackagePrefix is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "HARVESTED_PACKAGE_PREFIX"
                                    },
                                    "dataValue": {
                                        "type": "nameBuilder",
                                        "specialType": "HARVESTED_PACKAGE_PREFIX"
                                    }
                                }
                            },
                            {
                                "entity": "package",
                                "identifier": "packageDbId",
                                "retrieveRecord": false,
                                "orderNumber": 9,
                                "action": "create",
                                "apiEndpoint": "packages",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createPackage": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "packageLabel": {
                                                            "type": "nameBuilder"
                                                        }
                                                    }
                                                }   
                                            }
                                        ]
                                    }
                                },
                                "data": {}
                            }
                        ]
                    }
                },
                {
                    "cropCode": [
                        "COWPEA", "SOYBEAN"
                    ],
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "generation": [
                        "contains F2", "contains F3"
                    ],
                    "harvestMethodAbbrev": "SINGLE_PLANT",
                    "configDescription": "This harvest use case applies to the COWPEA and SOYBEAN crops, for selfing crosses OR plots, where the germplasm state is not_fixed and the germplasm generation contains F2 or F3 (eg. F2, F3, BC2F2, BC3F3, etc.). Via the SINGLE PLANT harvest method, it creates N new germplasm records (N = number of plants), each with its own seed and package record, as well as other related records like names, attributes, and relation records.",
                    "config": {
                        "harvestType": "multiple",
                        "numericVariable": {
                            "variableName": "noOfPlant",
                            "variableType": "integer"
                        },
                        "entities": [
                            {
                                "entity": "germplasm",
                                "identifier": "germplasmDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "germplasm",
                                "data": {
                                    "generation": {
                                        "type": "generationAdvancement",
                                        "base": "F"
                                    },
                                    "state": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_STATE",
                                        "scaleValueAbbrev": "NOT_FIXED"
                                    },
                                    "germplasmType": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_TYPE",
                                        "scaleValueAbbrev": "SEGREGATING"
                                    },
                                    "nameType": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_NAME_TYPE",
                                        "scaleValueAbbrev": "CROSS_CODE"
                                    },
                                    "parentage" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmParentage"
                                    },
                                    "taxonomyDbId" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmTaxonomyId"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_name",
                                "identifier": "germplasmNameDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "germplasm-names",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmName": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "nameValue": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "designation"
                                    },
                                    "germplasmNameType": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "germplasmNameType"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_relation",
                                "identifier": "germplasmRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 3,
                                "action": "create",
                                "apiEndpoint": "germplasm-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmRelation": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentGermplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "family_member",
                                "identifier": "familyMemberDbId",
                                "retrieveRecord": false,
                                "orderNumber": 5,
                                "action": "create",
                                "apiEndpoint": "family-members",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "familyDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "familyDbId"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createFamilyMember": [
                                            {
                                                "condition": "germplasmCreated is true and familyDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "familyDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "familyDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed",
                                "identifier": "seedDbId",
                                "retrieveRecord": true,
                                "orderNumber": 6,
                                "action": "create",
                                "apiEndpoint": "seeds",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "existingGermplasmUsed",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "useExisting"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeed": [
                                            {
                                                "condition": "germplasmCreated is true or existingGermplasmUsed is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "sourceEntryDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "entryDbId"
                                    },
                                    "sourcePlotDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "plotDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 7,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "occurrence_data",
                                "identifier": "occurrenceDataDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "occurrence-data",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createOccurrenceData": [
                                            {
                                                "condition": "harvestedPackagePrefix is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "HARVESTED_PACKAGE_PREFIX"
                                    },
                                    "dataValue": {
                                        "type": "nameBuilder",
                                        "specialType": "HARVESTED_PACKAGE_PREFIX"
                                    }
                                }
                            },
                            {
                                "entity": "package",
                                "identifier": "packageDbId",
                                "retrieveRecord": false,
                                "orderNumber": 9,
                                "action": "create",
                                "apiEndpoint": "packages",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createPackage": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "packageLabel": {
                                                            "type": "nameBuilder"
                                                        }
                                                    }
                                                }   
                                            }
                                        ]
                                    }
                                },
                                "data": {}
                            }
                        ]
                    }
                },
                {
                    "cropCode": [
                        "COWPEA", "SOYBEAN"
                    ],
                    "crossMethodAbbrev": "SELFING",
                    "state": "fixed",
                    "harvestMethodAbbrev": "BULK",
                    "configDescription": "This harvest use case applies to the COWPEA and SOYBEAN crops, for selfing crosses OR plots, where the germplasm state is fixed. Via the BULK harvest method, it creates ONE seed record and ONE package record, linking it to the planted germplasm record.",
                    "config": {
                        "harvestType": "single",
                        "entities": [
                            {
                                "entity": "seed",
                                "identifier": "seedDbId",
                                "retrieveRecord": true,
                                "orderNumber": 6,
                                "action": "create",
                                "apiEndpoint": "seeds",
                                "data": {
                                    "germplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "sourceEntryDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "entryDbId"
                                    },
                                    "sourcePlotDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "plotDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 7,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "occurrence_data",
                                "identifier": "occurrenceDataDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "occurrence-data",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createOccurrenceData": [
                                            {
                                                "condition": "harvestedPackagePrefix is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "HARVESTED_PACKAGE_PREFIX"
                                    },
                                    "dataValue": {
                                        "type": "nameBuilder",
                                        "specialType": "HARVESTED_PACKAGE_PREFIX"
                                    }
                                }
                            },
                            {
                                "entity": "package",
                                "identifier": "packageDbId",
                                "retrieveRecord": false,
                                "orderNumber": 9,
                                "action": "create",
                                "apiEndpoint": "packages",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createPackage": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "packageLabel": {
                                                            "type": "nameBuilder"
                                                        }
                                                    }
                                                }   
                                            }
                                        ]
                                    }
                                },
                                "data": {}
                            }
                        ]
                    }
                },
                {
                    "cropCode": [
                        "CHICKPEA", "GROUNDNUT", "SORGHUM",
                        "FINGERMILLET", "PEARLMILLET", "PIGEONPEA"
                    ],
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "harvestMethodAbbrev": "BULK",
                    "configDescription": "This harvest use case applies to the CHICKPEA, GROUNDNUT, SORGHUM, FINGERMILLET, PEARLMILLET, and PIGEONPEA crops, for selfing crosses OR plots, where the germplasm state is not_fixed. Via the BULK harvest method, it creates ONE germplasm record, with its own seed and package record, as well as other related records like names, attributes, and relation records.",
                    "config": {
                        "harvestType": "single",
                        "entities": [
                            {
                                "entity": "germplasm",
                                "identifier": "germplasmDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "germplasm",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "noOfPlant",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "noOfPlant"
                                        },
                                        {
                                            "referenceName": "existingGermplasmDbId",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm-names-search",
                                            "apiParams": {
                                                "nameValue": {
                                                    "type": "recordInfo",
                                                    "subType": "buildData",
                                                    "fieldName": "designation"
                                                }
                                            },
                                            "apiResultFieldName": "germplasmDbId"
                                        },
                                        {
                                            "referenceName": "cropCode",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "cropCode"
                                        }
                                    ],
                                    "decisions": {
                                        "1_noOfPlantCheck": [
                                            {
                                                "condition": "noOfPlant = 0",
                                                "decision": {
                                                    "flags": {
                                                        "skip": true,
                                                        "terminateValidation": true,
                                                        "terminateUseCase": true
                                                    },
                                                    "messages": [
                                                        {
                                                            "type": "INFO",
                                                            "message": "No records were created because the number of plants value is 0."
                                                        }
                                                    ]
                                                }
                                            }
                                        ],
                                        "2_createGermplasm": [
                                            {
                                                "condition": "existingGermplasmDbId is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "existingGermplasmDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "useExisting": true,
                                                        "terminateValidation": true
                                                    },
                                                    "options": {
                                                        "identifier": "existingGermplasmDbId"
                                                    }
                                                }
                                            }
                                        ],
                                        "3_germplasmCodeLogic": [
                                            {
                                                "condition": "cropCode = 'CHICKPEA'",
                                                "decision": {
                                                    "data": {
                                                        "germplasmCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "CGE"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'GROUNDNUT'",
                                                "decision": {
                                                    "data": {
                                                        "germplasmCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "GGE"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'SORGHUM'",
                                                "decision": {
                                                    "data": {
                                                        "germplasmCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "SGE"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'FINGERMILLET'",
                                                "decision": {
                                                    "data": {
                                                        "germplasmCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "FGE"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'PEARLMILLET'",
                                                "decision": {
                                                    "data": {
                                                        "germplasmCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "MGE"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'PIGEONPEA'",
                                                "decision": {
                                                    "data": {
                                                        "germplasmCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "PGE"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "generation": {
                                        "type": "generationAdvancement",
                                        "base": "F"
                                    },
                                    "state": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_STATE",
                                        "scaleValueAbbrev": "NOT_FIXED"
                                    },
                                    "germplasmType": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_TYPE",
                                        "scaleValueAbbrev": "SEGREGATING"
                                    },
                                    "nameType": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_NAME_TYPE",
                                        "scaleValueAbbrev": "SELECTION_HISTORY"
                                    },
                                    "parentage" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmParentage"
                                    },
                                    "taxonomyDbId" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmTaxonomyId"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_name",
                                "identifier": "germplasmNameDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "germplasm-names",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmName": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "nameValue": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "designation"
                                    },
                                    "germplasmNameType": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "germplasmNameType"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_relation",
                                "identifier": "germplasmRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 3,
                                "action": "create",
                                "apiEndpoint": "germplasm-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmRelation": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentGermplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "heteroticGroup",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HETEROTIC_GROUP"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and heteroticGroup is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "heteroticGroup"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "HETEROTIC_GROUP"
                                    }
                                }
                            },
                            {
                                "entity": "family_member",
                                "identifier": "familyMemberDbId",
                                "retrieveRecord": false,
                                "orderNumber": 5,
                                "action": "create",
                                "apiEndpoint": "family-members",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "familyDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "familyDbId"
                                        },
                                        {
                                            "referenceName": "cropCode",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "cropCode"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createFamilyMember": [
                                            {
                                                "condition": "germplasmCreated is true and familyDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ],
                                        "2_familyCodeLogic": [
                                            {
                                                "condition": "cropCode = 'CHICKPEA'",
                                                "decision": {
                                                    "data": {
                                                        "familyCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "CFM"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'GROUNDNUT'",
                                                "decision": {
                                                    "data": {
                                                        "familyCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "GFM"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'SORGHUM'",
                                                "decision": {
                                                    "data": {
                                                        "familyCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "SFM"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'FINGERMILLET'",
                                                "decision": {
                                                    "data": {
                                                        "familyCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "FFM"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'PEARLMILLET'",
                                                "decision": {
                                                    "data": {
                                                        "familyCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "MFM"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'PIGEONPEA'",
                                                "decision": {
                                                    "data": {
                                                        "familyCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "PFM"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "familyDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "familyDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed",
                                "identifier": "seedDbId",
                                "retrieveRecord": true,
                                "orderNumber": 6,
                                "action": "create",
                                "apiEndpoint": "seeds",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "existingGermplasmUsed",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "useExisting"
                                        },
                                        {
                                            "referenceName": "cropCode",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "cropCode"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeed": [
                                            {
                                                "condition": "germplasmCreated is true or existingGermplasmUsed is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ],
                                        "2_seedCodeLogic": [
                                            {
                                                "condition": "cropCode = 'CHICKPEA'",
                                                "decision": {
                                                    "data": {
                                                        "seedCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "CSD"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'GROUNDNUT'",
                                                "decision": {
                                                    "data": {
                                                        "seedCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "GSD"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'SORGHUM'",
                                                "decision": {
                                                    "data": {
                                                        "seedCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "SSD"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'FINGERMILLET'",
                                                "decision": {
                                                    "data": {
                                                        "seedCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "FSD"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'PEARLMILLET'",
                                                "decision": {
                                                    "data": {
                                                        "seedCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "MSD"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'PIGEONPEA'",
                                                "decision": {
                                                    "data": {
                                                        "seedCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "PSD"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "sourceEntryDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "entryDbId"
                                    },
                                    "sourcePlotDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "plotDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 7,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "occurrence_data",
                                "identifier": "occurrenceDataDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "occurrence-data",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createOccurrenceData": [
                                            {
                                                "condition": "harvestedPackagePrefix is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "HARVESTED_PACKAGE_PREFIX"
                                    },
                                    "dataValue": {
                                        "type": "nameBuilder",
                                        "specialType": "HARVESTED_PACKAGE_PREFIX"
                                    }
                                }
                            },
                            {
                                "entity": "package",
                                "identifier": "packageDbId",
                                "retrieveRecord": false,
                                "orderNumber": 9,
                                "action": "create",
                                "apiEndpoint": "packages",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        },
                                        {
                                            "referenceName": "cropCode",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "cropCode"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createPackage": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "packageLabel": {
                                                            "type": "nameBuilder"
                                                        }
                                                    }
                                                }   
                                            }
                                        ],
                                        "2_packageCodeLogic": [
                                            {
                                                "condition": "cropCode = 'CHICKPEA'",
                                                "decision": {
                                                    "data": {
                                                        "packageCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "CPK"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'GROUNDNUT'",
                                                "decision": {
                                                    "data": {
                                                        "packageCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "GPK"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'SORGHUM'",
                                                "decision": {
                                                    "data": {
                                                        "packageCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "SPK"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'FINGERMILLET'",
                                                "decision": {
                                                    "data": {
                                                        "packageCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "FPK"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'PEARLMILLET'",
                                                "decision": {
                                                    "data": {
                                                        "packageCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "MPK"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'PIGEONPEA'",
                                                "decision": {
                                                    "data": {
                                                        "packageCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "PPK"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {}
                            }
                        ]
                    }
                },
                {
                    "cropCode": [
                        "CHICKPEA", "GROUNDNUT", "SORGHUM",
                        "FINGERMILLET", "PEARLMILLET", "PIGEONPEA"
                    ],
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "harvestMethodAbbrev": "SINGLE_PLANT",
                    "configDescription": "This harvest use case applies to the CHICKPEA, GROUNDNUT, SORGHUM, FINGERMILLET, PEARLMILLET, and PIGEONPEA crops, for selfing crosses OR plots, where the germplasm state is not_fixed. Via the SINGLE PLANT harvest method, it creates n NEW germplasm record (N = number of plants), each with its own seed and package record, as well as other related records like names, attributes, and relation records.",
                    "config": {
                        "harvestType": "multiple",
                        "numericVariable": {
                            "variableName": "noOfPlant",
                            "variableType": "integer"
                        },
                        "entities": [
                            {
                                "entity": "germplasm",
                                "identifier": "germplasmDbId",
                                "retrieveRecord": true,
                                "orderNumber": 1,
                                "action": "create",
                                "apiEndpoint": "germplasm",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "cropCode",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "cropCode"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasm": [
                                            {
                                                "default": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ],
                                        "2_germplasmCodeLogic": [
                                            {
                                                "condition": "cropCode = 'CHICKPEA'",
                                                "decision": {
                                                    "data": {
                                                        "germplasmCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "CGE"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'GROUNDNUT'",
                                                "decision": {
                                                    "data": {
                                                        "germplasmCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "GGE"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'SORGHUM'",
                                                "decision": {
                                                    "data": {
                                                        "germplasmCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "SGE"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'FINGERMILLET'",
                                                "decision": {
                                                    "data": {
                                                        "germplasmCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "FGE"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'PEARLMILLET'",
                                                "decision": {
                                                    "data": {
                                                        "germplasmCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "MGE"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'PIGEONPEA'",
                                                "decision": {
                                                    "data": {
                                                        "germplasmCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "PGE"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "generation": {
                                        "type": "generationAdvancement",
                                        "base": "F"
                                    },
                                    "state": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_STATE",
                                        "scaleValueAbbrev": "NOT_FIXED"
                                    },
                                    "germplasmType": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_TYPE",
                                        "scaleValueAbbrev": "SEGREGATING"
                                    },
                                    "nameType": {
                                        "type": "scaleValue",
                                        "variableAbbrev": "GERMPLASM_NAME_TYPE",
                                        "scaleValueAbbrev": "SELECTION_HISTORY"
                                    },
                                    "parentage" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmParentage"
                                    },
                                    "taxonomyDbId" : {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmTaxonomyId"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_name",
                                "identifier": "germplasmNameDbId",
                                "retrieveRecord": false,
                                "orderNumber": 2,
                                "action": "create",
                                "apiEndpoint": "germplasm-names",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmName": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "nameValue": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "designation"
                                    },
                                    "germplasmNameType": {
                                        "type": "recordInfo",
                                        "subType": "new",
                                        "fieldName": "germplasmNameType"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_relation",
                                "identifier": "germplasmRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 3,
                                "action": "create",
                                "apiEndpoint": "germplasm-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmRelation": [
                                            {
                                                "condition": "germplasmCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentGermplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "germplasm_attribute",
                                "identifier": "germplasmAttributeDbId",
                                "retrieveRecord": false,
                                "orderNumber": 4,
                                "action": "create",
                                "apiEndpoint": "germplasm-attributes",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "heteroticGroup",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "germplasm/:id/attributes-search",
                                            "urlReplacements": {
                                                "id": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "germplasmDbId"
                                                }
                                            },
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HETEROTIC_GROUP"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createGermplasmAttribute": [
                                            {
                                                "condition": "germplasmCreated is true and heteroticGroup is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "dataValue": {
                                                            "type": "recordInfo",
                                                            "subType": "retrieved",
                                                            "fieldName": "heteroticGroup"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "HETEROTIC_GROUP"
                                    }
                                }
                            },
                            {
                                "entity": "family_member",
                                "identifier": "familyMemberDbId",
                                "retrieveRecord": false,
                                "orderNumber": 5,
                                "action": "create",
                                "apiEndpoint": "family-members",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "familyDbId",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "familyDbId"
                                        },
                                        {
                                            "referenceName": "cropCode",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "cropCode"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createFamilyMember": [
                                            {
                                                "condition": "germplasmCreated is true and familyDbId is not null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ],
                                        "2_familyCodeLogic": [
                                            {
                                                "condition": "cropCode = 'CHICKPEA'",
                                                "decision": {
                                                    "data": {
                                                        "familyCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "CFM"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'GROUNDNUT'",
                                                "decision": {
                                                    "data": {
                                                        "familyCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "GFM"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'SORGHUM'",
                                                "decision": {
                                                    "data": {
                                                        "familyCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "SFM"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'FINGERMILLET'",
                                                "decision": {
                                                    "data": {
                                                        "familyCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "FFM"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'PEARLMILLET'",
                                                "decision": {
                                                    "data": {
                                                        "familyCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "MFM"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'PIGEONPEA'",
                                                "decision": {
                                                    "data": {
                                                        "familyCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "PFM"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "familyDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "familyDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed",
                                "identifier": "seedDbId",
                                "retrieveRecord": true,
                                "orderNumber": 6,
                                "action": "create",
                                "apiEndpoint": "seeds",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "germplasmCreated",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "existingGermplasmUsed",
                                            "type": "previousDecision",
                                            "entity": "germplasm",
                                            "decisionName": "useExisting"
                                        },
                                        {
                                            "referenceName": "cropCode",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "cropCode"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeed": [
                                            {
                                                "condition": "germplasmCreated is true or existingGermplasmUsed is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ],
                                        "2_seedCodeLogic": [
                                            {
                                                "condition": "cropCode = 'CHICKPEA'",
                                                "decision": {
                                                    "data": {
                                                        "seedCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "CSD"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'GROUNDNUT'",
                                                "decision": {
                                                    "data": {
                                                        "seedCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "GSD"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'SORGHUM'",
                                                "decision": {
                                                    "data": {
                                                        "seedCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "SSD"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'FINGERMILLET'",
                                                "decision": {
                                                    "data": {
                                                        "seedCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "FSD"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'PEARLMILLET'",
                                                "decision": {
                                                    "data": {
                                                        "seedCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "MSD"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'PIGEONPEA'",
                                                "decision": {
                                                    "data": {
                                                        "seedCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "PSD"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "sourceEntryDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "entryDbId"
                                    },
                                    "sourcePlotDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "plotDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 7,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "occurrence_data",
                                "identifier": "occurrenceDataDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "occurrence-data",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createOccurrenceData": [
                                            {
                                                "condition": "harvestedPackagePrefix is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "HARVESTED_PACKAGE_PREFIX"
                                    },
                                    "dataValue": {
                                        "type": "nameBuilder",
                                        "specialType": "HARVESTED_PACKAGE_PREFIX"
                                    }
                                }
                            },
                            {
                                "entity": "package",
                                "identifier": "packageDbId",
                                "retrieveRecord": false,
                                "orderNumber": 9,
                                "action": "create",
                                "apiEndpoint": "packages",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        },
                                        {
                                            "referenceName": "cropCode",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "cropCode"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createPackage": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "packageLabel": {
                                                            "type": "nameBuilder"
                                                        }
                                                    }
                                                }   
                                            }
                                        ],
                                        "2_packageCodeLogic": [
                                            {
                                                "condition": "cropCode = 'CHICKPEA'",
                                                "decision": {
                                                    "data": {
                                                        "packageCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "CPK"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'GROUNDNUT'",
                                                "decision": {
                                                    "data": {
                                                        "packageCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "GPK"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'SORGHUM'",
                                                "decision": {
                                                    "data": {
                                                        "packageCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "SPK"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'FINGERMILLET'",
                                                "decision": {
                                                    "data": {
                                                        "packageCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "FPK"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'PEARLMILLET'",
                                                "decision": {
                                                    "data": {
                                                        "packageCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "MPK"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'PIGEONPEA'",
                                                "decision": {
                                                    "data": {
                                                        "packageCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "PPK"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {}
                            }
                        ]
                    }
                },
                {
                    "cropCode": [
                        "CHICKPEA", "GROUNDNUT", "SORGHUM",
                        "FINGERMILLET", "PEARLMILLET", "PIGEONPEA"
                    ],
                    "crossMethodAbbrev": "SELFING",
                    "state": "fixed",
                    "harvestMethodAbbrev": "BULK",
                    "configDescription": "This harvest use case applies to the CHICKPEA, GROUNDNUT, SORGHUM, FINGERMILLET, PEARLMILLET, and PIGEONPEA crops, for selfing crosses OR plots, where the germplasm state is fixed. Via the BULK harvest method, it creates a single NEW seed record and a single NEW package record, as well as other related records like names, attributes, and relation records. The new seed record is linked to the planted germplasm.",
                    "config": {
                        "harvestType": "single",
                        "entities": [
                            {
                                "entity": "seed",
                                "identifier": "seedDbId",
                                "retrieveRecord": true,
                                "orderNumber": 6,
                                "action": "create",
                                "apiEndpoint": "seeds",
                                "data": {
                                    "germplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "sourceEntryDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "entryDbId"
                                    },
                                    "sourcePlotDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "plotDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 7,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "occurrence_data",
                                "identifier": "occurrenceDataDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "occurrence-data",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createOccurrenceData": [
                                            {
                                                "condition": "harvestedPackagePrefix is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "HARVESTED_PACKAGE_PREFIX"
                                    },
                                    "dataValue": {
                                        "type": "nameBuilder",
                                        "specialType": "HARVESTED_PACKAGE_PREFIX"
                                    }
                                }
                            },
                            {
                                "entity": "package",
                                "identifier": "packageDbId",
                                "retrieveRecord": false,
                                "orderNumber": 9,
                                "action": "create",
                                "apiEndpoint": "packages",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        },
                                        {
                                            "referenceName": "cropCode",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "cropCode"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createPackage": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "packageLabel": {
                                                            "type": "nameBuilder"
                                                        }
                                                    }
                                                }   
                                            }
                                        ],
                                        "2_packageCodeLogic": [
                                            {
                                                "condition": "cropCode = 'CHICKPEA'",
                                                "decision": {
                                                    "data": {
                                                        "packageCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "CPK"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'GROUNDNUT'",
                                                "decision": {
                                                    "data": {
                                                        "packageCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "GPK"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'SORGHUM'",
                                                "decision": {
                                                    "data": {
                                                        "packageCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "SPK"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'FINGERMILLET'",
                                                "decision": {
                                                    "data": {
                                                        "packageCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "FPK"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'PEARLMILLET'",
                                                "decision": {
                                                    "data": {
                                                        "packageCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "MPK"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'PIGEONPEA'",
                                                "decision": {
                                                    "data": {
                                                        "packageCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "PPK"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {}
                            }
                        ]
                    }
                },
                {
                    "cropCode": [
                        "CHICKPEA", "GROUNDNUT", "SORGHUM",
                        "FINGERMILLET", "PEARLMILLET", "PIGEONPEA"
                    ],
                    "crossMethodAbbrev": "SELFING",
                    "state": "fixed",
                    "harvestMethodAbbrev": "SINGLE_PLANT",
                    "configDescription": "This harvest use case applies to the CHICKPEA, GROUNDNUT, SORGHUM, FINGERMILLET, PEARLMILLET, and PIGEONPEA crops, for selfing crosses OR plots, where the germplasm state is fixed. Via the SINGLE PLANT harvest method, it creates n NEW seed records (N = number of plants), each with its own package record, as well as other related records like names, attributes, and relation records. Each new seed record is linked to the planted germplasm.",
                    "config": {
                        "harvestType": "multiple",
                        "numericVariable": {
                            "variableName": "noOfPlant",
                            "variableType": "integer"
                        },
                        "entities": [
                            {
                                "entity": "seed",
                                "identifier": "seedDbId",
                                "retrieveRecord": true,
                                "orderNumber": 6,
                                "action": "create",
                                "apiEndpoint": "seeds",
                                "data": {
                                    "germplasmDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "germplasmDbId"
                                    },
                                    "sourceEntryDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "entryDbId"
                                    },
                                    "sourcePlotDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "plotDbId"
                                    }
                                }
                            },
                            {
                                "entity": "seed_relation",
                                "identifier": "seedRelationDbId",
                                "retrieveRecord": false,
                                "orderNumber": 7,
                                "action": "create",
                                "apiEndpoint": "seed-relations",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createSeedRelation": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "parentSeedDbId": {
                                        "type": "recordInfo",
                                        "subType": "planted",
                                        "fieldName": "seedDbId"
                                    },
                                    "orderNumber": {
                                        "type": "plainText",
                                        "textValue": "1"
                                    }
                                }
                            },
                            {
                                "entity": "occurrence_data",
                                "identifier": "occurrenceDataDbId",
                                "retrieveRecord": false,
                                "orderNumber": 8,
                                "action": "create",
                                "apiEndpoint": "occurrence-data",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createOccurrenceData": [
                                            {
                                                "condition": "harvestedPackagePrefix is null",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {
                                    "variableAbbrev": {
                                        "type": "plainText",
                                        "textValue": "HARVESTED_PACKAGE_PREFIX"
                                    },
                                    "dataValue": {
                                        "type": "nameBuilder",
                                        "specialType": "HARVESTED_PACKAGE_PREFIX"
                                    }
                                }
                            },
                            {
                                "entity": "package",
                                "identifier": "packageDbId",
                                "retrieveRecord": false,
                                "orderNumber": 9,
                                "action": "create",
                                "apiEndpoint": "packages",
                                "validation": {
                                    "dataRequired": [
                                        {
                                            "referenceName": "seedCreated",
                                            "type": "previousDecision",
                                            "entity": "seed",
                                            "decisionName": "createNew"
                                        },
                                        {
                                            "referenceName": "harvestedPackagePrefix",
                                            "type": "apiRetrieval",
                                            "subType": "singleValue",
                                            "apiMethod": "POST",
                                            "apiEndpoint": "occurrence-data-search",
                                            "apiParams": {
                                                "variableAbbrev": {
                                                    "type": "plainText",
                                                    "textValue": "HARVESTED_PACKAGE_PREFIX"
                                                },
                                                "occurrenceDbId": {
                                                    "type": "recordInfo",
                                                    "subType": "planted",
                                                    "fieldName": "occurrenceDbId"
                                                }
                                            },
                                            "apiResultFieldName": "dataValue"
                                        },
                                        {
                                            "referenceName": "cropCode",
                                            "type": "recordInfo",
                                            "subType": "planted",
                                            "fieldName": "cropCode"
                                        }
                                    ],
                                    "decisions": {
                                        "1_createPackage": [
                                            {
                                                "condition": "seedCreated is true",
                                                "decision": {
                                                    "flags": {
                                                        "createNew": true
                                                    },
                                                    "data": {
                                                        "packageLabel": {
                                                            "type": "nameBuilder"
                                                        }
                                                    }
                                                }   
                                            }
                                        ],
                                        "2_packageCodeLogic": [
                                            {
                                                "condition": "cropCode = 'CHICKPEA'",
                                                "decision": {
                                                    "data": {
                                                        "packageCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "CPK"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'GROUNDNUT'",
                                                "decision": {
                                                    "data": {
                                                        "packageCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "GPK"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'SORGHUM'",
                                                "decision": {
                                                    "data": {
                                                        "packageCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "SPK"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'FINGERMILLET'",
                                                "decision": {
                                                    "data": {
                                                        "packageCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "FPK"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'PEARLMILLET'",
                                                "decision": {
                                                    "data": {
                                                        "packageCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "MPK"
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                "condition": "cropCode = 'PIGEONPEA'",
                                                "decision": {
                                                    "data": {
                                                        "packageCodePrefix": {
                                                            "type": "plainText",
                                                            "textValue": "PPK"
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "data": {}
                            }
                        ]
                    }
                }
            ]
        $$,
        1,
        'harvest_manager',
        (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),
        'BDS-29 BDS-4849 - CB-HM: Insert new config for Harvest Use Case Requirements Configuration'
    )
;

--rollback DELETE FROM platform.config WHERE abbrev = 'HM_HARVEST_USE_CASE_BEHAVIOR';