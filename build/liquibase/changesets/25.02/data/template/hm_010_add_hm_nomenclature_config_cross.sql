--liquibase formatted sql

--changeset postgres:hm_010_add_hm_nomenclature_config_cross context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM platform.config WHERE abbrev = 'HM_NOMENCLATURE_CONFIG_CROSS') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-29 BDS-4849 - CB-HM: Insert new config for Harvest Nomenclature Configuration for Crosses



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_NOMENCLATURE_CONFIG_CROSS',
        'Harvest Manager - Harvest Nomenclature Configuration for Crosses',
        $$			
            [
                {
                    "cropCode": "*",
                    "crossMethodAbbrev": "SELFING",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "|",
                                "orderNumber": 1
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 2
                            }
                        ]
                    }
                },
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": "!= SELFING",
                    "harvestMethodAbbrev": "BULK",
                    "config": {
                        "pattern": [
                            {
                                "type": "freeText",
                                "value": "IR ",
                                "orderNumber": 0
                            },
                            {
                                "type": "dbSequence",
                                "sequenceSchema": "germplasm",
                                "sequenceName": "cross_number_seq",
                                "orderNumber": 1
                            }
                        ]
                    }
                }
            ]
        $$,
        1,
        'harvest_manager',
        (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),
        'BDS-29 BDS-4849 - CB-HM: Insert new config for Harvest Nomenclature Configuration for Crosses'
    )
;

--rollback DELETE FROM platform.config WHERE abbrev = 'HM_NOMENCLATURE_CONFIG_CROSS';