--liquibase formatted sql

--changeset postgres:hm_008_add_hm_nomenclature_config_harvested_package_prefix context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM platform.config WHERE abbrev = 'HM_NOMENCLATURE_CONFIG_HARVESTED_PACKAGE_PREFIX') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-29 BDS-4849 - CB-HM: Insert new config for Harvest Nomenclature Configuration for Harvested Package Prefix



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_NOMENCLATURE_CONFIG_HARVESTED_PACKAGE_PREFIX',
        'Harvest Manager - Harvest Nomenclature Configuration for Harvested Package Prefix',
        $$			
            [
                {
                    "cropCode": [
                        "MAIZE", "WHEAT", "COWPEA", "SOYBEAN",
                        "CHICKPEA", "GROUNDNUT", "SORGHUM",
                        "FINGERMILLET", "PEARLMILLET", "PIGEONPEA"
                    ],
                    "crossMethodAbbrev": "*",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "programCode",
                                "orderNumber": 0
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "experimentYearYY",
                                "orderNumber": 1
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "experimentSeasonCode",
                                "orderNumber": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 3
                            },
                            {
								"type": "counter",
								"subType": "autoIncrement",
								"options": {
									"schema": "experiment",
									"table": "occurrence_data",
									"field": "data_value",
									"zeroPadded": true,
									"maxDigits": 3
								},
                                "orderNumber": 4
							}
                        ]
                    }
                }
            ]
        $$,
        1,
        'harvest_manager',
        (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),
        'BDS-29 BDS-4849 - CB-HM: Insert new config for Harvest Nomenclature Configuration for Harvested Package Prefix'
    )
;

--rollback DELETE FROM platform.config WHERE abbrev = 'HM_NOMENCLATURE_CONFIG_HARVESTED_PACKAGE_PREFIX';