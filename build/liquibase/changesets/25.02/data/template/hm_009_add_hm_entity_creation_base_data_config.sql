--liquibase formatted sql

--changeset postgres:hm_009_add_hm_entity_creation_base_data_config context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM platform.config WHERE abbrev = 'HM_ENTITY_CREATION_BASE_DATA') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-29 BDS-4962 - CB-HM: Insert new config for Entity Creation Base Data



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_ENTITY_CREATION_BASE_DATA',
        'Harvest Manager - Configuration for Entity Creation Base Data (PLEASE DO NOT MODIFY. See notes for more info.)',
        $$			
            {
                "germplasm": {
                    "designation": {
                        "type": "nameBuilder"
                    },
                    "cropDbId" : {
                        "type": "recordInfo",
                        "subType": "planted",
                        "fieldName": "cropDbId"
                    }
                },
                "germplasm_name": {
                    "germplasmDbId": {
                        "type": "recordInfo",
                        "subType": "new",
                        "fieldName": "germplasmDbId"
                    },
                    "germplasmNameStatus": {
                        "type": "plainText",
                        "textValue": "standard"
                    },
                    "germplasmNormalizedName": {
                        "type": "recordInfo",
                        "subType": "new",
                        "fieldName": "germplasmNormalizedName"
                    }
                },
                "germplasm_relation": {
                    "childGermplasmDbId": {
                        "type": "recordInfo",
                        "subType": "new",
                        "fieldName": "germplasmDbId"
                    }
                },
                "germplasm_attribute": {
                    "germplasmDbId": {
                        "type": "recordInfo",
                        "subType": "new",
                        "fieldName": "germplasmDbId"
                    }
                },
                "family": {
                    "familyName": {
                        "type": "nameBuilder"
                    },
                    "crossDbId": {
                        "type": "recordInfo",
                        "subType": "planted",
                        "fieldName": "crossDbId"
                    }
                },
                "family_member": {
                    "germplasmDbId": {
                        "type": "recordInfo",
                        "subType": "new",
                        "fieldName": "germplasmDbId"
                    }
                },
                "seed": {
                    "germplasmDbId": {
                        "type": "recordInfo",
                        "subType": "new",
                        "fieldName": "germplasmDbId"
                    },
                    "seedName": {
                        "type": "nameBuilder"
                    },
                    "programDbId": {
                        "type": "recordInfo",
                        "subType": "planted",
                        "fieldName": "programDbId"
                    },
                    "harvestDate": {
                        "type": "recordInfo",
                        "subType": "planted",
                        "fieldName": "harvestDate"
                    },
                    "harvestMethod": {
                        "type": "recordInfo",
                        "subType": "planted",
                        "fieldName": "harvestMethod"
                    },
                    "crossDbId": {
                        "type": "recordInfo",
                        "subType": "planted",
                        "fieldName": "crossDbId"
                    },
                    "harvestSource": {
                        "type": "recordInfo",
                        "subType": "planted",
                        "fieldName": "harvestSource"
                    },
                    "sourceExperimentDbId": {
                        "type": "recordInfo",
                        "subType": "planted",
                        "fieldName": "experimentDbId"
                    },
                    "sourceOccurrenceDbId": {
                        "type": "recordInfo",
                        "subType": "planted",
                        "fieldName": "occurrenceDbId"
                    },
                    "sourceLocationDbId": {
                        "type": "recordInfo",
                        "subType": "planted",
                        "fieldName": "locationDbId"
                    }
                },
                "seed_relation": {
                    "childSeedDbId": {
                        "type": "recordInfo",
                        "subType": "new",
                        "fieldName": "seedDbId"
                    }
                },
                "seed_attribute": {
                    "seedDbId": {
                        "type": "recordInfo",
                        "subType": "new",
                        "fieldName": "seedDbId"
                    }
                },
                "package": {
                    "seedDbId": {
                        "type": "recordInfo",
                        "subType": "new",
                        "fieldName": "seedDbId"
                    },
                    "programDbId": {
                        "type": "recordInfo",
                        "subType": "planted",
                        "fieldName": "programDbId"
                    },
                    "packageLabel": {
                        "type": "nameBuilder"
                    },
                    "packageStatus": {
                        "type": "scaleValue",
                        "variableAbbrev": "PACKAGE_STATUS",
                        "scaleValueAbbrev": "ACTIVE"
                    },
					"packageUnit": {
                        "type": "scaleValue",
                        "variableAbbrev": "PACKAGE_UNIT",
                        "scaleValueAbbrev": "G"
                    },
                    "packageQuantity": {
                        "type": "plainText",
                        "textValue": "0"
                    }
                },
                "cross": {
                    "crossName": {
                        "type": "nameBuilder"
                    },
                    "experimentDbId": {
                        "type": "recordInfo",
                        "subType": "planted",
                        "fieldName": "experimentDbId"
                    }
                },
                "cross_parent": {
                    "experimentDbId": {
                        "type": "recordInfo",
                        "subType": "planted",
                        "fieldName": "experimentDbId"
                    },
                    "germplasmDbId": {
                        "type": "recordInfo",
                        "subType": "planted",
                        "fieldName": "germplasmDbId"
                    },
                    "seedDbId": {
                        "type": "recordInfo",
                        "subType": "planted",
                        "fieldName": "seedDbId"
                    },
                    "entryDbId": {
                        "type": "recordInfo",
                        "subType": "planted",
                        "fieldName": "entryDbId"
                    }
                },
                "occurrence_data": {
                    "occurrenceDbId": {
                        "type": "recordInfo",
                        "subType": "planted",
                        "fieldName": "occurrenceDbId"
                    }
                }
            }
        $$,
        1,
        'harvest_manager',
        (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),
        'PLEASE DO NOT MODIFY. Kindly reach out to the CB development team if changes are needed. (BDS-29 BDS-4962 - CB-HM: Insert new config for Entity Creation Base Data)'
    )
;

--rollback DELETE FROM platform.config WHERE abbrev = 'HM_ENTITY_CREATION_BASE_DATA';