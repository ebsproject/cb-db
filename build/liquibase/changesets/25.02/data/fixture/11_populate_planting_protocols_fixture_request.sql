--liquibase formatted sql

--changeset postgres:11_populate_planting_protocols_fixture_request context:fixture splitStatements:false rollbackSplitStatements:false
--comment:BDS-4530 Populate tenant_protocol connected to experiment TestingData2_NewCB-UI



INSERT INTO  tenant.protocol 
	(protocol_code,protocol_name,protocol_type,description,program_id,creator_id,notes)
SELECT
	    'PLANTING_PROTOCOL_'||ex.experiment_code AS protocol_code,
	    'Planting Protocol Exp'||substring(ex.experiment_code, 4) AS protocol_name,
	    'planting' AS protocol_type,
	    null AS description,
	    ex.program_id,
	    (SELECT id FROM tenant.person WHERE username ='admin') AS creator_id,
		'Inserted via liquibase changeset (BDS-4530)' notes
	FROM
	   	experiment.experiment ex
	   	WHERE
		ex.experiment_name in ('TestingData2_NewCB-UI','TestingData1_NewCB-UI');



--rollback DELETE FROM
--rollback     tenant.protocol
--rollback WHERE
--rollback 		protocol_code ILIKE 'PLANTING_PROTOCOL_%'
--rollback AND
--rollback 		notes = 'Inserted via liquibase changeset (BDS-4530)';