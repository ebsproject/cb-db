--liquibase formatted sql

--changeset author_name:hm_001_insert_new_scale_value_for_mta_status context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-29 BDS-5114 CB-HM: Add 'PUD2' as scale value for MTA_STATUS


-- add scale values to MTA_STATUS variable
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 1) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'MTA_STATUS'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('PUD2', 'PUD2', 'PUD2', 'PUD2', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'MTA_STATUS'
;


-- rollback DELETE FROM
-- rollback     master.scale_value
-- rollback WHERE
-- rollback     abbrev IN (
-- rollback         'MTA_STATUS_PUD2'
-- rollback     )
-- rollback ;