--liquibase formatted sql

--changeset postgres:6_place_geospatial_object_fixture_request context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-4530 Populate place_geospatial_object connected to experiment TestingData2_NewCB-UI



INSERT INTO place.geospatial_object
(geospatial_object_code,geospatial_object_name,geospatial_object_type,geospatial_object_subtype,geospatial_coordinates,altitude,description,parent_geospatial_object_id,root_geospatial_object_id,is_void,creator_id,notes,coordinates)
 VALUES 
('IRRIHQ-2024-WS-058','IRRIHQ-2024-WS-058','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,NULL),
('PH_NE_SM2-2024-WS-001','PH_NE_SM2-2024-WS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NE_SM2' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,NULL),
('PH_BO_UA-2024-WS-001','PH_BO_UA-2024-WS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_BO_UA' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,NULL),
('PH_AN_RM-2024-WS-001','PH_AN_RM-2024-WS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_AN_RM' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,NULL),
('PH_AN_RM-2024-CS-001','PH_AN_RM-2024-CS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_AN_RM' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,NULL),
('PH_BO_UA-2024-CS-001','PH_BO_UA-2024-CS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_BO_UA' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,NULL),
('PH_BO_SG-2024-CS-001','PH_BO_SG-2024-CS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_BO_SG' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,NULL),
('PH_IB_SO-2024-CS-001','PH_IB_SO-2024-CS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_IB_SO' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,NULL),
('IRRIHQ-2024-CS-004','IRRIHQ-2024-CS-004','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,NULL),
('PH_NE_SM2-2024-CS-003','PH_NE_SM2-2024-CS-003','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NE_SM2' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,NULL);



--rollback DELETE FROM place.geospatial_object WHERE geospatial_object_code IN (
--rollback 'IRRIHQ-2024-WS-058',
--rollback 'PH_NE_SM2-2024-WS-001',
--rollback 'PH_BO_UA-2024-WS-001',
--rollback 'PH_AN_RM-2024-WS-001',
--rollback 'PH_AN_RM-2024-CS-001',
--rollback 'PH_BO_UA-2024-CS-001',
--rollback 'PH_BO_SG-2024-CS-001',
--rollback 'PH_IB_SO-2024-CS-001',
--rollback 'IRRIHQ-2024-CS-004',
--rollback 'PH_NE_SM2-2024-CS-003'
--rollback );