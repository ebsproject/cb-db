--liquibase formatted sql

--changeset postgres:add_variable_TOT_PLT_GAP context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-5141 CB-DB: Insert TOT_PLT_GAP variable



-- TOT_PLT_GAP
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('TOT_PLT_GAP', 'TOT_PLT_GAP', 'Total plot gap', 'float', false, 'observation', 'plot', 'occurrence', 'TOT_PLT_GAP', 'active', '1', 'TOT_PLT_GAP','25.02')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'TOT_PLT_GAP' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('TOT_PLT_GAP', 'Total plot gap', 'Total plot gap') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('TOT_PLT_GAP_METHOD', 'Total plot gap method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('TOT_PLT_GAP_SCALE', 'Total plot gap scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('TOT_PLT_GAP');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('TOT_PLT_GAP_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('TOT_PLT_GAP');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('TOT_PLT_GAP_SCALE');


--changeset postgres:update_TOT_PLTGAP_AREA_SQM_formula_parameter context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-5141 CB-DB: update TOT_PLTGAP_AREA_SQM formula



UPDATE master.formula_parameter SET
order_number = 2
WHERE
formula_id in (select id from master.formula f where formula ilike 'TOT_PLTGAP_AREA_SQM%') 
AND
param_variable_id in (select id from master.variable where abbrev = 'DIST_BET_ROWS');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('TOT_PLT_GAP') -- must be the same order as defined in the database function
    AND var.abbrev = 'TOT_PLTGAP_AREA_SQM'
;

SELECT master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.formula_parameter WHERE
--rollback formula_id in (select id from master.formula f where formula ilike 'TOT_PLTGAP_AREA_SQM%') 
--rollback AND
--rollback param_variable_id in (select id from master.variable where abbrev = 'TOT_PLT_GAP');
--rollback UPDATE master.formula_parameter SET
--rollback order_number = 1
--rollback WHERE
--rollback formula_id in (select id from master.formula f where formula ilike 'TOT_PLTGAP_AREA_SQM%') 
--rollback AND
--rollback param_variable_id in (select id from master.variable where abbrev = 'DIST_BET_ROWS');