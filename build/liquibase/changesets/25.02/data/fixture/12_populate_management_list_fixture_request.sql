--liquibase formatted sql

--changeset postgres:12_populate_management_list_fixture_request context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-4530 Populate platform_list connected to experiment TestingData2_NewCB-UI



INSERT INTO platform.list 
    (abbrev, name, display_name, type, entity_id, creator_id, notes, list_sub_type)
SELECT
	'MANAGEMENT_PROTOCOL_' || occur.occurrence_code AS abbrev,													
	occur.occurrence_name|| ' Management Protocol (' || (SELECT experiment_code from experiment.experiment where id = occur.experiment_id) || ')' AS name,
	occur.occurrence_name|| ' Management Protocol (' || (SELECT experiment_code from experiment.experiment where id = occur.experiment_id) || ')' AS display_name,
	'variable' AS type,
	(SELECT id FROM "dictionary".entity WHERE abbrev = 'VARIABLE') AS entity_id,
	(SELECT id FROM tenant.person WHERE username ='admin') AS creator_id,
	'Inserted via liquibase changeset (BDS-4530)' notes,
	'management protocol' AS list_sub_type
FROM 
	experiment.occurrence occur 
WHERE
	occur.experiment_id IN (SELECT id from experiment.experiment where experiment_name in ('TestingData2_NewCB-UI','TestingData1_NewCB-UI'));



--rollback DELETE FROM
--rollback     platform.list
--rollback WHERE
--rollback name ILIKE 'TestingData2_NewCB-UI'
--rollback AND
--rollback notes = 'Inserted via liquibase changeset (BDS-4530)'



