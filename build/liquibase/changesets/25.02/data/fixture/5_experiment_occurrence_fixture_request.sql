--liquibase formatted sql

--changeset postgres:5_experiment_occurrence_fixture_request context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-4530 Populate experiment_occurrence connected to experiment TestingData2_NewCB-UI



INSERT INTO experiment.occurrence
(occurrence_code,occurrence_name,occurrence_status,description,experiment_id,geospatial_object_id,is_void,creator_id,notes,rep_count,site_id,field_id,occurrence_number,remarks,entry_count,plot_count,occurrence_design_type)
 VALUES 
((SELECT experiment_code||'-3' FROM experiment.experiment WHERE experiment_name = 'TestingData2_NewCB-UI'),'TestingData2_NewCB-UI-003','planted;trait data collected;trait data quality checked',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='TestingData2_NewCB-UI' LIMIT 1),NULL,False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_AN_RM' LIMIT 1),NULL,3,NULL,60,120,'Row-Column'),
((SELECT experiment_code||'-4' FROM experiment.experiment WHERE experiment_name = 'TestingData2_NewCB-UI'),'TestingData2_NewCB-UI-004','planted',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='TestingData2_NewCB-UI' LIMIT 1),NULL,False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_BO_UA' LIMIT 1),NULL,4,NULL,60,120,'Row-Column'),
((SELECT experiment_code||'-2' FROM experiment.experiment WHERE experiment_name = 'TestingData2_NewCB-UI'),'TestingData2_NewCB-UI-002','planted',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='TestingData2_NewCB-UI' LIMIT 1),NULL,False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NE_SM2' LIMIT 1),NULL,2,NULL,60,120,'Alpha-Lattice'),
((SELECT experiment_code||'-1' FROM experiment.experiment WHERE experiment_name = 'TestingData2_NewCB-UI'),'TestingData2_NewCB-UI-001','planted',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='TestingData2_NewCB-UI' LIMIT 1),NULL,False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),NULL,1,NULL,60,120,'Alpha-Lattice'),

((SELECT experiment_code||'-1' FROM experiment.experiment WHERE experiment_name = 'TestingData1_NewCB-UI'),'TestingData1_NewCB-UI-001','planted;trait data collected;trait data quality checked',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='TestingData1_NewCB-UI' LIMIT 1),NULL,False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),NULL,1,NULL,60,120,'Alpha-Lattice'),
((SELECT experiment_code||'-6' FROM experiment.experiment WHERE experiment_name = 'TestingData1_NewCB-UI'),'TestingData1_NewCB-UI-006','planted',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='TestingData1_NewCB-UI' LIMIT 1),NULL,False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_BO_SG' LIMIT 1),NULL,6,NULL,60,112,'Partially Replicated'),
((SELECT experiment_code||'-2' FROM experiment.experiment WHERE experiment_name = 'TestingData1_NewCB-UI'),'TestingData1_NewCB-UI-002','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='TestingData1_NewCB-UI' LIMIT 1),NULL,False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NE_SM2' LIMIT 1),NULL,2,NULL,60,120,'Alpha-Lattice'),
((SELECT experiment_code||'-3' FROM experiment.experiment WHERE experiment_name = 'TestingData1_NewCB-UI'),'TestingData1_NewCB-UI-003','planted;trait data collected;trait data quality checked',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='TestingData1_NewCB-UI' LIMIT 1),NULL,False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_AN_RM' LIMIT 1),NULL,3,NULL,60,90,'Augmented RCBD'),
((SELECT experiment_code||'-4' FROM experiment.experiment WHERE experiment_name = 'TestingData1_NewCB-UI'),'TestingData1_NewCB-UI-004','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='TestingData1_NewCB-UI' LIMIT 1),NULL,False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_BO_UA' LIMIT 1),NULL,4,NULL,60,90,'Augmented RCBD'),
((SELECT experiment_code||'-5' FROM experiment.experiment WHERE experiment_name = 'TestingData1_NewCB-UI'),'TestingData1_NewCB-UI-005','planted;trait data collected;trait data quality checked',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='TestingData1_NewCB-UI' LIMIT 1),NULL,False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_IB_SO' LIMIT 1),NULL,5,NULL,60,112,'Partially Replicated');



--rollback DELETE FROM experiment.occurrence WHERE occurrence_name IN (
--rollback 'TestingData2_NewCB-UI-003',
--rollback 'TestingData2_NewCB-UI-004',
--rollback 'TestingData2_NewCB-UI-002',
--rollback 'TestingData2_NewCB-UI-001',
--rollback 'TestingData1_NewCB-UI-001',
--rollback 'TestingData1_NewCB-UI-006',
--rollback 'TestingData1_NewCB-UI-002',
--rollback 'TestingData1_NewCB-UI-003',
--rollback 'TestingData1_NewCB-UI-004',
--rollback 'TestingData1_NewCB-UI-005'
--rollback )