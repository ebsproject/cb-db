--liquibase formatted sql

--changeset postgres:7_experiment_location_fixture_request context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-4530 Populate experiment_location connected to experiment TestingData2_NewCB-UI



INSERT INTO experiment.location
(location_code,location_name,location_status,location_type,description,steward_id,geospatial_object_id,is_void,creator_id,notes,location_year,season_id,site_id,field_id,remarks,entry_count,plot_count)
 VALUES 
('IRRIHQ-2024-WS-058','IRRIHQ-2024-WS-058','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ-2024-WS-058' LIMIT 1),False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,2024,(SELECT id FROM tenant.season WHERE season_code ='WS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),NULL,58,0,120),
('PH_NE_SM2-2024-WS-001','PH_NE_SM2-2024-WS-001','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NE_SM2-2024-WS-001' LIMIT 1),False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,2024,(SELECT id FROM tenant.season WHERE season_code ='WS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NE_SM2' LIMIT 1),NULL,1,0,120),
('PH_BO_UA-2024-WS-001','PH_BO_UA-2024-WS-001','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_BO_UA-2024-WS-001' LIMIT 1),False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,2024,(SELECT id FROM tenant.season WHERE season_code ='WS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_BO_UA' LIMIT 1),NULL,1,0,120),
('PH_AN_RM-2024-WS-001','PH_AN_RM-2024-WS-001','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_AN_RM-2024-WS-001' LIMIT 1),False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,2024,(SELECT id FROM tenant.season WHERE season_code ='WS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_AN_RM' LIMIT 1),NULL,1,0,120),
('PH_IB_SO-2024-CS-001','PH_IB_SO-2024-CS-001','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_IB_SO-2024-CS-001' LIMIT 1),False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,2024,(SELECT id FROM tenant.season WHERE season_code ='CS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_IB_SO' LIMIT 1),NULL,1,0,112),
('PH_BO_UA-2024-CS-001','PH_BO_UA-2024-CS-001','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_BO_UA-2024-CS-001' LIMIT 1),False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,2024,(SELECT id FROM tenant.season WHERE season_code ='CS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_BO_UA' LIMIT 1),NULL,1,0,90),
('PH_AN_RM-2024-CS-001','PH_AN_RM-2024-CS-001','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_AN_RM-2024-CS-001' LIMIT 1),False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,2024,(SELECT id FROM tenant.season WHERE season_code ='CS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_AN_RM' LIMIT 1),NULL,1,0,90),
('PH_BO_SG-2024-CS-001','PH_BO_SG-2024-CS-001','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_BO_SG-2024-CS-001' LIMIT 1),False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,2024,(SELECT id FROM tenant.season WHERE season_code ='CS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_BO_SG' LIMIT 1),NULL,1,0,112),
('IRRIHQ-2024-CS-004','IRRIHQ-2024-CS-004','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ-2024-CS-004' LIMIT 1),False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,2024,(SELECT id FROM tenant.season WHERE season_code ='CS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),NULL,4,0,120),
('PH_NE_SM2-2024-CS-003','PH_NE_SM2-2024-CS-003','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NE_SM2-2024-CS-003' LIMIT 1),False,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,2024,(SELECT id FROM tenant.season WHERE season_code ='CS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NE_SM2' LIMIT 1),NULL,3,0,120);



--rollback DELETE FROM experiment.location WHERE location_code IN (
--rollback 'IRRIHQ-2024-WS-058',
--rollback 'PH_NE_SM2-2024-WS-001',
--rollback 'PH_BO_UA-2024-WS-001',
--rollback 'PH_AN_RM-2024-WS-001',
--rollback 'PH_IB_SO-2024-CS-001',
--rollback 'PH_BO_UA-2024-CS-001',
--rollback 'PH_AN_RM-2024-CS-001',
--rollback 'PH_BO_SG-2024-CS-001',
--rollback 'IRRIHQ-2024-CS-004',
--rollback 'PH_NE_SM2-2024-CS-003'
--rollback );