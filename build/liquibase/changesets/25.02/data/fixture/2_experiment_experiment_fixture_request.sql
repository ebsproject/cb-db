--liquibase formatted sql

--changeset postgres:2_experiment_experiment_fixture_request context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-4530 Populate experiment_experiment with TestingData2_NewCB-UI



INSERT INTO experiment.experiment
(program_id,pipeline_id,stage_id,project_id,experiment_year,season_id,planting_season,experiment_code,experiment_name,experiment_objective,experiment_type,experiment_sub_type,experiment_sub_sub_type,experiment_design_type,experiment_status,description,steward_id,creator_id,experiment_plan_id,is_void,notes,data_process_id,crop_id,remarks)
 VALUES 
((SELECT id FROM tenant.program WHERE program_code ='IRSEA' LIMIT 1),(SELECT id FROM tenant.pipeline WHERE pipeline_code ='IRSEA_PIPELINE' LIMIT 1),(SELECT id FROM tenant.stage WHERE stage_code ='AYT' LIMIT 1),NULL,2024,(SELECT id FROM tenant.season WHERE season_code ='WS' LIMIT 1),NULL,experiment.generate_code('experiment'),'TestingData2_NewCB-UI',NULL,'Breeding Trial',NULL,NULL,NULL,'planted',NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,False,'updated entries',(SELECT id FROM master.item WHERE abbrev ='BREEDING_TRIAL_DATA_PROCESS' LIMIT 1),(SELECT id FROM tenant.crop WHERE crop_code='RICE' LIMIT 1),'generate'),
((SELECT id FROM tenant.program WHERE program_code ='IRSEA' LIMIT 1),(SELECT id FROM tenant.pipeline WHERE pipeline_code ='IRSEA_PIPELINE' LIMIT 1),(SELECT id FROM tenant.stage WHERE stage_code ='OYT' LIMIT 1),NULL,2024,(SELECT id FROM tenant.season WHERE season_code ='CS' LIMIT 1),NULL,experiment.generate_code('experiment'),'TestingData1_NewCB-UI',NULL,'Breeding Trial',NULL,NULL,NULL,'planted',NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,False,'updated entries',(SELECT id FROM master.item WHERE abbrev ='BREEDING_TRIAL_DATA_PROCESS' LIMIT 1),(SELECT id FROM tenant.crop WHERE crop_code='RICE' LIMIT 1),'generate');;



--rollback DELETE FROM experiment.experiment WHERE experiment_name IN ('TestingData2_NewCB-UI','TestingData1_NewCB-UI');