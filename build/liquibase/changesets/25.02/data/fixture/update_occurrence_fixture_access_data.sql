--liquibase formatted sql

--changeset postgres:update_occurrence_fixture_access_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-5102 Update fixture occurrence access data column



WITH t_occurrence AS (
    SELECT
        occ.id AS occurrence_id,
        format($$
            {
                "person": {
                    "%1$s": {
                        "permission": "%3$s",
                        "addedBy": %1$s,
                        "addedOn": "%4$s"
                    }
                },
                "program": {
                    "%2$s": {
                        "permission":"%3$s",
                        "addedBy": %1$s,
                        "addedOn": "%4$s"
                    }
                }
            }
        $$,
            expt.creator_id,
            expt.program_id,
            CASE 
			    WHEN prsnrole.id = 25 THEN 'write'
			    WHEN prsnrole.id = 26 THEN 'write'
			    ELSE 'read'
			end,
            now()
        )::json AS access_data
    FROM
        experiment.experiment AS expt
        INNER JOIN experiment.occurrence AS occ
            ON occ.experiment_id = expt.id
        INNER JOIN tenant.person_role AS prsnrole
            ON prsnrole.person_role_code = 'DATA_OWNER'
        where occ.access_data is null
)
UPDATE
    experiment.occurrence AS occ
SET
    access_data = t.access_data
FROM
    t_occurrence AS t
where
	occ.id = t.occurrence_id
;



--rollback SELECT NULL;