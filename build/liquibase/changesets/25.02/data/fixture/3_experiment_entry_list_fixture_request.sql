--liquibase formatted sql

--changeset postgres:3_experiment_entry_list_fixture_request context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-4530 Populate experiment_entry_list connected to experiment TestingData2_NewCB-UI



INSERT INTO experiment.entry_list
(entry_list_code,entry_list_name,description,entry_list_status,experiment_id,is_void,notes,entry_list_type,cross_count,experiment_year,season_id,site_id,crop_id,program_id,creator_id)
 VALUES 
(experiment.generate_code('entry_list'),'TestingData2_NewCB-UI','new','completed',(SELECT id FROM experiment.experiment WHERE experiment_name ='TestingData2_NewCB-UI' LIMIT 1),False,NULL,'entry list',0,NULL,NULL,NULL,(SELECT id FROM tenant.crop WHERE crop_code='RICE' LIMIT 1),(SELECT id FROM tenant.program WHERE program_code ='IRSEA' LIMIT 1),(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1)),
(experiment.generate_code('entry_list'),'TestingData1_NewCB-UI','new','completed',(SELECT id FROM experiment.experiment WHERE experiment_name ='TestingData1_NewCB-UI' LIMIT 1),False,NULL,'entry list',0,NULL,NULL,NULL,(SELECT id FROM tenant.crop WHERE crop_code='RICE' LIMIT 1),(SELECT id FROM tenant.program WHERE program_code ='IRSEA' LIMIT 1),(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1));



--rollback DELETE FROM experiment.entry_list WHERE entry_list_name IN ('TestingData2_NewCB-UI','TestingData1_NewCB-UI');