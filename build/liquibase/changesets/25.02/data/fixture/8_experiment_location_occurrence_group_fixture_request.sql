--liquibase formatted sql

--changeset postgres:8_experiment_location_occurrence_group_fixture_request context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-4530 Populate experiment_location_occurrence_group connected to experiment TestingData2_NewCB-UI



INSERT INTO experiment.location_occurrence_group
(location_id,occurrence_id,order_number,is_void,notes,creator_id)
 VALUES 
((SELECT id FROM experiment.location WHERE location_name ='IRRIHQ-2024-WS-058' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='TestingData2_NewCB-UI-001' LIMIT 1),1,False,NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1)),
((SELECT id FROM experiment.location WHERE location_name ='PH_BO_UA-2024-WS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='TestingData2_NewCB-UI-004' LIMIT 1),1,False,NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1)),
((SELECT id FROM experiment.location WHERE location_name ='PH_AN_RM-2024-WS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='TestingData2_NewCB-UI-003' LIMIT 1),1,False,NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1)),
((SELECT id FROM experiment.location WHERE location_name ='PH_NE_SM2-2024-WS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='TestingData2_NewCB-UI-002' LIMIT 1),1,False,NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1)),
((SELECT id FROM experiment.location WHERE location_name ='IRRIHQ-2024-CS-004' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='TestingData1_NewCB-UI-001' LIMIT 1),1,False,NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1)),
((SELECT id FROM experiment.location WHERE location_name ='PH_NE_SM2-2024-CS-003' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='TestingData1_NewCB-UI-002' LIMIT 1),1,False,NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1)),
((SELECT id FROM experiment.location WHERE location_name ='PH_IB_SO-2024-CS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='TestingData1_NewCB-UI-005' LIMIT 1),1,False,NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1)),
((SELECT id FROM experiment.location WHERE location_name ='PH_BO_UA-2024-CS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='TestingData1_NewCB-UI-004' LIMIT 1),1,False,NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1)),
((SELECT id FROM experiment.location WHERE location_name ='PH_BO_SG-2024-CS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='TestingData1_NewCB-UI-006' LIMIT 1),1,False,NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1)),
((SELECT id FROM experiment.location WHERE location_name ='PH_AN_RM-2024-CS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='TestingData1_NewCB-UI-003' LIMIT 1),1,False,NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1));



--rollback DELETE FROM experiment.location_occurrence_group WHERE occurrence_id IN (SELECT id FROM experiment.occurrence WHERE occurrence_name IN (
--rollback 'TestingData2_NewCB-UI-001',
--rollback 'TestingData2_NewCB-UI-004',
--rollback 'TestingData2_NewCB-UI-003',
--rollback 'TestingData2_NewCB-UI-002',
--rollback 'TestingData1_NewCB-UI-001',
--rollback 'TestingData1_NewCB-UI-002',
--rollback 'TestingData1_NewCB-UI-005',
--rollback 'TestingData1_NewCB-UI-004', 
--rollback 'TestingData1_NewCB-UI-006',
--rollback 'TestingData1_NewCB-UI-003' 
--rollback ));