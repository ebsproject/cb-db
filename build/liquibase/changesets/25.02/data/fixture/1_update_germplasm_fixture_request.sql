--liquibase formatted sql

--changeset postgres:1_update_germplasm_fixture_request context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-4530 Update germplasm.germplasm records on this request



UPDATE germplasm.germplasm
SET
designation='IR28SE1001',
parentage='IR09N542/MILYANG 23',
generation='F2',
germplasm_state='fixed',
germplasm_name_type='line_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR28SE1001',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 100089-B;IR28SE1001',
germplasm_uuid='6fa6e009-a886-48fb-8516-b50983ba5ed4'
WHERE designation = 'IR 100089-B';

UPDATE germplasm.germplasm
SET
designation='IR28SE1004',
parentage='PR 35786-B-3-3-2-1-1/IR08N210',
generation='F2',
germplasm_state='fixed',
germplasm_name_type='line_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR28SE1004',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 103300-B;IR28SE1004',
germplasm_uuid='a93ef8c4-0870-41b0-981f-eb397b4ccdaa'
WHERE designation = 'IR 103300-B';

UPDATE germplasm.germplasm
SET
designation='IR28SE1002',
parentage='PR 37866-1B-1-4/IR08N210',
generation='F2',
germplasm_state='fixed',
germplasm_name_type='line_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR28SE1002',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 103324-B;IR28SE1002',
germplasm_uuid='b286e189-480b-427f-a6e9-77def69837cd'
WHERE designation = 'IR 103324-B';

UPDATE germplasm.germplasm
SET
designation='IR28SE1005',
parentage='IRRI 154*2/IR05N160',
generation='F2',
germplasm_state='fixed',
germplasm_name_type='line_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR28SE1005',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 99101;IR28SE1005',
germplasm_uuid='2020ed9a-b5ec-40aa-a384-2705253f27f5'
WHERE designation = 'IR 99101';

UPDATE germplasm.germplasm
SET
designation='IR28SE1006',
parentage='IRRI 149/COLOMBIA XXI ',
generation='F2',
germplasm_state='fixed',
germplasm_name_type='line_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR28SE1006',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 103353-B;IR28SE1006',
germplasm_uuid='77fa3d6d-54e5-48dc-943b-269da1c6b42a'
WHERE designation = 'IR 103353-B';

UPDATE germplasm.germplasm
SET
designation='IR28SE1003',
parentage='IR10F365/IR 72 ',
generation='F2',
germplasm_state='fixed',
germplasm_name_type='line_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR28SE1003',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 103359-B;IR28SE1003',
germplasm_uuid='9f733f99-fd49-42c7-9be9-e73c24cd133a'
WHERE designation = 'IR 103359-B';

UPDATE germplasm.germplasm
SET
designation='IR 103353-B-B RGA',
parentage='IRRI 149/COLOMBIA XXI ',
generation='F3',
germplasm_state='fixed',
germplasm_name_type='derivative_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR103353-B-BRGA',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 103353-B-B RGA',
germplasm_uuid='3f8bded6-a961-44d6-9683-0028ea6aef88'
WHERE designation = 'IR 103353-B-B RGA';

UPDATE germplasm.germplasm
SET
designation='IR 103353-B-B RGA-B RGA',
parentage='IRRI 149/COLOMBIA XXI ',
generation='F4',
germplasm_state='fixed',
germplasm_name_type='derivative_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR103353-B-BRGA-BRGA',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 103353-B-B RGA-B RGA',
germplasm_uuid='883de629-b459-4f25-b6a7-ad14a95bf143'
WHERE designation = 'IR 103353-B-B RGA-B RGA';

UPDATE germplasm.germplasm
SET
designation='IR 103353-B-B RGA-B RGA-B RGA',
parentage='IRRI 149/COLOMBIA XXI ',
generation='F5',
germplasm_state='fixed',
germplasm_name_type='derivative_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR103353-B-BRGA-BRGA-BRGA',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 103353-B-B RGA-B RGA-B RGA',
germplasm_uuid='f2127b62-7929-42cc-b973-e4644bb6a5d6'
WHERE designation = 'IR 103353-B-B RGA-B RGA-B RGA';

UPDATE germplasm.germplasm
SET
designation='IR 103300-B-B RGA',
parentage='PR 35786-B-3-3-2-1-1/IR08N210 ',
generation='F3',
germplasm_state='fixed',
germplasm_name_type='derivative_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR103300-B-BRGA',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 103300-B-B RGA',
germplasm_uuid='d347c164-8795-44a8-91bb-c26092b3050b'
WHERE designation = 'IR 103300-B-B RGA';

UPDATE germplasm.germplasm
SET
designation='IR 103300-B-B RGA-B RGA',
parentage='PR 35786-B-3-3-2-1-1/IR08N210 ',
generation='F4',
germplasm_state='fixed',
germplasm_name_type='derivative_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR103300-B-BRGA-BRGA',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 103300-B-B RGA-B RGA',
germplasm_uuid='c68a594a-c852-4208-b5ca-580c36f7b1df'
WHERE designation = 'IR 103300-B-B RGA-B RGA';

UPDATE germplasm.germplasm
SET
designation='IR 103300-B-B RGA-B RGA-B RGA',
parentage='PR 35786-B-3-3-2-1-1/IR08N210 ',
generation='F5',
germplasm_state='fixed',
germplasm_name_type='derivative_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR103300-B-BRGA-BRGA-BRGA',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 103300-B-B RGA-B RGA-B RGA',
germplasm_uuid='a9f82a2d-a085-44d2-8a94-2ec017f251f6'
WHERE designation = 'IR 103300-B-B RGA-B RGA-B RGA';

UPDATE germplasm.germplasm
SET
designation='IR 103359-B-B RGA',
parentage='IR10F365/IR 72 ',
generation='F3',
germplasm_state='fixed',
germplasm_name_type='derivative_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR103359-B-BRGA',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 103359-B-B RGA;IR 103359-B-B RGA1;IR 103359-B-B RGA2',
germplasm_uuid='dca55ef6-19a8-43cb-b44f-382ccdb38b9d'
WHERE designation = 'IR 103359-B-B RGA';

UPDATE germplasm.germplasm
SET
designation='IR 103359-B-B RGA-B RGA',
parentage='IR10F365/IR 72 ',
generation='F4',
germplasm_state='fixed',
germplasm_name_type='derivative_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR103359-B-BRGA-BRGA',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 103359-B-B RGA-B RGA',
germplasm_uuid='66fac0a4-5a17-404b-ad0e-a225aa89fb74'
WHERE designation = 'IR 103359-B-B RGA-B RGA';

UPDATE germplasm.germplasm
SET
designation='IR 103359-B-B RGA-B RGA-B RGA',
parentage='IR10F365/IR 72 ',
generation='F5',
germplasm_state='fixed',
germplasm_name_type='derivative_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR103359-B-BRGA-BRGA-BRGA',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 103359-B-B RGA-B RGA-B RGA',
germplasm_uuid='1930d264-e00d-48bd-bbf6-2b37941c2d3e'
WHERE designation = 'IR 103359-B-B RGA-B RGA-B RGA';

UPDATE germplasm.germplasm
SET
designation='IR 103324-B-B RGA',
parentage='PR 37866-1B-1-4/IR08N210 ',
generation='F3',
germplasm_state='fixed',
germplasm_name_type='derivative_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR103324-B-BRGA',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 103324-B-B RGA',
germplasm_uuid='1f9e829c-47b4-4a58-978a-010489d77d34'
WHERE designation = 'IR 103324-B-B RGA';

UPDATE germplasm.germplasm
SET
designation='IR 103324-B-B RGA-B RGA',
parentage='PR 37866-1B-1-4/IR08N210 ',
generation='F4',
germplasm_state='fixed',
germplasm_name_type='derivative_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR103324-B-BRGA-BRGA',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 103324-B-B RGA-B RGA',
germplasm_uuid='aedbe11c-1676-4cc3-b43d-d147e406d844'
WHERE designation = 'IR 103324-B-B RGA-B RGA';

UPDATE germplasm.germplasm
SET
designation='IR 103324-B-B RGA-B RGA-B RGA',
parentage='PR 37866-1B-1-4/IR08N210 ',
generation='F5',
germplasm_state='fixed',
germplasm_name_type='derivative_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR103324-B-BRGA-BRGA-BRGA',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 103324-B-B RGA-B RGA-B RGA',
germplasm_uuid='965158c9-acb7-41a4-84e4-596e7fd0be1a'
WHERE designation = 'IR 103324-B-B RGA-B RGA-B RGA';

UPDATE germplasm.germplasm
SET
designation='IR 99101-B RGA',
parentage='IRRI 154*2/IR05N160 ',
generation='F3',
germplasm_state='fixed',
germplasm_name_type='derivative_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR99101-BRGA',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 99101-B RGA',
germplasm_uuid='9f6cfc6f-3423-4e0a-bb2a-f3dd60f8b8a5'
WHERE designation = 'IR 99101-B RGA';

UPDATE germplasm.germplasm
SET
designation='IR 99101-B RGA-B RGA',
parentage='IRRI 154*2/IR05N160 ',
generation='F4',
germplasm_state='fixed',
germplasm_name_type='derivative_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR99101-BRGA-BRGA',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 99101-B RGA-B RGA',
germplasm_uuid='4ffa7d1e-44f2-48b8-849f-5b43c38bd2b7'
WHERE designation = 'IR 99101-B RGA-B RGA';

UPDATE germplasm.germplasm
SET
designation='IR 99101-B RGA-B RGA-B RGA',
parentage='IRRI 154*2/IR05N160 ',
generation='F5',
germplasm_state='fixed',
germplasm_name_type='derivative_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR99101-BRGA-BRGA-BRGA',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 99101-B RGA-B RGA-B RGA',
germplasm_uuid='cda9ed41-be93-43f4-a6a4-3cf332fc320c'
WHERE designation = 'IR 99101-B RGA-B RGA-B RGA';

UPDATE germplasm.germplasm
SET
designation='IR 100089-B-B RGA',
parentage='IR09N542/MILYANG 23 ',
generation='F3',
germplasm_state='fixed',
germplasm_name_type='derivative_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR100089-B-BRGA',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 100089-B-B RGA',
germplasm_uuid='e9854cf1-5870-40ac-88ab-faa01651c97b'
WHERE designation = 'IR 100089-B-B RGA';

UPDATE germplasm.germplasm
SET
designation='IR 100089-B-B RGA-B RGA',
parentage='IR09N542/MILYANG 23 ',
generation='F4',
germplasm_state='fixed',
germplasm_name_type='derivative_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR100089-B-BRGA-BRGA',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 100089-B-B RGA-B RGA',
germplasm_uuid='7d85ca2a-a880-4a48-adff-3bce114fef85'
WHERE designation = 'IR 100089-B-B RGA-B RGA';

UPDATE germplasm.germplasm
SET
designation='IR 100089-B-B RGA-B RGA-B RGA',
parentage='IR09N542/MILYANG 23 ',
generation='F5',
germplasm_state='fixed',
germplasm_name_type='derivative_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR100089-B-BRGA-BRGA-BRGA',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 100089-B-B RGA-B RGA-B RGA',
germplasm_uuid='ab246738-a9c6-4c52-92e7-1d7573854eea'
WHERE designation = 'IR 100089-B-B RGA-B RGA-B RGA';

UPDATE germplasm.germplasm
SET
designation='IR29SE1001',
parentage='IR09N542/MILYANG 23',
generation='F6',
germplasm_state='fixed',
germplasm_name_type='line_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR29SE1001',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR29SE1001;IR 100089-B-B RGA-B RGA-B RGA-B RGA',
germplasm_uuid='ebe5b441-057e-4cdb-9583-4d2f06b560ca'
WHERE designation = 'IR 100089-B-B RGA-B RGA-B RGA-B RGA';

UPDATE germplasm.germplasm
SET
designation='IR29SE1003',
parentage='IR10F365/IR 72',
generation='F6',
germplasm_state='fixed',
germplasm_name_type='line_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR29SE1003',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR29SE1003;IR 103359-B-B RGA-B RGA-B RGA-B RGA',
germplasm_uuid='e4f1009b-7789-4b03-b42f-427297958926'
WHERE designation = 'IR 103359-B-B RGA-B RGA-B RGA-B RGA';

UPDATE germplasm.germplasm
SET
designation='IR29SE1005',
parentage='IRRI 149/COLOMBIA XXI',
generation='F6',
germplasm_state='fixed',
germplasm_name_type='line_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR29SE1005',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 103353-B-B RGA-B RGA-B RGA-B RGA;IR29SE1005',
germplasm_uuid='4e8d5cbc-061b-4b79-9d20-41da472baa07'
WHERE designation = 'IR 103353-B-B RGA-B RGA-B RGA-B RGA';

UPDATE germplasm.germplasm
SET
designation='IR29SE1002',
parentage='IR09N542/MILYANG 23',
generation='F7',
germplasm_state='fixed',
germplasm_name_type='line_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR29SE1002',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 100089-B-B RGA-B RGA-B RGA-B RGA-B RGA;IR29SE1002',
germplasm_uuid='5568afce-8abe-4736-93cb-b009f57e15da'
WHERE designation = 'IR 100089-B-B RGA-B RGA-B RGA-B RGA-B RGA';

UPDATE germplasm.germplasm
SET
designation='IR29SE1004',
parentage='IR10F365/IR 72',
generation='F7',
germplasm_state='fixed',
germplasm_name_type='line_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR29SE1004',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 103359-B-B RGA-B RGA-B RGA-B RGA-B RGA;IR29SE1004',
germplasm_uuid='6a90d53b-09f0-46b0-a68c-e1b12cc035b1'
WHERE designation = 'IR 103359-B-B RGA-B RGA-B RGA-B RGA-B RGA';

UPDATE germplasm.germplasm
SET
designation='IR29SE1006',
parentage='IRRI 149/COLOMBIA XXI',
generation='F7',
germplasm_state='fixed',
germplasm_name_type='line_name',
crop_id = (SELECT id FROM tenant.crop WHERE crop_code='RICE'),
taxonomy_id = (SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa'),
product_profile_id=NULL,
germplasm_normalized_name='IR29SE1006',
creator_id = (SELECT id FROM tenant.person WHERE username='admin'),
is_void='false',
notes='',
germplasm_type='progeny',
other_names='IR 103353-B-B RGA-B RGA-B RGA-B RGA-B RGA;IR29SE1006',
germplasm_uuid='7cb82663-3a01-4ada-a043-3341bf288d17'
WHERE designation = 'IR 103353-B-B RGA-B RGA-B RGA-B RGA-B RGA';



--rollback SELECT NULL;