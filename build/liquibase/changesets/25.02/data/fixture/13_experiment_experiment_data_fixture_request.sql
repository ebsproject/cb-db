--liquibase formatted sql

--changeset postgres:populate_experiment_experiment_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-4530 Populate experiment_data connected to experiment TestingData2_NewCB-UI



INSERT INTO experiment.experiment_data
(experiment_id,variable_id,data_value,data_qc_code,protocol_id,is_void,notes,creator_id)
 VALUES 
((SELECT id FROM experiment.experiment WHERE experiment_name ='TestingData2_NewCB-UI' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='TRAIT_PROTOCOL_LIST_ID' LIMIT 1),313390,'N',(SELECT id FROM tenant.protocol WHERE protocol_code= (SELECT 'TRAIT_PROTOCOL_'||experiment_code FROM experiment.experiment WHERE experiment_name = 'TestingData2_NewCB-UI') LIMIT 1),False,NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1)),
((SELECT id FROM experiment.experiment WHERE experiment_name ='TestingData2_NewCB-UI' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='MANAGEMENT_PROTOCOL_LIST_ID' LIMIT 1),313391,'N',(SELECT id FROM tenant.protocol WHERE protocol_code= (SELECT 'MANAGEMENT_PROTOCOL_'||experiment_code FROM experiment.experiment WHERE experiment_name = 'TestingData2_NewCB-UI') LIMIT 1),False,NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1)),
((SELECT id FROM experiment.experiment WHERE experiment_name ='TestingData1_NewCB-UI' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='TRAIT_PROTOCOL_LIST_ID' LIMIT 1),313390,'N',(SELECT id FROM tenant.protocol WHERE protocol_code= (SELECT 'TRAIT_PROTOCOL_'||experiment_code FROM experiment.experiment WHERE experiment_name = 'TestingData1_NewCB-UI') LIMIT 1),False,NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1)),
((SELECT id FROM experiment.experiment WHERE experiment_name ='TestingData1_NewCB-UI' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='MANAGEMENT_PROTOCOL_LIST_ID' LIMIT 1),313391,'N',(SELECT id FROM tenant.protocol WHERE protocol_code= (SELECT 'MANAGEMENT_PROTOCOL_'||experiment_code FROM experiment.experiment WHERE experiment_name = 'TestingData1_NewCB-UI') LIMIT 1),False,NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1));



--rollback DELETE FROM experiment.experiment_data WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE experiment_name IN  ('TestingData2_NewCB-UI','TestingData1_NewCB-UI'))