--liquibase formatted sql

--changeset postgres:disable_triggers_that_populate_update_document_columns context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-2083 Create a script to disable triggers that populate/update document columns



ALTER TABLE experiment.experiment
DISABLE TRIGGER experiment_update_experiment_document_tgr;

ALTER TABLE experiment.experiment
DISABLE TRIGGER experiment_update_location_document_from_experiment_tgr;

ALTER TABLE experiment.experiment
DISABLE TRIGGER experiment_update_occurrence_document_from_experiment_tgr;

ALTER TABLE experiment.location
DISABLE TRIGGER location_update_location_document_tgr;

ALTER TABLE experiment.location_occurrence_group
DISABLE TRIGGER location_occurrence_group_update_location_document_tgr;

ALTER TABLE experiment.location_occurrence_group
DISABLE TRIGGER location_occurrence_group_update_occurrence_document_tgr;

ALTER TABLE experiment.occurrence
DISABLE TRIGGER occurrence_update_location_document_from_occurrence_tgr;

ALTER TABLE experiment.occurrence
DISABLE TRIGGER occurrence_update_occurrence_document_tgr;

ALTER TABLE germplasm.germplasm
DISABLE TRIGGER germplasm_update_germplasm_document_tgr;

ALTER TABLE germplasm.germplasm
DISABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;

ALTER TABLE germplasm.germplasm_name
DISABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;

ALTER TABLE germplasm.package
DISABLE TRIGGER package_update_package_document_tgr;

ALTER TABLE germplasm.seed
DISABLE TRIGGER seed_update_package_document_from_seed_tgr;

ALTER TABLE master.property
DISABLE TRIGGER property_update_variable_document_from_property_tgr;

ALTER TABLE master.scale
DISABLE TRIGGER scale_update_variable_document_from_scale_tgr;

ALTER TABLE master.variable
DISABLE TRIGGER variable_update_variable_document_tgr;

ALTER TABLE place.geospatial_object
DISABLE TRIGGER geospatial_object_update_location_document_from_field_tgr;

ALTER TABLE place.geospatial_object
DISABLE TRIGGER geospatial_object_update_location_document_from_site_tgr;

ALTER TABLE place.geospatial_object
DISABLE TRIGGER geospatial_object_update_occurrence_document_from_field_tgr;

ALTER TABLE place.geospatial_object
DISABLE TRIGGER geospatial_object_update_occurrence_document_from_site_tgr;

ALTER TABLE platform.application
DISABLE TRIGGER application_update_application_document_tgr;

ALTER TABLE tenant.person
DISABLE TRIGGER person_creator_update_experiment_document_tgr;

ALTER TABLE tenant.person
DISABLE TRIGGER person_steward_update_experiment_document_tgr;

ALTER TABLE tenant.person
DISABLE TRIGGER person_update_location_document_from_creator_tgr;

ALTER TABLE tenant.person
DISABLE TRIGGER person_update_location_document_from_steward_tgr;

ALTER TABLE tenant.person
DISABLE TRIGGER person_update_occurrence_document_from_creator_tgr;

ALTER TABLE tenant.person
DISABLE TRIGGER person_update_person_document_tgr;

ALTER TABLE tenant.project
DISABLE TRIGGER project_update_experiment_document_tgr;

ALTER TABLE tenant.project
DISABLE TRIGGER project_update_occurrence_document_from_project_tgr;

ALTER TABLE tenant.season
DISABLE TRIGGER season_update_experiment_document_tgr;

ALTER TABLE tenant.season
DISABLE TRIGGER season_update_location_document_from_season_tgr;

ALTER TABLE tenant.season
DISABLE TRIGGER season_update_occurrence_document_from_season_tgr;

ALTER TABLE tenant.stage
DISABLE TRIGGER stage_update_experiment_document_tgr;

ALTER TABLE tenant.stage
DISABLE TRIGGER stage_update_occurrence_document_from_stage_tgr;



-- revert changes
--rollback ALTER TABLE experiment.experiment
--rollback ENABLE TRIGGER experiment_update_experiment_document_tgr;
--rollback 
--rollback ALTER TABLE experiment.experiment
--rollback ENABLE TRIGGER experiment_update_location_document_from_experiment_tgr;
--rollback 
--rollback ALTER TABLE experiment.experiment
--rollback ENABLE TRIGGER experiment_update_occurrence_document_from_experiment_tgr;
--rollback 
--rollback ALTER TABLE experiment.location
--rollback ENABLE TRIGGER location_update_location_document_tgr;
--rollback 
--rollback ALTER TABLE experiment.location_occurrence_group
--rollback ENABLE TRIGGER location_occurrence_group_update_location_document_tgr;
--rollback 
--rollback ALTER TABLE experiment.location_occurrence_group
--rollback ENABLE TRIGGER location_occurrence_group_update_occurrence_document_tgr;
--rollback 
--rollback ALTER TABLE experiment.occurrence
--rollback ENABLE TRIGGER occurrence_update_location_document_from_occurrence_tgr;
--rollback 
--rollback ALTER TABLE experiment.occurrence
--rollback ENABLE TRIGGER occurrence_update_occurrence_document_tgr;
--rollback 
--rollback ALTER TABLE germplasm.germplasm
--rollback ENABLE TRIGGER germplasm_update_germplasm_document_tgr;
--rollback 
--rollback ALTER TABLE germplasm.germplasm
--rollback ENABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;
--rollback 
--rollback ALTER TABLE germplasm.germplasm_name
--rollback ENABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
--rollback 
--rollback ALTER TABLE germplasm.package
--rollback ENABLE TRIGGER package_update_package_document_tgr;
--rollback 
--rollback ALTER TABLE germplasm.seed
--rollback ENABLE TRIGGER seed_update_package_document_from_seed_tgr;
--rollback 
--rollback ALTER TABLE master."property"
--rollback ENABLE TRIGGER property_update_variable_document_from_property_tgr;
--rollback 
--rollback ALTER TABLE master.scale
--rollback ENABLE TRIGGER scale_update_variable_document_from_scale_tgr;
--rollback 
--rollback ALTER TABLE master.variable
--rollback ENABLE TRIGGER variable_update_variable_document_tgr;
--rollback 
--rollback ALTER TABLE place.geospatial_object
--rollback ENABLE TRIGGER geospatial_object_update_location_document_from_field_tgr;
--rollback 
--rollback ALTER TABLE place.geospatial_object
--rollback ENABLE TRIGGER geospatial_object_update_location_document_from_site_tgr;
--rollback 
--rollback ALTER TABLE place.geospatial_object
--rollback ENABLE TRIGGER geospatial_object_update_occurrence_document_from_field_tgr;
--rollback 
--rollback ALTER TABLE place.geospatial_object
--rollback ENABLE TRIGGER geospatial_object_update_occurrence_document_from_site_tgr;
--rollback 
--rollback ALTER TABLE platform.application
--rollback ENABLE TRIGGER application_update_application_document_tgr;
--rollback 
--rollback ALTER TABLE tenant.person
--rollback ENABLE TRIGGER person_creator_update_experiment_document_tgr;
--rollback 
--rollback ALTER TABLE tenant.person
--rollback ENABLE TRIGGER person_steward_update_experiment_document_tgr;
--rollback 
--rollback ALTER TABLE tenant.person
--rollback ENABLE TRIGGER person_update_location_document_from_creator_tgr;
--rollback 
--rollback ALTER TABLE tenant.person
--rollback ENABLE TRIGGER person_update_location_document_from_steward_tgr;
--rollback 
--rollback ALTER TABLE tenant.person
--rollback ENABLE TRIGGER person_update_occurrence_document_from_creator_tgr;
--rollback 
--rollback ALTER TABLE tenant.person
--rollback ENABLE TRIGGER person_update_person_document_tgr;
--rollback 
--rollback ALTER TABLE tenant.project
--rollback ENABLE TRIGGER project_update_experiment_document_tgr;
--rollback 
--rollback ALTER TABLE tenant.project
--rollback ENABLE TRIGGER project_update_occurrence_document_from_project_tgr;
--rollback 
--rollback ALTER TABLE tenant.season
--rollback ENABLE TRIGGER season_update_experiment_document_tgr;
--rollback 
--rollback ALTER TABLE tenant.season
--rollback ENABLE TRIGGER season_update_location_document_from_season_tgr;
--rollback 
--rollback ALTER TABLE tenant.season
--rollback ENABLE TRIGGER season_update_occurrence_document_from_season_tgr;
--rollback 
--rollback ALTER TABLE tenant.stage
--rollback ENABLE TRIGGER stage_update_experiment_document_tgr;
--rollback 
--rollback ALTER TABLE tenant.stage
--rollback ENABLE TRIGGER stage_update_occurrence_document_from_stage_tgr;
