--liquibase formatted sql

--changeset postgres:add_parent_child_constraints_to_germplasm_relation context:schema splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2084 Add parent child constraints to germplasm relation



ALTER TABLE germplasm.germplasm_relation
    ADD CONSTRAINT germplasm_relation_parent_child_id_chk
    CHECK (parent_germplasm_id <> child_germplasm_id)
;



--rollback ALTER TABLE germplasm.germplasm_relation
--rollback DROP CONSTRAINT germplasm_relation_parent_child_id_chk;