--liquibase formatted sql

--changeset postgres:populate_modifier_id_and_timestamp_from_transaction_dataset context:schema splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2279 Populate modifier_id and timestamp from transaction_dataset



CREATE OR REPLACE FUNCTION data_terminal.update_modifier_id_and_timestamp_from_transaction_dataset()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
BEGIN
     
    IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
        UPDATE 
            data_terminal.transaction 
        SET 
            modification_timestamp = now(),
            modifier_id = NEW.modifier_id
        WHERE 
            id
        IN
            (
                SELECT 
                    transaction_id
                FROM
                    data_terminal.transaction_dataset td
                WHERE
                    td.id = NEW.id
            );
    END IF;
    
    RETURN NEW;
END;
$function$
;

DROP TRIGGER IF EXISTS update_modifier_id_and_timestamp_from_transaction_dataset_tgr ON data_terminal.transaction_dataset;

CREATE TRIGGER update_modifier_id_and_timestamp_from_transaction_dataset_tgr BEFORE
INSERT OR UPDATE
    ON
    data_terminal.transaction_dataset FOR EACH ROW EXECUTE FUNCTION data_terminal.update_modifier_id_and_timestamp_from_transaction_dataset()
;



--rollback DROP TRIGGER IF EXISTS update_modifier_id_and_timestamp_from_transaction_dataset_tgr ON data_terminal.transaction_dataset;
--rollback DROP FUNCTION IF EXISTS data_terminal.update_modifier_id_and_timestamp_from_transaction_dataset();
