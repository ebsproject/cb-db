--liquibase formatted sql

--changeset postgres:add_parent_child_constraints_to_seed_relation context:schema splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2084 Add parent child constraints to seed relation



ALTER TABLE germplasm.seed_relation
    ADD CONSTRAINT seed_relation_parent_child_id_chk
    CHECK (parent_seed_id <> child_seed_id)
;



--rollback ALTER TABLE germplasm.seed_relation
--rollback DROP CONSTRAINT seed_relation_parent_child_id_chk;