--liquibase formatted sql

--changeset postgres:populate_formula_parameter_order_number context:schema splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2162 Create and populate order_number column in the master formula_parameter table



-- add order_number column
ALTER TABLE master.formula_parameter 
ADD COLUMN order_number int;

--  create function
CREATE OR REPLACE
FUNCTION master.populate_order_number_for_master_formula_parameter()
    RETURNS VOID
    LANGUAGE plpgsql
AS $function$
BEGIN
WITH formula_parameter_order_number_cte AS
(
    SELECT
        sf.id AS formula_id,
        var.id AS variable_id,
        fpo.ordinal_position AS order_number
    FROM
        (
        SELECT
            routines.routine_name,
            parameters.parameter_name,
            parameters.ordinal_position
        FROM
            information_schema.routines
        LEFT JOIN information_schema.parameters ON
            routines.specific_name = parameters.specific_name
        WHERE
            routines.specific_schema = 'master'
        ORDER BY
            routines.routine_name,
            parameters.ordinal_position
    ) AS fpo
    LEFT JOIN (
        SELECT
            id,
            SUBSTRING(
            function_name 
            FROM 
            POSITION('.' IN function_name)+ 1 FOR POSITION('(' IN function_name) - POSITION('.' IN function_name)-1
        ) AS subs
        FROM
            master.formula
    ) AS sf ON
        fpo.routine_name = sf.subs
    LEFT JOIN (
        SELECT
            *
        FROM
            master.variable v
    ) AS var ON
        fpo.parameter_name = LOWER(var.abbrev)
    ORDER BY
        sf.id ASC,
        fpo.ordinal_position
)
UPDATE
    master.formula_parameter
SET
    order_number = formula_parameter_order_number_cte.order_number
FROM
    formula_parameter_order_number_cte
WHERE
    master.formula_parameter.formula_id = formula_parameter_order_number_cte.formula_id
AND
    master.formula_parameter.param_variable_id = formula_parameter_order_number_cte.variable_id;
END;

$function$
;
-- run the function
SELECT
    master.populate_order_number_for_master_formula_parameter();



--rollback DROP FUNCTION master.populate_order_number_for_master_formula_parameter();
--rollback ALTER TABLE master.formula_parameter  DROP COLUMN order_number;