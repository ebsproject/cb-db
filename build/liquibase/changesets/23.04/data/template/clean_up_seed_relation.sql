--liquibase formatted sql

--changeset postgres:clean_up_seed_relation context:template splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2084: Clean up seed_relation



DELETE FROM
    germplasm.seed_relation
WHERE
    parent_seed_id = child_seed_id
;



--rollback SELECT NULL;