--liquibase formatted sql

--changeset postgres:clean_up_germplasm_relation context:template splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2084: Clean up germplasm_relation



DELETE FROM
    germplasm.germplasm_relation
WHERE
    parent_germplasm_id = child_germplasm_id
;



--rollback SELECT NULL;