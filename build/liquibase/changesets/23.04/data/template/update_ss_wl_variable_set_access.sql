--liquibase formatted sql

--changeset postgres:update_ss_wl_variable_set_access context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5408 SS: Download of seed and package templates failing from Seed Search Working List for non-admin users



UPDATE
    master.variable_set
SET
    access_type = $$PUBLIC$$
WHERE
    abbrev IN ('SEED_INVENTORY_SEED_METADATA','SEED_INVENTORY_PACKAGE_METADATA');



--rollback UPDATE
--rollback     master.variable_set
--rollback SET
--rollback     access_type = null
--rollback WHERE
--rollback     abbrev IN ('SEED_INVENTORY_SEED_METADATA','SEED_INVENTORY_PACKAGE_METADATA');