--liquibase formatted sql

--changeset postgres:add_unique_constraint_to_abbrev_in_platform.list context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-554 Add unique constraint to abbrev in platform.list



ALTER TABLE
    platform.list
ADD CONSTRAINT 
    list_abbrev_unq UNIQUE (abbrev);

COMMENT ON CONSTRAINT messages_message_unq ON api.messages
    IS 'Platform lists can be retrieved easily using its unique abbrev.';



--rollback ALTER TABLE platform.list
--rollback DROP CONSTRAINT list_abbrev_unq;