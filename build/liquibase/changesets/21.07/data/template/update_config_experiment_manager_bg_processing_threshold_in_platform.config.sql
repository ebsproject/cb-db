--liquibase formatted sql

--changeset postgres:update_config_experiment_manager_bg_processing_threshold_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-585 Update config EXPERIMENT_MANAGER_BG_PROCESSING_THRESHOLD in platform.config



UPDATE
    platform.config
SET
    config_value = 
    '
        {
            "saveListMembers": {
                "size": "1000",
                "description": "Save list members"
            },
            "generateLocation": {
                "size": "1000",
                "description": "Generate location for an Occurrence"
            },
            "uploadPlantingArrays": {
                "size": "1000",
                "description": "Upload planting arrays for an Occurrence"
            },
            "selectOccurrences": {
                "size": "300",
                "description": "Limit for selecting Occurrences across pages"
            }
        }
    '
WHERE
    abbrev = 'EXPERIMENT_MANAGER_BG_PROCESSING_THRESHOLD';



--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "saveListMembers": {
--rollback                 "size": "1000",
--rollback                 "description": "Save list members"
--rollback             },
--rollback             "generateLocation": {
--rollback                 "size": "1000",
--rollback                 "description": "Generate location for an Occurrence"
--rollback             },
--rollback             "uploadPlantingArrays": {
--rollback                 "size": "1000",
--rollback                 "description": "Upload planting arrays for an Occurrence"
--rollback             }
--rollback         }
--rollback     '
--rollback WHERE
--rollback     abbrev = 'EXPERIMENT_MANAGER_BG_PROCESSING_THRESHOLD';