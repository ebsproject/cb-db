--liquibase formatted sql

--changeset postgres:insert_experiments context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DB-584 Insert experiments



--# experiment.experiment

-- insert 52,000 records (eta: ~8.528s)
INSERT INTO
    experiment.experiment (
        program_id,
        pipeline_id,
        stage_id,
        project_id,
        experiment_year,
        season_id,
        planting_season,
        experiment_code,
        experiment_name,
        experiment_type,
        experiment_sub_type,
        experiment_sub_sub_type,
        experiment_design_type,
        experiment_status,
        steward_id,
        crop_id,
        data_process_id,
        creator_id,
        description,
        notes
    )
SELECT
    prog.id AS program_id,
    pipe.id AS pipeline_id,
    stg.id AS stage_id,
    proj.id AS project_id,
    expt.experiment_year::integer AS experiment_year,
    ssn.id AS season_id,
    (expt.experiment_year || ssn.season_code) AS planting_season,
    experiment.generate_code('experiment') AS experiment_code,
    concat_ws('-', prog.program_code, stg.stage_code, expt.experiment_year, ssn.season_code, lpad(gs.experiment_number::text, 3, '0')) AS experiment_name,
    expt.experiment_type AS experiment_type,
    expt.experiment_sub_type AS experiment_sub_type,
    expt.experiment_sub_sub_type AS experiment_sub_sub_type,
    expt.experiment_design_type AS experiment_design_type,
    expt.experiment_status AS experiment_status,
    stwd.id AS steward_id,
    crop.id AS crop_id,
    dproc.id AS data_process_id,
    crtr.id AS creator_id,
    (expt.notes || ' ' || expt.description) AS description,
    expt.notes AS notes
FROM
    generate_series(1, 6500) AS gs (experiment_number),
    (
        VALUES
        ('WTP', NULL, 'OYT', NULL, '2018', 'DS', NULL, 'Breeding Trial', NULL, NULL, 'RCBD', 'planted', 'a.carlson', 'WHEAT', 'BREEDING_TRIAL_DATA_PROCESS', 'a.carlson', 'Generate large wheat experiment test data', 'DB-584'),
        ('WTP', NULL, 'PYT', NULL, '2018', 'WS', NULL, 'Breeding Trial', NULL, NULL, 'RCBD', 'planted', 'a.carlson', 'WHEAT', 'BREEDING_TRIAL_DATA_PROCESS', 'a.carlson', 'Generate large wheat experiment test data', 'DB-584'),
        ('WTP', NULL, 'AYT', NULL, '2019', 'DS', NULL, 'Breeding Trial', NULL, NULL, 'RCBD', 'planted', 'a.carlson', 'WHEAT', 'BREEDING_TRIAL_DATA_PROCESS', 'a.carlson', 'Generate large wheat experiment test data', 'DB-584'),
        ('WTP', NULL, 'OYT', NULL, '2019', 'WS', NULL, 'Breeding Trial', NULL, NULL, 'RCBD', 'planted', 'h.tamsin', 'WHEAT', 'BREEDING_TRIAL_DATA_PROCESS', 'h.tamsin', 'Generate large wheat experiment test data', 'DB-584'),
        ('WTP', NULL, 'PYT', NULL, '2020', 'DS', NULL, 'Breeding Trial', NULL, NULL, 'RCBD', 'planted', 'h.tamsin', 'WHEAT', 'BREEDING_TRIAL_DATA_PROCESS', 'h.tamsin', 'Generate large wheat experiment test data', 'DB-584'),
        ('WTP', NULL, 'AYT', NULL, '2020', 'WS', NULL, 'Breeding Trial', NULL, NULL, 'RCBD', 'planted', 'h.tamsin', 'WHEAT', 'BREEDING_TRIAL_DATA_PROCESS', 'h.tamsin', 'Generate large wheat experiment test data', 'DB-584'),
        ('WTP', NULL, 'OYT', NULL, '2021', 'DS', NULL, 'Breeding Trial', NULL, NULL, 'RCBD', 'planted', 'k.khadija', 'WHEAT', 'BREEDING_TRIAL_DATA_PROCESS', 'k.khadija', 'Generate large wheat experiment test data', 'DB-584'),
        ('WTP', NULL, 'PYT', NULL, '2021', 'WS', NULL, 'Breeding Trial', NULL, NULL, 'RCBD', 'planted', 'k.khadija', 'WHEAT', 'BREEDING_TRIAL_DATA_PROCESS', 'k.khadija', 'Generate large wheat experiment test data', 'DB-584')
    ) AS expt (
        program,
        pipeline,
        stage,
        project,
        experiment_year,
        season,
        planting_season,
        experiment_type,
        experiment_sub_type,
        experiment_sub_sub_type,
        experiment_design_type,
        experiment_status,
        steward,
        crop,
        data_process,
        creator,
        description,
        notes
    )
    JOIN tenant.program AS prog
        ON prog.program_code = expt.program
    LEFT JOIN tenant.pipeline AS pipe
        ON pipe.pipeline_code = expt.pipeline
    JOIN tenant.stage AS stg
        ON stg.stage_code = expt.stage
    LEFT JOIN tenant.project AS proj
        ON proj.project_code = expt.project
    JOIN tenant.season AS ssn
        ON ssn.season_code = expt.season
    JOIN tenant.person AS stwd
        ON stwd.username = expt.steward
    JOIN tenant.crop AS crop
        ON crop.crop_code = expt.crop
    JOIN master.item AS dproc
        ON dproc.abbrev = expt.data_process
    JOIN tenant.person AS crtr
        ON crtr.username = expt.creator
ORDER BY
    expt.experiment_year,
    expt.season,
    gs.experiment_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment AS expt
--rollback WHERE
--rollback     expt.notes LIKE 'DB-584%'
--rollback ;
--rollback 
--rollback -- total eta: ~8.392s



--changeset postgres:insert_protocols context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DB-584 Insert protocols



--# tenant.protocol

-- insert 208,000 records (eta: ~10.446s)
INSERT INTO
    tenant.protocol (
        protocol_code,
        protocol_name,
        protocol_type,
        program_id,
        creator_id
    )
SELECT
    (protocol_code_prefix || '_' || expt.experiment_code) AS protocol_code,
    (protocol_name_prefix || ' ' ||  initcap(expt.experiment_code)) AS protocol_name,
    prot.protocol_type,
    expt.program_id,
    expt.creator_id
FROM (
        VALUES
        ('TRAIT_PROTOCOL', 'Trait Protocol', 'trait'),
        ('MANAGEMENT_PROTOCOL', 'Management Protocol', 'management'),
        ('PLANTING_PROTOCOL', 'Planting Protocol', 'planting'),
        ('HARVEST_PROTOCOL', 'Harvest Protocol', 'harvest')
    ) AS prot (
        protocol_code_prefix,
        protocol_name_prefix,
        protocol_type
    ),
    experiment.experiment AS expt
WHERE
    expt.notes LIKE 'DB-584%'
ORDER BY
    expt.id
;



-- revert changes
--rollback --ALTER TABLE tenant.protocol DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'tenant.protocol'::regclass::oid AND indisready = TRUE;
--rollback 
--rollback -- delete 208,000 records (eta: ~13.570s)
--rollback DELETE FROM
--rollback     tenant.protocol AS prot
--rollback USING (
--rollback         SELECT
--rollback             (protocol_code_prefix || '_' || expt.experiment_code) AS protocol_code
--rollback         FROM (
--rollback                 VALUES
--rollback                 ('TRAIT_PROTOCOL'),
--rollback                 ('MANAGEMENT_PROTOCOL'),
--rollback                 ('PLANTING_PROTOCOL'),
--rollback                 ('HARVEST_PROTOCOL')
--rollback             ) AS prot (
--rollback                 protocol_code_prefix
--rollback             )
--rollback             JOIN experiment.experiment AS expt
--rollback                 ON expt.notes LIKE 'DB-584%'
--rollback     ) AS t
--rollback WHERE
--rollback     t.protocol_code = prot.protocol_code
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'tenant.protocol'::regclass::oid AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~1.507s)
--rollback REINDEX TABLE tenant.protocol;
--rollback 
--rollback --ALTER TABLE tenant.protocol ENABLE TRIGGER ALL;
--rollback 
--rollback -- total eta: ~15.077s



--changeset postgres:insert_experiment_data_part_1 context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DB-584 Insert experiment data - part 1



--# experiment.experiment_data

-- disable triggers and constraints to speed up data insertions
--ALTER TABLE experiment.experiment_data DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.experiment_data'::regclass::oid AND indisready = TRUE;

-- insert 676,000 records (eta: ~2.846s)
INSERT INTO
    experiment.experiment_data (
        experiment_id,
        variable_id,
        data_value,
        data_qc_code,
        protocol_id,
        creator_id
    )
SELECT
    expt.id AS experiment_id,
    var.id AS variable_id,
    exptdata.data_value AS data_value,
    exptdata.data_qc_code AS data_qc_code,
    prot.id AS protocol_id,
    expt.creator_id AS creator_id
FROM
    (
        VALUES
        ('ESTABLISHMENT', 'transplanted', 'N', 'PLANTING_PROTOCOL'),
        ('PLANTING_TYPE', 'Flat', 'N', 'PLANTING_PROTOCOL'),
        ('PLOT_TYPE', '6R', 'N', 'PLANTING_PROTOCOL'),
        ('ROWS_PER_PLOT_CONT', '6', 'N', 'PLANTING_PROTOCOL'),
        ('DIST_BET_ROWS', '20', 'N', 'PLANTING_PROTOCOL'),
        ('PLOT_WIDTH', '1.2', 'N', 'PLANTING_PROTOCOL'),
        ('PLOT_LN', '20', 'N', 'PLANTING_PROTOCOL'),
        ('PLOT_AREA_2', '24', 'N', 'PLANTING_PROTOCOL'),
        ('ALLEY_LENGTH', '1', 'N', 'PLANTING_PROTOCOL'),
        ('SEEDING_RATE', 'Normal', 'N', 'PLANTING_PROTOCOL'),
        ('HV_METH_DISC', 'Bulk', 'N', 'HARVEST_PROTOCOL'),
        ('FIRST_PLOT_POSITION_VIEW', 'Top Left', 'N', NULL),
        ('PROTOCOL_TARGET_LEVEL', 'occurrence', 'N', NULL)
    ) AS exptdata (
        variable,
        data_value,
        data_qc_code,
        protocol_code_prefix
    )
    JOIN experiment.experiment AS expt
        ON expt.notes LIKE 'DB-584%'
    JOIN master.variable AS var
        ON var.abbrev = exptdata.variable
    LEFT JOIN tenant.protocol AS prot
        ON prot.protocol_code = (exptdata.protocol_code_prefix || '_' || expt.experiment_code)
ORDER BY
    expt.id,
    prot.id,
    var.id
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.experiment_data'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~2.66s)
-- REINDEX TABLE experiment.experiment_data;

-- restore triggers and constraints
--ALTER TABLE experiment.experiment_data ENABLE TRIGGER ALL;
SET session_replication_role = origin;

-- total eta: ~5.506s



-- revert changes
--rollback --ALTER TABLE experiment.experiment_data DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.experiment_data'::regclass::oid AND indisready = TRUE;
--rollback 
--rollback -- delete 676,000 records (eta: ~2.713s)
--rollback DELETE FROM
--rollback     experiment.experiment_data AS exptdata
--rollback USING
--rollback     experiment.experiment AS expt,
--rollback     master.variable AS var
--rollback WHERE
--rollback     expt.id = exptdata.experiment_id
--rollback     AND var.id = exptdata.variable_id
--rollback     AND expt.notes LIKE 'DB-584%'
--rollback     AND var.abbrev IN (
--rollback         'ESTABLISHMENT',
--rollback         'PLANTING_TYPE',
--rollback         'PLOT_TYPE',
--rollback         'ROWS_PER_PLOT_CONT',
--rollback         'DIST_BET_ROWS',
--rollback         'PLOT_WIDTH',
--rollback         'PLOT_LN',
--rollback         'PLOT_AREA_2',
--rollback         'ALLEY_LENGTH',
--rollback         'SEEDING_RATE',
--rollback         'HV_METH_DISC',
--rollback         'FIRST_PLOT_POSITION_VIEW',
--rollback         'PROTOCOL_TARGET_LEVEL'
--rollback     )
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.experiment_data'::regclass::oid AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~0.66s)
--rollback REINDEX TABLE experiment.experiment_data;
--rollback 
--rollback --ALTER TABLE experiment.experiment_data ENABLE TRIGGER ALL;
--rollback 
--rollback -- total eta: ~3.373s



--changeset postgres:insert_experiment_protocol_lists context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DB-584 Insert experiment protocol lists



--# platform.list

-- insert 104,000 records (eta: ~4.622s)
INSERT INTO
    platform.list (
        abbrev,
        name,
        display_name,
        type,
        entity_id,
        creator_id,
        list_usage,
        status,
        is_active
    )
SELECT
    (list.list_code_prefix || '_' || expt.experiment_code) AS abbrev,
    (expt.experiment_name || ' ' || list.list_name_prefix || ' (' || expt.experiment_code || ')') AS name,
    (expt.experiment_name || ' ' || list.list_name_prefix || ' (' || expt.experiment_code || ')') AS display_name,
    list.list_type AS type,
    entity.id AS entity_id,
    expt.creator_id,
    'working list' AS list_usage,
    'created' AS status,
    TRUE AS is_active
FROM (
        VALUES
        ('TRAIT_PROTOCOL', 'Trait Protocol', 'trait protocol', 'TRAIT'),
        ('MANAGEMENT_PROTOCOL', 'Management Protocol', 'management protocol', 'MANAGEMENT')
    ) AS list (
        list_code_prefix,
        list_name_prefix,
        list_type,
        entity
    )
    JOIN experiment.experiment AS expt
        ON expt.notes LIKE 'DB-584%'
    JOIN dictionary.entity AS entity
        ON entity.abbrev = list.entity
;



-- revert changes
--rollback --ALTER TABLE platform.list DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'platform.list'::regclass::oid AND indisready = TRUE;
--rollback 
--rollback -- delete 104,000 records (eta: ~TIMEs)
--rollback DELETE FROM
--rollback     platform.list AS list
--rollback USING (
--rollback         SELECT
--rollback             (list_code_prefix || '_' || expt.experiment_code) AS list_code
--rollback         FROM (
--rollback                 VALUES
--rollback                 ('TRAIT_PROTOCOL'),
--rollback                 ('MANAGEMENT_PROTOCOL')
--rollback             ) AS list (
--rollback                 list_code_prefix
--rollback             )
--rollback             JOIN experiment.experiment AS expt
--rollback                 ON expt.notes LIKE 'DB-584%'
--rollback     ) AS t
--rollback WHERE
--rollback     t.list_code = list.abbrev
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'platform.list'::regclass::oid AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~TIMEs)
--rollback REINDEX TABLE platform.list;
--rollback 
--rollback --ALTER TABLE platform.list ENABLE TRIGGER ALL;
--rollback 
--rollback -- total eta: ~TIMEs



--changeset postgres:insert_experiment_protocol_list_variables context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DB-584 Insert experiment protocol list variables



--# platform.list_member

-- disable triggers and constraints to speed up data insertions
--ALTER TABLE platform.list_member DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'platform.list_member'::regclass::oid AND indisready = TRUE;

-- insert 1,196,000 records (eta: ~8.675s)
INSERT INTO
    platform.list_member (
        list_id,
        data_id,
        order_number,
        display_value,
        is_active,
        creator_id
    )
WITH t_list AS (
    SELECT
        (list_code_prefix || '_' || expt.experiment_code) AS list_code
    FROM (
            VALUES
            ('TRAIT_PROTOCOL'),
            ('MANAGEMENT_PROTOCOL')
        ) AS list (
            list_code_prefix
        )
        JOIN experiment.experiment AS expt
            ON expt.notes LIKE 'DB-584%'
)
SELECT
    list.id AS list_id,
    var.id AS data_id,
    ROW_NUMBER() OVER (PARTITION BY list.id, listmem.list_type) AS order_number,
    var.label AS display_value,
    TRUE AS is_active,
    list.creator_id
FROM (
        VALUES
        ('TRAIT_PROTOCOL', 'AYLD_CONT'),
        ('TRAIT_PROTOCOL', 'BB_SES5_GH_SCOR_1_9'),
        ('TRAIT_PROTOCOL', 'FLW_DATE_CONT'),
        ('TRAIT_PROTOCOL', 'HT1_CONT'),
        ('TRAIT_PROTOCOL', 'HT2_CONT'),
        ('TRAIT_PROTOCOL', 'HT3_CONT'),
        ('TRAIT_PROTOCOL', 'HT_CAT'),
        ('TRAIT_PROTOCOL', 'HVDATE_CONT'),
        ('TRAIT_PROTOCOL', 'LG_SCOR_1_9'),
        ('TRAIT_PROTOCOL', 'MC_CONT'),
        ('MANAGEMENT_PROTOCOL', 'DIST_BET_ROWS'),
        ('MANAGEMENT_PROTOCOL', 'FERT1_BRAND'),
        ('MANAGEMENT_PROTOCOL', 'FERT1_DATE_CONT'),
        ('MANAGEMENT_PROTOCOL', 'FERT1_METH_DISC'),
        ('MANAGEMENT_PROTOCOL', 'FERT1_TYPE_DISC'),
        ('MANAGEMENT_PROTOCOL', 'FERT2_BRAND'),
        ('MANAGEMENT_PROTOCOL', 'FERT2_DATE_CONT'),
        ('MANAGEMENT_PROTOCOL', 'FERT3_BRAND'),
        ('MANAGEMENT_PROTOCOL', 'FERT3_DATE_CONT'),
        ('MANAGEMENT_PROTOCOL', 'FERT3_METH_DISC'),
        ('MANAGEMENT_PROTOCOL', 'FERT3_TYPE_DISC'),
        ('MANAGEMENT_PROTOCOL', 'ROWS_PER_PLOT_CONT'),
        ('MANAGEMENT_PROTOCOL', 'SEEDING_DATE_CONT')
    ) AS listmem (
        list_type, variable
    )
    JOIN t_list AS t
        ON t.list_code LIKE listmem.list_type || '%'
    JOIN platform.list AS list
        ON list.abbrev = t.list_code
    JOIN master.variable AS var
        ON var.abbrev = listmem.variable
ORDER BY
    list.id,
    order_number
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'platform.list_member'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~4.440s)
-- REINDEX TABLE platform.list_member;

-- restore triggers and constraints
--ALTER TABLE platform.list_member ENABLE TRIGGER ALL;
SET session_replication_role = origin;

-- total eta: ~13.115s



-- revert changes
--rollback --ALTER TABLE platform.list_member DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'platform.list_member'::regclass::oid AND indisready = TRUE;
--rollback 
--rollback -- delete 1,196,000 records (eta: ~3.296s)
--rollback DELETE FROM
--rollback     platform.list_member AS listmem
--rollback USING
--rollback     (
--rollback         SELECT
--rollback             (list_code_prefix || '_' || expt.experiment_code) AS list_code
--rollback         FROM (
--rollback                 VALUES
--rollback                 ('TRAIT_PROTOCOL'),
--rollback                 ('MANAGEMENT_PROTOCOL')
--rollback             ) AS list (
--rollback                 list_code_prefix
--rollback             )
--rollback             JOIN experiment.experiment AS expt
--rollback                 ON expt.notes LIKE 'DB-584%'
--rollback     ) AS t
--rollback     JOIN platform.list AS list
--rollback         ON t.list_code = list.abbrev
--rollback WHERE
--rollback     list.id = listmem.list_id
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'platform.list_member'::regclass::oid AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~0.712s)
--rollback REINDEX TABLE platform.list_member;
--rollback 
--rollback --ALTER TABLE platform.list_member ENABLE TRIGGER ALL;
--rollback 
--rollback -- total eta: ~4.008s



--changeset postgres:insert_experiment_data_part_2 context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DB-584 Insert experiment data - part 2



--# experiment.experiment_data

-- disable triggers and constraints to speed up data insertions
--ALTER TABLE experiment.experiment_data DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.experiment_data'::regclass::oid AND indisready = TRUE;

-- insert 104,000 records (eta: ~1.166s)
INSERT INTO
    experiment.experiment_data (
        experiment_id,
        variable_id,
        data_value,
        data_qc_code,
        protocol_id,
        creator_id
    )
WITH t_list AS (
    SELECT
        (list_code_prefix || '_' || expt.experiment_code) AS list_code
    FROM (
            VALUES
            ('TRAIT_PROTOCOL'),
            ('MANAGEMENT_PROTOCOL')
        ) AS list (
            list_code_prefix
        )
        JOIN experiment.experiment AS expt
            ON expt.notes LIKE 'DB-584%'
)
SELECT
    expt.id AS experiment_id,
    var.id AS variable_id,
    list.id::varchar AS data_value,
    exptdata.data_qc_code AS data_qc_code,
    prot.id AS protocol_id,
    expt.creator_id AS creator_id
FROM
    (
        VALUES
        ('TRAIT_PROTOCOL_LIST_ID', NULL, 'N', 'TRAIT_PROTOCOL'),
        ('MANAGEMENT_PROTOCOL_LIST_ID', NULL, 'N', 'MANAGEMENT_PROTOCOL')
    ) AS exptdata (
        variable,
        data_value,
        data_qc_code,
        protocol_code_prefix
    )
    JOIN experiment.experiment AS expt
        ON expt.notes LIKE 'DB-584%'
    JOIN t_list AS t
        ON t.list_code = (exptdata.protocol_code_prefix || '_' || expt.experiment_code)
    JOIN master.variable AS var
        ON var.abbrev = exptdata.variable
    LEFT JOIN tenant.protocol AS prot
        ON prot.protocol_code = t.list_code
    JOIN platform.list AS list
        ON list.abbrev = t.list_code
ORDER BY
    expt.id,
    prot.id,
    var.id
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.experiment_data'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~3.559s)
-- REINDEX TABLE experiment.experiment_data;

-- restore triggers and constraints
--ALTER TABLE experiment.experiment_data ENABLE TRIGGER ALL;
SET session_replication_role = origin;

-- total eta: ~4.725s



-- revert changes
--rollback --ALTER TABLE experiment.experiment_data DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.experiment_data'::regclass::oid AND indisready = TRUE;
--rollback 
--rollback -- delete 104,000 records (eta: ~0.398s)
--rollback DELETE FROM
--rollback     experiment.experiment_data AS exptdata
--rollback USING
--rollback     experiment.experiment AS expt,
--rollback     master.variable AS var
--rollback WHERE
--rollback     expt.id = exptdata.experiment_id
--rollback     AND var.id = exptdata.variable_id
--rollback     AND expt.notes LIKE 'DB-584%'
--rollback     AND var.abbrev IN (
--rollback         'TRAIT_PROTOCOL_LIST_ID',
--rollback         'MANAGEMENT_PROTOCOL_LIST_ID'
--rollback     )
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.experiment_data'::regclass::oid AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~2.803s)
--rollback REINDEX TABLE experiment.experiment_data;
--rollback 
--rollback --ALTER TABLE experiment.experiment_data ENABLE TRIGGER ALL;
--rollback 
--rollback -- total eta: ~3.201s



--changeset postgres:insert_experiment_protocols context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DB-584 Insert experiment protocols



--# experiment.experiment_protocol

-- disable triggers and constraints to speed up data insertions
--ALTER TABLE experiment.experiment_protocol DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.experiment_protocol'::regclass::oid AND indisready = TRUE;

-- insert 208,000 records (eta: ~1.124s)
INSERT INTO
    experiment.experiment_protocol (
        experiment_id,
        protocol_id,
        order_number,
        creator_id
    )
WITH t_protocol AS (
    SELECT
        expt.id AS experiment_id,
        expt.experiment_code,
        expt.experiment_name,
        (protocol_code_prefix || '_' || expt.experiment_code) AS protocol_code,
        expt.creator_id
    FROM (
            VALUES
            ('TRAIT_PROTOCOL'),
            ('MANAGEMENT_PROTOCOL'),
            ('PLANTING_PROTOCOL'),
            ('HARVEST_PROTOCOL')
        ) AS prot (
            protocol_code_prefix
        )
        JOIN experiment.experiment AS expt
            ON expt.notes LIKE 'DB-584%'
)
SELECT
    t.experiment_id,
    prot.id AS protocol_id,
    ROW_NUMBER() OVER (PARTITION BY t.experiment_id) AS order_number,
    t.creator_id
FROM
    t_protocol AS t
    JOIN tenant.protocol AS prot
        ON t.protocol_code = prot.protocol_code
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.experiment_protocol'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~1.48s)
-- REINDEX TABLE experiment.experiment_protocol;

-- restore triggers and constraints
--ALTER TABLE experiment.experiment_protocol ENABLE TRIGGER ALL;
SET session_replication_role = origin;

-- total eta: ~2.604s



-- revert changes
--rollback --ALTER TABLE experiment.experiment_protocol DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.experiment_protocol'::regclass::oid AND indisready = TRUE;
--rollback 
--rollback -- delete 208,000 records (eta: ~1.578s)
--rollback DELETE FROM
--rollback     experiment.experiment_protocol AS exptprot
--rollback USING (
--rollback         SELECT
--rollback             expt.id AS experiment_id,
--rollback             expt.experiment_code,
--rollback             expt.experiment_name,
--rollback             (protocol_code_prefix || '_' || expt.experiment_code) AS protocol_code,
--rollback             expt.creator_id
--rollback         FROM (
--rollback                 VALUES
--rollback                 ('TRAIT_PROTOCOL'),
--rollback                 ('MANAGEMENT_PROTOCOL'),
--rollback                 ('PLANTING_PROTOCOL'),
--rollback                 ('HARVEST_PROTOCOL')
--rollback             ) AS prot (
--rollback                 protocol_code_prefix
--rollback             )
--rollback             JOIN experiment.experiment AS expt
--rollback                 ON expt.notes LIKE 'DB-584%'
--rollback     ) AS t
--rollback     JOIN tenant.protocol AS prot
--rollback         ON t.protocol_code = prot.protocol_code
--rollback WHERE
--rollback     exptprot.protocol_id = prot.id
--rollback     AND t.experiment_id = exptprot.experiment_id
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.experiment_protocol'::regclass::oid AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~0.192s)
--rollback REINDEX TABLE experiment.experiment_protocol;
--rollback 
--rollback --ALTER TABLE experiment.experiment_protocol ENABLE TRIGGER ALL;
--rollback 
--rollback -- total eta: ~1.77s



--changeset postgres:insert_entry_lists context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DB-584 Insert entry lists



--# experiment.entry_list

-- insert 52,000 records (eta: ~5.365s)
INSERT INTO
    experiment.entry_list (
        entry_list_code,
        entry_list_name,
        entry_list_status,
        entry_list_type,
        experiment_id,
        creator_id
    )
SELECT
    (expt.experiment_code || '_ENTLIST') AS entry_list_code,
    (expt.experiment_name || ' Entry List') AS entry_list_name,
    'completed' AS entry_list_status,
    'entry list' AS entry_list_type,
    expt.id AS experiment_id,
    expt.creator_id AS creator_id
FROM
    experiment.experiment AS expt
WHERE
    expt.notes LIKE 'DB-584%'
;



-- revert changes
--rollback --ALTER TABLE experiment.entry_list DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.entry_list'::regclass::oid AND indisready = TRUE;
--rollback 
--rollback -- delete 52,000 records (eta: ~0.133s)
--rollback DELETE FROM
--rollback     experiment.entry_list AS entlist
--rollback USING
--rollback     experiment.experiment AS expt
--rollback WHERE
--rollback     expt.id = entlist.experiment_id
--rollback     AND expt.notes LIKE 'DB-584%'
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.entry_list'::regclass::oid AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~0.105s)
--rollback REINDEX TABLE experiment.entry_list;
--rollback 
--rollback --ALTER TABLE experiment.entry_list ENABLE TRIGGER ALL;
--rollback 
--rollback -- total eta: ~0.238s



--changeset postgres:insert_entries context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DB-584 Insert entries



--# experiment.entry

-- disable trigger and constraints to speed up data insertions
--ALTER TABLE experiment.entry DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.entry'::regclass::oid AND indisready = TRUE;

-- prepare statement to copy entries from another experiment
PREPARE
    copy_entries(
        varchar, varchar
        -- target_experiment_name_fragment, source_experiment_name
    )
AS
    INSERT INTO
        experiment.entry (
            entry_code,
            entry_number,
            entry_name,
            entry_type,
            entry_role,
            entry_class,
            entry_status,
            germplasm_id,
            seed_id,
            package_id,
            entry_list_id,
            creator_id
        )
    SELECT
        t.entry_number AS entry_code,
        t.*,
        entlist.id AS entry_list_id,
        entlist.creator_id
    FROM
        experiment.experiment AS expt
        JOIN experiment.entry_list AS entlist
            ON entlist.experiment_id = expt.id,
        (
            SELECT
                row_number() OVER () AS entry_number,
                t.*
            FROM (
                    SELECT
                        srcent.entry_name,
                        srcent.entry_type,
                        srcent.entry_role,
                        srcent.entry_class,
                        srcent.entry_status,
                        srcent.germplasm_id,
                        srcent.seed_id,
                        srcent.package_id
                    FROM
                        experiment.experiment AS srcexpt
                        JOIN experiment.entry_list AS srcentlist
                            ON srcentlist.experiment_id = srcexpt.id
                        JOIN experiment.entry AS srcent
                            ON srcent.entry_list_id = srcentlist.id
                    WHERE
                        srcexpt.experiment_name = $2
                        AND srcent.entry_number BETWEEN 1 AND 100
                    UNION ALL
                        SELECT
                            srcent.entry_name,
                            srcent.entry_type,
                            srcent.entry_role,
                            srcent.entry_class,
                            srcent.entry_status,
                            srcent.germplasm_id,
                            srcent.seed_id,
                            srcent.package_id
                        FROM
                            experiment.experiment AS srcexpt
                            JOIN experiment.entry_list AS srcentlist
                                ON srcentlist.experiment_id = srcexpt.id
                            JOIN experiment.entry AS srcent
                                ON srcent.entry_list_id = srcentlist.id
                        WHERE
                            srcexpt.experiment_name = $3
                            AND srcent.entry_number BETWEEN 1 AND 100
                    UNION ALL
                        SELECT
                            srcent.entry_name,
                            srcent.entry_type,
                            srcent.entry_role,
                            srcent.entry_class,
                            srcent.entry_status,
                            srcent.germplasm_id,
                            srcent.seed_id,
                            srcent.package_id
                        FROM
                            experiment.experiment AS srcexpt
                            JOIN experiment.entry_list AS srcentlist
                                ON srcentlist.experiment_id = srcexpt.id
                            JOIN experiment.entry AS srcent
                                ON srcent.entry_list_id = srcentlist.id
                        WHERE
                            srcexpt.experiment_name = $4
                            AND srcent.entry_number BETWEEN 1 AND 100
                    UNION ALL
                        SELECT
                            srcent.entry_name,
                            srcent.entry_type,
                            srcent.entry_role,
                            srcent.entry_class,
                            srcent.entry_status,
                            srcent.germplasm_id,
                            srcent.seed_id,
                            srcent.package_id
                        FROM
                            experiment.experiment AS srcexpt
                            JOIN experiment.entry_list AS srcentlist
                                ON srcentlist.experiment_id = srcexpt.id
                            JOIN experiment.entry AS srcent
                                ON srcent.entry_list_id = srcentlist.id
                        WHERE
                            srcexpt.experiment_name = $5
                            AND srcent.entry_number BETWEEN 1 AND 100
                ) AS t
        ) AS t
    WHERE
        expt.experiment_name LIKE $1 || '%'
        AND expt.notes LIKE 'DB-584%'
;


-- insert 1,625,000 records (total eta: ~1m 44s)
EXECUTE copy_entries('WTP-OYT-2018-DS', 'BW-HB-2017-B-001', 'BW-HB-2017-B-002', 'BW-HB-2017-B-003', 'BW-HB-2017-B-004'); -- batch 1 (eta: ~7.331s)
EXECUTE copy_entries('WTP-PYT-2018-WS', 'BW-HB-2017-B-002', 'BW-HB-2017-B-003', 'BW-HB-2017-B-004', 'BW-HB-2017-B-001'); -- batch 2 (eta: ~6.954s)
EXECUTE copy_entries('WTP-AYT-2019-DS', 'BW-HB-2017-B-003', 'BW-HB-2017-B-004', 'BW-HB-2017-B-001', 'BW-HB-2017-B-002'); -- batch 3 (eta: ~6.964s)

-- copy entries for batch 2
EXECUTE copy_entries('WTP-OYT-2019-WS', 'BW-HB-2017-B-004', 'BW-HB-2017-B-001', 'BW-HB-2017-B-002', 'BW-HB-2017-B-003'); -- batch 4 (eta: ~10.876s)
EXECUTE copy_entries('WTP-PYT-2020-DS', 'BW-HB-2017-B-001', 'BW-HB-2017-B-002', 'BW-HB-2017-B-003', 'BW-HB-2017-B-004'); -- batch 5 (eta: ~15.185s)
EXECUTE copy_entries('WTP-AYT-2020-WS', 'BW-HB-2017-B-002', 'BW-HB-2017-B-003', 'BW-HB-2017-B-004', 'BW-HB-2017-B-001'); -- batch 6 (eta: ~17.615s)

-- copy entries for batch 3
EXECUTE copy_entries('WTP-OYT-2021-DS', 'BW-HB-2017-B-003', 'BW-HB-2017-B-004', 'BW-HB-2017-B-001', 'BW-HB-2017-B-002'); -- batch 7 (eta: ~18.757s)
EXECUTE copy_entries('WTP-PYT-2021-WS', 'BW-HB-2017-B-004', 'BW-HB-2017-B-001', 'BW-HB-2017-B-002', 'BW-HB-2017-B-003'); -- batch 8 (eta: ~21.171s)

-- drop prepared statement
DEALLOCATE copy_entries;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.entry'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~2m 36s)
-- REINDEX TABLE experiment.entry;

-- restore triggers and constraints
--ALTER TABLE experiment.entry ENABLE TRIGGER ALL;
SET session_replication_role = origin;

-- total eta: ~4m 20s



-- revert changes
--rollback --ALTER TABLE experiment.entry DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.entry'::regclass::oid AND indisready = TRUE;
--rollback 
--rollback -- delete 13,000,000 records (eta: ~48.661s)
--rollback DELETE FROM
--rollback     experiment.entry AS ent
--rollback USING
--rollback     experiment.entry_list AS entlist
--rollback     JOIN experiment.experiment AS expt
--rollback         ON expt.id = entlist.experiment_id
--rollback WHERE
--rollback     entlist.id = ent.entry_list_id
--rollback     AND expt.notes LIKE 'DB-584%'
--rollback ;
--rollback
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.entry'::regclass::oid AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~28.747s)
--rollback REINDEX TABLE experiment.entry;
--rollback 
--rollback --ALTER TABLE experiment.entry ENABLE TRIGGER ALL;
--rollback 
--rollback -- total eta: ~1m 17s



--changeset postgres:insert_occurrences context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DB-584 Insert occurrences



--# experiment.occurrence

-- disable trigger and constraints to speed up data insertions
--ALTER TABLE experiment.occurrence DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.occurrence'::regclass::oid AND indisready = TRUE;

-- insert 156,000 records (eta: ~0.628s)
INSERT INTO
    experiment.occurrence (
        occurrence_code,
        occurrence_name,
        occurrence_status,
        experiment_id,
        site_id,
        field_id,
        rep_count,
        occurrence_number,
        creator_id
    )
SELECT
    expt.experiment_code || '-' || lpad(geo.occurrence_number::text, 3, '0') AS occurrence_code,
    expt.experiment_name || '-' || lpad(geo.occurrence_number::text, 3, '0') AS occurrence_name,
    occ.occurrence_status,
    expt.id AS experiment_id,
    site.id AS site_id,
    field.id AS field_id,
    occ.rep_count,
    geo.occurrence_number,
    expt.creator_id
FROM
    experiment.experiment AS expt,
    (
        VALUES
        -- a.carlson
        ('WTP-OYT-2018-DS', 'planted', 1),
        ('WTP-PYT-2018-WS', 'planted', 1),
        ('WTP-AYT-2019-DS', 'planted', 1),
        -- h.tamsin
        ('WTP-OYT-2019-WS', 'planted', 1),
        ('WTP-PYT-2020-DS', 'planted', 1),
        ('WTP-AYT-2020-WS', 'planted', 1),
        -- k.khadija
        ('WTP-OYT-2021-DS', 'planted', 1),
        ('WTP-PYT-2021-WS', 'planted', 1)
    ) AS occ (
        experiment_name_fragment,
        occurrence_status,
        rep_count
    ),
    (
        VALUES
        (1, 'EBN', 'EBN_A1'),
        (2, 'CON', 'CON_E1'),
        (3, 'TOA', NULL)
    ) AS geo (
        occurrence_number, site, field
    )
    JOIN place.geospatial_object AS site
        ON site.geospatial_object_code = geo.site
    LEFT JOIN place.geospatial_object AS field
        ON field.geospatial_object_code = geo.field
WHERE
    expt.notes LIKE 'DB-584%'
    AND expt.experiment_name LIKE occ.experiment_name_fragment || '%'
ORDER BY
    expt.id
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.occurrence'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~0.768s)
-- REINDEX TABLE experiment.occurrence;

-- restore triggers and constraints
--ALTER TABLE experiment.occurrence ENABLE TRIGGER ALL;
SET session_replication_role = origin;

-- total eta: ~1.396s



-- revert changes
--rollback --ALTER TABLE experiment.occurrence DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.occurrence'::regclass::oid AND indisready = TRUE;
--rollback 
--rollback -- delete 156,000 records (eta: ~0.230s)
--rollback DELETE FROM
--rollback     experiment.occurrence AS occ
--rollback USING
--rollback     experiment.experiment AS expt
--rollback WHERE
--rollback     expt.id = occ.experiment_id
--rollback     AND expt.notes LIKE 'DB-584%'
--rollback ;
--rollback
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.occurrence'::regclass::oid AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~0.177s)
--rollback REINDEX TABLE experiment.occurrence;
--rollback 
--rollback --ALTER TABLE experiment.occurrence ENABLE TRIGGER ALL;
--rollback 
--rollback -- total eta: ~0.407s



--changeset postgres:insert_planting_areas context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DB-584 Insert planting areas



--# place.geospatial_object

-- disable trigger and constraints to speed up data insertions
--ALTER TABLE place.geospatial_object DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'place.geospatial_object'::regclass::oid AND indisready = TRUE;

-- insert 156,000 records (eta: ~4.429s)
INSERT INTO
    place.geospatial_object (
        geospatial_object_code,
        geospatial_object_name,
        geospatial_object_type,
        geospatial_object_subtype,
        parent_geospatial_object_id,
        root_geospatial_object_id,
        creator_id
    )
WITH t_occ AS (
    SELECT
        expt.id AS experiment_id,
        expt.experiment_year,
        expt.season_id,
        ssn.season_code,
        occ.id AS occurrence_id,
        occ.occurrence_name,
        occ.site_id,
        occ.field_id,
        site.geospatial_object_code AS site_code,
        site.parent_geospatial_object_id AS site_parent_id,
        site.root_geospatial_object_id AS site_root_id,
        occ.creator_id
    FROM
        experiment.occurrence AS occ
        JOIN experiment.experiment AS expt
            ON expt.id = occ.experiment_id
        JOIN tenant.season AS ssn
            ON ssn.id = expt.season_id
        JOIN place.geospatial_object AS site
            ON site.id = occ.site_id
    WHERE
        expt.notes LIKE 'DB-584%'
    ORDER BY
        expt.id,
        occ.id
), t_logrp AS (
    SELECT
        occ.*,
        NTILE(156000) OVER (ORDER BY occ.experiment_year, occ.season_id, occ.site_id) AS location_occurrence_group_number -- total number of planting areas/locations
    FROM
        t_occ AS occ
    ORDER BY
        location_occurrence_group_number
), t_loc1 AS (
    SELECT
        t.location_occurrence_group_number,
        (array_agg(DISTINCT t.experiment_year))[1] AS experiment_year,
        (array_agg(DISTINCT t.season_id))[1] AS season_id,
        (array_agg(DISTINCT t.season_code))[1] AS season_code,
        (array_agg(DISTINCT t.site_id))[1] AS site_id,
        (array_agg(DISTINCT t.site_code))[1] AS site_code,
        (array_agg(DISTINCT t.field_id))[1] AS field_id,
        (array_agg(DISTINCT t.site_parent_id))[1] AS site_parent_id,
        (array_agg(DISTINCT t.site_root_id))[1] AS site_root_id,
        (array_agg(DISTINCT t.creator_id))[1] AS creator_id
    FROM
        t_logrp AS t
    GROUP BY
        t.location_occurrence_group_number
), t_loc2 AS (
    SELECT
        t.experiment_year,
        t.season_id,
        t.season_code,
        t.site_id,
        t.site_code,
        ROW_NUMBER() OVER (PARTITION BY t.experiment_year, t.season_code, t.site_code) AS location_number,
        (array_agg(DISTINCT t.field_id))[1] AS field_id,
        (array_agg(DISTINCT t.site_parent_id))[1] AS site_parent_id,
        (array_agg(DISTINCT t.site_root_id))[1] AS site_root_id,
        (array_agg(DISTINCT t.creator_id))[1] AS creator_id
    FROM
        t_loc1 AS t
    GROUP BY
        t.experiment_year,
        t.season_id,
        t.season_code,
        t.site_id,
        t.site_code,
        t.location_occurrence_group_number % 6500 -- number of locations per breeding season
    ORDER BY
        t.experiment_year,
        t.season_id,
        t.site_id
), t_pa AS (
    SELECT
        concat_ws('-', 'DB-584', t.site_code, t.experiment_year, t.season_code, lpad(t.location_number::TEXT, 6, '0')) AS geospatial_object_code, -- number of digits
        concat_ws('-', t.site_code, t.experiment_year, t.season_code, lpad(t.location_number::TEXT, 6, '0')) AS geospatial_object_name, -- number of digits
        'planting area' AS geospatial_object_type,
        'breeding location' AS geospatial_object_subtype,
        t.site_parent_id AS parent_geospatial_object_id,
        t.site_root_id AS root_geospatial_object_id,
        t.creator_id
    FROM
        t_loc2 AS t
    ORDER BY
        t.experiment_year,
        t.season_id,
        t.site_id,
        t.location_number
)
SELECT
    t.*
FROM
    t_pa AS t
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'place.geospatial_object'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~1.193s)
-- REINDEX TABLE place.geospatial_object;

-- restore triggers and constraints
--ALTER TABLE place.geospatial_object ENABLE TRIGGER ALL;
SET session_replication_role = origin;

-- total eta: ~5.622s



-- revert changes
--rollback --ALTER TABLE place.geospatial_object DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'place.geospatial_object'::regclass::oid AND indisready = TRUE;
--rollback 
--rollback -- delete 156,000 records (eta: ~0.931s)
--rollback DELETE FROM
--rollback     place.geospatial_object AS geo
--rollback WHERE
--rollback     geo.geospatial_object_code LIKE ANY((
--rollback         SELECT
--rollback             array_agg(DISTINCT (concat_ws('-', 'DB-584', site.geospatial_object_code, expt.experiment_year, ssn.season_code) || '-%')::varchar) AS geospatial_object_code_fragments
--rollback         FROM
--rollback             experiment.occurrence AS occ
--rollback             JOIN experiment.experiment AS expt
--rollback                 ON expt.id = occ.experiment_id
--rollback             JOIN tenant.season AS ssn
--rollback                 ON ssn.id = expt.season_id
--rollback             JOIN place.geospatial_object AS site
--rollback                 ON site.id = occ.site_id
--rollback         WHERE
--rollback             expt.notes LIKE 'DB-584%'
--rollback     )::varchar[])
--rollback ;
--rollback
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'place.geospatial_object'::regclass::oid AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~0.258s)
--rollback REINDEX TABLE place.geospatial_object;
--rollback 
--rollback --ALTER TABLE place.geospatial_object ENABLE TRIGGER ALL;
--rollback 
--rollback -- total eta: ~1.189s



--changeset postgres:insert_locations context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DB-584 Insert locations



--# experiment.location

-- disable trigger and constraints to speed up data insertions
--ALTER TABLE experiment.location DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.location'::regclass::oid AND indisready = TRUE;

-- insert 156,000 records (eta: ~7.211s)
INSERT INTO
    experiment.location (
        location_code,
        location_name,
        location_status,
        location_type,
        location_year,
        season_id,
        location_number,
        site_id,
        field_id,
        steward_id,
        creator_id,
        geospatial_object_id
    )
WITH t_occ AS (
    SELECT
        expt.id AS experiment_id,
        expt.experiment_year,
        expt.season_id,
        ssn.season_code,
        occ.id AS occurrence_id,
        occ.occurrence_name,
        occ.site_id,
        occ.field_id,
        site.geospatial_object_code AS site_code,
        site.parent_geospatial_object_id AS site_parent_id,
        site.root_geospatial_object_id AS site_root_id,
        occ.creator_id
    FROM
        experiment.occurrence AS occ
        JOIN experiment.experiment AS expt
            ON expt.id = occ.experiment_id
        JOIN tenant.season AS ssn
            ON ssn.id = expt.season_id
        JOIN place.geospatial_object AS site
            ON site.id = occ.site_id
    WHERE
        expt.notes LIKE 'DB-584%'
    ORDER BY
        expt.id,
        occ.id
), t_logrp AS (
    SELECT
        occ.*,
        NTILE(156000) OVER (ORDER BY occ.experiment_year, occ.season_id, occ.site_id) AS location_occurrence_group_number -- total number of planting areas/locations
    FROM
        t_occ AS occ
    ORDER BY
        location_occurrence_group_number
), t_loc1 AS (
    SELECT
        t.location_occurrence_group_number,
        (array_agg(DISTINCT t.experiment_year))[1] AS experiment_year,
        (array_agg(DISTINCT t.season_id))[1] AS season_id,
        (array_agg(DISTINCT t.season_code))[1] AS season_code,
        (array_agg(DISTINCT t.site_id))[1] AS site_id,
        (array_agg(DISTINCT t.site_code))[1] AS site_code,
        (array_agg(DISTINCT t.field_id))[1] AS field_id,
        (array_agg(DISTINCT t.site_parent_id))[1] AS site_parent_id,
        (array_agg(DISTINCT t.site_root_id))[1] AS site_root_id,
        (array_agg(DISTINCT t.creator_id))[1] AS creator_id
    FROM
        t_logrp AS t
    GROUP BY
        t.location_occurrence_group_number
), t_loc2 AS (
    SELECT
        t.experiment_year,
        t.season_id,
        t.season_code,
        t.site_id,
        t.site_code,
        ROW_NUMBER() OVER (PARTITION BY t.experiment_year, t.season_code, t.site_code) AS location_number,
        (array_agg(DISTINCT t.field_id))[1] AS field_id,
        (array_agg(DISTINCT t.site_parent_id))[1] AS site_parent_id,
        (array_agg(DISTINCT t.site_root_id))[1] AS site_root_id,
        (array_agg(DISTINCT t.creator_id))[1] AS creator_id
    FROM
        t_loc1 AS t
    GROUP BY
        t.experiment_year,
        t.season_id,
        t.season_code,
        t.site_id,
        t.site_code,
        t.location_occurrence_group_number % 6500 -- number of locations per breeding season
    ORDER BY
        t.experiment_year,
        t.season_id,
        t.site_id
), t_loc3 AS (
    SELECT
        concat_ws('-', 'DB-584', t.site_code, t.experiment_year, t.season_code, lpad(t.location_number::TEXT, 6, '0')) AS location_code,
        concat_ws('-', t.site_code, t.experiment_year, t.season_code, lpad(t.location_number::TEXT, 6, '0')) AS location_name,
        'planted' AS location_status,
        'planting area' AS location_type,
        t.experiment_year AS location_year,
        t.season_id,
        t.location_number,
        t.site_id,
        t.field_id,
        t.creator_id AS steward_id,
        t.creator_id
    FROM
        t_loc2 AS t
    ORDER BY
        t.experiment_year,
        t.season_id,
        t.site_id,
        t.location_number
), t_loc4 AS (
    SELECT
        t.*,
        geo.id AS geospatial_object_id
    FROM
        t_loc3 AS t
        JOIN place.geospatial_object AS geo
            ON geo.geospatial_object_code = t.location_code
    ORDER BY
        t.location_year,
        t.season_id,
        t.site_id,
        t.location_number
)
SELECT
    t.*
FROM
    t_loc4 AS t
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.location'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~1.703s)
-- REINDEX TABLE experiment.location;

-- restore triggers and constraints
--ALTER TABLE experiment.location ENABLE TRIGGER ALL;
SET session_replication_role = origin;

-- total eta: ~8.914s



-- revert changes
--rollback --ALTER TABLE experiment.location DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.location'::regclass::oid AND indisready = TRUE;
--rollback 
--rollback -- delete 156,000 records (eta: ~0.831s)
--rollback DELETE FROM
--rollback     experiment.location AS loc
--rollback WHERE
--rollback     loc.location_code LIKE ANY((
--rollback         SELECT
--rollback             array_agg(DISTINCT (concat_ws('-', 'DB-584', site.geospatial_object_code, expt.experiment_year, ssn.season_code) || '-%')::varchar) AS location_code_fragments
--rollback         FROM
--rollback             experiment.occurrence AS occ
--rollback             JOIN experiment.experiment AS expt
--rollback                 ON expt.id = occ.experiment_id
--rollback             JOIN tenant.season AS ssn
--rollback                 ON ssn.id = expt.season_id
--rollback             JOIN place.geospatial_object AS site
--rollback                 ON site.id = occ.site_id
--rollback         WHERE
--rollback             expt.notes LIKE 'DB-584%'
--rollback     )::varchar[])
--rollback ;
--rollback
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.location'::regclass::oid AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~0.321s)
--rollback REINDEX TABLE experiment.location;
--rollback 
--rollback --ALTER TABLE experiment.location ENABLE TRIGGER ALL;
--rollback 
--rollback -- total eta: ~1.152s



--changeset postgres:insert_location_occurrence_groups context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DB-584 Insert location-occurrence groups



--# experiment.location_occurrence_group

-- disable trigger and constraints to speed up data insertions
--ALTER TABLE experiment.location_occurrence_group DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.location_occurrence_group'::regclass::oid AND indisready = TRUE;

-- insert 156,000 records (eta: ~4.331s)
INSERT INTO
    experiment.location_occurrence_group (
        location_id,
        occurrence_id,
        order_number,
        creator_id
    )
WITH t_occ AS (
    SELECT
        expt.id AS experiment_id,
        expt.experiment_year,
        expt.season_id,
        ssn.season_code,
        occ.id AS occurrence_id,
        occ.occurrence_name,
        occ.site_id,
        occ.field_id,
        site.geospatial_object_code AS site_code,
        site.parent_geospatial_object_id AS site_parent_id,
        site.root_geospatial_object_id AS site_root_id,
        occ.creator_id
    FROM
        experiment.occurrence AS occ
        JOIN experiment.experiment AS expt
            ON expt.id = occ.experiment_id
        JOIN tenant.season AS ssn
            ON ssn.id = expt.season_id
        JOIN place.geospatial_object AS site
            ON site.id = occ.site_id
    WHERE
        expt.notes LIKE 'DB-584%'
    ORDER BY
        expt.id,
        occ.id
), t_logrp AS (
    SELECT
        occ.*,
        NTILE(156000) OVER (ORDER BY occ.experiment_year, occ.season_id, occ.site_id) AS location_occurrence_group_number -- total number of planting areas/locations
    FROM
        t_occ AS occ
    ORDER BY
        location_occurrence_group_number
), t_loc1 AS (
    SELECT
        t.location_occurrence_group_number,
        array_agg(t.occurrence_id) AS occurrence_id_list,
        (array_agg(DISTINCT t.experiment_year))[1] AS experiment_year,
        (array_agg(DISTINCT t.season_id))[1] AS season_id,
        (array_agg(DISTINCT t.season_code))[1] AS season_code,
        (array_agg(DISTINCT t.site_id))[1] AS site_id,
        (array_agg(DISTINCT t.site_code))[1] AS site_code,
        (array_agg(DISTINCT t.field_id))[1] AS field_id,
        (array_agg(DISTINCT t.site_parent_id))[1] AS site_parent_id,
        (array_agg(DISTINCT t.site_root_id))[1] AS site_root_id,
        (array_agg(DISTINCT t.creator_id))[1] AS creator_id
    FROM
        t_logrp AS t
    GROUP BY
        t.location_occurrence_group_number
), t_loc2 AS (
    SELECT
        t.experiment_year,
        t.season_id,
        t.season_code,
        t.site_id,
        t.site_code,
        t.occurrence_id_list,
        ROW_NUMBER() OVER (PARTITION BY t.experiment_year, t.season_code, t.site_code) AS location_number,
        (array_agg(DISTINCT t.field_id))[1] AS field_id,
        (array_agg(DISTINCT t.site_parent_id))[1] AS site_parent_id,
        (array_agg(DISTINCT t.site_root_id))[1] AS site_root_id,
        (array_agg(DISTINCT t.creator_id))[1] AS creator_id
    FROM
        t_loc1 AS t
    GROUP BY
        t.experiment_year,
        t.season_id,
        t.season_code,
        t.site_id,
        t.site_code,
        t.occurrence_id_list,
        t.location_occurrence_group_number % 6500 -- number of locations per breeding season
    ORDER BY
        t.experiment_year,
        t.season_id,
        t.site_id
), t_loc AS (
    SELECT
        loc.id AS location_id,
        occ.id AS occurrence_id,
        --ROW_NUMBER() OVER (PARTITION BY loc.id ORDER BY occ.id) AS order_number,
        1 AS order_number,
        loc.creator_id
    FROM
        experiment.location AS loc
        JOIN t_loc2 AS t
            ON loc.location_code = CONCAT_WS('-', 'DB-584', t.site_code, t.experiment_year, t.season_code, lpad(t.location_number::TEXT, 6, '0')),
        UNNEST(t.occurrence_id_list) AS occ (id)
    WHERE
        loc.location_code LIKE 'DB-584%'
    ORDER BY
        loc.location_year,
        loc.season_id,
        loc.site_id,
        loc.location_number
)
SELECT
    t.*
FROM
    t_loc AS t
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.location_occurrence_group'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~0.459s)
-- REINDEX TABLE experiment.location_occurrence_group;

-- restore triggers and constraints
--ALTER TABLE experiment.location_occurrence_group ENABLE TRIGGER ALL;
SET session_replication_role = origin;

-- total eta: ~4.79s



-- revert changes
--rollback --ALTER TABLE experiment.location_occurrence_group DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.location_occurrence_group'::regclass::oid AND indisready = TRUE;
--rollback 
--rollback -- delete 156,000 records (eta: ~0.268s)
--rollback DELETE FROM
--rollback     experiment.location_occurrence_group AS logrp
--rollback USING
--rollback     experiment.location AS loc
--rollback WHERE
--rollback     loc.id = logrp.location_id
--rollback     AND loc.location_code LIKE 'DB-584%'
--rollback ;
--rollback
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.location_occurrence_group'::regclass::oid AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~0.104s)
--rollback REINDEX TABLE experiment.location_occurrence_group;
--rollback 
--rollback --ALTER TABLE experiment.location_occurrence_group ENABLE TRIGGER ALL;
--rollback 
--rollback -- total eta: ~0.372s



--changeset postgres:insert_plots context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DB-584 Insert plots



--# experiment.plot

-- disable trigger and constraints to speed up data insertions
--ALTER TABLE experiment.plot DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.plot'::regclass::oid AND indisready = TRUE;

-- prepare statement to populate plots and randomize design, planting area, and field coordinates
PREPARE
    populate_plots(
        integer, integer
        -- batch_count, batch_number
    )
AS
    INSERT INTO
        experiment.plot (
            occurrence_id,
            location_id,
            entry_id,
            plot_code,
            plot_number,
            plot_type,
            rep,
            design_x,
            design_y,
            pa_x,
            pa_y,
            field_x,
            field_y,
            block_number,
            plot_qc_code,
            plot_status,
            harvest_status,
            creator_id
        )
    WITH t_loc AS (
        SELECT
            expt.id AS experiment_id,
            entlist.id AS entry_list_id,
            occ.id AS occurrence_id,
            loc.id AS location_id,
            occ.rep_count,
            logrp.order_number,
            logrp.creator_id
        FROM
            experiment.experiment AS expt
            JOIN experiment.entry_list AS entlist
                ON entlist.experiment_id = expt.id
            JOIN experiment.occurrence AS occ
                ON occ.experiment_id = expt.id
            JOIN experiment.location_occurrence_group AS logrp
                ON logrp.occurrence_id = occ.id
            JOIN experiment.location AS loc
                ON loc.id = logrp.location_id
        WHERE
            loc.location_code LIKE 'DB-584%'
        ORDER BY
            loc.id
        LIMIT
            $1
        OFFSET
            $2
    ), t_plot AS (
        SELECT
            t.occurrence_id,
            t.location_id,
            ent.id AS entry_id,
            ROW_NUMBER() OVER (PARTITION BY t.location_id, t.occurrence_id ORDER BY t.location_id, t.occurrence_id) AS plot_number,
            'plot' AS plot_type,
            gs.rep,
            t.order_number AS block_number,
            'Q' AS plot_qc_code,
            'active' AS plot_status,
            'NO_HARVEST' AS harvest_status,
            t.creator_id
        FROM
            t_loc AS t
            JOIN experiment.entry AS ent
                ON ent.entry_list_id = t.entry_list_id,
            generate_series(1, t.rep_count) AS gs (rep)
    ), t_coords AS (
        SELECT
            t_design.plot_number,
            t_design.x AS design_x,
            t_design.y AS design_y,
            t_pa.x AS pa_x,
            t_pa.y AS pa_y,
            t_field.x AS field_x,
            t_field.y AS field_y
        FROM
            (
                SELECT
                    o AS order_number,
                    ROW_NUMBER() OVER (PARTITION BY o ORDER BY random()) AS plot_number,
                    x,
                    y
                FROM
                    generate_series(1, 1) AS o,
                    generate_series(1, 10) AS x,
                    generate_series(1, 25) AS y
            ) AS t_design,
            (
                SELECT
                    ROW_NUMBER() OVER (ORDER BY random()) AS plot_number,
                    x,
                    y
                FROM
                    generate_series(1, 10 * 1) AS x,
                    generate_series(1, 25) AS y
            ) AS t_pa,
            (
                SELECT
                    ROW_NUMBER() OVER (ORDER BY random()) AS plot_number,
                    x,
                    y
                FROM
                    generate_series(1, 10 * 1) AS x,
                    generate_series(1, 25) AS y
            ) AS t_field
        WHERE
            t_design.plot_number = t_pa.plot_number
            AND t_design.plot_number = t_field.plot_number
    )
    SELECT
        t.occurrence_id,
        t.location_id,
        t.entry_id,
        t.plot_number AS plot_code,
        t.plot_number,
        t.plot_type,
        t.rep,
        u.design_x,
        u.design_y,
        u.pa_x,
        u.pa_y,
        u.field_x,
        u.field_y,
        t.block_number,
        t.plot_qc_code,
        t.plot_status,
        t.harvest_status,
        t.creator_id
    FROM
        t_plot AS t
        JOIN t_coords AS u
            ON t.plot_number = u.plot_number
;

-- insert 2,500,000 records per batch (1,500,000 records in the last batch) (total eta: ~6m 48s)
EXECUTE populate_plots(10000, 10000 * 0); -- batch 0 (eta: ~26.243s)
EXECUTE populate_plots(10000, 10000 * 1); -- batch 1 (eta: ~25.640s)
EXECUTE populate_plots(10000, 10000 * 2); -- batch 2 (eta: ~25.917s)
EXECUTE populate_plots(10000, 10000 * 3); -- batch 3 (eta: ~26.268s)
EXECUTE populate_plots(10000, 10000 * 4); -- batch 4 (eta: ~26.117s)
EXECUTE populate_plots(10000, 10000 * 5); -- batch 5 (eta: ~25.676s)
EXECUTE populate_plots(10000, 10000 * 6); -- batch 6 (eta: ~25.148s)
EXECUTE populate_plots(10000, 10000 * 7); -- batch 7 (eta: ~25.736s)
EXECUTE populate_plots(10000, 10000 * 8); -- batch 8 (eta: ~25.980s)
EXECUTE populate_plots(10000, 10000 * 9); -- batch 9 (eta: ~25.918s)
EXECUTE populate_plots(10000, 10000 * 10); -- batch 10 (eta: ~26.79s)
EXECUTE populate_plots(10000, 10000 * 11); -- batch 11 (eta: ~25.687s)
EXECUTE populate_plots(10000, 10000 * 12); -- batch 12 (eta: ~25.948s)
EXECUTE populate_plots(10000, 10000 * 13); -- batch 13 (eta: ~26.96s)
EXECUTE populate_plots(10000, 10000 * 14); -- batch 14 (eta: ~25.458s)
EXECUTE populate_plots(10000, 10000 * 15); -- batch 15 (eta: ~18.653s)

-- drop prepared statement
DEALLOCATE populate_plots;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.plot'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~7m 47s)
-- REINDEX TABLE experiment.plot;

-- restore triggers and constraints
--ALTER TABLE experiment.plot ENABLE TRIGGER ALL;
SET session_replication_role = origin;

-- total eta: ~14m 35s



-- revert changes
--rollback --ALTER TABLE experiment.plot DISABLE TRIGGER ALL;
--rollback
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.plot'::regclass::oid AND indisready = TRUE;
--rollback
--rollback -- delete 39,000,000 records (eta: ~TIMEs)
--rollback DELETE FROM
--rollback     experiment.plot AS plot
--rollback USING
--rollback     experiment.occurrence AS occ
--rollback     JOIN experiment.location_occurrence_group AS logrp
--rollback         ON logrp.occurrence_id = occ.id
--rollback     JOIN experiment.location AS loc
--rollback         ON loc.id = logrp.location_id
--rollback     JOIN experiment.experiment AS expt
--rollback         ON expt.id = occ.experiment_id
--rollback WHERE
--rollback     occ.id = plot.occurrence_id
--rollback     AND expt.notes LIKE 'DB-584%'
--rollback ;
--rollback
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.plot'::regclass::oid AND indisready = FALSE;
--rollback
--rollback -- reindex table (eta: ~TIMEs)
--rollback REINDEX TABLE experiment.plot;
--rollback
--rollback --ALTER TABLE experiment.plot ENABLE TRIGGER ALL;
--rollback 
--rollback -- total eta: ~TIMEs



--changeset postgres:insert_plot_data context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DB-584 Insert plot data



--# experiment.plot_data

-- disable triggers and constraints to speed up data insertions
--ALTER TABLE experiment.plot_data DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.plot_data'::regclass::oid AND indisready = TRUE;

-- insert 39,000,000 records for HV_METH_DISC (eta: ~1m 46s)
INSERT INTO
    experiment.plot_data (
        plot_id,
        variable_id,
        data_value,
        data_qc_code,
        creator_id
    )
SELECT
    plot.id AS plot_id,
    var.id AS variable_id,
    plotdata.data_value,
    'Q' AS data_qc_code,
    plot.creator_id
FROM (
        VALUES
        ('HV_METH_DISC', 'Bulk')
    ) AS plotdata (
        variable, data_value
    )
    JOIN master.variable AS var
        ON var.abbrev = plotdata.variable,
    experiment.location AS loc
    JOIN experiment.plot AS plot
        ON plot.location_id = loc.id
WHERE
    loc.location_code LIKE 'DB-584%'
;

-- insert 39,000,000 records for HVDATE_CONT (eta: ~2m 26s)
INSERT INTO
    experiment.plot_data (
        plot_id,
        variable_id,
        data_value,
        data_qc_code,
        creator_id
    )
SELECT
    plot.id AS plot_id,
    var.id AS variable_id,
    (loc.location_year || '-' || plotdata.data_value) AS data_value,
    'Q' AS data_qc_code,
    plot.creator_id
FROM (
        VALUES
        ('HVDATE_CONT', 'DS', '05-25'),
        ('HVDATE_CONT', 'WS', '10-25')
    ) AS plotdata (
        variable, season, data_value
    )
    JOIN master.variable AS var
        ON var.abbrev = plotdata.variable
    JOIN tenant.season AS ssn
        ON ssn.season_code = plotdata.season
    JOIN experiment.location AS loc
        ON loc.season_id = ssn.id
    JOIN experiment.plot AS plot
        ON plot.location_id = loc.id
WHERE
    loc.location_code LIKE 'DB-584%'
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.plot_data'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~9m 50s)
-- REINDEX TABLE experiment.plot_data;

-- restore triggers and constraints
--ALTER TABLE experiment.plot_data ENABLE TRIGGER ALL;
SET session_replication_role = origin;

-- total eta: ~14m 2s



-- revert changes
--rollback --ALTER TABLE experiment.plot_data DISABLE TRIGGER ALL;
--rollback
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.plot_data'::regclass::oid AND indisready = TRUE;
--rollback
--rollback -- delete 39,000,000 records (eta: ~TIMEs)
--rollback DELETE FROM
--rollback     experiment.plot_data AS plotdata
--rollback USING
--rollback     experiment.location AS loc
--rollback     JOIN experiment.plot AS plot
--rollback         ON plot.location_id = loc.id,
--rollback     master.variable AS var
--rollback WHERE
--rollback     plotdata.plot_id = plot.id
--rollback     AND var.id = plotdata.variable_id
--rollback     AND loc.location_code LIKE 'DB-584%'
--rollback     AND var.abbrev = 'HV_METH_DISC'
--rollback ;
--rollback
--rollback -- delete 39,000,000 records (eta: ~TIMEs)
--rollback DELETE FROM
--rollback     experiment.plot_data AS plotdata
--rollback USING
--rollback     experiment.location AS loc
--rollback     JOIN experiment.plot AS plot
--rollback         ON plot.location_id = loc.id,
--rollback     master.variable AS var
--rollback WHERE
--rollback     plotdata.plot_id = plot.id
--rollback     AND var.id = plotdata.variable_id
--rollback     AND loc.location_code LIKE 'DB-584%'
--rollback     AND var.abbrev = 'HVDATE_CONT'
--rollback ;
--rollback
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.plot_data'::regclass::oid AND indisready = FALSE;
--rollback
--rollback -- reindex table (eta: ~TIMEs)
--rollback REINDEX TABLE experiment.plot_data;
--rollback
--rollback --ALTER TABLE experiment.plot_data ENABLE TRIGGER ALL;



--changeset postgres:insert_planting_instructions context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DB-584 Insert planting instructions



--# experiment.planting_instruction

-- disable triggers and constraints to speed up data insertions
--ALTER TABLE experiment.planting_instruction DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.planting_instruction'::regclass::oid AND indisready = TRUE;

-- prepare statement to populate planting instructions of plots
PREPARE
    populate_planting_instructions(
        integer, integer
        -- limit_number, offset_number
    )
AS
    INSERT INTO
        experiment.planting_instruction (
            entry_code,
            entry_number,
            entry_name,
            entry_type,
            entry_role,
            entry_class,
            entry_status,
            entry_id,
            plot_id,
            germplasm_id,
            seed_id,
            package_id,
            package_log_id,
            creator_id
        )
    WITH t_loc AS (
        SELECT
            expt.id AS experiment_id,
            occ.id AS occurrence_id,
            loc.id AS location_id
        FROM
            experiment.experiment AS expt
            JOIN experiment.occurrence AS occ
                ON occ.experiment_id = expt.id
            JOIN experiment.location_occurrence_group AS logrp
                ON logrp.occurrence_id = occ.id
            JOIN experiment.location AS loc
                ON loc.id = logrp.location_id
        WHERE
            expt.notes LIKE 'DB-584%'
        ORDER BY
            loc.id
        LIMIT
            $1
        OFFSET
            $2
    )
    SELECT
        ent.entry_code,
        ent.entry_number,
        ent.entry_name,
        ent.entry_type,
        ent.entry_role,
        ent.entry_class,
        ent.entry_status,
        ent.id AS entry_id,
        plot.id AS plot_id,
        ent.germplasm_id,
        ent.seed_id,
        ent.package_id,
        NULL AS package_log_id,
        plot.creator_id
    FROM
        t_loc AS t
        JOIN experiment.plot AS plot
            ON plot.location_id = t.location_id
        JOIN experiment.entry AS ent
            ON ent.id = plot.entry_id
;

-- insert 2,500,000 records per batch (1,500,000 records in the last batch) (total eta: ~4m 30s)
EXECUTE populate_planting_instructions(10000, 10000 * 0); -- batch 0 (eta: ~11.594s)
EXECUTE populate_planting_instructions(10000, 10000 * 1); -- batch 1 (eta: ~13.38s)
EXECUTE populate_planting_instructions(10000, 10000 * 2); -- batch 2 (eta: ~11.828s)
EXECUTE populate_planting_instructions(10000, 10000 * 3); -- batch 3 (eta: ~14.69s)
EXECUTE populate_planting_instructions(10000, 10000 * 4); -- batch 4 (eta: ~12.169s)
EXECUTE populate_planting_instructions(10000, 10000 * 5); -- batch 5 (eta: ~16.492s)
EXECUTE populate_planting_instructions(10000, 10000 * 6); -- batch 6 (eta: ~17.208s)
EXECUTE populate_planting_instructions(10000, 10000 * 7); -- batch 7 (eta: ~16.155s)
EXECUTE populate_planting_instructions(10000, 10000 * 8); -- batch 8 (eta: ~17.579s)
EXECUTE populate_planting_instructions(10000, 10000 * 9); -- batch 9 (eta: ~18.211s)
EXECUTE populate_planting_instructions(10000, 10000 * 10); -- batch 10 (eta: ~18.100s)
EXECUTE populate_planting_instructions(10000, 10000 * 11); -- batch 11 (eta: ~21.245s)
EXECUTE populate_planting_instructions(10000, 10000 * 12); -- batch 12 (eta: ~21.134s)
EXECUTE populate_planting_instructions(10000, 10000 * 13); -- batch 13 (eta: ~23.363s)
EXECUTE populate_planting_instructions(10000, 10000 * 14); -- batch 14 (eta: ~22.206s)
EXECUTE populate_planting_instructions(10000, 10000 * 15); -- batch 15 (eta: ~15.167s)

-- drop prepared statement
DEALLOCATE populate_planting_instructions;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.planting_instruction'::regclass::oid AND indisready = FALSE;

-- reindex table takes (eta: ~13m 32s)
-- REINDEX TABLE experiment.planting_instruction;

-- restore triggers and constraints
--ALTER TABLE experiment.planting_instruction ENABLE TRIGGER ALL;
SET session_replication_role = origin;

-- total eta: ~18m 2s



-- revert changes
--rollback --ALTER TABLE experiment.planting_instruction DISABLE TRIGGER ALL;
--rollback
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.planting_instruction'::regclass::oid AND indisready = TRUE;
--rollback
--rollback -- delete 39,000,000 records (eta: ~TIMEs)
--rollback DELETE FROM
--rollback     experiment.planting_instruction AS plantinst
--rollback USING
--rollback     experiment.location AS loc
--rollback     JOIN experiment.plot AS plot
--rollback         ON plot.location_id = loc.id
--rollback WHERE
--rollback     plantinst.plot_id = plot.id
--rollback     AND loc.location_code LIKE 'DB-584%'
--rollback ;
--rollback
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.planting_instruction'::regclass::oid AND indisready = FALSE;
--rollback
--rollback -- reindex table (eta: ~TIMEs)
--rollback REINDEX TABLE experiment.planting_instruction;
--rollback
--rollback --ALTER TABLE experiment.planting_instruction ENABLE TRIGGER ALL;



--changeset postgres:update_seed_sources context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DB-584 Update seed sources



--# germplasm.seed

-- disable triggers and constraints to speed up data insertions
--ALTER TABLE germplasm.seed DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.seed'::regclass::oid AND indisready = TRUE;

-- prepare statement to update seed source columns
PREPARE
    update_seed_sources(
        integer, integer, integer
        -- limit_number, offset_number, batch_number
    )
AS
    UPDATE
        germplasm.seed AS sd
    SET
        source_experiment_id = t.experiment_id,
        source_entry_id = t.entry_id,
        source_occurrence_id = t.occurrence_id,
        source_location_id = t.location_id,
        source_plot_id = t.plot_id
    FROM (
            SELECT
                t_seeds.seed_id,
                t_experiments.experiment_id,
                t_experiments.entry_id,
                t_experiments.occurrence_id,
                t_experiments.location_id,
                t_experiments.plot_id
            FROM (
                    SELECT
                        ROW_NUMBER() OVER () AS order_number,
                        s.*
                    FROM (
                            SELECT
                                sd.id AS seed_id
                            FROM
                                tenant.crop AS crop
                                JOIN germplasm.germplasm AS ge
                                    ON ge.crop_id = crop.id
                                JOIN germplasm.seed AS sd
                                    ON sd.germplasm_id = ge.id
                            WHERE
                                crop.crop_code = 'WHEAT'
                                AND ge.designation LIKE 'WTGE%'
                                AND sd.source_experiment_id IS NULL
                            ORDER BY
                                sd.id
                            LIMIT
                                $1
                        ) AS s
                ) AS t_seeds, (
                    SELECT
                        ROW_NUMBER() OVER () AS order_number,
                        e.*
                    FROM (
                            SELECT
                                expt.id AS experiment_id,
                                ent.id AS entry_id,
                                occ.id AS occurrence_id,
                                loc.id AS location_id,
                                plot.id AS plot_id
                            FROM
                                (
                                    SELECT
                                        expt.id
                                    FROM
                                        experiment.experiment AS expt
                                    WHERE
                                        expt.notes LIKE 'DB-584%'
                                    ORDER BY
                                        expt.id
                                    LIMIT
                                        $1 / 100
                                    OFFSET
                                        ($2 / 100) * $3
                                ) AS expt
                                JOIN experiment.entry_list AS entlist
                                    ON entlist.experiment_id = expt.id
                                JOIN experiment.occurrence AS occ
                                    ON occ.experiment_id = expt.id
                                JOIN experiment.location_occurrence_group AS logrp
                                    ON logrp.occurrence_id = occ.id
                                JOIN experiment.location AS loc
                                    ON loc.id = logrp.location_id
                                JOIN experiment.entry AS ent
                                    ON ent.entry_list_id = entlist.id
                                JOIN experiment.plot AS plot
                                    ON plot.entry_id = ent.id
                            LIMIT
                                $1
                            OFFSET
                                $2 * $3
                        ) AS e
                ) AS t_experiments
            WHERE
                t_seeds.order_number = t_experiments.order_number
        ) AS t
    WHERE
        sd.id = t.seed_id
;

-- update 1,500,000 seeds (total eta: ~3m 45s)

-- update 100,000 records per run (eta: ~15s per run)
EXECUTE update_seed_sources(100000, 100000, 0); -- update 100,000 seed sources
EXECUTE update_seed_sources(100000, 100000, 1); -- update 100,000 seed sources
EXECUTE update_seed_sources(100000, 100000, 2); -- update 100,000 seed sources
EXECUTE update_seed_sources(100000, 100000, 3); -- update 100,000 seed sources
EXECUTE update_seed_sources(100000, 100000, 4); -- update 100,000 seed sources

EXECUTE update_seed_sources(100000, 100000, 5); -- update 100,000 seed sources
EXECUTE update_seed_sources(100000, 100000, 6); -- update 100,000 seed sources
EXECUTE update_seed_sources(100000, 100000, 7); -- update 100,000 seed sources
EXECUTE update_seed_sources(100000, 100000, 8); -- update 100,000 seed sources
EXECUTE update_seed_sources(100000, 100000, 9); -- update 100,000 seed sources

EXECUTE update_seed_sources(100000, 100000, 10); -- update 100,000 seed sources
EXECUTE update_seed_sources(100000, 100000, 11); -- update 100,000 seed sources
EXECUTE update_seed_sources(100000, 100000, 12); -- update 100,000 seed sources
EXECUTE update_seed_sources(100000, 100000, 13); -- update 100,000 seed sources
EXECUTE update_seed_sources(100000, 100000, 14); -- update 100,000 seed sources

-- drop prepared statement
DEALLOCATE update_seed_sources;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.seed'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~TIMEs)
-- REINDEX TABLE germplasm.seed;

-- restore triggers and constraints
--ALTER TABLE germplasm.seed ENABLE TRIGGER ALL;
SET session_replication_role = origin;
ALTER TABLE germplasm.seed DISABLE TRIGGER seed_update_package_document_from_seed_tgr;

-- total eta: ~TIMEs



-- revert changes
--rollback --ALTER TABLE germplasm.seed DISABLE TRIGGER ALL;
--rollback
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.seed'::regclass::oid AND indisready = TRUE;
--rollback
--rollback -- update 1,500,000 records (eta: ~TIMEs)
--rollback UPDATE
--rollback     germplasm.seed AS sd
--rollback SET
--rollback     source_experiment_id = NULL,
--rollback     source_entry_id = NULL,
--rollback     source_occurrence_id = NULL,
--rollback     source_location_id = NULL,
--rollback     source_plot_id = NULL
--rollback FROM
--rollback     tenant.crop AS crop
--rollback     JOIN germplasm.germplasm AS ge
--rollback         ON ge.crop_id = crop.id
--rollback WHERE
--rollback     crop.crop_code = 'WHEAT'
--rollback     AND ge.designation LIKE 'WTGE%'
--rollback     AND sd.germplasm_id = ge.id
--rollback     AND source_experiment_id IS NOT NULL
--rollback ;
--rollback
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.seed'::regclass::oid AND indisready = FALSE;
--rollback
--rollback -- reindex table (eta: ~TIMEs)
--rollback REINDEX TABLE germplasm.seed;
--rollback
--rollback --ALTER TABLE germplasm.seed ENABLE TRIGGER ALL;
--rollback ALTER TABLE germplasm.seed DISABLE TRIGGER seed_update_package_document_from_seed_tgr;
