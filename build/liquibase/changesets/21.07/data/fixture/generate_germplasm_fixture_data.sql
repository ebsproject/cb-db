--liquibase formatted sql

--changeset postgres:insert_crop context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-456 Insert crop



--# tenant.crop

--/* insert
INSERT INTO
    tenant.crop (
        crop_code,
        crop_name,
        description,
        creator_id
    )
SELECT
    crop.crop_code,
    crop.crop_name,
    crop.description,
    crtr.id AS creator_id
FROM (
        VALUES
        ('CROPX', 'Crop X', 'Test Crop', 1)
    ) AS crop (
        crop_code,
        crop_name,
        description,
        creator
    )
    JOIN tenant.person AS crtr
        ON crtr.id = crop.creator
;
--*/



-- revert changes
--rollback DELETE FROM
--rollback     tenant.crop
--rollback WHERE
--rollback     crop_code = 'CROPX'
--rollback ;



--changeset postgres:insert_crop_program context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-456 Insert crop program



--# tenant.crop_program

--/* insert
-- insert 1 record/s to table (eta: ~4ms)
INSERT INTO
    tenant.crop_program (
        crop_program_code,
        crop_program_name,
        organization_id,
        crop_id,
        creator_id
    )
SELECT
    t.crop_program_code,
    t.crop_program_name,
    org.id AS organization_id,
    crop.id AS crop_id,
    crtr.id AS creator_id
FROM (
        VALUES
        ('CROPPROGX', 'Crop Program X', 1, 'IRRI', 'CROPX')
    ) AS t (
        crop_program_code, crop_program_name, creator_id, organization_id, crop_id
    )
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
    JOIN tenant.organization AS org
        ON org.organization_code = t.organization_id
    JOIN tenant.crop AS crop
        ON crop.crop_code = t.crop_id
;
--*/



-- revert changes
--rollback -- delete 1 records (eta: ~27ms)
--rollback DELETE FROM
--rollback     tenant.crop_program AS cropprog
--rollback WHERE
--rollback     cropprog.crop_program_code = 'CROPPROGX'
--rollback ;



--changeset postgres:insert_program context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-456 Insert program



--# tenant.crop_program

--/* insert
-- insert 1 record/s to table (eta: ~8ms)
INSERT INTO
    tenant.program (
        program_code,
        program_name,
        program_type,
        program_status,
        crop_program_id,
        creator_id
    )
SELECT
    t.program_code,
    t.program_name,
    t.program_type,
    t.program_status,
    cropprog.id AS crop_program_id,
    crtr.id AS creator_id
FROM (
        VALUES
        ('PROGX', 'Program X', 'breeding', 'active', 'CROPPROGX', 1)
    ) AS t (
        program_code,
        program_name,
        program_type,
        program_status,
        crop_program_id,
        creator_id
    )
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
    JOIN tenant.crop_program AS cropprog
        ON cropprog.crop_program_code = t.crop_program_id
;
--*/



-- revert changes
-- delete 1 record/s (eta: ~113ms)
--rollback DELETE FROM
--rollback     tenant.program AS prog
--rollback WHERE
--rollback     prog.program_code = 'PROGX'
--rollback ;



--changeset postgres:insert_taxonomy context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-456 Insert taxonomy



--# germplasm.taxonomy

--/* insert
INSERT INTO
    germplasm.taxonomy (
        taxon_id,
        taxonomy_name,
        description,
        crop_id,
        creator_id
    )
SELECT
    t.taxon_id,
    t.taxonomy_name,
    t.description,
    crop.id AS crop_id,
    crtr.id AS creator_id
FROM (
        VALUES
        ('111', 'Taxonomy X', 'Test Taxonomy', 'CROPX', 1)
    ) AS t (
        taxon_id,
        taxonomy_name,
        description,
        crop_id,
        creator_id
    )
    JOIN tenant.crop AS crop
        ON crop.crop_code = t.crop_id
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
;
--*/



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.taxonomy
--rollback WHERE
--rollback     taxon_id = '111'
--rollback ;



--changeset postgres:insert_germplasm context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-456 Insert germplasm



--# germplasm.germplasm

--/* insert
-- disable triggers and constraints to speed up data insertions
--ALTER TABLE germplasm.germplasm DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.germplasm'::regclass::oid AND indisready = TRUE;

-- insert 1,000,000 records to table (eta: ~11.59s)
INSERT INTO
    germplasm.germplasm (
        designation,
        parentage,
        generation,
        germplasm_state,
        germplasm_name_type,
        crop_id,
        germplasm_normalized_name,
        taxonomy_id,
        creator_id,
        germplasm_type
    )
SELECT
    (t.designation || '-' || gs.num) AS designation,
    t.parentage,
    (t.generation || floor(random() * (12-6 + 1) + 6)::int) AS generation,
    t.germplasm_state,
    t.germplasm_name_type,
    crop.id AS crop_id,
    (t.germplasm_normalized_name || '-' || gs.num) AS germplasm_normalized_name,
    taxon.id AS taxonomy_id,
    crtr.id AS creator_id,
    t.germplasm_type
FROM (
        VALUES
        ('CXGE', '?/?', 'F', 'fixed', 'line_name', 'CROPX', 'CXGE', '111', 'fixed_line', 1)
    ) AS t (
        designation,
        parentage,
        generation,
        germplasm_state,
        germplasm_name_type,
        crop_id,
        germplasm_normalized_name,
        taxonomy_id,
        germplasm_type,
        creator_id
    )
    JOIN tenant.crop AS crop
        ON crop.crop_code = t.crop_id
    JOIN germplasm.taxonomy AS taxon
        ON taxon.taxon_id = t.taxonomy_id
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id,
    generate_series(1, 250000) AS gs (num) -- 250k fixed lines
UNION ALL
    SELECT
        (t.designation || '-' || gs.num) AS designation,
        t.parentage,
        (t.generation || ceiling(random() * (6 + 1))::int) AS generation,
        t.germplasm_state,
        t.germplasm_name_type,
        crop.id AS crop_id,
        (t.germplasm_normalized_name || '-' || gs.num) AS germplasm_normalized_name,
        taxon.id AS taxonomy_id,
        crtr.id AS creator_id,
        t.germplasm_type
    FROM (
            VALUES
            ('CXGE', '?/?', 'F', 'not_fixed', 'derivative_name', 'CROPX', 'CXGE', '111', 'progeny', 1)
        ) AS t (
            designation,
            parentage,
            generation,
            germplasm_state,
            germplasm_name_type,
            crop_id,
            germplasm_normalized_name,
            taxonomy_id,
            germplasm_type,
            creator_id
        )
        JOIN tenant.crop AS crop
            ON crop.crop_code = t.crop_id
        JOIN germplasm.taxonomy AS taxon
            ON taxon.taxon_id = t.taxonomy_id
        JOIN tenant.person AS crtr
            ON crtr.id = t.creator_id,
        generate_series(250001, 1000000) AS gs (num) -- 750k progenies
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.germplasm'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~6.384s)
-- REINDEX TABLE germplasm.germplasm;

-- restore triggers and constraints
--ALTER TABLE germplasm.germplasm ENABLE TRIGGER ALL;
SET session_replication_role = origin;
ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_germplasm_document_tgr;
ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;

-- total eta: ~17.974s
--*/



-- revert changes
--rollback --ALTER TABLE germplasm.germplasm DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.germplasm'::regclass::oid AND indisprimary = FALSE AND indisready = TRUE;
--rollback 
--rollback -- delete 1,000,000 records (eta: ~2.106s)
--rollback DELETE FROM
--rollback     germplasm.germplasm AS ge
--rollback USING
--rollback     tenant.crop AS crop
--rollback WHERE
--rollback     crop.id = ge.crop_id
--rollback     AND crop.crop_code = 'CROPX'
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.germplasm'::regclass::oid AND indisprimary = FALSE AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~2.70s)
--rollback REINDEX TABLE germplasm.germplasm;
--rollback 
--rollback --ALTER TABLE germplasm.germplasm ENABLE TRIGGER ALL;
--rollback ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_germplasm_document_tgr;
--rollback ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;
--rollback 
--rollback -- total eta: ~4.806s



--changeset postgres:insert_germplasm_names context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-456 Insert germplasm names



--# germplasm.germplasm_name

--/* insert
-- disable triggers and constraints to speed up data insertions
--ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.germplasm_name'::regclass::oid AND indisready = TRUE;

-- insert 3,000,000 records to table (eta: ~16.516s)
INSERT INTO
    germplasm.germplasm_name (
        germplasm_id,
        name_value,
        germplasm_name_type,
        germplasm_name_status,
        germplasm_normalized_name,
        creator_id
    )
SELECT
    ge.id AS germplasm_id,
    ge.designation || t.name_value AS name_value,
    t.germplasm_name_type,
    t.germplasm_name_status,
    ge.germplasm_normalized_name || t.germplasm_normalized_name AS germplasm_normalized_name,
    ge.creator_id
FROM (
        VALUES
        ('', 'line_name', 'standard', ''),
        ('A', 'alternative_cultivar_name', 'active', ' A'),
        ('B', 'alternative_cultivar_name', 'active', ' B')
    ) AS t (
        name_value,
        germplasm_name_type,
        germplasm_name_status,
        germplasm_normalized_name
    )
    JOIN tenant.crop AS crop
        ON crop.crop_code = 'CROPX'
    JOIN germplasm.germplasm AS ge
        ON ge.crop_id = crop.id
        AND ge.germplasm_state = 'fixed'
UNION ALL
    SELECT
        ge.id AS germplasm_id,
        ge.designation || t.name_value AS name_value,
        t.germplasm_name_type,
        t.germplasm_name_status,
        ge.germplasm_normalized_name || t.germplasm_normalized_name AS germplasm_normalized_name,
        ge.creator_id
    FROM (
            VALUES
            ('', 'derivative_name', 'standard', ''),
            ('A', 'alternative_derivative_name', 'active', ' A'),
            ('B', 'alternative_derivative_name', 'active', ' B')
        ) AS t (
            name_value,
            germplasm_name_type,
            germplasm_name_status,
            germplasm_normalized_name
        )
        JOIN tenant.crop AS crop
            ON crop.crop_code = 'CROPX'
        JOIN germplasm.germplasm AS ge
            ON ge.crop_id = crop.id
            AND ge.germplasm_state = 'not_fixed'
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.germplasm_name'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~20.493s)
-- REINDEX TABLE germplasm.germplasm_name;

-- restore triggers and constraints
--ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER ALL;
SET session_replication_role = origin;
ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;

-- total eta: ~37.009s
--*/



-- revert changes
--rollback --ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.germplasm_name'::regclass::oid AND indisprimary = FALSE AND indisready = TRUE;
--rollback 
--rollback -- delete 3,000,000 records (eta: ~9.399s)
--rollback DELETE FROM
--rollback     germplasm.germplasm_name AS gename
--rollback USING
--rollback     germplasm.germplasm AS ge
--rollback     JOIN tenant.crop AS crop
--rollback         ON crop.id = ge.crop_id
--rollback WHERE
--rollback     ge.id = gename.germplasm_id
--rollback     AND crop.crop_code = 'CROPX'
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.germplasm_name'::regclass::oid AND indisprimary = FALSE AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~2.195s)
--rollback REINDEX TABLE germplasm.germplasm_name;
--rollback 
--rollback --ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER ALL;
--rollback ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
--rollback 
--rollback -- total eta: ~11.594s



--changeset postgres:insert_germplasm_attributes context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-456 Insert germplasm attributes



--# germplasm.germplasm_attribute

--/* insert
-- disable triggers and constraints to speed up data insertions
--ALTER TABLE germplasm.germplasm_attribute DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.germplasm_attribute'::regclass::oid AND indisready = TRUE;

-- insert 1,500,000 records to table (eta: ~5.500s)
INSERT INTO
    germplasm.germplasm_attribute (
        germplasm_id,
        variable_id,
        data_value,
        data_qc_code,
        creator_id
    )
SELECT
    ge.id AS germplasm_id,
    var.id AS variable_id,
    (floor(random() * 8 + 1))::varchar AS data_value,
    t.data_qc_code,
    crtr.id AS creator_id
FROM (
        VALUES
        ('CROSS_NUMBER', 'G', 1)
    ) AS t (
        variable_id,
        data_qc_code,
        creator_id
    )
    JOIN master.variable AS var
        ON var.abbrev = t.variable_id
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
    JOIN tenant.crop AS crop
        ON crop.crop_code = 'CROPX'
    JOIN germplasm.germplasm AS ge
        ON ge.crop_id = crop.id
        AND ge.germplasm_state = 'not_fixed'
UNION ALL
    SELECT
        ge.id AS germplasm_id,
        var.id AS variable_id,
        ((array['S', 'W'])[floor(random() * 2 + 1)])::varchar AS data_value,
        t.data_qc_code,
        crtr.id AS creator_id
    FROM (
            VALUES
            ('GROWTH_HABIT', 'G', 1)
        ) AS t (
            variable_id,
            data_qc_code,
            creator_id
        )
        JOIN master.variable AS var
            ON var.abbrev = t.variable_id
        JOIN tenant.person AS crtr
            ON crtr.id = t.creator_id
        JOIN tenant.crop AS crop
            ON crop.crop_code = 'CROPX'
        JOIN germplasm.germplasm AS ge
            ON ge.crop_id = crop.id
            AND ge.germplasm_state = 'not_fixed'
ORDER BY
    germplasm_id,
    variable_id
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.germplasm_attribute'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~8.163s)
-- REINDEX TABLE germplasm.germplasm_attribute;

-- restore triggers and constraints
--ALTER TABLE germplasm.germplasm_attribute ENABLE TRIGGER ALL;
SET session_replication_role = origin;

-- total eta: ~13.663s
--*/



-- revert changes
--rollback --ALTER TABLE germplasm.germplasm_attribute DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.germplasm_attribute'::regclass::oid AND indisprimary = FALSE AND indisready = TRUE;
--rollback 
--rollback -- delete 1,500,000 records (eta: ~2.569s)
--rollback DELETE FROM
--rollback     germplasm.germplasm_attribute AS geattr
--rollback USING
--rollback     germplasm.germplasm AS ge
--rollback     JOIN tenant.crop AS crop
--rollback         ON crop.id = ge.crop_id
--rollback WHERE
--rollback     ge.id = geattr.germplasm_id
--rollback     AND crop.crop_code = 'CROPX'
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.germplasm_attribute'::regclass::oid AND indisprimary = FALSE AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~0.834s)
--rollback REINDEX TABLE germplasm.germplasm_attribute;
--rollback 
--rollback --ALTER TABLE germplasm.germplasm_attribute ENABLE TRIGGER ALL;
--rollback 
--rollback -- total eta: ~3.403s



--changeset postgres:insert_seeds context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-456 Insert seeds



--# germplasm.seed

--/* insert
-- disable triggers and constraints to speed up data insertions
--ALTER TABLE germplasm.seed DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.seed'::regclass::oid AND indisready = TRUE;

-- insert 2,500,000 records to table (eta: ~28.664s)
INSERT INTO
    germplasm.seed (
        seed_code,
        seed_name,
        harvest_date,
        harvest_method,
        germplasm_id,
        program_id,
        source_experiment_id,
        source_entry_id,
        source_occurrence_id,
        source_location_id,
        source_plot_id,
        cross_id,
        harvest_source,
        creator_id
    )
SELECT
    germplasm.generate_code('seed') AS seed_code,
    (ge.designation || '-SD' || gs.num) AS seed_name,
    (timestamp '2016-01-01' + random() * (timestamp '2020-12-31' - timestamp '2016-01-01'))::date AS harvest_date,
    'Bulk' AS harvest_method,
    ge.id AS germplasm_id,
    prog.id AS program_id,
    NULL::integer AS source_experiment_id,
    NULL::integer AS source_entry_id,
    NULL::integer AS source_occurrence_id,
    NULL::integer AS source_location_id,
    NULL::integer AS source_plot_id,
    NULL::integer AS cross_id,
    'plot' AS harvest_source,
    ge.creator_id
FROM
    germplasm.germplasm AS ge
    JOIN tenant.crop AS crop
        ON crop.id = ge.crop_id
    JOIN tenant.program AS prog
        ON prog.program_code = 'PROGX',
    generate_series(1, 10) AS gs (num)
WHERE
    crop.crop_code = 'CROPX'
    AND ge.germplasm_state = 'fixed'
;

-- insert 7,500,000 records to table (eta: ~1m 28s)
INSERT INTO
    germplasm.seed (
        seed_code,
        seed_name,
        harvest_date,
        harvest_method,
        germplasm_id,
        program_id,
        source_experiment_id,
        source_entry_id,
        source_occurrence_id,
        source_location_id,
        source_plot_id,
        cross_id,
        harvest_source,
        creator_id
    )
SELECT
    germplasm.generate_code('seed') AS seed_code,
    (ge.designation || '-' || gs.num) AS seed_name,
    (timestamp '2016-01-01' + random() * (timestamp '2020-12-31' - timestamp '2016-01-01'))::date AS harvest_date,
    ((array['Bulk', 'Single Plant Selection', 'Plant-specific'])[floor(random() * 3 + 1)]) AS harvest_method,
    ge.id AS germplasm_id,
    prog.id AS program_id,
    NULL::integer AS source_experiment_id,
    NULL::integer AS source_entry_id,
    NULL::integer AS source_occurrence_id,
    NULL::integer AS source_location_id,
    NULL::integer AS source_plot_id,
    NULL::integer AS cross_id,
    ((array['plot', 'cross'])[floor(random() * 2 + 1)]) AS harvest_source,
    ge.creator_id
FROM
    germplasm.germplasm AS ge
    JOIN tenant.crop AS crop
        ON crop.id = ge.crop_id
    JOIN tenant.program AS prog
        ON prog.program_code = 'PROGX',
    generate_series(1, 10) AS gs (num)
WHERE
    crop.crop_code = 'CROPX'
    AND ge.germplasm_state = 'not_fixed'
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.seed'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~1m 29s)
-- REINDEX TABLE germplasm.seed;

-- restore triggers and constraints
--ALTER TABLE germplasm.seed ENABLE TRIGGER ALL;
SET session_replication_role = origin;

-- total eta: ~205.664s (~3m 25s)
--*/



-- revert changes
--rollback --ALTER TABLE germplasm.seed DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.seed'::regclass::oid AND indisprimary = FALSE AND indisready = TRUE;
--rollback 
--rollback -- delete 2,500,000 records (eta: ~9.758s)
--rollback DELETE FROM
--rollback     germplasm.seed AS t
--rollback USING
--rollback     germplasm.germplasm AS ge
--rollback     JOIN tenant.crop AS crop
--rollback         ON crop.id = ge.crop_id
--rollback WHERE
--rollback     ge.id = t.germplasm_id
--rollback     AND crop.crop_code = 'CROPX'
--rollback     AND ge.germplasm_state = 'fixed'
--rollback ;
--rollback 
--rollback -- delete 7,500,000 records (eta: ~18.593s) 
--rollback DELETE FROM
--rollback     germplasm.seed AS t
--rollback USING
--rollback     germplasm.germplasm AS ge
--rollback     JOIN tenant.crop AS crop
--rollback         ON crop.id = ge.crop_id
--rollback WHERE
--rollback     ge.id = t.germplasm_id
--rollback     AND crop.crop_code = 'CROPX'
--rollback     AND ge.germplasm_state = 'not_fixed'
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.seed'::regclass::oid AND indisprimary = FALSE AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~17.940s)
--rollback REINDEX TABLE germplasm.seed;
--rollback 
--rollback --ALTER TABLE germplasm.seed ENABLE TRIGGER ALL;
--rollback 
--rollback -- total eta: ~46.291s



--changeset postgres:insert_seed_attributes context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-456 Insert seed attributes



--# germplasm.seed_attribute

--/* insert
-- disable triggers and constraints to speed up data insertions
--ALTER TABLE germplasm.seed_attribute DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.seed_attribute'::regclass::oid AND indisready = TRUE;

-- insert 1,071,340 (random) records to table (eta: ~4.44s)
INSERT INTO
    germplasm.seed_attribute (
        seed_id,
        variable_id,
        data_value,
        data_qc_code,
        creator_id
    )
SELECT
    sd.id AS seed_id,
    var.id AS variable_id,
    t.data_value,
    t.data_qc_code,
    crtr.id AS creator_id
FROM (
        VALUES
        ('USE', 'Active', 'G', 1)
    ) AS t (
        variable_id,
        data_value,
        data_qc_code,
        creator_id
    )
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
    JOIN master.variable AS var
        ON var.abbrev = t.variable_id
    JOIN tenant.crop AS crop
        ON crop.crop_code = 'CROPX'
    JOIN germplasm.germplasm AS ge
        ON ge.crop_id = crop.id
        AND ge.germplasm_state = 'not_fixed'
        AND ge.generation = 'F1'
    JOIN germplasm.seed AS sd
        ON sd.germplasm_id = ge.id
;

-- insert 3,209,320 (random) records to table (eta: ~11.185s)
INSERT INTO
    germplasm.seed_attribute (
        seed_id,
        variable_id,
        data_value,
        data_qc_code,
        creator_id
    )
SELECT
    sd.id AS seed_id,
    var.id AS variable_id,
    ((array['Active', 'Regular'])[floor(random() * 2 + 1)]) AS data_value,
    t.data_qc_code,
    crtr.id AS creator_id
FROM (
        VALUES
        ('USE', 'G', 1)
    ) AS t (
        variable_id,
        data_qc_code,
        creator_id
    )
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
    JOIN master.variable AS var
        ON var.abbrev = t.variable_id
    JOIN tenant.crop AS crop
        ON crop.crop_code = 'CROPX'
    JOIN germplasm.germplasm AS ge
        ON ge.crop_id = crop.id
        AND ge.germplasm_state = 'not_fixed'
        AND ge.generation IN ('F2', 'F3', 'F4')
    JOIN germplasm.seed AS sd
        ON sd.germplasm_id = ge.id
;

-- insert 3,219,340 (random) records to table (eta: ~11.875s)
INSERT INTO
    germplasm.seed_attribute (
        seed_id,
        variable_id,
        data_value,
        data_qc_code,
        creator_id
    )
SELECT
    sd.id AS seed_id,
    var.id AS variable_id,
    ((array['Active', 'Regular'])[floor(random() * 2 + 1)]) AS data_value,
    t.data_qc_code,
    crtr.id AS creator_id
FROM (
        VALUES
        ('USE', 'G', 1)
    ) AS t (
        variable_id,
        data_qc_code,
        creator_id
    )
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
    JOIN master.variable AS var
        ON var.abbrev = t.variable_id
    JOIN tenant.crop AS crop
        ON crop.crop_code = 'CROPX'
    JOIN germplasm.germplasm AS ge
        ON ge.crop_id = crop.id
        AND ge.germplasm_state = 'not_fixed'
        AND ge.generation IN ('F5', 'F6', 'F7')
    JOIN germplasm.seed AS sd
        ON sd.germplasm_id = ge.id
;

-- insert 2,500,000 (random) records to table (eta: ~8.590s)
INSERT INTO
    germplasm.seed_attribute (
        seed_id,
        variable_id,
        data_value,
        data_qc_code,
        creator_id
    )
SELECT
    sd.id AS seed_id,
    var.id AS variable_id,
    ((array['Active', 'Regular'])[floor(random() * 2 + 1)]) AS data_value,
    t.data_qc_code,
    crtr.id AS creator_id
FROM (
        VALUES
        ('USE', 'G', 1)
    ) AS t (
        variable_id,
        data_qc_code,
        creator_id
    )
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
    JOIN master.variable AS var
        ON var.abbrev = t.variable_id
    JOIN tenant.crop AS crop
        ON crop.crop_code = 'CROPX'
    JOIN germplasm.germplasm AS ge
        ON ge.crop_id = crop.id
        AND ge.germplasm_state = 'fixed'
    JOIN germplasm.seed AS sd
        ON sd.germplasm_id = ge.id
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.seed_attribute'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~1m 2s)
-- REINDEX TABLE germplasm.seed_attribute;

-- restore triggers and constraints
--ALTER TABLE germplasm.seed_attribute ENABLE TRIGGER ALL;
SET session_replication_role = origin;

-- total eta: ~98.09s (~1m 38s)
--*/



-- revert changes
--rollback --ALTER TABLE germplasm.seed_attribute DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.seed_attribute'::regclass::oid AND indisprimary = FALSE AND indisready = TRUE;
--rollback 
--rollback -- delete 1,071,340 (random) records (eta: ~5.441s)
--rollback DELETE FROM
--rollback     germplasm.seed_attribute AS t
--rollback USING
--rollback     germplasm.germplasm AS ge
--rollback     JOIN tenant.crop AS crop
--rollback         ON crop.id = ge.crop_id
--rollback     JOIN germplasm.seed AS sd
--rollback         ON sd.germplasm_id = ge.id
--rollback WHERE
--rollback     sd.id = t.seed_id
--rollback     AND ge.germplasm_state = 'not_fixed'
--rollback     AND ge.generation = 'F1'
--rollback     AND crop.crop_code = 'CROPX'
--rollback ;
--rollback 
--rollback -- delete 2,500,000 (random) records (eta: ~13.237s)
--rollback DELETE FROM
--rollback     germplasm.seed_attribute AS t
--rollback USING
--rollback     germplasm.germplasm AS ge
--rollback     JOIN tenant.crop AS crop
--rollback         ON crop.id = ge.crop_id
--rollback     JOIN germplasm.seed AS sd
--rollback         ON sd.germplasm_id = ge.id
--rollback WHERE
--rollback     sd.id = t.seed_id
--rollback     AND ge.germplasm_state = 'fixed'
--rollback     AND crop.crop_code = 'CROPX'
--rollback ;
--rollback 
--rollback -- delete 3,209,320 (random) records (eta: ~12.26s)
--rollback DELETE FROM
--rollback     germplasm.seed_attribute AS t
--rollback USING
--rollback     germplasm.germplasm AS ge
--rollback     JOIN tenant.crop AS crop
--rollback         ON crop.id = ge.crop_id
--rollback     JOIN germplasm.seed AS sd
--rollback         ON sd.germplasm_id = ge.id
--rollback WHERE
--rollback     sd.id = t.seed_id
--rollback     AND ge.germplasm_state = 'not_fixed'
--rollback     AND ge.generation IN ('F2', 'F3', 'F4')
--rollback     AND crop.crop_code = 'CROPX'
--rollback ;
--rollback 
--rollback -- delete 3,219,340 (random) records (eta: ~26.698s)
--rollback DELETE FROM
--rollback     germplasm.seed_attribute AS t
--rollback USING
--rollback     germplasm.germplasm AS ge
--rollback     JOIN tenant.crop AS crop
--rollback         ON crop.id = ge.crop_id
--rollback     JOIN germplasm.seed AS sd
--rollback         ON sd.germplasm_id = ge.id
--rollback WHERE
--rollback     sd.id = t.seed_id
--rollback     AND ge.germplasm_state = 'not_fixed'
--rollback     AND ge.generation IN ('F5', 'F6', 'F7')
--rollback     AND crop.crop_code = 'CROPX'
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.seed_attribute'::regclass::oid AND indisprimary = FALSE AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~6.319s)
--rollback REINDEX TABLE germplasm.seed_attribute;
--rollback 
--rollback --ALTER TABLE germplasm.seed_attribute ENABLE TRIGGER ALL;
--rollback 
--rollback -- total eta: ~63.955s (~1m 3s)



--changeset postgres:insert_crosses context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-456 Insert crosses



--# germplasm.cross

--/* insert
-- disable triggers and constraints to speed up data insertions
--ALTER TABLE germplasm.cross DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.cross'::regclass::oid AND indisready = TRUE;

-- insert 125,000 records to table (eta: ~1.251s)
INSERT INTO
    germplasm.cross (
        cross_name,
        cross_method,
        germplasm_id,
        seed_id,
        experiment_id,
        entry_id,
        is_method_autofilled,
        harvest_status,
        creator_id
    )
SELECT
    c.cross_name,
    t.cross_method,
    NULL::integer AS germplasm_id,
    NULL::integer AS seed_id,
    NULL::integer AS experiment_id,
    NULL::integer AS entry_id,
    t.is_method_autofilled,
    t.harvest_status,
    crtr.id AS creator_id
FROM (
        VALUES
        ('single cross', FALSE, 'COMPLETED', 1)
    ) AS t (
        cross_method,
        is_method_autofilled,
        harvest_status,
        creator_id
    )
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id,
    (
        SELECT
            gef.cross_id,
            (gef.designation || '|' || gem.designation) AS cross_name
        FROM (
                SELECT
                    row_number() OVER () AS cross_id,
                    ge.*
                FROM
                    germplasm.germplasm AS ge
                    JOIN tenant.crop AS crop
                        ON crop.id = ge.crop_id
                WHERE
                    crop.crop_code = 'CROPX'
                    AND ge.germplasm_state = 'fixed'
                ORDER BY
                    ge.id
                LIMIT
                    125000
                OFFSET
                    0
            ) AS gef,
            (
                SELECT
                    row_number() OVER () - 125000 AS cross_id,
                    ge.*
                FROM
                    germplasm.germplasm AS ge
                    JOIN tenant.crop AS crop
                        ON crop.id = ge.crop_id
                WHERE
                    crop.crop_code = 'CROPX'
                    AND ge.germplasm_state = 'fixed'
                ORDER BY
                    ge.id
                LIMIT
                    125000
                OFFSET
                    125000
            ) AS gem
        WHERE
            gef.cross_id = gem.cross_id
    ) AS c
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.cross'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~0.681s)
-- REINDEX TABLE germplasm.cross;

-- restore triggers and constraints
--ALTER TABLE germplasm.cross ENABLE TRIGGER ALL;
SET session_replication_role = origin;

-- total eta: ~1.932s
--*/



-- revert changes
--rollback --ALTER TABLE germplasm.cross DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.cross'::regclass::oid AND indisready = TRUE;
--rollback 
--rollback -- delete 125,000 records (eta: ~0.297s)
--rollback DELETE FROM
--rollback     germplasm.cross AS crs
--rollback WHERE
--rollback     crs.cross_name ILIKE 'CXGE%'
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.cross'::regclass::oid AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~0.202s)
--rollback REINDEX TABLE germplasm.cross;
--rollback 
--rollback --ALTER TABLE germplasm.cross ENABLE TRIGGER ALL;
--rollback 
--rollback -- total eta: ~0.499s



--changeset postgres:insert_cross_parents context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-456 Insert cross parents



--# germplasm.cross_parent

--/* insert
-- disable triggers and constraints to speed up data insertions
--ALTER TABLE germplasm.cross_parent DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.cross_parent'::regclass::oid AND indisready = TRUE;

-- insert 250,000 records to table (eta: ~6.250s)
INSERT INTO
    germplasm.cross_parent (
        cross_id,
        germplasm_id,
        seed_id,
        parent_role,
        order_number,
        experiment_id,
        entry_id,
        plot_id,
        creator_id
    )
WITH cp AS (
    SELECT
        gef.cross_id,
        gef.germplasm_id AS female_germplasm_id,
        gem.germplasm_id AS male_germplasm_id,
        gef.seed_id AS female_seed_id,
        gem.seed_id AS male_seed_id,
        (gef.designation || '|' || gem.designation) AS cross_name
    FROM (
            SELECT
                row_number() OVER () AS cross_id,
                ge.id AS germplasm_id,
                ge.designation,
                (array_agg(sd.id))[1] AS seed_id
            FROM
                germplasm.germplasm AS ge
                JOIN tenant.crop AS crop
                    ON crop.id = ge.crop_id
                JOIN germplasm.seed AS sd
                    ON sd.germplasm_id = ge.id
            WHERE
                crop.crop_code = 'CROPX'
                AND ge.germplasm_state = 'fixed'
            GROUP BY
                ge.id
            ORDER BY
                ge.id
            LIMIT
                125000
            OFFSET
                0
        ) AS gef,
        (
            SELECT
                row_number() OVER () - 125000 AS cross_id,
                ge.id AS germplasm_id,
                ge.designation,
                (array_agg(sd.id))[1] AS seed_id
            FROM
                germplasm.germplasm AS ge
                JOIN tenant.crop AS crop
                    ON crop.id = ge.crop_id
                JOIN germplasm.seed AS sd
                    ON sd.germplasm_id = ge.id
            WHERE
                crop.crop_code = 'CROPX'
                AND ge.germplasm_state = 'fixed'
            GROUP BY
                ge.id
            ORDER BY
                ge.id
            LIMIT
                125000
            OFFSET
                125000
        ) AS gem
    WHERE
        gef.cross_id = gem.cross_id
)
SELECT
    crs.id AS cross_id,
    cp.female_germplasm_id AS germplasm_id,
    cp.female_seed_id AS seed_id,
    'female' AS parent_role,
    1 AS order_number,
    NULL::integer AS experiment_id,
    NULL::integer AS entry_id,
    NULL::integer AS plot_id,
    1 AS creator_id
FROM
    cp
    JOIN germplasm.cross AS crs
        ON crs.cross_name = cp.cross_name
UNION ALL
    SELECT
        crs.id AS cross_id,
        cp.male_germplasm_id AS germplasm_id,
        cp.male_seed_id AS seed_id,
        'male' AS parent_role,
        2 AS order_number,
        NULL::integer AS experiment_id,
        NULL::integer AS entry_id,
        NULL::integer AS plot_id,
        1 AS creator_id
    FROM
        cp
        JOIN germplasm.cross AS crs
            ON crs.cross_name = cp.cross_name
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.cross_parent'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~1.890s)
-- REINDEX TABLE germplasm.cross_parent;

-- restore triggers and constraints
--ALTER TABLE germplasm.cross_parent ENABLE TRIGGER ALL;
SET session_replication_role = origin;

-- total eta: ~8.14s
--*/



-- revert changes
--rollback --ALTER TABLE germplasm.cross_parent DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.cross_parent'::regclass::oid AND indisready = TRUE;
--rollback 
--rollback -- delete 250,000 records (eta: ~0.322s)
--rollback DELETE FROM
--rollback     germplasm.cross_parent AS crspar
--rollback USING
--rollback     germplasm.cross AS crs
--rollback WHERE
--rollback     crs.id = crspar.cross_id
--rollback     AND crs.cross_name LIKE 'CXGE%'
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.cross_parent'::regclass::oid AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~0.324s)
--rollback REINDEX TABLE germplasm.cross_parent;
--rollback 
--rollback --ALTER TABLE germplasm.cross_parent ENABLE TRIGGER ALL;
--rollback 
--rollback -- total eta: ~0.646s



--changeset postgres:insert_cross_data context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-456 Insert cross data



--# germplasm.cross_data

--/* insert
-- disable triggers and constraints to speed up data insertions
--ALTER TABLE germplasm.cross_data DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.cross_data'::regclass::oid AND indisready = TRUE;

-- insert 750,000 records to table (eta: ~2.521s)
INSERT INTO
    germplasm.cross_data (
        cross_id,
        variable_id,
        data_value,
        data_qc_code,
        collection_timestamp,
        creator_id
    )
SELECT
    t.cross_id,
    t.variable_id,
    t.data_value::date::varchar AS data_value,
    t.data_qc_code,
    t.data_value + INTERVAL '10 days' AS collection_timestamp,
    t.creator_id
FROM (
        SELECT
            crs.id AS cross_id,
            var.id AS variable_id,
            (timestamp '2016-01-01' + random() * (timestamp '2020-12-31' - timestamp '2016-01-01')) AS data_value,
            t.data_qc_code,
            crtr.id AS creator_id
        FROM (
                VALUES
                ('DATE_CROSSED', 'Q', 1),
                ('HVDATE_CONT', 'Q', 1)
            ) AS t (
                variable_id,
                data_qc_code,
                creator_id
            )
            JOIN master.variable AS var
                ON var.abbrev = t.variable_id
            JOIN tenant.person AS crtr
                ON crtr.id = t.creator_id
            JOIN germplasm.cross AS crs
                ON crs.cross_name LIKE 'CXGE%'
    ) AS t
UNION ALL
    SELECT
        crs.id AS cross_id,
        var.id AS variable_id,
        ((array['Bulk', 'Single Plant Selection', 'Plant-specific'])[floor(random() * 3 + 1)]) AS data_value,
        t.data_qc_code,
        (timestamp '2016-01-01' + random() * (timestamp '2020-12-31' - timestamp '2016-01-01')) AS collection_timestamp,
        crtr.id AS creator_id
    FROM (
            VALUES
            ('HV_METH_DISC', 'Q', 1)
        ) AS t (
            variable_id,
            data_qc_code,
            creator_id
        )
        JOIN master.variable AS var
            ON var.abbrev = t.variable_id
        JOIN tenant.person AS crtr
            ON crtr.id = t.creator_id
        JOIN germplasm.cross AS crs
            ON crs.cross_name LIKE 'CXGE%'
UNION ALL
    SELECT
        crs.id AS cross_id,
        var.id AS variable_id,
        (floor(random() * 100 + 1))::varchar AS data_value,
        t.data_qc_code,
        (timestamp '2016-01-01' + random() * (timestamp '2020-12-31' - timestamp '2016-01-01')) AS collection_timestamp,
        crtr.id AS creator_id
    FROM (
            VALUES
            ('NO_OF_PLANTS', 'Q', 1),
            ('NO_OF_SEED', 'Q', 1),
            ('NO_OF_EARS', 'Q', 1)
        ) AS t (
            variable_id,
            data_qc_code,
            creator_id
        )
        JOIN master.variable AS var
            ON var.abbrev = t.variable_id
        JOIN tenant.person AS crtr
            ON crtr.id = t.creator_id
        JOIN germplasm.cross AS crs
            ON crs.cross_name LIKE 'CXGE%'
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.cross_data'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~3.528s)
-- REINDEX TABLE germplasm.cross_data;

-- restore triggers and constraints
--ALTER TABLE germplasm.cross_data ENABLE TRIGGER ALL;
SET session_replication_role = origin;

-- total eta: ~6.049s
--*/



-- revert changes
--rollback --ALTER TABLE germplasm.cross_data DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.cross_data'::regclass::oid AND indisready = TRUE;
--rollback 
--rollback -- delete 750,000 records (eta: ~1.590s)
--rollback DELETE FROM
--rollback     germplasm.cross_data AS crsdata
--rollback USING
--rollback     germplasm.cross AS crs
--rollback WHERE
--rollback     crs.id = crsdata.cross_id
--rollback     AND crs.cross_name LIKE 'CXGE%'
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.cross_data'::regclass::oid AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~0.484s)
--rollback REINDEX TABLE germplasm.cross_data;
--rollback 
--rollback --ALTER TABLE germplasm.cross_data ENABLE TRIGGER ALL;
--rollback 
--rollback -- total eta: ~2.074s



--changeset postgres:insert_packages context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-456 Insert packages



--# germplasm.package

--/* insert
-- disable triggers and constraints to speed up data insertions
--ALTER TABLE germplasm.package DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.package'::regclass::oid AND indisready = TRUE;

-- insert 2,500,000 records to table (eta: ~26.651s)
INSERT INTO
    germplasm.package (
        package_code,
        package_label,
        package_quantity,
        package_unit,
        package_status,
        seed_id,
        program_id,
        geospatial_object_id,
        facility_id,
        creator_id
    )
SELECT
    germplasm.generate_code('package') AS package_code,
    (sd.seed_name || t.package_label || gs.num) AS package_label,
    floor(random() * 300) AS package_quantity,
    t.package_unit,
    t.package_status,
    sd.id AS seed_id,
    prog.id AS program_id,
    NULL AS geospatial_object_id,
    NULL AS facility_id,
    crtr.id AS creator_id
FROM (
        VALUES
        ('-PKG', 'g', 'active', 1)
    ) AS t (
        package_label,
        package_unit,
        package_status,
        creator_id
    )
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
    JOIN tenant.crop AS crop
        ON crop.crop_code = 'CROPX'
    JOIN tenant.program AS prog
        ON prog.program_code = 'PROGX'
    JOIN germplasm.germplasm AS ge
        ON ge.crop_id = crop.id
    JOIN germplasm.seed AS sd
        ON sd.germplasm_id = ge.id,
    generate_series(1, 1) AS gs (num)
WHERE
    ge.germplasm_state = 'fixed'
;

-- insert 7,500,000 records to table (eta: ~1m 28s)
INSERT INTO
    germplasm.package (
        package_code,
        package_label,
        package_quantity,
        package_unit,
        package_status,
        seed_id,
        program_id,
        geospatial_object_id,
        facility_id,
        creator_id
    )
SELECT
    germplasm.generate_code('package') AS package_code,
    (sd.seed_name || t.package_label || gs.num) AS package_label,
    floor(random() * 300) AS package_quantity,
    t.package_unit,
    t.package_status,
    sd.id AS seed_id,
    prog.id AS program_id,
    NULL AS geospatial_object_id,
    NULL AS facility_id,
    crtr.id AS creator_id
FROM (
        VALUES
        ('-PKG', 'g', 'active', 1)
    ) AS t (
        package_label,
        package_unit,
        package_status,
        creator_id
    )
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
    JOIN tenant.crop AS crop
        ON crop.crop_code = 'CROPX'
    JOIN tenant.program AS prog
        ON prog.program_code = 'PROGX'
    JOIN germplasm.germplasm AS ge
        ON ge.crop_id = crop.id
    JOIN germplasm.seed AS sd
        ON sd.germplasm_id = ge.id,
    generate_series(1, 1) AS gs (num)
WHERE
    ge.germplasm_state = 'not_fixed'
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.package'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~1m 4s)
-- REINDEX TABLE germplasm.package;

-- restore triggers and constraints
--ALTER TABLE germplasm.package ENABLE TRIGGER ALL;
SET session_replication_role = origin;
ALTER TABLE germplasm.package DISABLE TRIGGER package_update_package_document_tgr;

-- total eta: ~2m 58s
--*/



-- revert changes
--rollback --ALTER TABLE germplasm.package DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.package'::regclass::oid AND indisready = TRUE;
--rollback 
--rollback -- delete 2,500,000 records (eta: ~42.361s)
--rollback DELETE FROM
--rollback     germplasm.package AS pkg
--rollback USING (
--rollback         SELECT
--rollback             pkg.id
--rollback         FROM
--rollback             tenant.crop AS crop
--rollback             JOIN germplasm.germplasm AS ge
--rollback                 ON ge.crop_id = crop.id
--rollback             JOIN germplasm.seed AS sd
--rollback                 ON sd.germplasm_id = ge.id
--rollback             JOIN germplasm.package AS pkg
--rollback                 ON pkg.seed_id = sd.id
--rollback         WHERE
--rollback             crop.crop_code = 'CROPX'
--rollback             AND ge.germplasm_state = 'fixed'
--rollback         LIMIT
--rollback             2500000
--rollback     ) AS t
--rollback WHERE
--rollback     t.id = pkg.id
--rollback ;
--rollback 
--rollback -- delete 2,500,000 records (eta: ~2m 14s)
--rollback DELETE FROM
--rollback     germplasm.package AS pkg
--rollback USING (
--rollback         SELECT
--rollback             pkg.id
--rollback         FROM
--rollback             tenant.crop AS crop
--rollback             JOIN germplasm.germplasm AS ge
--rollback                 ON ge.crop_id = crop.id
--rollback             JOIN germplasm.seed AS sd
--rollback                 ON sd.germplasm_id = ge.id
--rollback             JOIN germplasm.package AS pkg
--rollback                 ON pkg.seed_id = sd.id
--rollback         WHERE
--rollback             crop.crop_code = 'CROPX'
--rollback             AND ge.germplasm_state = 'not_fixed'
--rollback         LIMIT
--rollback             2500000
--rollback     ) AS t
--rollback WHERE
--rollback     t.id = pkg.id
--rollback ;
--rollback 
--rollback -- delete 2,500,000 records (eta: ~1m 2s)
--rollback DELETE FROM
--rollback     germplasm.package AS pkg
--rollback USING (
--rollback         SELECT
--rollback             pkg.id
--rollback         FROM
--rollback             tenant.crop AS crop
--rollback             JOIN germplasm.germplasm AS ge
--rollback                 ON ge.crop_id = crop.id
--rollback             JOIN germplasm.seed AS sd
--rollback                 ON sd.germplasm_id = ge.id
--rollback             JOIN germplasm.package AS pkg
--rollback                 ON pkg.seed_id = sd.id
--rollback         WHERE
--rollback             crop.crop_code = 'CROPX'
--rollback             AND ge.germplasm_state = 'not_fixed'
--rollback         LIMIT
--rollback             2500000
--rollback     ) AS t
--rollback WHERE
--rollback     t.id = pkg.id
--rollback ;
--rollback 
--rollback -- delete 2,500,000 records (eta: ~55.587s)
--rollback DELETE FROM
--rollback     germplasm.package AS pkg
--rollback USING (
--rollback         SELECT
--rollback             pkg.id
--rollback         FROM
--rollback             tenant.crop AS crop
--rollback             JOIN germplasm.germplasm AS ge
--rollback                 ON ge.crop_id = crop.id
--rollback             JOIN germplasm.seed AS sd
--rollback                 ON sd.germplasm_id = ge.id
--rollback             JOIN germplasm.package AS pkg
--rollback                 ON pkg.seed_id = sd.id
--rollback         WHERE
--rollback             crop.crop_code = 'CROPX'
--rollback             AND ge.germplasm_state = 'not_fixed'
--rollback         LIMIT
--rollback             2500000
--rollback     ) AS t
--rollback WHERE
--rollback     t.id = pkg.id
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.package'::regclass::oid AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~10.825s)
--rollback REINDEX TABLE germplasm.package;
--rollback 
--rollback --ALTER TABLE germplasm.package ENABLE TRIGGER ALL;
--rollback ALTER TABLE germplasm.package DISABLE TRIGGER package_update_package_document_tgr;
--rollback 
--rollback -- total eta: ~5m 4s



--changeset postgres:insert_package_logs context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-456 Insert package logs



--# germplasm.package_log

--/* insert
-- fix id sequence
SELECT SETVAL('germplasm.package_log_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM germplasm.package_log;

-- disable triggers and constraints to speed up data insertions
--ALTER TABLE germplasm.package_log DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.package_log'::regclass::oid AND indisready = TRUE;

-- insert 10,000,000 records to table (eta: ~50.45s)
INSERT INTO
    germplasm.package_log (
        package_id,
        package_quantity,
        package_unit,
        package_transaction_type,
        entity_id,
        data_id,
        creator_id
    )
SELECT
    pkg.id AS package_id,
    pkg.package_quantity,
    pkg.package_unit,
    t.package_transaction_type,
    enty.id AS entity_id,
    t.data_id::integer AS data_id,
    crtr.id AS creator_id
FROM (
        VALUES
        ('deposit', 'PLOT', NULL, 1)
    ) AS t (
        package_transaction_type,
        entity_id,
        data_id,
        creator_id
    )
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
    JOIN dictionary.entity AS enty
        ON enty.abbrev = t.entity_id
    JOIN tenant.crop AS crop
        ON crop.crop_code = 'CROPX'
    JOIN germplasm.germplasm AS ge
        ON ge.crop_id = crop.id
    JOIN germplasm.seed AS sd
        ON sd.germplasm_id = ge.id
    LEFT JOIN germplasm.package AS pkg
        ON pkg.seed_id = sd.id
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.package_log'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~45.347s)
-- REINDEX TABLE germplasm.package_log;

-- restore triggers and constraints
--ALTER TABLE germplasm.package_log ENABLE TRIGGER ALL;
SET session_replication_role = origin;

-- total eta: ~1m 35s
--*/



-- revert changes
--rollback --ALTER TABLE germplasm.package_log DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.package_log'::regclass::oid AND indisready = TRUE;
--rollback 
--rollback -- delete 2,500,000 records (eta: ~42.863s)
--rollback DELETE FROM
--rollback     germplasm.package_log AS pkglog
--rollback USING (
--rollback         SELECT
--rollback             pkglog.id
--rollback         FROM
--rollback             tenant.crop AS crop
--rollback             JOIN germplasm.germplasm AS ge
--rollback                 ON ge.crop_id = crop.id
--rollback             JOIN germplasm.seed AS sd
--rollback                 ON sd.germplasm_id = ge.id
--rollback             JOIN germplasm.package AS pkg
--rollback                 ON pkg.seed_id = sd.id
--rollback             JOIN germplasm.package_log AS pkglog
--rollback                 ON pkglog.package_id = pkg.id
--rollback         WHERE
--rollback             crop.crop_code = 'CROPX'
--rollback         LIMIT
--rollback             2500000
--rollback     ) AS t
--rollback WHERE
--rollback     t.id = pkglog.id
--rollback ;
--rollback 
--rollback -- delete 2,500,000 records (eta: ~52.417s)
--rollback DELETE FROM
--rollback     germplasm.package_log AS pkglog
--rollback USING (
--rollback         SELECT
--rollback             pkglog.id
--rollback         FROM
--rollback             tenant.crop AS crop
--rollback             JOIN germplasm.germplasm AS ge
--rollback                 ON ge.crop_id = crop.id
--rollback             JOIN germplasm.seed AS sd
--rollback                 ON sd.germplasm_id = ge.id
--rollback             JOIN germplasm.package AS pkg
--rollback                 ON pkg.seed_id = sd.id
--rollback             JOIN germplasm.package_log AS pkglog
--rollback                 ON pkglog.package_id = pkg.id
--rollback         WHERE
--rollback             crop.crop_code = 'CROPX'
--rollback         LIMIT
--rollback             2500000
--rollback     ) AS t
--rollback WHERE
--rollback     t.id = pkglog.id
--rollback ;
--rollback 
--rollback -- delete 2,500,000 records (eta: ~1m 33s)
--rollback DELETE FROM
--rollback     germplasm.package_log AS pkglog
--rollback USING (
--rollback         SELECT
--rollback             pkglog.id
--rollback         FROM
--rollback             tenant.crop AS crop
--rollback             JOIN germplasm.germplasm AS ge
--rollback                 ON ge.crop_id = crop.id
--rollback             JOIN germplasm.seed AS sd
--rollback                 ON sd.germplasm_id = ge.id
--rollback             JOIN germplasm.package AS pkg
--rollback                 ON pkg.seed_id = sd.id
--rollback             JOIN germplasm.package_log AS pkglog
--rollback                 ON pkglog.package_id = pkg.id
--rollback         WHERE
--rollback             crop.crop_code = 'CROPX'
--rollback         LIMIT
--rollback             2500000
--rollback     ) AS t
--rollback WHERE
--rollback     t.id = pkglog.id
--rollback ;
--rollback 
--rollback -- delete 2,500,000 records (eta: ~1m)
--rollback DELETE FROM
--rollback     germplasm.package_log AS pkglog
--rollback USING (
--rollback         SELECT
--rollback             pkglog.id
--rollback         FROM
--rollback             tenant.crop AS crop
--rollback             JOIN germplasm.germplasm AS ge
--rollback                 ON ge.crop_id = crop.id
--rollback             JOIN germplasm.seed AS sd
--rollback                 ON sd.germplasm_id = ge.id
--rollback             JOIN germplasm.package AS pkg
--rollback                 ON pkg.seed_id = sd.id
--rollback             JOIN germplasm.package_log AS pkglog
--rollback                 ON pkglog.package_id = pkg.id
--rollback         WHERE
--rollback             crop.crop_code = 'CROPX'
--rollback         LIMIT
--rollback             2500000
--rollback     ) AS t
--rollback WHERE
--rollback     t.id = pkglog.id
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.package_log'::regclass::oid AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~7.440s)
--rollback REINDEX TABLE germplasm.package_log;
--rollback 
--rollback --ALTER TABLE germplasm.package_log ENABLE TRIGGER ALL;
--rollback 
--rollback -- total eta: ~4m 15s
