--liquibase formatted sql

--changeset postgres:populate_mta_status_and_rsht context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1958 Populate mta_status and rsht



--populate mta
INSERT INTO 
    germplasm.germplasm_mta 
        (germplasm_id, seed_id, mta_status, use, creator_id, notes)
SELECT 
    gg.id AS germplasm_id,
    gs.id AS seed_id,
    t.mta_status AS mta_status,
    'Active' AS use,
    1 AS creator_id,
    'DEVOPS-1958 Populate mta_status and rsht'
FROM
    (
        VALUES
            ('IR12A329','PUD1','300266828'),
            ('IR12A185','PUD1','300266833'),
            ('IR10F559','PUD1','300266839'),
            ('IR13A294','PUD1','300266838'),
            ('IR13A107','PUD1','300266835'),
            ('IR12A341','PUD1','300266825'),
            ('IR11A314','PUD1','300266857'),
            ('IR12A258','PUD1','300266829'),
            ('IR13N115','PUD1','300266831'),
            ('IRRI 168','Rel1','1402027'),
            ('IRRI 156','Rel1','4016513'),
            ('IRRI 146','Rel1','4016518'),
            ('IR12A289','PUD1','300266836'),
            ('IR06A145','Rel1','1960898'),
            ('IR12N281','PUD1','300266827'),
            ('IR12N198','PUD1','300266840'),
            ('IR13N103','PUD1','300266824'),
            ('IRRI 154','Rel1','4016512'),
            ('IR11L236','PUD1','300286272'),
            ('IR12A334','PUD1','300266823'),
            ('IRRI 105','Rel1','4039769'),
            ('IR12A223','PUD1','300266843'),
            ('IR12N205','PUD1','3426130'),
            ('IR14A512','PUD1','4216878'),
            ('IRRI 161','Rel1','4284388'),
            ('PIR-26>C0-2071-1-4-2-1','PUD2','4537049'),
            ('IR14A523','PUD1','4216993'),
            ('IR14V1029','PUD1','4189068'),
            ('NSIC 2011 RC 282','SMTA','4285748'),
            ('SL 8','SMTA','300266703')
    ) AS t(designation, mta_status, seed_name)
    INNER JOIN germplasm.germplasm gg
    ON
        gg.designation = t.designation
    LEFT JOIN germplasm.seed gs
    ON
        gs.seed_name = t.seed_name
;

--populate seed_data
INSERT INTO
    germplasm.seed_data 
        (seed_id, variable_id, data_value, data_qc_code, notes, creator_id)
SELECT 
    s.id AS seed_id,
    v.id AS variable_id,
    t.rsht AS data_value,
    'G' AS data_qc_code,
    'DEVOPS-1958 Populate mta_status and rsht' AS notes,
    1 AS creator_id 
FROM 
    (
        VALUES 
            ('300266828','AT22-022-9-Exp230530'),
            ('300266833','AT22-022-5-Exp230530'),
            ('300266839','AT22-022-2-Exp230530'),
            ('300266838','AT22-022-16-Exp230530'),
            ('300266835','AT22-022-15-Exp230530'),
            ('300266825','AT22-022-11-Exp230530'),
            ('300266857','AT22-022-3-Exp230530'),
            ('300266829','AT22-022-7-Exp230530'),
            ('300266831','AT22-022-18-Exp230530'),
            ('1402027','AT22-022-27-Exp230530'),
            ('4016513','AT22-022-25-Exp230530'),
            ('4016518','AT22-022-23-Exp230530'),
            ('300266836','AT22-022-8-Exp230530'),
            ('1960898','AT22-022-1-Exp230530'),
            ('300266827','AT22-022-14-Exp230530'),
            ('300266840','AT22-022-12-Exp230530'),
            ('300266824','AT22-022-17-Exp230530'),
            ('4016512','AT22-022-24-Exp230530'),
            ('300286272','AT22-022-4-Exp230530'),
            ('300266823','AT22-022-10-Exp230530'),
            ('4039769','AT22-022-22-Exp230530'),
            ('300266843','AT22-022-6-Exp230530'),
            ('3426130','AT22-022-13-Exp230530'),
            ('4216878','AT22-022-19-Exp230530'),
            ('4284388','AT22-022-26-Exp230530'),
            ('4537049','AT22-022-29-Exp230530'),
            ('4216993','AT22-022-20-Exp230530'),
            ('4189068','AT22-022-21-Exp230530'),
            ('4285748','AT22-022-28-Exp230530'),
            ('300266703','AT22-022-30-Exp230530')
    ) AS t(seed_name,rsht)
JOIN 
    germplasm.seed s 
ON
    s.seed_name = t.seed_name
JOIN
    master.variable v 
ON
    abbrev = 'RSHT_NO' 
;



-- revert changes
--rollback DELETE FROM germplasm.seed_data WHERE notes = 'DEVOPS-1958 Populate mta_status and rsht';
--rollback DELETE FROM germplasm.germplasm_mta WHERE notes ='DEVOPS-1958 Populate mta_status and rsht';