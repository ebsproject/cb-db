--liquibase formatted sql

--changeset postgres:4_populate_occurrence_ba_exp_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1716 Populate occurrence BA_EXP_1



INSERT INTO 
    experiment.occurrence
        (occurrence_code,occurrence_name,occurrence_status,description,experiment_id,geospatial_object_id,creator_id,rep_count,site_id,field_id,occurrence_number,remarks,entry_count,plot_count)
SELECT
    occurrence_code,occurrence_name,occurrence_status,description,experiment_id,geospatial_object_id::int,creator_id,rep_count::int,site_id,field_id::int,occurrence_number,remarks,entry_count,plot_count
FROM
    (
        VALUES
            ('EXP0017556-001','AF2023_AL_T25_V2-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_code='EXP0017556'),NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRIHQ'),NULL,1,NULL,25,75),
            ('EXP0017556-002','AF2023_AL_T25_V2-002','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_code='EXP0017556'),NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH_BG_LT'),NULL,2,NULL,25,75),
            ('EXP0017556-003','AF2023_AL_T25_V2-003','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_code='EXP0017556'),NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH_MA_LB'),NULL,3,NULL,25,75),
            ('EXP0017556-004','AF2023_AL_T25_V2-004','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_code='EXP0017556'),NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IN_UP_KN'),NULL,4,NULL,25,75),
            ('EXP0017556-005','AF2023_AL_T25_V2-005','draft',NULL,(SELECT id FROM experiment.experiment WHERE experiment_code='EXP0017556'),NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH_NC_MD'),NULL,5,NULL,0,0))
     t (occurrence_code,occurrence_name,occurrence_status,description,experiment_id,geospatial_object_id,creator_id,rep_count,site_id,field_id,occurrence_number,remarks,entry_count,plot_count)
;



--rollback DELETE FROM
--rollback    experiment.occurrence
--rollback WHERE
--rollback    occurrence_code
--rollback IN
--rollback    (
--rollback        'EXP0017556-001',
--rollback        'EXP0017556-002',
--rollback        'EXP0017556-003',
--rollback        'EXP0017556-004',
--rollback        'EXP0017556-005'
--rollback    )
--rollback ;