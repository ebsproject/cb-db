--liquibase formatted sql

--changeset postgres:populate_experiment context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1834 Populate experiment



INSERT INTO 
    experiment.experiment
        (program_id,pipeline_id,stage_id,project_id,experiment_year,season_id,planting_season,experiment_code,experiment_name,experiment_objective,experiment_type,experiment_sub_type,experiment_sub_sub_type,experiment_design_type,experiment_status,description,steward_id,experiment_plan_id,creator_id,data_process_id,crop_id,remarks)
SELECT
    program_id,pipeline_id::int,stage_id,project_id::int,experiment_year,season_id,planting_season,experiment_code,experiment_name,experiment_objective,experiment_type,experiment_sub_type,experiment_sub_sub_type,experiment_design_type,experiment_status,description,steward_id,experiment_plan_id::int,creator_id,data_process_id,crop_id,remarks
FROM
    (
        VALUES
            ((SELECT id FROM tenant.program WHERE program_code='IRSEA'),NULL,(SELECT id FROM tenant.stage WHERE stage_code='F5'),NULL,2020,(SELECT id FROM tenant.season WHERE season_code='DS'),NULL,'EXP0000350','CROSS5_SC F5',NULL,'Generation Nursery',NULL,NULL,'Entry Order','planted',NULL,(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),NULL,(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS'),(SELECT id FROM tenant.crop WHERE crop_code='RICE'),NULL))
     t (program_id,pipeline_id,stage_id,project_id,experiment_year,season_id,planting_season,experiment_code,experiment_name,experiment_objective,experiment_type,experiment_sub_type,experiment_sub_sub_type,experiment_design_type,experiment_status,description,steward_id,experiment_plan_id,creator_id,data_process_id,crop_id,remarks)
;



--rollback DELETE FROM
--rollback    experiment.experiment
--rollback WHERE
--rollback    experiment_code
--rollback IN
--rollback    (
--rollback        'EXP0000350'
--rollback    )
--rollback ;