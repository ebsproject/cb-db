--liquibase formatted sql

--changeset postgres:populate_location_occurrence_group context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1834 Populate location_occurrence_group



INSERT INTO 
    experiment.location_occurrence_group
        (location_id,occurrence_id,order_number,creator_id)
SELECT
    location_id,occurrence_id,order_number,creator_id
FROM
    (
        VALUES
            ((SELECT id FROM experiment.location WHERE location_code='IRRIHQ-2020-DS-012'),(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0000350-001'),1,(SELECT id FROM tenant.person WHERE person_name='EBS, Admin')))
     t (location_id,occurrence_id,order_number,creator_id)
;



--rollback DELETE FROM
--rollback    experiment.location_occurrence_group
--rollback WHERE
--rollback    location_id
--rollback IN
--rollback    (
--rollback        (SELECT id FROM experiment.location WHERE location_code='IRRIHQ-2020-DS-012')
--rollback    )
--rollback ;