--liquibase formatted sql

--changeset postgres:24_populate_location_occurrence_group_ba_exp_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1716 Populate location_occurrence_group BA_EXP_1



INSERT INTO 
    experiment.location_occurrence_group
        (location_id,occurrence_id,order_number,creator_id)
SELECT
    location_id,occurrence_id,order_number,creator_id
FROM
    (
        VALUES
            ((SELECT id FROM experiment.location WHERE location_code='IN_UP_KN-2023-DS-001'),(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0017556-004'),1,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')))
     t (location_id,occurrence_id,order_number,creator_id)
;



--rollback DELETE FROM
--rollback    experiment.location_occurrence_group
--rollback WHERE
--rollback    location_id
--rollback IN
--rollback    (
--rollback        SELECT id FROM experiment.location WHERE location_code='IN_UP_KN-2023-DS-001'
--rollback    )
--rollback ;