--liquibase formatted sql

--changeset postgres:1_populate_experiment_ba_exp_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1716 Populate experiment BA_EXP_2



INSERT INTO 
    experiment.experiment
        (program_id,pipeline_id,stage_id,project_id,experiment_year,season_id,planting_season,experiment_code,experiment_name,experiment_objective,experiment_type,experiment_sub_type,experiment_sub_sub_type,experiment_design_type,experiment_status,description,steward_id,experiment_plan_id,creator_id,data_process_id,crop_id,remarks)
SELECT
    program_id,pipeline_id,stage_id,project_id::int,experiment_year,season_id,planting_season,experiment_code,experiment_name,experiment_objective,experiment_type,experiment_sub_type,experiment_sub_sub_type,experiment_design_type,experiment_status,description,steward_id,experiment_plan_id::int,creator_id,data_process_id,crop_id,remarks
FROM
    (
        VALUES
            ((SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.pipeline WHERE pipeline_code='IRSEA_PIPELINE'),(SELECT id FROM tenant.stage WHERE stage_code='IYT'),NULL,2023,(SELECT id FROM tenant.season WHERE season_code='DS'),NULL,'EXP0017557','AF2023_AL_T30_V2a',NULL,'Breeding Trial',NULL,NULL,'Alpha-Lattice','planted',NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),(SELECT id FROM master.item WHERE abbrev='BREEDING_TRIAL_DATA_PROCESS'),(SELECT id FROM tenant.crop WHERE crop_code='RICE'),NULL))
     t (program_id,pipeline_id,stage_id,project_id,experiment_year,season_id,planting_season,experiment_code,experiment_name,experiment_objective,experiment_type,experiment_sub_type,experiment_sub_sub_type,experiment_design_type,experiment_status,description,steward_id,experiment_plan_id,creator_id,data_process_id,crop_id,remarks)
;



--rollback DELETE FROM
--rollback    experiment.experiment
--rollback WHERE
--rollback    experiment_code
--rollback IN
--rollback    (
--rollback        'EXP0017557'
--rollback    )
--rollback ;