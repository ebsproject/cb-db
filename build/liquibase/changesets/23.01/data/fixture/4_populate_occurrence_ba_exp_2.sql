--liquibase formatted sql

--changeset postgres:4_populate_occurrence_ba_exp_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1716 Populate occurrence BA_EXP_2



INSERT INTO 
    experiment.occurrence
        (occurrence_code,occurrence_name,occurrence_status,description,experiment_id,geospatial_object_id,creator_id,rep_count,site_id,field_id,occurrence_number,remarks,entry_count,plot_count)
SELECT
    occurrence_code,occurrence_name,occurrence_status,description,experiment_id,geospatial_object_id::int,creator_id,rep_count::int,site_id,field_id::int,occurrence_number,remarks,entry_count,plot_count
FROM
    (
        VALUES
            ('EXP0017557-001','AF2023_AL_T30_V2a-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_code='EXP0017557'),NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRIHQ'),NULL,1,NULL,30,90),
            ('EXP0017557-002','AF2023_AL_T30_V2a-002','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_code='EXP0017557'),NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH_NE_SM2'),NULL,2,NULL,30,90),
            ('EXP0017557-003','AF2023_AL_T30_V2a-003','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_code='EXP0017557'),NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH_AN_RM'),NULL,3,NULL,30,90),
            ('EXP0017557-004','AF2023_AL_T30_V2a-004','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_code='EXP0017557'),NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH_BO_UA'),NULL,4,NULL,30,90))
     t (occurrence_code,occurrence_name,occurrence_status,description,experiment_id,geospatial_object_id,creator_id,rep_count,site_id,field_id,occurrence_number,remarks,entry_count,plot_count)
;



--rollback DELETE FROM
--rollback    experiment.occurrence
--rollback WHERE
--rollback    occurrence_code
--rollback IN
--rollback    (
--rollback        'EXP0017557-001',
--rollback        'EXP0017557-002',
--rollback        'EXP0017557-003',
--rollback        'EXP0017557-004'
--rollback    )
--rollback ;