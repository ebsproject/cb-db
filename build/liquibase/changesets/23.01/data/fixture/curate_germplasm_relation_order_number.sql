--liquibase formatted sql

--changeset postgres:curate_germplasm_relation_order_number context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1856 Curate germplasm_relation order_number



WITH t1 AS (
    SELECT 
        gr.id
        --gr.parent_germplasm_id,
        --gr.child_germplasm_id,
        --gr.order_number 
    FROM
        germplasm.germplasm g 
    JOIN
        germplasm.germplasm_relation gr 
    ON
        gr.parent_germplasm_id  = g.id 
    WHERE 
        crop_id = (SELECT id FROM tenant.crop c WHERE c.crop_code='BARLEY')
    AND 
        mod(gr.id,2)=0
    ORDER BY 
        gr.id
) 
UPDATE 
    germplasm.germplasm_relation AS gr
SET
    order_number = 2
FROM 
    t1
WHERE 
    gr.id = t1.id 
;



-- revert changes
--rollback WITH t1 AS (
--rollback     SELECT 
--rollback         gr.id
--rollback         --gr.parent_germplasm_id,
--rollback         --gr.child_germplasm_id,
--rollback         --gr.order_number 
--rollback     FROM
--rollback         germplasm.germplasm g 
--rollback     JOIN
--rollback         germplasm.germplasm_relation gr 
--rollback     ON
--rollback         gr.parent_germplasm_id  = g.id 
--rollback     WHERE 
--rollback         crop_id = (SELECT id FROM tenant.crop c WHERE c.crop_code='BARLEY')
--rollback     AND 
--rollback         mod(gr.id,2)=0
--rollback     ORDER BY 
--rollback         gr.id
--rollback ) 
--rollback UPDATE 
--rollback     germplasm.germplasm_relation AS gr
--rollback SET
--rollback     order_number = 1
--rollback FROM 
--rollback     t1
--rollback WHERE 
--rollback     gr.id = t1.id 
--rollback ;