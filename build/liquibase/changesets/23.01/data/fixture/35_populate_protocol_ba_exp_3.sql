--liquibase formatted sql

--changeset postgres:35_populate_protocol_ba_exp_3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1716 Populate protocol BA_EXP_3



INSERT INTO 
    tenant.protocol
        (protocol_code,protocol_name,protocol_type,description,program_id,creator_id)
SELECT
    protocol_code,protocol_name,protocol_type,description,program_id,creator_id
FROM
    (
        VALUES
            ('HARVEST_PROTOCOL_EXP0018544','Harvest Protocol Exp0018544','harvest',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ('PLANTING_PROTOCOL_EXP0018544','Planting Protocol Exp0018544','planting',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ('TRAIT_PROTOCOL_EXP0018544','Trait Protocol Exp0018544','trait',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ('MANAGEMENT_PROTOCOL_EXP0018544','Management Protocol Exp0018544','management',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')))
     t (protocol_code,protocol_name,protocol_type,description,program_id,creator_id)
;



--rollback DELETE FROM
--rollback    tenant.protocol
--rollback WHERE
--rollback    protocol_code
--rollback IN
--rollback    (
--rollback        'HARVEST_PROTOCOL_EXP0018544',
--rollback        'PLANTING_PROTOCOL_EXP0018544',
--rollback        'TRAIT_PROTOCOL_EXP0018544',
--rollback        'MANAGEMENT_PROTOCOL_EXP0018544'
--rollback    )
--rollback ;