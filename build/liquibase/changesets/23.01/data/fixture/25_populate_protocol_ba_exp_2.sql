--liquibase formatted sql

--changeset postgres:25_populate_protocol_ba_exp_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1716 Populate protocol BA_EXP_2



INSERT INTO 
    tenant.protocol
        (protocol_code,protocol_name,protocol_type,description,program_id,creator_id)
SELECT
    protocol_code,protocol_name,protocol_type,description,program_id,creator_id
FROM
    (
        VALUES
            ('TRAIT_PROTOCOL_EXP0017557','Trait Protocol Exp0017557','trait',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ('PLANTING_PROTOCOL_EXP0017557','Planting Protocol Exp0017557','planting',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ('HARVEST_PROTOCOL_EXP0017557','Harvest Protocol Exp0017557','harvest',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ('MANAGEMENT_PROTOCOL_EXP0017557','Management Protocol Exp0017557','management',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')))
     t (protocol_code,protocol_name,protocol_type,description,program_id,creator_id)
;



--rollback DELETE FROM
--rollback    tenant.protocol
--rollback WHERE
--rollback    protocol_code
--rollback IN
--rollback    (
--rollback        'TRAIT_PROTOCOL_EXP0017557',
--rollback        'PLANTING_PROTOCOL_EXP0017557',
--rollback        'HARVEST_PROTOCOL_EXP0017557',
--rollback        'MANAGEMENT_PROTOCOL_EXP0017557'
--rollback    )
--rollback ;