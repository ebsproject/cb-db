--liquibase formatted sql

--changeset postgres:4_populate_occurrence_ba_exp_3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1716 Populate occurrence BA_EXP_3



INSERT INTO 
    experiment.occurrence
        (occurrence_code,occurrence_name,occurrence_status,description,experiment_id,geospatial_object_id,creator_id,rep_count,site_id,field_id,occurrence_number,remarks,entry_count,plot_count)
SELECT
    occurrence_code,occurrence_name,occurrence_status,description,experiment_id,geospatial_object_id::int,creator_id,rep_count::int,site_id,field_id::int,occurrence_number,remarks,entry_count,plot_count
FROM
    (
        VALUES
            ('EXP0018544-001','AF2023_AL_T30_V3-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_code='EXP0018544'),NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH_NE_SM'),NULL,1,NULL,30,90),
            ('EXP0018544-002','AF2023_AL_T30_V3-002','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_code='EXP0018544'),NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='EBN'),NULL,2,NULL,30,90),
            ('EXP0018544-003','AF2023_AL_T30_V3-003','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_code='EXP0018544'),NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IN_JH_RN'),NULL,3,NULL,30,90),
            ('EXP0018544-004','AF2023_AL_T30_V3-004','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_code='EXP0018544'),NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH_BG_LT'),NULL,4,NULL,30,90),
            ('EXP0018544-005','AF2023_AL_T30_V3-005','planted',NULL,(SELECT id FROM experiment.experiment WHERE experiment_code='EXP0018544'),NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH_NE_SM2'),NULL,5,NULL,30,90),
            ('EXP0018544-006','AF2023_AL_T30_V3-006','mapped;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_code='EXP0018544'),NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='LA_VI'),NULL,6,NULL,30,90),
            ('EXP0018544-007','AF2023_AL_T30_V3-007','created',NULL,(SELECT id FROM experiment.experiment WHERE experiment_code='EXP0018544'),NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH_CS_PL'),NULL,7,NULL,30,90))
     t (occurrence_code,occurrence_name,occurrence_status,description,experiment_id,geospatial_object_id,creator_id,rep_count,site_id,field_id,occurrence_number,remarks,entry_count,plot_count)
;



--rollback DELETE FROM
--rollback    experiment.occurrence
--rollback WHERE
--rollback    occurrence_code
--rollback IN
--rollback    (
--rollback        'EXP0018544-001',
--rollback        'EXP0018544-002',
--rollback        'EXP0018544-003',
--rollback        'EXP0018544-004',
--rollback        'EXP0018544-005',
--rollback        'EXP0018544-006',
--rollback        'EXP0018544-007'
--rollback    )
--rollback ;