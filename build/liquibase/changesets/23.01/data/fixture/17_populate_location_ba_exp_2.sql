--liquibase formatted sql

--changeset postgres:17_populate_location_ba_exp_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1716 Populate location BA_EXP_2



INSERT INTO 
    experiment.location
        (location_code,location_name,location_status,location_type,description,steward_id,location_planting_date,location_harvest_date,geospatial_object_id,creator_id,location_year,season_id,site_id,field_id,location_number,remarks,entry_count,plot_count)
SELECT
    location_code,location_name,location_status,location_type,description,steward_id,location_planting_date::date,location_harvest_date::date,geospatial_object_id::int,creator_id,location_year,season_id,site_id,field_id::int,location_number,remarks,entry_count,plot_count
FROM
    (
        VALUES
            ('IRRIHQ-2023-DS-002','IRRIHQ-2023-DS-002','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRIHQ-2023-DS-002'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),2023,(SELECT id FROM tenant.season WHERE season_code='DS'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRIHQ'),NULL,2,NULL,0,90))
     t (location_code,location_name,location_status,location_type,description,steward_id,location_planting_date,location_harvest_date,geospatial_object_id,creator_id,location_year,season_id,site_id,field_id,location_number,remarks,entry_count,plot_count)
;



--rollback DELETE FROM
--rollback    experiment.location
--rollback WHERE
--rollback    location_code
--rollback IN
--rollback    (
--rollback        'IRRIHQ-2023-DS-002'
--rollback    )
--rollback ;