--liquibase formatted sql

--changeset postgres:37_populate_list_ba_exp_3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1716 Populate list BA_EXP_3



INSERT INTO 
    platform.list
        (abbrev,name,display_name,type,entity_id,description,remarks,creator_id,record_uuid,list_usage,status,is_active,list_sub_type)
SELECT
    abbrev,name,display_name,type,entity_id,description,remarks,creator_id,record_uuid::uuid,list_usage,status,is_active,list_sub_type
FROM
    (
        VALUES
            ('TRAIT_PROTOCOL_EXP0018544','AF2023_AL_T30_V3 Trait Protocol (EXP0018544)','AF2023_AL_T30_V3 Trait Protocol (EXP0018544)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'8a938308-eb2d-40a3-a8da-f65d3a617295','working list','created',True,'trait protocol'),
            ('MANAGEMENT_PROTOCOL_EXP0018544','AF2023_AL_T30_V3 Management Protocol (EXP0018544)','AF2023_AL_T30_V3 Management Protocol (EXP0018544)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'0740fe79-5b31-4bd3-897d-250c5b9dfee8','working list','created',True,'management protocol'),
            ('TRAIT_PROTOCOL_EXP0018544-001','AF2023_AL_T30_V3-001 Trait Protocol (EXP0018544-001)','AF2023_AL_T30_V3-001 Trait Protocol (EXP0018544-001)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'8938691d-102d-4e9d-b63f-de3c5599da1a','working list','created',True,NULL),
            ('MANAGEMENT_PROTOCOL_EXP0018544-001','AF2023_AL_T30_V3-001 Management Protocol (EXP0018544-001)','AF2023_AL_T30_V3-001 Management Protocol (EXP0018544-001)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'6750b223-d272-4073-9e5f-8d71b0ab2ab5','working list','created',True,NULL),
            ('TRAIT_PROTOCOL_EXP0018544-002','AF2023_AL_T30_V3-002 Trait Protocol (EXP0018544-002)','AF2023_AL_T30_V3-002 Trait Protocol (EXP0018544-002)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'eb294cba-cba1-4109-9cca-1722bc879373','working list','created',True,NULL),
            ('MANAGEMENT_PROTOCOL_EXP0018544-002','AF2023_AL_T30_V3-002 Management Protocol (EXP0018544-002)','AF2023_AL_T30_V3-002 Management Protocol (EXP0018544-002)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'b062fb77-40b0-46ca-b321-3512ecc751f4','working list','created',True,NULL),
            ('TRAIT_PROTOCOL_EXP0018544-003','AF2023_AL_T30_V3-003 Trait Protocol (EXP0018544-003)','AF2023_AL_T30_V3-003 Trait Protocol (EXP0018544-003)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'11632773-6127-4040-93e0-4a0fcddc591e','working list','created',True,NULL),
            ('MANAGEMENT_PROTOCOL_EXP0018544-003','AF2023_AL_T30_V3-003 Management Protocol (EXP0018544-003)','AF2023_AL_T30_V3-003 Management Protocol (EXP0018544-003)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'6e8bc1c2-45f6-444f-b26b-a1607d2842ff','working list','created',True,NULL),
            ('TRAIT_PROTOCOL_EXP0018544-004','AF2023_AL_T30_V3-004 Trait Protocol (EXP0018544-004)','AF2023_AL_T30_V3-004 Trait Protocol (EXP0018544-004)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'0830bddd-488d-4b8e-8bf0-527a2a3a173a','working list','created',True,NULL),
            ('MANAGEMENT_PROTOCOL_EXP0018544-004','AF2023_AL_T30_V3-004 Management Protocol (EXP0018544-004)','AF2023_AL_T30_V3-004 Management Protocol (EXP0018544-004)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'f559d7f5-d02c-41c5-b570-622fa3e1d2f9','working list','created',True,NULL),
            ('TRAIT_PROTOCOL_EXP0018544-005','AF2023_AL_T30_V3-005 Trait Protocol (EXP0018544-005)','AF2023_AL_T30_V3-005 Trait Protocol (EXP0018544-005)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'77620e38-1281-48a2-a8b1-2a38add54641','working list','created',True,NULL),
            ('MANAGEMENT_PROTOCOL_EXP0018544-005','AF2023_AL_T30_V3-005 Management Protocol (EXP0018544-005)','AF2023_AL_T30_V3-005 Management Protocol (EXP0018544-005)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'28cc56a6-8544-41be-914d-ce1749f25aa3','working list','created',True,NULL),
            ('TRAIT_PROTOCOL_EXP0018544-006','AF2023_AL_T30_V3-006 Trait Protocol (EXP0018544-006)','AF2023_AL_T30_V3-006 Trait Protocol (EXP0018544-006)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'6eea06bc-0aa8-4730-999c-cb915bacdeb9','working list','created',True,NULL),
            ('MANAGEMENT_PROTOCOL_EXP0018544-006','AF2023_AL_T30_V3-006 Management Protocol (EXP0018544-006)','AF2023_AL_T30_V3-006 Management Protocol (EXP0018544-006)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'c0f161ba-0c84-41d3-ba7c-d935b6c194e6','working list','created',True,NULL),
            ('TRAIT_PROTOCOL_EXP0018544-007','AF2023_AL_T30_V3-007 Trait Protocol (EXP0018544-007)','AF2023_AL_T30_V3-007 Trait Protocol (EXP0018544-007)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'15201aca-8c53-4c25-9077-d2424f2f8b90','working list','created',True,NULL),
            ('MANAGEMENT_PROTOCOL_EXP0018544-007','AF2023_AL_T30_V3-007 Management Protocol (EXP0018544-007)','AF2023_AL_T30_V3-007 Management Protocol (EXP0018544-007)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'20b40e73-dc7e-4db6-aaf3-7d65b50e6414','working list','created',True,NULL))
     t (abbrev,name,display_name,type,entity_id,description,remarks,creator_id,record_uuid,list_usage,status,is_active,list_sub_type)
;



--rollback DELETE FROM
--rollback    platform.list
--rollback WHERE
--rollback    abbrev
--rollback IN
--rollback    (
--rollback        'TRAIT_PROTOCOL_EXP0018544',
--rollback        'MANAGEMENT_PROTOCOL_EXP0018544',
--rollback        'TRAIT_PROTOCOL_EXP0018544-001',
--rollback        'MANAGEMENT_PROTOCOL_EXP0018544-001',
--rollback        'TRAIT_PROTOCOL_EXP0018544-002',
--rollback        'MANAGEMENT_PROTOCOL_EXP0018544-002',
--rollback        'TRAIT_PROTOCOL_EXP0018544-003',
--rollback        'MANAGEMENT_PROTOCOL_EXP0018544-003',
--rollback        'TRAIT_PROTOCOL_EXP0018544-004',
--rollback        'MANAGEMENT_PROTOCOL_EXP0018544-004',
--rollback        'TRAIT_PROTOCOL_EXP0018544-005',
--rollback        'MANAGEMENT_PROTOCOL_EXP0018544-005',
--rollback        'TRAIT_PROTOCOL_EXP0018544-006',
--rollback        'MANAGEMENT_PROTOCOL_EXP0018544-006',
--rollback        'TRAIT_PROTOCOL_EXP0018544-007',
--rollback        'MANAGEMENT_PROTOCOL_EXP0018544-007'
--rollback    )
--rollback ;