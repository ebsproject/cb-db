--liquibase formatted sql

--changeset postgres:populate_geospatial_object context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1834 Populate geospatial_object



INSERT INTO 
    place.geospatial_object
        (geospatial_object_code,geospatial_object_name,geospatial_object_type,geospatial_object_subtype,geospatial_coordinates,altitude,description,creator_id)
SELECT
    geospatial_object_code,geospatial_object_name,geospatial_object_type,geospatial_object_subtype,geospatial_coordinates::polygon,altitude::float,description,creator_id
FROM
    (
        VALUES
            ('IRRIHQ-2020-DS-012','IRRIHQ-2020-DS-012','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE person_name='EBS, Admin')))
     t (geospatial_object_code,geospatial_object_name,geospatial_object_type,geospatial_object_subtype,geospatial_coordinates,altitude,description,creator_id)
;

UPDATE place.geospatial_object SET parent_geospatial_object_id=(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRIHQ') WHERE geospatial_object_code='IRRIHQ-2020-DS-012';
UPDATE place.geospatial_object SET root_geospatial_object_id=(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH') WHERE geospatial_object_code='IRRIHQ-2020-DS-012';



--rollback DELETE FROM
--rollback    place.geospatial_object
--rollback WHERE
--rollback    geospatial_object_code
--rollback IN
--rollback    (
--rollback        'IRRIHQ-2020-DS-012'
--rollback    )
--rollback ;