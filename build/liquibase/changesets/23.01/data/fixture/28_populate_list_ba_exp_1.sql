--liquibase formatted sql

--changeset postgres:28_populate_list_ba_exp_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1716 Populate list BA_EXP_1



INSERT INTO 
    platform.list
        (abbrev,name,display_name,type,entity_id,description,remarks,creator_id,record_uuid,list_usage,status,is_active,list_sub_type)
SELECT
    abbrev,name,display_name,type,entity_id,description,remarks,creator_id,record_uuid::uuid,list_usage,status,is_active,list_sub_type
FROM
    (
        VALUES
            ('MANAGEMENT_PROTOCOL_EXP0017556','AF2023_AL_T25_V2 Management Protocol (EXP0017556)','AF2023_AL_T25_V2 Management Protocol (EXP0017556)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'76f0ecc1-1644-4c67-b323-60aa734251b4','working list','created',True,'management protocol'),
            ('TRAIT_PROTOCOL_EXP0017556','AF2023_AL_T25_V2 Trait Protocol (EXP0017556)','AF2023_AL_T25_V2 Trait Protocol (EXP0017556)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'9f246174-0581-4a76-a3ba-11e3e780e71b','working list','created',True,'trait protocol'),
            ('MANAGEMENT_PROTOCOL_EXP0017556-001','AF2023_AL_T25_V2-001 Management Protocol (EXP0017556-001)','AF2023_AL_T25_V2-001 Management Protocol (EXP0017556-001)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'d576dc3d-a0fd-41bd-baf6-05c177b6db23','working list','created',True,NULL),
            ('TRAIT_PROTOCOL_EXP0017556-001','AF2023_AL_T25_V2-001 Trait Protocol (EXP0017556-001)','AF2023_AL_T25_V2-001 Trait Protocol (EXP0017556-001)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'736f76d9-492c-433d-891a-ec27734d47b5','working list','created',True,NULL),
            ('MANAGEMENT_PROTOCOL_EXP0017556-002','AF2023_AL_T25_V2-002 Management Protocol (EXP0017556-002)','AF2023_AL_T25_V2-002 Management Protocol (EXP0017556-002)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'7b684a9c-9022-4002-8522-d4fc9fd95977','working list','created',True,NULL),
            ('TRAIT_PROTOCOL_EXP0017556-002','AF2023_AL_T25_V2-002 Trait Protocol (EXP0017556-002)','AF2023_AL_T25_V2-002 Trait Protocol (EXP0017556-002)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'e5992d34-9708-4fe2-8219-06c1eb958ff0','working list','created',True,NULL),
            ('MANAGEMENT_PROTOCOL_EXP0017556-003','AF2023_AL_T25_V2-003 Management Protocol (EXP0017556-003)','AF2023_AL_T25_V2-003 Management Protocol (EXP0017556-003)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'6fc5e79d-6200-436f-9c6d-79e4b3ce5037','working list','created',True,NULL),
            ('TRAIT_PROTOCOL_EXP0017556-003','AF2023_AL_T25_V2-003 Trait Protocol (EXP0017556-003)','AF2023_AL_T25_V2-003 Trait Protocol (EXP0017556-003)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'a28a2a88-af8d-4186-9f80-a523ea3aae22','working list','created',True,NULL),
            ('MANAGEMENT_PROTOCOL_EXP0017556-004','AF2023_AL_T25_V2-004 Management Protocol (EXP0017556-004)','AF2023_AL_T25_V2-004 Management Protocol (EXP0017556-004)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'bb3df8b1-0283-48b5-bd15-5c577e0804ef','working list','created',True,NULL),
            ('TRAIT_PROTOCOL_EXP0017556-004','AF2023_AL_T25_V2-004 Trait Protocol (EXP0017556-004)','AF2023_AL_T25_V2-004 Trait Protocol (EXP0017556-004)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'94ad4c99-4a19-4820-803a-cf459b88f43a','working list','created',True,NULL),
            ('MANAGEMENT_PROTOCOL_EXP0017556-005','AF2023_AL_T25_V2-005 Management Protocol (EXP0017556-005)','AF2023_AL_T25_V2-005 Management Protocol (EXP0017556-005)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'dc8977db-8ddf-41ea-bdbc-51ecd8e7fbb6','working list','created',True,NULL),
            ('TRAIT_PROTOCOL_EXP0017556-005','AF2023_AL_T25_V2-005 Trait Protocol (EXP0017556-005)','AF2023_AL_T25_V2-005 Trait Protocol (EXP0017556-005)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'0fdf7428-c7af-405b-b4cc-2fd233cbb9ad','working list','created',True,NULL))
     t (abbrev,name,display_name,type,entity_id,description,remarks,creator_id,record_uuid,list_usage,status,is_active,list_sub_type)
;



--rollback DELETE FROM
--rollback    platform.list
--rollback WHERE
--rollback    abbrev
--rollback IN
--rollback    (
--rollback        'MANAGEMENT_PROTOCOL_EXP0017556',
--rollback        'TRAIT_PROTOCOL_EXP0017556',
--rollback        'MANAGEMENT_PROTOCOL_EXP0017556-001',
--rollback        'TRAIT_PROTOCOL_EXP0017556-001',
--rollback        'MANAGEMENT_PROTOCOL_EXP0017556-002',
--rollback        'TRAIT_PROTOCOL_EXP0017556-002',
--rollback        'MANAGEMENT_PROTOCOL_EXP0017556-003',
--rollback        'TRAIT_PROTOCOL_EXP0017556-003',
--rollback        'MANAGEMENT_PROTOCOL_EXP0017556-004',
--rollback        'TRAIT_PROTOCOL_EXP0017556-004',
--rollback        'MANAGEMENT_PROTOCOL_EXP0017556-005',
--rollback        'TRAIT_PROTOCOL_EXP0017556-005'
--rollback    )
--rollback ;