--liquibase formatted sql

--changeset postgres:28_populate_experiment_data_ba_exp_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1716 Populate experiment_data BA_EXP_2



INSERT INTO 
    experiment.experiment_data
        (experiment_id,variable_id,data_value,data_qc_code,protocol_id,creator_id)
SELECT
    experiment_id,variable_id,data_value,data_qc_code,protocol_id,creator_id
FROM
    (
        VALUES
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0017557'),(SELECT id FROM master.variable WHERE abbrev='FIRST_PLOT_POSITION_VIEW'),'Top Left','N',NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0017557'),(SELECT id FROM master.variable WHERE abbrev='TRAIT_PROTOCOL_LIST_ID'),'158157','N',(SELECT id FROM tenant.protocol WHERE protocol_code='TRAIT_PROTOCOL_EXP0017557'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0017557'),(SELECT id FROM master.variable WHERE abbrev='MANAGEMENT_PROTOCOL_LIST_ID'),'158158','N',(SELECT id FROM tenant.protocol WHERE protocol_code='MANAGEMENT_PROTOCOL_EXP0017557'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')))
     t (experiment_id,variable_id,data_value,data_qc_code,protocol_id,creator_id)
;



--rollback DELETE FROM
--rollback    experiment.experiment_data
--rollback WHERE
--rollback    experiment_id
--rollback IN
--rollback    (
--rollback        SELECT id FROM experiment.experiment WHERE experiment_code='EXP0017557'
--rollback    )
--rollback ;