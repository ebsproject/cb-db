--liquibase formatted sql

--changeset postgres:16_populate_geospatial_object_ba_exp_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1716 Populate geospatial_object BA_EXP_2



INSERT INTO 
    place.geospatial_object
        (geospatial_object_code,geospatial_object_name,geospatial_object_type,geospatial_object_subtype,geospatial_coordinates,altitude,description,creator_id)
SELECT
    geospatial_object_code,geospatial_object_name,geospatial_object_type,geospatial_object_subtype,geospatial_coordinates::polygon,altitude::float,description,creator_id
FROM
    (
        VALUES
            ('IRRIHQ-2023-DS-002','IRRIHQ-2023-DS-002','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')))
     t (geospatial_object_code,geospatial_object_name,geospatial_object_type,geospatial_object_subtype,geospatial_coordinates,altitude,description,creator_id)
;



--rollback DELETE FROM
--rollback    place.geospatial_object
--rollback WHERE
--rollback    geospatial_object_code
--rollback IN
--rollback    (
--rollback        'IRRIHQ-2023-DS-002'
--rollback    )
--rollback ;