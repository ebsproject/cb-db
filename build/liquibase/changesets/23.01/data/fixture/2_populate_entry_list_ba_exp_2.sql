--liquibase formatted sql

--changeset postgres:2_populate_entry_list_ba_exp_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1716 Populate entry_list BA_EXP_2



INSERT INTO 
    experiment.entry_list
        (entry_list_code,entry_list_name,description,entry_list_status,experiment_id,creator_id,entry_list_type,cross_count,experiment_year,season_id,site_id,crop_id,program_id)
SELECT
    entry_list_code,entry_list_name,description,entry_list_status,experiment_id,creator_id,entry_list_type,cross_count,experiment_year::int,season_id::int,site_id::int,crop_id,program_id
FROM
    (
        VALUES
            ('ENTLIST00017491','AF2023_AL_T30_V2a Entry List',NULL,'completed',(SELECT id FROM experiment.experiment WHERE experiment_code='EXP0017557'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'entry list',0,NULL,NULL,NULL,(SELECT id FROM tenant.crop WHERE crop_code='RICE'),(SELECT id FROM tenant.program WHERE program_code='IRSEA')))
     t (entry_list_code,entry_list_name,description,entry_list_status,experiment_id,creator_id,entry_list_type,cross_count,experiment_year,season_id,site_id,crop_id,program_id)
;



--rollback DELETE FROM
--rollback    experiment.entry_list
--rollback WHERE
--rollback    entry_list_code
--rollback IN
--rollback    (
--rollback        'ENTLIST00017491'
--rollback    )
--rollback ;