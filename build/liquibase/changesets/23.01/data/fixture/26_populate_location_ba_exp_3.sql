--liquibase formatted sql

--changeset postgres:26_populate_location_ba_exp_3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1716 Populate location BA_EXP_3



INSERT INTO 
    experiment.location
        (location_code,location_name,location_status,location_type,description,steward_id,location_planting_date,location_harvest_date,geospatial_object_id,creator_id,location_year,season_id,site_id,field_id,location_number,remarks,entry_count,plot_count)
SELECT
    location_code,location_name,location_status,location_type,description,steward_id,location_planting_date::date,location_harvest_date::date,geospatial_object_id,creator_id,location_year,season_id,site_id,field_id::int,location_number,remarks,entry_count,plot_count
FROM
    (
        VALUES
            ('PH_BG_LT-2023-DS-002','PH_BG_LT-2023-DS-002','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH_BG_LT-2023-DS-002'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),2023,(SELECT id FROM tenant.season WHERE season_code='DS'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH_BG_LT'),NULL,2,NULL,0,90))
     t (location_code,location_name,location_status,location_type,description,steward_id,location_planting_date,location_harvest_date,geospatial_object_id,creator_id,location_year,season_id,site_id,field_id,location_number,remarks,entry_count,plot_count)
;



--rollback DELETE FROM
--rollback    experiment.location
--rollback WHERE
--rollback    location_code
--rollback IN
--rollback    (
--rollback        'PH_BG_LT-2023-DS-002'
--rollback    )
--rollback ;