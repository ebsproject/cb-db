--liquibase formatted sql

--changeset postgres:27_populate_experiment_protocol_ba_exp_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1716 Populate experiment_protocol BA_EXP_1



INSERT INTO 
    experiment.experiment_protocol
        (experiment_id,protocol_id,order_number,creator_id)
SELECT
    experiment_id,protocol_id,order_number,creator_id
FROM
    (
        VALUES
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0017556'),(SELECT id FROM tenant.protocol WHERE protocol_code='HARVEST_PROTOCOL_EXP0017556'),5,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0017556'),(SELECT id FROM tenant.protocol WHERE protocol_code='PLANTING_PROTOCOL_EXP0017556'),1,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0017556'),(SELECT id FROM tenant.protocol WHERE protocol_code='MANAGEMENT_PROTOCOL_EXP0017556'),4,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0017556'),(SELECT id FROM tenant.protocol WHERE protocol_code='TRAIT_PROTOCOL_EXP0017556'),3,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')))
     t (experiment_id,protocol_id,order_number,creator_id)
;



--rollback DELETE FROM
--rollback    experiment.experiment_protocol
--rollback WHERE
--rollback    experiment_id
--rollback IN
--rollback    (
--rollback        SELECT id FROM experiment.experiment WHERE experiment_code='EXP0017556'
--rollback    )
--rollback ;