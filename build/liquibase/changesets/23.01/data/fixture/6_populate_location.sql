--liquibase formatted sql

--changeset postgres:populate_location context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1834 Populate location



INSERT INTO 
    experiment.location
        (location_code,location_name,location_status,location_type,description,steward_id,location_planting_date,location_harvest_date,geospatial_object_id,creator_id,location_year,season_id,site_id,field_id,location_number,remarks,entry_count,plot_count)
SELECT
    location_code,location_name,location_status,location_type,description,steward_id,location_planting_date::date,location_harvest_date::date,geospatial_object_id,creator_id,location_year,season_id,site_id,field_id::int,location_number,remarks,entry_count,plot_count
FROM
    (
        VALUES
            ('IRRIHQ-2020-DS-012','IRRIHQ-2020-DS-012','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRIHQ-2020-DS-012'),(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),2020,(SELECT id FROM tenant.season WHERE season_code='DS'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRIHQ'),NULL,12,NULL,0,300))
     t (location_code,location_name,location_status,location_type,description,steward_id,location_planting_date,location_harvest_date,geospatial_object_id,creator_id,location_year,season_id,site_id,field_id,location_number,remarks,entry_count,plot_count)
;



--rollback DELETE FROM
--rollback    experiment.location
--rollback WHERE
--rollback    location_code
--rollback IN
--rollback    (
--rollback        'IRRIHQ-2020-DS-012'
--rollback    )
--rollback ;