--liquibase formatted sql

--changeset postgres:26_populate_protocol_ba_exp_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1716 Populate protocol BA_EXP_1



INSERT INTO 
    tenant.protocol
        (protocol_code,protocol_name,protocol_type,description,program_id,creator_id)
SELECT
    protocol_code,protocol_name,protocol_type,description,program_id,creator_id
FROM
    (
        VALUES
            ('HARVEST_PROTOCOL_EXP0017556','Harvest Protocol Exp0017556','harvest',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ('PLANTING_PROTOCOL_EXP0017556','Planting Protocol Exp0017556','planting',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ('MANAGEMENT_PROTOCOL_EXP0017556','Management Protocol Exp0017556','management',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ('TRAIT_PROTOCOL_EXP0017556','Trait Protocol Exp0017556','trait',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')))
     t (protocol_code,protocol_name,protocol_type,description,program_id,creator_id)
;



--rollback DELETE FROM
--rollback    tenant.protocol
--rollback WHERE
--rollback    protocol_code
--rollback IN
--rollback    (
--rollback        'HARVEST_PROTOCOL_EXP0017556',
--rollback        'PLANTING_PROTOCOL_EXP0017556',
--rollback        'MANAGEMENT_PROTOCOL_EXP0017556',
--rollback        'TRAIT_PROTOCOL_EXP0017556'
--rollback    )
--rollback ;