--liquibase formatted sql

--changeset postgres:populate_entry_list context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1834 Populate entry_list



INSERT INTO 
    experiment.entry_list
        (entry_list_code,entry_list_name,description,entry_list_status,experiment_id,creator_id,entry_list_type,cross_count,experiment_year,season_id,site_id,crop_id,program_id)
SELECT
    entry_list_code,entry_list_name,description,entry_list_status,experiment_id,creator_id,entry_list_type,cross_count,experiment_year::int,season_id::int,site_id::int,crop_id,program_id
FROM
    (
        VALUES
            ('ENTLIST00000353-M','CROSS5_SC F5 Entry List',NULL,'completed',(SELECT id FROM experiment.experiment WHERE experiment_code='EXP0000350'),(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),'entry list',0,NULL,NULL,NULL,(SELECT id FROM tenant.crop WHERE crop_code='RICE'),(SELECT id FROM tenant.program WHERE program_code='IRSEA')))
     t (entry_list_code,entry_list_name,description,entry_list_status,experiment_id,creator_id,entry_list_type,cross_count,experiment_year,season_id,site_id,crop_id,program_id)
;



--rollback DELETE FROM
--rollback    experiment.entry_list
--rollback WHERE
--rollback    entry_list_code
--rollback IN
--rollback    (
--rollback        'ENTLIST00000353-M'
--rollback    )
--rollback ;