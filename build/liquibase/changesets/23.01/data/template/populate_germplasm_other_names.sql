--liquibase formatted sql

--changeset postgres:populate_germplasm_other_names context:template splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1836 Populate germplasm other_names



UPDATE germplasm.germplasm SET modification_timestamp = now();



-- revert changes
--rollback SELECT NULL;