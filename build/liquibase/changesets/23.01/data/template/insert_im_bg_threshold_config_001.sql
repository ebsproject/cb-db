--liquibase formatted sql

--changeset postgres:insert_im_bg_threshold_config_001 context:template splitstatements:false rollbacksplitstatements:false
--comment: CORB-4796 IM DB: Insert background process threshold config for Inventory Manager



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'INVENTORY_MANAGER_BG_PROCESSING_THRESHOLD',
        'Background processing threshold values for INVENTORY MANAGER tool features',
        $$			
        {
            "fileUploadRowCount": {
                "size": "10000",
                "description": "General maximum row count for file uploads in Inventory Manager."
            }
        }
        $$,
        1,
        'inventory_manager',
        1,
        'CORB-4796 - j.bantay 2023-01-24'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='INVENTORY_MANAGER_BG_PROCESSING_THRESHOLD';