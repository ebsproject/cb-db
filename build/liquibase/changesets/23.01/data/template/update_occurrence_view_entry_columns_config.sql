--liquibase formatted sql

--changeset postgres:update_occurrence_view_entry_columns_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4808 EM: Remove package quantity and package unit columns in View Entries of Occurrence


UPDATE
	platform.config
SET 
	config_value = '{
            "VIEW_ENTRY_COLUMNS": [
                {
                    "attribute": "occurrenceName",
                    "abbrev": "OCCURRENCE_NAME",
                    "visible": false
                },
                {
                    "attribute": "entryNumber",
                    "abbrev": "ENTNO"
                },
                {
                    "attribute": "entryCode",
                    "abbrev": "ENTRY_CODE"
                },
                {
                    "attribute": "entryName",
                    "abbrev": "GERMPLASM_NAME",
                    "format": "raw",
                    "hasViewGermplasmInfo": {
                        "id": "germplasmDbId",
                        "label": "entryName"
                    }
                },
                {
                    "attribute": "germplasmCode",
                    "abbrev": "GERMPLASM_CODE"
                },
                {
                    "attribute": "germplasmState",
                    "abbrev": "GERMPLASM_STATE"
                },
                {
                    "attribute": "germplasmType",
                    "abbrev": "GERMPLASM_TYPE"
                },
                {
                    "attribute": "generation",
                    "abbrev": "GENERATION"
                },
                {
                    "attribute": "parentage",
                    "abbrev": "PARENTAGE"
                },
                {
                    "attribute": "entryType",
                    "abbrev": "ENTRY_TYPE"
                },
                {
                    "attribute": "entryRole",
                    "abbrev": "ENTRY_ROLE"
                },
                {
                    "attribute": "entryClass",
                    "abbrev": "ENTRY_CLASS"
                },
                {
                    "attribute": "seedName",
                    "abbrev": "SEED_NAME"
                },
                {
                    "attribute": "packageLabel",
                    "abbrev": "PACKAGE_LABEL"
                }
            ],
            "VIEW_PLOT_COLUMNS": [
                {
                    "attribute": "plotNumber",
                    "abbrev": "PLOTNO"
                },
                {
                    "attribute": "entryNumber",
                    "abbrev": "ENTNO"
                },
                {
                    "attribute": "entryType",
                    "abbrev": "ENTRY_TYPE"
                },
                {
                    "attribute": "entryClass",
                    "abbrev": "ENTRY_CLASS"
                },
                {
                    "attribute": "entryName",
                    "abbrev": "GERMPLASM_NAME",
                    "format": "raw",
                    "hasViewGermplasmInfo": {
                        "id": "germplasmDbId",
                        "label": "entryName"
                    }
                },
                {
                    "attribute": "germplasmCode",
                    "abbrev": "GERMPLASM_CODE"
                },
                {
                    "attribute": "germplasmState",
                    "abbrev": "GERMPLASM_STATE"
                },
                {
                    "attribute": "germplasmType",
                    "abbrev": "GERMPLASM_TYPE"
                },
                {
                    "attribute": "parentage",
                    "abbrev": "PARENTAGE"
                },
                {
                    "attribute": "entryCode",
                    "abbrev": "ENTRY_CODE"
                },
                {
                    "attribute": "plotCode",
                    "abbrev": "PLOT_CODE"
                },
                {
                    "attribute": "rep",
                    "abbrev": "REP"
                },
                {
                    "attribute": "designX",
                    "abbrev": "DESIGN_X"
                },
                {
                    "attribute": "designY",
                    "abbrev": "DESIGN_Y"
                },
                {
                    "attribute": "paX",
                    "abbrev": "PA_X"
                },
                {
                    "attribute": "paY",
                    "abbrev": "PA_Y"
                },
                {
                    "attribute": "fieldX",
                    "abbrev": "FIELD_X",
                    "visible": false
                },
                {
                    "attribute": "fieldY",
                    "abbrev": "FIELD_Y",
                    "visible": false
                },
                {
                    "attribute": "rowBlockNumber",
                    "abbrev": "ROW_BLOCK_NUMBER",
                    "visible": false
                },
                {
                    "attribute": "colBlockNumber",
                    "abbrev": "COL_BLOCK_NUMBER",
                    "visible": false
                },
                {
                    "attribute": "blockNumber",
                    "abbrev": "BLOCK_NO_CONT",
                    "visible": false
                }
            ]
        }'
WHERE abbrev = 'VIEW_OCCURRENCE_DATA_DEFAULT';


--rollback  
--rollback  UPDATE
--rollback  	platform.config
--rollback  SET 
--rollback  	config_value = '{
--rollback              "VIEW_ENTRY_COLUMNS": [
--rollback                  {
--rollback                      "attribute": "occurrenceName",
--rollback                      "abbrev": "OCCURRENCE_NAME",
--rollback                      "visible": false
--rollback                  },
--rollback                  {
--rollback                      "attribute": "entryNumber",
--rollback                      "abbrev": "ENTNO"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "entryCode",
--rollback                      "abbrev": "ENTRY_CODE"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "entryName",
--rollback                      "abbrev": "GERMPLASM_NAME",
--rollback                      "format": "raw",
--rollback                      "hasViewGermplasmInfo": {
--rollback                          "id": "germplasmDbId",
--rollback                          "label": "entryName"
--rollback                      }
--rollback                  },
--rollback                  {
--rollback                      "attribute": "germplasmCode",
--rollback                      "abbrev": "GERMPLASM_CODE"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "germplasmState",
--rollback                      "abbrev": "GERMPLASM_STATE"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "germplasmType",
--rollback                      "abbrev": "GERMPLASM_TYPE"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "generation",
--rollback                      "abbrev": "GENERATION"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "parentage",
--rollback                      "abbrev": "PARENTAGE"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "entryType",
--rollback                      "abbrev": "ENTRY_TYPE"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "entryRole",
--rollback                      "abbrev": "ENTRY_ROLE"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "entryClass",
--rollback                      "abbrev": "ENTRY_CLASS"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "seedName",
--rollback                      "abbrev": "SEED_NAME"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "packageLabel",
--rollback                      "abbrev": "PACKAGE_LABEL"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "packageQuantity",
--rollback                      "abbrev": "PACKAGE_QUANTITY"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "packageUnit",
--rollback                      "abbrev": "PACKAGE_UNIT"
--rollback                  }
--rollback              ],
--rollback              "VIEW_PLOT_COLUMNS": [
--rollback                  {
--rollback                      "attribute": "plotNumber",
--rollback                      "abbrev": "PLOTNO"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "entryNumber",
--rollback                      "abbrev": "ENTNO"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "entryType",
--rollback                      "abbrev": "ENTRY_TYPE"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "entryClass",
--rollback                      "abbrev": "ENTRY_CLASS"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "entryName",
--rollback                      "abbrev": "GERMPLASM_NAME",
--rollback                      "format": "raw",
--rollback                      "hasViewGermplasmInfo": {
--rollback                          "id": "germplasmDbId",
--rollback                          "label": "entryName"
--rollback                      }
--rollback                  },
--rollback                  {
--rollback                      "attribute": "germplasmCode",
--rollback                      "abbrev": "GERMPLASM_CODE"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "germplasmState",
--rollback                      "abbrev": "GERMPLASM_STATE"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "germplasmType",
--rollback                      "abbrev": "GERMPLASM_TYPE"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "parentage",
--rollback                      "abbrev": "PARENTAGE"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "entryCode",
--rollback                      "abbrev": "ENTRY_CODE"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "plotCode",
--rollback                      "abbrev": "PLOT_CODE"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "rep",
--rollback                      "abbrev": "REP"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "designX",
--rollback                      "abbrev": "DESIGN_X"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "designY",
--rollback                      "abbrev": "DESIGN_Y"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "paX",
--rollback                      "abbrev": "PA_X"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "paY",
--rollback                      "abbrev": "PA_Y"
--rollback                  },
--rollback                  {
--rollback                      "attribute": "fieldX",
--rollback                      "abbrev": "FIELD_X",
--rollback                      "visible": false
--rollback                  },
--rollback                  {
--rollback                      "attribute": "fieldY",
--rollback                      "abbrev": "FIELD_Y",
--rollback                      "visible": false
--rollback                  },
--rollback                  {
--rollback                      "attribute": "rowBlockNumber",
--rollback                      "abbrev": "ROW_BLOCK_NUMBER",
--rollback                      "visible": false
--rollback                  },
--rollback                  {
--rollback                      "attribute": "colBlockNumber",
--rollback                      "abbrev": "COL_BLOCK_NUMBER",
--rollback                      "visible": false
--rollback                  },
--rollback                  {
--rollback                      "attribute": "blockNumber",
--rollback                      "abbrev": "BLOCK_NO_CONT",
--rollback                      "visible": false
--rollback                  }
--rollback              ]
--rollback          }'
--rollback  WHERE abbrev = 'VIEW_OCCURRENCE_DATA_DEFAULT';