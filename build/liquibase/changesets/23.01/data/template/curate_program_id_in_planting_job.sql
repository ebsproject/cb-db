--liquibase formatted sql

--changeset postgres:curate_program_id_in_planting_job context:template splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1908 Curate program_id planting job



WITH t1 AS (
    SELECT 
        --pjo.occurrence_id,
        --o.id,
        pj.id,
        pj.program_id planting_job_program_id,
        e.program_id experiment_program_id
    FROM 
        experiment.planting_job pj 
    LEFT JOIN 
        experiment.planting_job_occurrence pjo
    ON 
        pjo.planting_job_id = pj.id
    LEFT JOIN 
        experiment.occurrence o 
    ON 
        pjo.occurrence_id = o.id
    LEFT JOIN 
        experiment.experiment e 
    ON 
        o.experiment_id  = e.id
    WHERE 
        pj.is_void =FALSE
    ORDER BY
        pj.id
)
UPDATE
    experiment.planting_job AS pj
SET
    program_id = t1.experiment_program_id 
FROM
    t1
WHERE
    pj.id = t1.id
;



-- revert changes
--rollback SELECT NULL;