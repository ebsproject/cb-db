--liquibase formatted sql

--changeset postgres:update_ss_bg_threshold_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4695 As a user, I should be able to use lists that consists of more than 500 items



-- update config
UPDATE platform.config
SET
    config_value = $${
        "addWorkingList": {
            "size": "500",
            "description": "Update entries of an Experiment"
        },
        "inputListLimit": {
            "size": "1000",
            "description": "Maximum number of input list items to be processed"
        },
        "deleteWorkingList": {
            "size": "500",
            "description": "Delete entries of an Experiment"
        },
        "exportWorkingList": {
            "size": "1000",
            "description": "Minimum threshold to trigger background job"
        }
    }$$
WHERE
    abbrev = 'FIND_SEEDS_BG_PROCESSING_THRESHOLD';



--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $${
--rollback   "addWorkingList": {
--rollback     "size": "500",
--rollback     "description": "Update entries of an Experiment"
--rollback   },
--rollback   "inputListLimit": {
--rollback     "size": "500",
--rollback     "description": "Maximum number of input list items to be processed"
--rollback   },
--rollback   "deleteWorkingList": {
--rollback     "size": "500",
--rollback     "description": "Delete entries of an Experiment"
--rollback   },
--rollback   "exportWorkingList": {
--rollback     "size": "1000",
--rollback     "description": "Minimum threshold to trigger background job"
--rollback   }
--rollback      }$$
--rollback WHERE
--rollback     abbrev = 'FIND_SEEDS_BG_PROCESSING_THRESHOLD';