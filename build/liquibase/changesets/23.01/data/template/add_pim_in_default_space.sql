--liquibase formatted sql

--changeset postgres:add_pim_in_default_space context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4614 Add PIM in default space



UPDATE 
    platform.space 
SET 
    menu_data = 
    '
        {
            "left_menu_items": [{
                    "name": "experiment-creation",
                    "label": "Experiment creation",
                    "appAbbrev": "EXPERIMENT_CREATION"
                },
                {
                    "name": "experiment-manager",
                    "label": "Experiment manager",
                    "appAbbrev": "OCCURRENCES"
                },
                {
                    "name": "cross-manger",
                    "label": "Cross manager",
                    "appAbbrev": "CROSS_MANAGER"
                }, 
                {
                    "name": "planting-instructions-manager",
                    "label": "Planting instructions manager",
                    "appAbbrev": "PLANTING_INSTRUCTIONS_MANAGER"
                },
                {
                    "name": "data-collection-qc-quality-control",
                    "label": "Data collection",
                    "appAbbrev": "QUALITY_CONTROL"
                },
                {
                    "name": "seeds-harvest-manager",
                    "label": "Harvest manager",
                    "appAbbrev": "HARVEST_MANAGER"
                },
                {
                    "name": "searchs-germplasm",
                    "label": "Germplasm",
                    "appAbbrev": "GERMPLASM_CATALOG"
                },
                {
                    "name": "search-seeds",
                    "label": "Seed search",
                    "appAbbrev": "FIND_SEEDS"
                },
                {
                    "name": "search-traits",
                    "label": "Traits",
                    "appAbbrev": "TRAITS"
                },
                {
                    "name": "list-manager",
                    "label": "List manager",
                    "appAbbrev": "LISTS"
                }

            ],
            "main_menu_items": [{
                "icon": "help",
                "name": "help",
                "items": [{
                        "url": "https://ebsproject.atlassian.net/servicedesk/customer/portal/2",
                        "icon": "help",
                        "name": "help_support-portal",
                        "label": "Service Desk",
                        "tooltip": "Go to EBS Service Desk Portal"
                    },
                    {
                        "url": "https://ebsproject.atlassian.net/wiki/spaces/EUG/overview",
                        "icon": "book",
                        "name": "help_user-guides",
                        "label": "User Guides",
                        "tooltip": "Go to EBS User Guides"
                    },
                    {
                        "url": "https://cbapi-dev.ebsproject.org",
                        "icon": "code",
                        "name": "help_api",
                        "label": "API",
                        "tooltip": "Go to EBS CB-API Documentation"
                    }
                ],
                "label": "Help"
            }]
        }
    '
WHERE abbrev = 'DEFAULT';



--rollback UPDATE 
--rollback    platform.space 
--rollback SET 
--rollback    menu_data = '
--rollback      {
--rollback        "left_menu_items": [
--rollback          {
--rollback            "name": "experiment-creation",
--rollback            "label": "Experiment creation",
--rollback            "appAbbrev": "EXPERIMENT_CREATION"
--rollback          },
--rollback          {
--rollback            "name": "experiment-manager",
--rollback            "label": "Experiment manager",
--rollback            "appAbbrev": "OCCURRENCES"
--rollback          },
--rollback          {
--rollback            "name": "cross-manger",
--rollback            "label": "Cross manager",
--rollback            "appAbbrev": "CROSS_MANAGER"
--rollback          },
--rollback          {
--rollback            "name": "data-collection-qc-quality-control",
--rollback            "label": "Data collection",
--rollback            "appAbbrev": "QUALITY_CONTROL"
--rollback          },
--rollback          {
--rollback            "name": "seeds-harvest-manager",
--rollback            "label": "Harvest manager",
--rollback            "appAbbrev": "HARVEST_MANAGER"
--rollback          },
--rollback          {
--rollback            "name": "searchs-germplasm",
--rollback            "label": "Germplasm",
--rollback            "appAbbrev": "GERMPLASM_CATALOG"
--rollback          },
--rollback          {
--rollback            "name": "search-seeds",
--rollback            "label": "Seed search",
--rollback            "appAbbrev": "FIND_SEEDS"
--rollback          },
--rollback          {
--rollback            "name": "inventory-manager",
--rollback            "label": "Inventory manager",
--rollback            "appAbbrev": "INVENTORY_MANAGER"
--rollback          },
--rollback          {
--rollback            "name": "search-traits",
--rollback            "label": "Traits",
--rollback            "appAbbrev": "TRAITS"
--rollback          },
--rollback          {
--rollback            "name": "list-manager",
--rollback            "label": "List manager",
--rollback            "appAbbrev": "LISTS"
--rollback          }
--rollback        ],
--rollback        "main_menu_items": [
--rollback          {
--rollback            "icon": "help",
--rollback            "name": "help",
--rollback            "items": [
--rollback              {
--rollback                "url": "https://ebsproject.atlassian.net/servicedesk/customer/portal/2",
--rollback                "icon": "help",
--rollback                "name": "help_support-portal",
--rollback                "label": "Service Desk",
--rollback                "tooltip": "Go to EBS Service Desk Portal"
--rollback              },
--rollback              {
--rollback                "url": "https://ebsproject.atlassian.net/wiki/spaces/EUG/overview",
--rollback                "icon": "book",
--rollback                "name": "help_user-guides",
--rollback                "label": "User Guides",
--rollback                "tooltip": "Go to EBS User Guides"
--rollback              },
--rollback              {
--rollback                "url": "https://cbapi-dev.ebsproject.org",
--rollback                "icon": "code",
--rollback                "name": "help_api",
--rollback                "label": "API",
--rollback                "tooltip": "Go to EBS CB-API Documentation"
--rollback              }
--rollback            ],
--rollback            "label": "Help"
--rollback          }
--rollback        ]
--rollback      } '
--rollback WHERE abbrev = 'DEFAULT';
