--liquibase formatted sql

--changeset postgres:curate_double_transactions_for_harvest_record_creation.sql context:template splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1922 Curate double transactions for harvest record creation



UPDATE
    api.background_job
SET
    is_void = TRUE
WHERE
    worker_name = 'CreateHarvestRecords'
    AND job_status IN ('IN_QUEUE', 'IN_PROGRESS')
    AND creation_timestamp BETWEEN '1970-01-01 00:00:00.0' AND '2023-02-01 23:59:59.0'
    AND is_void = FALSE
;



-- revert changes
--rollback SELECT NULL;