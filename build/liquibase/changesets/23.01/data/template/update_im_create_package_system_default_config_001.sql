--liquibase formatted sql

--changeset postgres:update_im_create_package_system_default_config_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4790 IM DB: Change all instances of seeds-search to seed-packages-search?dataLevel=all in all IM configs



-- update config
UPDATE platform.config
SET
    config_value = $$
    {
        "values": [
            {
            "name": "Seed Code",
            "type": "column",
            "view": {
                "visible": "false",
                "entities": []
            },
            "usage": "required",
            "abbrev": "SEED_CODE",
            "entity": "package",
            "required": "true",
            "api_field": "seedDbId",
            "data_type": "string",
            "http_method": "POST",
            "value_filter": "seedCode",
            "skip_creation": "false",
            "retrieve_db_id": "true",
            "url_parameters": "dataLevel=all&limit=1",
            "db_id_api_field": "seedDbId",
            "search_endpoint": "seed-packages-search",
            "additional_filters": {}
            },
            {
            "name": "Seed Name",
            "type": "column",
            "view": {
                "visible": "true",
                "entities": [
                "package"
                ]
            },
            "usage": "optional",
            "abbrev": "SEED_NAME",
            "entity": "package",
            "required": "false",
            "api_field": "",
            "data_type": "string",
            "http_method": "POST",
            "value_filter": "seedName",
            "skip_creation": "true",
            "retrieve_db_id": "true",
            "url_parameters": "dataLevel=all&limit=1",
            "db_id_api_field": "seedDbId",
            "search_endpoint": "seed-packages-search",
            "additional_filters": {}
            },
            {
            "name": "Package Label",
            "type": "column",
            "view": {
                "visible": "true",
                "entities": [
                "package"
                ]
            },
            "usage": "required",
            "abbrev": "PACKAGE_LABEL",
            "entity": "package",
            "required": "true",
            "api_field": "packageLabel",
            "data_type": "string",
            "http_method": "",
            "value_filter": "",
            "skip_creation": "false",
            "retrieve_db_id": "false",
            "url_parameters": "",
            "db_id_api_field": "",
            "search_endpoint": "",
            "additional_filters": {}
            },
            {
            "name": "Program",
            "type": "column",
            "view": {
                "visible": "true",
                "entities": [
                "package"
                ]
            },
            "usage": "required",
            "abbrev": "PROGRAM",
            "entity": "package",
            "required": "true",
            "api_field": "programDbId",
            "data_type": "string",
            "http_method": "POST",
            "value_filter": "programCode",
            "skip_creation": "false",
            "retrieve_db_id": "true",
            "url_parameters": "limit=1",
            "db_id_api_field": "programDbId",
            "search_endpoint": "programs-search",
            "additional_filters": {}
            },
            {
            "name": "Package Status",
            "type": "column",
            "view": {
                "visible": "true",
                "entities": [
                "package"
                ]
            },
            "usage": "required",
            "abbrev": "PACKAGE_STATUS",
            "entity": "package",
            "required": "true",
            "api_field": "packageStatus",
            "data_type": "string",
            "http_method": "",
            "value_filter": "",
            "skip_creation": "false",
            "retrieve_db_id": "false",
            "url_parameters": "",
            "db_id_api_field": "",
            "search_endpoint": "",
            "additional_filters": {}
            }
        ]
        }
    $$
WHERE
    abbrev = 'IM_CREATE_PACKAGE_SYSTEM_DEFAULT';



--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $$
--rollback     {
--rollback         "values": [
--rollback             {
--rollback             "name": "Seed Code",
--rollback             "type": "column",
--rollback             "view": {
--rollback                 "visible": "false",
--rollback                 "entities": []
--rollback             },
--rollback             "usage": "required",
--rollback             "abbrev": "SEED_CODE",
--rollback             "entity": "package",
--rollback             "required": "true",
--rollback             "api_field": "seedDbId",
--rollback             "data_type": "string",
--rollback             "http_method": "POST",
--rollback             "value_filter": "seedCode",
--rollback             "skip_creation": "false",
--rollback             "retrieve_db_id": "true",
--rollback             "url_parameters": "limit=1",
--rollback             "db_id_api_field": "seedDbId",
--rollback             "search_endpoint": "seeds-search",
--rollback             "additional_filters": {}
--rollback             },
--rollback             {
--rollback             "name": "Seed Name",
--rollback             "type": "column",
--rollback             "view": {
--rollback                 "visible": "true",
--rollback                 "entities": [
--rollback                 "package"
--rollback                 ]
--rollback             },
--rollback             "usage": "optional",
--rollback             "abbrev": "SEED_NAME",
--rollback             "entity": "package",
--rollback             "required": "false",
--rollback             "api_field": "",
--rollback             "data_type": "string",
--rollback             "http_method": "POST",
--rollback             "value_filter": "seedName",
--rollback             "skip_creation": "true",
--rollback             "retrieve_db_id": "true",
--rollback             "url_parameters": "limit=1",
--rollback             "db_id_api_field": "seedDbId",
--rollback             "search_endpoint": "seeds-search",
--rollback             "additional_filters": {}
--rollback             },
--rollback             {
--rollback             "name": "Package Label",
--rollback             "type": "column",
--rollback             "view": {
--rollback                 "visible": "true",
--rollback                 "entities": [
--rollback                 "package"
--rollback                 ]
--rollback             },
--rollback             "usage": "required",
--rollback             "abbrev": "PACKAGE_LABEL",
--rollback             "entity": "package",
--rollback             "required": "true",
--rollback             "api_field": "packageLabel",
--rollback             "data_type": "string",
--rollback             "http_method": "",
--rollback             "value_filter": "",
--rollback             "skip_creation": "false",
--rollback             "retrieve_db_id": "false",
--rollback             "url_parameters": "",
--rollback             "db_id_api_field": "",
--rollback             "search_endpoint": "",
--rollback             "additional_filters": {}
--rollback             },
--rollback             {
--rollback             "name": "Program",
--rollback             "type": "column",
--rollback             "view": {
--rollback                 "visible": "true",
--rollback                 "entities": [
--rollback                 "package"
--rollback                 ]
--rollback             },
--rollback             "usage": "required",
--rollback             "abbrev": "PROGRAM",
--rollback             "entity": "package",
--rollback             "required": "true",
--rollback             "api_field": "programDbId",
--rollback             "data_type": "string",
--rollback             "http_method": "POST",
--rollback             "value_filter": "programCode",
--rollback             "skip_creation": "false",
--rollback             "retrieve_db_id": "true",
--rollback             "url_parameters": "limit=1",
--rollback             "db_id_api_field": "programDbId",
--rollback             "search_endpoint": "programs-search",
--rollback             "additional_filters": {}
--rollback             },
--rollback             {
--rollback             "name": "Package Status",
--rollback             "type": "column",
--rollback             "view": {
--rollback                 "visible": "true",
--rollback                 "entities": [
--rollback                 "package"
--rollback                 ]
--rollback             },
--rollback             "usage": "required",
--rollback             "abbrev": "PACKAGE_STATUS",
--rollback             "entity": "package",
--rollback             "required": "true",
--rollback             "api_field": "packageStatus",
--rollback             "data_type": "string",
--rollback             "http_method": "",
--rollback             "value_filter": "",
--rollback             "skip_creation": "false",
--rollback             "retrieve_db_id": "false",
--rollback             "url_parameters": "",
--rollback             "db_id_api_field": "",
--rollback             "search_endpoint": "",
--rollback             "additional_filters": {}
--rollback             }
--rollback         ]
--rollback         }
--rollback     $$
--rollback WHERE
--rollback     abbrev = 'IM_CREATE_PACKAGE_SYSTEM_DEFAULT';