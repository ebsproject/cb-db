--liquibase formatted sql

--changeset postgres:add_column_other_names_to_germplasm.germplasm context:schema splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1835 Add column other_names to germplasm.germplasm



ALTER TABLE
    germplasm.germplasm
ADD COLUMN
    other_names varchar
;

COMMENT ON COLUMN germplasm.germplasm.other_names
    IS 'Other Names: It contains concatenation of all germplasm name values related to the germplasm record [GERM_OTHERNAMES]';



-- revert changes
--rollback ALTER TABLE
--rollback     germplasm.germplasm
--rollback DROP COLUMN
--rollback     other_names
--rollback ;