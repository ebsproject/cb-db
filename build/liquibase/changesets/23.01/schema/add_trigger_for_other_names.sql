--liquibase formatted sql

--changeset postgres:add_trigger_for_other_names context:schema splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1836 Update function/trigger which can be used to update the other_names column value  



CREATE OR REPLACE FUNCTION germplasm.populate_other_names_for_germplasm_germplasm()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE 
    var_other_name varchar;
BEGIN
        IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
                SELECT 
                    string_agg(gn.name_value,';') INTO var_other_name
                FROM 
                    germplasm.germplasm g 
                JOIN
                    germplasm.germplasm_name gn 
                ON
                    gn.germplasm_id = g.id
                WHERE 
                    g.id = new.id
                ;
                NEW.other_names = var_other_name;
        END IF;
    RETURN NEW;
END;
$function$
;

CREATE TRIGGER germplasm_populate_other_names_from_germplasm_tgr BEFORE
INSERT OR UPDATE
    ON
    germplasm.germplasm FOR EACH ROW EXECUTE FUNCTION germplasm.populate_other_names_for_germplasm_germplasm()
;

CREATE OR REPLACE FUNCTION germplasm.populate_other_names_for_germplasm_germplasm_from_germplasm_germplasm_name()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE
    var_document varchar;
BEGIN
     
    IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
        UPDATE 
            germplasm.germplasm  
        SET 
            modification_timestamp = now() 
        WHERE 
            id
        IN
            (
                SELECT 
                    germplasm_id
                FROM
                    germplasm.germplasm_name g
                WHERE
                    germplasm_id = new.id
            );
    END IF;
    
    RETURN NEW;
END;
$function$
;

CREATE TRIGGER germplasm_populate_other_names_from_germplasm_name_tgr BEFORE
INSERT OR UPDATE
    ON
    germplasm.germplasm_name FOR EACH ROW EXECUTE FUNCTION germplasm.populate_other_names_for_germplasm_germplasm_from_germplasm_germplasm_name()
;



-- revert changes
--rollback DROP TRIGGER IF EXISTS germplasm_populate_other_names_from_germplasm_name_tgr ON germplasm.germplasm_name ;
--rollback DROP FUNCTION IF EXISTS germplasm.populate_other_names_for_germplasm_germplasm_from_germplasm_germplasm_name();
--rollback DROP TRIGGER IF EXISTS germplasm_populate_other_names_from_germplasm_tgr ON germplasm.germplasm;
--rollback DROP FUNCTION IF EXISTS germplasm.populate_other_names_for_germplasm_germplasm();
