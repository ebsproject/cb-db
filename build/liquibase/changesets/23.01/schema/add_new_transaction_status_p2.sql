--liquibase formatted sql

--changeset postgres:add_new_transaction_status_p2 context:schema splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1961 Add new transaction status p2



ALTER TABLE IF EXISTS data_terminal.transaction_dataset DROP CONSTRAINT IF EXISTS transaction_dataset_status;

ALTER TABLE IF EXISTS data_terminal.transaction_dataset
    ADD CONSTRAINT transaction_dataset_status CHECK (status::text = ANY (ARRAY['new'::character varying::text, 'invalid'::character varying::text, 'invalid;updated'::character varying::text, 'updated'::character varying::text, 'missing'::character varying::text, 'missing;updated'::character varying::text]));



-- revert changes
--rollback ALTER TABLE IF EXISTS data_terminal.transaction_dataset DROP CONSTRAINT IF EXISTS transaction_dataset_status;
--rollback 
--rollback ALTER TABLE IF EXISTS data_terminal.transaction_dataset
--rollback     ADD CONSTRAINT transaction_dataset_status CHECK (status::text = ANY (ARRAY['new'::character varying::text, 'invalid'::character varying::text, 'invalid;updated'::character varying::text, 'updated'::character varying::text]));