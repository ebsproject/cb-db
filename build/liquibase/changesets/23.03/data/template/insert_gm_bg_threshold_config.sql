--liquibase formatted sql

--changeset postgres:insert_gm_bg_threshold_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5231 GM: Add depth threshold value to background process threshold config



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'GERMPLASM_MANAGER_BG_PROCESSING_THRESHOLD',
        'Background processing threshold values for GERMPLASM MANAGER tool features',
        $$			
        {
            "ancestryExportDepth": {
                "size": "6",
                "description": "Maximum depth of exported germplasm ancestry information."
            }
        }
        $$,
        1,
        'germplasm_manager',
        1,
        'CORB-5231 - k.delarosa 2023-03-16'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev = 'GERMPLASM_MANAGER_BG_PROCESSING_THRESHOLD';