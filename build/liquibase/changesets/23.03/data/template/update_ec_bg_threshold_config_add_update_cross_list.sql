--liquibase formatted sql

--changeset postgres:update_ec_bg_threshold_config_add_update_cross_list context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5292 EC: Crosses>Manager Crosses - bulk update 4950 crosses leads to 504



UPDATE
    platform.config
SET
    config_value = $$
{
    "createEntries": {
        "size": "500",
        "description": "Create entries of an Experiment"
    },
    "deleteEntries": {
        "size": "1000",
        "description": "Delete entries of an Experiment"
    },
    "updateEntries": {
        "size": "500",
        "description": "Update entries of an Experiment"
    },
    "inputListLimit": {
        "size": "1000",
        "description": "Input list size limit for validation for adding entries"
    },
    "createOccurrences": {
        "size": "500",
        "description": "Create occurrence plot and/or planting instruction records of an Experiment"
    },
    "deleteOccurrences": {
        "size": "500",
        "description": "Delete occurrence plot and/or planting instruction records of an Experiment"
    },
    "reorderAllEntries": {
        "size": "200",
        "description": "Reorder all entries of an Experiment"
    },
    "updateCrossList": {
        "size": "1000",
        "description": "Update cross list records"
    },
    "exportCrossListLimit": {
        "size": "1000",
        "description": "Limit for exporting cross list records"
    }
}
$$
WHERE
    abbrev = 'EXPERIMENT_CREATION_BG_PROCESSING_THRESHOLD';



--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $$
--rollback {
--rollback     "createEntries": {
--rollback         "size": "500",
--rollback         "description": "Create entries of an Experiment"
--rollback     },
--rollback     "deleteEntries": {
--rollback         "size": "1000",
--rollback         "description": "Delete entries of an Experiment"
--rollback     },
--rollback     "updateEntries": {
--rollback         "size": "500",
--rollback         "description": "Update entries of an Experiment"
--rollback     },
--rollback     "inputListLimit": {
--rollback         "size": "1000",
--rollback         "description": "Input list size limit for validation for adding entries"
--rollback     },
--rollback     "createOccurrences": {
--rollback         "size": "500",
--rollback         "description": "Create occurrence plot and/or planting instruction records of an Experiment"
--rollback     },
--rollback     "deleteOccurrences": {
--rollback         "size": "500",
--rollback         "description": "Delete occurrence plot and/or planting instruction records of an Experiment"
--rollback     },
--rollback     "reorderAllEntries": {
--rollback         "size": "200",
--rollback         "description": "Reorder all entries of an Experiment"
--rollback     },
--rollback     "exportCrossListLimit": {
--rollback         "size": "1000",
--rollback         "description": "Limit for exporting cross list records"
--rollback     }
--rollback }
--rollback $$
--rollback WHERE
--rollback     abbrev = 'EXPERIMENT_CREATION_BG_PROCESSING_THRESHOLD';