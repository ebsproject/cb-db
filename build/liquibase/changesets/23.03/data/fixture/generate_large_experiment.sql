--liquibase formatted sql

--changeset postgres:insert_experiment context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert experiment



SET session_replication_role = replica;

COPY (
    SELECT
        prog.id AS program_id,
        pipe.id AS pipeline_id,
        stg.id AS stage_id,
        proj.id AS project_id,
        expt.experiment_year::integer AS experiment_year,
        ssn.id AS season_id,
        (expt.experiment_year || ssn.season_code) AS planting_season,
        experiment.generate_code('experiment') AS experiment_code,
        concat_ws('-', 'LRG', prog.program_code, stg.stage_code, expt.experiment_year, ssn.season_code, lpad(gs.experiment_number::text, 3, '0')) AS experiment_name,
        expt.experiment_type AS experiment_type,
        expt.experiment_sub_type AS experiment_sub_type,
        expt.experiment_sub_sub_type AS experiment_sub_sub_type,
        expt.experiment_design_type AS experiment_design_type,
        expt.experiment_status AS experiment_status,
        stwd.id AS steward_id,
        crop.id AS crop_id,
        dproc.id AS data_process_id,
        crtr.id AS creator_id,
        (expt.notes || ' ' || expt.description) AS description,
        expt.notes AS notes
    FROM
        generate_series(1, 30000) AS gs (experiment_number),
        (
            VALUES
            ('PROGL', NULL, 'OYT', NULL, '2018', 'DS', NULL, 'Breeding Trial', NULL, NULL, 'RCBD', 'planted', 'admin', 'RICE', 'BREEDING_TRIAL_DATA_PROCESS', 'admin', 'DEVOPS-2036 Insert experiment', 'DEVOPS-2036')
            -- ('IRSEA', NULL, 'PYT', NULL, '2018', 'WS', NULL, 'Breeding Trial', NULL, NULL, 'RCBD', 'planted', 'admin', 'RICE', 'BREEDING_TRIAL_DATA_PROCESS', 'admin', 'DEVOPS-2036 Insert experiment', 'DEVOPS-2036'),
            -- ('IRSEA', NULL, 'AYT', NULL, '2019', 'DS', NULL, 'Breeding Trial', NULL, NULL, 'RCBD', 'planted', 'admin', 'RICE', 'BREEDING_TRIAL_DATA_PROCESS', 'admin', 'DEVOPS-2036 Insert experiment', 'DEVOPS-2036'),
            -- ('IRSEA', NULL, 'OYT', NULL, '2019', 'WS', NULL, 'Breeding Trial', NULL, NULL, 'RCBD', 'planted', 'admin', 'RICE', 'BREEDING_TRIAL_DATA_PROCESS', 'admin', 'DEVOPS-2036 Insert experiment', 'DEVOPS-2036'),
            -- ('IRSEA', NULL, 'PYT', NULL, '2020', 'DS', NULL, 'Breeding Trial', NULL, NULL, 'RCBD', 'planted', 'admin', 'RICE', 'BREEDING_TRIAL_DATA_PROCESS', 'admin', 'DEVOPS-2036 Insert experiment', 'DEVOPS-2036'),
            -- ('IRSEA', NULL, 'AYT', NULL, '2020', 'WS', NULL, 'Breeding Trial', NULL, NULL, 'RCBD', 'planted', 'admin', 'RICE', 'BREEDING_TRIAL_DATA_PROCESS', 'admin', 'DEVOPS-2036 Insert experiment', 'DEVOPS-2036'),
            -- ('IRSEA', NULL, 'OYT', NULL, '2021', 'DS', NULL, 'Breeding Trial', NULL, NULL, 'RCBD', 'planted', 'admin', 'RICE', 'BREEDING_TRIAL_DATA_PROCESS', 'admin', 'DEVOPS-2036 Insert experiment', 'DEVOPS-2036'),
            -- ('IRSEA', NULL, 'PYT', NULL, '2021', 'WS', NULL, 'Breeding Trial', NULL, NULL, 'RCBD', 'planted', 'admin', 'RICE', 'BREEDING_TRIAL_DATA_PROCESS', 'admin', 'DEVOPS-2036 Insert experiment', 'DEVOPS-2036')
        ) AS expt (
            program,
            pipeline,
            stage,
            project,
            experiment_year,
            season,
            planting_season,
            experiment_type,
            experiment_sub_type,
            experiment_sub_sub_type,
            experiment_design_type,
            experiment_status,
            steward,
            crop,
            data_process,
            creator,
            description,
            notes
        )
        JOIN tenant.program AS prog
            ON prog.program_code = expt.program
        LEFT JOIN tenant.pipeline AS pipe
            ON pipe.pipeline_code = expt.pipeline
        JOIN tenant.stage AS stg
            ON stg.stage_code = expt.stage
        LEFT JOIN tenant.project AS proj
            ON proj.project_code = expt.project
        JOIN tenant.season AS ssn
            ON ssn.season_code = expt.season
        JOIN tenant.person AS stwd
            ON stwd.username = expt.steward
        JOIN tenant.crop AS crop
            ON crop.crop_code = expt.crop
        JOIN master.item AS dproc
            ON dproc.abbrev = expt.data_process
        JOIN tenant.person AS crtr
            ON crtr.username = expt.creator
    ORDER BY
        expt.experiment_year,
        expt.season,
        gs.experiment_number
)
TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/experiment_experiment.csv' DELIMITER ',' CSV HEADER;

COPY
    experiment.experiment (
        program_id,
        pipeline_id,
        stage_id,
        project_id,
        experiment_year,
        season_id,
        planting_season,
        experiment_code,
        experiment_name,
        experiment_type,
        experiment_sub_type,
        experiment_sub_sub_type,
        experiment_design_type,
        experiment_status,
        steward_id,
        crop_id,
        data_process_id,
        creator_id,
        description,
        notes
    )
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/experiment_experiment.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment AS expt
--rollback WHERE
--rollback     expt.notes LIKE 'DEVOPS-2036%'
--rollback ;



--changeset postgres:insert_entry_list context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert entry_list



SET session_replication_role = replica;

COPY (
    SELECT
        (expt.experiment_code || '_ENTLIST') AS entry_list_code,
        (expt.experiment_name || ' Entry List') AS entry_list_name,
        'completed' AS entry_list_status,
        'entry list' AS entry_list_type,
        expt.id AS experiment_id,
        expt.creator_id AS creator_id,
        'DEVOPS-2036 Insert entry_list'
    FROM
        experiment.experiment AS expt
    WHERE
        expt.notes LIKE 'DEVOPS-2036%'
)
TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/experiment_entry_list.csv' DELIMITER ',' CSV HEADER;

COPY
    experiment.entry_list (
        entry_list_code,
        entry_list_name,
        entry_list_status,
        entry_list_type,
        experiment_id,
        creator_id,
        notes
    )
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/experiment_entry_list.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;



-- revert changes
--rollback SET session_replication_role = replica;
--rollback DELETE FROM
--rollback     experiment.entry_list AS entlist
--rollback USING
--rollback     experiment.experiment AS expt
--rollback WHERE
--rollback     expt.id = entlist.experiment_id
--rollback     AND expt.notes LIKE 'DEVOPS-2036%';
--rollback SET session_replication_role = origin;
--rollback ;



--changeset postgres:insert_entry context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert entry



SET session_replication_role = replica;

COPY (
    WITH t1 AS (
        SELECT
            (SELECT (random() * (max(sd.id) - min(sd.id) + 1)+min(sd.id))::int AS random_num FROM germplasm.seed sd WHERE seed_name LIKE 'CLG%') seed_id,
            --(SELECT (random() * (22736656 - 12736657 + 1)+12736657)::int AS random_num) seed_id,
            el.id entry_list_id
        FROM 
            experiment.entry_list el,
            generate_series(1,30) num
        WHERE
            notes LIKE 'DEVOPS-2036%'
    ), t2 AS (
        SELECT 
            ROW_NUMBER() OVER (PARTITION BY entry_list_id ORDER BY entry_list_id) entry_code,
            seed_name AS entry_name,
            entry_list_id,
            'entry' entry_type,
            'active' entry_status,
            sd.id seed_id,
            p.id package_id,
            sd.germplasm_id,
            NULL entry_class,
            NULL entry_role,
            1 creator_id,
            'DEVOPS-2036 Insert entry' notes
        FROM 
            t1
        JOIN
            germplasm.seed sd 
        ON
            sd.id = t1.seed_id
        JOIN 
            germplasm.package p 
        ON
            p.seed_id = sd.id
    )
    SELECT
        t.entry_code,
        t.entry_code AS entry_number, 
        entry_name, 
        entry_type, 
        entry_role, 
        entry_class, 
        entry_status, 
        entry_list_id, 
        germplasm_id, 
        seed_id,
        package_id, 
        creator_id, 
        notes
    FROM t2 t
)
TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/experiment_entry.csv' DELIMITER ',' CSV HEADER;

COPY
    experiment.entry
        (
            entry_code, entry_number, entry_name, entry_type, entry_role, entry_class, 
            entry_status, entry_list_id, germplasm_id, seed_id, package_id, creator_id, notes
        )
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/experiment_entry.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM experiment.entry WHERE notes='DEVOPS-2036 Insert entry';
--rollback SET session_replication_role = origin;



--changeset postgres:insert_entry_data context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert entry data



SET session_replication_role = replica;

COPY (
    SELECT
        e.id entry_id,
        var.id AS variable_id,
        t.data_value AS data_value,
        t.data_qc_code AS data_qc_code,
        'DEVOPS-2036 Populate entry data' AS notes,
        1 creator_id
    FROM
        (
            VALUES 
                ('PLANNED_SEEDING_DATE','2015-12-29','G'),
                ('HV_METH_DISC','Bulk','G')
                -- ('PLANNED_SEEDING_VOLUME','0','G'),
                -- ('PLANNED_HARVEST_DATE','2017-01-11','G'),
                -- ('PLANNED_HARVEST_VOLUME','176','G'),
                -- ('DRYING_DATE','2015-09-23','G'),
                -- ('REMARKS','Sowed in cell blue trays for advancement','G'),
                -- ('SELECTION_REMARKS','one seed from each plant','G'),
                -- ('PURPOSE','Breeding','G'),
                -- ('NO_OF_PLANTS','120','G')
        ) AS t(var_abbrev, data_value, data_qc_code)
    JOIN master.variable var
        ON var.abbrev = t.var_abbrev
    JOIN
        experiment.entry e
    ON
        e.entry_name LIKE 'CLGE%'
) TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/experiment_entry_data.csv' DELIMITER ',' CSV HEADER;

COPY
    experiment.entry_data
        (entry_id, variable_id, data_value, data_qc_code, notes, creator_id)
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/experiment_entry_data.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM experiment.entry_data WHERE notes='DEVOPS-2036 Populate entry data';
--rollback SET session_replication_role = origin;



--changeset postgres:insert_occurrence context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert occurrence



SET session_replication_role = replica;

COPY (
    SELECT
        expt.experiment_code || '-' || lpad(geo.occurrence_number::text, 3, '0') AS occurrence_code,
        expt.experiment_name || '-' || lpad(geo.occurrence_number::text, 3, '0') AS occurrence_name,
        occ.occurrence_status,
        expt.id AS experiment_id,
        site.id AS site_id,
        field.id AS field_id,
        occ.rep_count,
        geo.occurrence_number,
        1 AS creator_id,
        'DEVOPS-2036 Insert occurrence' AS notes,
        30 AS entry_count,
        90 AS plot_count
    FROM
        experiment.experiment AS expt,
        (
            VALUES
            -- a.carlson
            ('IRSEA-OYT-2018-DS', 'planted', 3)
            --('IRSEA-PYT-2018-WS', 'planted', 3)
            --('IRSEA-AYT-2019-DS', 'planted', 3),
            -- h.tamsin
            --('IRSEA-OYT-2019-WS', 'planted', 3),
            --('IRSEA-PYT-2020-DS', 'planted', 3),
            --('IRSEA-AYT-2020-WS', 'planted', 3),
            -- k.khadija
            --('IRSEA-OYT-2021-DS', 'planted', 3),
            --('IRSEA-PYT-2021-WS', 'planted', 3)
        ) AS occ (
            experiment_name_fragment,
            occurrence_status,
            rep_count
        ),
        (
            VALUES
            (1, 'IRRIHQ', 'UB1'),
            (2, 'BD_DA_GZ', 'BRRI_1')
            --(3, 'IN_TG_HY', 'HYDERABAD_FARM'),
            --(4, 'MM_NY', 'YEZIN'),
            --(5, 'LK_KT', 'BOMBUWELA_FARM')
        ) AS geo (
            occurrence_number, site, field
        )
        JOIN place.geospatial_object AS site
            ON site.geospatial_object_code = geo.site
        JOIN place.geospatial_object AS field
            ON field.geospatial_object_code = geo.field
    WHERE
        expt.notes LIKE 'DEVOPS-2036%'
        --AND expt.experiment_name LIKE occ.experiment_name_fragment || '%'
    ORDER BY
        expt.id
)
TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/experiment_occurrence.csv' DELIMITER ',' CSV HEADER;

COPY
    experiment.occurrence (
        occurrence_code,
        occurrence_name,
        occurrence_status,
        experiment_id,
        site_id,
        field_id,
        rep_count,
        occurrence_number,
        creator_id,
        notes,
        entry_count,
        plot_count
    )
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/experiment_occurrence.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM experiment.occurrence WHERE notes='DEVOPS-2036 Insert occurrence';
--rollback SET session_replication_role = origin;



--changeset postgres:insert_planting_areas context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert planting areas



SET session_replication_role = replica;

COPY (
    WITH t_occ AS (
        SELECT
            expt.id AS experiment_id,
            expt.experiment_year,
            expt.season_id,
            ssn.season_code,
            occ.id AS occurrence_id,
            occ.occurrence_name,
            occ.site_id,
            occ.field_id,
            site.geospatial_object_code AS site_code,
            site.parent_geospatial_object_id AS site_parent_id,
            site.root_geospatial_object_id AS site_root_id,
            occ.creator_id
        FROM
            experiment.occurrence AS occ
            JOIN experiment.experiment AS expt
                ON expt.id = occ.experiment_id
            JOIN tenant.season AS ssn
                ON ssn.id = expt.season_id
            JOIN place.geospatial_object AS site
                ON site.id = occ.site_id
        WHERE
            expt.notes LIKE 'DEVOPS-2036%'
        ORDER BY
            expt.id,
            occ.id
    ), t_logrp AS (
        SELECT
            occ.*,
            NTILE(5000) OVER (ORDER BY occ.experiment_year, occ.season_id, occ.site_id) AS location_occurrence_group_number -- total number of planting areas/locations
        FROM
            t_occ AS occ
        ORDER BY
            location_occurrence_group_number
    ), t_loc1 AS (
        SELECT
            t.location_occurrence_group_number,
            (array_agg(DISTINCT t.experiment_year))[1] AS experiment_year,
            (array_agg(DISTINCT t.season_id))[1] AS season_id,
            (array_agg(DISTINCT t.season_code))[1] AS season_code,
            (array_agg(DISTINCT t.site_id))[1] AS site_id,
            (array_agg(DISTINCT t.site_code))[1] AS site_code,
            (array_agg(DISTINCT t.field_id))[1] AS field_id,
            (array_agg(DISTINCT t.site_parent_id))[1] AS site_parent_id,
            (array_agg(DISTINCT t.site_root_id))[1] AS site_root_id,
            (array_agg(DISTINCT t.creator_id))[1] AS creator_id
        FROM
            t_logrp AS t
        GROUP BY
            t.location_occurrence_group_number
    ), t_loc2 AS (
        SELECT
            t.experiment_year,
            t.season_id,
            t.season_code,
            t.site_id,
            t.site_code,
            ROW_NUMBER() OVER (PARTITION BY t.experiment_year, t.season_code, t.site_code) AS location_number,
            (array_agg(DISTINCT t.field_id))[1] AS field_id,
            (array_agg(DISTINCT t.site_parent_id))[1] AS site_parent_id,
            (array_agg(DISTINCT t.site_root_id))[1] AS site_root_id,
            (array_agg(DISTINCT t.creator_id))[1] AS creator_id
        FROM
            t_loc1 AS t
        GROUP BY
            t.experiment_year,
            t.season_id,
            t.season_code,
            t.site_id,
            t.site_code,
            t.location_occurrence_group_number --% 2 -- number of locations per breeding season
        ORDER BY
            t.experiment_year,
            t.season_id,
            t.site_id
    ), t_pa AS (
        SELECT
            concat_ws('-', 'DEVOPS-2036', t.site_code, t.experiment_year, t.season_code, t.location_number::TEXT) geospatial_object_code,
            concat_ws('-', t.site_code, t.experiment_year, t.season_code, t.location_number::TEXT) geospatial_object_name, -- number of digits
            'planting area' AS geospatial_object_type,
            'breeding location' AS geospatial_object_subtype,
            t.site_parent_id AS parent_geospatial_object_id,
            t.site_root_id AS root_geospatial_object_id,
            t.creator_id,
            'DEVOPS-2036 Insert planting areas' AS notes
        FROM
            t_loc2 AS t
        ORDER BY
            t.experiment_year,
            t.season_id,
            t.site_id,
            t.location_number
    )
    SELECT
        DISTINCT ON (geospatial_object_code)
        t.*
    FROM
        t_pa AS t
) 
TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/experiment_planting_area.csv' DELIMITER ',' CSV HEADER;

COPY
    place.geospatial_object (
        geospatial_object_code,
        geospatial_object_name,
        geospatial_object_type,
        geospatial_object_subtype,
        parent_geospatial_object_id,
        root_geospatial_object_id,
        creator_id,
        notes
    )
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/experiment_planting_area.csv' DELIMITER ',' CSV HEADER;


SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM place.geospatial_object WHERE notes='DEVOPS-2036 Insert planting areas';
--rollback SET session_replication_role = origin;



--changeset postgres:insert_location context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert location



SET session_replication_role = replica;

COPY (
    WITH t_occ AS (
        SELECT
            expt.id AS experiment_id,
            expt.experiment_year,
            expt.season_id,
            ssn.season_code,
            occ.id AS occurrence_id,
            occ.occurrence_name,
            occ.site_id,
            occ.field_id,
            site.geospatial_object_code AS site_code,
            site.parent_geospatial_object_id AS site_parent_id,
            site.root_geospatial_object_id AS site_root_id,
            occ.creator_id
        FROM
            experiment.occurrence AS occ
            JOIN experiment.experiment AS expt
                ON expt.id = occ.experiment_id
            JOIN tenant.season AS ssn
                ON ssn.id = expt.season_id
            JOIN place.geospatial_object AS site
                ON site.id = occ.site_id
        WHERE
            expt.notes LIKE 'DEVOPS-2036%'
        ORDER BY
            expt.id,
            occ.id
    ), t_logrp AS (
        SELECT
            occ.*,
            NTILE(5000) OVER (ORDER BY occ.experiment_year, occ.season_id, occ.site_id) AS location_occurrence_group_number -- total number of planting areas/locations
        FROM
            t_occ AS occ
        ORDER BY
            location_occurrence_group_number
    ), t_loc1 AS (
        SELECT
            t.location_occurrence_group_number,
            (array_agg(DISTINCT t.experiment_year))[1] AS experiment_year,
            (array_agg(DISTINCT t.season_id))[1] AS season_id,
            (array_agg(DISTINCT t.season_code))[1] AS season_code,
            (array_agg(DISTINCT t.site_id))[1] AS site_id,
            (array_agg(DISTINCT t.site_code))[1] AS site_code,
            (array_agg(DISTINCT t.field_id))[1] AS field_id,
            (array_agg(DISTINCT t.site_parent_id))[1] AS site_parent_id,
            (array_agg(DISTINCT t.site_root_id))[1] AS site_root_id,
            (array_agg(DISTINCT t.creator_id))[1] AS creator_id
        FROM
            t_logrp AS t
        GROUP BY
            t.location_occurrence_group_number
    ), t_loc2 AS (
        SELECT
            t.experiment_year,
            t.season_id,
            t.season_code,
            t.site_id,
            t.site_code,
            ROW_NUMBER() OVER (PARTITION BY t.experiment_year, t.season_code, t.site_code) AS location_number,
            (array_agg(DISTINCT t.field_id))[1] AS field_id,
            (array_agg(DISTINCT t.site_parent_id))[1] AS site_parent_id,
            (array_agg(DISTINCT t.site_root_id))[1] AS site_root_id,
            (array_agg(DISTINCT t.creator_id))[1] AS creator_id
        FROM
            t_loc1 AS t
        GROUP BY
            t.experiment_year,
            t.season_id,
            t.season_code,
            t.site_id,
            t.site_code,
            t.location_occurrence_group_number --% 2 -- number of locations per breeding season
        ORDER BY
            t.experiment_year,
            t.season_id,
            t.site_id
    ), t_loc3 AS (
        SELECT
            concat_ws('-', 'DEVOPS-2036', t.site_code, t.experiment_year, t.season_code, t.location_number::TEXT) AS location_code,
            concat_ws('-', t.site_code, t.experiment_year, t.season_code, t.location_number::TEXT) AS location_name,
            'planted' AS location_status,
            'planting area' AS location_type,
            t.experiment_year AS location_year,
            t.season_id,
            t.location_number,
            t.site_id,
            t.field_id,
            t.creator_id AS steward_id,
            t.creator_id
        FROM
            t_loc2 AS t
        ORDER BY
            t.experiment_year,
            t.season_id,
            t.site_id,
            t.location_number
    ), t_loc4 AS (
        SELECT
            t.*,
            geo.id AS geospatial_object_id,
            'DEVOPS-2036 Insert location'
        FROM
            t_loc3 AS t
            JOIN place.geospatial_object AS geo
                ON geo.geospatial_object_code = t.location_code
        ORDER BY
            t.location_year,
            t.season_id,
            t.site_id,
            t.location_number
    )
    SELECT
        t.*
    FROM
        t_loc4 AS t
)
TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/experiment_location.csv' DELIMITER ',' CSV HEADER;

COPY
    experiment.location (
        location_code,
        location_name,
        location_status,
        location_type,
        location_year,
        season_id,
        location_number,
        site_id,
        field_id,
        steward_id,
        creator_id,
        geospatial_object_id,
        notes
    )
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/experiment_location.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM experiment.location WHERE notes='DEVOPS-2036 Insert location';
--rollback SET session_replication_role = origin;



--changeset postgres:insert_location_occurrence_group context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert location_occurrence_group



SET session_replication_role = replica;

COPY (
    WITH t_occ AS (
        SELECT
            expt.id AS experiment_id,
            expt.experiment_year,
            expt.season_id,
            ssn.season_code,
            occ.id AS occurrence_id,
            occ.occurrence_name,
            occ.site_id,
            occ.field_id,
            site.geospatial_object_code AS site_code,
            site.parent_geospatial_object_id AS site_parent_id,
            site.root_geospatial_object_id AS site_root_id,
            occ.creator_id
        FROM
            experiment.occurrence AS occ
            JOIN experiment.experiment AS expt
                ON expt.id = occ.experiment_id
            JOIN tenant.season AS ssn
                ON ssn.id = expt.season_id
            JOIN place.geospatial_object AS site
                ON site.id = occ.site_id
        WHERE
            expt.notes LIKE 'DEVOPS-2036%'
        ORDER BY
            expt.id,
            occ.id
    ), t_logrp AS (
        SELECT
            occ.*,
            NTILE(5000) OVER (ORDER BY occ.experiment_year, occ.season_id, occ.site_id) AS location_occurrence_group_number -- total number of planting areas/locations
        FROM
            t_occ AS occ
        ORDER BY
            location_occurrence_group_number
    ), t_loc1 AS (
        SELECT
            t.location_occurrence_group_number,
            array_agg(t.occurrence_id) AS occurrence_id_list,
            (array_agg(DISTINCT t.experiment_year))[1] AS experiment_year,
            (array_agg(DISTINCT t.season_id))[1] AS season_id,
            (array_agg(DISTINCT t.season_code))[1] AS season_code,
            (array_agg(DISTINCT t.site_id))[1] AS site_id,
            (array_agg(DISTINCT t.site_code))[1] AS site_code,
            (array_agg(DISTINCT t.field_id))[1] AS field_id,
            (array_agg(DISTINCT t.site_parent_id))[1] AS site_parent_id,
            (array_agg(DISTINCT t.site_root_id))[1] AS site_root_id,
            (array_agg(DISTINCT t.creator_id))[1] AS creator_id
        FROM
            t_logrp AS t
        GROUP BY
            t.location_occurrence_group_number
    ), t_loc2 AS (
        SELECT
            t.experiment_year,
            t.season_id,
            t.season_code,
            t.site_id,
            t.site_code,
            t.occurrence_id_list,
            ROW_NUMBER() OVER (PARTITION BY t.experiment_year, t.season_code, t.site_code) AS location_number,
            (array_agg(DISTINCT t.field_id))[1] AS field_id,
            (array_agg(DISTINCT t.site_parent_id))[1] AS site_parent_id,
            (array_agg(DISTINCT t.site_root_id))[1] AS site_root_id,
            (array_agg(DISTINCT t.creator_id))[1] AS creator_id
        FROM
            t_loc1 AS t
        GROUP BY
            t.experiment_year,
            t.season_id,
            t.season_code,
            t.site_id,
            t.site_code,
            t.occurrence_id_list,
            t.location_occurrence_group_number --% 500 -- number of locations per breeding season
        ORDER BY
            t.experiment_year,
            t.season_id,
            t.site_id
    ), t_loc AS (
        SELECT
            loc.id AS location_id,
            occ.id AS occurrence_id,
            ROW_NUMBER() OVER (PARTITION BY loc.id ORDER BY occ.id) AS order_number,
            loc.creator_id,
            'DEVOPS-2036 Insert location_occurrence_group'
        FROM
            experiment.location AS loc
            JOIN t_loc2 AS t
                ON loc.location_code = CONCAT_WS('-', 'DEVOPS-2036', t.site_code, t.experiment_year, t.season_code, t.location_number::TEXT),
            UNNEST(t.occurrence_id_list) AS occ (id)
        WHERE
            loc.location_code LIKE 'DEVOPS-2036%'
        ORDER BY
            loc.location_year,
            loc.season_id,
            loc.site_id,
            loc.location_number
    )
    SELECT
        t.*
    FROM
        t_loc AS t
)
TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/experiment_location_group.csv' DELIMITER ',' CSV HEADER;

COPY
    experiment.location_occurrence_group (
        location_id,
        occurrence_id,
        order_number,
        creator_id,
        notes
    )
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/experiment_location_group.csv' DELIMITER ',' CSV HEADER;


SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM experiment.location_occurrence_group WHERE notes='DEVOPS-2036 Insert location_occurrence_group';
--rollback SET session_replication_role = origin;



--changeset postgres:insert_plot context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert plot



SET session_replication_role = replica;

COPY (
    WITH t_loc AS (
        SELECT
            expt.id AS experiment_id,
            entlist.id AS entry_list_id,
            occ.id AS occurrence_id,
            loc.id AS location_id,
            occ.rep_count,
            logrp.order_number,
            logrp.creator_id
        FROM
            experiment.experiment AS expt
            JOIN experiment.entry_list AS entlist
                ON entlist.experiment_id = expt.id
            JOIN experiment.occurrence AS occ
                ON occ.experiment_id = expt.id
            JOIN experiment.location_occurrence_group AS logrp
                ON logrp.occurrence_id = occ.id
            JOIN experiment.location AS loc
                ON loc.id = logrp.location_id
        WHERE
            loc.location_code LIKE 'DEVOPS-2036%'
        ORDER BY
            loc.id
    ), t_plot AS (
        SELECT
            t.occurrence_id,
            t.location_id,
            ent.id AS entry_id,
            ROW_NUMBER() OVER (PARTITION BY t.location_id, t.occurrence_id ORDER BY t.location_id, t.occurrence_id) AS plot_number,
            'plot' AS plot_type,
            gs.rep,
            t.order_number,
            t.order_number AS block_number,
            'Q' AS plot_qc_code,
            'active' AS plot_status,
            'READY' AS harvest_status,
            t.creator_id
        FROM
            t_loc AS t
            JOIN experiment.entry AS ent
                ON ent.entry_list_id = t.entry_list_id,
            generate_series(1, t.rep_count) AS gs (rep)
    ), t_coords AS (
        SELECT
            t_design.order_number,
            t_design.plot_number,
            t_design.x AS design_x,
            t_design.y AS design_y,
            t_pa.x AS pa_x,
            t_pa.y AS pa_y,
            t_field.x AS field_x,
            t_field.y AS field_y
        FROM
            (
                SELECT
                    o AS order_number,
                    ROW_NUMBER() OVER () AS layout_order_number,
                    ROW_NUMBER() OVER (PARTITION BY o ORDER BY random()) AS plot_number,
                    x,
                    y
                FROM
                    generate_series(1, 2) AS o, -- no. of occurrences per location
                    generate_series(1, 10) AS x, -- for 1 occurrence only
                    generate_series(1, 100) AS y -- for 1 occurrence only
            ) AS t_design,
            (
                SELECT
                    ROW_NUMBER() OVER () AS layout_order_number,
                    ROW_NUMBER() OVER (ORDER BY random()) AS plot_number,
                    x,
                    y
                FROM
                    generate_series(1, 10 * 2) AS x, -- for all plots in a location (occurrences x plots)
                    generate_series(1, 100) AS y
            ) AS t_pa,
            (
                SELECT
                    ROW_NUMBER() OVER () AS layout_order_number,
                    ROW_NUMBER() OVER (ORDER BY random()) AS plot_number,
                    x,
                    y
                FROM
                    generate_series(1, 10 * 2) AS x, -- for all plots in a location (occurrences x plots)
                    generate_series(1, 100) AS y
            ) AS t_field
        WHERE
            t_design.layout_order_number = t_pa.layout_order_number
            AND t_design.layout_order_number = t_field.layout_order_number
    )
    SELECT
        t.occurrence_id,
        t.location_id,
        t.entry_id,
        t.plot_number AS plot_code,
        t.plot_number,
        t.plot_type,
        t.rep,
        u.design_x,
        u.design_y,
        u.pa_x,
        u.pa_y,
        u.field_x,
        u.field_y,
        t.block_number,
        t.plot_qc_code,
        t.plot_status,
        t.harvest_status,
        t.creator_id,
        'DEVOPS-2036 Insert plot' notes
    FROM
        t_plot AS t
        JOIN t_coords AS u
            ON t.plot_number = u.plot_number
            AND t.order_number = u.order_number
)
TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/experiment_plot.csv' DELIMITER ',' CSV HEADER;

COPY
    experiment.plot (
        occurrence_id,
        location_id,
        entry_id,
        plot_code,
        plot_number,
        plot_type,
        rep,
        design_x,
        design_y,
        pa_x,
        pa_y,
        field_x,
        field_y,
        block_number,
        plot_qc_code,
        plot_status,
        harvest_status,
        creator_id,
        notes
        )
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/experiment_plot.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM experiment.plot WHERE notes='DEVOPS-2036 Insert plot';
--rollback SET session_replication_role = origin;



--changeset postgres:insert_plot_data context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert plot data



SET session_replication_role = replica;

COPY (
 SELECT
        p.id AS plot_id,
        var.id AS variable_id,
        t.data_value AS data_value,
        t.data_qc_code AS data_qc_code,
        1 creator_id,
        'DEVOPS-2036 Populate plot data' notes
    FROM
        (
            VALUES 
                --SET 1
                ('BB_SES5_GH_SCOR_1_9','4','G'),
                ('HVDATE_CONT','2021-06-09','Q'),
                ('LG_SCOR_1_9','9','G'),
                ('MC_CONT','11.7','G'),
                ('HT_CONT','99.5','Q')
                -- ('HT1_CONT','72','Q'),
                -- ('HT2_CONT','94','G'),
                -- ('HT3_CONT','90','Q'),
                -- ('HT4_CONT','94','G')
                -- ('AYLD_CONT','1512.91','G'),
                -- ('FLW_DATE_CONT','2021-04-29','Q'),
                -- ('HV_METH_DISC','Bulk','Q'),
                -- ('HT5_CONT','94','G'),
                -- ('HT6_CONT','109','G'),
                -- ('NO_OF_PLANTS','155','G'),
                -- ('CML5_CONT',NULL,'M'),
                -- ('THRESHING_TIME_CONT',NULL,'G'),
                -- ('DENSITY_CONT','5.03','G'),
                -- ('PANNO_SEL','65','G'),
                -- ('HT7_CONT','103','G')
        ) AS t(var_abbrev, data_value, data_qc_code)
     JOIN master.variable var
                ON var.abbrev = t.var_abbrev
     JOIN experiment.plot p
                 ON p.notes LIKE 'DEVOPS-2036%'
)
TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/experiment_plot_data.csv' DELIMITER ',' CSV HEADER;

COPY
    experiment.plot_data
        (plot_id, variable_id, data_value, data_qc_code, creator_id, notes)
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/experiment_plot_data.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM experiment.plot_data WHERE notes='DEVOPS-2036 Populate plot data';
--rollback SET session_replication_role = origin;



--changeset postgres:insert_planting_instruction context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert planting instruction



SET session_replication_role = replica;

COPY (
    SELECT
        ent.entry_code,
        ent.entry_number,
        ent.entry_name,
        ent.entry_type,
        ent.entry_role,
        ent.entry_class,
        ent.entry_status,
        ent.id AS entry_id,
        plot.id AS plot_id,
        ent.germplasm_id,
        ent.seed_id,
        ent.package_id,
        NULL AS package_log_id,
        plot.creator_id,
        'DEVOPS-2036 Insert planting instruction'
    FROM
        experiment.experiment AS expt
        JOIN experiment.occurrence AS occ
            ON occ.experiment_id = expt.id
        JOIN experiment.plot AS plot
            ON plot.occurrence_id = occ.id
        JOIN experiment.entry AS ent
            ON ent.id = plot.entry_id
    WHERE
        expt.notes LIKE 'DEVOPS-2036%'
    ORDER BY
        plot.id
)
TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/experiment_planting_instruction.csv' DELIMITER ',' CSV HEADER;

COPY
    experiment.planting_instruction (
        entry_code,
        entry_number,
        entry_name,
        entry_type,
        entry_role,
        entry_class,
        entry_status,
        entry_id,
        plot_id,
        germplasm_id,
        seed_id,
        package_id,
        package_log_id,
        creator_id,
        notes
)
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/experiment_planting_instruction.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM experiment.planting_instruction WHERE notes='DEVOPS-2036 Insert planting instruction';
--rollback SET session_replication_role = origin;



--changeset postgres:insert_experiment_design context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert experiment design



SET session_replication_role = replica;

COPY (
    SELECT
        eo.id AS occurrence_id,
        ep.rep AS design_id,
        ep.id AS plot_id,
        'replication block',
        ep.rep AS block_value,
        ep.rep AS block_level_number,
        exp.creator_id AS creator_id,
        'replicate' AS block_name,
        'DEVOPS-2036 Insert experiment design' AS  notes
    FROM 
        experiment.experiment exp
    INNER JOIN
        experiment.occurrence eo
    ON
        eo.experiment_id = exp.id
    INNER JOIN
        experiment.plot ep
    ON
        ep.occurrence_id = eo.id
    WHERE 
        exp.notes LIKE 'DEVOPS-2036%'
)
TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/experiment_eperiment_design.csv' DELIMITER ',' CSV HEADER;

COPY
   experiment.experiment_design (
       occurrence_id, design_id, plot_id, block_type, block_value,
       block_level_number, creator_id, block_name, notes
   )
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/experiment_eperiment_design.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM experiment.experiment_design WHERE notes='DEVOPS-2036 Insert experiment design';
--rollback SET session_replication_role = origin;



--changeset postgres:insert_list context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert list



SET session_replication_role = replica;

COPY (
    SELECT
        (list.list_code_prefix || '_' || expt.experiment_code) AS abbrev,
        (expt.experiment_name || ' ' || list.list_name_prefix || ' (' || expt.experiment_code || ')') AS name,
        (expt.experiment_name || ' ' || list.list_name_prefix || ' (' || expt.experiment_code || ')') AS display_name,
        list.list_type AS type,
        entity.id AS entity_id,
        expt.creator_id,
        'working list' AS list_usage,
        'created' AS status,
        TRUE AS is_active,
        'DEVOPS-2036 Insert list'
    FROM (
            VALUES
            ('TRAIT_PROTOCOL', 'Trait Protocol', 'trait protocol', 'TRAIT'),
            ('MANAGEMENT_PROTOCOL', 'Management Protocol', 'management protocol', 'MANAGEMENT')
        ) AS list (
            list_code_prefix,
            list_name_prefix,
            list_type,
            entity
        )
        JOIN experiment.experiment AS expt
            ON expt.notes LIKE 'DEVOPS-2036%'
        JOIN dictionary.entity AS entity
            ON entity.abbrev = list.entity
)
TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/platform_list.csv' DELIMITER ',' CSV HEADER;

COPY
    platform.list (
        abbrev,
        name,
        display_name,
        type,
        entity_id,
        creator_id,
        list_usage,
        status,
        is_active,
        notes
    )
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/platform_list.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM platform.list WHERE notes='DEVOPS-2036 Insert list';
--rollback SET session_replication_role = origin;



--changeset postgres:insert_list_member context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert list member



SET session_replication_role = replica;

COPY (
    WITH t_list AS (
        SELECT
            (list_code_prefix || '_' || expt.experiment_code) AS list_code
        FROM (
                VALUES
                ('TRAIT_PROTOCOL'),
                ('MANAGEMENT_PROTOCOL')
            ) AS list (
                list_code_prefix
            )
            JOIN experiment.experiment AS expt
                ON expt.notes LIKE 'DEVOPS-2036%'
    )
    SELECT
        list.id AS list_id,
        var.id AS data_id,
        ROW_NUMBER() OVER (PARTITION BY list.id, listmem.list_type) AS order_number,
        var.label AS display_value,
        TRUE AS is_active,
        1 creator_id,
        'DEVOPS-2036 Insert list member' AS notes
    FROM (
            VALUES
            ('TRAIT_PROTOCOL', 'AYLD_CONT'),
            ('TRAIT_PROTOCOL', 'BB_SES5_GH_SCOR_1_9'),
            ('TRAIT_PROTOCOL', 'FLW_DATE_CONT'),
            ('TRAIT_PROTOCOL', 'HT1_CONT'),
            ('TRAIT_PROTOCOL', 'HT2_CONT'),
            ('TRAIT_PROTOCOL', 'HT3_CONT'),
            ('TRAIT_PROTOCOL', 'HT_CAT'),
            ('TRAIT_PROTOCOL', 'HVDATE_CONT'),
            ('TRAIT_PROTOCOL', 'LG_SCOR_1_9'),
            ('TRAIT_PROTOCOL', 'MC_CONT'),
            ('MANAGEMENT_PROTOCOL', 'DIST_BET_ROWS'),
            ('MANAGEMENT_PROTOCOL', 'FERT1_BRAND'),
            ('MANAGEMENT_PROTOCOL', 'FERT1_DATE_CONT'),
            ('MANAGEMENT_PROTOCOL', 'FERT1_METH_DISC'),
            ('MANAGEMENT_PROTOCOL', 'FERT1_TYPE_DISC'),
            ('MANAGEMENT_PROTOCOL', 'FERT2_BRAND'),
            ('MANAGEMENT_PROTOCOL', 'FERT2_DATE_CONT'),
            ('MANAGEMENT_PROTOCOL', 'FERT3_BRAND'),
            ('MANAGEMENT_PROTOCOL', 'FERT3_DATE_CONT'),
            ('MANAGEMENT_PROTOCOL', 'FERT3_METH_DISC'),
            ('MANAGEMENT_PROTOCOL', 'FERT3_TYPE_DISC'),
            ('MANAGEMENT_PROTOCOL', 'ROWS_PER_PLOT_CONT'),
            ('MANAGEMENT_PROTOCOL', 'SEEDING_DATE_CONT')
        ) AS listmem (
            list_type, variable
        )
        JOIN t_list AS t
            ON t.list_code ILIKE listmem.list_type || '%'
        JOIN platform.list AS list
            ON list.abbrev = t.list_code
        JOIN master.variable AS var
            ON var.abbrev = listmem.variable
    ORDER BY
        list.id,
        order_number
)
TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/platform_list_member.csv' DELIMITER ',' CSV HEADER;

COPY
    platform.list_member (
        list_id,
        data_id,
        order_number,
        display_value,
        is_active,
        creator_id,
        notes
    )
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/platform_list_member.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM platform.list_member WHERE notes='DEVOPS-2036 Insert list member';
--rollback SET session_replication_role = origin;



--changeset postgres:insert_transaction context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert transaction



SET session_replication_role = replica;

COPY (
    SELECT
		'committed' status,
        location_id,
		'[{"dataUnit": "plot", "dataUnitCount": "25", "variableDbIds": ["141", "561"], "occurrenceDbId": "'||lg.occurrence_id||'"}]' occurrence,
        1 creator_id,
        1 committer_id,
        'harvest manager' action,
        'DEVOPS-2036 Insert transaction'
	FROM
		experiment.location_occurrence_group  lg,
		generate_series(1,100)
)
TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/data_terminal_transaction.csv' DELIMITER ',' CSV HEADER;

COPY
    data_terminal.transaction (
        status,
        location_id,
        occurrence,
        creator_id,
        committer_id,
        action,
        notes
    )
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/data_terminal_transaction.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM platform.list_member WHERE notes='DEVOPS-2036 Insert list member';
--rollback SET session_replication_role = origin;



--changeset postgres:insert_transaction_dataset context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert transaction data_set



SET session_replication_role = replica;

COPY (
    SELECT 
        t.id transaction_id,
        v.id variable_id,
        var.value,
        'new' status,
        'plot' data_unit,
        'plot_data' entity,
        '18198304' entity_id,
        1 creator_id,
        'DEVOPS-2036 Insert transaction data_set' notes
    FROM 
        data_terminal."transaction" t,
        (
            VALUES 
                ('HVDATE_CONT','2018-01-30'),
                ('HV_METH_DISC','bulk')
        ) AS var(abbrev,value)
        JOIN
            master.variable v
        ON
            v.abbrev  = var.abbrev

)
TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/data_terminal_transaction_dataset.csv' DELIMITER ',' CSV HEADER;

COPY
    data_terminal.transaction_dataset (
        transaction_id,
        variable_id,
        value,
        status,
        data_unit,
        entity,
        entity_id,
        creator_id,
        notes
    )
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/data_terminal_transaction_dataset.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM platform.list_member WHERE notes='DEVOPS-2036 Insert list member';
--rollback SET session_replication_role = origin;



--changeset postgres:update_entries context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Update entries



WITH t1 AS (
    SELECT 
        s.id seed_id,
        seed_name seed_name,
        germplasm_id germplasm_id,
        p.id package_id
    FROM 
        germplasm.seed s
    JOIN
        germplasm.package p 
    ON
        p.seed_id = p.id
    WHERE 
        seed_name 
    LIKE 'CLG%' ORDER BY random()  LIMIT 30
), t2 AS (
    SELECT 
        seed_id,
        seed_name,
        germplasm_id,
        package_id,
        ROW_NUMBER() OVER() AS entry_number
    FROM 
        t1
    ORDER BY entry_number
), t3 AS (
    SELECT 
        t.*,
        e.id entry_id
    FROM t2 t
    JOIN
        experiment.entry e 
    ON
        e.entry_number::int = t.entry_number
    WHERE 
        e.notes LIKE 'DEVOPS-2036%'
)
UPDATE 
    experiment.entry e 
SET 
    entry_name = t3.seed_name,
    seed_id = t3.seed_id,
    germplasm_id = t3.germplasm_id,
    package_id  = t3.package_id
FROM t3 
    WHERE e.id = t3.entry_id
;



--rollback SELECT NULL;