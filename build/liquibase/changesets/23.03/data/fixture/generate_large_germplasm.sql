--liquibase formatted sql

--changeset postgres:insert_crop context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert crop



--# tenant.crop

--/* insert
INSERT INTO
    tenant.crop (
        crop_code,
        crop_name,
        description,
        creator_id
    )
SELECT
    crop.crop_code,
    crop.crop_name,
    crop.description,
    crtr.id AS creator_id
FROM (
        VALUES
        ('CROPL', 'Crop L', 'Test Crop L', 1)
    ) AS crop (
        crop_code,
        crop_name,
        description,
        creator
    )
    JOIN tenant.person AS crtr
        ON crtr.id = crop.creator
;
--*/



-- revert changes
--rollback DELETE FROM
--rollback     tenant.crop
--rollback WHERE
--rollback     crop_code = 'CROPL'
--rollback ;



--changeset postgres:insert_crop_program context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert crop program



--# tenant.crop_program

--/* insert
-- insert 1 record/s to table (eta: ~4ms)
INSERT INTO
    tenant.crop_program (
        crop_program_code,
        crop_program_name,
        organization_id,
        crop_id,
        creator_id
    )
SELECT
    t.crop_program_code,
    t.crop_program_name,
    org.id AS organization_id,
    crop.id AS crop_id,
    crtr.id AS creator_id
FROM (
        VALUES
        ('CROPPROGL', 'Crop Program L', 1, 'IRRI', 'CROPL')
    ) AS t (
        crop_program_code, crop_program_name, creator_id, organization_id, crop_id
    )
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
    JOIN tenant.organization AS org
        ON org.organization_code = t.organization_id
    JOIN tenant.crop AS crop
        ON crop.crop_code = t.crop_id
;
--*/



-- revert changes
--rollback -- delete 1 records (eta: ~27ms)
--rollback DELETE FROM
--rollback     tenant.crop_program AS cropprog
--rollback WHERE
--rollback     cropprog.crop_program_code = 'CROPPROGL'
--rollback ;



--changeset postgres:insert_program context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert program



--# tenant.crop_program

--/* insert
-- insert 1 record/s to table (eta: ~8ms)
INSERT INTO
    tenant.program (
        program_code,
        program_name,
        program_type,
        program_status,
        crop_program_id,
        creator_id
    )
SELECT
    t.program_code,
    t.program_name,
    t.program_type,
    t.program_status,
    cropprog.id AS crop_program_id,
    crtr.id AS creator_id
FROM (
        VALUES
        ('PROGL', 'Program L', 'breeding', 'active', 'CROPPROGL', 1)
    ) AS t (
        program_code,
        program_name,
        program_type,
        program_status,
        crop_program_id,
        creator_id
    )
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
    JOIN tenant.crop_program AS cropprog
        ON cropprog.crop_program_code = t.crop_program_id
;
--*/



-- revert changes
-- delete 1 record/s (eta: ~113ms)
--rollback DELETE FROM
--rollback     tenant.program AS prog
--rollback WHERE
--rollback     prog.program_code = 'PROGL'
--rollback ;



--changeset postgres:insert_taxonomy context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert taxonomy



--# germplasm.taxonomy

--/* insert
INSERT INTO
    germplasm.taxonomy (
        taxon_id,
        taxonomy_name,
        description,
        crop_id,
        creator_id
    )
SELECT
    t.taxon_id,
    t.taxonomy_name,
    t.description,
    crop.id AS crop_id,
    crtr.id AS creator_id
FROM (
        VALUES
        ('123', 'Taxonomy L', 'Test Taxonomy', 'CROPL', 1)
    ) AS t (
        taxon_id,
        taxonomy_name,
        description,
        crop_id,
        creator_id
    )
    JOIN tenant.crop AS crop
        ON crop.crop_code = t.crop_id
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
;
--*/



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.taxonomy
--rollback WHERE
--rollback     taxon_id = '123'
--rollback ;



--changeset postgres:insert_germplasm context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert germplasm



--# germplasm.germplasm

--/* insert
-- disable triggers and constraints to speed up data insertions
--ALTER TABLE germplasm.germplasm DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.germplasm'::regclass::oid AND indisready = TRUE;



COPY (
    SELECT
        (t.designation || '-' || gs.num) AS designation,
        t.parentage,
        (t.generation || floor(random() * (12-6 + 1) + 6)::int) AS generation,
        --t.germplasm_state,
        (SELECT CASE WHEN RANDOM() < 0.5 THEN 'fixed' ELSE 'not_fixed' END) AS germplasm_state,
        t.germplasm_name_type,
        crop.id AS crop_id,
        (t.germplasm_normalized_name || '-' || gs.num) AS germplasm_normalized_name,
        taxon.id AS taxonomy_id,
        crtr.id AS creator_id,
        --t.germplasm_type
        (SELECT CASE WHEN RANDOM() < 0.5 THEN 'fixed_line' ELSE 'progeny' END) AS germplasm_type
    FROM (
            VALUES
            ('CLGE', '?/?', 'F', 'line_name', 'CROPL', 'CLGE', '123', 1)
        ) AS t (
            designation,
            parentage,
            generation,
            --germplasm_state,
            germplasm_name_type,
            crop_id,
            germplasm_normalized_name,
            taxonomy_id,
            --germplasm_type,
            creator_id
        )
        JOIN tenant.crop AS crop
            ON crop.crop_code = t.crop_id
        JOIN germplasm.taxonomy AS taxon
            ON taxon.taxon_id = t.taxonomy_id
        JOIN tenant.person AS crtr
            ON crtr.id = t.creator_id,
        generate_series(1, 5000000) AS gs (num)
) TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_germplasm.csv' DELIMITER ',' CSV HEADER;
COPY germplasm.germplasm (
        designation,
        parentage,
        generation,
        germplasm_state,
        germplasm_name_type,
        crop_id,
        germplasm_normalized_name,
        taxonomy_id,
        creator_id,
        germplasm_type
    )
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_germplasm.csv' DELIMITER ',' CSV HEADER;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.germplasm'::regclass::oid AND indisready = FALSE;

-- REINDEX TABLE germplasm.germplasm;

-- restore triggers and constraints
--ALTER TABLE germplasm.germplasm ENABLE TRIGGER ALL;
SET session_replication_role = origin;
ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_germplasm_document_tgr;
ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;

--*/



-- revert changes
--rollback --ALTER TABLE germplasm.germplasm DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.germplasm'::regclass::oid AND indisprimary = FALSE AND indisready = TRUE;
--rollback 
--rollback -- delete 1,000,000 records (eta: ~2.106s)
--rollback DELETE FROM
--rollback     germplasm.germplasm AS ge
--rollback USING
--rollback     tenant.crop AS crop
--rollback WHERE
--rollback     crop.id = ge.crop_id
--rollback     AND crop.crop_code = 'CROPL'
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.germplasm'::regclass::oid AND indisprimary = FALSE AND indisready = FALSE;
--rollback 
--rollback REINDEX TABLE germplasm.germplasm;
--rollback 
--rollback --ALTER TABLE germplasm.germplasm ENABLE TRIGGER ALL;
--rollback ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_germplasm_document_tgr;
--rollback ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;
--rollback 



--changeset postgres:insert_germplasm_names context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert germplasm names



SET session_replication_role = replica;

COPY (
    SELECT
        ge.id AS germplasm_id,
        ge.designation || t.name_value AS name_value,
        t.germplasm_name_type,
        t.germplasm_name_status,
        ge.germplasm_normalized_name || t.germplasm_normalized_name AS germplasm_normalized_name,
        ge.creator_id
    FROM (
            VALUES
            ('', 'line_name', 'standard', ''),
            ('A', 'alternative_cultivar_name', 'active', ' A'),
            ('B', 'alternative_cultivar_name', 'active', ' B')
        ) AS t (
            name_value,
            germplasm_name_type,
            germplasm_name_status,
            germplasm_normalized_name
        )
        JOIN tenant.crop AS crop
            ON crop.crop_code = 'CROPL'
        JOIN germplasm.germplasm AS ge
            ON ge.crop_id = crop.id
            AND ge.germplasm_state = 'fixed'
    UNION ALL
        SELECT
            ge.id AS germplasm_id,
            ge.designation || t.name_value AS name_value,
            t.germplasm_name_type,
            t.germplasm_name_status,
            ge.germplasm_normalized_name || t.germplasm_normalized_name AS germplasm_normalized_name,
            ge.creator_id
        FROM (
                VALUES
                ('', 'derivative_name', 'standard', ''),
                ('A', 'alternative_derivative_name', 'active', ' A'),
                ('B', 'alternative_derivative_name', 'active', ' B')
            ) AS t (
                name_value,
                germplasm_name_type,
                germplasm_name_status,
                germplasm_normalized_name
            )
            JOIN tenant.crop AS crop
                ON crop.crop_code = 'CROPL'
            JOIN germplasm.germplasm AS ge
                ON ge.crop_id = crop.id
                AND ge.germplasm_state = 'not_fixed'
) TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_germplasm_name.csv' DELIMITER ',' CSV HEADER;

COPY
    germplasm.germplasm_name (
        germplasm_id,
        name_value,
        germplasm_name_type,
        germplasm_name_status,
        germplasm_normalized_name,
        creator_id
    )
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_germplasm_name.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;
ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;



-- revert changes
--rollback --ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.germplasm_name'::regclass::oid AND indisprimary = FALSE AND indisready = TRUE;
--rollback 
--rollback DELETE FROM
--rollback     germplasm.germplasm_name AS gename
--rollback USING
--rollback     germplasm.germplasm AS ge
--rollback     JOIN tenant.crop AS crop
--rollback         ON crop.id = ge.crop_id
--rollback WHERE
--rollback     ge.id = gename.germplasm_id
--rollback     AND crop.crop_code = 'CROPL'
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.germplasm_name'::regclass::oid AND indisprimary = FALSE AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~2.195s)
--rollback REINDEX TABLE germplasm.germplasm_name;
--rollback 
--rollback --ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER ALL;
--rollback ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
--rollback 



--changeset postgres:insert_germplasm_attribute context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert germplasm attribute



SET session_replication_role = replica;

COPY (
    SELECT
        ge.id AS germplasm_id,
        var.id AS variable_id,
        t.data_value AS data_value,
        t.data_qc_code AS data_qc_code,
        'DEVOPS-2036 Populate germplasm attribute' notes,
        1 creator_id
    FROM
        (
            VALUES 
                --SET 1
                ('CROSS_NUMBER', '0', 'G')
                -- ('GRAIN_COLOR', 'BG', 'G'),
                -- ('GROWTH_HABIT', 'S', 'G'),
                -- ('DH_FIRST_INCREASE_COMPLETED', 'FALSE', 'G'),
                -- ('HETEROTIC_GROUP', 'A', 'G')
        ) AS t(var_abbrev, data_value, data_qc_code)
     JOIN master.variable var
                ON var.abbrev = t.var_abbrev
     JOIN tenant.crop AS crop
                ON crop.crop_code = 'CROPL'
     JOIN germplasm.germplasm AS ge
                ON ge.crop_id = crop.id
) TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_germplasm_attribute.csv' DELIMITER ',' CSV HEADER;

COPY
    germplasm.germplasm_attribute
        (germplasm_id, variable_id, data_value, data_qc_code, notes, creator_id)
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_germplasm_attribute.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM germplasm.germplasm_attribute WHERE notes='DEVOPS-2036 Populate germplasm attribute';
--rollback SET session_replication_role = origin;



--changeset postgres:insert_seed context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert seed



SET session_replication_role = replica;

COPY (
    SELECT
        germplasm.generate_code('seed') AS seed_code,
        (ge.designation || '-' || gs.num) AS seed_name,
        (timestamp '2016-01-01' + random() * (timestamp '2020-12-31' - timestamp '2016-01-01'))::date AS harvest_date,
        ((array['Bulk', 'Single Plant Selection', 'Plant-specific'])[floor(random() * 3 + 1)]) AS harvest_method,
        ge.id AS germplasm_id,
        prog.id AS program_id,
        NULL::integer AS source_experiment_id,
        NULL::integer AS source_entry_id,
        NULL::integer AS source_occurrence_id,
        NULL::integer AS source_location_id,
        NULL::integer AS source_plot_id,
        NULL::integer AS cross_id,
        ((array['plot', 'cross'])[floor(random() * 2 + 1)]) AS harvest_source,
        ge.creator_id AS creator_id,
        'DEVOPS-2036 Populate seed records' AS notes
    FROM
        germplasm.germplasm AS ge
        JOIN tenant.crop AS crop
            ON crop.id = ge.crop_id
        JOIN tenant.program AS prog
            ON prog.program_code = 'PROGL',
        generate_series(1, 2) AS gs (num)
    WHERE
        crop.crop_code = 'CROPL'
        --AND ge.germplasm_state = 'not_fixed'
)  TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_seed.csv' DELIMITER ',' CSV HEADER;
COPY
    germplasm.seed
        (
            seed_code, seed_name, harvest_date,
            harvest_method, germplasm_id, program_id,
            source_experiment_id, source_entry_id, source_occurrence_id,
            source_location_id, source_plot_id, cross_id,
            harvest_source, creator_id, notes
        )
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_seed.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM germplasm.seed WHERE notes='DEVOPS-2036 Populate seed records';
--rollback SET session_replication_role = origin;



--changeset postgres:insert_package context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert package



SET session_replication_role = replica;

COPY (
    SELECT
        germplasm.generate_code('package') AS package_code,
        (sd.seed_name || t.package_label || gs.num) AS package_label,
        floor(random() * 300) AS package_quantity,
        t.package_unit,
        t.package_status,
        sd.id AS seed_id,
        prog.id AS program_id,
        NULL AS geospatial_object_id,
        NULL AS facility_id,
        crtr.id AS creator_id,
        'DEVOPS-2036 Populate package records' AS notes
    FROM (
            VALUES
            ('-PKG', 'g', 'active', 1)
        ) AS t (
            package_label,
            package_unit,
            package_status,
            creator_id
        )
        JOIN tenant.person AS crtr
            ON crtr.id = t.creator_id
        JOIN tenant.crop AS crop
            ON crop.crop_code = 'CROPL'
        JOIN tenant.program AS prog
            ON prog.program_code = 'PROGL'
        JOIN germplasm.germplasm AS ge
            ON ge.crop_id = crop.id
        JOIN germplasm.seed AS sd
            ON sd.germplasm_id = ge.id,
        generate_series(1, 1) AS gs (num)
) TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_package.csv' DELIMITER ',' CSV HEADER;

COPY germplasm.package (
        package_code,
        package_label,
        package_quantity,
        package_unit,
        package_status,
        seed_id,
        program_id,
        geospatial_object_id,
        facility_id,
        creator_id,
        notes
    )
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_package.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM germplasm.package WHERE notes='DEVOPS-2036 Populate package records';
--rollback SET session_replication_role = origin;




--changeset postgres:insert_package_data context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert package data



SET session_replication_role = replica;

COPY (
    SELECT
        p.id AS package_id,
        var.id AS variable_id,
        t.data_value AS data_value,
        t.data_qc_code AS data_qc_code,
        'DEVOPS-2036 Populate package data' notes,
        1 creator_id
    FROM
        (
            VALUES 
                --SET 1
                ('HVDATE_CONT','2016-04-12','G')
                -- ('MC_CONT','21','G'),
                -- ('SRC_SEASON','Dry','G'),
                -- ('SRC_PLOT','XXXX','G'),
                -- ('SRC_ENTRY','XXXX','G'),
                -- ('SOURCE_PLOT_NO','XXXX','G'),
                -- ('SOURCE_HARV_YEAR','2016','G'),
                -- ('BARCODE','GR-XXXX','G'),
                -- ('RSHT_NO','0','G'),
                -- ('CROP_YEAR','2016DS','G')
        ) AS t(var_abbrev, data_value, data_qc_code)
        JOIN master.variable var
                ON var.abbrev = t.var_abbrev
        JOIN tenant.crop AS crop
                ON crop.crop_code = 'CROPL'
        JOIN germplasm.germplasm AS ge
                ON ge.crop_id = crop.id
        JOIN germplasm.seed AS sd
                    ON sd.germplasm_id = ge.id
        JOIN germplasm.package p
                    ON p.seed_id = sd.id
)  TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_package_data.csv' DELIMITER ',' CSV HEADER;

COPY germplasm.package_data (
   package_id, 
   variable_id, 
   data_value, 
   data_qc_code, 
   notes, 
   creator_id
)  FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_package_data.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM germplasm.package_data WHERE notes='DEVOPS-2036 Populate package data';
--rollback SET session_replication_role = origin;



--changeset postgres:insert_package_log context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert package log



SET session_replication_role = replica;
    
COPY (
    SELECT
        pkg.id AS package_id,
        pkg.package_quantity,
        pkg.package_unit,
        t.package_transaction_type,
        enty.id AS entity_id,
        t.data_id::integer AS data_id,
        crtr.id AS creator_id,
        'DEVOPS-2036 Insert package log'
    FROM (
            VALUES
            ('deposit', 'PLOT', NULL, 1)
        ) AS t (
            package_transaction_type,
            entity_id,
            data_id,
            creator_id
        )
        JOIN tenant.person AS crtr
            ON crtr.id = t.creator_id
        JOIN dictionary.entity AS enty
            ON enty.abbrev = t.entity_id
        JOIN tenant.crop AS crop
            ON crop.crop_code = 'CROPL'
        JOIN germplasm.germplasm AS ge
            ON ge.crop_id = crop.id
        JOIN germplasm.seed AS sd
            ON sd.germplasm_id = ge.id
        LEFT JOIN germplasm.package AS pkg
            ON pkg.seed_id = sd.id
) TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_package_log.csv' DELIMITER ',' CSV HEADER;
COPY
    germplasm.package_log (
        package_id,
        package_quantity,
        package_unit,
        package_transaction_type,
        entity_id,
        data_id,
        creator_id,
        notes
    )
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_package_log.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM germplasm.package_log WHERE notes='DEVOPS-2036 Insert package log';
--rollback SET session_replication_role = origin;



--changeset postgres:insert_cross context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert cross



SET session_replication_role = replica;

COPY (
    SELECT
        c.cross_name,
        t.cross_method,
        NULL::integer AS germplasm_id,
        NULL::integer AS seed_id,
        NULL::integer AS experiment_id,
        NULL::integer AS entry_id,
        t.is_method_autofilled,
        t.harvest_status,
        crtr.id AS creator_id,
        'DEVOPS-2036 Insert cross' AS notes
    FROM (
            VALUES
            ('single cross', FALSE, 'COMPLETED', 1)
        ) AS t (
            cross_method,
            is_method_autofilled,
            harvest_status,
            creator_id
        )
        JOIN tenant.person AS crtr
            ON crtr.id = t.creator_id,
        (
            SELECT
                gef.cross_id,
                (gef.designation || '|' || gem.designation) AS cross_name
            FROM (
                    SELECT
                        row_number() OVER () AS cross_id,
                        ge.*
                    FROM
                        germplasm.germplasm AS ge
                        JOIN tenant.crop AS crop
                            ON crop.id = ge.crop_id
                    WHERE
                        crop.crop_code = 'CROPL'
                    ORDER BY
                        ge.id
                ) AS gef,
                (
                    SELECT
                        abs(row_number() OVER ()-5000001) AS cross_id,
                        ge.*
                    FROM
                        germplasm.germplasm AS ge
                        JOIN tenant.crop AS crop
                            ON crop.id = ge.crop_id
                    WHERE
                        crop.crop_code = 'CROPL'
                    ORDER BY
                        ge.id
                ) AS gem
            WHERE
                gef.cross_id = gem.cross_id
        ) AS c
) TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_cross.csv' DELIMITER ',' CSV HEADER;
COPY
    germplasm.cross (
        cross_name,
        cross_method,
        germplasm_id,
        seed_id,
        experiment_id,
        entry_id,
        is_method_autofilled,
        harvest_status,
        creator_id,
        notes
    )
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_cross.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM germplasm.cross WHERE notes='DEVOPS-2036 Insert cross';
--rollback SET session_replication_role = origin;



--changeset postgres:insert_cross_parent context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert cross parents



SET session_replication_role = replica;

COPY (    
    WITH cp AS (
        SELECT
            gef.cross_id,
            gef.germplasm_id AS female_germplasm_id,
            gem.germplasm_id AS male_germplasm_id,
            gef.seed_id AS female_seed_id,
            gem.seed_id AS male_seed_id,
            (gef.designation || '|' || gem.designation) AS cross_name
        FROM (
                SELECT
                    row_number() OVER () AS cross_id,
                    ge.id AS germplasm_id,
                    ge.designation,
                    (array_agg(sd.id))[1] AS seed_id
                FROM
                    germplasm.germplasm AS ge
                    JOIN tenant.crop AS crop
                        ON crop.id = ge.crop_id
                    JOIN germplasm.seed AS sd
                        ON sd.germplasm_id = ge.id
                WHERE
                    crop.crop_code = 'CROPL'
                    --AND ge.germplasm_state = 'fixed'
                GROUP BY
                    ge.id
                ORDER BY
                    ge.id
            ) AS gef,
            (
                SELECT
                    row_number() OVER () AS cross_id,
                    ge.id AS germplasm_id,
                    ge.designation,
                    (array_agg(sd.id))[1] AS seed_id
                FROM
                    germplasm.germplasm AS ge
                    JOIN tenant.crop AS crop
                        ON crop.id = ge.crop_id
                    JOIN germplasm.seed AS sd
                        ON sd.germplasm_id = ge.id
                WHERE
                    crop.crop_code = 'CROPL'
                    --AND ge.germplasm_state = 'fixed'
                GROUP BY
                    ge.id
                ORDER BY
                    ge.id desc
            ) AS gem
        WHERE
            gef.cross_id = gem.cross_id
    )
    SELECT
        crs.id AS cross_id,
        cp.female_germplasm_id AS germplasm_id,
        cp.female_seed_id AS seed_id,
        'female' AS parent_role,
        1 AS order_number,
        NULL::integer AS experiment_id,
        NULL::integer AS entry_id,
        NULL::integer AS plot_id,
        1 AS creator_id,
        'DEVOPS-2036 Insert cross parents'
    FROM
        cp
        JOIN germplasm.cross AS crs
            ON crs.cross_name = cp.cross_name
    UNION ALL
        SELECT
            crs.id AS cross_id,
            cp.male_germplasm_id AS germplasm_id,
            cp.male_seed_id AS seed_id,
            'male' AS parent_role,
            2 AS order_number,
            NULL::integer AS experiment_id,
            NULL::integer AS entry_id,
            NULL::integer AS plot_id,
            1 AS creator_id,
            'DEVOPS-2036 Insert cross parents'
        FROM
            cp
            JOIN germplasm.cross AS crs
                ON crs.cross_name = cp.cross_name
) TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_cross_parent.csv' DELIMITER ',' CSV HEADER;

COPY
    germplasm.cross_parent (
        cross_id,
        germplasm_id,
        seed_id,
        parent_role,
        order_number,
        experiment_id,
        entry_id,
        plot_id,
        creator_id,
        notes
    )
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_cross_parent.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM germplasm.cross_parent WHERE notes='DEVOPS-2036 Insert cross parents';
--rollback SET session_replication_role = origin;



--changeset postgres:insert_cross_data context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert cross data



SET session_replication_role = replica;

COPY (
    SELECT
        t.cross_id,
        t.variable_id,
        t.data_value::date::varchar AS data_value,
        t.data_qc_code,
        t.data_value + INTERVAL '10 days' AS collection_timestamp,
        t.creator_id,
        'DEVOPS-2036 Insert cross data' AS notes
    FROM (
            SELECT
                crs.id AS cross_id,
                var.id AS variable_id,
                (timestamp '2016-01-01' + random() * (timestamp '2020-12-31' - timestamp '2016-01-01')) AS data_value,
                t.data_qc_code,
                crtr.id AS creator_id
            FROM (
                    VALUES
                    --('DATE_CROSSED', 'Q', 1),
                    ('HVDATE_CONT', 'Q', 1)
                ) AS t (
                    variable_id,
                    data_qc_code,
                    creator_id
                )
                JOIN master.variable AS var
                    ON var.abbrev = t.variable_id
                JOIN tenant.person AS crtr
                    ON crtr.id = t.creator_id
                JOIN germplasm.cross AS crs
                    ON crs.cross_name LIKE 'CLGE%'
        ) AS t
    -- UNION ALL
    --     SELECT
    --         crs.id AS cross_id,
    --         var.id AS variable_id,
    --         ((array['Bulk', 'Single Plant Selection', 'Plant-specific'])[floor(random() * 3 + 1)]) AS data_value,
    --         t.data_qc_code,
    --         (timestamp '2016-01-01' + random() * (timestamp '2020-12-31' - timestamp '2016-01-01')) AS collection_timestamp,
    --         crtr.id AS creator_id,
    --         'DEVOPS-2036 Insert cross data' AS notes
    --     FROM (
    --             VALUES
    --             ('HV_METH_DISC', 'Q', 1)
    --         ) AS t (
    --             variable_id,
    --             data_qc_code,
    --             creator_id
    --         )
    --         JOIN master.variable AS var
    --             ON var.abbrev = t.variable_id
    --         JOIN tenant.person AS crtr
    --             ON crtr.id = t.creator_id
    --         JOIN germplasm.cross AS crs
    --             ON crs.cross_name LIKE 'CLGE%'
    -- UNION ALL
    --     SELECT
    --         crs.id AS cross_id,
    --         var.id AS variable_id,
    --         (floor(random() * 100 + 1))::varchar AS data_value,
    --         t.data_qc_code,
    --         (timestamp '2016-01-01' + random() * (timestamp '2020-12-31' - timestamp '2016-01-01')) AS collection_timestamp,
    --         crtr.id AS creator_id,
    --         'DEVOPS-2036 Insert cross data' AS notes
    --     FROM (
    --             VALUES
    --             ('NO_OF_PLANTS', 'Q', 1),
    --             ('NO_OF_SEED', 'Q', 1),
    --             ('NO_OF_EARS', 'Q', 1)
    --         ) AS t (
    --             variable_id,
    --             data_qc_code,
    --             creator_id
    --         )
    --         JOIN master.variable AS var
    --             ON var.abbrev = t.variable_id
    --         JOIN tenant.person AS crtr
    --             ON crtr.id = t.creator_id
    --         JOIN germplasm.cross AS crs
    --             ON crs.cross_name LIKE 'CLGE%'
    -- ;
)
TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_cross_data.csv' DELIMITER ',' CSV HEADER;

COPY
    germplasm.cross_data (
        cross_id,
        variable_id,
        data_value,
        data_qc_code,
        collection_timestamp,
        creator_id,
        notes
    )
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_cross_data.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM germplasm.cross_data WHERE notes='DEVOPS-2036 Insert cross data';
--rollback SET session_replication_role = origin;



--changeset postgres:insert_germplasm_relation context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert germplasm relation



SET session_replication_role = replica;

COPY (
    SELECT 
        par.germplasm_id parent_germplasm_id,
        chd.germplasm_id child_germplasm_id,
        ROW_NUMBER() OVER (PARTITION BY cp.cross_id, par.germplasm_id ORDER BY cp.cross_id) AS order_number,
        1 creator_id,
        'DEVOPS-2036 Insert germplasm relation' AS notes
    FROM 
        germplasm.cross_parent cp
    JOIN
        germplasm.cross_parent par
    ON
        par.cross_id = cp.cross_id AND par.order_number =1
    JOIN
        germplasm.cross_parent chd
    ON
        chd.cross_id = cp.cross_id AND chd.order_number =2
    ORDER BY 
        cp.cross_id,
        parent_germplasm_id,
        child_germplasm_id
)
TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_germplasm_relation.csv' DELIMITER ',' CSV HEADER;

COPY
    germplasm.germplasm_relation (
        parent_germplasm_id, child_germplasm_id, order_number, creator_id, notes
    )
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_germplasm_relation.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM germplasm.germplasm_relation WHERE notes='DEVOPS-2036 Insert germplasm relation';
--rollback SET session_replication_role = origin;



--changeset postgres:insert_family context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert family



SET session_replication_role = replica;


COPY (
    SELECT 
        CONCAT('CLGE-FAM-',ROW_NUMBER() OVER (ORDER BY id)) family_code,
        CONCAT('CLGE FAM ',ROW_NUMBER() OVER (ORDER BY id)) family_name,
        crs.id AS cross_id,
        1 creator_id,
        'DEVOPS-2036 Insert family' AS notes
    FROM 
        germplasm.CROSS crs
    WHERE 
        notes LIKE 'DEVOPS-2036%'
)
TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_family.csv' DELIMITER ',' CSV HEADER;

COPY
    germplasm.family
        (family_code, family_name, cross_id, creator_id, notes)
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_family.csv' DELIMITER ',' CSV HEADER;

SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM germplasm.family WHERE notes='DEVOPS-2036 Insert family';
--rollback SET session_replication_role = origin;



--changeset postgres:insert_family_member context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2036 Insert family member



SET session_replication_role = replica;

COPY (
    SELECT 
        f.id family_id,
        cp.germplasm_id,
        cp.order_number,
        1 creator_id,
        'DEVOPS-2036 Insert family member'
    FROM 
        germplasm.cross_parent cp
    JOIN
        germplasm."family" f 
    ON
        f.cross_id  = cp.cross_id 
    WHERE 
        cp.notes LIKE 'DEVOPS-2036%'
    ORDER BY cp.cross_id, cp.order_number
)
TO '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_family_member.csv' DELIMITER ',' CSV HEADER;

COPY
    germplasm.family_member
        (family_id, germplasm_id, order_number, creator_id, notes)
FROM '/Users/generomuga/Documents/ebs/cb-db/build/liquibase/changesets/23.03/temp/germplasm_family_member.csv' DELIMITER ',' CSV HEADER;


SET session_replication_role = origin;



--rollback SET session_replication_role = replica;
--rollback DELETE FROM germplasm.family_member WHERE notes='DEVOPS-2036 Insert family member';
--rollback SET session_replication_role = origin;