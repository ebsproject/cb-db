--liquibase formatted sql

--changeset postgres:clean_up_experiment.entry_data context:fixture splitStatements:false
--comment: EBS-1962 Clean up experiment.entry_data



UPDATE experiment.entry_data SET is_void=true WHERE variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=false;



--rollback UPDATE experiment.entry_data SET is_void=false WHERE variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=true;



--changeset postgres:clean_up_experiment.experiment_data context:fixture splitStatements:false
--comment: EBS-1962 Clean up experiment.experiment_data



UPDATE experiment.experiment_data SET is_void=true WHERE variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=false;



--rollback UPDATE experiment.experiment_data SET is_void=false WHERE variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=true;



--changeset postgres:clean_up_experiment.location_data context:fixture splitStatements:false
--comment: EBS-1962 Clean up experiment.location_data



UPDATE experiment.location_data SET is_void=true WHERE variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=false;



--rollback UPDATE experiment.location_data SET is_void=false WHERE variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=true;



--changeset postgres:clean_up_experiment.occurrence_data context:fixture splitStatements:false
--comment: EBS-1962 Clean up experiment.occurrence_data



UPDATE experiment.occurrence_data SET is_void=true WHERE variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=false;



--rollback UPDATE experiment.occurrence_data SET is_void=false WHERE variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=true;



--changeset postgres:clean_up_experiment.plant_data context:fixture splitStatements:false
--comment: EBS-1962 Clean up experiment.plant_data



UPDATE experiment.plant_data SET is_void=true WHERE variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=false;



--rollback UPDATE experiment.plant_data SET is_void=false WHERE variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=true;



--changeset postgres:clean_up_experiment.plot_data context:fixture splitStatements:false
--comment: EBS-1962 Clean up experiment.plot_data



UPDATE experiment.plot_data SET is_void=true WHERE variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=false;



--rollback UPDATE experiment.plot_data SET is_void=false WHERE variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=true;



--changeset postgres:clean_up_experiment.subplot_data context:fixture splitStatements:false
--comment: EBS-1962 Clean up experiment.subplot_data



UPDATE experiment.subplot_data SET is_void=true WHERE variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=false;



--rollback UPDATE experiment.subplot_data SET is_void=false WHERE variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=true;



--changeset postgres:clean_up_germplasm.package_data context:fixture splitStatements:false
--comment: EBS-1962 Clean up germplasm.package_data



UPDATE germplasm.package_data SET is_void=true WHERE variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=false;



--rollback UPDATE germplasm.package_data SET is_void=false WHERE variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=true;



--changeset postgres:clean_up_tenant.protocol_data context:fixture splitStatements:false
--comment: EBS-1962 Clean up tenant.protocol_data



UPDATE tenant.protocol_data SET is_void=true WHERE variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=false;



--rollback UPDATE tenant.protocol_data SET is_void=false WHERE variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=true;