--liquibase formatted sql

--changeset postgres:update_list_usage_in_platform.list context:fixture splitStatements:false
--comment: EBS-1864 Update list_usage in platform.list



UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0006';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0007';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0008';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0009';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0012';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0013';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0014';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0015';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0016';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0017';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0018';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0019';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0021';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0022';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0023';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0024';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0025';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0026';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0027';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0028';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0029';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0031';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0032';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0033';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0041';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0042';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0043';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0044';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0045';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0046';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0047';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0049';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0051';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0052';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0053';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0055';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0056';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0057';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0058';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0059';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0060';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0061';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0062';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0063';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0065';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0066';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0068';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0071';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0072';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0073';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0074';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0075';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0076';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0077';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0078';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0079';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0080';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0081';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0082';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0083';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0084';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0085';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0086';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0087';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0088';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0089';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0090';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0091';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0092';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0093';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0094';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0095';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0096';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0097';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0098';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0099';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0100';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0101';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0105';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0106';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0107';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0108';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0109';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0110';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0111';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0112';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0113';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0114';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0115';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0116';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0117';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0118';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0119';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0120';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0121';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0122';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0123';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0125';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0126';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0127';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0128';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0130';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0131';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0132';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0133';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0134';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0135';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0136';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0137';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0138';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0139';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0140';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0141';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0142';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0143';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0144';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0145';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0146';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0147';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0148';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0149';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0150';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0151';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0152';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0153';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0154';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0155';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0156';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0157';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0158';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0159';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0162';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0163';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0165';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0166';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0167';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0168';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0169';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0170';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0171';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0172';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0173';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0174';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0175';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0176';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0177';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0178';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0179';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0180';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0181';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0182';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0183';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0184';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0185';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0187';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0188';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0189';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0190';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0192';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0193';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0194';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0195';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0196';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0197';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0198';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0199';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0200';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0202';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0204';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0205';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0206';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0207';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0208';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0209';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0212';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0213';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0214';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0215';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0216';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0217';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0218';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0219';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0220';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0221';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0222';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0223';
UPDATE platform.list SET list_usage='working list' WHERE name='TRAIT LIST FOR EXPT0224';



--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0006';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0007';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0008';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0009';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0012';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0013';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0014';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0015';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0016';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0017';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0018';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0019';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0021';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0022';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0023';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0024';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0025';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0026';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0027';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0028';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0029';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0031';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0032';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0033';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0041';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0042';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0043';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0044';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0045';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0046';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0047';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0049';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0051';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0052';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0053';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0055';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0056';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0057';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0058';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0059';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0060';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0061';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0062';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0063';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0065';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0066';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0068';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0071';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0072';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0073';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0074';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0075';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0076';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0077';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0078';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0079';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0080';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0081';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0082';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0083';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0084';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0085';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0086';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0087';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0088';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0089';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0090';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0091';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0092';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0093';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0094';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0095';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0096';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0097';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0098';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0099';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0100';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0101';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0105';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0106';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0107';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0108';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0109';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0110';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0111';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0112';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0113';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0114';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0115';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0116';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0117';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0118';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0119';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0120';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0121';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0122';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0123';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0125';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0126';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0127';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0128';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0130';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0131';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0132';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0133';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0134';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0135';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0136';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0137';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0138';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0139';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0140';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0141';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0142';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0143';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0144';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0145';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0146';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0147';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0148';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0149';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0150';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0151';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0152';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0153';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0154';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0155';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0156';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0157';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0158';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0159';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0162';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0163';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0165';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0166';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0167';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0168';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0169';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0170';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0171';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0172';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0173';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0174';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0175';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0176';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0177';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0178';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0179';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0180';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0181';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0182';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0183';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0184';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0185';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0187';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0188';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0189';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0190';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0192';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0193';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0194';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0195';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0196';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0197';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0198';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0199';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0200';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0202';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0204';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0205';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0206';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0207';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0208';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0209';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0212';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0213';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0214';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0215';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0216';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0217';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0218';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0219';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0220';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0221';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0222';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0223';
--rollback UPDATE platform.list SET list_usage='final list' WHERE name='TRAIT LIST FOR EXPT0224';