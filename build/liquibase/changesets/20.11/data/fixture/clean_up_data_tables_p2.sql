--liquibase formatted sql

--changeset postgres:clean_up_master.formula context:fixture splitStatements:false
--comment: EBS-1962 Clean up master.formula



UPDATE master.formula SET is_void=true WHERE result_variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=false;



--rollback UPDATE master.formula SET is_void=false WHERE result_variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=true;



--changeset postgres:clean_up_master.formula_parameter context:fixture splitStatements:false
--comment: EBS-1962 Clean up master.formula_parameter



UPDATE master.formula_parameter SET is_void=true WHERE param_variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=false;



--rollback UPDATE master.formula_parameter SET is_void=false WHERE param_variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=true;



--changeset postgres:clean_up_master.variable context:fixture splitStatements:false
--comment: EBS-1962 Clean up master.variable



UPDATE master.variable SET is_void=true WHERE member_variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=false;



--rollback UPDATE master.variable SET is_void=false WHERE member_variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=true;



--changeset postgres:clean_up_master.record_variable context:fixture splitStatements:false
--comment: EBS-1962 Clean up master.record_variable



UPDATE master.record_variable SET is_void=true WHERE variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=false;



--rollback UPDATE master.record_variable SET is_void=false WHERE variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=true;



--changeset postgres:clean_up_master.variable_set_member context:fixture splitStatements:false
--comment: EBS-1962 Clean up master.variable_set_member



UPDATE master.variable_set_member SET is_void=true WHERE variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=false;



--rollback UPDATE master.variable_set_member SET is_void=false WHERE variable_id IN (SELECT id FROM master.variable WHERE is_void=true) AND is_void=true;