--liquibase formatted sql

--changeset postgres:update_germplasm_name_type_scale_values_to_master.scale_value_p2 context:fixture splitStatements:false
--comment: EBS-1886 Update germplasm_name_type scale values to master.scale_value p2



INSERT INTO
    master.scale_value
        (scale_id,value,order_number,description,display_name,creator_id,abbrev)
SELECT 
    t.* 
FROM
    (
    VALUES
        ((SELECT scale_id FROM master.variable WHERE abbrev='GERMPLASM_NAME_TYPE'),'CML_code',29,'CML_code','CML_code',1,'GERMPLASM_NAME_TYPE_CML_CODE'),
        ((SELECT scale_id FROM master.variable WHERE abbrev='GERMPLASM_NAME_TYPE'),'line_code',30,'line_code','line_code',1,'GERMPLASM_NAME_TYPE_LINE_CODE'),
        ((SELECT scale_id FROM master.variable WHERE abbrev='GERMPLASM_NAME_TYPE'),'Line_code',31,'Line_code','Line_code',1,'GERMPLASM_NAME_TYPE_LINE_CODE'),
        ((SELECT scale_id FROM master.variable WHERE abbrev='GERMPLASM_NAME_TYPE'),'line_code_1',32,'line_code_1','line_code_1',1,'GERMPLASM_NAME_TYPE_LINE_CODE_1'),
        ((SELECT scale_id FROM master.variable WHERE abbrev='GERMPLASM_NAME_TYPE'),'selection_history',33,'selection_history','selection_history',1,'GERMPLASM_NAME_TYPE_SELECTION_HISTORY'),
        ((SELECT scale_id FROM master.variable WHERE abbrev='GERMPLASM_NAME_TYPE'),'unknown',34,'unknown','unknown',1,'GERMPLASM_NAME_TYPE_UNKNOWN')
    ) t;



--rollback DELETE FROM master.scale_value WHERE abbrev IN ('GERMPLASM_NAME_TYPE_CML_CODE','GERMPLASM_NAME_TYPE_LINE_CODE','GERMPLASM_NAME_TYPE_LINE_CODE','GERMPLASM_NAME_TYPE_LINE_CODE_1','GERMPLASM_NAME_TYPE_SELECTION_HISTORY','GERMPLASM_NAME_TYPE_UNKNOWN');