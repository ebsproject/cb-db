--liquibase formatted sql

--changeset postgres:update_germplasm_name_type_in_germplasm.germplasm context:fixture splitStatements:false
--comment: EBS-1886 Update germplasm_name_type in germplasm.germplasm



UPDATE 
    germplasm.germplasm
SET
    germplasm_name_type='line_code'
WHERE
    germplasm_name_type='Line_code';



--rollback SELECT NULL;