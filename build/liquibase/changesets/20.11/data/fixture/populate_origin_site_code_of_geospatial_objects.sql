--liquibase formatted sql

--changeset postgres:populate_origin_site_code_of_geospatial_objects context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1926 Populate origin site codes of geospatial objects



-- create geospatial object attribute records for the sites
INSERT INTO
    place.geospatial_object_attribute (geospatial_object_id, variable_id, data_value, creator_id)
SELECT
    geo.id AS geospatial_object_id,
    var.id AS variable_id,
    t.origin_site_code AS data_value,
    1 AS creator_id
FROM
    place.geospatial_object AS geo
    INNER JOIN (
        VALUES
        ('Ciudad Obregon', 'CO', 'CO'),
        ('El Batan', 'EB', 'EB'),
        ('Toluca', 'TO', 'TO'),
        ('Kiboko', 'KI', 'KB')
    ) AS t (
        geospatial_object_name,
        geospatial_object_code,
        origin_site_code
    )
        ON geo.geospatial_object_name = t.geospatial_object_name
        AND geo.geospatial_object_code = t.geospatial_object_code
        AND geo.is_void = FALSE
    INNER JOIN master.variable AS var
        ON var.abbrev = 'ORIGIN_SITE_CODE'
        AND var.is_void = FALSE
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object_attribute AS geoattr
--rollback USING (
--rollback     SELECT
--rollback         geoattr.id AS geospatial_object_attribute_id
--rollback     FROM
--rollback         place.geospatial_object AS geo
--rollback         INNER JOIN (
--rollback             VALUES
--rollback             ('Ciudad Obregon', 'CO', 'CO'),
--rollback             ('El Batan', 'EB', 'EB'),
--rollback             ('Toluca', 'TO', 'TO'),
--rollback             ('Kiboko', 'KI', 'KB')
--rollback         ) AS t (
--rollback             geospatial_object_name,
--rollback             geospatial_object_code,
--rollback             origin_site_code
--rollback         )
--rollback             ON geo.geospatial_object_name = t.geospatial_object_name
--rollback             AND geo.geospatial_object_code = t.geospatial_object_code
--rollback             AND geo.is_void = FALSE
--rollback         INNER JOIN master.variable AS var
--rollback             ON var.abbrev = 'ORIGIN_SITE_CODE'
--rollback             AND var.is_void = FALSE
--rollback         INNER JOIN place.geospatial_object_attribute AS geoattr
--rollback             ON geoattr.geospatial_object_id = geo.id
--rollback             AND geoattr.variable_id = var.id
--rollback             AND geoattr.data_value = t.origin_site_code
--rollback             AND geoattr.is_void = FALSE
--rollback ) AS t
--rollback WHERE
--rollback     geoattr.id = t.geospatial_object_attribute_id
--rollback ;
