--liquibase formatted sql

--changeset postgres:activate_no_of_ears_variable context:fixture splitStatements:false
--comment: EBS-1838 Activate NO_OF_EARS variable



UPDATE 
    master.variable
SET
    is_void='false'
WHERE
    abbrev='NO_OF_EARS';



--rollback UPDATE 
--rollback     master.variable
--rollback SET
--rollback     is_void='true'
--rollback WHERE
--rollback     abbrev='NO_OF_EARS';