--liquibase formatted sql

--changeset postgres:update_germplasm_name_type_scale_values_to_master.scale_value context:fixture splitStatements:false
--comment: EBS-1886 Update germplasm_name_type scale values to master.scale_value



INSERT INTO
    master.scale_value
        (scale_id,value,order_number,description,display_name,creator_id,abbrev)
SELECT 
    t.* 
FROM
    (
    VALUES
        ((SELECT scale_id FROM master.variable WHERE abbrev='GERMPLASM_NAME_TYPE'),'pedigree',27,'pedigree','pedigree',1,'GERMPLASM_NAME_TYPE_PEDIGREE'),
        ((SELECT scale_id FROM master.variable WHERE abbrev='GERMPLASM_NAME_TYPE'),'bcid',28,'bcid','bcid',1,'GERMPLASM_NAME_TYPE_BCID')
    ) t;



--rollback DELETE FROM master.scale_value WHERE abbrev IN ('GERMPLASM_NAME_TYPE_PEDIGREE','GERMPLASM_NAME_TYPE_BCID');