--liquibase formatted sql

--changeset postgres:update_hvdate_cont_variable_label context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1918 Update HVDATE_CONT variable label



-- update label of HVDATE_CONT
UPDATE
    master.variable
SET
    label = 'HARVEST DATE'
WHERE
    abbrev = 'HVDATE_CONT'
;



--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     label = 'HVDATE'
--rollback WHERE 
--rollback     abbrev = 'HVDATE_CONT'
--rollback ;
