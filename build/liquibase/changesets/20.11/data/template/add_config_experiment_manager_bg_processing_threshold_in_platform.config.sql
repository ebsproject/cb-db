--liquibase formatted sql

--changeset postgres:add_config_experiment_manager_bg_processing_threshold_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1838? Add config EXPERIMENT_MANAGER_BG_PROCESSING_THRESHOLD in platform.config



INSERT INTO 
    platform.config 
        (abbrev, name, config_value, usage, creator_id, notes)
VALUES
    (
    'EXPERIMENT_MANAGER_BG_PROCESSING_THRESHOLD',
    'Background processing threshold values for Experiment Manager tool features',
    '
    {
        "generateLocation": {
            "size": "1000",
            "description": "Generate location for an Occurrence"
        },
        "uploadPlantingArrays": {
            "size": "1000",
            "description": "Upload planting arrays for an Occurrence"
        },
        "saveListMembers": {
            "size": "1000",
            "description": "Save list members"
        }
    }
    ',
    'application',
    1,
    'added by jp.ramos'
    );



--rollback DELETE FROM platform.config WHERE abbrev='EXPERIMENT_MANAGER_BG_PROCESSING_THRESHOLD';