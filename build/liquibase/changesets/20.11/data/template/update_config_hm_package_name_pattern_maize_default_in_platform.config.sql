--liquibase formatted sql

--changeset postgres:update_config_hm_package_name_pattern_maize_default_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1838 Update config HM_PACKAGE_NAME_PATTERN_MAIZE_DEFAULT in platform.config



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "pattern": [
            {
                "type": "seedField",
                "order_number": 0,
                "label":"Seed Name",
                "seedInfoField":"seedName"
            },
            {
                "type": "delimeter",
                "order_number": 1,
                "value": "-"     
            },
            {
                "type": "free-text",
                "order_number": 2,
                "value":"P"
                
            },
            {
                "type": "plotField",
                "order_number": 3,
                "label":"Plot No",
                "plotInfoField": "plotNumber"
            },
                {
                "type": "delimeter",
                "order_number": 4,
                "value": "-"     
            },
                {
                "type": "free-text",
                "order_number": 5,
                "value":"PK001"
                
            }
            ]
        }
    '
WHERE abbrev = 'HM_PACKAGE_NAME_PATTERN_MAIZE_DEFAULT';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "pattern": [
--rollback                 {
--rollback                     "type": "field",
--rollback                     "label": "Seed Name",
--rollback                     "order_number": 0,
--rollback                     "seedInfoField": "seedName"
--rollback                 },
--rollback                 {
--rollback                     "type": "delimeter",
--rollback                     "value": "-",
--rollback                     "order_number": 1
--rollback                 },
--rollback                 {
--rollback                     "type": "free-text",
--rollback                     "value": "PK001",
--rollback                     "order_number": 2
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE abbrev = 'HM_PACKAGE_NAME_PATTERN_MAIZE_DEFAULT';