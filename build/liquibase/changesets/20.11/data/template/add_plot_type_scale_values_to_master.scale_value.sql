--liquibase formatted sql

--changeset postgres:add_plot_type_scale_values_to_master.scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1674 Add plot type scale values to master.scale_value



INSERT INTO
    master.scale_value
        (scale_id,value,order_number,description,creator_id,abbrev)
SELECT
    t.*
FROM
    (
        VALUES 
            ((SELECT scale_id FROM master.variable WHERE abbrev='PLOT_TYPE'),'Stage I-II',46,'Stage I-II',1,'PLOT_TYPE_STAGE_I_II'),
            ((SELECT scale_id FROM master.variable WHERE abbrev='PLOT_TYPE'),'Stage III-IV',47,'Stage III-IV',1,'PLOT_TYPE_STAGE_III_IV'),
            ((SELECT scale_id FROM master.variable WHERE abbrev='PLOT_TYPE'),'MLN/FAW screening',48,'MLN/FAW screening',1,'PLOT_TYPE_MLN_FAW_SCREENING'),
            ((SELECT scale_id FROM master.variable WHERE abbrev='PLOT_TYPE'),'Foliar disease screening',49,'Foliar disease screening',1,'PLOT_TYPE_FOLIAR_DISEASE_SCREENING'),
            ((SELECT scale_id FROM master.variable WHERE abbrev='PLOT_TYPE'),'1C/6H_PGI_Riego',50,'1C/6H_PGI_Riego',1,'PLOT_TYPE_1C_6H_PGI_RIEGO'),
            ((SELECT scale_id FROM master.variable WHERE abbrev='PLOT_TYPE'),'1C/6H_PGII_Riego',51,'1C/6H_PGII_Riego',1,'PLOT_TYPE_1C_6H_PGII_RIEGO'),
            ((SELECT scale_id FROM master.variable WHERE abbrev='PLOT_TYPE'),'1C/6H_PC_Riego',52,'1C/6H_PC_Riego',1,'PLOT_TYPE_1C_6H_PC_RIEGO'),
            ((SELECT scale_id FROM master.variable WHERE abbrev='PLOT_TYPE'),'1C/6H_HP_Riego',53,'1C/6H_HP_Riego',1,'PLOT_TYPE_1C_6H_HP_RIEGO'),
            ((SELECT scale_id FROM master.variable WHERE abbrev='PLOT_TYPE'),'2C/2H_PG_Sequia',54,'2C/2H_PG_Sequia',1,'PLOT_TYPE_2C_2H_PG_SEQUIA'),
            ((SELECT scale_id FROM master.variable WHERE abbrev='PLOT_TYPE'),'2C/2H_PC_Sequia',55,'2C/2H_PC_Sequia',1,'PLOT_TYPE_2C_2H_PC_SEQUIA'),
            ((SELECT scale_id FROM master.variable WHERE abbrev='PLOT_TYPE'),'2C/6H_PG_Calor',56,'2C/6H_PG_Calor',1,'PLOT_TYPE_2C_6H_PG_CALOR'),
            ((SELECT scale_id FROM master.variable WHERE abbrev='PLOT_TYPE'),'2C/6H_PC_Calor',57,'2C/6H_PC_Calor',1,'PLOT_TYPE_2C_6H_PC_CALOR'),
            ((SELECT scale_id FROM master.variable WHERE abbrev='PLOT_TYPE'),'1Melga/8H',58,'1Melga/8H',1,'PLOT_TYPE_1MELGA_8H'),
            ((SELECT scale_id FROM master.variable WHERE abbrev='PLOT_TYPE'),'Melga_unknown',59,'Melga_unknown',1,'PLOT_TYPE_MELGA_UNKNOWN')
    ) t;



--rollback DELETE FROM master.scale_value WHERE abbrev IN ('PLOT_TYPE_STAGE_I_II','PLOT_TYPE_STAGE_III_IV','PLOT_TYPE_MLN_FAW_SCREENING','PLOT_TYPE_FOLIAR_DISEASE_SCREENING','PLOT_TYPE_1C_6H_PGI_RIEGO','PLOT_TYPE_1C_6H_PGII_RIEGO','PLOT_TYPE_1C_6H_PC_RIEGO','PLOT_TYPE_1C_6H_HP_RIEGO','PLOT_TYPE_2C_2H_PG_SEQUIA','PLOT_TYPE_2C_2H_PC_SEQUIA','PLOT_TYPE_2C_6H_PG_CALOR','PLOT_TYPE_2C_6H_PC_CALOR','PLOT_TYPE_1MELGA_8H','PLOT_TYPE_MELGA_UNKNOWN') 
--rollback AND scale_id = (SELECT scale_id FROM master.variable WHERE abbrev='PLOT_TYPE');