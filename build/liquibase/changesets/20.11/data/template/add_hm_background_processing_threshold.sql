--liquibase formatted sql

--changeset postgres:add_hm_background_processing_threshold context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1838 Add HM background processing threshold



-- add background processing threshold for Harvest Manager
INSERT INTO
    platform.config (
        abbrev, name, config_value, rank, usage
    )
    VALUES (
        'HARVEST_MANAGER_BG_PROCESSING_THRESHOLD',
        'Background processing threshold values for HARVEST MANAGER tool features', 
        '{
            "deletePlotData": {
                "size": "2000",
                "description": "Delete plot data of plots"
            },
            "deleteSeedlots": {
                "size": "500",
                "description": "Delete seedlots of plots"
            }
        }'::json, 1, 'harvest_manager_bg_process'
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     platform.config
--rollback WHERE
--rollback     abbrev IN (
--rollback         'HARVEST_MANAGER_BG_PROCESSING_THRESHOLD'
--rollback     )
--rollback ;
