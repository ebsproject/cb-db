--liquibase formatted sql

--changeset postgres:update_field_variable_label_and_display_name context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1838 Update field variable label and display_name



UPDATE 
    master.variable 
SET 
    (label,display_name,notes) = ('Planting Area', 'Planting Area', 'Update the label Field to Planting Area') 
WHERE 
    abbrev='FIELD';



--rollback UPDATE 
--rollback     master.variable 
--rollback SET 
--rollback     (label,display_name,notes) = ('Field', 'Field', NULL) 
--rollback WHERE 
--rollback     abbrev='FIELD';