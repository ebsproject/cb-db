--liquibase formatted sql

--changeset postgres:add_config_find_seeds_bg_processing_threshold_in_plaform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1838 Add config FIND_SEEDS_BG_PROCESSING_THRESHOLD in platform.config



INSERT INTO 
    platform.config
        (abbrev, name, config_value, rank, usage)
VALUES 
    ('FIND_SEEDS_BG_PROCESSING_THRESHOLD', 'Background processing threshold values for Find Seeds tool features', 
    '{
        "deleteWorkingList": {
            "size": "500",
            "description": "Delete entries of an Experiment"
        },
        "addWorkingList": {
            "size": "500",
            "description": "Update entries of an Experiment"
        }
  }'::json, 1, 'find_seeds_bg_process');



--rollback DELETE FROM platform.config WHERE abbrev='FIND_SEEDS_BG_PROCESSING_THRESHOLD';