--liquibase formatted sql

--changeset postgres:remove_name_type_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1918 Remove NAME_TYPE variable



DELETE FROM master.record_variable WHERE variable_id=(SELECT id FROM master.variable WHERE abbrev='NAME_TYPE');

DELETE FROM master.variable WHERE abbrev='NAME_TYPE';



--rollback INSERT INTO master.variable
--rollback     (id,abbrev,label,name,data_type,not_null,type,status,display_name,property_id,method_id,scale_id,creation_timestamp,creator_id,modification_timestamp,modifier_id,is_void,usage,is_column,is_computed)
--rollback VALUES
--rollback     (
--rollback         698,
--rollback         'NAME_TYPE',
--rollback         'NAME_TYPE',
--rollback         'Name type',
--rollback         'character varying',	
--rollback         'true',
--rollback         'metadata',	
--rollback         'active',	
--rollback         'Name Type',
--rollback         452,	
--rollback         680,
--rollback         677,	
--rollback         '2014-07-07 16:01:07.70078',
--rollback         1,	
--rollback         '2020-10-09 15:47:18.034306',	
--rollback         1,	
--rollback         'false',	
--rollback         'application',	
--rollback         'false',	
--rollback         'false'
--rollback     );

--rollback SELECT SETVAL('master.variable_id_seq', COALESCE(MAX(id), 1)) FROM master.variable;

--rollback INSERT INTO master.record_variable
--rollback     (id,record_id,variable_id,is_mandatory,order_number,creation_timestamp,creator_id,modifier_id,is_void)
--rollback VALUES 
--rollback     (
--rollback        85,	
--rollback        18,	
--rollback        (SELECT id FROM master.variable WHERE abbrev='NAME_TYPE'),	
--rollback        'true',	
--rollback        1,	
--rollback        '2014-11-12 01:38:45.574282',
--rollback        1,	
--rollback        1,	
--rollback        'false'
--rollback     );

--rollback SELECT SETVAL('master.record_variable_id_seq', COALESCE(MAX(id), 1)) FROM master.record_variable;