--liquibase formatted sql

--changeset postgres:add_new_tables_in_dictionary.table context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1849 Add new tables in dictionary.table



INSERT INTO
    dictionary.table 
        (database_id,schema_id,abbrev,name,creator_id)
SELECT
    t.* 
FROM
    (
        VALUES 
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='GERMPLASM'),'CROSS','cross',1),
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='GERMPLASM'),'CROSS_PARENT','cross_parent',1),
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='EXPERIMENT'),'ENTRY_LIST','entry_list',1),
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='EXPERIMENT'),'EXPERIMENT','experiment',1), 
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='EXPERIMENT'),'EXPERIMENT_BLOCK','experiment_block',1),
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='EXPERIMENT'),'EXPERIMENT_DATA','experiment_data',1), 
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='EXPERIMENT'),'EXPERIMENT_DESIGN','experiment_design',1), 
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='EXPERIMENT'),'EXPERIMENT_GROUP','experiment_group',1),
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='EXPERIMENT'),'EXPERIMENT_PROTOCOL','experiment_protocol',1), 
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='OPERATIONAL'),'FILE_CABINET','file_cabinet',1),
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='GERMPLASM'),'GERMPLASM_NAME','germplasm_name',1), 
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='PLATFORM'),'LIST','list',1),
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='PLATFORM'),'LIST_MEMBER','list_member',1),
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='PLATFORM'),'LIST_PERMISSION','list',1),
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='EXPERIMENT'),'LOCATION','location',1),
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='EXPERIMENT'),'LOCATION_DATA','location_data',1),
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='EXPERIMENT'),'LOCATION_OCCURRENCE_GROUP','location_occurrence_group',1), 
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='EXPERIMENT'),'OCCURRENCE_DATA','occurrence_data',1), 
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='EXPERIMENT'),'OCCURRENCE_LAYOUT','occurrence_layout',1),
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='TENANT'),'PERSON','person',1), 
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='PLATFORM'),'PLAN_TEMPLATE','plan_template',1), 
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='EXPERIMENT'),'PLANTING_INSTRUCTION','planting_instruction',1), 
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='TENANT'),'PROTOCOL','protocol',1), 
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='TENANT'),'SEASON','season',1),
            (1,(SELECT id FROM dictionary.schema WHERE abbrev='TENANT'),'TEAM_MEMBER','team_member',1)
    ) t;



--rollback DELETE FROM dictionary.table
--rollback WHERE abbrev IN ('CROSS','CROSS_PARENT','ENTRY_LIST','EXPERIMENT','EXPERIMENT_BLOCK','EXPERIMENT_DATA','EXPERIMENT_DESIGN','EXPERIMENT_GROUP','EXPERIMENT_PROTOCOL','FILE_CABINET','GERMPLASM_NAME','LIST','LIST_MEMBER','LIST_PERMISSION','LOCATION','LOCATION_DATA','LOCATION_OCCURRENCE_GROUP','OCCURRENCE_DATA','OCCURRENCE_LAYOUT','PERSON','PLAN_TEMPLATE','PLANTING_INSTRUCTION','PROTOCOL','SEASON','TEAM_MEMBER');



--changeset postgres:add_new_entities_in_dictionary.entity context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1849 Add new entities in dictionary.entity



INSERT INTO
    dictionary.entity 
        (abbrev,name,creator_id,table_id)
SELECT
    t.* 
FROM
    (
        VALUES 
            ('CROSS','cross',1,(SELECT id FROM dictionary.table WHERE abbrev='CROSS')),
            ('CROSS_PARENT','cross parent',1,(SELECT id FROM dictionary.table WHERE abbrev='CROSS_PARENT')),
            ('ENTRY_LIST','entry list',1,(SELECT id FROM dictionary.table WHERE abbrev='ENTRY_LIST')),
            ('EXPERIMENT','experiment',1,(SELECT id FROM dictionary.table WHERE abbrev='EXPERIMENT')), 
            ('EXPERIMENT_BLOCK','experiment block',1,(SELECT id FROM dictionary.table WHERE abbrev='EXPERIMENT_BLOCK')),
            ('EXPERIMENT_DATA','experiment data',1,(SELECT id FROM dictionary.table WHERE abbrev='EXPERIMENT_DATA')), 
            ('EXPERIMENT_DESIGN','experiment design',1,(SELECT id FROM dictionary.table WHERE abbrev='EXPERIMENT_DESIGN')), 
            ('EXPERIMENT_GROUP','experiment group',1,(SELECT id FROM dictionary.table WHERE abbrev='EXPERIMENT_GROUP')),
            ('EXPERIMENT_PROTOCOL','experiment protocol',1,(SELECT id FROM dictionary.table WHERE abbrev='EXPERIMENT_PROTOCOL')), 
            ('FILE_CABINET','file cabinet',1,(SELECT id FROM dictionary.table WHERE abbrev='FILE_CABINET')),
            ('GERMPLASM_NAME','germplasm name',1,(SELECT id FROM dictionary.table WHERE abbrev='GERMPLASM_NAME')), 
            ('LIST','list',1,(SELECT id FROM dictionary.table WHERE abbrev='LIST')),
            ('LIST_MEMBER','list member',1,(SELECT id FROM dictionary.table WHERE abbrev='LIST_MEMBER')),
            ('LIST_PERMISSION','list permission',1,(SELECT id FROM dictionary.table WHERE abbrev='LIST_PERMISSION')),
            ('LOCATION','location',1,(SELECT id FROM dictionary.table WHERE abbrev='LOCATION')),
            ('LOCATION_DATA','location data',1,(SELECT id FROM dictionary.table WHERE abbrev='LOCATION_DATA')),
            ('LOCATION_OCCURRENCE_GROUP','location occurrence group',1,(SELECT id FROM dictionary.table WHERE abbrev='LOCATION_OCCURRENCE_GROUP')), 
            ('OCCURRENCE_DATA','occurrence data',1,(SELECT id FROM dictionary.table WHERE abbrev='OCCURRENCE_DATA')), 
            ('OCCURRENCE_LAYOUT','occurrence layout',1,(SELECT id FROM dictionary.table WHERE abbrev='OCCURRENCE_LAYOUT')),
            ('PERSON','person',1,(SELECT id FROM dictionary.table WHERE abbrev='PERSON')), 
            ('PLAN_TEMPLATE','plan template',1,(SELECT id FROM dictionary.table WHERE abbrev='PLAN_TEMPLATE')), 
            ('PLANTING_INSTRUCTION','planting instruction',1,(SELECT id FROM dictionary.table WHERE abbrev='PLANTING_INSTRUCTION')), 
            ('PROTOCOL','protocol',1,(SELECT id FROM dictionary.table WHERE abbrev='PROTOCOL')), 
            ('SEASON','season',1,(SELECT id FROM dictionary.table WHERE abbrev='SEASON')),
            ('TEAM_MEMBER','team member',1,(SELECT id FROM dictionary.table WHERE abbrev='TEAM_MEMBER'))
    ) t;



--rollback DELETE FROM dictionary.entity
--rollback WHERE abbrev IN ('CROSS','CROSS_PARENT','ENTRY_LIST','EXPERIMENT','EXPERIMENT_BLOCK','EXPERIMENT_DATA','EXPERIMENT_DESIGN','EXPERIMENT_GROUP','EXPERIMENT_PROTOCOL','FILE_CABINET','GERMPLASM_NAME','LIST','LIST_MEMBER','LIST_PERMISSION','LOCATION','LOCATION_DATA','LOCATION_OCCURRENCE_GROUP','OCCURRENCE_DATA','OCCURRENCE_LAYOUT','PERSON','PLAN_TEMPLATE','PLANTING_INSTRUCTION','PROTOCOL','SEASON','TEAM_MEMBER');