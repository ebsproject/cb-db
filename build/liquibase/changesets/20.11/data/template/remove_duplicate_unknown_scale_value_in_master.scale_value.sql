--liquibase formatted sql

--changeset postgres:remove_duplicate_unknown_scale_value_in_master.scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1956 Remove duplication unknown scale value in master.scale_value



DELETE FROM 
    master.scale_value
WHERE
    value='Unknown'::varchar
AND
    scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='GENERATION');

UPDATE
    master.scale_value
SET
    order_number=206
WHERE
    scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='GENERATION')
AND
    value='VHY(F1)';

UPDATE
    master.scale_value
SET
    order_number=207
WHERE
    scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='GENERATION')
AND
    value='VHY(F1,M1)ISE';

UPDATE
    master.scale_value
SET
    order_number=208
WHERE
    scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='GENERATION')
AND
    value='VHY(F1,M1)NSI';



--rollback UPDATE
--rollback     master.scale_value
--rollback SET
--rollback     order_number=207
--rollback WHERE
--rollback     scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='GENERATION')
--rollback AND
--rollback     value='VHY(F1)';

--rollback UPDATE
--rollback     master.scale_value
--rollback SET
--rollback     order_number=208
--rollback WHERE
--rollback     scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='GENERATION')
--rollback AND
--rollback     value='VHY(F1,M1)ISE';

--rollback UPDATE
--rollback     master.scale_value
--rollback SET
--rollback     order_number=209
--rollback WHERE
--rollback     scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='GENERATION')
--rollback AND
--rollback     value='VHY(F1,M1)NSI';

--rollback INSERT INTO
--rollback     master.scale_value
--rollback         (scale_id,value,order_number,description,display_name,creator_id)
--rollback VALUES
--rollback     (
--rollback         (SELECT scale_id FROM master.variable WHERE abbrev='GENERATION'),
--rollback         'Unknown',
--rollback         206,
--rollback         'Unknown',
--rollback         'Unknown',
--rollback         1
--rollback     );