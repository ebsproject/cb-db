--liquibase formatted sql

--changeset postgres:update_occurrence_name_variable_label context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1918 Update OCCURRENCE_NAME variable label



-- update label of OCCURRENCE_NAME
UPDATE
    master.variable
SET
    label = 'OCCURRENCE'
WHERE
    abbrev = 'OCCURRENCE_NAME'
;



--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     label = 'Occurrence Name'
--rollback WHERE 
--rollback     abbrev = 'OCCURRENCE_NAME'
--rollback ;
