--liquibase formatted sql

--changeset postgres:add_config_experiment_creation_bg_processing_threshold_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1838 Add config EXPERIMENT_CREATION_BG_PROCESSING_THRESHOLD in platform.config



INSERT INTO 
    platform.config
        (abbrev, name, config_value, rank, usage, creator_id, notes)
VALUES 
    ('EXPERIMENT_CREATION_BG_PROCESSING_THRESHOLD', 'Background processing threshold values for Experiment Creation tool features', 
    '{
        "deleteEntries": {
            "size": "500",
            "description": "Delete entries of an Experiment"
        },
        "updateEntries": {
            "size": "500",
            "description": "Update entries of an Experiment"
        },
        "createEntries": {
            "size": "500",
            "description": "Create entries of an Experiment"
        }
  }'::json, 1, 'experiment_creation_bg_process', 1,'added by j.antonio');



--rollback DELETE FROM platform.config WHERE abbrev='EXPERIMENT_CREATION_BG_PROCESSING_THRESHOLD';