--liquibase formatted sql

--changeset postgres:update_config_experiment_creation_bg_processing_threshold_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1838 Update config EXPERIMENT_CREATION_BG_PROCESSING_THRESHOLD in platform.config



UPDATE 
    platform.config 
SET 
    config_value = 
    '{
        "createEntries": {
            "size": "500",
            "description": "Create entries of an Experiment"
        },
        "deleteEntries": {
            "size": "500",
            "description": "Delete entries of an Experiment"
        },
        "updateEntries": {
            "size": "500",
            "description": "Update entries of an Experiment"
        },
        "deleteOccurrences": {
            "size": "500",
            "description": "Delete occurrence plot and/or planting instruction records of an Experiment"
        },
        "createOccurrences": {
            "size": "500",
            "description": "Create occurrence plot and/or planting instruction records of an Experiment"
        }
    }' 
WHERE abbrev = 'EXPERIMENT_CREATION_BG_PROCESSING_THRESHOLD';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '  
--rollback         {
--rollback         "createEntries": {
--rollback             "size": "500",
--rollback             "description": "Create entries of an Experiment"
--rollback         },
--rollback         "deleteEntries": {
--rollback             "size": "500",
--rollback             "description": "Delete entries of an Experiment"
--rollback         },
--rollback         "updateEntries": {
--rollback             "size": "500",
--rollback             "description": "Update entries of an Experiment"
--rollback         }
--rollback         } 
--rollback     '
--rollback WHERE abbrev = 'EXPERIMENT_CREATION_BG_PROCESSING_THRESHOLD';