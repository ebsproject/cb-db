--liquibase formatted sql

--changeset postgres:remigrate_germplasm_name_type_scale_values_to_master.scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1886 Re-migrate germplasm_name_type scale values to master.scale_value



DELETE FROM master.scale_value WHERE abbrev IN ('GERMPLASM_NAME_TYPE_PEDIGREE','GERMPLASM_NAME_TYPE_BCID');

INSERT INTO
    master.scale_value
        (scale_id,value,order_number,description,display_name,creator_id,abbrev)
SELECT 
    t.* 
FROM
    (
    VALUES
        ((SELECT scale_id FROM master.variable WHERE abbrev='GERMPLASM_NAME_TYPE'),'pedigree',27,'pedigree','pedigree',1,'GERMPLASM_NAME_TYPE_PEDIGREE'),
        ((SELECT scale_id FROM master.variable WHERE abbrev='GERMPLASM_NAME_TYPE'),'bcid',28,'bcid','bcid',1,'GERMPLASM_NAME_TYPE_BCID')
    ) t;

DELETE FROM master.scale_value WHERE abbrev IN ('GERMPLASM_NAME_TYPE_CML_CODE','GERMPLASM_NAME_TYPE_LINE_CODE','GERMPLASM_NAME_TYPE_LINE_CODE','GERMPLASM_NAME_TYPE_LINE_CODE_1','GERMPLASM_NAME_TYPE_SELECTION_HISTORY','GERMPLASM_NAME_TYPE_UNKNOWN');

INSERT INTO
    master.scale_value
        (scale_id,value,order_number,description,display_name,creator_id,abbrev)
SELECT 
    t.* 
FROM
    (
    VALUES
        ((SELECT scale_id FROM master.variable WHERE abbrev='GERMPLASM_NAME_TYPE'),'CML_code',29,'CML_code','CML_code',1,'GERMPLASM_NAME_TYPE_CML_CODE'),
        ((SELECT scale_id FROM master.variable WHERE abbrev='GERMPLASM_NAME_TYPE'),'line_code',30,'line_code','line_code',1,'GERMPLASM_NAME_TYPE_LINE_CODE'),
        ((SELECT scale_id FROM master.variable WHERE abbrev='GERMPLASM_NAME_TYPE'),'line_code_1',31,'line_code_1','line_code_1',1,'GERMPLASM_NAME_TYPE_LINE_CODE_1'),
        ((SELECT scale_id FROM master.variable WHERE abbrev='GERMPLASM_NAME_TYPE'),'selection_history',32,'selection_history','selection_history',1,'GERMPLASM_NAME_TYPE_SELECTION_HISTORY'),
        ((SELECT scale_id FROM master.variable WHERE abbrev='GERMPLASM_NAME_TYPE'),'unknown',33,'unknown','unknown',1,'GERMPLASM_NAME_TYPE_UNKNOWN')
    ) t;



--rollback SELECT NULL;