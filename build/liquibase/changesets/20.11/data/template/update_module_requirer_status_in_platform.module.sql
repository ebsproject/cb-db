--liquibase formatted sql

--changeset postgres:update_module_requirer_status_in_platform.module context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1838 Update module requirer status in platform.module



UPDATE 
    platform.module 
SET 
    required_status = 'occurrences created' 
WHERE 
    abbrev 
IN 
    (
        'INTENTIONAL_CROSSING_NURSERY_PLACE_ACT_MOD',
        'CROSS_PARENT_NURSERY_PHASE_I_PLACE_ACT_MOD',
        'CROSS_PARENT_NURSERY_PHASE_II_PLACE_ACT_MOD'
    );



--rollback UPDATE 
--rollback     platform.module 
--rollback SET 
--rollback     required_status = 'oiccurrences created' 
--rollback WHERE 
--rollback     abbrev 
--rollback IN 
--rollback     (
--rollback         'INTENTIONAL_CROSSING_NURSERY_PLACE_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_PLACE_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_II_PLACE_ACT_MOD'
--rollback     );