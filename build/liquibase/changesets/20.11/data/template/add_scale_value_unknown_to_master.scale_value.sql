--liquibase formatted sql

--changeset postgres:add_scale_value_unknown_to_master.scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1954 Add scale value unknown to master.scale_value



INSERT INTO
    master.scale_value
        (scale_id,value,order_number,description,creator_id,abbrev)
SELECT
    t.*
FROM
    (
        VALUES 
            ((SELECT scale_id FROM master.variable WHERE abbrev='CROSS_METHOD'),'UNKNOWN',10,'Unknown',1,'CROSS_METHOD_UNKNOWN')
    ) t;



--rollback DELETE FROM master.scale_value WHERE abbrev IN ('CROSS_METHOD_UNKNOWN') AND scale_id = (SELECT scale_id FROM master.variable WHERE abbrev='CROSS_METHOD');