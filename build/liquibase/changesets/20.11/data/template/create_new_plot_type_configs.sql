--liquibase formatted sql

--changeset postgres:create_new_plot_type_configs context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1744 Create new plot type configs



-- Plot Type for Maize, flat planting 
INSERT INTO
    platform.config (
        abbrev, name, rank, usage, creator_id, config_value
    )
    VALUES
    ('PLOT_TYPE_STAGE_I_II','Experiment Protocol Plot types default values for Plot type Stage I-II',1,'experiment_creation',1,'{"Name": "Required experiment level protocol plot type variables for Plot type Stage I-II", "Values": [{"default": 1, "disabled": true, "required": "required", "field_label": "Rows per plot", "variable_abbrev": "ROWS_PER_PLOT_CONT", "field_description": "Number of rows per plot"}, {"unit": "m", "default": 0.75, "disabled": true, "required": "required", "field_label": "Dist. bet. rows", "variable_abbrev": "DIST_BET_ROWS", "field_description": "Distance between rows"}, {"unit": "m", "default": 5, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot width", "variable_abbrev": "PLOT_WIDTH", "field_description": "Plot width"}, {"unit": "m", "default": 0.75, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot length", "variable_abbrev": "PLOT_LN", "field_description": "Plot length"}, {"unit": "sqm", "default": 3.75, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot area", "variable_abbrev": "PLOT_AREA_2", "field_description": "Plot area"}, {"unit": "m", "default": 1, "disabled": true, "required": false, "field_label": "Alley length", "variable_abbrev": "ALLEY_LENGTH", "field_description": "Alley length"}, {"unit": "m", "default": false, "disabled": false, "required": false, "field_label": "Seeding Rate", "allow_new_val": true, "variable_abbrev": "SEEDING_RATE", "field_description": "Seeding Rate"}]}'),
    ('PLOT_TYPE_STAGE_III_IV','Experiment Protocol Plot types default values for Plot type Stage III-IV',1,'experiment_creation',1,'{"Name": "Required experiment level protocol plot type variables for Plot type Stage III-IV", "Values": [{"default": 2, "disabled": true, "required": "required", "field_label": "Rows per plot", "variable_abbrev": "ROWS_PER_PLOT_CONT", "field_description": "Number of rows per plot"}, {"unit": "m", "default": 0.75, "disabled": true, "required": "required", "field_label": "Dist. bet. rows", "variable_abbrev": "DIST_BET_ROWS", "field_description": "Distance between rows"}, {"unit": "m", "default": 5, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot width", "variable_abbrev": "PLOT_WIDTH", "field_description": "Plot width"}, {"unit": "m", "default": 1.5, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot length", "variable_abbrev": "PLOT_LN", "field_description": "Plot length"}, {"unit": "sqm", "default": 7.5, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot area", "variable_abbrev": "PLOT_AREA_2", "field_description": "Plot area"}, {"unit": "m", "default": 1, "disabled": true, "required": false, "field_label": "Alley length", "variable_abbrev": "ALLEY_LENGTH", "field_description": "Alley length"}, {"unit": "m", "default": false, "disabled": false, "required": false, "field_label": "Seeding Rate", "allow_new_val": true, "variable_abbrev": "SEEDING_RATE", "field_description": "Seeding Rate"}]}'),
    ('PLOT_TYPE_MLN_FAW_SCREENING','Experiment Protocol Plot types default values for Plot type MLN/FAW screening',1,'experiment_creation',1,'{"Name": "Required experiment level protocol plot type variables for Plot type MLN/FAW screening", "Values": [{"default": 1, "disabled": true, "required": "required", "field_label": "Rows per plot", "variable_abbrev": "ROWS_PER_PLOT_CONT", "field_description": "Number of rows per plot"}, {"unit": "m", "default": 0.75, "disabled": true, "required": "required", "field_label": "Dist. bet. rows", "variable_abbrev": "DIST_BET_ROWS", "field_description": "Distance between rows"}, {"unit": "m", "default": 3, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot width", "variable_abbrev": "PLOT_WIDTH", "field_description": "Plot width"}, {"unit": "m", "default": 0.75, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot length", "variable_abbrev": "PLOT_LN", "field_description": "Plot length"}, {"unit": "sqm", "default": 2.25, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot area", "variable_abbrev": "PLOT_AREA_2", "field_description": "Plot area"}, {"unit": "m", "default": 1, "disabled": true, "required": false, "field_label": "Alley length", "variable_abbrev": "ALLEY_LENGTH", "field_description": "Alley length"}, {"unit": "m", "default": false, "disabled": false, "required": false, "field_label": "Seeding Rate", "allow_new_val": true, "variable_abbrev": "SEEDING_RATE", "field_description": "Seeding Rate"}]}'),
    ('PLOT_TYPE_FOLIAR_DISEASE_SCREENING','Experiment Protocol Plot types default values for Plot type Foliar disease screening',1,'experiment_creation',1,'{"Name": "Required experiment level protocol plot type variables for Plot type Foliar disease screening", "Values": [{"default": 1, "disabled": true, "required": "required", "field_label": "Rows per plot", "variable_abbrev": "ROWS_PER_PLOT_CONT", "field_description": "Number of rows per plot"}, {"unit": "m", "default": 0.75, "disabled": true, "required": "required", "field_label": "Dist. bet. rows", "variable_abbrev": "DIST_BET_ROWS", "field_description": "Distance between rows"}, {"unit": "m", "default": 2.5, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot width", "variable_abbrev": "PLOT_WIDTH", "field_description": "Plot width"}, {"unit": "m", "default": 0.75, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot length", "variable_abbrev": "PLOT_LN", "field_description": "Plot length"}, {"unit": "sqm", "default": 1.875, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot area", "variable_abbrev": "PLOT_AREA_2", "field_description": "Plot area"}, {"unit": "m", "default": 1, "disabled": true, "required": false, "field_label": "Alley length", "variable_abbrev": "ALLEY_LENGTH", "field_description": "Alley length"}, {"unit": "m", "default": false, "disabled": false, "required": false, "field_label": "Seeding Rate", "allow_new_val": true, "variable_abbrev": "SEEDING_RATE", "field_description": "Seeding Rate"}]}')
;


-- Plot Type for Wheat, bed  planting
INSERT INTO
    platform.config (
        abbrev, name, rank, usage, creator_id, config_value
    )
    VALUES
    ('PLOT_TYPE_1C_6H_PGI_RIEGO','Experiment Protocol Plot types default values for Plot type 1C/6H_PGI_Riego',1,'experiment_creation',1,'{"Name": "Required experiment level protocol plot type variables for Plot type 1C/6H_PGI_Riego", "Values": [{"default": 1, "disabled": true, "required": "required", "field_label": "No of beds per plot", "variable_abbrev": "NO_OF_BEDS_PER_PLOT", "field_description": "Number of beds per plot"}, {"unit": "m", "default": 6, "disabled": true, "required": "required", "field_label": "No of rows per bed", "variable_abbrev": "NO_OF_ROWS_PER_BED", "field_description": "Number of rows per bed"}, {"unit": "m", "default": false, "computed": "computed", "disabled": false, "required": "required", "field_label": "Bed width", "variable_abbrev": "BED_WIDTH", "field_description": "Bed width"}, {"unit": "m", "default": 1.6, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot width", "variable_abbrev": "PLOT_WIDTH_WHEAT_BED", "field_description": "Plot width"}, {"unit": "m", "default": 5, "computed": "computed", "disabled": true, "required": "required", "field_label": "Bed length", "variable_abbrev": "PLOT_LN", "field_description": "Plot length"}, {"unit": "sqm", "default": 8, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot area", "variable_abbrev": "PLOT_AREA_4", "field_description": "Plot area"}, {"unit": "m", "default": false, "disabled": false, "required": false, "field_label": "Alley length", "variable_abbrev": "ALLEY_LENGTH", "field_description": "Alley length"}, {"unit": "m", "default": false, "disabled": false, "required": false, "field_label": "Seeding Rate", "allow_new_val": true, "variable_abbrev": "SEEDING_RATE", "field_description": "Seeding Rate"}]}'),
    ('PLOT_TYPE_1C_6H_PGII_RIEGO','Experiment Protocol Plot types default values for Plot type 1C/6H_PGII_Riego',1,'experiment_creation',1,'{"Name": "Required experiment level protocol plot type variables for Plot type 1C/6H_PGII_Riego", "Values": [{"default": 1, "disabled": true, "required": "required", "field_label": "No of beds per plot", "variable_abbrev": "NO_OF_BEDS_PER_PLOT", "field_description": "Number of beds per plot"}, {"unit": "m", "default": 6, "disabled": true, "required": "required", "field_label": "No of rows per bed", "variable_abbrev": "NO_OF_ROWS_PER_BED", "field_description": "Number of rows per bed"}, {"unit": "m", "default": false, "computed": "computed", "disabled": false, "required": "required", "field_label": "Bed width", "variable_abbrev": "BED_WIDTH", "field_description": "Bed width"}, {"unit": "m", "default": 1.6, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot width", "variable_abbrev": "PLOT_WIDTH_WHEAT_BED", "field_description": "Plot width"}, {"unit": "m", "default": 4, "computed": "computed", "disabled": true, "required": "required", "field_label": "Bed length", "variable_abbrev": "PLOT_LN", "field_description": "Plot length"}, {"unit": "sqm", "default": 6.4, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot area", "variable_abbrev": "PLOT_AREA_4", "field_description": "Plot area"}, {"unit": "m", "default": false, "disabled": false, "required": false, "field_label": "Alley length", "variable_abbrev": "ALLEY_LENGTH", "field_description": "Alley length"}, {"unit": "m", "default": false, "disabled": false, "required": false, "field_label": "Seeding Rate", "allow_new_val": true, "variable_abbrev": "SEEDING_RATE", "field_description": "Seeding Rate"}]}'),
    ('PLOT_TYPE_1C_6H_PC_RIEGO','Experiment Protocol Plot types default values for Plot type 1C/6H_PC_Riego',1,'experiment_creation',1,'{"Name": "Required experiment level protocol plot type variables for Plot type 1C/6H_PC_Riego", "Values": [{"default": 1, "disabled": true, "required": "required", "field_label": "No of beds per plot", "variable_abbrev": "NO_OF_BEDS_PER_PLOT", "field_description": "Number of beds per plot"}, {"unit": "m", "default": 6, "disabled": true, "required": "required", "field_label": "No of rows per bed", "variable_abbrev": "NO_OF_ROWS_PER_BED", "field_description": "Number of rows per bed"}, {"unit": "m", "default": false, "computed": "computed", "disabled": false, "required": "required", "field_label": "Bed width", "variable_abbrev": "BED_WIDTH", "field_description": "Bed width"}, {"unit": "m", "default": 1.6, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot width", "variable_abbrev": "PLOT_WIDTH_WHEAT_BED", "field_description": "Plot width"}, {"unit": "m", "default": 1.5, "computed": "computed", "disabled": true, "required": "required", "field_label": "Bed length", "variable_abbrev": "PLOT_LN", "field_description": "Plot length"}, {"unit": "sqm", "default": 2.4, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot area", "variable_abbrev": "PLOT_AREA_4", "field_description": "Plot area"}, {"unit": "m", "default": false, "disabled": false, "required": false, "field_label": "Alley length", "variable_abbrev": "ALLEY_LENGTH", "field_description": "Alley length"}, {"unit": "m", "default": false, "disabled": false, "required": false, "field_label": "Seeding Rate", "allow_new_val": true, "variable_abbrev": "SEEDING_RATE", "field_description": "Seeding Rate"}]}'),
    ('PLOT_TYPE_1C_2H_HP_RIEGO','Experiment Protocol Plot types default values for Plot type 1C/2H_HP_Riego',1,'experiment_creation',1,'{"Name": "Required experiment level protocol plot type variables for Plot type 1C/2H_HP_Riego", "Values": [{"default": 1, "disabled": true, "required": "required", "field_label": "No of beds per plot", "variable_abbrev": "NO_OF_BEDS_PER_PLOT", "field_description": "Number of beds per plot"}, {"unit": "m", "default": 2, "disabled": true, "required": "required", "field_label": "No of rows per bed", "variable_abbrev": "NO_OF_ROWS_PER_BED", "field_description": "Number of rows per bed"}, {"unit": "m", "default": false, "computed": "computed", "disabled": false, "required": "required", "field_label": "Bed width", "variable_abbrev": "BED_WIDTH", "field_description": "Bed width"}, {"unit": "m", "default": 0.8, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot width", "variable_abbrev": "PLOT_WIDTH_WHEAT_BED", "field_description": "Plot width"}, {"unit": "m", "default": 0.2, "computed": "computed", "disabled": true, "required": "required", "field_label": "Bed length", "variable_abbrev": "PLOT_LN", "field_description": "Plot length"}, {"unit": "sqm", "default": 0.16, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot area", "variable_abbrev": "PLOT_AREA_4", "field_description": "Plot area"}, {"unit": "m", "default": false, "disabled": false, "required": false, "field_label": "Alley length", "variable_abbrev": "ALLEY_LENGTH", "field_description": "Alley length"}, {"unit": "m", "default": false, "disabled": false, "required": false, "field_label": "Seeding Rate", "allow_new_val": true, "variable_abbrev": "SEEDING_RATE", "field_description": "Seeding Rate"}]}'),
    ('PLOT_TYPE_2C_2H_PG_SEQUIA','Experiment Protocol Plot types default values for Plot type 2C/2H_PG_Sequia',1,'experiment_creation',1,'{"Name": "Required experiment level protocol plot type variables for Plot type 2C/2H_PG_Sequia", "Values": [{"default": 2, "disabled": true, "required": "required", "field_label": "No of beds per plot", "variable_abbrev": "NO_OF_BEDS_PER_PLOT", "field_description": "Number of beds per plot"}, {"unit": "m", "default": 2, "disabled": true, "required": "required", "field_label": "No of rows per bed", "variable_abbrev": "NO_OF_ROWS_PER_BED", "field_description": "Number of rows per bed"}, {"unit": "m", "default": false, "computed": "computed", "disabled": false, "required": "required", "field_label": "Bed width", "variable_abbrev": "BED_WIDTH", "field_description": "Bed width"}, {"unit": "m", "default": 1.6, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot width", "variable_abbrev": "PLOT_WIDTH_WHEAT_BED", "field_description": "Plot width"}, {"unit": "m", "default": 3.5, "computed": "computed", "disabled": true, "required": "required", "field_label": "Bed length", "variable_abbrev": "PLOT_LN", "field_description": "Plot length"}, {"unit": "sqm", "default": 5.6, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot area", "variable_abbrev": "PLOT_AREA_4", "field_description": "Plot area"}, {"unit": "m", "default": false, "disabled": false, "required": false, "field_label": "Alley length", "variable_abbrev": "ALLEY_LENGTH", "field_description": "Alley length"}, {"unit": "m", "default": false, "disabled": false, "required": false, "field_label": "Seeding Rate", "allow_new_val": true, "variable_abbrev": "SEEDING_RATE", "field_description": "Seeding Rate"}]}'),
    ('PLOT_TYPE_2C_2H_PC_SEQUIA','Experiment Protocol Plot types default values for Plot type 2C/2H_PC_Sequia',1,'experiment_creation',1,'{"Name": "Required experiment level protocol plot type variables for Plot type 2C/2H_PC_Sequia", "Values": [{"default": 2, "disabled": true, "required": "required", "field_label": "No of beds per plot", "variable_abbrev": "NO_OF_BEDS_PER_PLOT", "field_description": "Number of beds per plot"}, {"unit": "m", "default": 2, "disabled": true, "required": "required", "field_label": "No of rows per bed", "variable_abbrev": "NO_OF_ROWS_PER_BED", "field_description": "Number of rows per bed"}, {"unit": "m", "default": false, "computed": "computed", "disabled": false, "required": "required", "field_label": "Bed width", "variable_abbrev": "BED_WIDTH", "field_description": "Bed width"}, {"unit": "m", "default": 1.6, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot width", "variable_abbrev": "PLOT_WIDTH_WHEAT_BED", "field_description": "Plot width"}, {"unit": "m", "default": 1.5, "computed": "computed", "disabled": true, "required": "required", "field_label": "Bed length", "variable_abbrev": "PLOT_LN", "field_description": "Plot length"}, {"unit": "sqm", "default": 2.4, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot area", "variable_abbrev": "PLOT_AREA_4", "field_description": "Plot area"}, {"unit": "m", "default": false, "disabled": false, "required": false, "field_label": "Alley length", "variable_abbrev": "ALLEY_LENGTH", "field_description": "Alley length"}, {"unit": "m", "default": false, "disabled": false, "required": false, "field_label": "Seeding Rate", "allow_new_val": true, "variable_abbrev": "SEEDING_RATE", "field_description": "Seeding Rate"}]}'),
    ('PLOT_TYPE_2C_6H_PG_CALOR','Experiment Protocol Plot types default values for Plot type 2C/6H_PG_Calor',1,'experiment_creation',1,'{"Name": "Required experiment level protocol plot type variables for Plot type 2C/6H_PG_Calor", "Values": [{"default": 2, "disabled": true, "required": "required", "field_label": "No of beds per plot", "variable_abbrev": "NO_OF_BEDS_PER_PLOT", "field_description": "Number of beds per plot"}, {"unit": "m", "default": 6, "disabled": true, "required": "required", "field_label": "No of rows per bed", "variable_abbrev": "NO_OF_ROWS_PER_BED", "field_description": "Number of rows per bed"}, {"unit": "m", "default": false, "computed": "computed", "disabled": false, "required": "required", "field_label": "Bed width", "variable_abbrev": "BED_WIDTH", "field_description": "Bed width"}, {"unit": "m", "default": 1.6, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot width", "variable_abbrev": "PLOT_WIDTH_WHEAT_BED", "field_description": "Plot width"}, {"unit": "m", "default": 3.5, "computed": "computed", "disabled": true, "required": "required", "field_label": "Bed length", "variable_abbrev": "PLOT_LN", "field_description": "Plot length"}, {"unit": "sqm", "default": 5.6, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot area", "variable_abbrev": "PLOT_AREA_4", "field_description": "Plot area"}, {"unit": "m", "default": false, "disabled": false, "required": false, "field_label": "Alley length", "variable_abbrev": "ALLEY_LENGTH", "field_description": "Alley length"}, {"unit": "m", "default": false, "disabled": false, "required": false, "field_label": "Seeding Rate", "allow_new_val": true, "variable_abbrev": "SEEDING_RATE", "field_description": "Seeding Rate"}]}'),
    ('PLOT_TYPE_1C_6H_PC_CALOR','Experiment Protocol Plot types default values for Plot type 1C/6H_PC_Calor',1,'experiment_creation',1,'{"Name": "Required experiment level protocol plot type variables for Plot type 1C/6H_PC_Calor", "Values": [{"default": 1, "disabled": true, "required": "required", "field_label": "No of beds per plot", "variable_abbrev": "NO_OF_BEDS_PER_PLOT", "field_description": "Number of beds per plot"}, {"unit": "m", "default": 6, "disabled": true, "required": "required", "field_label": "No of rows per bed", "variable_abbrev": "NO_OF_ROWS_PER_BED", "field_description": "Number of rows per bed"}, {"unit": "m", "default": false, "computed": "computed", "disabled": false, "required": "required", "field_label": "Bed width", "variable_abbrev": "BED_WIDTH", "field_description": "Bed width"}, {"unit": "m", "default": 1.6, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot width", "variable_abbrev": "PLOT_WIDTH_WHEAT_BED", "field_description": "Plot width"}, {"unit": "m", "default": 1.5, "computed": "computed", "disabled": true, "required": "required", "field_label": "Bed length", "variable_abbrev": "PLOT_LN", "field_description": "Plot length"}, {"unit": "sqm", "default": 2.4, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot area", "variable_abbrev": "PLOT_AREA_4", "field_description": "Plot area"}, {"unit": "m", "default": false, "disabled": false, "required": false, "field_label": "Alley length", "variable_abbrev": "ALLEY_LENGTH", "field_description": "Alley length"}, {"unit": "m", "default": false, "disabled": false, "required": false, "field_label": "Seeding Rate", "allow_new_val": true, "variable_abbrev": "SEEDING_RATE", "field_description": "Seeding Rate"}]}')
;

-- Melga planting for Wheat
INSERT INTO
    platform.config (
        abbrev, name, rank, usage, creator_id, config_value
    )
    VALUES
    ('PLOT_TYPE_1MELGA_8H','Experiment Protocol Plot types default values for Plot type 1Melga/8H',1,'experiment_creation',1,'{"Name": "Required experiment level protocol plot type variables for Plot type 1Melga/8H", "Values": [{"default": 1, "disabled": true, "required": "required", "field_label": "No of beds per plot", "variable_abbrev": "NO_OF_BEDS_PER_PLOT", "field_description": "Number of beds per plot"}, {"unit": "m", "default": 8, "disabled": true, "required": "required", "field_label": "No of rows per bed", "variable_abbrev": "NO_OF_ROWS_PER_BED", "field_description": "Number of rows per bed"}, {"unit": "m", "default": 1.6, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot width", "variable_abbrev": "PLOT_WIDTH", "field_description": "Plot width"}, {"unit": "m", "default": 5, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot length", "variable_abbrev": "PLOT_LN", "field_description": "Plot length"}, {"unit": "sqm", "default": 8, "computed": "computed", "disabled": true, "required": "required", "field_label": "Plot area", "variable_abbrev": "PLOT_AREA_4", "field_description": "Plot area"}, {"unit": "m", "default": false, "disabled": false, "required": false, "field_label": "Alley length", "variable_abbrev": "ALLEY_LENGTH", "field_description": "Alley length"}, {"unit": "m", "default": false, "disabled": false, "required": false, "field_label": "Seeding Rate", "allow_new_val": true, "variable_abbrev": "SEEDING_RATE", "field_description": "Seeding Rate"}]}'),
    ('PLOT_TYPE_MELGA_UNKNOWN','Experiment Protocol Plot types default values for Plot type Melga_unknown',1,'experiment_creation',1,'{"Name": "Required experiment level protocol plot type variables for Plot type Melga_unknown", "Values": [{"default": false, "disabled": false, "required": "required", "field_label": "No of beds per plot", "allow_new_val": true, "variable_abbrev": "NO_OF_BEDS_PER_PLOT", "field_description": "Number of beds per plot"}, {"unit": "m", "default": false, "disabled": false, "required": "required", "field_label": "No of rows per bed", "allow_new_val": true, "variable_abbrev": "NO_OF_ROWS_PER_BED", "field_description": "Number of rows per bed"}, {"unit": "m", "default": false, "computed": "computed", "disabled": false, "required": "required", "field_label": "Plot width", "allow_new_val": true, "variable_abbrev": "PLOT_WIDTH", "field_description": "Plot width"}, {"unit": "m", "default": false, "computed": "computed", "disabled": false, "required": "required", "field_label": "Plot length", "allow_new_val": true, "variable_abbrev": "PLOT_LN", "field_description": "Plot length"}, {"unit": "sqm", "default": false, "computed": "computed", "disabled": false, "required": "required", "field_label": "Plot area", "allow_new_val": false, "variable_abbrev": "PLOT_AREA_4", "field_description": "Plot area"}, {"unit": "m", "default": false, "disabled": false, "required": false, "field_label": "Alley length", "allow_new_val": true, "variable_abbrev": "ALLEY_LENGTH", "field_description": "Alley length"}, {"unit": "m", "default": false, "disabled": false, "required": false, "field_label": "Seeding Rate", "allow_new_val": true, "variable_abbrev": "SEEDING_RATE", "field_description": "Seeding Rate"}]}')
;


-- revert changes
--rollback DELETE FROM
--rollback     platform.config
--rollback WHERE
--rollback     abbrev IN (
--rollback         'PLOT_TYPE_STAGE_I_II',
--rollback         'PLOT_TYPE_STAGE_III_IV',
--rollback         'PLOT_TYPE_MLN_FAW_SCREENING',
--rollback         'PLOT_TYPE_FOLIAR_DISEASE_SCREENING',
--rollback         'PLOT_TYPE_1C_6H_PGI_RIEGO',
--rollback         'PLOT_TYPE_1C_6H_PGII_RIEGO',
--rollback         'PLOT_TYPE_1C_6H_PC_RIEGO',
--rollback         'PLOT_TYPE_1C_2H_HP_RIEGO',
--rollback         'PLOT_TYPE_2C_2H_PG_SEQUIA',
--rollback         'PLOT_TYPE_2C_2H_PC_SEQUIA',
--rollback         'PLOT_TYPE_2C_6H_PG_CALOR',
--rollback         'PLOT_TYPE_1C_6H_PC_CALOR',
--rollback         'PLOT_TYPE_1MELGA_8H',
--rollback         'PLOT_TYPE_MELGA_UNKNOWN'
--rollback     )
--rollback ;



--changeset postgres:delete_scale_values_from_plot_type context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1744 Delete scale values from PLOT_TYPE



-- delete 2 scale values from PLOT_TYPE
DELETE FROM
    master.scale_value
WHERE
    abbrev IN (
        'PLOT_TYPE_1C_6H_HP_RIEGO',
        'PLOT_TYPE_2C_6H_PC_CALOR'
    )
    AND scale_id = (
        SELECT
            scale_id
        FROM
            master.variable
        WHERE
            abbrev = 'PLOT_TYPE'
    )
;


-- update ordering
UPDATE
    master.scale_value AS sv
SET
    order_number = t.new_order_number
FROM (
        VALUES
        ('PLOT_TYPE_2C_2H_PG_SEQUIA', 53),
        ('PLOT_TYPE_2C_2H_PC_SEQUIA', 54),
        ('PLOT_TYPE_2C_6H_PG_CALOR', 55),
        ('PLOT_TYPE_1MELGA_8H', 56),
        ('PLOT_TYPE_MELGA_UNKNOWN', 57)
    ) AS t (
        abbrev, new_order_number
    )
WHERE
    sv.abbrev = t.abbrev
    AND sv.scale_id = (
        SELECT
            scale_id
        FROM
            master.variable
        WHERE
            abbrev = 'PLOT_TYPE'
    )
;



-- revert changes
--rollback INSERT INTO 
--rollback     master.scale_value (scale_id, value, order_number, description, display_name, abbrev)
--rollback VALUES 
--rollback      (
--rollback          (SELECT scale_id FROM master.variable WHERE abbrev='PLOT_TYPE'),
--rollback          '1C/6H_HP_Riego',
--rollback          53,
--rollback          '1C/6H_HP_Riego',
--rollback          NULL,
--rollback          'PLOT_TYPE_1C_6H_HP_RIEGO'
--rollback      ),
--rollback      (
--rollback          (SELECT scale_id FROM master.variable WHERE abbrev='PLOT_TYPE'),
--rollback          '2C/6H_PC_Calor',
--rollback          57,
--rollback          '2C/6H_PC_Calor',
--rollback          NULL,
--rollback          'PLOT_TYPE_2C_6H_PC_CALOR'
--rollback      )
--rollback ;
--rollback 
--rollback UPDATE
--rollback     master.scale_value AS sv
--rollback SET
--rollback     order_number = t.new_order_number
--rollback FROM (
--rollback         VALUES
--rollback         ('PLOT_TYPE_2C_2H_PG_SEQUIA', 54),
--rollback         ('PLOT_TYPE_2C_2H_PC_SEQUIA', 55),
--rollback         ('PLOT_TYPE_2C_6H_PG_CALOR', 56),
--rollback         ('PLOT_TYPE_1MELGA_8H', 58),
--rollback         ('PLOT_TYPE_MELGA_UNKNOWN', 59)
--rollback     ) AS t (
--rollback         abbrev, new_order_number
--rollback     )
--rollback WHERE
--rollback     sv.abbrev = t.abbrev
--rollback     AND sv.scale_id = (
--rollback         SELECT
--rollback             scale_id
--rollback         FROM
--rollback             master.variable
--rollback         WHERE
--rollback             abbrev = 'PLOT_TYPE'
--rollback     )
--rollback ;
--rollback 