--liquibase formatted sql

--changeset postgres:update_config_default_in_platform.space context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1838 Update config DEFAULT in platform.space



UPDATE 
    platform.space 
SET 
    menu_data = '
        {
            "left_menu_items": [{
                "name": "experiment-creation",
                "label": "Experiment creation",
                "appAbbrev": "EXPERIMENT_CREATION"
            }, {
                "name": "experiment-manager",
                "label": "Experiment manager",
                "appAbbrev": "OCCURRENCES"
            }, {
                "name": "data-collection-qc-quality-control",
                "label": "Data collection",
                "appAbbrev": "QUALITY_CONTROL"
            }, {
                "name": "seeds-harvest-manager",
                "label": "Harvest manager",
                "appAbbrev": "HARVEST_MANAGER"
            }, {
                "name": "search",
                "items": [{
                    "name": "searchs-germplasm",
                    "label": "Germplasm",
                    "appAbbrev": "GERMPLASM_CATALOG"
                }, {
                    "name": "search-seeds",
                    "label": "Seeds",
                    "appAbbrev": "FIND_SEEDS"
                }, {
                    "name": "search-traits",
                    "label": "Traits",
                    "appAbbrev": "TRAITS"
                }],
                "label": "Search"
            }],
            "main_menu_items": [{
                "icon": "help_outline",
                "name": "help",
                "items": [{
                    "icon": "feedback",
                    "name": "help_feedback",
                    "label": "Send feedback",
                    "tooltip": "Send feedback"
                }, {
                    "url": "https://riceinfo.atlassian.net/servicedesk/customer",
                    "icon": "headset_mic",
                    "name": "help_support-portal",
                    "label": "Support portal",
                    "tooltip": "Go to B4R Support portal"
                }, {
                    "url": "https://riceinfo.atlassian.net/wiki/spaces/ABOUT/pages/326172737/Breeding4Results+B4R",
                    "icon": "info_outline",
                    "name": "help_wiki",
                    "label": "Wiki",
                    "tooltip": "Go to B4R Wiki site"
                }, {
                    "url": "https://uat-b4rapi.b4rdev.org",
                    "icon": "code",
                    "name": "help_api",
                    "label": "API",
                    "tooltip": "Go to B4R API Documentation"
                }],
                "label": "Help"
            }]
        }
    ' 
WHERE abbrev = 'DEFAULT';



--rollback UPDATE 
--rollback     platform.space 
--rollback SET 
--rollback     menu_data =
--rollback     '   
--rollback         {
--rollback             "left_menu_items": [
--rollback                 {
--rollback                     "name": "experiment-creation",
--rollback                     "label": "Experiment creation",
--rollback                     "appAbbrev": "EXPERIMENT_CREATION"
--rollback                 },
--rollback                 {
--rollback                     "name": "experiment-manager",
--rollback                     "label": "Experiment manager",
--rollback                     "appAbbrev": "OCCURRENCES"
--rollback                 },
--rollback                 {
--rollback                     "name": "data-collection-qc-quality-control",
--rollback                     "label": "Data collection",
--rollback                     "appAbbrev": "QUALITY_CONTROL"
--rollback                 },
--rollback                 {
--rollback                     "name": "seeds-harvest-manager",
--rollback                     "label": "Harvest manager",
--rollback                     "appAbbrev": "HARVEST_MANAGER"
--rollback                 },
--rollback                 {
--rollback                     "name": "search",
--rollback                     "items": [
--rollback                         {
--rollback                             "name": "searchs-germplasm",
--rollback                             "label": "Germplasm",
--rollback                             "appAbbrev": "GERMPLASM_CATALOG"
--rollback                         },
--rollback                         {
--rollback                             "name": "search-seeds",
--rollback                             "label": "Seeds",
--rollback                             "appAbbrev": "FIND_SEEDS"
--rollback                         },
--rollback                         {
--rollback                             "name": "search-traits",
--rollback                             "label": "Traits",
--rollback                             "appAbbrev": "TRAITS"
--rollback                         }
--rollback                     ],
--rollback                     "label": "Search"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE abbrev = 'DEFAULT';