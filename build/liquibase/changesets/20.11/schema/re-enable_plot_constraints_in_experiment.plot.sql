--liquibase formatted sql

--changeset postgres:re-enable_plot_constraints_in_experiment.plot context:schema splitStatements:false
--comment: EBS-1704 Re-enable the plot_location_id_pa_x_pa_y_unq and plot_location_id_field_x_field_y_unq constraints in experiment.plot



ALTER TABLE experiment.plot
    ADD CONSTRAINT plot_location_id_field_x_field_y_unq UNIQUE (location_id, field_x, field_y);

COMMENT ON CONSTRAINT plot_location_id_field_x_field_y_unq ON experiment.plot
    IS 'A plot can be retrieved by its unique location, and field x and field y coordinates.';

ALTER TABLE experiment.plot
    ADD CONSTRAINT plot_location_id_pa_x_pa_y_unq UNIQUE (location_id, pa_x, pa_y);
 
COMMENT ON CONSTRAINT plot_location_id_pa_x_pa_y_unq ON experiment.plot
    IS 'A plot can be retrieved by its unique location, and planting area x and planting area y coordinates.';



--rollback ALTER TABLE experiment.plot DROP CONSTRAINT plot_location_id_field_x_field_y_unq;
--rollback ALTER TABLE experiment.plot DROP CONSTRAINT plot_location_id_pa_x_pa_y_unq;