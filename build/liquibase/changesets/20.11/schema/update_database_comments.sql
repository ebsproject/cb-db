--liquibase formatted sql

--changeset postgres:update_schema_comments context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1861 Update schema comments



-- update schema comments
COMMENT ON SCHEMA data_terminal IS 'Staging area for incoming data collected from various sources';
COMMENT ON SCHEMA experiment IS 'Experiments, occurrences, and locations';
COMMENT ON SCHEMA germplasm IS 'Germplasm, seeds, and packages';
COMMENT ON SCHEMA place IS 'Geospatial objects and facilities';
COMMENT ON SCHEMA platform IS 'Platform and other system-wide objects';
COMMENT ON SCHEMA seed_warehouse IS 'Samples and plates';
COMMENT ON SCHEMA temporary_data IS 'Temporary storage of incoming or data under processing';
COMMENT ON SCHEMA tenant IS 'Tenant and other master data';



-- revert changes
--rollback COMMENT ON SCHEMA data_terminal IS '';
--rollback COMMENT ON SCHEMA experiment IS '';
--rollback COMMENT ON SCHEMA germplasm IS '';
--rollback COMMENT ON SCHEMA place IS '';
--rollback COMMENT ON SCHEMA platform IS '';
--rollback COMMENT ON SCHEMA seed_warehouse IS '';
--rollback COMMENT ON SCHEMA temporary_data IS '';
--rollback COMMENT ON SCHEMA tenant IS '';



--changeset postgres:update_table_comments context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1861 Update table comments



-- update table comments
COMMENT ON TABLE api.authorization IS 'Authorization used for API calls';
COMMENT ON TABLE api.background_job IS 'Background jobs used in API calls';
COMMENT ON TABLE api.messages IS 'Registry of messages returned by API calls';
COMMENT ON TABLE api.oauth_access_tokens IS 'Access tokens used in API calls';
COMMENT ON TABLE data_terminal.transaction IS 'Transaction generated for staging incoming data';
COMMENT ON TABLE dictionary.entity IS 'Dictionary of entities or concepts';
COMMENT ON TABLE master.formula IS 'Computed formulas associated with variables';
COMMENT ON TABLE master.formula_parameter IS 'Parameters needed for the computation of formulas';
COMMENT ON TABLE master.item_service_team IS 'List of items associated with a service';
COMMENT ON TABLE master.notification IS 'Notifications';
COMMENT ON TABLE master.service_relation IS 'Relationship of one service with another';
COMMENT ON TABLE master.timing_rule IS 'Timing rules for task data collection';
COMMENT ON TABLE master.user_filter IS 'List of filters defined by users';
COMMENT ON TABLE master.variable IS 'A variable is a data element, which has precise meaning throughout the data model. Each variable has a set of mandatory attributes, which are inherited by the children through the data model.';
COMMENT ON TABLE master.variable_mapping IS 'Mapping of variable with another variable or value';
COMMENT ON TABLE operational.file_cabinet IS 'Collection of attached or uploaded files';
COMMENT ON TABLE operational.platform IS 'Platform for genotyping services';
COMMENT ON TABLE operational.study_task_definition IS 'Table for storing the study task definition such as the tasks list, services availed by the study, and timing rules';
COMMENT ON TABLE operational.vendor IS 'Vendors for genotyping services';
COMMENT ON TABLE platform.data_export_transaction IS 'Transactions for data exports';
COMMENT ON TABLE platform.plan_template IS 'Planning templates for experiment creation';
COMMENT ON TABLE public.databasechangelog IS 'Registry of database changes';
COMMENT ON TABLE public.databasechangeloglock IS 'Active execution of database changes';
COMMENT ON TABLE seed_warehouse.plate IS 'Plates used in genotyping services';
COMMENT ON TABLE seed_warehouse.plate_layout IS 'Layout of plates used in genotyping services';
COMMENT ON TABLE seed_warehouse.temp_sample IS 'Temporary placeholder of samples in genotyping services';



-- revert changes
--rollback COMMENT ON TABLE api.authorization IS '';
--rollback COMMENT ON TABLE api.background_job IS '';
--rollback COMMENT ON TABLE api.messages IS '';
--rollback COMMENT ON TABLE api.oauth_access_tokens IS '';
--rollback COMMENT ON TABLE data_terminal.transaction IS '';
--rollback COMMENT ON TABLE dictionary.entity IS '';
--rollback COMMENT ON TABLE master.formula IS '';
--rollback COMMENT ON TABLE master.formula_parameter IS '';
--rollback COMMENT ON TABLE master.item_service_team IS '';
--rollback COMMENT ON TABLE master.notification IS '';
--rollback COMMENT ON TABLE master.service_relation IS '';
--rollback COMMENT ON TABLE master.timing_rule IS '';
--rollback COMMENT ON TABLE master.user_filter IS '';
--rollback COMMENT ON TABLE master.variable IS 'A variable is a data element, which has precise meaning throughout the data model.
--rollback Each variable has a set of mandatory attributes, which are inherited by the children through the data model.';
--rollback COMMENT ON TABLE master.variable_mapping IS '';
--rollback COMMENT ON TABLE operational.file_cabinet IS '';
--rollback COMMENT ON TABLE operational.platform IS '';
--rollback COMMENT ON TABLE operational.study_task_definition IS 'Table for storing the study task definition such as the tasks list, services availed by the study, and timing rules
--rollback 
--rollback (v.calaminos)';
--rollback COMMENT ON TABLE operational.vendor IS '';
--rollback COMMENT ON TABLE platform.data_export_transaction IS '';
--rollback COMMENT ON TABLE platform.plan_template IS '';
--rollback COMMENT ON TABLE public.databasechangelog IS '';
--rollback COMMENT ON TABLE public.databasechangeloglock IS '';
--rollback COMMENT ON TABLE seed_warehouse.plate IS '';
--rollback COMMENT ON TABLE seed_warehouse.plate_layout IS '';
--rollback COMMENT ON TABLE seed_warehouse.temp_sample IS '';



--changeset postgres:update_notes_column_comments context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1861 Update notes column comments



-- update notes comments
COMMENT ON COLUMN experiment.entry.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.entry_data.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.entry_list.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.experiment.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.experiment_block.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.experiment_data.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.experiment_design.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.experiment_group.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.experiment_group_member.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.experiment_plan.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.experiment_protocol.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.location.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.location_data.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.location_occurrence_group.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.location_status.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.location_task.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.occurrence.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.occurrence_data.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.occurrence_group.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.occurrence_group_member.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.occurrence_layout.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.plant.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.plant_data.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.planting_envelope.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.planting_instruction.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.plot.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.plot_data.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.sample.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.sample_measurement.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.subplot.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.subplot_data.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN germplasm.cross.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN germplasm.cross_attribute.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN germplasm.cross_parent.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN germplasm.envelope.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN germplasm.family.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN germplasm.genetic_population.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN germplasm.genetic_population_attribute.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN germplasm.germplasm.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN germplasm.germplasm_attribute.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN germplasm.germplasm_name.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN germplasm.germplasm_relation.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN germplasm.germplasm_state_attribute.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN germplasm.germplasm_trait.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN germplasm.package.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN germplasm.package_relation.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN germplasm.package_trait.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN germplasm.product_profile.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN germplasm.product_profile_attribute.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN germplasm.seed.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN germplasm.seed_attribute.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN germplasm.seed_relation.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN germplasm.seed_trait.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN germplasm.taxonomy.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN place.facility.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN place.geospatial_object.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN place.geospatial_object_attribute.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN platform.data_export_transaction.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.breeding_zone.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.crop.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.crop_program.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.crop_program_team.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.experiment_plan.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.organization.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.person.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.person_role.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.phase.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.phase_scheme.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.pipeline.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.program.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.program_config.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.program_team.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.project.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.protocol.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.protocol_data.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.scheme.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.scheme_stage.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.season.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.service.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.service_provider.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.service_provider_team.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.stage.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.team.notes IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN tenant.team_member.notes IS 'Notes: Technical details about the record [NOTES]';



-- revert changes
--rollback COMMENT ON COLUMN experiment.entry.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.entry_data.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.entry_list.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.experiment.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.experiment_block.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.experiment_data.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.experiment_design.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.experiment_group.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.experiment_group_member.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.experiment_plan.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.experiment_protocol.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.location.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.location_data.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.location_occurrence_group.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.location_status.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.location_task.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.occurrence.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.occurrence_data.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.occurrence_group.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.occurrence_group_member.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.occurrence_layout.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.plant.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.plant_data.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.planting_envelope.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.planting_instruction.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.plot.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.plot_data.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.sample.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.sample_measurement.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.subplot.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN experiment.subplot_data.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN germplasm.cross.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN germplasm.cross_attribute.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN germplasm.cross_parent.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN germplasm.envelope.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN germplasm.family.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN germplasm.genetic_population.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN germplasm.genetic_population_attribute.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN germplasm.germplasm.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN germplasm.germplasm_attribute.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN germplasm.germplasm_name.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN germplasm.germplasm_relation.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN germplasm.germplasm_state_attribute.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN germplasm.germplasm_trait.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN germplasm.package.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN germplasm.package_relation.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN germplasm.package_trait.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN germplasm.product_profile.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN germplasm.product_profile_attribute.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN germplasm.seed.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN germplasm.seed_attribute.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN germplasm.seed_relation.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN germplasm.seed_trait.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN germplasm.taxonomy.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN place.facility.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN place.geospatial_object.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN place.geospatial_object_attribute.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN platform.data_export_transaction.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.breeding_zone.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.crop.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.crop_program.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.crop_program_team.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.experiment_plan.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.organization.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.person.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.person_role.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.phase.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.phase_scheme.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.pipeline.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.program.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.program_config.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.program_team.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.project.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.protocol.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.protocol_data.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.scheme.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.scheme_stage.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.season.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.service.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.service_provider.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.service_provider_team.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.stage.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.team.notes IS 'NOTES: Technical details about the record [ISVOID]';
--rollback COMMENT ON COLUMN tenant.team_member.notes IS 'NOTES: Technical details about the record [ISVOID]';
