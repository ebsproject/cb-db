--liquibase formatted sql

--changeset postgres:add_new_columns_in_api.background_job context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1713 Add new columns in api.background_job



ALTER TABLE 
	api.background_job
ADD COLUMN 
	entity_id integer,
ADD COLUMN
    entity character varying not null,
ADD COLUMN
    endpoint_entity character varying not null,
ADD COLUMN
    application character varying not null,
ADD COLUMN
    method character varying not null,
ADD FOREIGN KEY 
    (entity)
REFERENCES 
    dictionary.entity (abbrev),
ADD FOREIGN KEY 
    (endpoint_entity)
REFERENCES 
    dictionary.entity (abbrev),
ADD FOREIGN KEY 
    (application)
REFERENCES 
    platform.application (abbrev);
   
COMMENT ON COLUMN api.background_job.entity_id
  IS 'Reference to the experiment where the background job is used';

COMMENT ON COLUMN api.background_job.entity
  IS 'Reference to the entity where the background job is used';

COMMENT ON COLUMN api.background_job.endpoint_entity
  IS 'Reference to the entity where the background job is used';

COMMENT ON COLUMN api.background_job.application
  IS 'Reference to the application where the background job is used';

COMMENT ON COLUMN api.background_job.method
  IS 'Method used in the background job';



--rollback ALTER TABLE api.background_job
--rollback DROP COLUMN entity_id,
--rollback DROP COLUMN entity,
--rollback DROP COLUMN endpoint_entity,
--rollback DROP COLUMN application,
--rollback DROP COLUMN method;