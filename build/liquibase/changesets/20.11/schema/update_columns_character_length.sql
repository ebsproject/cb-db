--liquibase formatted sql

--changeset postgres:update_entry_name_character_length context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-2014 Update entry_name character length



ALTER TABLE 
    experiment.entry 
ALTER COLUMN 
    entry_name TYPE character varying;

ALTER TABLE 
    experiment.planting_instruction
ALTER COLUMN 
    entry_name TYPE character varying;



--rollback ALTER TABLE 
--rollback     experiment.entry 
--rollback ALTER COLUMN 
--rollback     entry_name TYPE character varying(128);
--rollback 
--rollback ALTER TABLE 
--rollback     experiment.planting_instruction
--rollback ALTER COLUMN 
--rollback     entry_name TYPE character varying(100);



--changeset postgres:update_germplasm_normalized_name_character_length context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-2014 Update germplasm_normalized_name character length



ALTER TABLE 
    germplasm.germplasm 
ALTER COLUMN 
    germplasm_normalized_name TYPE character varying;

ALTER TABLE 
    germplasm.germplasm_name
ALTER COLUMN 
    germplasm_normalized_name TYPE character varying;



--rollback ALTER TABLE 
--rollback     germplasm.germplasm 
--rollback ALTER COLUMN 
--rollback     germplasm_normalized_name TYPE character varying(256);
--rollback 
--rollback ALTER TABLE 
--rollback     germplasm.germplasm_name
--rollback ALTER COLUMN 
--rollback     germplasm_normalized_name TYPE character varying(256);



--changeset postgres:update_designation_and_name_value_character_length context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-2014 Update designation character length



ALTER TABLE 
    germplasm.germplasm 
ALTER COLUMN 
    designation TYPE character varying;

ALTER TABLE 
    germplasm.germplasm_name
ALTER COLUMN 
    name_value TYPE character varying;



--rollback ALTER TABLE 
--rollback     germplasm.germplasm 
--rollback ALTER COLUMN 
--rollback     designation TYPE character varying(256);
--rollback 
--rollback ALTER TABLE 
--rollback     germplasm.germplasm_name
--rollback ALTER COLUMN 
--rollback     name_value TYPE character varying(256);