--liquibase formatted sql

--changeset postgres:temporarily_drop_plot_location_id_plot_order_number_unq_in_experiment.plot context:schema splitStatements:false
--comment: EBS-1874 Temporarily drop constraint plot_location_id_plot_order_number_unq in experiment.plot



ALTER TABLE experiment.plot DROP CONSTRAINT plot_location_id_plot_order_number_unq;



--rollback ALTER TABLE experiment.plot
--rollback     ADD CONSTRAINT plot_location_id_plot_order_number_unq UNIQUE (location_id, plot_order_number);
--rollback 
--rollback COMMENT ON CONSTRAINT plot_location_id_plot_order_number_unq ON experiment.plot
--rollback     IS 'A plot can be retrieved by its unique location and plot order number.';