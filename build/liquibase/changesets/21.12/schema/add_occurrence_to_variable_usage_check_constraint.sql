--liquibase formatted sql

--changeset postgres:add_occurrence_to_variable_usage_check_constraint context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-863 Add occurrence to variable usage check constraint



-- drop check constraint
ALTER TABLE
    master.variable
DROP CONSTRAINT
    variable_usage_chk
;

-- add country to list
ALTER TABLE
    master.variable
ADD CONSTRAINT
    variable_usage_chk CHECK (usage = ANY (ARRAY['application', 'study', 'undefined', 'management', 'experiment', 'occurrence', 'experiment_manager']));
;



-- revert changes
--rollback ALTER TABLE
--rollback     master.variable
--rollback DROP CONSTRAINT
--rollback     variable_usage_chk
--rollback ;
--rollback 
--rollback ALTER TABLE
--rollback     master.variable
--rollback ADD CONSTRAINT
--rollback     variable_usage_chk CHECK (usage = ANY (ARRAY['application', 'study', 'undefined', 'management', 'experiment', 'experiment_manager']));
--rollback ;
