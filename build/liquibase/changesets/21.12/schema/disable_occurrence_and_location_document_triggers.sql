--liquibase formatted sql

--changeset postgres:disable_occurrence_document_triggers context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-797 Disable occurrence document triggers



ALTER TABLE experiment.occurrence
    DISABLE TRIGGER occurrence_update_location_document_from_occurrence_tgr;

ALTER TABLE experiment.occurrence
    DISABLE TRIGGER occurrence_update_occurrence_document_tgr;



-- revert changes
--rollback ALTER TABLE experiment.occurrence
--rollback     ENABLE TRIGGER occurrence_update_location_document_from_occurrence_tgr;
--rollback 
--rollback ALTER TABLE experiment.occurrence
--rollback     ENABLE TRIGGER occurrence_update_occurrence_document_tgr;



--changeset postgres:disable_location_document_trigger context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-797 Disable location document trigger



ALTER TABLE experiment.location
    DISABLE TRIGGER location_update_location_document_tgr;



-- revert changes
--rollback ALTER TABLE experiment.location
--rollback     ENABLE TRIGGER location_update_location_document_tgr;
