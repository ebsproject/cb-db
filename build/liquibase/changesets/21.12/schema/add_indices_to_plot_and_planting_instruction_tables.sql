--liquibase formatted sql

--changeset postgres:add_indices_to_experiment.plot_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-797 Add indices to experiment.plot table



CREATE INDEX plot_occurrence_id_is_void_idx
    ON experiment.plot (occurrence_id, is_void);

CREATE INDEX plot_location_id_is_void_idx
    ON experiment.plot (location_id, is_void);

ANALYZE experiment.plot;



-- revert changes
--rollback DROP INDEX experiment.plot_occurrence_id_is_void_idx;
--rollback DROP INDEX experiment.plot_location_id_is_void_idx;



--changeset postgres:add_index_to_experiment.planting_instruction_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-797 Add index to experiment.planting_instruction table



CREATE INDEX planting_instruction_plot_id_is_void_idx ON experiment.planting_instruction (plot_id, is_void);

ANALYZE experiment.planting_instruction;



-- revert changes
--rollback DROP INDEX experiment.planting_instruction_plot_id_is_void_idx;
