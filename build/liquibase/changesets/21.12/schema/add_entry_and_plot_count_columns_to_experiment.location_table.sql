--liquibase formatted sql

--changeset postgres:add_entry_count_column_to_experiment.location_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-797 Add entry_count column to experiment.location table



-- add column
ALTER TABLE
    experiment.location
ADD COLUMN
    entry_count integer
    NOT NULL
    DEFAULT 0
;

COMMENT ON COLUMN experiment.location.entry_count
    IS 'Entry Count: Number of entries in the location [LOC_ENTCOUNT]';

-- add index to new column
CREATE INDEX location_entry_count_idx
    ON experiment.location USING btree (entry_count);



-- revert changes
--rollback DROP INDEX experiment.location_entry_count_idx;
--rollback 
--rollback ALTER TABLE
--rollback     experiment.location
--rollback DROP COLUMN
--rollback     entry_count
--rollback ;



--changeset postgres:add_plot_count_column_to_experiment.location_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-797 Add plot_count column to experiment.location table



-- add column
ALTER TABLE
    experiment.location
ADD COLUMN
    plot_count integer
    NOT NULL
    DEFAULT 0
;

COMMENT ON COLUMN experiment.location.plot_count
    IS 'Plot Count: Number of plots in the location [LOC_PLOTCOUNT]';

-- add index to new column
CREATE INDEX location_plot_count_idx
    ON experiment.location USING btree (plot_count);



-- revert changes
--rollback DROP INDEX experiment.location_plot_count_idx;
--rollback 
--rollback ALTER TABLE
--rollback     experiment.location
--rollback DROP COLUMN
--rollback     plot_count
--rollback ;
