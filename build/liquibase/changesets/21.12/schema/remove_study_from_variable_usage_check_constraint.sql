--liquibase formatted sql

--changeset postgres:remove_study_from_variable_usage_check_constraint context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-863 Remove study from variable usage check constraint



-- drop check constraint
ALTER TABLE
    master.variable
DROP CONSTRAINT
    variable_usage_chk
;

-- add country to list
ALTER TABLE
    master.variable
ADD CONSTRAINT
    variable_usage_chk CHECK (usage = ANY (ARRAY['application', 'undefined', 'management', 'experiment', 'occurrence', 'experiment_manager']));
;



-- revert changes
--rollback ALTER TABLE
--rollback     master.variable
--rollback DROP CONSTRAINT
--rollback     variable_usage_chk
--rollback ;
--rollback 
--rollback ALTER TABLE
--rollback     master.variable
--rollback ADD CONSTRAINT
--rollback     variable_usage_chk CHECK (usage = ANY (ARRAY['application', 'study', 'undefined', 'management', 'experiment', 'occurrence', 'experiment_manager']));
--rollback ;
