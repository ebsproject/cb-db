--liquibase formatted sql

--changeset postgres:add_entry_count_column_to_experiment.occurrence_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-797 Add entry_count column to experiment.occurrence table



-- add column
ALTER TABLE
    experiment.occurrence
ADD COLUMN
    entry_count integer
    NOT NULL
    DEFAULT 0
;

COMMENT ON COLUMN experiment.occurrence.entry_count
    IS 'Entry Count: Number of entries in the occurrence [OCC_ENTCOUNT]';

-- add index to new column
CREATE INDEX occurrence_entry_count_idx
    ON experiment.occurrence USING btree (entry_count);



-- revert changes
--rollback DROP INDEX experiment.occurrence_entry_count_idx;
--rollback 
--rollback ALTER TABLE
--rollback     experiment.occurrence
--rollback DROP COLUMN
--rollback     entry_count
--rollback ;



--changeset postgres:add_plot_count_column_to_experiment.occurrence_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-797 Add plot_count column to experiment.occurrence table



-- add column
ALTER TABLE
    experiment.occurrence
ADD COLUMN
    plot_count integer
    NOT NULL
    DEFAULT 0
;

COMMENT ON COLUMN experiment.occurrence.plot_count
    IS 'Plot Count: Number of plots in the occurrence [OCC_PLOTCOUNT]';

-- add index to new column
CREATE INDEX occurrence_plot_count_idx
    ON experiment.occurrence USING btree (plot_count);



-- revert changes
--rollback DROP INDEX experiment.occurrence_plot_count_idx;
--rollback 
--rollback ALTER TABLE
--rollback     experiment.occurrence
--rollback DROP COLUMN
--rollback     plot_count
--rollback ;
