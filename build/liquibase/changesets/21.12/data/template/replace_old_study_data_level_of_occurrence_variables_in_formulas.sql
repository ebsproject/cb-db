--liquibase formatted sql

--changeset postgres:replace_old_study_data_level_of_occurrence_variables_in_formulas context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-863 Replace old study data level of occurrence variables in formulas


-- update data_level
UPDATE
    master.formula AS form
SET
    data_level = 'occurrence'
WHERE
    data_level = 'study'
;



-- revert changes
--rollback UPDATE
--rollback     master.formula AS form
--rollback SET
--rollback     data_level = 'study'
--rollback WHERE
--rollback     data_level = 'occurrence'
--rollback ;



--changeset postgres:replace_old_study_data_level_of_occurrence_variables_in_formula_parameters context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-863 Replace old study data level of occurrence variables in formula parameters



-- update parameter variable data level
UPDATE
    master.formula_parameter AS formparam
SET
    data_level = t.new_data_level
FROM (
        VALUES
        ('ADJHVAREA_CONT1 = ROWS_PER_PLOT_CONT * HVHILL_CONT / TOTAL_HILL_CONT::float', 'ADJHVAREA_CONT1', 'ROWS_PER_PLOT_CONT', 'study', 'occurrence'),
        ('PLOT_LN = (DIST_BET_HILLS/100) * HILLS_PER_ROW_CONT', 'PLOT_LN', 'HILLS_PER_ROW_CONT', 'study', 'occurrence'),
        ('PLOT_LN = (DIST_BET_HILLS/100) * HILLS_PER_ROW_CONT', 'PLOT_LN', 'DIST_BET_HILLS', 'study', 'occurrence'),
        ('ADJHVAREA_CONT3=ROWS_PER_PLOT_CONT * 0.2 * PLOT_LN_DSR::float::float::float', 'ADJHVAREA_CONT3', 'PLOT_LN_DSR', 'study', 'occurrence'),
        ('ADJHVAREA_CONT3=ROWS_PER_PLOT_CONT * 0.2 * PLOT_LN_DSR::float::float::float', 'ADJHVAREA_CONT3', 'ROWS_PER_PLOT_CONT', 'study', 'occurrence'),
        ('PLOT_AREA_SQM_CONT = ROWS_PER_PLOT_CONT * HILLS_PER_ROW_CONT * (DIST_BET_HILLS * DIST_BET_ROWS)/10000', 'PLOT_AREA_SQM_CONT', 'DIST_BET_ROWS', 'study', 'occurrence'),
        ('PLOT_AREA_SQM_CONT = ROWS_PER_PLOT_CONT * HILLS_PER_ROW_CONT * (DIST_BET_HILLS * DIST_BET_ROWS)/10000', 'PLOT_AREA_SQM_CONT', 'DIST_BET_HILLS', 'study', 'occurrence'),
        ('PLOT_AREA_SQM_CONT = ROWS_PER_PLOT_CONT * HILLS_PER_ROW_CONT * (DIST_BET_HILLS * DIST_BET_ROWS)/10000', 'PLOT_AREA_SQM_CONT', 'HILLS_PER_ROW_CONT', 'study', 'occurrence'),
        ('PLOT_AREA_SQM_CONT = ROWS_PER_PLOT_CONT * HILLS_PER_ROW_CONT * (DIST_BET_HILLS * DIST_BET_ROWS)/10000', 'PLOT_AREA_SQM_CONT', 'ROWS_PER_PLOT_CONT', 'study', 'occurrence'),
        ('ADJHVAREA_CONT2 = ROWS_PER_PLOT_CONT * DIST_BET_ROWS/100 * PLOT_LN_DSR', 'ADJHVAREA_CONT2', 'ROWS_PER_PLOT_CONT', 'study', 'occurrence'),
        ('ADJHVAREA_CONT2 = ROWS_PER_PLOT_CONT * DIST_BET_ROWS/100 * PLOT_LN_DSR', 'ADJHVAREA_CONT2', 'PLOT_LN_DSR', 'study', 'occurrence'),
        ('ADJHVAREA_CONT2 = ROWS_PER_PLOT_CONT * DIST_BET_ROWS/100 * PLOT_LN_DSR', 'ADJHVAREA_CONT2', 'DIST_BET_ROWS', 'study', 'occurrence'),
        ('FLW_CONT = FLW_DATE_CONT - SEEDING_DATE_CONT', 'FLW_CONT', 'SEEDING_DATE_CONT', 'study;entry;plot', 'occurrence;entry;plot')
    ) AS t (
        formula,
        result_variable,
        param_variable,
        old_data_level,
        new_data_level
    )
    JOIN master.variable AS resvar
        ON resvar.abbrev = t.result_variable
    JOIN master.variable AS parvar
        ON parvar.abbrev = t.param_variable
    JOIN master.formula AS form
        ON form.formula = t.formula
WHERE
    formparam.formula_id = form.id
    AND formparam.param_variable_id = parvar.id
    AND form.result_variable_id = resvar.id
;



-- revert changes
--rollback UPDATE
--rollback     master.formula_parameter AS formparam
--rollback SET
--rollback     data_level = t.old_data_level
--rollback FROM (
--rollback         VALUES
--rollback         ('ADJHVAREA_CONT1 = ROWS_PER_PLOT_CONT * HVHILL_CONT / TOTAL_HILL_CONT::float', 'ADJHVAREA_CONT1', 'ROWS_PER_PLOT_CONT', 'study', 'occurrence'),
--rollback         ('PLOT_LN = (DIST_BET_HILLS/100) * HILLS_PER_ROW_CONT', 'PLOT_LN', 'HILLS_PER_ROW_CONT', 'study', 'occurrence'),
--rollback         ('PLOT_LN = (DIST_BET_HILLS/100) * HILLS_PER_ROW_CONT', 'PLOT_LN', 'DIST_BET_HILLS', 'study', 'occurrence'),
--rollback         ('ADJHVAREA_CONT3=ROWS_PER_PLOT_CONT * 0.2 * PLOT_LN_DSR::float::float::float', 'ADJHVAREA_CONT3', 'PLOT_LN_DSR', 'study', 'occurrence'),
--rollback         ('ADJHVAREA_CONT3=ROWS_PER_PLOT_CONT * 0.2 * PLOT_LN_DSR::float::float::float', 'ADJHVAREA_CONT3', 'ROWS_PER_PLOT_CONT', 'study', 'occurrence'),
--rollback         ('PLOT_AREA_SQM_CONT = ROWS_PER_PLOT_CONT * HILLS_PER_ROW_CONT * (DIST_BET_HILLS * DIST_BET_ROWS)/10000', 'PLOT_AREA_SQM_CONT', 'DIST_BET_ROWS', 'study', 'occurrence'),
--rollback         ('PLOT_AREA_SQM_CONT = ROWS_PER_PLOT_CONT * HILLS_PER_ROW_CONT * (DIST_BET_HILLS * DIST_BET_ROWS)/10000', 'PLOT_AREA_SQM_CONT', 'DIST_BET_HILLS', 'study', 'occurrence'),
--rollback         ('PLOT_AREA_SQM_CONT = ROWS_PER_PLOT_CONT * HILLS_PER_ROW_CONT * (DIST_BET_HILLS * DIST_BET_ROWS)/10000', 'PLOT_AREA_SQM_CONT', 'HILLS_PER_ROW_CONT', 'study', 'occurrence'),
--rollback         ('PLOT_AREA_SQM_CONT = ROWS_PER_PLOT_CONT * HILLS_PER_ROW_CONT * (DIST_BET_HILLS * DIST_BET_ROWS)/10000', 'PLOT_AREA_SQM_CONT', 'ROWS_PER_PLOT_CONT', 'study', 'occurrence'),
--rollback         ('ADJHVAREA_CONT2 = ROWS_PER_PLOT_CONT * DIST_BET_ROWS/100 * PLOT_LN_DSR', 'ADJHVAREA_CONT2', 'ROWS_PER_PLOT_CONT', 'study', 'occurrence'),
--rollback         ('ADJHVAREA_CONT2 = ROWS_PER_PLOT_CONT * DIST_BET_ROWS/100 * PLOT_LN_DSR', 'ADJHVAREA_CONT2', 'PLOT_LN_DSR', 'study', 'occurrence'),
--rollback         ('ADJHVAREA_CONT2 = ROWS_PER_PLOT_CONT * DIST_BET_ROWS/100 * PLOT_LN_DSR', 'ADJHVAREA_CONT2', 'DIST_BET_ROWS', 'study', 'occurrence'),
--rollback         ('FLW_CONT = FLW_DATE_CONT - SEEDING_DATE_CONT', 'FLW_CONT', 'SEEDING_DATE_CONT', 'study;entry;plot', 'occurrence;entry;plot')
--rollback     ) AS t (
--rollback         formula,
--rollback         result_variable,
--rollback         param_variable,
--rollback         old_data_level,
--rollback         new_data_level
--rollback     )
--rollback     JOIN master.variable AS resvar
--rollback         ON resvar.abbrev = t.result_variable
--rollback     JOIN master.variable AS parvar
--rollback         ON parvar.abbrev = t.param_variable
--rollback     JOIN master.formula AS form
--rollback         ON form.formula = t.formula
--rollback WHERE
--rollback     formparam.formula_id = form.id
--rollback     AND formparam.param_variable_id = parvar.id
--rollback     AND form.result_variable_id = resvar.id
--rollback ;
