--liquibase formatted sql

--changeset postgres:replace_old_study_data_level_of_occurrence_variables context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-863 Replace old study data level of occurence variables



-- update data_level where study is only present
UPDATE
    master.variable AS var
SET
    data_level = REPLACE(var.data_level, 'study', 'occurrence')
WHERE
    var.data_level ILIKE '%study%'
    AND var.data_level NOT ILIKE '%occurrence%'
;

-- update data_level where both study and occurrence are present
UPDATE
    master.variable AS var
SET
    data_level = array_to_string(array(
        SELECT
            trim(t.data_level)
        FROM
            UNNEST(string_to_array(var.data_level, ',')) AS t (data_level)
        WHERE
            t.data_level NOT ILIKE '%study%'
    ), ',')
WHERE
    var.data_level ILIKE '%study%'
    AND var.data_level ILIKE '%occurrence%'
;



-- revert changes
--rollback UPDATE
--rollback     master.variable AS var
--rollback SET
--rollback     data_level = t.old_data_level
--rollback FROM (
--rollback         VALUES
--rollback         ('DH_FIRST_INCREASE_COMPLETED', 'experiment, study'),
--rollback         ('OCCURRENCE_NAME', 'experiment, study, occurrence'),
--rollback         ('FERT_OTHER_AMT_CONT', 'study, occurrence'),
--rollback         ('FERT1_TYPE_DISC', 'study, occurrence'),
--rollback         ('FERT_OTHER_TYPE_DISC', 'study, occurrence'),
--rollback         ('ROW', 'study, occurrence'),
--rollback         ('ROW_LENGTH_CONT', 'study, occurrence'),
--rollback         ('FERT3_AMT_CONT', 'study, occurrence'),
--rollback         ('PROGRAM', 'study, occurrence'),
--rollback         ('ORIGIN_SITE_CODE', 'experiment, study, occurrence'),
--rollback         ('PROTOCOL_TARGET_LEVEL', 'experiment, study, occurrence'),
--rollback         ('MANAGEMENT_PROTOCOL_LIST_ID', 'experiment, study, occurrence'),
--rollback         ('HARVEST_INSTRUCTIONS', 'experiment, study, occurrence'),
--rollback         ('POLLINATION_INSTRUCTIONS', 'experiment, study, occurrence'),
--rollback         ('OCCURRENCE_COUNT', 'experiment, study, occurrence'),
--rollback         ('FERT1_AMT_CONT', 'study, occurrence'),
--rollback         ('USE', 'experiment, study, occurrence'),
--rollback         ('GROWTH_HABIT', 'experiment, study, occurrence'),
--rollback         ('FERT1_DATE_CONT', 'study, occurrence'),
--rollback         ('ENTRY_COUNT_CONT', 'study, occurrence'),
--rollback         ('PLANNED_PESTICIDE_DATE', 'study, occurrence'),
--rollback         ('PLOT_AREA_SQM_CONT', 'study, occurrence'),
--rollback         ('PLANTING_JOB_FACILITY', 'experiment, study, occurrence'),
--rollback         ('FERT1_BRAND', 'study, occurrence'),
--rollback         ('FERT4_BRAND', 'study, occurrence'),
--rollback         ('SOIL_TEXT', 'study, occurrence'),
--rollback         ('INSCTPEST_BRAND2', 'study, occurrence'),
--rollback         ('FERT2_METH_DISC', 'study, occurrence'),
--rollback         ('COUNTRY', 'study, occurrence'),
--rollback         ('FERT2_TYPE_DISC', 'study, occurrence'),
--rollback         ('FERT3_BRAND', 'study, occurrence'),
--rollback         ('FERT3_DATE_CONT', 'study, occurrence'),
--rollback         ('FERT3_METH_DISC', 'study, occurrence'),
--rollback         ('FERT3_TYPE_DISC', 'study, occurrence'),
--rollback         ('SEEDING_DATE_CONT', 'study,plot, occurrence'),
--rollback         ('LIST_SUB_TYPE', 'experiment, study, occurrence'),
--rollback         ('FIRST_PLOT_POSITION_VIEW', 'experiment, study, occurrence'),
--rollback         ('HT9_CONT', 'study, occurrence'),
--rollback         ('PLOT_AREA_2', 'experiment, study, occurrence'),
--rollback         ('ESTAB', 'study, occurrence'),
--rollback         ('PLOT_AREA_3', 'experiment, study, occurrence'),
--rollback         ('DIST_BET_ROWS', 'study,entry,plot, occurrence'),
--rollback         ('DESIGN', 'study, occurrence'),
--rollback         ('FLD_LOC', 'study, occurrence'),
--rollback         ('STUDY_TITLE', 'study, occurrence'),
--rollback         ('ROWS_PER_PLOT_CONT', 'study,plot, occurrence'),
--rollback         ('SEED_USED_PAN_CONT', 'entry,study, occurrence'),
--rollback         ('INSCTPEST_TYPE_DISC', 'study, occurrence'),
--rollback         ('PEST_DATE_CONT', 'study, occurrence'),
--rollback         ('FERT2_AMT_CONT', 'study, occurrence'),
--rollback         ('FERT2_DATE_CONT', 'study, occurrence'),
--rollback         ('TRANS_DATE_CONT', 'study,entry,plot, occurrence'),
--rollback         ('PLOT_AREA_4', 'experiment, study, occurrence'),
--rollback         ('PLANTING_JOB_CODE', 'experiment, study, occurrence'),
--rollback         ('PHASE', 'study, occurrence'),
--rollback         ('PLOT_LN', 'study, occurrence'),
--rollback         ('PLACE', 'study, occurrence'),
--rollback         ('YEAR', 'study, occurrence'),
--rollback         ('CREATOR_MULTIPLE', 'study, occurrence'),
--rollback         ('TASK_DATE_CONT', 'study, occurrence'),
--rollback         ('BSD_END_DATE_MULTIPLE', 'study, occurrence'),
--rollback         ('PLOT_SIZE_SQM_CONT', 'study, occurrence'),
--rollback         ('PLANTING_JOB_STATUS', 'experiment, study, occurrence'),
--rollback         ('IS_STUDY_BLIND', 'study, occurrence'),
--rollback         ('HILLS_PER_ROW_CONT', 'study, occurrence'),
--rollback         ('DIST_BET_HILLS', 'study,entry,plot, occurrence'),
--rollback         ('PLOT_AREA_HA_CONT', 'study, occurrence'),
--rollback         ('FERT4_AMT_CONT', 'study, occurrence'),
--rollback         ('FERT4_TYPE_DISC', 'study, occurrence'),
--rollback         ('FERT4_DATE_CONT', 'study, occurrence'),
--rollback         ('PLOT_WIDTH_RICE', 'experiment, study, occurrence'),
--rollback         ('PLOT_NUMBERING_ORDER', 'experiment, study, occurrence'),
--rollback         ('NO_OF_REPS_IN_BLOCK', 'experiment, study, occurrence'),
--rollback         ('PLANTING_JOB_TYPE', 'experiment, study, occurrence'),
--rollback         ('PESTICIDE_TASK_DONE', 'study, occurrence'),
--rollback         ('PLOT_AREA_1', 'experiment, study, occurrence'),
--rollback         ('PLANTING_JOB_INSTRUCTIONS', 'experiment, study, occurrence'),
--rollback         ('TRAIT_PROTOCOL_LIST_ID', 'experiment, study, occurrence'),
--rollback         ('SEEDS_PER_ENVELOPE', 'experiment, study, occurrence'),
--rollback         ('ENVELOPES_PER_PLOT', 'experiment, study, occurrence'),
--rollback         ('STUDY', 'study, occurrence'),
--rollback         ('STATUS_MULTIPLE', 'study, occurrence'),
--rollback         ('FERT_OTHER_DATE_CONT', 'study, occurrence'),
--rollback         ('FERT1_METH_DISC', 'study, occurrence'),
--rollback         ('PLANTING_JOB_DUE_DATE', 'experiment, study, occurrence'),
--rollback         ('FERT2_BRAND', 'study, occurrence'),
--rollback         ('FERT1_RATIO', 'study, occurrence'),
--rollback         ('FERT4_METH_DISC', 'study, occurrence'),
--rollback         ('FERT_OTHER_BRAND', 'study, occurrence'),
--rollback         ('FERT_OTHER_METH_DISC', 'study, occurrence'),
--rollback         ('BLK', 'study, occurrence'),
--rollback         ('FLDROW_COUNT', 'study, occurrence'),
--rollback         ('FLDCOL_COUNT', 'study, occurrence'),
--rollback         ('RESEARCHER', 'study, occurrence'),
--rollback         ('ESTABLISHMENT', 'study, occurrence'),
--rollback         ('HV_AREA_SQM', 'study,plot, occurrence'),
--rollback         ('STUDY_REMARKS', 'study, occurrence'),
--rollback         ('INSCTPEST_BRAND1', 'study, occurrence'),
--rollback         ('SETNO', 'study, occurrence'),
--rollback         ('INSCTPEST_DATE_CONT1', 'study, occurrence'),
--rollback         ('WEED_CONTRL', 'study, occurrence'),
--rollback         ('SOIL_TYPE_DISC', 'study, occurrence'),
--rollback         ('SOILPH', 'study, occurrence'),
--rollback         ('SALINITY', 'study, occurrence'),
--rollback         ('ORGANIC', 'study, occurrence'),
--rollback         ('STUDY_OBJECTIVE', 'study, occurrence'),
--rollback         ('BREEDSTAGE', 'study, occurrence'),
--rollback         ('INSCTPEST_DATE_CONT2', 'study, occurrence'),
--rollback         ('INSCTPEST_BRAND3', 'study, occurrence'),
--rollback         ('INSCTPEST_DATE_CONT3', 'study, occurrence'),
--rollback         ('FERT_TYPE_DISC', 'study, occurrence'),
--rollback         ('INSCTPEST_BRAND4', 'study, occurrence'),
--rollback         ('INSCTPEST_BRAND5', 'study, occurrence'),
--rollback         ('INSCTPEST_BRAND6', 'study, occurrence'),
--rollback         ('INSCTPEST_BRAND7', 'study, occurrence'),
--rollback         ('INSCTPEST_DATE_CONT4', 'study, occurrence'),
--rollback         ('INSCTPEST_DATE_CONT5', 'study, occurrence'),
--rollback         ('INSCTPEST_DATE_CONT6', 'study, occurrence'),
--rollback         ('INSCTPEST_DATE_CONT7', 'study, occurrence'),
--rollback         ('MODULENO', 'entry,plot,study, occurrence'),
--rollback         ('LATITUDE', 'study, occurrence'),
--rollback         ('LONGITUDE', 'study, occurrence'),
--rollback         ('ELEVATION', 'study, occurrence'),
--rollback         ('SERV_REQUESTOR', 'study, occurrence'),
--rollback         ('PLOT_CODE_PATTERN', 'study, occurrence'),
--rollback         ('FIELD_AREA_HA_CONT', 'study, occurrence'),
--rollback         ('INSCTPEST_BRAND8', 'study, occurrence'),
--rollback         ('INSCTPEST_BRAND9', 'study, occurrence'),
--rollback         ('INSCTPEST_DATE_CONT8', 'study, occurrence'),
--rollback         ('INSCTPEST_DATE_CONT9', 'study, occurrence'),
--rollback         ('ADJHVAREA_CONT2', 'plot,study, occurrence'),
--rollback         ('INSCTPEST_BRAND10', 'study, occurrence'),
--rollback         ('INSCTPEST_BRAND11', 'study, occurrence'),
--rollback         ('FERT_RATIO', 'study, occurrence'),
--rollback         ('INSCTPEST_DATE_CONT10', 'study, occurrence'),
--rollback         ('INSCTPEST_DATE_CONT11', 'study, occurrence'),
--rollback         ('FERT2_RATIO', 'study, occurrence'),
--rollback         ('FERT3_RATIO', 'study, occurrence'),
--rollback         ('FERT4_RATIO', 'study, occurrence'),
--rollback         ('REQUESTOR_MULTIPLE', 'study, occurrence'),
--rollback         ('FERT_OTHER_RATIO', 'study, occurrence'),
--rollback         ('SEEDING_DATE_CONT_MULTIPLE', 'study, occurrence'),
--rollback         ('FLD_LOC_MULTIPLE', 'study, occurrence'),
--rollback         ('PULLING_DATE_MULTIPLE', 'study, occurrence'),
--rollback         ('TRANS_DATE_CONT_MULTIPLE', 'study, occurrence'),
--rollback         ('ROW_MULTIPLE', 'study, occurrence'),
--rollback         ('BSD_START_DATE_MULTIPLE', 'study, occurrence'),
--rollback         ('DIST_BET_ROWS_MULTIPLE', 'study, occurrence'),
--rollback         ('HILLS_PER_ROW_CONT_MULTIPLE', 'study, occurrence'),
--rollback         ('DIST_BET_HILLS_MULTIPLE', 'study, occurrence'),
--rollback         ('RETRANS_VOL_MULTIPLE', 'study, occurrence'),
--rollback         ('RETRANS_DATE_MULTIPLE', 'study, occurrence'),
--rollback         ('FERT_DATE_CONT_MULTIPLE', 'study, occurrence'),
--rollback         ('FERT_TYPE_DISC_MULTIPLE', 'study, occurrence'),
--rollback         ('FERT_AMT_CONT_MULTIPLE', 'study, occurrence'),
--rollback         ('FERT_BRAND_MULTIPLE', 'study, occurrence'),
--rollback         ('FERT_METH_DISC_MULTIPLE', 'study, occurrence'),
--rollback         ('FERT_RATIO_MULTIPLE', 'study, occurrence'),
--rollback         ('INSCTPEST_BRAND_MULTIPLE', 'study, occurrence'),
--rollback         ('FERT_AMT_KGHA_CONT_MULTIPLE', 'study, occurrence'),
--rollback         ('RECEPTION_DATE_MULTIPLE', 'study, occurrence'),
--rollback         ('START_DATE_MULTIPLE', 'study, occurrence'),
--rollback         ('END_DATE_MULTIPLE', 'study, occurrence'),
--rollback         ('TEMPERATURE_C_CONT_MULTIPLE', 'study, occurrence'),
--rollback         ('ROWS_PER_PLOT_CONT_MULTIPLE', 'study, occurrence'),
--rollback         ('PEST_TYPE_DISC_MULTIPLE', 'study, occurrence'),
--rollback         ('PEST_DATE_CONT_MULTIPLE', 'study, occurrence'),
--rollback         ('WEED_CONTROL_MULTIPLE', 'study, occurrence'),
--rollback         ('WEED_CONTROL_DATE_CONT_MULTIPLE', 'study, occurrence'),
--rollback         ('HERBICIDE_BRAND_MULTIPLE', 'study, occurrence'),
--rollback         ('SELECTION_TYPE_MULTIPLE', 'study, occurrence'),
--rollback         ('ARCHIVER_ID', 'study,entry,plot, occurrence'),
--rollback         ('DATE_CROSSED_MULTIPLE', 'study, occurrence'),
--rollback         ('PLANNED_DATE', 'study, occurrence'),
--rollback         ('THRESHING_DATE_CONT_MULTIPLE', 'study, occurrence'),
--rollback         ('THRESHING_METHOD_DISC_MULTIPLE', 'study, occurrence'),
--rollback         ('DURATION_H_CONT_MULTIPLE', 'study, occurrence'),
--rollback         ('HVDATE_CONT_MULTIPLE', 'study, occurrence'),
--rollback         ('HV_METH_DISC_MULTIPLE', 'study, occurrence'),
--rollback         ('GRNYLD_HARV_CON_MULTIPLE', 'study, occurrence'),
--rollback         ('TASK_ACTOR_MULTIPLE', 'study, occurrence'),
--rollback         ('TASK_REMARKS_MULTIPLE', 'study, occurrence'),
--rollback         ('WEED_CONTRL_MULTIPLE', 'study, occurrence'),
--rollback         ('IS_PLANNED_DATE', 'study, occurrence'),
--rollback         ('RELATIVE_HUMIDITY_PERC', 'study, occurrence'),
--rollback         ('TASK_DATE_CONT_MULTIPLE', 'study, occurrence'),
--rollback         ('INSCTPEST_DATE_CONT_MULTIPLE', 'study, occurrence'),
--rollback         ('OCS_REQ_NUM', 'study, occurrence'),
--rollback         ('PLANNED_DATE_MULTIPLE', 'study, occurrence'),
--rollback         ('OCS_REQ_NUM_MULTIPLE', 'study, occurrence'),
--rollback         ('SOURCE_GID', 'study, occurrence'),
--rollback         ('SEED_SAMPLE_ID', 'study, occurrence'),
--rollback         ('SITE_MEAN', 'study, occurrence'),
--rollback         ('ANALYSIS_REMARKS', 'study, occurrence'),
--rollback         ('HARV_PLOT_SIZE_SQM_CONT', 'study, occurrence'),
--rollback         ('PLOT_LN_DSR', 'study,plot, occurrence'),
--rollback         ('ENTRY_CODE_PATTERN', 'study, occurrence'),
--rollback         ('SEEDING_IN_DRY_BED', 'study,plot, occurrence'),
--rollback         ('ARCHIVE_TIMESTAMP', 'study,entry,plot, occurrence'),
--rollback         ('STUDY_STATUS', 'study, occurrence'),
--rollback         ('TRIALMEAN', 'study, occurrence'),
--rollback         ('WEATHER_OBSERVATION', 'study, occurrence'),
--rollback         ('LEAF_SAMPLING_DATE', 'study,entry,plot, occurrence'),
--rollback         ('NO_OF_HARVESTED_PLOTS', 'study, occurrence'),
--rollback         ('NO_OF_PROGENIES', 'study, occurrence'),
--rollback         ('DRYING_METHOD', 'study, occurrence'),
--rollback         ('DRYING_METHOD_MULTIPLE', 'study, occurrence'),
--rollback         ('RELATIVE_HUMIDITY_PERC_MULTIPLE', 'study, occurrence'),
--rollback         ('GRID_CODE_PATTERN', 'study, occurrence'),
--rollback         ('AGE_OF_SEED', 'study, occurrence'),
--rollback         ('SAMPLE_SOURCE', 'study, occurrence'),
--rollback         ('NO_OF_SAMPLES_PER_REP', 'study, occurrence'),
--rollback         ('SAMPLE_SELECTION_VALUE', 'study, occurrence'),
--rollback         ('INOCULATION_DATE_MULTIPLE', 'study, occurrence'),
--rollback         ('SCORING_DATE_MULTIPLE', 'study, occurrence'),
--rollback         ('REC_REQ_DATE_MULTIPLE', 'study, occurrence'),
--rollback         ('SEND_RESULT_DATE_MULTIPLE', 'study, occurrence'),
--rollback         ('SERVICE_PROCESS_PATH_IDS', 'study, occurrence'),
--rollback         ('RGA_TARGET_GENERATION', 'study, occurrence'),
--rollback         ('DATE_RECEIVED_MULTIPLE', 'study, occurrence'),
--rollback         ('DRYING_STARTED_MULTIPLE', 'study, occurrence'),
--rollback         ('DRYING_FINISHED_MULTIPLE', 'study, occurrence'),
--rollback         ('THRESHING_EQUIPMENT_MULTIPLE', 'study, occurrence'),
--rollback         ('COCOON_DATE_STARTED_MULTIPLE', 'study, occurrence'),
--rollback         ('COCOON_DATE_RELEASED_MULTIPLE', 'study, occurrence'),
--rollback         ('DATE_RELEASED_TO_CUSTOMER_MULTIPLE', 'study, occurrence'),
--rollback         ('DATE_DELIVERED_TO_LTE_PGF_MULTIPLE', 'study, occurrence'),
--rollback         ('DATE_STARTED_MULTIPLE', 'study, occurrence'),
--rollback         ('DATE_FINISHED_MULTIPLE', 'study, occurrence'),
--rollback         ('COCOON_NUMBER_MULTIPLE', 'study, occurrence'),
--rollback         ('GH_LOC', 'study, occurrence'),
--rollback         ('DISEASE_NAME', 'study, occurrence'),
--rollback         ('GENOTYPING_PLATFORM', 'study, occurrence'),
--rollback         ('ISOLATES', 'study, occurrence'),
--rollback         ('PERCENT_CHECK_PLOTS', 'study, occurrence'),
--rollback         ('LOCATION_REPS', 'study, occurrence'),
--rollback         ('LOCATION_BLOCKS', 'study, occurrence'),
--rollback         ('HAS_EXPERIMENT_GROUP', 'study, occurrence'),
--rollback         ('CROP', 'study, occurrence'),
--rollback         ('EXPERIMENT_TYPE', 'study, occurrence'),
--rollback         ('EXPERIMENT_TEMPLATE', 'study, occurrence'),
--rollback         ('EXPERIMENT_CODE', 'study, occurrence'),
--rollback         ('NO_OF_ROWS_PER_ENTRY_MULTIPLE', 'study, occurrence'),
--rollback         ('PLANTING_TYPE', 'experiment, study, occurrence'),
--rollback         ('STARTING_CORNER', 'experiment, study, occurrence'),
--rollback         ('NO_OF_BLOCKS', 'experiment, study, occurrence'),
--rollback         ('NO_OF_COLS_IN_BLOCK', 'experiment, study, occurrence'),
--rollback         ('NO_OF_ROWS_IN_BLOCK', 'experiment, study, occurrence'),
--rollback         ('NO_OF_PLANTS_PER_ROW_MULTIPLE', 'study, occurrence'),
--rollback         ('PLANTING_INSTRUCTIONS', 'experiment, study, occurrence'),
--rollback         ('PLOT_WIDTH', 'experiment, study, occurrence'),
--rollback         ('PLOT_WIDTH_WHEAT_FLAT', 'experiment, study, occurrence'),
--rollback         ('PLOT_WIDTH_MAIZE_BED', 'experiment, study, occurrence'),
--rollback         ('DISTANCE_BET_PLOTS_IN_M', 'experiment, study, occurrence'),
--rollback         ('NO_OF_BEDS_PER_PLOT', 'experiment, study, occurrence'),
--rollback         ('NO_OF_ROWS_PER_BED', 'experiment, study, occurrence'),
--rollback         ('BED_WIDTH', 'experiment, study, occurrence'),
--rollback         ('HILLS_BY_PLOT', 'experiment, study, occurrence'),
--rollback         ('PLOT_DENSITY', 'experiment, study, occurrence'),
--rollback         ('ALLEY_LENGTH', 'experiment, study, occurrence'),
--rollback         ('PLOT_UNIT_MEASUREMENT', 'experiment, study, occurrence'),
--rollback         ('DIST_BET_ROWS_IN_M', 'experiment, study, occurrence'),
--rollback         ('PROTOCOL_TYPE', 'experiment, study, occurrence'),
--rollback         ('PLOT_LN_1', 'experiment, study, occurrence'),
--rollback         ('PLOT_WIDTH_WHEAT_BED', 'experiment, study, occurrence'),
--rollback         ('SAMPLE_TYPE', 'study, occurrence'),
--rollback         ('PLATE_LAYOUT_ID', 'study, occurrence'),
--rollback         ('LIST_TYPE', 'study, occurrence'),
--rollback         ('GENOTYPE_RESULTS', 'study, occurrence'),
--rollback         ('STUDY_TYPE', 'study, occurrence'),
--rollback         ('EXPERIMENT_DESIGN_TYPE', 'experiment, study, occurrence'),
--rollback         ('FACILITY_TYPE', 'study, occurrence'),
--rollback         ('EXPERIMENT_STEWARD_OLD', 'study, occurrence'),
--rollback         ('PROJECT_OLD', 'study, occurrence'),
--rollback         ('DISTANCE_BET_PLOTS', 'experiment, study, occurrence'),
--rollback         ('LOCATION_STEWARD', 'experiment, study, occurrence'),
--rollback         ('EXPERIMENT_STATUS', 'experiment, study, occurrence'),
--rollback         ('EVALUATION_STAGE', 'study, occurrence'),
--rollback         ('HARVESTED_PACKAGE_PREFIX', 'study, occurrence'),
--rollback         ('EXPERIMENT_SUB_SUB_TYPE', 'study, occurrence'),
--rollback         ('LOCATION_HARVEST_DATE', 'experiment, study, occurrence'),
--rollback         ('PACKAGE_UNIT', 'experiment, study, occurrence'),
--rollback         ('ENTRY_LIST_TYPE', 'experiment, study, occurrence'),
--rollback         ('EXPERIMENT_NAME', 'study, occurrence'),
--rollback         ('SEEDING_RATE', 'experiment, study, occurrence'),
--rollback         ('CREATION_TIMESTAMP_MULTIPLE', 'study, occurrence'),
--rollback         ('PACKAGE_STATUS', 'experiment, study, occurrence'),
--rollback         ('PLANTING_SEASON', 'experiment, study, occurrence'),
--rollback         ('EXPERIMENT_SUB_TYPE', 'experiment, study, occurrence'),
--rollback         ('LOCATION_PLANTING_DATE', 'experiment, study, occurrence'),
--rollback         ('LOCATION_NAME', 'experiment, study, occurrence')
--rollback     ) AS t (
--rollback         abbrev,
--rollback         old_data_level
--rollback     )
--rollback WHERE
--rollback     var.abbrev = t.abbrev
--rollback ;



--liquibase formatted sql

--changeset postgres:replace_old_study_usage_of_occurrence_variables context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-863 Replace old study usage of occurence variables



-- update usage from study to occurrence
UPDATE
    master.variable AS var
SET
    usage = 'occurrence'
WHERE
    usage = 'study'
;



-- revert changes
--rollback UPDATE
--rollback     master.variable AS var
--rollback SET
--rollback     usage = 'study'
--rollback WHERE
--rollback     usage = 'occurrence'
--rollback ;
