--liquibase formatted sql

--changeset postgres:populate_entry_and_plot_count_columns_in_experiment.occurrence_table context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-797 Populate entry and plot count columns in experiment.occurrence table



-- prepare statement to populate entry and plot counts of occurrences
PREPARE populate_occurrence_entry_and_plot_counts(integer, integer) AS
    UPDATE
        experiment.occurrence AS occ
    SET
        entry_count = t.entry_count,
        plot_count = t.plot_count
    FROM (
            SELECT
                occ.id AS occurrence_id,
                (
                    SELECT
                        count(DISTINCT plot.entry_id)
                    FROM
                        experiment.plot AS plot
                        JOIN experiment.planting_instruction AS plantinst
                            ON plantinst.plot_id = plot.id
                        JOIN experiment.entry AS ent
                            ON ent.id = plot.entry_id
                    WHERE
                        plot.occurrence_id = occ.id
                        AND plot.is_void = FALSE
                        AND plantinst.is_void = FALSE
                        AND ent.is_void = FALSE
                ) AS entry_count,
                (
                    SELECT
                        count(*)
                    FROM
                        experiment.plot AS plot
                        JOIN experiment.planting_instruction AS plantinst
                            ON plantinst.plot_id = plot.id
                        JOIN experiment.entry AS ent
                            ON ent.id = plot.entry_id
                    WHERE
                        plot.occurrence_id = occ.id
                        AND plot.is_void = FALSE
                        AND plantinst.is_void = FALSE
                        AND ent.is_void = FALSE
                ) AS plot_count
            FROM
                experiment.occurrence AS occ
            WHERE
                occ.id IN (
                    SELECT
                        occ.id
                    FROM
                        experiment.occurrence AS occ
                    ORDER BY
                        occ.id ASC
                    LIMIT
                        $1
                    OFFSET
                        $1 * $2
                )
        ) AS t
    WHERE
        occ.id = t.occurrence_id
        AND (
            t.entry_count > 0
            OR t.plot_count > 0
        )
;

-- populate entry and plot counts of every 10000 occurrences (ETA: ~30-60s per batch)
EXECUTE populate_occurrence_entry_and_plot_counts(10000, 0);
EXECUTE populate_occurrence_entry_and_plot_counts(10000, 1);
EXECUTE populate_occurrence_entry_and_plot_counts(10000, 2);
EXECUTE populate_occurrence_entry_and_plot_counts(10000, 3);
EXECUTE populate_occurrence_entry_and_plot_counts(10000, 4);
EXECUTE populate_occurrence_entry_and_plot_counts(10000, 5);

DEALLOCATE populate_occurrence_entry_and_plot_counts;



-- revert changes
--rollback UPDATE
--rollback     experiment.occurrence AS occ
--rollback SET
--rollback     entry_count = 0,
--rollback     plot_count = 0
--rollback ;



--changeset postgres:populate_entry_and_plot_count_columns_in_experiment.location_table context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-797 Populate entry and plot count columns in experiment.location table



-- prepare statement to populate entry and plot counts of locations
PREPARE populate_location_entry_and_plot_counts(integer, integer) AS
    UPDATE
        experiment.location AS loc
    SET
        entry_count = t.entry_count,
        plot_count = t.plot_count
    FROM (
            SELECT
                loc.id AS location_id,
                (
                    SELECT
                        count(DISTINCT plot.entry_id)
                    FROM
                        experiment.plot AS plot
                        JOIN experiment.planting_instruction AS plantinst
                            ON plantinst.plot_id = plot.id
                        JOIN experiment.entry AS ent
                            ON ent.id = plot.entry_id
                    WHERE
                        plot.location_id = loc.id
                        AND plot.is_void = FALSE
                        AND plantinst.is_void = FALSE
                        AND ent.is_void = FALSE
                ) AS entry_count,
                (
                    SELECT
                        count(plot.id)
                    FROM
                        experiment.plot AS plot
                        JOIN experiment.planting_instruction AS plantinst
                            ON plantinst.plot_id = plot.id
                        JOIN experiment.entry AS ent
                            ON ent.id = plot.entry_id
                    WHERE
                        plot.location_id = loc.id
                        AND plot.is_void = FALSE
                        AND plantinst.is_void = FALSE
                        AND ent.is_void = FALSE
                ) AS plot_count
            FROM
                experiment.location AS loc
            WHERE
                loc.id IN (
                    SELECT
                        loc.id
                    FROM
                        experiment.location AS loc
                    ORDER BY
                        loc.id ASC
                    LIMIT
                        $1
                    OFFSET
                        $1 * $2
                )
        ) AS t
    WHERE
        loc.id = t.location_id
        AND (
            t.entry_count > 0
            OR t.plot_count > 0
        )
;

-- populate entry and plot counts of every 10000 locations (ETA: ~40-60s per batch)
EXECUTE populate_location_entry_and_plot_counts(10000, 0);
EXECUTE populate_location_entry_and_plot_counts(10000, 1);
EXECUTE populate_location_entry_and_plot_counts(10000, 2);
EXECUTE populate_location_entry_and_plot_counts(10000, 3);
EXECUTE populate_location_entry_and_plot_counts(10000, 4);
EXECUTE populate_location_entry_and_plot_counts(10000, 5);

DEALLOCATE populate_location_entry_and_plot_counts;



-- revert changes
--rollback UPDATE
--rollback     experiment.location AS loc
--rollback SET
--rollback     entry_count = 0,
--rollback     plot_count = 0
--rollback ;
