--liquibase formatted sql

--changeset postgres:add_im_update_configs_empty_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4359 IM DB: Add EMPTY IM UPDATE configs

--seed - rice config
INSERT INTO
    platform.config(
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES (
    'IM_UPDATE_SEED_RICE_DEFAULT',
    'Inventory Manager configuration for Rice default variables - seed update', 
    $${
        "values": []
    }$$,
    1,
    'Inventory Manager file upload and data validation',
    1,
    'added by j.bantay'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'IM_UPDATE_SEED_RICE_DEFAULT';



--seed - maize config
INSERT INTO
    platform.config(
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES (
    'IM_UPDATE_SEED_MAIZE_DEFAULT',
    'Inventory Manager configuration for Maize default variables - seed update', 
    $${
        "values": []
    }$$,
    1,
    'Inventory Manager file upload and data validation',
    1,
    'added by j.bantay'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'IM_UPDATE_SEED_MAIZE_DEFAULT';



--seed - wheat config
INSERT INTO
    platform.config(
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES (
    'IM_UPDATE_SEED_WHEAT_DEFAULT',
    'Inventory Manager configuration for Wheat default variables - seed update', 
    $${
        "values": []
    }$$,
    1,
    'Inventory Manager file upload and data validation',
    1,
    'added by j.bantay'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'IM_UPDATE_SEED_WHEAT_DEFAULT';



--package - rice config
INSERT INTO
    platform.config(
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES (
    'IM_UPDATE_PACKAGE_RICE_DEFAULT',
    'Inventory Manager configuration for Rice default variables - package update', 
    $${
        "values": []
    }$$,
    1,
    'Inventory Manager file upload and data validation',
    1,
    'added by j.bantay'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'IM_UPDATE_PACKAGE_RICE_DEFAULT';



--package - maize config
INSERT INTO
    platform.config(
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES (
    'IM_UPDATE_PACKAGE_MAIZE_DEFAULT',
    'Inventory Manager configuration for Maize default variables - package update', 
    $${
        "values": []
    }$$,
    1,
    'Inventory Manager file upload and data validation',
    1,
    'added by j.bantay'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'IM_UPDATE_PACKAGE_MAIZE_DEFAULT';



--package - wheat config
INSERT INTO
    platform.config(
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES (
    'IM_UPDATE_PACKAGE_WHEAT_DEFAULT',
    'Inventory Manager configuration for Wheat default variables - package update', 
    $${
        "values": []
    }$$,
    1,
    'Inventory Manager file upload and data validation',
    1,
    'added by j.bantay'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'IM_UPDATE_PACKAGE_WHEAT_DEFAULT';