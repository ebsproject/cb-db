--liquibase formatted sql

--changeset postgres:update_gm_config_wheat_default_visibility_attribute_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4360 GM DB: Modify visibility attribute in GM Wheat default config



-- update config
UPDATE platform.config
SET
    config_value = $${
        "values": [
            {
                "name": "Germplasm Type",
                "type": "column",
                "usage": "optional",
                "abbrev": "GERMPLASM_TYPE",
                "entity": "germplasm",
                "view": {
                    "visible": "true",
                    "entities": [
                        "germplasm"
                    ]
                },
                "required": "false",
                "api_field": "germplasmType",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Grain Color",
                "type": "attribute",
                "usage": "optional",
                "abbrev": "GRAIN_COLOR",
                "entity": "germplasm",
                "view": {
                    "visible": "false",
                    "entities": [ ]
                },
                "required": "true",
                "api_field": "dataValue",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Growth Habit",
                "type": "attribute",
                "usage": "optional",
                "abbrev": "GROWTH_HABIT",
                "entity": "germplasm",
                "view": {
                    "visible": "false",
                    "entities": [ ]
                },
                "required": "true",
                "api_field": "dataValue",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Cross Number",
                "type": "attribute",
                "usage": "optional",
                "abbrev": "CROSS_NUMBER",
                "entity": "germplasm",
                "view": {
                    "visible": "false",
                    "entities": [ ]
                },
                "required": "true",
                "api_field": "dataValue",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Program Code",
                "type": "column",
                "usage": "optional",
                "abbrev": "PROGRAM_CODE",
                "entity": "seed",
                "view": {
                    "visible": "true",
                    "entities": [
                        "seed"
                    ]
                },
                "required": "false",
                "api_field": "programDbId",
                "data_type": "string",
                "retrieve_db_id": "true",
                "retrieve_endpoint": "programs",
                "retrieve_api_value_field": "programDbId",
                "retrieve_api_search_field": "programCode"
            },
            {
                "name": "Harvest Date",
                "type": "column",
                "usage": "optional",
                "abbrev": "HVDATE_CONT",
                "entity": "seed",
                "view": {
                    "visible": "true",
                    "entities": [
                        "seed"
                    ]
                },
                "required": "false",
                "api_field": "harvestDate",
                "data_type": "date",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Harvest Method",
                "type": "column",
                "usage": "optional",
                "abbrev": "HV_METH_DISC",
                "entity": "seed",
                "view": {
                    "visible": "true",
                    "entities": [
                        "seed"
                    ]
                },
                "required": "false",
                "api_field": "harvestMethod",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Description",
                "type": "column",
                "usage": "optional",
                "abbrev": "DESCRIPTION",
                "entity": "seed",
                "view": {
                    "visible": "true",
                    "entities": [
                        "seed"
                    ]
                },
                "required": "false",
                "api_field": "description",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "MTA Status",
                "type": "attribute",
                "usage": "optional",
                "abbrev": "MTA_STATUS",
                "entity": "seed",
                "view": {
                    "visible": "false",
                    "entities": [ ]
                },
                "required": "false",
                "api_field": "dataValue",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "IP Status",
                "type": "attribute",
                "usage": "optional",
                "abbrev": "IP_STATUS",
                "entity": "seed",
                "view": {
                    "visible": "false",
                    "entities": [ ]
                },
                "required": "false",
                "api_field": "dataValue",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Source Organization",
                "type": "attribute",
                "usage": "optional",
                "abbrev": "SOURCE_ORGANIZATION",
                "entity": "seed",
                "view": {
                    "visible": "false",
                    "entities": [ ]
                },
                "required": "false",
                "api_field": "dataValue",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Origin",
                "type": "attribute",
                "usage": "optional",
                "abbrev": "ORIGIN",
                "entity": "seed",
                "view": {
                    "visible": "false",
                    "entities": [ ]
                },
                "required": "false",
                "api_field": "dataValue",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Source Study",
                "type": "attribute",
                "usage": "optional",
                "abbrev": "SOURCE_STUDY",
                "entity": "seed",
                "view": {
                    "visible": "false",
                    "entities": [ ]
                },
                "required": "false",
                "api_field": "dataValue",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Source Harvest Year",
                "type": "attribute",
                "usage": "optional",
                "abbrev": "SOURCE_HARV_YEAR",
                "entity": "seed",
                "view": {
                    "visible": "false",
                    "entities": [ ]
                },
                "required": "false",
                "api_field": "dataValue",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "MTA Number",
                "type": "attribute",
                "usage": "optional",
                "abbrev": "MTA_NUMBER",
                "entity": "seed",
                "view": {
                    "visible": "false",
                    "entities": [ ]
                },
                "required": "false",
                "api_field": "dataValue",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Import Attribute",
                "type": "attribute",
                "usage": "optional",
                "abbrev": "IMPORT_ATTRIBUTE",
                "entity": "seed",
                "view": {
                    "visible": "false",
                    "entities": [ ]
                },
                "required": "false",
                "api_field": "dataValue",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Package Quantity",
                "type": "column",
                "usage": "required",
                "abbrev": "VOLUME",
                "entity": "package",
                "view": {
                    "visible": "true",
                    "entities": [
                        "package"
                    ]
                },
                "required": "true",
                "api_field": "packageQuantity",
                "data_type": "float",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Package Unit",
                "type": "column",
                "usage": "required",
                "abbrev": "PACKAGE_UNIT",
                "entity": "package",
                "view": {
                    "visible": "true",
                    "entities": [
                        "package"
                    ]
                },
                "required": "true",
                "api_field": "packageUnit",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            }
        ]
    }$$
WHERE
    abbrev = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_WHEAT_DEFAULT';



--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "values": [
--rollback             {
--rollback                 "name": "Germplasm Type",
--rollback                 "type": "column",
--rollback                 "usage": "optional",
--rollback                 "abbrev": "GERMPLASM_TYPE",
--rollback                 "entity": "germplasm",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "api_field": "germplasmType",
--rollback                 "data_type": "string",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_value_field": "",
--rollback                 "retrieve_api_search_field": ""
--rollback             },
--rollback             {
--rollback                 "name": "Grain Color",
--rollback                 "type": "attribute",
--rollback                 "usage": "optional",
--rollback                 "abbrev": "GRAIN_COLOR",
--rollback                 "entity": "germplasm",
--rollback                 "visible": "true",
--rollback                 "required": "true",
--rollback                 "api_field": "dataValue",
--rollback                 "data_type": "string",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_value_field": "",
--rollback                 "retrieve_api_search_field": ""
--rollback             },
--rollback             {
--rollback                 "name": "Growth Habit",
--rollback                 "type": "attribute",
--rollback                 "usage": "optional",
--rollback                 "abbrev": "GROWTH_HABIT",
--rollback                 "entity": "germplasm",
--rollback                 "visible": "true",
--rollback                 "required": "true",
--rollback                 "api_field": "dataValue",
--rollback                 "data_type": "string",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_value_field": "",
--rollback                 "retrieve_api_search_field": ""
--rollback             },
--rollback             {
--rollback                 "name": "Cross Number",
--rollback                 "type": "attribute",
--rollback                 "usage": "optional",
--rollback                 "abbrev": "CROSS_NUMBER",
--rollback                 "entity": "germplasm",
--rollback                 "visible": "true",
--rollback                 "required": "true",
--rollback                 "api_field": "dataValue",
--rollback                 "data_type": "string",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_value_field": "",
--rollback                 "retrieve_api_search_field": ""
--rollback             },
--rollback             {
--rollback                 "name": "Program Code",
--rollback                 "type": "column",
--rollback                 "usage": "optional",
--rollback                 "abbrev": "PROGRAM_CODE",
--rollback                 "entity": "seed",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "api_field": "programDbId",
--rollback                 "data_type": "string",
--rollback                 "retrieve_db_id": "true",
--rollback                 "retrieve_endpoint": "programs",
--rollback                 "retrieve_api_value_field": "programDbId",
--rollback                 "retrieve_api_search_field": "programCode"
--rollback             },
--rollback             {
--rollback                 "name": "Harvest Date",
--rollback                 "type": "column",
--rollback                 "usage": "optional",
--rollback                 "abbrev": "HVDATE_CONT",
--rollback                 "entity": "seed",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "api_field": "harvestDate",
--rollback                 "data_type": "date",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_value_field": "",
--rollback                 "retrieve_api_search_field": ""
--rollback             },
--rollback             {
--rollback                 "name": "Harvest Method",
--rollback                 "type": "column",
--rollback                 "usage": "optional",
--rollback                 "abbrev": "HV_METH_DISC",
--rollback                 "entity": "seed",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "api_field": "harvestMethod",
--rollback                 "data_type": "string",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_value_field": "",
--rollback                 "retrieve_api_search_field": ""
--rollback             },
--rollback             {
--rollback                 "name": "Description",
--rollback                 "type": "column",
--rollback                 "usage": "optional",
--rollback                 "abbrev": "DESCRIPTION",
--rollback                 "entity": "seed",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "api_field": "description",
--rollback                 "data_type": "string",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_value_field": "",
--rollback                 "retrieve_api_search_field": ""
--rollback             },
--rollback             {
--rollback                 "name": "MTA Status",
--rollback                 "type": "attribute",
--rollback                 "usage": "optional",
--rollback                 "abbrev": "MTA_STATUS",
--rollback                 "entity": "seed",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "api_field": "dataValue",
--rollback                 "data_type": "string",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_value_field": "",
--rollback                 "retrieve_api_search_field": ""
--rollback             },
--rollback             {
--rollback                 "name": "IP Status",
--rollback                 "type": "attribute",
--rollback                 "usage": "optional",
--rollback                 "abbrev": "IP_STATUS",
--rollback                 "entity": "seed",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "api_field": "dataValue",
--rollback                 "data_type": "string",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_value_field": "",
--rollback                 "retrieve_api_search_field": ""
--rollback             },
--rollback             {
--rollback                 "name": "Source Organization",
--rollback                 "type": "attribute",
--rollback                 "usage": "optional",
--rollback                 "abbrev": "SOURCE_ORGANIZATION",
--rollback                 "entity": "seed",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "api_field": "dataValue",
--rollback                 "data_type": "string",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_value_field": "",
--rollback                 "retrieve_api_search_field": ""
--rollback             },
--rollback             {
--rollback                 "name": "Origin",
--rollback                 "type": "attribute",
--rollback                 "usage": "optional",
--rollback                 "abbrev": "ORIGIN",
--rollback                 "entity": "seed",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "api_field": "dataValue",
--rollback                 "data_type": "string",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_value_field": "",
--rollback                 "retrieve_api_search_field": ""
--rollback             },
--rollback             {
--rollback                 "name": "Source Study",
--rollback                 "type": "attribute",
--rollback                 "usage": "optional",
--rollback                 "abbrev": "SOURCE_STUDY",
--rollback                 "entity": "seed",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "api_field": "dataValue",
--rollback                 "data_type": "string",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_value_field": "",
--rollback                 "retrieve_api_search_field": ""
--rollback             },
--rollback             {
--rollback                 "name": "Source Harvest Year",
--rollback                 "type": "attribute",
--rollback                 "usage": "optional",
--rollback                 "abbrev": "SOURCE_HARV_YEAR",
--rollback                 "entity": "seed",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "api_field": "dataValue",
--rollback                 "data_type": "string",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_value_field": "",
--rollback                 "retrieve_api_search_field": ""
--rollback             },
--rollback             {
--rollback                 "name": "MTA Number",
--rollback                 "type": "attribute",
--rollback                 "usage": "optional",
--rollback                 "abbrev": "MTA_NUMBER",
--rollback                 "entity": "seed",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "api_field": "dataValue",
--rollback                 "data_type": "string",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_value_field": "",
--rollback                 "retrieve_api_search_field": ""
--rollback             },
--rollback             {
--rollback                 "name": "Import Attribute",
--rollback                 "type": "attribute",
--rollback                 "usage": "optional",
--rollback                 "abbrev": "IMPORT_ATTRIBUTE",
--rollback                 "entity": "seed",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "api_field": "dataValue",
--rollback                 "data_type": "string",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_value_field": "",
--rollback                 "retrieve_api_search_field": ""
--rollback             },
--rollback             {
--rollback                 "name": "Package Quantity",
--rollback                 "type": "column",
--rollback                 "usage": "required",
--rollback                 "abbrev": "VOLUME",
--rollback                 "entity": "package",
--rollback                 "visible": "true",
--rollback                 "required": "true",
--rollback                 "api_field": "packageQuantity",
--rollback                 "data_type": "float",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_value_field": "",
--rollback                 "retrieve_api_search_field": ""
--rollback             },
--rollback             {
--rollback                 "name": "Package Unit",
--rollback                 "type": "column",
--rollback                 "usage": "required",
--rollback                 "abbrev": "PACKAGE_UNIT",
--rollback                 "entity": "package",
--rollback                 "visible": "true",
--rollback                 "required": "true",
--rollback                 "api_field": "packageUnit",
--rollback                 "data_type": "string",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_value_field": "",
--rollback                 "retrieve_api_search_field": ""
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_WHEAT_DEFAULT';