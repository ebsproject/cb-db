--liquibase formatted sql

--changeset author_name:add_new_scale_conversion context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1484 Add new scale conversion



INSERT INTO
    master.scale_conversion
        (source_unit_id, target_unit_id, conversion_value, creator_id, crop_id)
SELECT
    t.source_unit_id,
    t.target_unit_id,
    t.conversion_value,
    1,
    t.crop_id
FROM
    (
        VALUES
        -- Wheat conversion
        (
            (SELECT id FROM master.scale_value sv WHERE sv.abbrev = 'PACKAGE_UNIT_G'),
            (SELECT id FROM master.scale_value sv WHERE sv.abbrev = 'PACKAGE_UNIT_SEEDS'),
            '18'::float,
            (SELECT id FROM tenant.crop WHERE crop_code='WHEAT')                       
        ),
        -- Maize conversion
        (
            (SELECT id FROM master.scale_value sv WHERE sv.abbrev = 'PACKAGE_UNIT_G'),
            (SELECT id FROM master.scale_value sv WHERE sv.abbrev = 'PACKAGE_UNIT_SEEDS'),
            '2.25'::float,
            (SELECT id FROM tenant.crop WHERE crop_code='MAIZE') 
        ),
        (
            (SELECT id FROM master.scale_value sv WHERE sv.abbrev = 'PACKAGE_UNIT_KG'),
            (SELECT id FROM master.scale_value sv WHERE sv.abbrev = 'PACKAGE_UNIT_SEEDS'),
            '2250'::float,
            (SELECT id FROM tenant.crop WHERE crop_code='MAIZE') 
        ),
        -- Default conversion
        (
            (SELECT id FROM master.scale_value sv WHERE sv.abbrev = 'PACKAGE_UNIT_KG'),
            (SELECT id FROM master.scale_value sv WHERE sv.abbrev = 'PACKAGE_UNIT_G'),
            '1000'::float,
            NULL 
        ),
        (
            (SELECT id FROM master.scale_value sv WHERE sv.abbrev = 'PACKAGE_UNIT_G'),
            (SELECT id FROM master.scale_value sv WHERE sv.abbrev = 'PACKAGE_UNIT_PANICLES'),
            '1'::float,
            NULL 
        ),
        (
            (SELECT id FROM master.scale_value sv WHERE sv.abbrev = 'PACKAGE_UNIT_G'),
            (SELECT id FROM master.scale_value sv WHERE sv.abbrev = 'PACKAGE_UNIT_SEEDS'),
            '1'::float,
            NULL 
        ),
        (
            (SELECT id FROM master.scale_value sv WHERE sv.abbrev = 'PACKAGE_UNIT_G'),
            (SELECT id FROM master.scale_value sv WHERE sv.abbrev = 'PACKAGE_UNIT_G'),
            '1'::float,
            NULL 
        ),
        (
            (SELECT id FROM master.scale_value sv WHERE sv.abbrev = 'PACKAGE_UNIT_G'),
            (SELECT id FROM master.scale_value sv WHERE sv.abbrev = 'PACKAGE_UNIT_PLANT'),
            '1'::float,
            NULL 
        )
    ) AS t (source_unit_id, target_unit_id, conversion_value, crop_id)
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_conversion AS sc 
--rollback WHERE
--rollback     sc.source_unit_id = (SELECT id FROM master.scale_value WHERE abbrev='PACKAGE_UNIT_G')
--rollback AND
--rollback     sc.target_unit_id  = (SELECT id FROM master.scale_value WHERE abbrev='PACKAGE_UNIT_SEEDS')
--rollback ;
--rollback DELETE FROM
--rollback     master.scale_conversion AS sc
--rollback WHERE
--rollback     sc.source_unit_id = (SELECT id FROM master.scale_value WHERE abbrev='PACKAGE_UNIT_KG')
--rollback AND
--rollback     sc.target_unit_id  = (SELECT id FROM master.scale_value WHERE abbrev='PACKAGE_UNIT_SEEDS')
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.scale_conversion AS sc
--rollback WHERE
--rollback     sc.source_unit_id = (SELECT id FROM master.scale_value WHERE abbrev='PACKAGE_UNIT_KG')
--rollback AND
--rollback     sc.target_unit_id  = (SELECT id FROM master.scale_value WHERE abbrev='PACKAGE_UNIT_G')
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.scale_conversion AS sc
--rollback WHERE
--rollback     sc.source_unit_id = (SELECT id FROM master.scale_value WHERE abbrev='PACKAGE_UNIT_G')
--rollback AND
--rollback     sc.target_unit_id  = (SELECT id FROM master.scale_value WHERE abbrev='PACKAGE_UNIT_PANICLES')
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.scale_conversion AS sc
--rollback WHERE
--rollback     sc.source_unit_id = (SELECT id FROM master.scale_value WHERE abbrev='PACKAGE_UNIT_G')
--rollback AND
--rollback     sc.target_unit_id  = (SELECT id FROM master.scale_value WHERE abbrev='PACKAGE_UNIT_G')
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.scale_conversion AS sc
--rollback WHERE
--rollback     sc.source_unit_id = (SELECT id FROM master.scale_value WHERE abbrev='PACKAGE_UNIT_G')
--rollback AND
--rollback     sc.target_unit_id  = (SELECT id FROM master.scale_value WHERE abbrev='PACKAGE_UNIT_PLANT')
--rollback ;