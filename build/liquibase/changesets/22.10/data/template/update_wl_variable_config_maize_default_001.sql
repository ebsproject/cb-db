--liquibase formatted sql

--changeset postgres:update_wl_variable_config_maize_default context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4304 SS: Working List - Create new variable configuration records



-- update config
UPDATE platform.config
SET
    abbrev = 'WORKING_LIST_VARIABLES_CONFIG_MAIZE_DEFAULT',
    config_value = $${
        "values":[
            {
                "abbrev":"PROGRAM",
                "display_name":"Program",
                "label":"Package Program",
                "description":"Package program",
                "browser_column":"true",
                "visible_browser":"true",
                "visible_export":"true",
                "visible_template":"true",
                "sort":"true",
                "filter":"true",
                "data_level":"package"
            },
            {
                "abbrev":"SEED_NAME",
                "display_name":"Seed Name",
                "label":"Seed Name",
                "description":"Seed Name",
                "browser_column":"true",
                "visible_browser":"true",
                "visible_export":"true",
                "visible_template":"true",
                "sort":"true",
                "filter":"true",
                "data_level":"seed"
            },
            {
                "abbrev":"SEED_CODE",
                "display_name":"Seed Code",
                "label":"Seed Code",
                "description":"Seed Code",
                "browser_column":"true",
                "visible_browser":"true",
                "visible_export":"true",
                "visible_template":"true",
                "sort":"true",
                "filter":"true",
                "data_level":"seed"
            },
            {
                "abbrev":"PACKAGE_CODE",
                "display_name":"Package Code",
                "label":"Package Code",
                "description":"Package Code",
                "browser_column":"true",
                "visible_browser":"true",
                "visible_export":"true",
                "visible_template":"true",
                "sort":"true",
                "filter":"true",
                "data_level":"package"
            },
            {
                "abbrev":"PACKAGE_LABEL",
                "display_name":"Package Label",
                "label":"Package Label",
                "description":"Package Label",
                "browser_column":"true",
                "visible_browser":"true",
                "visible_export":"true",
                "visible_template":"true",
                "sort":"true",
                "filter":"true",
                "data_level":"package"
            },
            {
                "abbrev":"GERMPLASM_CODE",
                "display_name":"Germplasm Code",
                "label":"Germplasm Code",
                "description":"Germplasm Code",
                "browser_column":"true",
                "visible_browser":"true",
                "visible_export":"true",
                "visible_template":"true",
                "sort":"true",
                "filter":"true",
                "data_level":"germplasm"
            },
            {
                "abbrev":"GERMPLASM_NAME",
                "display_name":"Germplasm Name",
                "label":"Germplasm Name",
                "description":"Germplasm Name",
                "browser_column":"true",
                "visible_browser":"true",
                "visible_export":"true",
                "visible_template":"true",
                "sort":"true",
                "filter":"true",
                "data_level":"germplasm"
            },
            {
                "abbrev":"PARENTAGE",
                "display_name":"Parentage",
                "label":"Parentage",
                "description":"Parentage",
                "browser_column":"true",
                "visible_browser":"true",
                "visible_export":"true",
                "visible_template":"true",
                "sort":"true",
                "filter":"true",
                "data_level":"germplasm"
            },
            {
                "abbrev":"SEED_ID",
                "display_name":"Seed ID",
                "label":"Seed ID",
                "description":"Seed ID",
                "browser_column":"false",
                "visible_browser":"false",
                "visible_export":"false",
                "visible_template":"false",
                "sort":"false",
                "filter":"false",
                "data_level":"seed"
            },
            {
                "abbrev":"PACKAGE_ID",
                "display_name":"Package ID",
                "label":"Package ID",
                "description":"Package ID",
                "browser_column":"false",
                "visible_browser":"false",
                "visible_export":"false",
                "visible_template":"false",
                "sort":"false",
                "filter":"false",
                "data_level":"package"
            },
            {
                "abbrev":"GERMPLASM_ID",
                "display_name":"Germplasm ID",
                "label":"Germplasm ID",
                "description":"Germplasm ID",
                "browser_column":"false",
                "visible_browser":"false",
                "visible_export":"false",
                "visible_template":"false",
                "sort":"false",
                "filter":"false",
                "data_level":"germplasm"
            }
        ]
    }$$
WHERE
    abbrev = 'WORKING_LIST_VARIABLES_CONFIG_MAIZE_DEFAULT';



--rollback UPDATE platform.config
--rollback SET
--rollback     abbrev = 'WORKING_LIST_VARIABLES_CONFIG_MAIZE_DEFAULT',
--rollback     config_value = $${
--rollback  "values":[
--rollback      {
--rollback          "abbrev":"PROGRAM",
--rollback          "display_name":"Program",
--rollback          "label":"Package Program",
--rollback          "description":"Package program",
--rollback          "visible_browser":"true",
--rollback          "visible_export":"true",
--rollback          "visible_template":"true",
--rollback          "sort":"true",
--rollback          "filter":"true",
--rollback          "data_level":"package"
--rollback      },
--rollback      {
--rollback          "abbrev":"SEED_NAME",
--rollback          "display_name":"Seed Name",
--rollback          "label":"Seed Name",
--rollback          "description":"Seed Name",
--rollback          "visible_browser":"true",
--rollback          "visible_export":"true",
--rollback          "visible_template":"true",
--rollback          "sort":"true",
--rollback          "filter":"true",
--rollback          "data_level":"seed"
--rollback      },
--rollback      {
--rollback          "abbrev":"SEED_CODE",
--rollback          "display_name":"Seed Code",
--rollback          "label":"Seed Code",
--rollback          "description":"Seed Code",
--rollback          "visible_browser":"true",
--rollback          "visible_export":"true",
--rollback          "visible_template":"true",
--rollback          "sort":"true",
--rollback          "filter":"true",
--rollback          "data_level":"seed"
--rollback      },
--rollback      {
--rollback          "abbrev":"PACKAGE_CODE",
--rollback          "display_name":"Package Code",
--rollback          "label":"Package Code",
--rollback          "description":"Package Code",
--rollback          "visible_browser":"true",
--rollback          "visible_export":"true",
--rollback          "visible_template":"true",
--rollback          "sort":"true",
--rollback          "filter":"true",
--rollback          "data_level":"package"
--rollback      },
--rollback      {
--rollback          "abbrev":"PACKAGE_LABEL",
--rollback          "display_name":"Package Label",
--rollback          "label":"Package Label",
--rollback          "description":"Package Label",
--rollback          "visible_browser":"true",
--rollback          "visible_export":"true",
--rollback          "visible_template":"true",
--rollback          "sort":"true",
--rollback          "filter":"true",
--rollback          "data_level":"package"
--rollback      },
--rollback      {
--rollback          "abbrev":"GERMPLASM_CODE",
--rollback          "display_name":"Germplasm Code",
--rollback          "label":"Germplasm Code",
--rollback          "description":"Germplasm Code",
--rollback          "visible_browser":"true",
--rollback          "visible_export":"true",
--rollback          "visible_template":"true",
--rollback          "sort":"true",
--rollback          "filter":"true",
--rollback          "data_level":"germplasm"
--rollback      },
--rollback      {
--rollback          "abbrev":"GERMPLASM_NAME",
--rollback          "display_name":"Germplasm Name",
--rollback          "label":"Germplasm Name",
--rollback          "description":"Germplasm Name",
--rollback          "visible_browser":"true",
--rollback          "visible_export":"true",
--rollback          "visible_template":"true",
--rollback          "sort":"true",
--rollback          "filter":"true",
--rollback          "data_level":"germplasm"
--rollback      },
--rollback      {
--rollback          "abbrev":"PARENTAGE",
--rollback          "display_name":"Parentage",
--rollback          "label":"Parentage",
--rollback          "description":"Parentage",
--rollback          "visible_browser":"true",
--rollback          "visible_export":"true",
--rollback          "visible_template":"true",
--rollback          "sort":"true",
--rollback          "filter":"true",
--rollback          "data_level":"germplasm"
--rollback      },
--rollback      {
--rollback          "abbrev":"SEED_ID",
--rollback          "display_name":"Seed ID",
--rollback          "label":"Seed ID",
--rollback          "description":"Seed ID",
--rollback          "visible_browser":"false",
--rollback          "visible_export":"false",
--rollback          "visible_template":"false",
--rollback          "sort":"false",
--rollback          "filter":"false",
--rollback          "data_level":"seed"
--rollback      },
--rollback      {
--rollback          "abbrev":"PACKAGE_ID",
--rollback          "display_name":"Package ID",
--rollback          "label":"Package ID",
--rollback          "description":"Package ID",
--rollback          "visible_browser":"false",
--rollback          "visible_export":"false",
--rollback          "visible_template":"false",
--rollback          "sort":"false",
--rollback          "filter":"false",
--rollback          "data_level":"package"
--rollback      },
--rollback      {
--rollback          "abbrev":"GERMPLASM_ID",
--rollback          "display_name":"Germplasm ID",
--rollback          "label":"Germplasm ID",
--rollback          "description":"Germplasm ID",
--rollback          "visible_browser":"false",
--rollback          "visible_export":"false",
--rollback          "visible_template":"false",
--rollback          "sort":"false",
--rollback          "filter":"false",
--rollback          "data_level":"germplasm"
--rollback      }
--rollback  ]                
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'WORKING_LIST_VARIABLES_CONFIG_MAIZE_DEFAULT';