--liquibase formatted sql

--changeset postgres:add_im_update_seed_config_system_default_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4359 IM DB: Add IM System default UPDATE - SEED config


INSERT INTO
    platform.config(
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES (
    'IM_UPDATE_SEED_SYSTEM_DEFAULT',
    'Inventory Manager configuration for system-default variables - seed update', 
    $${
        "values":[
            {
                "name": "Seed Code",
                "type": "column",
                "usage": "required",
                "abbrev": "SEED_CODE",
                "entity": "seed",
                "view": {
                    "visible": "false",
                    "entities": [ ]
                },
                "required": "true",
                "api_field": "seedDbId",
                "data_type": "string",
                "http_method": "POST",
                "value_filter": "seedCode",
                "skip_update": "true",
                "url_paramters": "limit=1",
                "retrieve_db_id": "true",
                "db_id_api_field": "seedDbId",
                "search_endpoint": "seeds-search",
                "additional_filters": {}
                
            },
            {
                "name": "Seed Name",
                "type": "column",
                "usage": "optional",
                "abbrev": "SEED_NAME",
                "entity": "seed",
                "view": {
                    "visible": "true",
                    "entities": [
                        "seed"
                    ]
                },
                "required": "false",
                "api_field": "",
                "data_type": "string",
                "http_method": "POST",
                "value_filter": "seedName",
                "skip_update": "true",
                "url_paramters": "limit=1",
                "retrieve_db_id": "true",
                "db_id_api_field": "seedDbId",
                "additional_filters": {},
                "search_endpoint": "seeds-search"
            },
            {
                "name": "Program Code",
                "type": "column",
                "usage": "required",
                "abbrev": "PROGRAM_CODE",
                "entity": "seed",
                "view": {
                    "visible": "true",
                    "entities": [
                        "seed"
                    ]
                },
                "required": "false",
                "api_field": "programDbId",
                "data_type": "string",
                "http_method": "POST",
                "value_filter": "programCode",
                "skip_update": "false",
                "retrieve_db_id": "true",
                "url_parameters": "limit=1",
                "db_id_api_field": "programDbId",
                "search_endpoint": "programs-search",
                "additional_filters": {}
            },
            {
                "name": "Harvest Date",
                "type": "column",
                "usage": "optional",
                "abbrev": "HVDATE_CONT",
                "entity": "seed",
                "view": {
                    "visible": "true",
                    "entities": [
                        "seed"
                    ]
                },
                "required": "false",
                "api_field": "harvestDate",
                "data_type": "string",
                "http_method": "",
                "value_filter": "",
                "skip_update": "false",
                "retrieve_db_id": "false",
                "url_parameters": "",
                "db_id_api_field": "",
                "search_endpoint": "",
                "additional_filters": {}
            },
            {
                "name": "Harvest Method",
                "type": "column",
                "usage": "optional",
                "abbrev": "HV_METH_DISC",
                "entity": "seed",
                "view": {
                    "visible": "true",
                    "entities": [
                        "seed"
                    ]
                },
                "required": "false",
                "api_field": "harvestMethod",
                "data_type": "string",
                "http_method": "",
                "value_filter": "",
                "skip_update": "false",
                "retrieve_db_id": "false",
                "url_parameters": "",
                "db_id_api_field": "",
                "search_endpoint": "",
                "additional_filters": {}
            },
            {
                "name": "Source Experiment Code",
                "type": "column",
                "usage": "optional",
                "abbrev": "EXPERIMENT_CODE",
                "entity": "seed",
                "view": {
                    "visible": "true",
                    "entities": [
                        "seed"
                    ]
                },
                "required": "false",
                "api_field": "sourceExperimentDbId",
                "data_type": "string",
                "http_method": "POST",
                "value_filter": "experimentCode",
                "skip_update": "false",
                "retrieve_db_id": "true",
                "url_parameters": "limit=1",
                "db_id_api_field": "experimentDbId",
                "search_endpoint": "experiments-search",
                "additional_filters": {}
            },
            {
                "name": "Description",
                "type": "column",
                "usage": "optional",
                "abbrev": "DESCRIPTION",
                "entity": "seed",
                "view": {
                    "visible": "true",
                    "entities": [
                        "seed"
                    ]
                },
                "required": "false",
                "api_field": "description",
                "data_type": "string",
                "http_method": "",
                "value_filter": "",
                "skip_update": "false",
                "retrieve_db_id": "false",
                "url_parameters": "",
                "db_id_api_field": "",
                "search_endpoint": "",
                "additional_filters": {}
            }
        ]
    }$$,
    1,
    'Inventory Manager file upload and data validation',
    1,
    'added by j.bantay'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'IM_UPDATE_SEED_SYSTEM_DEFAULT';
