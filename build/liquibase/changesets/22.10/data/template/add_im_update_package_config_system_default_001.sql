--liquibase formatted sql

--changeset postgres:add_im_update_package_config_system_default_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4359 IM DB: Add IM System default UPDATE - PACKAGE config


INSERT INTO
    platform.config(
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES (
    'IM_UPDATE_PACKAGE_SYSTEM_DEFAULT',
    'Inventory Manager configuration for system-default variables - package update', 
    $${
        "values":[
            {
                "name": "Package Code",
                "type": "column",
                "usage": "required",
                "abbrev": "PACKAGE_CODE",
                "entity": "package",
                "view": {
                    "visible": "false",
                    "entities": [ ]
                },
                "required": "true",
                "api_field": "packageDbId",
                "data_type": "string",
                "http_method": "POST",
                "value_filter": "packageCode",
                "skip_update": "true",
                "url_paramters": "dataLevel=all&limit=1",
                "retrieve_db_id": "true",
                "db_id_api_field": "packageDbId",
                "search_endpoint": "seeds-packages-search",
                "additional_filters": {}
                
            },
            {
                "name": "Package Label",
                "type": "column",
                "usage": "optional",
                "abbrev": "PACKAGE_LABEL",
                "entity": "package",
                "view": {
                    "visible": "true",
                    "entities": [
                        "package"
                    ]
                },
                "required": "false",
                "api_field": "packageDbId",
                "data_type": "string",
                "http_method": "POST",
                "value_filter": "packageCode",
                "skip_update": "true",
                "url_paramters": "dataLevel=all&limit=1",
                "retrieve_db_id": "true",
                "db_id_api_field": "packageDbId",
                "search_endpoint": "seeds-packages-search",
                "additional_filters": {}
            },
            {
                "name": "Program",
                "type": "column",
                "usage": "required",
                "abbrev": "PROGRAM",
                "entity": "package",
                "view": {
                    "visible": "true",
                    "entities": [
                        "package"
                    ]
                },
                "required": "false",
                "api_field": "programDbId",
                "data_type": "string",
                "http_method": "POST",
                "value_filter": "programCode",
                "skip_update": "false",
                "retrieve_db_id": "true",
                "url_parameters": "limit=1",
                "db_id_api_field": "programDbId",
                "search_endpoint": "programs-search",
                "additional_filters": {}
            },
            {
                "name": "Package Status",
                "type": "column",
                "usage": "required",
                "abbrev": "PACKAGE_STATUS",
                "entity": "package",
                "view": {
                    "visible": "true",
                    "entities": [
                        "package"
                    ]
                },
                "required": "false",
                "api_field": "packageStatus",
                "data_type": "string",
                "http_method": "",
                "value_filter": "",
                "skip_update": "false",
                "retrieve_db_id": "false",
                "url_parameters": "",
                "db_id_api_field": "",
                "search_endpoint": "",
                "additional_filters": {}
            },
            {
                "name": "Package Quantity",
                "type": "column",
                "usage": "required",
                "abbrev": "VOLUME",
                "entity": "package",
                "view": {
                    "visible": "true",
                    "entities": [
                        "package"
                    ]
                },
                "required": "false",
                "api_field": "packageQuantity",
                "data_type": "float",
                "http_method": "",
                "value_filter": "",
                "skip_update": "false",
                "retrieve_db_id": "false",
                "url_parameters": "",
                "db_id_api_field": "",
                "search_endpoint": "",
                "additional_filters": {}
            },
            {
                "name": "Facility Code",
                "type": "column",
                "usage": "optional",
                "abbrev": "FACILITY_CODE",
                "entity": "package",
                "view": {
                    "visible": "true",
                    "entities": [
                        "package"
                    ]
                },
                "required": "false",
                "api_field": "facilityDbId",
                "data_type": "string",
                "http_method": "POST",
                "value_filter": "facilityCode",
                "skip_update": "false",
                "retrieve_db_id": "true",
                "url_parameters": "limit=1",
                "db_id_api_field": "facilityDbId",
                "search_endpoint": "facilities-search",
                "additional_filters": {}
            }
        ]
    }$$,
    1,
    'Inventory Manager file upload and data validation',
    1,
    'added by j.bantay'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'IM_UPDATE_PACKAGE_SYSTEM_DEFAULT';
