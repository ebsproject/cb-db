--liquibase formatted sql

--changeset author_name:update_planting_job_status_to_draft context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1483 Update planting_job_status to draft



UPDATE
    experiment.planting_job
SET
    planting_job_status = 'draft'
;



-- revert changes
--rollback SELECT NULL;