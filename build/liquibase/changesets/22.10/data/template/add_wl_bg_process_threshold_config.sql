--liquibase formatted sql

--changeset postgres:add_wl_bg_process_threshold_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4328 SS: Working List - Create threshold configuration record for Working List



INSERT INTO
    platform.config(
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES (
    'WORKING_LIST_BG_PROCESSING_THRESHOLD',
    'Background processing threshold values for WORKING LIST tool features', 
    '{
        "addListItems": {
            "size": "500",
            "description": "Threshold value when adding list items to the working list"
        },
        "saveList": {
            "size": "10000",
            "description": "Threshold value when saving a list"
        },
        "deleteWorkingList": {
            "size": "1000",
            "description": "Threshold value when deleting list items"
        },
        "exportWorkingList": {
            "size": "1000",
            "description": "Threshold value when exporting list items"
        },
        "updateListItems": {
            "size": "500",
            "description": "Threshold value when updating working list items"
        }
    }',
    1,
    'working_list_bg_process',
    1,
    'added by k.delarosa'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'WORKING_LIST_BG_PROCESSING_THRESHOLD';