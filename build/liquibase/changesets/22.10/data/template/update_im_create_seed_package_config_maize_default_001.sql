--liquibase formatted sql

--changeset postgres:update_im_create_seed_package_config_maize_default_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4358 IM DB: Modify abbrev and visibility attribute of IM Maize default CREATE - SEED_PACKAGE config



-- update config
UPDATE platform.config
SET
    abbrev = 'IM_CREATE_SEED_PACKAGE_MAIZE_DEFAULT',
    config_value = $${
        "values": [
            {
                "name": "Program Code",
                "type": "column",
                "usage": "required",
                "abbrev": "PROGRAM_CODE",
                "entity": "seed",
                "view": {
                    "visible": "true",
                    "entities": [
                        "seed"
                    ]
                },
                "required": "true",
                "api_field": "programDbId",
                "data_type": "string",
                "http_method": "POST",
                "value_filter": "programCode",
                "skip_creation": "false",
                "retrieve_db_id": "true",
                "url_parameters": "limit=1",
                "db_id_api_field": "programDbId",
                "search_endpoint": "programs-search",
                "additional_filters": {}
            },
            {
                "name": "Harvest Date",
                "type": "column",
                "usage": "optional",
                "abbrev": "HVDATE_CONT",
                "entity": "seed",
                "view": {
                    "visible": "true",
                    "entities": [
                        "seed"
                    ]
                },
                "required": "false",
                "api_field": "harvestDate",
                "data_type": "string",
                "http_method": "",
                "value_filter": "",
                "skip_creation": "false",
                "retrieve_db_id": "false",
                "url_parameters": "",
                "db_id_api_field": "",
                "search_endpoint": "",
                "additional_filters": {}
            },
            {
                "name": "Harvest Method",
                "type": "column",
                "usage": "optional",
                "abbrev": "HV_METH_DISC",
                "entity": "seed",
                "view": {
                    "visible": "true",
                    "entities": [
                        "seed"
                    ]
                },
                "required": "false",
                "api_field": "harvestMethod",
                "data_type": "string",
                "http_method": "",
                "value_filter": "",
                "skip_creation": "false",
                "retrieve_db_id": "false",
                "url_parameters": "",
                "db_id_api_field": "",
                "search_endpoint": "",
                "additional_filters": {}
            },
            {
                "name": "Source Experiment Code",
                "type": "column",
                "usage": "optional",
                "abbrev": "EXPERIMENT_CODE",
                "entity": "seed",
                "view": {
                    "visible": "true",
                    "entities": [
                        "seed"
                    ]
                },
                "required": "false",
                "api_field": "sourceExperimentDbId",
                "data_type": "string",
                "http_method": "POST",
                "value_filter": "experimentCode",
                "skip_creation": "false",
                "retrieve_db_id": "true",
                "url_parameters": "limit=1",
                "db_id_api_field": "experimentDbId",
                "search_endpoint": "experiments-search",
                "additional_filters": {}
            },
            {
                "name": "Description",
                "type": "column",
                "usage": "optional",
                "abbrev": "DESCRIPTION",
                "entity": "seed",
                "view": {
                    "visible": "true",
                    "entities": [
                        "seed"
                    ]
                },
                "required": "false",
                "api_field": "description",
                "data_type": "string",
                "http_method": "",
                "value_filter": "",
                "skip_creation": "false",
                "retrieve_db_id": "false",
                "url_parameters": "",
                "db_id_api_field": "",
                "search_endpoint": "",
                "additional_filters": {}
            },
            {
                "name": "Package Quantity",
                "type": "column",
                "usage": "required",
                "abbrev": "VOLUME",
                "entity": "package",
                "view": {
                    "visible": "true",
                    "entities": [
                        "package"
                    ]
                },
                "required": "true",
                "api_field": "packageQuantity",
                "data_type": "float",
                "http_method": "",
                "value_filter": "",
                "skip_creation": "false",
                "retrieve_db_id": "false",
                "url_parameters": "",
                "db_id_api_field": "",
                "search_endpoint": "",
                "additional_filters": {}
            },
            {
                "name": "Package Unit",
                "type": "column",
                "usage": "required",
                "abbrev": "PACKAGE_UNIT",
                "entity": "package",
                "view": {
                    "visible": "true",
                    "entities": [
                        "package"
                    ]
                },
                "required": "true",
                "api_field": "packageUnit",
                "data_type": "string",
                "http_method": "",
                "value_filter": "",
                "skip_creation": "false",
                "retrieve_db_id": "false",
                "url_parameters": "",
                "db_id_api_field": "",
                "search_endpoint": "",
                "additional_filters": {}
            },
            {
                "name": "Facility Code",
                "type": "column",
                "usage": "optional",
                "abbrev": "FACILITY_CODE",
                "entity": "package",
                "view": {
                    "visible": "true",
                    "entities": [
                        "package"
                    ]
                },
                "required": "false",
                "api_field": "facilityDbId",
                "data_type": "string",
                "http_method": "POST",
                "value_filter": "facilityCode",
                "skip_creation": "false",
                "retrieve_db_id": "true",
                "url_parameters": "limit=1",
                "db_id_api_field": "facilityDbId",
                "search_endpoint": "facilities-search",
                "additional_filters": {}
            }
        ]
    }$$
WHERE
    abbrev = 'IM_FILE_UPLOAD_CONFIG_SEED_PACKAGE_MAIZE_DEFAULT';



--rollback UPDATE platform.config
--rollback SET
--rollback     abbrev = 'IM_FILE_UPLOAD_CONFIG_SEED_PACKAGE_MAIZE_DEFAULT',
--rollback     config_value = $${
--rollback         "values": [
--rollback             {
--rollback                 "name": "Program Code",
--rollback                 "type": "column",
--rollback                 "usage": "required",
--rollback                 "abbrev": "PROGRAM_CODE",
--rollback                 "entity": "seed",
--rollback                 "visible": "true",
--rollback                 "required": "true",
--rollback                 "api_field": "programDbId",
--rollback                 "data_type": "string",
--rollback                 "http_method": "POST",
--rollback                 "value_filter": "programCode",
--rollback                 "skip_creation": "false",
--rollback                 "retrieve_db_id": "true",
--rollback                 "url_parameters": "limit=1",
--rollback                 "db_id_api_field": "programDbId",
--rollback                 "search_endpoint": "programs-search",
--rollback                 "additional_filters": {}
--rollback             },
--rollback             {
--rollback                 "name": "Harvest Date",
--rollback                 "type": "column",
--rollback                 "usage": "optional",
--rollback                 "abbrev": "HVDATE_CONT",
--rollback                 "entity": "seed",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "api_field": "harvestDate",
--rollback                 "data_type": "string",
--rollback                 "http_method": "",
--rollback                 "value_filter": "",
--rollback                 "skip_creation": "false",
--rollback                 "retrieve_db_id": "false",
--rollback                 "url_parameters": "",
--rollback                 "db_id_api_field": "",
--rollback                 "search_endpoint": "",
--rollback                 "additional_filters": {}
--rollback             },
--rollback             {
--rollback                 "name": "Harvest Method",
--rollback                 "type": "column",
--rollback                 "usage": "optional",
--rollback                 "abbrev": "HV_METH_DISC",
--rollback                 "entity": "seed",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "api_field": "harvestMethod",
--rollback                 "data_type": "string",
--rollback                 "http_method": "",
--rollback                 "value_filter": "",
--rollback                 "skip_creation": "false",
--rollback                 "retrieve_db_id": "false",
--rollback                 "url_parameters": "",
--rollback                 "db_id_api_field": "",
--rollback                 "search_endpoint": "",
--rollback                 "additional_filters": {}
--rollback             },
--rollback             {
--rollback                 "name": "Source Experiment Code",
--rollback                 "type": "column",
--rollback                 "usage": "optional",
--rollback                 "abbrev": "EXPERIMENT_CODE",
--rollback                 "entity": "seed",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "api_field": "sourceExperimentDbId",
--rollback                 "data_type": "string",
--rollback                 "http_method": "POST",
--rollback                 "value_filter": "experimentCode",
--rollback                 "skip_creation": "false",
--rollback                 "retrieve_db_id": "true",
--rollback                 "url_parameters": "limit=1",
--rollback                 "db_id_api_field": "experimentDbId",
--rollback                 "search_endpoint": "experiments-search",
--rollback                 "additional_filters": {}
--rollback             },
--rollback             {
--rollback                 "name": "Description",
--rollback                 "type": "column",
--rollback                 "usage": "optional",
--rollback                 "abbrev": "DESCRIPTION",
--rollback                 "entity": "seed",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "api_field": "description",
--rollback                 "data_type": "string",
--rollback                 "http_method": "",
--rollback                 "value_filter": "",
--rollback                 "skip_creation": "false",
--rollback                 "retrieve_db_id": "false",
--rollback                 "url_parameters": "",
--rollback                 "db_id_api_field": "",
--rollback                 "search_endpoint": "",
--rollback                 "additional_filters": {}
--rollback             },
--rollback             {
--rollback                 "name": "Package Quantity",
--rollback                 "type": "column",
--rollback                 "usage": "required",
--rollback                 "abbrev": "VOLUME",
--rollback                 "entity": "package",
--rollback                 "visible": "true",
--rollback                 "required": "true",
--rollback                 "api_field": "packageQuantity",
--rollback                 "data_type": "float",
--rollback                 "http_method": "",
--rollback                 "value_filter": "",
--rollback                 "skip_creation": "false",
--rollback                 "retrieve_db_id": "false",
--rollback                 "url_parameters": "",
--rollback                 "db_id_api_field": "",
--rollback                 "search_endpoint": "",
--rollback                 "additional_filters": {}
--rollback             },
--rollback             {
--rollback                 "name": "Package Unit",
--rollback                 "type": "column",
--rollback                 "usage": "required",
--rollback                 "abbrev": "PACKAGE_UNIT",
--rollback                 "entity": "package",
--rollback                 "visible": "true",
--rollback                 "required": "true",
--rollback                 "api_field": "packageUnit",
--rollback                 "data_type": "string",
--rollback                 "http_method": "",
--rollback                 "value_filter": "",
--rollback                 "skip_creation": "false",
--rollback                 "retrieve_db_id": "false",
--rollback                 "url_parameters": "",
--rollback                 "db_id_api_field": "",
--rollback                 "search_endpoint": "",
--rollback                 "additional_filters": {}
--rollback             },
--rollback             {
--rollback                 "name": "Facility Code",
--rollback                 "type": "column",
--rollback                 "usage": "optional",
--rollback                 "abbrev": "FACILITY_CODE",
--rollback                 "entity": "package",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "api_field": "facilityDbId",
--rollback                 "data_type": "string",
--rollback                 "http_method": "POST",
--rollback                 "value_filter": "facilityCode",
--rollback                 "skip_creation": "false",
--rollback                 "retrieve_db_id": "true",
--rollback                 "url_parameters": "limit=1",
--rollback                 "db_id_api_field": "facilityDbId",
--rollback                 "search_endpoint": "facilities-search",
--rollback                 "additional_filters": {}
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'IM_CREATE_SEED_PACKAGE_MAIZE_DEFAULT';