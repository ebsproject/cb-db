--liquibase formatted sql

--changeset postgres:add_wl_variable_config_system_default context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4304 SS: Working List - Create new variable configuration records



INSERT INTO
    platform.config(
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES (
    'WORKING_LIST_VARIABLES_CONFIG_SYSTEM_DEFAULT',
    'Working List default system variable configuration', 
    $${
        "values":[
            {
                "abbrev":"PROGRAM",
                "display_name":"Program",
                "label":"Package Program",
                "description":"Package program",
                "browser_column":"true",
                "visible_browser":"true",
                "visible_export":"true",
                "visible_template":"true",
                "sort":"true",
                "filter":"true",
                "data_level":"package"
            },
            {
                "abbrev":"SEED_NAME",
                "display_name":"Seed Name",
                "label":"Seed Name",
                "description":"Seed Name",
                "browser_column":"true",
                "visible_browser":"true",
                "visible_export":"true",
                "visible_template":"true",
                "sort":"true",
                "filter":"true",
                "data_level":"seed"
            },
            {
                "abbrev":"SEED_CODE",
                "display_name":"Seed Code",
                "label":"Seed Code",
                "description":"Seed Code",
                "browser_column":"true",
                "visible_browser":"true",
                "visible_export":"true",
                "visible_template":"true",
                "sort":"true",
                "filter":"true",
                "data_level":"seed"
            },
            {
                "abbrev":"PACKAGE_CODE",
                "display_name":"Package Code",
                "label":"Package Code",
                "description":"Package Code",
                "browser_column":"true",
                "visible_browser":"true",
                "visible_export":"true",
                "visible_template":"true",
                "sort":"true",
                "filter":"true",
                "data_level":"package"
            },
            {
                "abbrev":"PACKAGE_LABEL",
                "display_name":"Package Label",
                "label":"Package Label",
                "description":"Package Label",
                "browser_column":"true",
                "visible_browser":"true",
                "visible_export":"true",
                "visible_template":"true",
                "sort":"true",
                "filter":"true",
                "data_level":"package"
            },
            {
                "abbrev":"GERMPLASM_CODE",
                "display_name":"Germplasm Code",
                "label":"Germplasm Code",
                "description":"Germplasm Code",
                "browser_column":"true",
                "visible_browser":"true",
                "visible_export":"true",
                "visible_template":"true",
                "sort":"true",
                "filter":"true",
                "data_level":"germplasm"
            },
            {
                "abbrev":"GERMPLASM_NAME",
                "display_name":"Germplasm Name",
                "label":"Germplasm Name",
                "description":"Germplasm Name",
                "browser_column":"true",
                "visible_browser":"true",
                "visible_export":"true",
                "visible_template":"true",
                "sort":"true",
                "filter":"true",
                "data_level":"germplasm"
            },
            {
                "abbrev":"PARENTAGE",
                "display_name":"Parentage",
                "label":"Parentage",
                "description":"Parentage",
                "browser_column":"true",
                "visible_browser":"true",
                "visible_export":"true",
                "visible_template":"true",
                "sort":"true",
                "filter":"true",
                "data_level":"germplasm"
            },
            {
                "abbrev":"SEED_ID",
                "display_name":"Seed ID",
                "label":"Seed ID",
                "description":"Seed ID",
                "browser_column":"false",
                "visible_browser":"false",
                "visible_export":"false",
                "visible_template":"false",
                "sort":"false",
                "filter":"false",
                "data_level":"seed"
            },
            {
                "abbrev":"PACKAGE_ID",
                "display_name":"Package ID",
                "label":"Package ID",
                "description":"Package ID",
                "browser_column":"false",
                "visible_browser":"false",
                "visible_export":"false",
                "visible_template":"false",
                "sort":"false",
                "filter":"false",
                "data_level":"package"
            },
            {
                "abbrev":"GERMPLASM_ID",
                "display_name":"Germplasm ID",
                "label":"Germplasm ID",
                "description":"Germplasm ID",
                "browser_column":"false",
                "visible_browser":"false",
                "visible_export":"false",
                "visible_template":"false",
                "sort":"false",
                "filter":"false",
                "data_level":"germplasm"
            }
        ]
    }$$,
    1,
    'Working List browser and export file variable configuration',
    1,
    'added by k.delarosa'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'WORKING_LIST_VARIABLES_CONFIG_SYSTEM_DEFAULT';