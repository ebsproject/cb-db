--liquibase formatted sql

--changeset postgres:update_planting_instructions_manager_threshold_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4399 PIM DB: Add threshold config for triggering worker in completing a Packing Job



UPDATE
    platform.config
SET
    config_value = '{
    "updateEntries": {
        "size": "1000",
        "description": "Update entry records"
    },
    "completePackingJob": {
        "size": 100,
        "description": "Threshold for completing packing job. Triggered when <size> is met."
    }
}'
WHERE
    abbrev = 'PLANTING_INSTRUCTIONS_MANAGER_BG_PROCESSING_THRESHOLD'
;



--rollback  UPDATE
--rollback  	platform.config
--rollback  SET
--rollback  	config_value = '{
--rollback      "updateEntries": {
--rollback          "size": "1000",
--rollback          "description": "Update entry records"
--rollback      }
--rollback  }'
--rollback  WHERE
--rollback  	abbrev = 'PLANTING_INSTRUCTIONS_MANAGER_BG_PROCESSING_THRESHOLD';