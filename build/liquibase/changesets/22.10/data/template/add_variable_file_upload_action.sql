--liquibase formatted sql

--changeset author_name:add_variable_file_upload_action context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM master.variable WHERE abbrev = 'FILE_UPLOAD_ACTION') WHEN TRUE THEN 1 ELSE 0 END;
--comment: DB-1491 Add variable FILE_UPLOAD_ACTION



-- create variable if not existing, else skip this changeset (use precondition to check if variable exists)
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name) 
    VALUES 
        ('FILE_UPLOAD_ACTION', 'FILE UPLOAD ACTION', 'FILE UPLOAD ACTION', 'character varying', true, 'metadata', NULL, 'application', NULL, 'active', '1', 'FILE UPLOAD ACTION')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'FILE_UPLOAD_ACTION' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('FILE_UPLOAD_ACTION', 'FILE UPLOAD ACTION', 'FILE UPLOAD ACTION') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('FILE_UPLOAD_ACTION_METHOD', 'FILE UPLOAD ACTION method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('FILE_UPLOAD_ACTION_SCALE', 'FILE UPLOAD ACTION scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'FILE_UPLOAD_ACTION'
--rollback     AND t.scale_id = var.scale_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.scale AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'FILE_UPLOAD_ACTION'
--rollback     AND t.id = var.scale_id
--rollback ;
--rollback DELETE FROM
--rollback     master.property AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'FILE_UPLOAD_ACTION'
--rollback     AND t.id = var.property_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.method AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'FILE_UPLOAD_ACTION'
--rollback     AND t.id = var.method_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.variable AS t
--rollback WHERE
--rollback     t.abbrev = 'FILE_UPLOAD_ACTION'
--rollback ;