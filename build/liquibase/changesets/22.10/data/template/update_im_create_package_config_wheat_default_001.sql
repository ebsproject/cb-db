--liquibase formatted sql

--changeset postgres:update_im_create_package_config_wheat_default_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4358 IM DB: Modify abbrev and visibility attribute of IM Wheat default CREATE - PACKAGE config



-- update config
UPDATE platform.config
SET
    abbrev = 'IM_CREATE_PACKAGE_WHEAT_DEFAULT',
    config_value = $${
        "values": [
            {
                "name": "Package Quantity",
                "type": "column",
                "usage": "required",
                "abbrev": "VOLUME",
                "entity": "package",
                "view": {
                    "visible": "true",
                    "entities": [
                        "package"
                    ]
                },
                "required": "true",
                "api_field": "packageQuantity",
                "data_type": "float",
                "http_method": "",
                "value_filter": "",
                "skip_creation": "false",
                "retrieve_db_id": "false",
                "url_parameters": "",
                "db_id_api_field": "",
                "search_endpoint": "",
                "additional_filters": {}
            },
            {
                "name": "Package Unit",
                "type": "column",
                "usage": "required",
                "abbrev": "PACKAGE_UNIT",
                "entity": "package",
                "view": {
                    "visible": "true",
                    "entities": [
                        "package"
                    ]
                },
                "required": "true",
                "api_field": "packageUnit",
                "data_type": "string",
                "http_method": "",
                "value_filter": "",
                "skip_creation": "false",
                "retrieve_db_id": "false",
                "url_parameters": "",
                "db_id_api_field": "",
                "search_endpoint": "",
                "additional_filters": {}
            },
            {
                "name": "Facility Code",
                "type": "column",
                "usage": "optional",
                "abbrev": "FACILITY_CODE",
                "entity": "package",
                "view": {
                    "visible": "true",
                    "entities": [
                        "package"
                    ]
                },
                "required": "false",
                "api_field": "facilityDbId",
                "data_type": "string",
                "http_method": "POST",
                "value_filter": "facilityCode",
                "skip_creation": "false",
                "retrieve_db_id": "true",
                "url_parameters": "limit=1",
                "db_id_api_field": "facilityDbId",
                "search_endpoint": "facilities-search",
                "additional_filters": {}
            }
        ]
    }$$
WHERE
    abbrev = 'IM_FILE_UPLOAD_CONFIG_PACKAGE_WHEAT_DEFAULT';



--rollback UPDATE platform.config
--rollback SET
--rollback     abbrev = 'IM_FILE_UPLOAD_CONFIG_PACKAGE_WHEAT_DEFAULT',
--rollback     config_value = $${
--rollback         "values": [
--rollback             {
--rollback                 "name": "Package Quantity",
--rollback                 "type": "column",
--rollback                 "usage": "required",
--rollback                 "abbrev": "VOLUME",
--rollback                 "entity": "package",
--rollback                 "visible": "true",
--rollback                 "required": "true",
--rollback                 "api_field": "packageQuantity",
--rollback                 "data_type": "float",
--rollback                 "http_method": "",
--rollback                 "value_filter": "",
--rollback                 "skip_creation": "false",
--rollback                 "retrieve_db_id": "false",
--rollback                 "url_parameters": "",
--rollback                 "db_id_api_field": "",
--rollback                 "search_endpoint": "",
--rollback                 "additional_filters": {}
--rollback             },
--rollback             {
--rollback                 "name": "Package Unit",
--rollback                 "type": "column",
--rollback                 "usage": "required",
--rollback                 "abbrev": "PACKAGE_UNIT",
--rollback                 "entity": "package",
--rollback                 "visible": "true",
--rollback                 "required": "true",
--rollback                 "api_field": "packageUnit",
--rollback                 "data_type": "string",
--rollback                 "http_method": "",
--rollback                 "value_filter": "",
--rollback                 "skip_creation": "false",
--rollback                 "retrieve_db_id": "false",
--rollback                 "url_parameters": "",
--rollback                 "db_id_api_field": "",
--rollback                 "search_endpoint": "",
--rollback                 "additional_filters": {}
--rollback             },
--rollback             {
--rollback                 "name": "Facility Code",
--rollback                 "type": "column",
--rollback                 "usage": "optional",
--rollback                 "abbrev": "FACILITY_CODE",
--rollback                 "entity": "package",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "api_field": "facilityDbId",
--rollback                 "data_type": "string",
--rollback                 "http_method": "POST",
--rollback                 "value_filter": "facilityCode",
--rollback                 "skip_creation": "false",
--rollback                 "retrieve_db_id": "true",
--rollback                 "url_parameters": "limit=1",
--rollback                 "db_id_api_field": "facilityDbId",
--rollback                 "search_endpoint": "facilities-search",
--rollback                 "additional_filters": {}
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'IM_CREATE_PACKAGE_WHEAT_DEFAULT';