--liquibase formatted sql

--changeset postgres:update_im_update_configs_fix_typos_002 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4367 IM DB: Fix typo: change packageCode to packageLabel in package label config



-- update package config
UPDATE platform.config
SET
    config_value = $${
        "values": [
            {
                "name": "Package Code",
                "type": "column",
                "view": {
                    "visible": "false",
                    "entities": []
                },
                "usage": "required",
                "abbrev": "PACKAGE_CODE",
                "entity": "package",
                "required": "true",
                "api_field": "packageDbId",
                "data_type": "string",
                "http_method": "POST",
                "skip_update": "false",
                "value_filter": "packageCode",
                "url_parameters": "dataLevel=all&limit=1",
                "retrieve_db_id": "true",
                "db_id_api_field": "packageDbId",
                "search_endpoint": "seed-packages-search",
                "additional_filters": {}
            },
            {
                "name": "Package Label",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "package"
                    ]
                },
                "usage": "optional",
                "abbrev": "PACKAGE_LABEL",
                "entity": "package",
                "required": "false",
                "api_field": "packageDbId",
                "data_type": "string",
                "http_method": "POST",
                "skip_update": "true",
                "value_filter": "packageLabel",
                "url_parameters": "dataLevel=all&limit=1",
                "retrieve_db_id": "true",
                "db_id_api_field": "packageDbId",
                "search_endpoint": "seed-packages-search",
                "additional_filters": {}
            },
            {
                "name": "Program",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "package"
                    ]
                },
                "usage": "required",
                "abbrev": "PROGRAM",
                "entity": "package",
                "required": "false",
                "api_field": "programDbId",
                "data_type": "string",
                "http_method": "POST",
                "skip_update": "false",
                "value_filter": "programCode",
                "retrieve_db_id": "true",
                "url_parameters": "limit=1",
                "db_id_api_field": "programDbId",
                "search_endpoint": "programs-search",
                "additional_filters": {}
            },
            {
                "name": "Package Status",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "package"
                    ]
                },
                "usage": "required",
                "abbrev": "PACKAGE_STATUS",
                "entity": "package",
                "required": "false",
                "api_field": "packageStatus",
                "data_type": "string",
                "http_method": "",
                "skip_update": "false",
                "value_filter": "",
                "retrieve_db_id": "false",
                "url_parameters": "",
                "db_id_api_field": "",
                "search_endpoint": "",
                "additional_filters": {}
            },
            {
                "name": "Package Quantity",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "package"
                    ]
                },
                "usage": "required",
                "abbrev": "VOLUME",
                "entity": "package",
                "required": "false",
                "api_field": "packageQuantity",
                "data_type": "float",
                "http_method": "",
                "skip_update": "false",
                "value_filter": "",
                "retrieve_db_id": "false",
                "url_parameters": "",
                "db_id_api_field": "",
                "search_endpoint": "",
                "additional_filters": {}
            },
            {
                "name": "Facility Code",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "package"
                    ]
                },
                "usage": "optional",
                "abbrev": "FACILITY_CODE",
                "entity": "package",
                "required": "false",
                "api_field": "facilityDbId",
                "data_type": "string",
                "http_method": "POST",
                "skip_update": "false",
                "value_filter": "facilityCode",
                "retrieve_db_id": "true",
                "url_parameters": "limit=1",
                "db_id_api_field": "facilityDbId",
                "search_endpoint": "facilities-search",
                "additional_filters": {}
            }
        ]
        }$$
WHERE
    abbrev = 'IM_UPDATE_PACKAGE_SYSTEM_DEFAULT';



--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "values": [
--rollback             {
--rollback                 "name": "Package Code",
--rollback                 "type": "column",
--rollback                 "view": {
--rollback                     "visible": "false",
--rollback                     "entities": []
--rollback                 },
--rollback                 "usage": "required",
--rollback                 "abbrev": "PACKAGE_CODE",
--rollback                 "entity": "package",
--rollback                 "required": "true",
--rollback                 "api_field": "packageDbId",
--rollback                 "data_type": "string",
--rollback                 "http_method": "POST",
--rollback                 "skip_update": "false",
--rollback                 "value_filter": "packageCode",
--rollback                 "url_parameters": "dataLevel=all&limit=1",
--rollback                 "retrieve_db_id": "true",
--rollback                 "db_id_api_field": "packageDbId",
--rollback                 "search_endpoint": "seed-packages-search",
--rollback                 "additional_filters": {}
--rollback             },
--rollback             {
--rollback                 "name": "Package Label",
--rollback                 "type": "column",
--rollback                 "view": {
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                     "package"
--rollback                     ]
--rollback                 },
--rollback                 "usage": "optional",
--rollback                 "abbrev": "PACKAGE_LABEL",
--rollback                 "entity": "package",
--rollback                 "required": "false",
--rollback                 "api_field": "packageDbId",
--rollback                 "data_type": "string",
--rollback                 "http_method": "POST",
--rollback                 "skip_update": "true",
--rollback                 "value_filter": "packageCode",
--rollback                 "url_parameters": "dataLevel=all&limit=1",
--rollback                 "retrieve_db_id": "true",
--rollback                 "db_id_api_field": "packageDbId",
--rollback                 "search_endpoint": "seed-packages-search",
--rollback                 "additional_filters": {}
--rollback             },
--rollback             {
--rollback                 "name": "Program",
--rollback                 "type": "column",
--rollback                 "view": {
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                     "package"
--rollback                     ]
--rollback                 },
--rollback                 "usage": "required",
--rollback                 "abbrev": "PROGRAM",
--rollback                 "entity": "package",
--rollback                 "required": "false",
--rollback                 "api_field": "programDbId",
--rollback                 "data_type": "string",
--rollback                 "http_method": "POST",
--rollback                 "skip_update": "false",
--rollback                 "value_filter": "programCode",
--rollback                 "retrieve_db_id": "true",
--rollback                 "url_parameters": "limit=1",
--rollback                 "db_id_api_field": "programDbId",
--rollback                 "search_endpoint": "programs-search",
--rollback                 "additional_filters": {}
--rollback             },
--rollback             {
--rollback                 "name": "Package Status",
--rollback                 "type": "column",
--rollback                 "view": {
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                     "package"
--rollback                     ]
--rollback                 },
--rollback                 "usage": "required",
--rollback                 "abbrev": "PACKAGE_STATUS",
--rollback                 "entity": "package",
--rollback                 "required": "false",
--rollback                 "api_field": "packageStatus",
--rollback                 "data_type": "string",
--rollback                 "http_method": "",
--rollback                 "skip_update": "false",
--rollback                 "value_filter": "",
--rollback                 "retrieve_db_id": "false",
--rollback                 "url_parameters": "",
--rollback                 "db_id_api_field": "",
--rollback                 "search_endpoint": "",
--rollback                 "additional_filters": {}
--rollback             },
--rollback             {
--rollback                 "name": "Package Quantity",
--rollback                 "type": "column",
--rollback                 "view": {
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                     "package"
--rollback                     ]
--rollback                 },
--rollback                 "usage": "required",
--rollback                 "abbrev": "VOLUME",
--rollback                 "entity": "package",
--rollback                 "required": "false",
--rollback                 "api_field": "packageQuantity",
--rollback                 "data_type": "float",
--rollback                 "http_method": "",
--rollback                 "skip_update": "false",
--rollback                 "value_filter": "",
--rollback                 "retrieve_db_id": "false",
--rollback                 "url_parameters": "",
--rollback                 "db_id_api_field": "",
--rollback                 "search_endpoint": "",
--rollback                 "additional_filters": {}
--rollback             },
--rollback             {
--rollback                 "name": "Facility Code",
--rollback                 "type": "column",
--rollback                 "view": {
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                     "package"
--rollback                     ]
--rollback                 },
--rollback                 "usage": "optional",
--rollback                 "abbrev": "FACILITY_CODE",
--rollback                 "entity": "package",
--rollback                 "required": "false",
--rollback                 "api_field": "facilityDbId",
--rollback                 "data_type": "string",
--rollback                 "http_method": "POST",
--rollback                 "skip_update": "false",
--rollback                 "value_filter": "facilityCode",
--rollback                 "retrieve_db_id": "true",
--rollback                 "url_parameters": "limit=1",
--rollback                 "db_id_api_field": "facilityDbId",
--rollback                 "search_endpoint": "facilities-search",
--rollback                 "additional_filters": {}
--rollback             }
--rollback         ]
--rollback         }$$
--rollback WHERE
--rollback     abbrev = 'IM_UPDATE_PACKAGE_SYSTEM_DEFAULT';