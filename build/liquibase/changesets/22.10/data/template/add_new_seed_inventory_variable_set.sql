--liquibase formatted sql

--changeset author_name:add_new_seed_inventory_variable_set context:template  splitStatements:false rollbackSplitStatements:false
--comment: DB-1497 Add new seed inventory variable set



INSERT INTO 
    master.variable_set 
        (abbrev, name, description, display_name, creator_id)
SELECT
    t.abbrev,
    t.name,
    t.description,
    t.display_name,
    1
FROM
    (
        VALUES
        ('SEED_INVENTORY_PACKAGE_METADATA','Seed Inventory Package Metadata','Seed Inventory Package Metadata','Seed Inventory Package Metadata'),
        ('SEED_INVENTORY_SEED_METADATA','Seed Inventory Seed Metadata','Seed Inventory Seed Metadata','Seed Inventory Seed Metadata')
    ) AS t (abbrev, name, description, display_name)
;



-- revert changes
--rollback DELETE FROM 
--rollback     master.variable_set 
--rollback WHERE
--rollback     abbrev 
--rollback IN
--rollback     ('SEED_INVENTORY_PACKAGE_METADATA','SEED_INVENTORY_SEED_METADATA')
--rollback ;



--changeset author_name:add_variable_to_variable_set_p1 context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1497 Add variable to variable set p1



WITH t_variable_set_member AS (
    SELECT
        COALESCE(MAX(vsm.order_number), 0) AS last_order_number,
        vs.id AS variable_set_id
    FROM
        master.variable_set vs
        LEFT JOIN master.variable_set_member vsm
            ON vsm.variable_set_id = vs.id
    WHERE
        vs.abbrev = 'SEED_INVENTORY_PACKAGE_METADATA'
        AND vs.is_void = FALSE
    GROUP BY
        vs.id
)
INSERT INTO master.variable_set_member
    (variable_set_id, variable_id, order_number, creator_id)
SELECT
    t.variable_set_id,
    var.id,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    1
FROM
   t_variable_set_member t,
   master.variable var
WHERE
    var.abbrev
IN
    (
        'VOLUME',
        'PACKAGE_UNIT',
        'FACILITY_CODE'
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     master.variable_set_member vsm
--rollback USING
--rollback     master.variable v,
--rollback     master.variable_set vs
--rollback WHERE
--rollback     vsm.variable_set_id = vs.id
--rollback     AND vs.abbrev = 'SEED_INVENTORY_PACKAGE_METADATA'
--rollback     AND vsm.variable_id = v.id
--rollback     AND v.abbrev IN (
--rollback         'VOLUME',
--rollback         'PACKAGE_UNIT',
--rollback         'FACILITY_CODE'
--rollback     )
--rollback ;



--changeset author_name:add_variable_to_variable_set_p2 context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1497 Add variable to variable set p2



WITH t_variable_set_member AS (
    SELECT
        COALESCE(MAX(vsm.order_number), 0) AS last_order_number,
        vs.id AS variable_set_id
    FROM
        master.variable_set vs
        LEFT JOIN master.variable_set_member vsm
            ON vsm.variable_set_id = vs.id
    WHERE
        vs.abbrev = 'SEED_INVENTORY_SEED_METADATA'
        AND vs.is_void = FALSE
    GROUP BY
        vs.id
)
INSERT INTO master.variable_set_member
    (variable_set_id, variable_id, order_number, creator_id)
SELECT
    t.variable_set_id,
    var.id,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    1
FROM
   t_variable_set_member t,
   master.variable var
WHERE
    var.abbrev
IN
    (
        'MTA_STATUS',
        'MTA_NUMBER',
        'IP_STATUS',
        'SOURCE_ORGANIZATION',
        'ORIGIN',
        'SOURCE_STUDY'
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     master.variable_set_member vsm
--rollback USING
--rollback     master.variable v,
--rollback     master.variable_set vs
--rollback WHERE
--rollback     vsm.variable_set_id = vs.id
--rollback     AND vs.abbrev = 'SEED_INVENTORY_SEED_METADATA'
--rollback     AND vsm.variable_id = v.id
--rollback     AND v.abbrev IN (
--rollback         'MTA_STATUS',
--rollback         'MTA_NUMBER',
--rollback         'IP_STATUS',
--rollback         'SOURCE_ORGANIZATION',
--rollback         'ORIGIN',
--rollback         'SOURCE_STUDY'
--rollback     )
--rollback ;