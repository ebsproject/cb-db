--liquibase formatted sql

--changeset postgres:update_gm_config_system_default_visibility_attribute_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4360 GM DB: Modify visibility attribute in GM System default config



-- update config
UPDATE platform.config
SET
    config_value = $${
        "values": [
            {
                "name": "Germplasm Name",
                "type": "column",
                "usage": "required",
                "abbrev": "DESIGNATION",
                "entity": "germplasm",
                "view": {
                    "visible": "true",
                    "entities": [
                        "germplasm",
                        "seed",
                        "package"
                    ]
                },
                "required": "true",
                "api_field": "designation",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Germplasm Name Type",
                "type": "column",
                "usage": "required",
                "abbrev": "GERMPLASM_NAME_TYPE",
                "entity": "germplasm",
                "view": {
                    "visible": "true",
                    "entities": [
                        "germplasm"
                    ]
                },
                "required": "true",
                "api_field": "nameType",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Parentage",
                "type": "column",
                "usage": "required",
                "abbrev": "PARENTAGE",
                "entity": "germplasm",
                "view": {
                    "visible": "true",
                    "entities": [
                        "germplasm"
                    ]
                },
                "required": "true",
                "api_field": "parentage",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Generation",
                "type": "column",
                "usage": "required",
                "abbrev": "GENERATION",
                "entity": "germplasm",
                "view": {
                    "visible": "true",
                    "entities": [
                        "germplasm"
                    ]
                },
                "required": "true",
                "api_field": "generation",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Germplasm State",
                "type": "column",
                "usage": "required",
                "abbrev": "GERMPLASM_STATE",
                "entity": "germplasm",
                "view": {
                    "visible": "true",
                    "entities": [
                        "germplasm"
                    ]
                },
                "required": "true",
                "api_field": "state",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Taxon ID",
                "type": "column",
                "usage": "required",
                "abbrev": "TAXON_ID",
                "entity": "germplasm",
                "view": {
                    "visible": "true",
                    "entities": [
                        "germplasm"
                    ]
                },
                "required": "true",
                "api_field": "taxonomyDbId",
                "data_type": "string",
                "retrieve_db_id": "true",
                "retrieve_endpoint": "taxonomies",
                "retrieve_api_value_field": "taxonomyDbId",
                "retrieve_api_search_field": "taxonId"
            },
            {
                "name": "Seed Name",
                "type": "column",
                "usage": "required",
                "abbrev": "SEED_NAME",
                "entity": "seed",
                "view": {
                    "visible": "true",
                    "entities": [
                        "seed"
                    ]
                },
                "required": "true",
                "api_field": "seedName",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Package Label",
                "type": "column",
                "usage": "required",
                "abbrev": "PACKAGE_LABEL",
                "entity": "package",
                "view": {
                    "visible": "true",
                    "entities": [
                        "package"
                    ]
                },
                "required": "true",
                "api_field": "packageLabel",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Program",
                "type": "column",
                "usage": "required",
                "abbrev": "PROGRAM",
                "entity": "package",
                "view": {
                    "visible": "true",
                    "entities": [
                        "package"
                    ]
                },
                "required": "true",
                "api_field": "programDbId",
                "data_type": "string",
                "retrieve_db_id": "true",
                "retrieve_endpoint": "programs",
                "retrieve_api_value_field": "programDbId",
                "retrieve_api_search_field": "programCode"
            },
            {
                "name": "Package Status",
                "type": "column",
                "usage": "required",
                "abbrev": "PACKAGE_STATUS",
                "entity": "package",
                "view": {
                    "visible": "true",
                    "entities": [
                        "package"
                    ]
                },
                "required": "true",
                "api_field": "packageStatus",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            }
        ]
    }$$
WHERE
    abbrev = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_SYSTEM_DEFAULT';



--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "values": [
--rollback             {
--rollback             "name": "Germplasm Name",
--rollback             "type": "column",
--rollback             "usage": "required",
--rollback             "abbrev": "DESIGNATION",
--rollback             "entity": "germplasm",
--rollback             "visible": "true",
--rollback             "required": "true",
--rollback             "api_field": "designation",
--rollback             "data_type": "string",
--rollback             "retrieve_db_id": "false",
--rollback             "retrieve_endpoint": "",
--rollback             "retrieve_api_value_field": "",
--rollback             "retrieve_api_search_field": ""
--rollback             },
--rollback             {
--rollback             "name": "Germplasm Name Type",
--rollback             "type": "column",
--rollback             "usage": "required",
--rollback             "abbrev": "GERMPLASM_NAME_TYPE",
--rollback             "entity": "germplasm",
--rollback             "visible": "true",
--rollback             "required": "true",
--rollback             "api_field": "nameType",
--rollback             "data_type": "string",
--rollback             "retrieve_db_id": "false",
--rollback             "retrieve_endpoint": "",
--rollback             "retrieve_api_value_field": "",
--rollback             "retrieve_api_search_field": ""
--rollback             },
--rollback             {
--rollback             "name": "Parentage",
--rollback             "type": "column",
--rollback             "usage": "required",
--rollback             "abbrev": "PARENTAGE",
--rollback             "entity": "germplasm",
--rollback             "visible": "true",
--rollback             "required": "true",
--rollback             "api_field": "parentage",
--rollback             "data_type": "string",
--rollback             "retrieve_db_id": "false",
--rollback             "retrieve_endpoint": "",
--rollback             "retrieve_api_value_field": "",
--rollback             "retrieve_api_search_field": ""
--rollback             },
--rollback             {
--rollback             "name": "Generation",
--rollback             "type": "column",
--rollback             "usage": "required",
--rollback             "abbrev": "GENERATION",
--rollback             "entity": "germplasm",
--rollback             "visible": "true",
--rollback             "required": "true",
--rollback             "api_field": "generation",
--rollback             "data_type": "string",
--rollback             "retrieve_db_id": "false",
--rollback             "retrieve_endpoint": "",
--rollback             "retrieve_api_value_field": "",
--rollback             "retrieve_api_search_field": ""
--rollback             },
--rollback             {
--rollback             "name": "Germplasm State",
--rollback             "type": "column",
--rollback             "usage": "required",
--rollback             "abbrev": "GERMPLASM_STATE",
--rollback             "entity": "germplasm",
--rollback             "visible": "true",
--rollback             "required": "true",
--rollback             "api_field": "state",
--rollback             "data_type": "string",
--rollback             "retrieve_db_id": "false",
--rollback             "retrieve_endpoint": "",
--rollback             "retrieve_api_value_field": "",
--rollback             "retrieve_api_search_field": ""
--rollback             },
--rollback             {
--rollback             "name": "Taxon ID",
--rollback             "type": "column",
--rollback             "usage": "required",
--rollback             "abbrev": "TAXON_ID",
--rollback             "entity": "germplasm",
--rollback             "visible": "true",
--rollback             "required": "true",
--rollback             "api_field": "taxonomyDbId",
--rollback             "data_type": "string",
--rollback             "retrieve_db_id": "true",
--rollback             "retrieve_endpoint": "taxonomies",
--rollback             "retrieve_api_value_field": "taxonomyDbId",
--rollback             "retrieve_api_search_field": "taxonId"
--rollback             },
--rollback             {
--rollback             "name": "Seed Name",
--rollback             "type": "column",
--rollback             "usage": "required",
--rollback             "abbrev": "SEED_NAME",
--rollback             "entity": "seed",
--rollback             "visible": "true",
--rollback             "required": "true",
--rollback             "api_field": "seedName",
--rollback             "data_type": "string",
--rollback             "retrieve_db_id": "false",
--rollback             "retrieve_endpoint": "",
--rollback             "retrieve_api_value_field": "",
--rollback             "retrieve_api_search_field": ""
--rollback             },
--rollback             {
--rollback             "name": "Package Label",
--rollback             "type": "column",
--rollback             "usage": "required",
--rollback             "abbrev": "PACKAGE_LABEL",
--rollback             "entity": "package",
--rollback             "visible": "true",
--rollback             "required": "true",
--rollback             "api_field": "packageLabel",
--rollback             "data_type": "string",
--rollback             "retrieve_db_id": "false",
--rollback             "retrieve_endpoint": "",
--rollback             "retrieve_api_value_field": "",
--rollback             "retrieve_api_search_field": ""
--rollback             },
--rollback             {
--rollback             "name": "Program",
--rollback             "type": "column",
--rollback             "usage": "required",
--rollback             "abbrev": "PROGRAM",
--rollback             "entity": "package",
--rollback             "visible": "true",
--rollback             "required": "true",
--rollback             "api_field": "programDbId",
--rollback             "data_type": "string",
--rollback             "retrieve_db_id": "true",
--rollback             "retrieve_endpoint": "programs",
--rollback             "retrieve_api_value_field": "programDbId",
--rollback             "retrieve_api_search_field": "programCode"
--rollback             },
--rollback             {
--rollback             "name": "Package Status",
--rollback             "type": "column",
--rollback             "usage": "required",
--rollback             "abbrev": "PACKAGE_STATUS",
--rollback             "entity": "package",
--rollback             "visible": "true",
--rollback       "required": "true",
--rollback       "api_field": "packageStatus",
--rollback       "data_type": "string",
--rollback       "retrieve_db_id": "false",
--rollback       "retrieve_endpoint": "",
--rollback       "retrieve_api_value_field": "",
--rollback       "retrieve_api_search_field": ""
--rollback     }
--rollback   ]
--rollback }$$
--rollback WHERE
--rollback     abbrev = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_SYSTEM_DEFAULT';