--liquibase formatted sql

--changeset author_name:curate_cross_count_values context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-880 CM-DB: Curate cross_count values



-- update cross_count column
UPDATE
    experiment.entry_list AS entlist
SET
    cross_count = t.cross_count,
    notes = platform.append_text(entlist.notes, 'DB-880 cross_count')
FROM (
        SELECT
            expt.id AS experiment_id,
            entlist.id AS entry_list_id,
            count(*) AS cross_count
        FROM
            experiment.experiment AS expt
            JOIN experiment.entry_list AS entlist
                ON entlist.experiment_id = expt.id
            JOIN germplasm.cross AS crs
                ON crs.experiment_id = expt.id
        WHERE
            entlist.is_void = FALSE
            AND expt.is_void = FALSE
            AND crs.is_void = FALSE
        GROUP BY
            expt.id,
            entlist.id
    ) AS t
WHERE
    t.entry_list_id = entlist.id
;



-- revert changes
--rollback SELECT NULL;