--liquibase formatted sql

--changeset postgres:curate_cross_count_values_p2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-880 CM-DB: Curate cross_count values p2



-- fix mismatch entry_list_id
WITH t1 AS (
    SELECT 
        c.experiment_id cross_experiment_id,
        el.experiment_id entlist_experiment_id,
        el.id entry_list_id,
        c.entry_list_id cross_entlist_id
    FROM 
        germplasm."cross" c 
    LEFT JOIN 
        experiment.entry_list el 
    ON
        el.experiment_id = c.experiment_id 
    WHERE
        el.id != c.entry_list_id 
    ORDER BY 
        el.id  
)
UPDATE germplasm.CROSS c SET entry_list_id  = t1.entry_list_id FROM t1 WHERE c.experiment_id = t1.entlist_experiment_id;

-- update cross_count column
UPDATE
    experiment.entry_list AS entlist
SET
    cross_count = t.cross_count,
    notes = platform.append_text(entlist.notes, 'DB-880 cross_count')
FROM (
        SELECT
            expt.id AS experiment_id,
            entlist.id AS entry_list_id,
            count(*) AS cross_count
        FROM
            experiment.experiment AS expt
            JOIN experiment.entry_list AS entlist
                ON entlist.experiment_id = expt.id
            JOIN germplasm.cross AS crs
                ON crs.experiment_id = expt.id
        WHERE
            entlist.is_void = FALSE
            AND expt.is_void = FALSE
            AND crs.is_void = FALSE
        GROUP BY
            expt.id,
            entlist.id
    ) AS t
WHERE
    t.entry_list_id = entlist.id
;



-- revert changes
--rollback SELECT NULL;