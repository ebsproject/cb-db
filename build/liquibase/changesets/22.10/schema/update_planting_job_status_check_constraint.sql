--liquibase formatted sql

--changeset author_name:update_planting_job_status_check_constraint context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1490 Update planting_job_status check constraint in experiment.planting_job



ALTER TABLE experiment.planting_job
    DROP CONSTRAINT planting_job_status_chk
;

ALTER TABLE experiment.planting_job
    ADD CONSTRAINT planting_job_status_chk
    CHECK (planting_job_status = ANY(ARRAY['draft'::text, 'ready for packing'::text, 'packing'::text, 'packed'::text, 'packing on hold'::text, 'packing cancelled'::text, 'updating entries in progress'::text, 'updating packages in progress'::text]))
;



-- revert changes
--rollback ALTER TABLE experiment.planting_job
--rollback     DROP CONSTRAINT planting_job_status_chk
--rollback ;
--rollback 
--rollback ALTER TABLE experiment.planting_job
--rollback     ADD CONSTRAINT planting_job_status_chk
--rollback     CHECK (planting_job_status = ANY(ARRAY['draft'::text, 'ready for packing'::text, 'packing'::text, 'packed'::text, 'packing on hold'::text, 'packing cancelled'::text, 'updating entries in progress'::text]))
--rollback ;