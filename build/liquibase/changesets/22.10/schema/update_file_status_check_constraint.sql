--liquibase formatted sql

--changeset author_name:update_file_status_check_constraint context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1481 Update file_status check constraint in germplasm.file_upload



ALTER TABLE germplasm.file_upload
    DROP CONSTRAINT file_upload_file_status_chk
;

ALTER TABLE germplasm.file_upload
    ADD CONSTRAINT file_upload_file_status_chk
    CHECK (file_status = ANY(ARRAY['in queue'::text, 'validation in progress'::text, 'validation error'::text, 'validated'::text, 'creation in progress'::text, 'creation failed'::text, 'completed'::text, 'update in progress'::text]))
;



-- revert changes
--rollback ALTER TABLE germplasm.file_upload
--rollback     DROP CONSTRAINT file_upload_file_status_chk
--rollback ;
--rollback 
--rollback ALTER TABLE germplasm.file_upload
--rollback     ADD CONSTRAINT file_upload_file_status_chk
--rollback     CHECK (file_status = ANY(ARRAY['in queue'::text, 'validation in progress'::text, 'validation error'::text, 'validated'::text, 'creation in progress'::text, 'creation failed'::text, 'completed'::text]))
--rollback ;