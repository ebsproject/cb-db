--liquibase formatted sql

--changeset author_name:drop_not_null_constraint_of_germplasm_id context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1480 Drop not null constraint of germplasm_id in germplasm.file_upload_germplasm



ALTER TABLE
    germplasm.file_upload_germplasm
ALTER COLUMN
    germplasm_id
    DROP NOT NULL
;



-- revert changes
--rollback ALTER TABLE
--rollback     germplasm.file_upload_germplasm
--rollback ALTER COLUMN
--rollback     germplasm_id
--rollback     SET NOT NULL
--rollback ;