--liquibase formatted sql

--changeset author_name:update_data_type_of_shipment_item_weight_to_float context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1519 Update data_type of shipment_item_weight to float



-- convert values of the column to float
ALTER TABLE inventory.shipment_item
    ALTER COLUMN shipment_item_weight TYPE float USING shipment_item_weight::float;



-- revert changes
--rollback ALTER TABLE inventory.shipment_item
--rollback     ALTER COLUMN shipment_item_weight TYPE integer USING shipment_item_weight::integer
--rollback ;