--liquibase formatted sql

--changeset author_name:add_column_action_to_germplasm.file_upload context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1479 Add column action to germplasm.file_upload



ALTER TABLE
    germplasm.file_upload
ADD COLUMN
    file_upload_action varchar(16)
    NOT NULL
    DEFAULT 'create'
;

COMMENT ON COLUMN germplasm.file_upload.file_upload_action
    IS 'File Upload Action: Describes whether the file upload is for creation (create) or updating (update) seed/package records [GEFILEUPLOAD_ACTION]';

ALTER TABLE germplasm.file_upload
    ADD CONSTRAINT file_upload_file_upload_action_chk
    CHECK (file_upload_action = ANY(ARRAY['create'::text,'update'::text,'delete'::text]))
;

-- create index for regular column
CREATE INDEX file_upload_file_upload_action_idx
    ON germplasm.file_upload USING btree (file_upload_action)
;



-- revert changes
--rollback ALTER TABLE
--rollback     germplasm.file_upload
--rollback DROP COLUMN
--rollback     file_upload_action
--rollback ;