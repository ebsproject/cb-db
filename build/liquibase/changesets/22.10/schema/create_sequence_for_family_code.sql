--liquibase formatted sql

--changeset postgres:create_sequence_for_family_code context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1454 Create germplasm.family_code_seq



-- create sequence for germplasm.family.family_code
CREATE SEQUENCE germplasm.family_code_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1
    NO CYCLE;

-- set default value for germplasm.family.family_code
ALTER TABLE germplasm.family
    ALTER COLUMN family_code SET DEFAULT nextval('germplasm.family_code_seq'::regclass);



--rollback ALTER TABLE germplasm.family
--rollback     ALTER COLUMN family_code DROP DEFAULT;

--rollback DROP SEQUENCE germplasm.family_code_seq;
