--liquibase formatted sql

--changeset author_name:add_column_crop_id_to_master.scale_conversion context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1493 Add column crop_id to master.scale_conversion



ALTER TABLE
    master.scale_conversion
ADD COLUMN
    crop_id integer
;

COMMENT ON COLUMN master.scale_conversion.crop_id
    IS 'Crop Id: A scale conversion associated with a crop [SCALECONV_CROPID]';

-- create index for regular column
CREATE INDEX scale_conversion_crop_idx
    ON master.scale_conversion USING btree (crop_id)
;

ALTER TABLE master.scale_conversion
    ADD CONSTRAINT scale_conversion_crop_id_fk
    FOREIGN KEY (crop_id)
    REFERENCES tenant.crop (id)
    ON UPDATE CASCADE ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT scale_conversion_crop_id_fk
    ON master.scale_conversion
    IS 'A scale conversion can be used for a specific crop. A crop can have zero or many scale conversion'
;



-- revert changes
--rollback ALTER TABLE
--rollback     master.scale_conversion
--rollback DROP COLUMN
--rollback     crop_id
--rollback ;