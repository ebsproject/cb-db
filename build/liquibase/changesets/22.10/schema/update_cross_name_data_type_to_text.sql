--liquibase formatted sql

--changeset postgres:update_cross_name_data_type_to_text context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1518 Update cross_name data_type to text



-- convert values of the column to text
ALTER TABLE germplasm.cross
    ALTER COLUMN cross_name TYPE varchar USING cross_name::varchar;



-- revert changes
--rollback ALTER TABLE germplasm.cross
--rollback     ALTER COLUMN cross_name TYPE varchar(256) USING cross_name::varchar;