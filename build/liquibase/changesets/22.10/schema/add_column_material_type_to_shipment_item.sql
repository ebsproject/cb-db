--liquibase formatted sql

--changeset author_name:add_column_material_type_to_shipment_item context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1444 Add column material_type to inventory.shipment_item



ALTER TABLE
    inventory.shipment_item
ADD COLUMN
    material_type varchar(16)
    NOT NULL
    DEFAULT 'non-genebank'
;

COMMENT ON COLUMN inventory.shipment_item.material_type
    IS 'Material Type: Material type of the shipment {non-genebank, genebank} [SHIPMENT_MTLTYPE]';

ALTER TABLE inventory.shipment_item
    ADD CONSTRAINT shipment_item_material_type_chk
    CHECK (material_type = ANY(ARRAY['genebank'::text, 'non-genebank'::text]))
;

CREATE INDEX shipment_item_material_type_idx
    ON inventory.shipment_item USING btree (material_type)
;



-- revert changes
--rollback ALTER TABLE
--rollback     inventory.shipment_item
--rollback DROP COLUMN
--rollback     material_type
--rollback ;