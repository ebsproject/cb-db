--liquibase formatted sql

--changeset author_name:add_column_is_submitted_to_experiment.planting_job context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1482 Add column is_submitted to experiment.planting_job



ALTER TABLE
    experiment.planting_job
ADD COLUMN
    is_submitted boolean
    NOT NULL
    DEFAULT false
;

COMMENT ON COLUMN experiment.planting_job.is_submitted
    IS 'Is Submitted: Status if the planting job is already submitted [PJOB_IS_SUBMITTED]';

-- create index for regular column
CREATE INDEX planting_job_is_submitted_idx
    ON experiment.planting_job USING btree (is_submitted)
;



-- revert changes
--rollback ALTER TABLE
--rollback     experiment.planting_job
--rollback DROP COLUMN
--rollback     is_submitted
--rollback ;