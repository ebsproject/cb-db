--liquibase formatted sql

--changeset postgres:create_family_member_germplasm_id_index context:schema splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT COUNT(*) FROM pg_indexes WHERE indexname = 'family_member_germplasm_id_idx'
--comment: CORB-6848 CB-DB: Queries by germplasm_id in the family_member table are very slow



-- add index to family_member.germplasm_id column
CREATE INDEX family_member_germplasm_id_idx
    ON germplasm.family_member USING btree (germplasm_id);



--rollback DROP INDEX germplasm.family_member_germplasm_id_idx;