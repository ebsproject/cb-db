--liquibase formatted sql

--changeset postgres:alter_terminal_status_constraint.sql context:schema splitStatements:false rollbackSplitStatements:false
--comment: CORB-2973 DC-DB: Update terminal transaction statuses



ALTER TABLE IF EXISTS data_terminal.transaction DROP CONSTRAINT IF EXISTS terminal_status;

ALTER TABLE IF EXISTS data_terminal.transaction
    ADD CONSTRAINT terminal_status CHECK (status::text = ANY (
        ARRAY[
            'in queue'::character varying::text, 
            
            'uploading in progress'::character varying::text, 
            'error in background process'::character varying::text, 

            'to be suppressed'::character varying::text,
            'suppression in progress'::character varying::text,

            'to be unsuppressed'::character varying::text,
            'undo suppression in progress'::character varying::text, 

            'removing data in progress'::character varying::text, 
            'undo removing data in progress'::character varying::text, 

            'uploaded'::character varying::text,
            'uploaded: to be calculated'::character varying::text,
            'calculation in progress'::character varying::text,

            'to be committed'::character varying::text,
            'committing in progress'::character varying::text,
            'committed'::character varying::text
        ])
    );



-- revert changes
--rollback ALTER TABLE IF EXISTS data_terminal.transaction DROP CONSTRAINT IF EXISTS terminal_status;
--rollback 
--rollback ALTER TABLE IF EXISTS data_terminal.transaction
--rollback     ADD CONSTRAINT terminal_status CHECK (status::text = ANY (ARRAY['uploading in progress'::character varying::text, 'error in background process'::character varying::text, 'in queue'::character varying::text, 'committing in progress'::character varying::text, 'suppression in progress'::character varying::text, 'undo suppression in progress'::character varying::text, 'removing data in progress'::character varying::text, 'undo removing data in progress'::character varying::text, 'uploaded'::character varying::text, 'validation in progress'::character varying::text, 'committed'::character varying::text, 'validated'::character varying::text]));
