--liquibase formatted sql

--changeset postgres:update_occurrence_code_for_multiple_occurrences context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3187 CB-DB: Update occurrence code of 'IRSEA-F1-2023-DS-005-001','IRSEA-F1-2023-DS-002-001','IRSEA-HB-2023-DS-005-001', and 'IRSEA-F1-2023-DS-003-001'



UPDATE 
    experiment.occurrence 
SET 
    occurrence_code = (experiment.generate_code('occurrence'))
WHERE occurrence_name SIMILAR TO '%IRSEA-HB-2023-DS-005%|%IRSEA-F1-2023-DS-002%|%IRSEA-F1-2023-DS-003%|%IRSEA-F1-2023-DS-005%' AND occurrence_status ILIKE 'planted%'



--rollback UPDATE
--rollback 	experiment.occurrence
--rollback SET
--rollback  occurrence_code = 'EXP0049833-001'
--rollback WHERE
--rollback 	occurrence_name = 'IRSEA-HB-2023-DS-005-001' AND occurrence_status ILIKE 'planted%';
--rollback 
--rollback UPDATE
--rollback 	experiment.occurrence
--rollback SET
--rollback  occurrence_code = 'EXP0049836-001'
--rollback WHERE
--rollback 	occurrence_name = 'IRSEA-F1-2023-DS-002-001' AND occurrence_status ILIKE 'planted%';
--rollback 
--rollback UPDATE
--rollback 	experiment.occurrence
--rollback SET
--rollback  occurrence_code = 'EXP0047456-001'
--rollback WHERE
--rollback 	occurrence_name = 'IRSEA-F1-2023-DS-003-001' AND occurrence_status ILIKE 'planted%';
--rollback 
--rollback UPDATE
--rollback 	experiment.occurrence
--rollback SET
--rollback  occurrence_code = 'EXP0047522-001'
--rollback WHERE
--rollback 	occurrence_name = 'IRSEA-F1-2023-DS-005-001' AND occurrence_status ILIKE 'planted%';
