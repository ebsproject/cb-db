--liquibase formatted sql

--changeset postgres:6_insert_COWPEA_and_SOYBEAN_germplasm_taxonomy_values context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3172 CB-DB: Insert COWPEA and SOYBEAN data to germplasm.taxonomy.



INSERT INTO germplasm.taxonomy
(taxon_id,taxonomy_name,description,crop_id,creator_id)
 VALUES 
('41647','Vigna unguiculata (L.) Walp','Vigna unguiculata (L.) Walp',(SELECT id FROM tenant.crop WHERE crop_code='COWPEA'),(SELECT id FROM tenant.person WHERE username='admin')),
('41651','Glycine max (L.) Merr.','Glycine max (L.) Merr.',(SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN'),(SELECT id FROM tenant.person WHERE username='admin'));



--rollback    DELETE
--rollback    FROM germplasm.taxonomy
--rollback    WHERE taxonomy_name IN ('Vigna unguiculata (L.) Walp',
--rollback                            'Glycine max (L.) Merr.');