--liquibase formatted sql

--changeset postgres:insert_formula_2_of_GY_CALC_THA context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3196 CB-DB: Insert formula GY_CALC_THA usng GY_CALC_GM2



INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula)
    VALUES ('GY_CALC_THA = GY_CALC_GM2/100', (SELECT id FROM master.variable WHERE abbrev = 'GY_CALC_THA'), (SELECT id FROM master.method WHERE abbrev = 'GY_CALC_THA_METHOD'), (SELECT data_level FROM master.variable WHERE abbrev = 'GY_CALC_THA'), 
    'master.formula_gy_calc_tha_2(gy_calc_gm2)', NULL, '
    CREATE OR REPLACE FUNCTION master.formula_gy_cacl_tha_2(
                gy_calc_gm2 float
            ) RETURNS float AS
            $body$
            BEGIN    
              RETURN gy_calc_gm2/100;
            END
            $body$
            LANGUAGE plpgsql;');



--rollback DELETE FROM master.formula WHERE formula IN ('GY_CALC_THA = GY_CALC_GM2/100');



--changeset postgres:insert_formula_parameter_of_GY_CALC_THA_formula_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3196 CB-DB: Insert formula parameter GY_CALC_THA usng GY_CALC_GM2



INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id, order_number)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id,
    1 as order_number
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('GY_CALC_GM2') -- must be the same order as defined in the database function
    AND var.abbrev = 'GY_CALC_THA'
    AND form.formula = 'GY_CALC_THA = GY_CALC_GM2/100';

SELECT master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.formula_parameter 
--rollback where formula_id in (SELECT id FROM master.formula WHERE formula = 'GY_CALC_THA = GY_CALC_GM2/100');



--changeset postgres:insert_function_of_GY_CALC_THA_formula_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3196 CB-DB: Insert GY_CALC_THA function usng GY_CALC_GM2



CREATE OR REPLACE FUNCTION master.formula_gy_cacl_tha_2(
    gy_calc_gm2 float
) RETURNS float AS
$body$
BEGIN    
  RETURN gy_calc_gm2/100;
END
$body$
LANGUAGE plpgsql;



--rollback DROP FUNCTION master.formula_gy_cacl_tha_2;