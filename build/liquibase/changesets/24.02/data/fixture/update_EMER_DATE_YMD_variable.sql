--liquibase formatted sql

--changeset postgres:update_EMER_DATE_YMD_variable context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3198 CB-DB: Update variable of EMER_DATE_YMD



UPDATE
	master.variable
SET
	usage = 'management',
	data_level = 'plot,occurrence'
WHERE abbrev = 'EMER_DATE_YMD';



--rollback UPDATE master.variable
--rollback SET usage = 'occurrence', data_level = 'plot'
--rollback WHERE abbrev = 'EMER_DATE_YMD';