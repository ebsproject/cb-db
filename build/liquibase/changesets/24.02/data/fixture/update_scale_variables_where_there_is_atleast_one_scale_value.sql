--liquibase formatted sql

--changeset postgres:update_scale_variables_where_there_is_atleast_one_scale_value context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3168 CB-DB: set scale.type to 'categorical' on variables with atleast 1 scale value.



UPDATE
    master.scale
SET
type = 'categorical'
where id IN (SELECT DISTINCT scale_id from master.scale_value)



--rollback UPDATE
--rollback     master.scale
--rollback SET
--rollback type = NULL
--rollback where id IN (SELECT DISTINCT scale_id from master.scale_value)