--liquibase formatted sql

--changeset postgres:1_insert_COWPEA_and_SOYBEAN_tenant_crop_values context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3172 CB-DB: Insert COWPEA and SOYBEAN data to tenant.crop.



INSERT INTO tenant.crop
(crop_code,crop_name,description,creator_id)
 VALUES 
('COWPEA','Cowpea','Cowpea crop',(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1)),
('SOYBEAN','Soybean','Soybean crop',(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));



--rollback    DELETE
--rollback    FROM tenant.crop
--rollback    WHERE crop_code IN ('COWPEA','SOYBEAN');