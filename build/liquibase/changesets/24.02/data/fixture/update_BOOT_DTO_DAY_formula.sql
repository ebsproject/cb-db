--liquibase formatted sql

--changeset postgres:update_BOOT_DTO_DAY_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3198 CB-DB: Update formula of BOOT_DTO_DAY



UPDATE 
	master.formula
SET 
	function_name = 'master.formula_boot_dto_day(boot_date_ymd, emer_date_ymd)',
	database_formula = 
	'CREATE OR REPLACE FUNCTION master.formula_boot_dto_day(
		boot_date_ymd date,
		emer_date_ymd date)
		RETURNS double precision
		LANGUAGE plpgsql
		COST 100
		VOLATILE PARALLEL UNSAFE
	AS $BODY$

		BEGIN
			RETURN boot_date_ymd - emer_date_ymd;
		END
				
	$BODY$;'
WHERE formula = 'BOOT_DTO_DAY = BOOT_DATE_YMD - EMER_DATE_YMD';



--rollback UPDATE master.formula
--rollback SET function_name = 'master.formula_boot_dto_day(boot_dto_day,boot_date_ymd,emer_date_ymd)',
--rollback database_formula = 'create or replace function master.formula_hd_dto_day(boot_dto_day date, boot_date_ymd date, emer_date_ymd date ) returns float as $body$ declare boot_dto_day date; local_boot_date_ymd date; local_emer_date_ymd date; begin boot_dto_day = boot_date_ymd - emer_date_ymd; return round(boot_dto_day::numeric, 3); end $body$ language plpgsql;'
--rollback WHERE formula = 'BOOT_DTO_DAY = BOOT_DATE_YMD - EMER_DATE_YMD'
--rollback ;



--changeset postgres:update_BOOT_DTO_DAY_function context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3198 CB-DB: Update formula function of BOOT_DTO_DAY



DROP FUNCTION master.formula_boot_dto_day(date,date,date);

CREATE OR REPLACE FUNCTION master.formula_boot_dto_day(
	boot_date_ymd date,
	emer_date_ymd date)
    RETURNS double precision
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$

	BEGIN
		RETURN boot_date_ymd - emer_date_ymd;
	END
			
$BODY$;



--rollback DROP FUNCTION master.formula_boot_dto_day(date,date);
--rollback create or replace function master.formula_boot_dto_day(boot_dto_day date, boot_date_ymd date, emer_date_ymd date ) returns float as $body$ declare boot_dto_day date; local_boot_date_ymd date; local_emer_date_ymd date; begin boot_dto_day = boot_date_ymd - emer_date_ymd; return round(boot_dto_day::numeric, 3); end $body$ language plpgsql;