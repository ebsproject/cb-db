--liquibase formatted sql

--changeset postgres:7_insert_COWPEA_and_SOYBEAN_platform_config_values context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3172 CB-DB: Insert COWPEA and SOYBEAN data to platform.config.



INSERT INTO platform.config
(abbrev, name, config_value, rank, usage, remarks, notes, creator_id)
--PLOT_TYPE_MAIZE_UNKNOWN values
SELECT 
'PLOT_TYPE_COWPEA_UNKNOWN' AS abbrev,
'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_COWPEA_UNKNOWN' AS name,
config_value, 
rank, 
usage, 
remarks, 
notes,
(SELECT id FROM tenant.person WHERE username='admin') AS creator_id
FROM 
platform.config
WHERE abbrev = 'PLOT_TYPE_MAIZE_UNKNOWN'
UNION
SELECT 
'PLOT_TYPE_SOYBEAN_UNKNOWN' AS abbrev,
'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_SOYBEAN_UNKNOWN' AS name,
config_value, 
rank, 
usage, 
remarks, 
notes,
(SELECT id FROM tenant.person WHERE username='admin') AS creator_id
FROM 
platform.config
WHERE abbrev = 'PLOT_TYPE_MAIZE_UNKNOWN'
UNION
--MAIZE_PLOT_TYPE values
SELECT 
'COWPEA_PLOT_TYPE' AS abbrev,
'Experiment Protocol Plot types for Cowpea with no selected planting type' AS name,
'{"Name": "Required experiment level protocol plot type variables", "Values": [{"default": false, "disabled": false, "required": "required", "field_label": "Plot Type", "order_number": 1, "variable_abbrev": "PLOT_TYPE", "field_description": "Plot Type", "plot_type_abbrevs": ["PLOT_TYPE_2R5M", "PLOT_TYPE_COWPEA_UNKNOWN"]}]}', 
rank, 
usage, 
remarks, 
NULL as notes,
(SELECT id FROM tenant.person WHERE username='admin') AS creator_id
FROM 
platform.config
WHERE abbrev = 'MAIZE_PLOT_TYPE'
UNION
SELECT 
'SOYBEAN_PLOT_TYPE' AS abbrev,
'Experiment Protocol Plot types for Soybean with no selected planting type' AS name,
'{"Name": "Required experiment level protocol plot type variables", "Values": [{"default": false, "disabled": false, "required": "required", "field_label": "Plot Type", "order_number": 1, "variable_abbrev": "PLOT_TYPE", "field_description": "Plot Type", "plot_type_abbrevs": ["PLOT_TYPE_2R5M", "PLOT_TYPE_SOYBEAN_UNKNOWN"]}]}', 
rank, 
usage, 
remarks, 
NULL as notes,
(SELECT id FROM tenant.person WHERE username='admin') AS creator_id
FROM 
platform.config
WHERE abbrev = 'MAIZE_PLOT_TYPE'
UNION
--MAIZE_PLANTING_TYPE values
SELECT 
'COWPEA_PLANTING_TYPE' AS abbrev,
'Experiment Crop-Planting type relation-COWPEA' AS name,
config_value, 
rank, 
usage, 
remarks, 
NULL as notes,
(SELECT id FROM tenant.person WHERE username='admin') AS creator_id
FROM 
platform.config
WHERE abbrev = 'MAIZE_PLANTING_TYPE'
UNION
SELECT 
'SOYBEAN_PLANTING_TYPE' AS abbrev,
'Experiment Crop-Planting type relation-SOYBEAN' AS name,
config_value, 
rank, 
usage, 
remarks, 
NULL as notes,
(SELECT id FROM tenant.person WHERE username='admin') AS creator_id
FROM 
platform.config
WHERE abbrev = 'MAIZE_PLANTING_TYPE'
UNION
--MAIZE_FLAT_PLOT_TYPE values
SELECT 
'COWPEA_FLAT_PLOT_TYPE' AS abbrev,
'Experiment Protocol Plot types for Cowpea' AS name,
'{"Name": "Required experiment level protocol plot type variables", "Values": [{"default": false, "disabled": false, "required": "required", "field_label": "Plot Type", "order_number": 1, "variable_abbrev": "PLOT_TYPE", "field_description": "Plot Type", "plot_type_abbrevs": ["PLOT_TYPE_2R5M", "PLOT_TYPE_COWPEA_UNKNOWN"]}]}', 
rank, 
usage, 
remarks, 
NULL as notes,
(SELECT id FROM tenant.person WHERE username='admin') AS creator_id
FROM 
platform.config
WHERE abbrev = 'MAIZE_FLAT_PLOT_TYPE'
UNION
SELECT 
'SOYBEAN_FLAT_PLOT_TYPE' AS abbrev,
'Experiment Protocol Plot types for Soybean' AS name,
'{"Name": "Required experiment level protocol plot type variables", "Values": [{"default": false, "disabled": false, "required": "required", "field_label": "Plot Type", "order_number": 1, "variable_abbrev": "PLOT_TYPE", "field_description": "Plot Type", "plot_type_abbrevs": ["PLOT_TYPE_2R5M", "PLOT_TYPE_SOYBEAN_UNKNOWN"]}]}', 
rank, 
usage, 
remarks, 
NULL as notes,
(SELECT id FROM tenant.person WHERE username='admin') AS creator_id
FROM 
platform.config
WHERE abbrev = 'MAIZE_FLAT_PLOT_TYPE'
UNION
--GM_FILE_UPLOAD_VARIABLES_CONFIG_MAIZE_DEFAULT value
SELECT 
'GM_FILE_UPLOAD_VARIABLES_CONFIG_COWPEA_DEFAULT' AS abbrev,
'Germplasm File Upload variables configuration for COWPEA' AS name,
config_value, 
rank, 
usage, 
remarks, 
NULL as notes,
(SELECT id FROM tenant.person WHERE username='admin') AS creator_id
FROM 
platform.config
WHERE abbrev = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_MAIZE_DEFAULT'
UNION
SELECT 
'GM_FILE_UPLOAD_VARIABLES_CONFIG_SOYBEAN_DEFAULT' AS abbrev,
'Germplasm File Upload variables configuration for SOYBEAN' AS name,
config_value, 
rank, 
usage, 
remarks, 
NULL as notes,
(SELECT id FROM tenant.person WHERE username='admin') AS creator_id
FROM 
platform.config
WHERE abbrev = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_MAIZE_DEFAULT'
;



--rollback    DELETE
--rollback    FROM platform.config
--rollback    WHERE abbrev IN ('PLOT_TYPE_COWPEA_UNKNOWN',
--rollback                     'PLOT_TYPE_SOYBEAN_UNKNOWN',
--rollback                     'COWPEA_PLOT_TYPE',
--rollback                     'SOYBEAN_PLOT_TYPE',
--rollback                     'COWPEA_PLANTING_TYPE',
--rollback                     'SOYBEAN_PLANTING_TYPE',
--rollback                     'COWPEA_FLAT_PLOT_TYPE',
--rollback                     'SOYBEAN_FLAT_PLOT_TYPE',
--rollback                     'GM_FILE_UPLOAD_VARIABLES_CONFIG_COWPEA_DEFAULT',
--rollback                     'GM_FILE_UPLOAD_VARIABLES_CONFIG_SOYBEAN_DEFAULT');




