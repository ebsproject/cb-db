--liquibase formatted sql

--changeset postgres:insert_GY_MOI12_5_CMP_THA_variable_and_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3214 CB-DB: Insert GY_MOI12_5_CMP_THA variable and formula



-- GY_MOI12_5_CMP_THA
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable(abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name, notes) 
    VALUES
        ('GY_MOI12_5_CMP_THA', 'GY_MOI12_5_CMP_THA', 'Grain yield (MOI12_5)', 'float', false, 'observation', 'plot', 'occurrence', 'Moisture-adjusted (to 12.5% moisture) grain yield calculated in tons/hectare', 'active', '1', 'Moisture-adjusted (to 12.5% moisture) grain yield calculated in tons/hectare','24.02')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'GY_MOI12_5_CMP_THA' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('GY_MOI12_5_CMP_THA', 'Grain yield (MOI12_5)', 'Moisture-adjusted (to 12.5% moisture) grain yield calculated in tons/hectare') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('GY_MOI12_5_CMP_THA_METHOD', 'Grain yield (MOI12_5) method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('GY_MOI12_5_CMP_THA_SCALE', 'Grain yield (MOI12_5) scale', NULL, NULL, 0, 30)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id)
    VALUES ('GY_MOI12_5_CMP_THA=GY_CMP_THA*(100-GMOI_M_PCT)/(100-12.5)', (SELECT id FROM master.variable WHERE abbrev = 'GY_MOI12_5_CMP_THA'), (SELECT id FROM master.method WHERE abbrev = 'GY_MOI12_5_CMP_THA_METHOD'), 'plot', '
    master.formula_gy_moi12_5_cmp_tha(gy_cmp_tha, gmoi_m_pct)', NULL, 
    'CREATE OR REPLACE FUNCTION master.formula_GY_MOI12_5_CMP_THA(
				gy_cmp_tha float,
                gmoi_m_pct float
			) RETURNS float as
			$body$
            BEGIN
				RETURN gy_cmp_tha*(100-gmoi_m_pct)/(100-12.5);
			END
			$body$
			LANGUAGE plpgsql;', '3', '1');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('GY_CMP_THA', 'GMOI_M_PCT') -- must be the same order as defined in the database function
    AND var.abbrev = 'GY_MOI12_5_CMP_THA'
;

UPDATE master.formula_parameter SET
order_number = 1
WHERE
formula_id in (select id from master.formula f where formula ilike 'GY_MOI12_5_CMP_THA%') 
AND
param_variable_id in (select id from master.variable where abbrev = 'GY_CMP_THA');

UPDATE master.formula_parameter SET
order_number = 2
WHERE
formula_id in (select id from master.formula f where formula ilike 'GY_MOI12_5_CMP_THA%') 
AND
param_variable_id in (select id from master.variable where abbrev = 'GMOI_M_PCT');


CREATE OR REPLACE FUNCTION master.formula_gy_moi12_5_cmp_tha(
				gy_cmp_tha float,
                gmoi_m_pct float
			) RETURNS float as
			$body$
            BEGIN
				RETURN gy_cmp_tha*(100-gmoi_m_pct)/(100-12.5);
			END
			$body$
			LANGUAGE plpgsql;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('GY_MOI12_5_CMP_THA');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'GY_MOI12_5_CMP_THA=GY_CMP_THA*(100-GMOI_M_PCT)/(100-12.5)');
--rollback DELETE FROM master.formula WHERE formula IN ('GY_MOI12_5_CMP_THA=GY_CMP_THA*(100-GMOI_M_PCT)/(100-12.5)');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('GY_MOI12_5_CMP_THA_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('GY_MOI12_5_CMP_THA');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('GY_MOI12_5_CMP_THA_SCALE');
--rollback DROP FUNCTION master.formula_gy_moi12_5_cmp_tha;