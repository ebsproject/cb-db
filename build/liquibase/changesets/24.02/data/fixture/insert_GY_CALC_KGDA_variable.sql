--liquibase formatted sql

--changeset postgres:insert_GY_CALC_KGDA_variable context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3196 CB-DB: Insert GY_CALC_KGDA variable record



--GY_CALC_KGDA
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('GY_CALC_KGDA', 'Grain Yield (KGDA)', 'Grain Yield (KGDA)', 'float', false, 'observation', 'plot', 'occurrence', NULL, 'active', '1', 'Grain Yield (KGDA)', '24.02')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'GY_CALC_KGDA' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('GY_CALC_KGDA', 'Grain yield', 'Grain Yield') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('GY_CALC_KGDA_METHOD', 'Grain Yield method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('GY_CALC_KGDA_SCALE', 'Grain Yield scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('GY_CALC_KGDA');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('GY_CALC_KGDA_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('GY_CALC_KGDA');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('GY_CALC_KGDA_SCALE');   