--liquibase formatted sql



--changeset postgres:trim_newline_in_function_name_and_database_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3202 CB-DB: Trim excess newlines in function_name and database_formula column



UPDATE
	master.formula
SET
	function_name = REGEXP_REPLACE(function_name, '^[\s]+', ''),
	database_formula = REGEXP_REPLACE(database_formula, '^[\s]+', '')



--rollback SELECT NULL;