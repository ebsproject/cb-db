--liquibase formatted sql

--changeset postgres:update_GY_CALC_GM2_variable context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3196 CB-DB: Update GY_CALC_GM2 variable record



UPDATE master.variable
SET
 name = 'Grain yield (GM2)'
WHERE abbrev = 'GY_CALC_GM2'
;



--rollback UPDATE master.variable
--rollback SET
--rollback  name = 'GY_Calc_gm2'
--rollback WHERE abbrev = 'GY_CALC_GM2'
--rollback ;