--liquibase formatted sql

--changeset postgres:update_YLD_CONT4_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3211 CB-DB: Update formula of YLD_CONT4



UPDATE 
	master.formula
SET 
	database_formula = 
	'CREATE OR REPLACE FUNCTION master.formula_yld_cont4(
		ayld_cont double precision,
		mf_cont double precision,
		adjhvarea_cont2 double precision)
		RETURNS double precision AS 
		$BODY$
		BEGIN
			RETURN ayld_cont*mf_cont*10/adjhvarea_cont2;
		END		
		$BODY$ LANGUAGE plpgsql;'
WHERE formula = 'YLD_CONT4 = AYLD_CONT * MF_CONT * 10/ADJHVAREA_CONT2';



--rollback UPDATE master.formula
--rollback SET database_formula = 'create or replace function master.formula_yld_cont4(ayld_cont float, mf_cont float, adjhvarea_cont2 float ) returns float as $body$ declare YLD_CONT4 float; local_ayld_g_cont float; local_hv_area_dsr_sqm float; begin YLD_CONT4=(ayld_g_cont/hv_area_dsr_sqm)*10; return round(YLD_CONT4::numeric, 3); end $body$ language plpgsql;'
--rollback WHERE formula = 'YLD_CONT4 = AYLD_CONT * MF_CONT * 10/ADJHVAREA_CONT2'
--rollback ;



--changeset postgres:update_YLD_CONT4_formula_parameter context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3211 CB-DB: Update formula_parameter of YLD_CONT4



UPDATE master.formula_parameter SET
order_number = 1
WHERE
formula_id in (select id from master.formula f where formula ilike 'YLD_CONT4%') 
AND
param_variable_id in (select id from master.variable where abbrev = 'AYLD_CONT');



UPDATE master.formula_parameter SET
order_number = 2
WHERE
formula_id in (select id from master.formula f where formula ilike 'YLD_CONT4%') 
AND
param_variable_id in (select id from master.variable where abbrev = 'MF_CONT');



UPDATE master.formula_parameter SET
order_number = 3
WHERE
formula_id in (select id from master.formula f where formula ilike 'YLD_CONT4%') 
AND
param_variable_id in (select id from master.variable where abbrev = 'ADJHVAREA_CONT2');



--rollback UPDATE master.formula_parameter SET
--rollback order_number = NULL
--rollback WHERE
--rollback formula_id in (select id from master.formula f where formula ilike 'YLD_CONT4%') 
--rollback AND
--rollback param_variable_id in (select id from master.variable where abbrev = 'AYLD_CONT');
--rollback UPDATE master.formula_parameter SET
--rollback order_number = NULL
--rollback WHERE
--rollback formula_id in (select id from master.formula f where formula ilike 'YLD_CONT4%') 
--rollback AND
--rollback param_variable_id in (select id from master.variable where abbrev = 'MF_CONT');
--rollback UPDATE master.formula_parameter SET
--rollback order_number = NULL
--rollback WHERE
--rollback formula_id in (select id from master.formula f where formula ilike 'YLD_CONT4%') 
--rollback AND
--rollback param_variable_id in (select id from master.variable where abbrev = 'ADJHVAREA_CONT2');




--changeset postgres:update_YLD_CONT4_function context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3211 CB-DB: Update formula function of YLD_CONT4



DROP FUNCTION master.formula_yld_cont4(float,float);

CREATE OR REPLACE FUNCTION master.formula_yld_cont4(
		ayld_cont double precision,
		mf_cont double precision,
		adjhvarea_cont2 double precision)
		RETURNS double precision AS 
		$BODY$
		BEGIN
			RETURN ayld_cont*mf_cont*10/adjhvarea_cont2;
		END		
		$BODY$ LANGUAGE plpgsql;



--rollback DROP FUNCTION master.formula_yld_cont4(double precision,double precision,double precision);
--rollback CREATE OR REPLACE FUNCTION master.formula_yld_cont4( ayld_g_cont float, hv_area_dsr_sqm float ) RETURNS float as $body$ DECLARE YLD_CONT4 float; local_ayld_g_cont float; local_hv_area_dsr_sqm float; BEGIN YLD_CONT4=(ayld_g_cont/hv_area_dsr_sqm)*10; RETURN round(YLD_CONT4::numeric, 3); END $body$ language plpgsql;