--liquibase formatted sql

--changeset postgres:update_formula_of_GY_MOI13_5_CMP_THA context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3214 CB-DB: Update formula of GY_MOI13_5_CMP_THA



UPDATE 
    master.formula 
SET
    database_formula =
    'CREATE OR REPLACE FUNCTION master.formula_gy_moi13_5_cmp_tha(
				gy_cmp_tha float,
                gmoi_m_pct float
			) RETURNS float as
			$body$
            BEGIN
				RETURN gy_cmp_tha*(100-gmoi_m_pct)/(100-13.5);
			END
			$body$
			LANGUAGE plpgsql;'
WHERE formula = 'GY_MOI13_5_CMP_THA=GY_CMP_THA*(100-GMOI_M_PCT)/(100-13.5)';



--rollback UPDATE master.formula SET database_formula = 'create or replace function master.formula_gy_moi13_5_cmp_tha( gy_cmp_tha float, gmoi_m_pct float ) returns float as $body$ declare gy_moi13_5_cmp_tha float; local_gy_cmp_tha float; local_gmoi_m_pct float; begin gy_moi13_5_cmp_tha=gy_cmp_tha*(100-gmoi_m_pct)/(100-13.5); return round(gy_moi13_5_cmp_tha::numeric, 3); end $body$ language plpgsql;';



--changeset postgres:insert_formula_parameter_of_GY_MOI13_5_CMP_THA context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3214 CB-DB: insert formula parameter and update existin parameter of GY_MOI13_5_CMP_THA



INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('GMOI_M_PCT') -- must be the same order as defined in the database function
    AND var.abbrev = 'GY_MOI13_5_CMP_THA'
;



UPDATE master.formula_parameter SET
order_number = 2
WHERE
formula_id in (select id from master.formula f where formula ilike 'GY_MOI13_5_CMP_THA%') 
AND
param_variable_id in (select id from master.variable where abbrev = 'GMOI_M_PCT');



--rollback DELETE FROM master.formula_parameter 
--rollback where formula_id in (SELECT id FROM master.formula WHERE formula = 'GY_MOI13_5_CMP_THA=GY_CMP_THA*(100-GMOI_M_PCT)/(100-13.5)') AND
--rollback param_variable_id in (select id from master.variable where abbrev = 'GMOI_M_PCT');



--changeset postgres:update_function_of_GY_MOI13_5_CMP_THA context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3214 CB-DB: update function of GY_MOI13_5_CMP_THA



CREATE OR REPLACE FUNCTION master.formula_gy_moi13_5_cmp_tha(
		gy_cmp_tha float,
        gmoi_m_pct float
	) RETURNS float as
	$body$
    BEGIN
		RETURN gy_cmp_tha*(100-gmoi_m_pct)/(100-13.5);
	END
	$body$
	LANGUAGE plpgsql;



--rollback CREATE OR REPLACE FUNCTION master.formula_gy_moi13_5_cmp_tha( gy_cmp_tha float, gmoi_m_pct float ) RETURNS float as $body$ DECLARE gy_moi13_5_cmp_tha float; local_gy_cmp_tha float; local_gmoi_m_pct float; BEGIN gy_moi13_5_cmp_tha=gy_cmp_tha*(100-gmoi_m_pct)/(100-13.5); RETURN round(gy_moi13_5_cmp_tha::numeric, 3); END $body$ language plpgsql;