--liquibase formatted sql

--changeset postgres:4_insert_COWPEA_and_SOYBEAN_tenant_team_values context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3172 CB-DB: Insert COWPEA and SOYBEAN data to tenant.team.



INSERT INTO tenant.team
(team_code,team_name,description,creator_id)
 VALUES 
('CWP_TEAM','Cowpea Breeding Team','Cowpea Breeding Team',(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1)),
('SYB_TEAM','Soybean Breeding Team','Soybean Breeding Team',(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));



--rollback    DELETE
--rollback    FROM tenant.team
--rollback    WHERE team_code IN ('CWP_TEAM','SYB_TEAM');