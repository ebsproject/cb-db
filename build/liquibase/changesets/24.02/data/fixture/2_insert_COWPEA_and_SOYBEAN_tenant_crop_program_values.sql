--liquibase formatted sql

--changeset postgres:2_insert_COWPEA_and_SOYBEAN_tenant_crop_program_values context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3172 CB-DB: Insert COWPEA and SOYBEAN data to tenant.crop_program.



INSERT INTO tenant.crop_program
(crop_program_code,crop_program_name,description,organization_id,crop_id,creator_id)
 VALUES 
('COWPEA_PROG','Cowpea Program','Cowpea Crop',(SELECT id FROM tenant.organization WHERE organization_code='CIMMYT' LIMIT 1),(SELECT id FROM tenant.crop WHERE crop_code='COWPEA' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1)),
('SOYBEAN_PROG','Soybean Program','Soybean Crop',(SELECT id FROM tenant.organization WHERE organization_code='CIMMYT' LIMIT 1),(SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));



--rollback    DELETE
--rollback    FROM tenant.crop_program
--rollback    WHERE crop_program_code IN ('COWPEA_PROG','SOYBEAN_PROG');