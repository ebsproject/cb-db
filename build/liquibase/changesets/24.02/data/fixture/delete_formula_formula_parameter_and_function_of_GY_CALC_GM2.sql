--liquibase formatted sql

--changeset postgres:delete_GY_CALC_GM2_formula_parameters context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3196 CB-DB: delete GY_CALC_GM2 formula_parameter



DELETE FROM master.formula_parameter 
WHERE formula_id IN (SELECT id FROM master.formula WHERE formula = 'GY_CALC_GM2 = GY_CALC_GM2/100');



--rollback INSERT INTO master.formula_parameter
--rollback     (formula_id, param_variable_id, data_level, result_variable_id)
--rollback SELECT
--rollback     form.id AS formula_id,
--rollback     pvar.id AS param_variable_id,
--rollback     pvar.data_level,
--rollback     form.result_variable_id
--rollback FROM
--rollback     master.formula AS form
--rollback     INNER JOIN master.variable AS var
--rollback         ON var.id = form.result_variable_id,
--rollback     master.variable AS pvar
--rollback WHERE
--rollback     pvar.abbrev IN ('GY_CALC_GM2') -- must be the same order as defined in the database function
--rollback     AND var.abbrev = 'GY_CALC_GM2'
--rollback ;



--changeset postgres:delete_GY_CALC_GM2_function context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3196 CB-DB: delete GY_CALC_GM2 function



DROP FUNCTION master.formula_gy_calc_gm2;



--rollback CREATE OR REPLACE FUNCTION master.formula_gy_calc_gm2( gy_calc_gm2 float ) RETURNS float as $body$ DECLARE gy_calc_gm2 float; local_gy_calc_gm2 float; BEGIN gy_calc_gm2 = gy_calc_gm2/100; RETURN round(gy_calc_gm2::numeric, 3); END $body$ language plpgsql;



--changeset postgres:delete_GY_CALC_GM2_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3196 CB-DB: delete GY_CALC_GM2 formula



DELETE FROM master.formula WHERE formula IN ('GY_CALC_GM2 = GY_CALC_GM2/100');



--rollback INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
--rollback     VALUES ('GY_CALC_GM2 = GY_CALC_GM2/100', (SELECT id FROM master.variable WHERE abbrev = 'GY_CALC_GM2'), (SELECT id FROM master.method WHERE abbrev = 'GY_CALC_GM2_METHOD'), 'plot', '
--rollback     master.formula_gy_calc_gm2(gy_calc_gm2)', NULL, '
--rollback     create or replace function master.formula_gy_calc_gm2( gy_calc_gm2 float ) returns float as $body$ declare gy_calc_gm2 float; local_gy_calc_gm2 float; begin gy_calc_gm2 = gy_calc_gm2/100; return round(gy_calc_gm2::numeric, 3); end $body$ language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');