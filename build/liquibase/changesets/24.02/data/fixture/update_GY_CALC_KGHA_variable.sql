--liquibase formatted sql

--changeset postgres:update_GY_CALC_KGHA_variable context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3196 CB-DB: Update GY_CALC_KGHA variable record



UPDATE master.variable
SET
 name = 'Grain yield (KGHA)'
WHERE abbrev = 'GY_CALC_KGHA'
;



--rollback UPDATE master.variable
--rollback SET
--rollback  name = 'GY_Calc_kgha'
--rollback WHERE abbrev = 'GY_CALC_KGHA'
--rollback ;