--liquibase formatted sql

--changeset postgres:3_insert_COWPEA_and_SOYBEAN_tenant_program_values context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3172 CB-DB: Insert COWPEA and SOYBEAN data to tenant.program.



INSERT INTO tenant.program
(program_code,program_name,program_type,program_status,description,crop_program_id,creator_id)
 VALUES 
('CWP','Cowpea Breeding Program','breeding','active','Cowpea Breeding Program',(SELECT id FROM tenant.crop_program WHERE crop_program_code='COWPEA_PROG' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1)),
('SYB','Soybean Breeding Program','breeding','active','Soybean Breeding Program',(SELECT id FROM tenant.crop_program WHERE crop_program_code='SOYBEAN_PROG' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));



--rollback    DELETE
--rollback    FROM tenant.program
--rollback    WHERE program_code IN ('CWP','SYB');