--liquibase formatted sql

--changeset postgres:update_voided_plot_records_is_void context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3203 CB-DB: Update voided plot records is_void inherently


UPDATE
	experiment.plot
SET
	is_void = true
WHERE plot_code SIMILAR TO 'VOIDED%' AND is_void = false;



--rollback UPDATE experiment.plot
--rollback SET is_void = false
--rollback WHERE plot_code SIMILAR TO 'VOIDED%' AND is_void = true;
