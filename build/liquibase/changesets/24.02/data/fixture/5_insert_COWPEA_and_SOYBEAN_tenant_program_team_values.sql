--liquibase formatted sql

--changeset postgres:5_insert_COWPEA_and_SOYBEAN_tenant_program_team_values context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3172 CB-DB: Insert COWPEA and SOYBEAN data to tenant.program_team.



INSERT INTO tenant.program_team
(program_id,team_id,order_number,creator_id)
 VALUES 
((SELECT id FROM tenant.program WHERE program_code='CWP' LIMIT 1),(SELECT id FROM tenant.team WHERE team_code='CWP_TEAM' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1)),
((SELECT id FROM tenant.program WHERE program_code='SYB' LIMIT 1),(SELECT id FROM tenant.team WHERE team_code='SYB_TEAM' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));



--rollback    DELETE
--rollback    FROM tenant.program_team
--rollback    WHERE (program_id IN (SELECT id FROM tenant.program WHERE program_code IN ('CWP','SYB')))
--rollback     AND order_number = 1;