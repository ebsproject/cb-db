--liquibase formatted sql

--changeset postgres:add_config_hm_data_browser_config_cowpea context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6855 Add HM_DATA_BROWSER_CONFIG_COWPEA for Cowpea in platform.config



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_DATA_BROWSER_CONFIG_COWPEA',
        'HM Data Browser Configuration for Cowpea',
        $$			
            {
                "default": {
                    "default": {
                        "default": {
                            "input_columns": [],
                            "numeric_variables": [],
                            "additional_required_variables": []
                        }
                    }
                },
                "CROSS_METHOD_SELFING": {
                    "fixed": {
                        "default": {
                            "input_columns": [
                                {
                                    "abbrev": "HVDATE_CONT",
                                    "required": true,
                                    "column_name": "harvestDate",
                                    "placeholder": "Harvest Date",
                                    "retrieve_scale": false
                                },
                                {
                                    "abbrev": "HV_METH_DISC",
                                    "required": true,
                                    "column_name": "harvestMethod",
                                    "placeholder": "Harvest Method",
                                    "retrieve_scale": false
                                },
                                {
                                    "abbrev": "<none>",
                                    "required": null,
                                    "column_name": "numericVar",
                                    "placeholder": "Not applicable",
                                    "retrieve_scale": false
                                }
                            ],
                            "numeric_variables": [
                                {
                                    "min": null,
                                    "type": "number",
                                    "abbrev": "<none>",
                                    "sub_type": "single_int",
                                    "field_name": "<none>",
                                    "placeholder": "Not applicable",
                                    "harvest_methods": [
                                        "HV_METH_DISC_BULK"
                                    ],
                                    "harvest_methods_optional": []
                                }
                            ],
                            "additional_required_variables": []
                        }
                    },
                    "default": {
                        "default": {
                            "input_columns": [
                                {
                                    "abbrev": "HVDATE_CONT",
                                    "required": true,
                                    "column_name": "harvestDate",
                                    "placeholder": "Harvest Date",
                                    "retrieve_scale": false
                                },
                                {
                                    "abbrev": "HV_METH_DISC",
                                    "required": true,
                                    "column_name": "harvestMethod",
                                    "placeholder": "Harvest Method",
                                    "retrieve_scale": false
                                },
                                {
                                    "abbrev": "<none>",
                                    "required": null,
                                    "column_name": "numericVar",
                                    "placeholder": "Not applicable",
                                    "retrieve_scale": false
                                }
                            ],
                            "numeric_variables": [
                                {
                                    "min": null,
                                    "type": "number",
                                    "abbrev": "<none>",
                                    "sub_type": "single_int",
                                    "field_name": "<none>",
                                    "placeholder": "Not applicable",
                                    "harvest_methods": [
                                        ""
                                    ],
                                    "harvest_methods_optional": []
                                }
                            ],
                            "additional_required_variables": []
                        }
                    },
                    "unknown": {
                        "default": {
                            "input_columns": [
                                {
                                    "abbrev": "HVDATE_CONT",
                                    "required": true,
                                    "column_name": "harvestDate",
                                    "placeholder": "Harvest Date",
                                    "retrieve_scale": false
                                },
                                {
                                    "abbrev": "HV_METH_DISC",
                                    "required": true,
                                    "column_name": "harvestMethod",
                                    "placeholder": "Harvest Method",
                                    "retrieve_scale": false
                                },
                                {
                                    "abbrev": "<none>",
                                    "required": null,
                                    "column_name": "numericVar",
                                    "placeholder": "Not applicable",
                                    "retrieve_scale": false
                                }
                            ],
                            "numeric_variables": [
                                {
                                    "min": null,
                                    "type": "number",
                                    "abbrev": "<none>",
                                    "sub_type": "single_int",
                                    "field_name": "<none>",
                                    "placeholder": "Not applicable",
                                    "harvest_methods": [
                                        ""
                                    ],
                                    "harvest_methods_optional": []
                                }
                            ],
                            "additional_required_variables": []
                        }
                    }
                }
            }
        $$,
        1,
        'harvest_manager',
        1,
        'CORB-6855 Add HM_DATA_BROWSER_CONFIG_COWPEA for Cowpea in platform.config'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='HM_DATA_BROWSER_CONFIG_COWPEA';