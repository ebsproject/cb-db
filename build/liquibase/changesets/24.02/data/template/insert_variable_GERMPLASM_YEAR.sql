--liquibase formatted sql

--changeset postgres:insert_variable_GERMPLASM_YEAR context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:CONTINUE
--precondition-sql-check expectedResult:0 SELECT COUNT(*) FROM master.variable WHERE abbrev IN ('GERMPLASM_YEAR');
--comment: DEVOPS-3194 CB-DB: Insert master.variable record GERMPLASM_YEAR



INSERT INTO master.variable
(abbrev, "label", "name", data_type, not_null, "type", status, display_name, ontology_reference, bibliographical_reference, variable_set, synonym, remarks, creator_id, modification_timestamp, modifier_id, notes, is_void, description, default_value, "usage", data_level, is_column, column_table, is_computed, member_data_type, target_variable_id, field_prop, member_variable_id, target_model, class_variable_id, json_type, notif, event_log, target_table, variable_document)
VALUES('GERMPLASM_YEAR', 'Germplasm Year of Origin', 'Germplasm Year of Origin', 'integer', false, 'metadata', 'active', 'Germplasm Year of Origin', NULL, NULL, NULL, NULL, NULL, (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1), NULL, NULL, 'add new record mlotho20230702', false, 'Year when the germplasm was developed or collected', NULL, 'undefined', 'germplasm', false, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[{"actor_id": "18", "new_data": {"id": 3455, "name": "Germplasm Year of Origin", "type": "metadata", "label": "Germplasm Year of Origin", "notes": "add new record mlotho20230702", "notif": null, "usage": "undefined", "abbrev": "GERMPLASM_YEAR", "status": "active", "is_void": false, "remarks": null, "synonym": null, "not_null": false, "scale_id": 1075, "data_type": "integer", "is_column": false, "json_type": null, "legacy_id": null, "method_id": 1107, "creator_id": 18, "data_level": "germplasm", "field_prop": null, "description": "Year when the germplasm was developed or collected", "is_computed": false, "modifier_id": null, "property_id": 725, "column_table": null, "display_name": "Germplasm Year of Origin", "target_model": null, "target_table": null, "variable_set": null, "default_value": "", "migrationnotes": null, "member_data_type": null, "class_variable_id": null, "variable_document": null, "creation_timestamp": "2023-07-02 23:42:26.83209+08", "member_variable_id": null, "ontology_reference": null, "target_variable_id": null, "bibliographical_reference": null}, "row_data": null, "action_type": "INSERT", "log_timestamp": "2023-07-02 23:42:26.83209+08", "transaction_id": "13061729"}]'::jsonb, NULL, NULL);



--rollback DELETE FROM master.variable
--rollback WHERE 
--rollback abbrev='GERMPLASM_YEAR';