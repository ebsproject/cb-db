--liquibase formatted sql

--changeset postgres:add_config_hm_prefix_germplasm_attribute_origin context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6863 Add HM_PREFIX_GERMPLASM_ATTRIBUTE_ORIGIN in platform.config



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_PREFIX_GERMPLASM_ATTRIBUTE_ORIGIN',
        'HM Prefix Germplasm Attribute "Origin" Configuration',
        $$			
            {
                "prefix": "IRRI",
                "delimiter": "-"
            }
        $$,
        1,
        'harvest_manager',
        1,
        'CORB-6863 Add HM_PREFIX_GERMPLASM_ATTRIBUTE_ORIGIN in platform.config'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='HM_PREFIX_GERMPLASM_ATTRIBUTE_ORIGIN';