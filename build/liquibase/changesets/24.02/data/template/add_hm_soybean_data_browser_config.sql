--liquibase formatted sql

--changeset postgres:add_config_hm_soybean_data_browser_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6856 24.02 HM: Create databrowser and nomenclature configuration records for SOYBEAN harvest use case



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_DATA_BROWSER_CONFIG_SOYBEAN',
        'Harvest Manager Data Browser Configuration for SOYBEAN',
        $${
            "default": {
                "default": {
                    "default": {
                        "input_columns": [],
                        "numeric_variables": [],
                        "additional_required_variables": []
                    }
                }
            },
            "CROSS_METHOD_SELFING": {
                "fixed": {
                    "default": {
                        "input_columns": [
                            {
                                "abbrev": "HVDATE_CONT",
                                "required": true,
                                "column_name": "harvestDate",
                                "placeholder": "Harvest Date",
                                "retrieve_scale": false
                            },
                            {
                                "abbrev": "HV_METH_DISC",
                                "required": true,
                                "column_name": "harvestMethod",
                                "placeholder": "Harvest Method",
                                "retrieve_scale": false
                            },
                            {
                                "abbrev": "<none>",
                                "required": null,
                                "column_name": "numericVar",
                                "placeholder": "Not applicable",
                                "retrieve_scale": false
                            }
                        ],
                        "numeric_variables": [
                            {
                                "min": null,
                                "type": "number",
                                "abbrev": "<none>",
                                "sub_type": "single_int",
                                "field_name": "<none>",
                                "placeholder": "Not applicable",
                                "harvest_methods": [
                                    "HV_METH_DISC_BULK"
                                ],
                                "harvest_methods_optional": []
                            }
                        ],
                        "additional_required_variables": []
                    }
                },
                "default": {
                    "default": {
                        "input_columns": [
                            {
                                "abbrev": "HVDATE_CONT",
                                "required": true,
                                "column_name": "harvestDate",
                                "placeholder": "Harvest Date",
                                "retrieve_scale": false
                            },
                            {
                                "abbrev": "HV_METH_DISC",
                                "required": true,
                                "column_name": "harvestMethod",
                                "placeholder": "Harvest Method",
                                "retrieve_scale": false
                            },
                            {
                                "abbrev": "<none>",
                                "required": null,
                                "column_name": "numericVar",
                                "placeholder": "Not applicable",
                                "retrieve_scale": false
                            }
                        ],
                        "numeric_variables": [
                            {
                                "min": null,
                                "type": "number",
                                "abbrev": "<none>",
                                "sub_type": "single_int",
                                "field_name": "<none>",
                                "placeholder": "Not applicable",
                                "harvest_methods": [
                                    ""
                                ],
                                "harvest_methods_optional": []
                            }
                        ],
                        "additional_required_variables": []
                    }
                },
                "unknown": {
                    "default": {
                        "input_columns": [
                            {
                                "abbrev": "HVDATE_CONT",
                                "required": true,
                                "column_name": "harvestDate",
                                "placeholder": "Harvest Date",
                                "retrieve_scale": false
                            },
                            {
                                "abbrev": "HV_METH_DISC",
                                "required": true,
                                "column_name": "harvestMethod",
                                "placeholder": "Harvest Method",
                                "retrieve_scale": false
                            },
                            {
                                "abbrev": "<none>",
                                "required": null,
                                "column_name": "numericVar",
                                "placeholder": "Not applicable",
                                "retrieve_scale": false
                            }
                        ],
                        "numeric_variables": [
                            {
                                "min": null,
                                "type": "number",
                                "abbrev": "<none>",
                                "sub_type": "single_int",
                                "field_name": "<none>",
                                "placeholder": "Not applicable",
                                "harvest_methods": [
                                    ""
                                ],
                                "harvest_methods_optional": []
                            }
                        ],
                        "additional_required_variables": []
                    }
                }
            }
        }$$,
        1,
        'harvest_manager',
        1,
        'CORB-6856 24.02 HM: Create databrowser and nomenclature configuration records for SOYBEAN harvest use case.'
    );



--rollback DELETE FROM platform.config WHERE abbrev='HM_DATA_BROWSER_CONFIG_SOYBEAN';