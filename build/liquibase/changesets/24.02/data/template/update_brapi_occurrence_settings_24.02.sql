--liquibase formatted sql

--changeset postgres:update_brapi_occurrence_settings_24.02.sql context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6876 Add current experiment year and variable BrAPI occurrence settings



UPDATE
	platform.config
SET
	config_value = $${
        "traits": 
            [
                "MC_CONT", 
                "AYLD_CONT"
            ], 
        "metadata": {
            "SEASON": [
                "DS", 
                "WS"
            ], 
            "EXPERIMENT_YEAR": [
                "2024",
                "2023"
            ]
        },
        "germplasm": {
            "showPedigree": true
        },
        "variable": {
            "configureTraits": false
        }
    }$$
WHERE
	abbrev = 'BRAPI_GLOBAL_OCCURRENCE_SETTINGS';

UPDATE
	platform.config
SET
	config_value = $${
        "traits": 
            [
                "MC_CONT", 
                "AYLD_CONT"
            ], 
        "metadata": {
            "SEASON": [
                "DS"
            ], 
            "EXPERIMENT_YEAR": [
                "2024",
                "2023"
            ]
        },
        "germplasm": {
            "showPedigree": true
        },
        "variable": {
            "configureTraits": false
        }
    }$$
WHERE
	abbrev = 'BRAPI_IRSEA_OCCURRENCE_SETTINGS';

UPDATE
	platform.config
SET
	config_value = $${
        "traits": 
            [
                "HT1_CONT", 
                "HT2_CONT"
            ], 
        "metadata": {
            "SEASON": [
                "DS",
                "WS"
            ], 
            "EXPERIMENT_YEAR": [
                "2024",
                "2023"
            ]
        },
        "germplasm": {
            "showPedigree": false
        },
        "variable": {
            "configureTraits": false
        }
    }$$
WHERE
	abbrev = 'BRAPI_COLLABORATOR_OCCURRENCE_SETTINGS';




--rollback UPDATE
--rollback 	platform.config
--rollback SET
--rollback 	config_value = $${
--rollback         "traits": 
--rollback             [
--rollback                 "MC_CONT", 
--rollback                 "AYLD_CONT"
--rollback             ], 
--rollback         "metadata": {
--rollback             "SEASON": [
--rollback                 "DS", 
--rollback                 "WS"
--rollback             ], 
--rollback             "EXPERIMENT_YEAR": [
--rollback                 "2023"
--rollback             ]
--rollback         },
--rollback         "germplasm": {
--rollback             "showPedigree": true
--rollback         }
--rollback     }$$
--rollback WHERE
--rollback 	abbrev = 'BRAPI_GLOBAL_OCCURRENCE_SETTINGS';
--rollback 
--rollback UPDATE
--rollback 	platform.config
--rollback SET
--rollback 	config_value = $${
--rollback         "traits": 
--rollback             [
--rollback                 "MC_CONT", 
--rollback                 "AYLD_CONT"
--rollback             ], 
--rollback         "metadata": {
--rollback             "SEASON": [
--rollback                 "DS"
--rollback             ], 
--rollback             "EXPERIMENT_YEAR": [
--rollback                 "2023"
--rollback             ]
--rollback         },
--rollback         "germplasm": {
--rollback             "showPedigree": true
--rollback         }
--rollback     }$$
--rollback WHERE
--rollback 	abbrev = 'BRAPI_IRSEA_OCCURRENCE_SETTINGS';
--rollback 
--rollback UPDATE
--rollback 	platform.config
--rollback SET
--rollback 	config_value = $${
--rollback         "traits": 
--rollback             [
--rollback                 "HT1_CONT", 
--rollback                 "HT2_CONT"
--rollback             ], 
--rollback         "metadata": {
--rollback             "SEASON": [
--rollback                 "DS", 
--rollback                 "WS"
--rollback             ], 
--rollback             "EXPERIMENT_YEAR": [
--rollback                 "2023"
--rollback             ]
--rollback         },
--rollback         "germplasm": {
--rollback             "showPedigree": false
--rollback         }
--rollback     }$$
--rollback WHERE
--rollback 	abbrev = 'BRAPI_COLLABORATOR_OCCURRENCE_SETTINGS';
