--liquibase formatted sql

--changeset postgres:add_config_harvested_package_prefix_pattern_default context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6855 Add HARVESTED_PACKAGE_PREFIX_PATTERN config for Cowpea and Soybean in platform.config



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HARVESTED_PACKAGE_PREFIX_PATTERN_COWPEA_DEFAULT',
        'HM: Harvested Package Prefix Pattern Cowpea Default',
        $$			
            {
            "pattern": [
                {
                    "type": "field",
                    "field_name": "programCode",
                    "order_number": 0
                },
                {
                    "type": "field",
                    "field_name": "experimentYearYY",
                    "order_number": 1
                },
                {
                    "type": "field",
                    "field_name": "experimentSeasonCode",
                    "order_number": 2
                }
            ]
        }
        $$,
        1,
        'harvest_manager',
        1,
        'CORB-6855 Add HARVESTED_PACKAGE_PREFIX_PATTERN config for Cowpea and Soybean in platform.config'
    ),
    (
        'HARVESTED_PACKAGE_PREFIX_PATTERN_SOYBEAN_DEFAULT',
        'HM: Harvested Package Prefix Pattern Soybean Default',
        $$			
            {
            "pattern": [
                {
                    "type": "field",
                    "field_name": "programCode",
                    "order_number": 0
                },
                {
                    "type": "field",
                    "field_name": "experimentYearYY",
                    "order_number": 1
                },
                {
                    "type": "field",
                    "field_name": "experimentSeasonCode",
                    "order_number": 2
                }
            ]
        }
        $$,
        1,
        'harvest_manager',
        1,
        'CORB-6855 Add HARVESTED_PACKAGE_PREFIX_PATTERN config for Cowpea and Soybean in platform.config'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='HARVESTED_PACKAGE_PREFIX_PATTERN_COWPEA_DEFAULT';
--rollback DELETE FROM platform.config WHERE abbrev='HARVESTED_PACKAGE_PREFIX_PATTERN_SOYBEAN_DEFAULT';