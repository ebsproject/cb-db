--liquibase formatted sql

--changeset postgres:add_config_hm_label_pattern_package_cowpea_default context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6855 Add HM_LABEL_PATTERN_PACKAGE_COWPEA_DEFAULT for Cowpea in platform.config



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_LABEL_PATTERN_PACKAGE_COWPEA_DEFAULT',
        'Harvest Manager Cowpea Package Label Pattern-Default',
        $$			
            {
                "PLOT": {
                    "default": {
                        "default": {
                            "default": {
                                "default": [
                                    {
                                        "type": "field",
                                        "entity": "plot",
                                        "field_name": "harvestedPackagePrefix",
                                        "order_number": 0
                                    },
                                    {
                                        "type": "delimiter",
                                        "value": "-",
                                        "order_number": 1
                                    },
                                    {
                                        "type": "counter",
                                        "max_digits": 4,
                                        "leading_zero": "yes",
                                        "order_number": 2
                                    }
                                ]
                            }
                        }
                    }
                },
                "CROSS": {
                    "default": {
                        "default": {
                            "default": {
                                "default": [
                                    {
                                        "type": "field",
                                        "entity": "cross",
                                        "field_name": "harvestedPackagePrefix",
                                        "order_number": 0
                                    },
                                    {
                                        "type": "delimiter",
                                        "value": "-",
                                        "order_number": 1
                                    },
                                    {
                                        "type": "counter",
                                        "max_digits": 4,
                                        "leading_zero": "yes",
                                        "order_number": 2
                                    }
                                ]
                            }
                        }
                    }
                },
                "harvest_mode": {
                    "cross_method": {
                        "germplasm_state": {
                            "germplasm_type": {
                                "harvest_method": [
                                    {
                                        "type": "free-text",
                                        "value": "ABC",
                                        "order_number": 0
                                    },
                                    {
                                        "type": "field",
                                        "entity": "<entity>",
                                        "field_name": "<field_name>",
                                        "order_number": 1
                                    },
                                    {
                                        "type": "delimiter",
                                        "value": "-",
                                        "order_number": 1
                                    },
                                    {
                                        "type": "counter",
                                        "order_number": 3
                                    },
                                    {
                                        "type": "db-sequence",
                                        "schema": "<schema>",
                                        "order_number": 4,
                                        "sequence_name": "<sequence_name>"
                                    }
                                ]
                            }
                        }
                    }
                }
                }
        $$,
        1,
        'harvest_manager',
        1,
        'CORB-6855 Add HM_LABEL_PATTERN_PACKAGE_COWPEA_DEFAULT for Cowpea in platform.config'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='HM_LABEL_PATTERN_PACKAGE_COWPEA_DEFAULT';