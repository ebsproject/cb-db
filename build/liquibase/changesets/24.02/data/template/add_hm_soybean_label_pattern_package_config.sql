--liquibase formatted sql

--changeset postgres:add_config_hm_soybean_label_pattern_package_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6856 24.02 HM: Create databrowser and nomenclature configuration records for SOYBEAN harvest use case



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_LABEL_PATTERN_PACKAGE_SOYBEAN_DEFAULT',
        'Harvest Manager package label pattern configuration for SOYBEAN',
        $${
            "PLOT": {
                "default": {
                "default": {
                    "default": {
                        "default": [
                            {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "harvestedPackagePrefix",
                            "order_number": 0
                            },
                            {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                            },
                            {
                            "type": "counter",
                            "max_digits": 4,
                            "leading_zero": "yes",
                            "order_number": 2
                            }
                        ]
                    }
                }
                }
            },
            "CROSS": {
                "default": {
                    "default": {
                        "default": {
                            "default": [
                                {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "harvestedPackagePrefix",
                                "order_number": 0
                                },
                                {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                                },
                                {
                                "type": "counter",
                                "max_digits": 4,
                                "leading_zero": "yes",
                                "order_number": 2
                                }
                            ]
                        }
                    }
                }
            },
            "harvest_mode": {
                "cross_method": {
                "germplasm_state": {
                    "germplasm_type": {
                    "harvest_method": [
                        {
                        "type": "free-text",
                        "value": "ABC",
                        "order_number": 0
                        },
                        {
                        "type": "field",
                        "entity": "<entity>",
                        "field_name": "<field_name>",
                        "order_number": 1
                        },
                        {
                        "type": "delimiter",
                        "value": "-",
                        "order_number": 1
                        },
                        {
                        "type": "counter",
                        "order_number": 3
                        },
                        {
                        "type": "db-sequence",
                        "schema": "<schema>",
                        "order_number": 4,
                        "sequence_name": "<sequence_name>"
                        }
                    ]
                    }
                }
                }
            }
        }$$,
        1,
        'harvest_manager',
        1,
        'CORB-6856 24.02 HM: Create databrowser and nomenclature configuration records for SOYBEAN harvest use case.'
    );



--rollback DELETE FROM platform.config WHERE abbrev='HM_LABEL_PATTERN_PACKAGE_SOYBEAN_DEFAULT';