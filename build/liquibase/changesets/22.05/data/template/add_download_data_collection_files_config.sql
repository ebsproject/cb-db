--liquibase formatted sql

--changeset postgres:add_download_data_collection_files_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3220 Create configs for collaborator and default configs for Download Data Collection files (aka Export for Field Book)



INSERT INTO
    platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
VALUES
    (
        'DOWNLOAD_DATA_COLLECTION_FILES_DEFAULT',
        'Download Data Collection Files Default',
        '{
            "DOWNLOAD_FOR_FIELD_BOOK_CSV_COLUMNS": [
                {
                    "attribute": "plotCode",
                    "abbrev": "PLOT_CODE"
                },
                {
                    "attribute": "plotNumber",
                    "abbrev": "PLOTNO"
                },
                {
                    "attribute": "plotRep",
                    "abbrev": "REP"
                },
                {
                    "attribute": "entryNumber",
                    "abbrev": "ENTNO"
                },
                {
                    "attribute": "entryName",
                    "abbrev": "DESIGNATION"
                },
                {
                    "attribute": "parentage",
                    "abbrev": "PARENTAGE"
                },
                {
                    "attribute": "fieldX",
                    "abbrev": "FIELD_X"
                },
                {
                    "attribute": "fieldY",
                    "abbrev": "FIELD_Y"
                },
                {
                    "attribute": "occurrenceDbId",
                    "abbrev": "OCCURRENCE_ID"
                },
                {
                    "attribute": "occurrenceName",
                    "abbrev": "OCCURRENCE_NAME"
                },
                {
                    "attribute": "occurrenceCode",
                    "abbrev": "OCCURRENCE_CODE"
                },
                {
                    "attribute": "locationDbId",
                    "abbrev": "LOCATION_ID"
                },
                {
                    "attribute": "locationName",
                    "abbrev": "LOCATION_NAME"
                },
                {
                    "attribute": "locationCode",
                    "abbrev": "LOCATION_CODE"
                }
            ]
        }',
        'data_collection'
    ),
    (
        'DOWNLOAD_DATA_COLLECTION_FILES_COLLABORATOR',
        'Download Data Collection Files Collaborator',
        '{
            "DOWNLOAD_FOR_FIELD_BOOK_CSV_COLUMNS": [
                {
                    "attribute": "plotCode",
                    "abbrev": "PLOT_CODE"
                },
                {
                    "attribute": "plotNumber",
                    "abbrev": "PLOTNO"
                },
                {
                    "attribute": "plotRep",
                    "abbrev": "REP"
                },
                {
                    "attribute": "entryNumber",
                    "abbrev": "ENTNO"
                },
                {
                    "attribute": "fieldX",
                    "abbrev": "FIELD_X"
                },
                {
                    "attribute": "fieldY",
                    "abbrev": "FIELD_Y"
                },
                {
                    "attribute": "occurrenceDbId",
                    "abbrev": "OCCURRENCE_ID"
                },
                {
                    "attribute": "occurrenceName",
                    "abbrev": "OCCURRENCE_NAME"
                },
                {
                    "attribute": "occurrenceCode",
                    "abbrev": "OCCURRENCE_CODE"
                },
                {
                    "attribute": "locationDbId",
                    "abbrev": "LOCATION_ID"
                },
                {
                    "attribute": "locationName",
                    "abbrev": "LOCATION_NAME"
                },
                {
                    "attribute": "locationCode",
                    "abbrev": "LOCATION_CODE"
                }
            ]
        }',
        'data_collection'
    );



--rollback DELETE FROM platform.config WHERE abbrev = 'DOWNLOAD_DATA_COLLECTION_FILES_DEFAULT';
--rollback DELETE FROM platform.config WHERE abbrev = 'DOWNLOAD_DATA_COLLECTION_FILES_COLLABORATOR';
