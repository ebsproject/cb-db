--liquibase formatted sql

--changeset postgres:delete_variable_seed_sample_id context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1201 Delete variable SEED_SAMPLE_ID



DELETE FROM
    master.variable
WHERE
    abbrev = 'SEED_SAMPLE_ID'
;



--rollback INSERT INTO
--rollback     master.variable (abbrev,name,description,label,data_type,not_null,type,usage,status,data_level,display_name,property_id,method_id,scale_id) 
--rollback VALUES 
--rollback     ('SEED_SAMPLE_ID','Seed sample ID',NULL,'Seed sample ID','character varying','false','identification','occurrence','active','occurrence','Seed Sample ID',709,1041,1033) 
--rollback ;