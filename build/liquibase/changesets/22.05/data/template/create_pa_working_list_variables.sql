--liquibase formatted sql

--changeset postgres:create_pa_wl_selected_index_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1215 Create PA_WL_SELECTED_INDEX variable
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT COUNT(*) FROM master.variable WHERE abbrev = 'PA_WL_SELECTED_INDEX';



-- create variable
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id) 
    VALUES 
        ('PA_WL_SELECTED_INDEX', 'PA WL SELECTED INDEX', 'PA Entry Working List Selected Index', 'character varying', false, 'system', 'undefined', 'application', NULL, 'active', '1')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'PA_WL_SELECTED_INDEX' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('PA_WL_SELECTED_INDEX', 'PA Entry Working List Index', 'PA Entry Working List Index') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('PA_WL_SELECTED_INDEX_METHOD', 'PA Entry Working List Selected Index method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('PA_WL_SELECTED_INDEX_SCALE', 'PA WL SELECTED INDEX scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'PA_WL_SELECTED_INDEX'
--rollback     AND t.scale_id = var.scale_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.scale AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'PA_WL_SELECTED_INDEX'
--rollback     AND t.id = var.scale_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.method AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'PA_WL_SELECTED_INDEX'
--rollback     AND t.id = var.method_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.variable AS t
--rollback WHERE
--rollback     t.abbrev = 'PA_WL_SELECTED_INDEX'
--rollback ;



--changeset postgres:create_pa_wl_all_filtered_index_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1215 Create PA_WL_ALL_FILTERED_INDEX variable
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT COUNT(*) FROM master.variable WHERE abbrev = 'PA_WL_ALL_FILTERED_INDEX';



-- create variable
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id) 
    VALUES 
        ('PA_WL_ALL_FILTERED_INDEX', 'PA WL ALL FILTERED INDEX', 'PA Entry Working List All Filtered Index', 'character varying', false, 'system', 'undefined', 'application', NULL, 'active', '1')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'PA_WL_ALL_FILTERED_INDEX' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('PA_WL_ALL_FILTERED_INDEX', 'PA Entry Working List All Filtered Index', 'PA Entry Working List All Filtered Index') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('PA_WL_ALL_FILTERED_INDEX_METHOD', 'PA Entry Working List All Filtered Index method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('PA_WL_ALL_FILTERED_INDEX_SCALE', 'PA WL ALL FILTERED INDEX scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'PA_WL_ALL_FILTERED_INDEX'
--rollback     AND t.scale_id = var.scale_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.scale AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'PA_WL_ALL_FILTERED_INDEX'
--rollback     AND t.id = var.scale_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.method AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'PA_WL_ALL_FILTERED_INDEX'
--rollback     AND t.id = var.method_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.variable AS t
--rollback WHERE
--rollback     t.abbrev = 'PA_WL_ALL_FILTERED_INDEX'
--rollback ;
