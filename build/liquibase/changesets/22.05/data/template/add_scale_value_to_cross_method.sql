--liquibase formatted sql

--changeset postgres:add_scale_value_to_cross_method context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1160 Add scale value to CROSS_METHOD



-- add scale value to CROSS_METHOD variable
INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, creator_id, scale_value_status)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.creator_id,
    scalval.scale_value_status
FROM
    master.variable AS var,
    (
        VALUES
        ('CROSS_METHOD_TRANSGENESIS', 'transgenesis', 15, 'Transgenesis', 'transgenesis', 1, NULL),
        ('CROSS_METHOD_GENOME_EDITING', 'genome editing', 16, 'Genome editing', 'genome editing', 1, NULL)
    ) AS scalval (
        abbrev, value, order_number, description, display_name, creator_id, scale_value_status
    )
WHERE
    var.abbrev = 'CROSS_METHOD'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN ('CROSS_METHOD_TRANSGENESIS','CROSS_METHOD_GENOME_EDITING')
--rollback ;
