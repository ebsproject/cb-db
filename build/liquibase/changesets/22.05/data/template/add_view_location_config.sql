--liquibase formatted sql

--changeset postgres:add_view_location_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3221 Create configs for collaborator and default configs for View and download Location info for Plot tab



INSERT INTO
    platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
VALUES
    (
        'VIEW_LOCATION_DATA_DEFAULT',
        'View Location Data Default',
        '{
            "VIEW_PLOT_COLUMNS": [
                {
                    "attribute": "occurrenceName",
                    "abbrev": "OCCURRENCE_NAME"
                },
                {
                    "attribute": "plotNumber",
                    "abbrev": "PLOTNO"
                },
                {
                    "attribute": "entryNumber",
                    "abbrev": "ENTNO"
                },
                {
                    "attribute": "entryClass",
                    "abbrev": "ENTRY_CLASS"
                },
                {
                    "attribute": "entryName",
                    "abbrev": "GERMPLASM_NAME",
                    "format": "raw",
                    "hasViewGermplasmInfo": {
                        "id": "entryName",
                        "label": "germplasmDbId"
                    }
                },
                {
                    "attribute": "germplasmCode",
                    "abbrev": "GERMPLASM_CODE"
                },
                {
                    "attribute": "germplasmState",
                    "abbrev": "GERMPLASM_STATE"
                },
                {
                    "attribute": "germplasmType",
                    "abbrev": "GERMPLASM_TYPE"
                },
                {
                    "attribute": "parentage",
                    "abbrev": "PARENTAGE"
                },
                {
                    "attribute": "entryCode",
                    "abbrev": "ENTRY_CODE"
                },
                {
                    "attribute": "plotCode",
                    "abbrev": "PLOT_CODE"
                },
                {
                    "attribute": "rep",
                    "abbrev": "REP"
                },
                {
                    "attribute": "designX",
                    "abbrev": "DESIGN_X"
                },
                {
                    "attribute": "designY",
                    "abbrev": "DESIGN_Y"
                },
                {
                    "attribute": "paX",
                    "abbrev": "PA_X"
                },
                {
                    "attribute": "paY",
                    "abbrev": "PA_Y"
                },
                {
                    "attribute": "fieldX",
                    "abbrev": "FIELD_X",
                    "visible": false
                },
                {
                    "attribute": "fieldY",
                    "abbrev": "FIELD_Y",
                    "visible": false
                },
                {
                    "attribute": "blockNumber",
                    "abbrev": "BLOCK_NO_CONT",
                    "visible": false
                }
            ]
        }',
        'experiment_manager'
    ),
    (
        'VIEW_LOCATION_DATA_COLLABORATOR',
        'View Location Data Collaborator',
        '{
            "VIEW_PLOT_COLUMNS": [
                {
                    "attribute": "occurrenceName",
                    "abbrev": "OCCURRENCE_NAME"
                },
                {
                    "attribute": "plotNumber",
                    "abbrev": "PLOTNO"
                },
                {
                    "attribute": "entryNumber",
                    "abbrev": "ENTNO"
                },
                {
                    "attribute": "entryClass",
                    "abbrev": "ENTRY_CLASS"
                },
                {
                    "attribute": "entryCode",
                    "abbrev": "ENTRY_CODE"
                },
                {
                    "attribute": "plotCode",
                    "abbrev": "PLOT_CODE"
                },
                {
                    "attribute": "rep",
                    "abbrev": "REP"
                },
                {
                    "attribute": "designX",
                    "abbrev": "DESIGN_X"
                },
                {
                    "attribute": "designY",
                    "abbrev": "DESIGN_Y"
                },
                {
                    "attribute": "paX",
                    "abbrev": "PA_X"
                },
                {
                    "attribute": "paY",
                    "abbrev": "PA_Y"
                },
                {
                    "attribute": "fieldX",
                    "abbrev": "FIELD_X",
                    "visible": false
                },
                {
                    "attribute": "fieldY",
                    "abbrev": "FIELD_Y",
                    "visible": false
                },
                {
                    "attribute": "blockNumber",
                    "abbrev": "BLOCK_NO_CONT",
                    "visible": false
                }
            ]
        }',
        'experiment_manager'
    );



--rollback DELETE FROM platform.config WHERE abbrev = 'VIEW_LOCATION_DATA_DEFAULT';
--rollback DELETE FROM platform.config WHERE abbrev = 'VIEW_LOCATION_DATA_COLLABORATOR';
