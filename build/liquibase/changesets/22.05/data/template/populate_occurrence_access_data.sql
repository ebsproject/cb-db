--liquibase formatted sql

--changeset postgres:grant_creator_and_program_access_to_occurrence context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1216 Grant creator and program access to occurrence



-- disable triggers
ALTER TABLE experiment.occurrence
    DISABLE TRIGGER occurrence_update_location_document_from_occurrence_tgr
;

ALTER TABLE experiment.occurrence
    DISABLE TRIGGER occurrence_update_occurrence_document_tgr
;

-- populate access_data
WITH t_occurrence AS (
    SELECT
        occ.id AS occurrence_id,
        format($$
            {
                "person": {
                    "%1$s": {
                        "dataRoleId": %3$s,
                        "addedBy": %1$s,
                        "addedOn": "%4$s"
                    }
                },
                "program": {
                    "%2$s": {
                        "dataRoleId": %3$s,
                        "addedBy": %1$s,
                        "addedOn": "%4$s"
                    }
                }
            }
        $$,
            expt.creator_id,
            expt.program_id,
            prsnrole.id,
            now()
        )::json AS access_data
    FROM
        experiment.experiment AS expt
        INNER JOIN experiment.occurrence AS occ
            ON occ.experiment_id = expt.id
        INNER JOIN tenant.person_role AS prsnrole
            ON prsnrole.person_role_code = 'DATA_OWNER'
)
UPDATE
    experiment.occurrence AS occ
SET
    access_data = t.access_data
FROM
    t_occurrence AS t
WHERE
    occ.id = t.occurrence_id
;

-- enable triggers
ALTER TABLE experiment.occurrence
    ENABLE TRIGGER occurrence_update_location_document_from_occurrence_tgr
;

ALTER TABLE experiment.occurrence
    ENABLE TRIGGER occurrence_update_occurrence_document_tgr
;



-- revert changes
--rollback ALTER TABLE experiment.occurrence
--rollback     DISABLE TRIGGER occurrence_update_location_document_from_occurrence_tgr
--rollback ;
--rollback 
--rollback ALTER TABLE experiment.occurrence
--rollback     DISABLE TRIGGER occurrence_update_occurrence_document_tgr
--rollback ;
--rollback 
--rollback UPDATE
--rollback     experiment.occurrence
--rollback SET
--rollback     access_data = NULL
--rollback ;
--rollback 
--rollback ALTER TABLE experiment.occurrence
--rollback     ENABLE TRIGGER occurrence_update_location_document_from_occurrence_tgr
--rollback ;
--rollback 
--rollback ALTER TABLE experiment.occurrence
--rollback     ENABLE TRIGGER occurrence_update_occurrence_document_tgr
--rollback ;
