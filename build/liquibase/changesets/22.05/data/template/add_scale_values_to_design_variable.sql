--liquibase formatted sql

--changeset postgres:add_scale_values_to_design_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3184 Add scale values to DESIGN variable



-- add scale value/s to a variable
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 1) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'DESIGN'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('Incomplete Block Design', 'Incomplete Block Design', 'Incomplete Block Design', 'INCOMPLETE_BLOCK', 'show'),
        ('On-Farm Trial RCBD', 'On-Farm Trial RCBD', 'On-Farm Trial RCBD', 'OFT_RCBD', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'DESIGN'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN (
--rollback         'DESIGN_INCOMPLETE_BLOCK',
--rollback         'DESIGN_OFT_RCBD'
--rollback     )
--rollback ;
