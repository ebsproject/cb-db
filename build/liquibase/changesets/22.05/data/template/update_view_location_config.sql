--liquibase formatted sql

--changeset postgres:add_view_location_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3221 Create configs for collaborator and default configs for View and download Location info for Plot tab


UPDATE
    platform.config
SET
    config_value = '{
            "VIEW_PLOT_COLUMNS": [
                {
                    "attribute": "occurrenceName",
                    "abbrev": "OCCURRENCE_NAME"
                },
                {
                    "attribute": "plotNumber",
                    "abbrev": "PLOTNO"
                },
                {
                    "attribute": "entryNumber",
                    "abbrev": "ENTNO"
                },
                {
                    "attribute": "entryClass",
                    "abbrev": "ENTRY_CLASS"
                },
                {
                    "attribute": "entryName",
                    "abbrev": "GERMPLASM_NAME",
                    "format": "raw",
                    "hasViewGermplasmInfo": {
                        "id": "germplasmDbId",
                        "label": "entryName"
                    }
                },
                {
                    "attribute": "germplasmCode",
                    "abbrev": "GERMPLASM_CODE"
                },
                {
                    "attribute": "germplasmState",
                    "abbrev": "GERMPLASM_STATE"
                },
                {
                    "attribute": "germplasmType",
                    "abbrev": "GERMPLASM_TYPE"
                },
                {
                    "attribute": "parentage",
                    "abbrev": "PARENTAGE"
                },
                {
                    "attribute": "entryCode",
                    "abbrev": "ENTRY_CODE"
                },
                {
                    "attribute": "plotCode",
                    "abbrev": "PLOT_CODE"
                },
                {
                    "attribute": "rep",
                    "abbrev": "REP"
                },
                {
                    "attribute": "designX",
                    "abbrev": "DESIGN_X"
                },
                {
                    "attribute": "designY",
                    "abbrev": "DESIGN_Y"
                },
                {
                    "attribute": "paX",
                    "abbrev": "PA_X"
                },
                {
                    "attribute": "paY",
                    "abbrev": "PA_Y"
                },
                {
                    "attribute": "fieldX",
                    "abbrev": "FIELD_X",
                    "visible": false
                },
                {
                    "attribute": "fieldY",
                    "abbrev": "FIELD_Y",
                    "visible": false
                },
                {
                    "attribute": "blockNumber",
                    "abbrev": "BLOCK_NO_CONT",
                    "visible": false
                }
            ]
        }'
WHERE
    abbrev = 'VIEW_LOCATION_DATA_DEFAULT';


--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = '{
--rollback             "VIEW_PLOT_COLUMNS": [
--rollback                 {
--rollback                     "attribute": "occurrenceName",
--rollback                     "abbrev": "OCCURRENCE_NAME"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "plotNumber",
--rollback                     "abbrev": "PLOTNO"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "entryNumber",
--rollback                     "abbrev": "ENTNO"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "entryClass",
--rollback                     "abbrev": "ENTRY_CLASS"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "entryName",
--rollback                     "abbrev": "GERMPLASM_NAME",
--rollback                     "format": "raw",
--rollback                     "hasViewGermplasmInfo": {
--rollback                         "id": "entryName",
--rollback                         "label": "germplasmDbId"
--rollback                     }
--rollback                 },
--rollback                 {
--rollback                     "attribute": "germplasmCode",
--rollback                     "abbrev": "GERMPLASM_CODE"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "germplasmState",
--rollback                     "abbrev": "GERMPLASM_STATE"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "germplasmType",
--rollback                     "abbrev": "GERMPLASM_TYPE"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "parentage",
--rollback                     "abbrev": "PARENTAGE"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "entryCode",
--rollback                     "abbrev": "ENTRY_CODE"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "plotCode",
--rollback                     "abbrev": "PLOT_CODE"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "rep",
--rollback                     "abbrev": "REP"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "designX",
--rollback                     "abbrev": "DESIGN_X"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "designY",
--rollback                     "abbrev": "DESIGN_Y"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "paX",
--rollback                     "abbrev": "PA_X"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "paY",
--rollback                     "abbrev": "PA_Y"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "fieldX",
--rollback                     "abbrev": "FIELD_X",
--rollback                     "visible": false
--rollback                 },
--rollback                 {
--rollback                     "attribute": "fieldY",
--rollback                     "abbrev": "FIELD_Y",
--rollback                     "visible": false
--rollback                 },
--rollback                 {
--rollback                     "attribute": "blockNumber",
--rollback                     "abbrev": "BLOCK_NO_CONT",
--rollback                     "visible": false
--rollback                 }
--rollback             ]
--rollback         }'
--rollback WHERE
--rollback     abbrev = 'VIEW_LOCATION_DATA_DEFAULT';