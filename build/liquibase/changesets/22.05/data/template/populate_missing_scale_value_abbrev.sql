--liquibase formatted sql

--changeset postgres:populate_missing_scale_value_abbrev context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1179 Populate missing scale value abbrev



-- update scale value abbrev
WITH t_scale_value AS (
    SELECT
        scalval.id AS scale_value_id,
        var.abbrev || '_' || platform.normalize_text_ws(scalval.value) AS scale_value_abbrev
    FROM
        master.variable AS var
        INNER JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        scalval.abbrev IS NULL
)
UPDATE
    master.scale_value AS scalval
SET
    abbrev = t.scale_value_abbrev
FROM
    t_scale_value AS t
WHERE
    scalval.id = t.scale_value_id
;



-- revert changes
--rollback SELECT NULL;
