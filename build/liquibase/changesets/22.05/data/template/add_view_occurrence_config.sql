--liquibase formatted sql

--changeset postgres:add_view_occurrence_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3222 Create configs for collaborator and default configs for View and download Occurrence info for Entry and Plot tab



INSERT INTO
    platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
VALUES
    (
        'VIEW_OCCURRENCE_DATA_DEFAULT',
        'View Occurrence Data Default',
        '{
            "VIEW_ENTRY_COLUMNS": [
                {
                    "attribute": "occurrenceName",
                    "abbrev": "OCCURRENCE_NAME",
                    "visible": false
                },
                {
                    "attribute": "entryNumber",
                    "abbrev": "ENTNO"
                },
                {
                    "attribute": "entryCode",
                    "abbrev": "ENTRY_CODE"
                },
                {
                    "attribute": "entryName",
                    "abbrev": "GERMPLASM_NAME",
                    "format": "raw",
                    "hasViewGermplasmInfo": {
                        "id": "germplasmDbId",
                        "label": "entryName"
                    }
                },
                {
                    "attribute": "germplasmCode",
                    "abbrev": "GERMPLASM_CODE"
                },
                {
                    "attribute": "germplasmState",
                    "abbrev": "GERMPLASM_STATE"
                },
                {
                    "attribute": "germplasmType",
                    "abbrev": "GERMPLASM_TYPE"
                },
                {
                    "attribute": "generation",
                    "abbrev": "GENERATION"
                },
                {
                    "attribute": "parentage",
                    "abbrev": "PARENTAGE"
                },
                {
                    "attribute": "entryType",
                    "abbrev": "ENTRY_TYPE"
                },
                {
                    "attribute": "entryRole",
                    "abbrev": "ENTRY_ROLE"
                },
                {
                    "attribute": "entryClass",
                    "abbrev": "ENTRY_CLASS"
                },
                {
                    "attribute": "seedName",
                    "abbrev": "SEED_NAME"
                },
                {
                    "attribute": "packageLabel",
                    "abbrev": "PACKAGE_LABEL"
                },
                {
                    "attribute": "packageQuantity",
                    "abbrev": "PACKAGE_QUANTITY"
                },
                {
                    "attribute": "packageUnit",
                    "abbrev": "PACKAGE_UNIT"
                }
            ],
            "VIEW_PLOT_COLUMNS": [
                {
                    "attribute": "plotNumber",
                    "abbrev": "PLOTNO"
                },
                {
                    "attribute": "entryNumber",
                    "abbrev": "ENTNO"
                },
                {
                    "attribute": "entryClass",
                    "abbrev": "ENTRY_CLASS"
                },
                {
                    "attribute": "entryName",
                    "abbrev": "GERMPLASM_NAME",
                    "format": "raw",
                    "hasViewGermplasmInfo": {
                        "id": "germplasmDbId",
                        "label": "entryName"
                    }
                },
                {
                    "attribute": "germplasmCode",
                    "abbrev": "GERMPLASM_CODE"
                },
                {
                    "attribute": "germplasmState",
                    "abbrev": "GERMPLASM_STATE"
                },
                {
                    "attribute": "germplasmType",
                    "abbrev": "GERMPLASM_TYPE"
                },
                {
                    "attribute": "parentage",
                    "abbrev": "PARENTAGE"
                },
                {
                    "attribute": "entryCode",
                    "abbrev": "ENTRY_CODE"
                },
                {
                    "attribute": "plotCode",
                    "abbrev": "PLOT_CODE"
                },
                {
                    "attribute": "rep",
                    "abbrev": "REP"
                },
                {
                    "attribute": "designX",
                    "abbrev": "DESIGN_X"
                },
                {
                    "attribute": "designY",
                    "abbrev": "DESIGN_Y"
                },
                {
                    "attribute": "paX",
                    "abbrev": "PA_X"
                },
                {
                    "attribute": "paY",
                    "abbrev": "PA_Y"
                },
                {
                    "attribute": "fieldX",
                    "abbrev": "FIELD_X",
                    "visible": false
                },
                {
                    "attribute": "fieldY",
                    "abbrev": "FIELD_Y",
                    "visible": false
                },
                {
                    "attribute": "rowBlockNumber",
                    "abbrev": "ROW_BLOCK_NUMBER",
                    "visible": false
                },
                {
                    "attribute": "colBlockNumber",
                    "abbrev": "COL_BLOCK_NUMBER",
                    "visible": false
                },
                {
                    "attribute": "blockNumber",
                    "abbrev": "BLOCK_NO_CONT",
                    "visible": false
                }
            ]
        }',
        'experiment_manager'
    ),
    (
        'VIEW_OCCURRENCE_DATA_COLLABORATOR',
        'View Occurrence Data Collaborator',
        '{
            "VIEW_ENTRY_COLUMNS": [
                {
                    "attribute":"occurrenceName",
                    "abbrev": "OCCURRENCE_NAME",
                    "visible": false
                },
                {
                    "attribute":"entryNumber",
                    "abbrev": "ENTNO"
                },
                {
                    "attribute":"entryCode",
                    "abbrev": "ENTRY_CODE"
                },
                {
                    "attribute":"entryClass",
                    "abbrev": "ENTRY_CLASS"
                },
                {
                    "attribute":"seedName",
                    "abbrev": "SEED_NAME"
                },
                {
                    "attribute":"packageLabel",
                    "abbrev": "PACKAGE_LABEL"
                },
                {
                    "attribute":"packageQuantity",
                    "abbrev": "PACKAGE_QUANTITY"
                },
                {
                    "attribute":"packageUnit",
                    "abbrev": "PACKAGE_UNIT"
                }
            ],
            "VIEW_PLOT_COLUMNS": [
                {
                    "attribute":"plotNumber",
                    "abbrev": "PLOTNO"
                },
                {
                    "attribute":"entryNumber",
                    "abbrev": "ENTNO"
                },
                {
                    "attribute":"entryClass",
                    "abbrev": "ENTRY_CLASS"
                },
                {
                    "attribute":"entryCode",
                    "abbrev": "ENTRY_CODE"
                },
                {
                    "attribute":"plotCode",
                    "abbrev": "PLOT_CODE"
                },
                {
                    "attribute":"rep",
                    "abbrev": "REP"
                },
                {
                    "attribute":"designX",
                    "abbrev": "DESIGN_X"
                },
                {
                    "attribute":"designY",
                    "abbrev": "DESIGN_Y"
                },
                {
                    "attribute":"paX",
                    "abbrev": "PA_X"
                },
                {
                    "attribute":"paY",
                    "abbrev": "PA_Y"
                },
                {
                    "attribute":"fieldX",
                    "abbrev": "FIELD_X",
                    "visible": false
                },
                {
                    "attribute":"fieldY",
                    "abbrev": "FIELD_Y",
                    "visible": false
                },
                {
                    "attribute": "rowBlockNumber",
                    "abbrev": "ROW_BLOCK_NUMBER",
                    "visible": false
                },
                {
                    "attribute": "colBlockNumber",
                    "abbrev": "COL_BLOCK_NUMBER",
                    "visible": false
                },
                {
                    "attribute": "blockNumber",
                    "abbrev": "BLOCK_NO_CONT",
                    "visible": false
                }
            ]
        }',
        'experiment_manager'
    );



--rollback DELETE FROM platform.config WHERE abbrev = 'VIEW_OCCURRENCE_DATA_DEFAULT';
--rollback DELETE FROM platform.config WHERE abbrev = 'VIEW_OCCURRENCE_DATA_COLLABORATOR';
