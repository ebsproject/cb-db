--liquibase formatted sql

--changeset postgres:delete_dangling_scale_values context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1179 Delete dangling scale values



-- delete scale values
DELETE FROM
    master.scale_value AS scalval
WHERE
    scalval.scale_id IN (
        SELECT
            scal.id AS scale_id
        FROM
            master.scale AS scal
            LEFT JOIN master.variable AS var
                ON var.scale_id = scal.id
        WHERE
            var.scale_id IS NULL
    )
;



-- revert changes
--rollback -- check backup CSV file
--rollback SELECT NULL;



--changeset postgres:delete_dangling_scales context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1179 Delete dangling scales



-- delete scales
DELETE FROM
    master.scale AS scal
WHERE
    scal.id IN (
        SELECT
            scal.id AS scale_id
        FROM
            master.scale AS scal
            LEFT JOIN master.variable AS var
                ON var.scale_id = scal.id
        WHERE
            var.scale_id IS NULL
    )
;



-- revert changes
--rollback -- check backup CSV file
--rollback SELECT NULL;
