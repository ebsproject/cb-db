--liquibase formatted sql

--changeset postgres:add_collaborator_space context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1182 Add collaborator space



INSERT INTO platform.space (abbrev, name, menu_data, description, order_number, creator_id) 
VALUES (
    'COLLABORATOR', 
    'collaborator', 
    '{
        "left_menu_items": [{
            "name": "experiment-manager",
            "label": "Experiment manager",
            "appAbbrev": "OCCURRENCES"
        }, {
            "name": "data-collection-qc-quality-control",
            "label": "Data collection",
            "appAbbrev": "QUALITY_CONTROL"
        }, {
            "name": "search-traits",
            "label": "Traits",
            "appAbbrev": "TRAITS"
        }],
        "main_menu_items": [{
            "icon": "help",
            "name": "help",
            "items": [{
                    "url": "https://ebsproject.atlassian.net/servicedesk/customer/portal/2",
                    "icon": "help",
                    "name": "help_support-portal",
                    "label": "Support Desk",
                    "tooltip": "Go to EBS Service Desk portal"
                },
                {
                    "url": "https://ebsproject.atlassian.net/wiki/spaces/EUG/overview",
                    "icon": "book",
                    "name": "help_user-guides",
                    "label": "User Guides",
                    "tooltip": "Go to EBS User Guides"
                }, {
                    "url": "https://cbapi-dev.ebsproject.org",
                    "icon": "code",
                    "name": "help_api",
                    "label": "API",
                    "tooltip": "Go to EBS CB-API Documentation"
                }
            ],
            "label": "Help"
        }]
    }', 
    'Space for Collaborator program', 
    2,
    1
);

--rollback DELETE FROM platform.space WHERE abbrev='COLLABORATOR';
