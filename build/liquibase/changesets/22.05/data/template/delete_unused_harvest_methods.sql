--liquibase formatted sql

--changeset postgres:delete_unused_harvest_methods context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1181 Delete unused harvest methods



-- delete scale values
WITH t_scalval AS (
    SELECT
        scalval.id,
        scalval.abbrev
    FROM
        master.scale_value AS scalval
        JOIN master.variable AS var
            ON var.scale_id = scalval.scale_id
    WHERE
        var.abbrev = 'HV_METH_DISC'
        AND scalval.abbrev IN (
            'HV_METH_DISC_BULK_FIXEDLINE',
            'HV_METH_DISC_SINGLE_PLANT_SEED_INCREASE',
            'HV_METH_DISC_HEAD_ROW_PURIFICATION',
            'HV_METH_DISC_HEAD_ROWS',
            'HV_METH_DISC_INDIVIDUAL_PANICLE',
            'HV_METH_DISC_INDIVIDUAL_PLANT'
        )
)
DELETE FROM
    master.scale_value AS scalval
USING
    t_scalval AS t
WHERE
    scalval.id = t.id
;

-- update ordering of scale values
WITH t_scalval AS (
    SELECT
        scalval.id,
        ROW_NUMBER() OVER (ORDER BY scalval.order_number) AS order_number
    FROM
        master.scale_value AS scalval
        JOIN master.variable AS var
            ON var.scale_id = scalval.scale_id
    WHERE
        var.abbrev = 'HV_METH_DISC'
    ORDER BY
        scalval.order_number
)
UPDATE
    master.scale_value AS scalval
SET
    order_number = t.order_number
FROM
    t_scalval AS t
WHERE
    scalval.id = t.id
;



-- revert changes
--rollback WITH t_last_order_number AS (
--rollback     SELECT
--rollback         COALESCE(MAX(scalval.order_number), 1) AS last_order_number
--rollback     FROM
--rollback         master.variable AS var
--rollback         LEFT JOIN master.scale_value AS scalval
--rollback             ON scalval.scale_id = var.scale_id
--rollback     WHERE
--rollback         var.abbrev = 'HV_METH_DISC'
--rollback         AND var.is_void = FALSE
--rollback         AND scalval.is_void = FALSE
--rollback )
--rollback INSERT INTO master.scale_value
--rollback     (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
--rollback SELECT
--rollback     var.scale_id,
--rollback     scalval.value,
--rollback     t.last_order_number + ROW_NUMBER() OVER () AS order_number,
--rollback     scalval.description,
--rollback     scalval.display_name,
--rollback     scalval.scale_value_status,
--rollback     var.abbrev || '_' || scalval.abbrev AS abbrev,
--rollback     var.creator_id
--rollback FROM
--rollback     master.variable AS var,
--rollback     t_last_order_number AS t,
--rollback     (
--rollback         VALUES
--rollback         ('Bulk fixedline', 'Bulk fixedline', 'Bulk fixedline', 'BULK_FIXEDLINE', NULL),
--rollback         ('Single plant seed increase', 'Single plant seed increase', 'Single plant seed increase', 'SINGLE_PLANT_SEED_INCREASE', NULL),
--rollback         ('Head-row purification', 'Head-row purification', 'Head-row purification', 'HEAD_ROW_PURIFICATION', NULL),
--rollback         ('Head-rows', 'Head-rows', 'Head-rows', 'HEAD_ROWS', NULL),
--rollback         ('Individual panicle', 'Individual panicle', 'Individual panicle', 'INDIVIDUAL_PANICLE', NULL),
--rollback         ('Individual plant', 'Individual plant', 'Individual plant', 'INDIVIDUAL_PLANT', NULL)
--rollback     ) AS scalval (
--rollback         value, description, display_name, abbrev, scale_value_status
--rollback     )
--rollback WHERE
--rollback     var.abbrev = 'HV_METH_DISC'
--rollback ;
