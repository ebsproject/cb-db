--liquibase formatted sql

--changeset postgres:update_hm_seed_name_config_rice_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3265 HM DB: Update name pattern configs structure



--update germplasm name config
UPDATE platform.config
SET
	config_value = '{
        "PLOT": {
            "default": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "db-sequence",
                                "schema": "germplasm",
                                "order_number": 0,
                                "sequence_name": "gid_seq"
                            }
                        ]
                    }
                }
            }
        },
        "CROSS": {
            "default": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "db-sequence",
                                "schema": "germplasm",
                                "order_number": 0,
                                "sequence_name": "gid_seq"
                            }
                        ]
                    }
                }
            },
            "single cross": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "db-sequence",
                                "schema": "germplasm",
                                "order_number": 0,
                                "sequence_name": "gid_seq"
                            }
                        ]
                    }
                }
            }
        },
        "harvest_mode": {
            "cross_method": {
                "germplasm_state": {
                    "germplasm_type": {
                        "harvest_method": [
                            {
                                "type": "free-text",
                                "value": "ABC",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "<entity>",
                                "field_name": "<field_name>",
                                "order_number": 1
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 3
                            },
                            {
                                "type": "db-sequence",
                                "schema": "<schema>",
                                "order_number": 4,
                                "sequence_name": "<sequence_name>"
                            }
                        ]
                    }
                }
            }
        }
    }
    '
WHERE
	abbrev = 'HM_NAME_PATTERN_SEED_RICE_DEFAULT'
;




--rollback UPDATE platform.config
--rollback SET
--rollback 	config_value = '{
--rollback         "PLOT": {
--rollback             "default": {
--rollback                 "default": {
--rollback                     "default": [
--rollback                         {
--rollback                             "type": "db-sequence",
--rollback                             "schema": "germplasm",
--rollback                             "order_number": 0,
--rollback                             "sequence_name": "gid_seq"
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             }
--rollback         },
--rollback         "CROSS": {
--rollback             "default": {
--rollback                 "default": {
--rollback                     "default": [
--rollback                         {
--rollback                             "type": "db-sequence",
--rollback                             "schema": "germplasm",
--rollback                             "order_number": 0,
--rollback                             "sequence_name": "gid_seq"
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             "single cross": {
--rollback                 "default": {
--rollback                     "default": [
--rollback                         {
--rollback                             "type": "db-sequence",
--rollback                             "schema": "germplasm",
--rollback                             "order_number": 0,
--rollback                             "sequence_name": "gid_seq"
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             }
--rollback         },
--rollback         "harvest_mode": {
--rollback             "cross_method": {
--rollback                 "germplasm_state/germplasm_type": {
--rollback                     "harvest_method": [
--rollback                         {
--rollback                             "type": "free-text",
--rollback                             "value": "ABC",
--rollback                             "order_number": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "<entity>",
--rollback                             "field_name": "<field_name>",
--rollback                             "order_number": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "order_number": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "counter",
--rollback                             "order_number": 3
--rollback                         },
--rollback                         {
--rollback                             "type": "db-sequence",
--rollback                             "schema": "<schema>",
--rollback                             "order_number": 4,
--rollback                             "sequence_name": "<sequence_name>"
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             }
--rollback         }
--rollback     }
--rollback     '
--rollback WHERE
--rollback 	abbrev = 'HM_NAME_PATTERN_SEED_RICE_DEFAULT'
--rollback ;