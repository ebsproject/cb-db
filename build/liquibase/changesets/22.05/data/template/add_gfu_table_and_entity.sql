--liquibase formatted sql

--changeset postgres:add_gfu_table_and_entity context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3334 GS DB: Add new table and entity for germplasm.file_upload



-- add GERMPLASM.FILE_UPLOAD table
INSERT INTO
    dictionary.table (database_id, schema_id, abbrev, name, comment, creator_id)
SELECT
	1,
	id,
	'GERMPLASM_FILE_UPLOAD',
	'file_upload',
	'Germplasm file upload transaction record',
	1
FROM
	dictionary.schema
WHERE
	abbrev = 'GERMPLASM';

-- add GERMPLASM.FILE_UPLOAD entity
INSERT INTO
    dictionary.entity (abbrev, name, description, creator_id, table_id)
SELECT
	'GERMPLASM_FILE_UPLOAD',
	'germplasm file upload',
	'Germplasm file upload transaction record',
	1,
	id
FROM
	dictionary.table
WHERE
	abbrev = 'GERMPLASM_FILE_UPLOAD';


-- revert changes
--rollback DELETE FROM
--rollback     dictionary.entity AS tablent
--rollback USING
--rollback     dictionary.table AS tabl
--rollback WHERE
--rollback     tabl.abbrev = 'GERMPLASM_FILE_UPLOAD'
--rollback     AND tabl.id = tablent.table_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     dictionary.table
--rollback WHERE
--rollback     abbrev = 'GERMPLASM_FILE_UPLOAD'
--rollback ;
