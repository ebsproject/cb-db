--liquibase formatted sql

--changeset postgres:add_new_breeding_stages context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1159 Add new breeding stages



-- add stages
INSERT INTO
    tenant.stage (
        stage_code,
        stage_name,
        description,
        creator_id
    )
SELECT
    t.stage_code,
    t.stage_name,
    t.stage_name AS description,
    1 AS creator_id
FROM (
        VALUES
        ('PT', 'Plant Transformation'),
        ('T0', 'Genome-engineered Line 0'),
        ('T1', 'Genome-engineered Line 1'),
        ('T2', 'Genome-engineered Line 2'),
        ('T3', 'Genome-engineered Line 3'),
        ('T4', 'Genome-engineered Line 4'),
        ('T5', 'Genome-engineered Line 5'),
        ('T6', 'Genome-engineered Line 6'),
        ('T7', 'Genome-engineered Line 7'),
        ('T8', 'Genome-engineered Line 8'),
        ('T9', 'Genome-engineered Line 9'),
        ('T10', 'Genome-engineered Line 10')
    ) AS t (
        stage_code,
        stage_name
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     tenant.stage
--rollback WHERE
--rollback     stage_code IN (
--rollback         'PT', 'T0', 'T1', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'T8', 'T9', 'T10'
--rollback     )
--rollback ;
--rollback 
--rollback SELECT SETVAL('tenant.stage_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM tenant.stage;
