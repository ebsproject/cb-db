--liquibase formatted sql

--changeset postgres:add_germplasm_data_level_to_variables context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1168 Add germplasm data level to variables



UPDATE
    master.variable
SET
    data_level = 'plot, entry, seed, germplasm'
WHERE
    abbrev = 'DESIGNATION'
;

UPDATE
    master.variable
SET
    data_level = 'germplasm'
WHERE
    abbrev IN (
        'GENERATION',
        'GERMPLASM_NAME_TYPE',
        'GERMPLASM_STATE',
        'GERMPLASM_TYPE'
    )
;

UPDATE
    master.variable
SET
    data_level = 'entry, germplasm'
WHERE
    abbrev = 'PARENTAGE'
;



-- revert changes
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     data_level = 'plot, entry, seed'
--rollback WHERE
--rollback     abbrev = 'DESIGNATION'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     data_level = NULL
--rollback WHERE
--rollback     abbrev IN (
--rollback         'GENERATION',
--rollback         'GERMPLASM_NAME_TYPE',
--rollback         'GERMPLASM_STATE',
--rollback         'GERMPLASM_TYPE'
--rollback     )
--rollback ;
--rollback 
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     data_level = 'entry'
--rollback WHERE
--rollback     abbrev = 'PARENTAGE'
--rollback ;



--changeset postgres:add_seed_data_level_to_variables context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1168 Add seed data level to variables



UPDATE
    master.variable
SET
    data_level = 'experiment, occurrence, location, seed'
WHERE
    abbrev = 'DESCRIPTION'
;

UPDATE
    master.variable
SET
    data_level = 'entry, plot, seed, cross'
WHERE
    abbrev = 'HVDATE_CONT'
;

UPDATE
    master.variable
SET
    data_level = 'plot, entry, cross, seed'
WHERE
    abbrev = 'HV_METH_DISC'
;

UPDATE
    master.variable
SET
    data_level = 'seed'
WHERE
    abbrev IN (
        'IP_STATUS',
        'MTA_STATUS',
        'ORIGIN'
    )
;



-- revert changes
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     data_level = 'experiment, occurrence, location'
--rollback WHERE
--rollback     abbrev = 'DESCRIPTION'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     data_level = 'plot, entry, cross'
--rollback WHERE
--rollback     abbrev = 'HV_METH_DISC'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     data_level = NULL
--rollback WHERE
--rollback     abbrev IN (
--rollback         'IP_STATUS',
--rollback         'MTA_STATUS',
--rollback         'ORIGIN'
--rollback     )
--rollback ;



--changeset postgres:add_package_data_level_to_variables context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1168 Add package data level to variables



UPDATE
    master.variable
SET
    data_level = 'experiment, occurrence, package'
WHERE
    abbrev = 'PACKAGE_STATUS'
;

UPDATE
    master.variable
SET
    data_level = 'occurrence, package'
WHERE
    abbrev = 'PROGRAM'
;

UPDATE
    master.variable
SET
    data_level = 'package'
WHERE
    abbrev IN (
        'LABEL',
        'UNIT',
        'VOLUME'
    )
;



-- revert changes
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     data_level = 'experiment, occurrence'
--rollback WHERE
--rollback     abbrev = 'PACKAGE_STATUS'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     data_level = 'occurrence'
--rollback WHERE
--rollback     abbrev = 'PROGRAM'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     data_level = NULL
--rollback WHERE
--rollback     abbrev IN (
--rollback         'LABEL',
--rollback         'UNIT',
--rollback         'VOLUME'
--rollback     )
--rollback ;
