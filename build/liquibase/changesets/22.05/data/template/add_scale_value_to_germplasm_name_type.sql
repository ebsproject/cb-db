--liquibase formatted sql

--changeset postgres:add_scale_value_to_germplasm_name_type context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1161 Add scale value to GERMPLASM_NAME_TYPE



-- add scale value to GERMPLASM_NAME_TYPE variable
INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, creator_id, scale_value_status)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.creator_id,
    scalval.scale_value_status
FROM
    master.variable AS var,
    (
        VALUES
        ('GERMPLASM_NAME_TYPE_TRANSGENIC_EVENT_ID', 'transgenic_event_id', 34, 'Transgenic Event ID', 'transgenic_event_id', 1, NULL)
    ) AS scalval (
        abbrev, value, order_number, description, display_name, creator_id, scale_value_status
    )
WHERE
    var.abbrev = 'GERMPLASM_NAME_TYPE'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN ('GERMPLASM_NAME_TYPE_TRANSGENIC_EVENT_ID')
--rollback ;
