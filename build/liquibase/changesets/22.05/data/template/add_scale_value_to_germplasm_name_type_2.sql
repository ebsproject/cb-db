--liquibase formatted sql

--changeset postgres:add_gene_construct_name_scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1228 Add gene_construct_name scale value to GERMPLASM_NAME_TYPE variable



-- add scale value/s to a variable
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 1) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'GERMPLASM_NAME_TYPE'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('gene_construct_name', 'gene_construct_name', 'gene_construct_name', 'GENE_CONSTRUCT_NAME', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'GERMPLASM_NAME_TYPE'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN (
--rollback         'GERMPLASM_NAME_TYPE_GENE_CONSTRUCT_NAME'
--rollback     )
--rollback ;
