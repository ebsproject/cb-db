--liquibase formatted sql

--changeset postgres:transfer_data_from_package_trait_table context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1122 Transfer data from germplasm.package_trait to germplasm.package_data table



-- transfer data
INSERT INTO
    germplasm.package_data (
        package_id,
        variable_id,
        data_value,
        data_qc_code,
        creator_id,
        creation_timestamp,
        modifier_id,
        modification_timestamp,
        is_void,
        notes,
        event_log
    )
SELECT
    package_id,
    variable_id,
    data_value,
    data_qc_code,
    creator_id,
    creation_timestamp,
    modifier_id,
    modification_timestamp,
    is_void,
    platform.append_text(notes, 'DB-1122 package_trait'),
    event_log
FROM
    germplasm.package_trait
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.package_data
--rollback WHERE
--rollback     notes LIKE '%DB-1122 package_trait%'
--rollback ;



--changeset postgres:truncate_package_trait_table context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1122 Truncate germplasm.package_trait table



-- truncate table
TRUNCATE germplasm.package_trait;

-- reset sequence
SELECT SETVAL('germplasm.package_trait_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM germplasm.package_trait;



-- revert changes
--rollback INSERT INTO
--rollback     germplasm.package_trait (
--rollback         package_id,
--rollback         variable_id,
--rollback         data_value,
--rollback         data_qc_code,
--rollback         creator_id,
--rollback         creation_timestamp,
--rollback         modifier_id,
--rollback         modification_timestamp,
--rollback         is_void,
--rollback         notes,
--rollback         event_log
--rollback     )
--rollback SELECT
--rollback     package_id,
--rollback     variable_id,
--rollback     data_value,
--rollback     data_qc_code,
--rollback     creator_id,
--rollback     creation_timestamp,
--rollback     modifier_id,
--rollback     modification_timestamp,
--rollback     is_void,
--rollback     platform.detach_text(notes, 'DB-1122 package_trait'),
--rollback     event_log
--rollback FROM
--rollback     germplasm.package_data
--rollback WHERE
--rollback     notes LIKE '%DB-1122 package_trait%'
--rollback ;
