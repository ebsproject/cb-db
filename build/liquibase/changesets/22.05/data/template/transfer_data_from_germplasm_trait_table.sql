--liquibase formatted sql

--changeset postgres:transfer_data_from_germplasm_trait context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1187 Transfer data from germplasm.germplasm_trait to germplasm.germplasm_attribute table



-- transfer data
INSERT INTO
    germplasm.germplasm_attribute (
        germplasm_id,
        variable_id,
        data_value,
        data_qc_code,
        creator_id,
        creation_timestamp,
        modifier_id,
        modification_timestamp,
        is_void,
        notes,
        event_log
    )
SELECT
    germplasm_id,
    variable_id,
    data_value,
    data_qc_code,
    creator_id,
    creation_timestamp,
    modifier_id,
    modification_timestamp,
    is_void,
    platform.append_text(notes, 'DB-1187 germplasm_trait'),
    event_log
FROM
    germplasm.germplasm_trait
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.germplasm_attribute
--rollback WHERE
--rollback     notes LIKE '%DB-1187 germplasm_trait%'
--rollback ;



--changeset postgres:truncate_germplasm_trait_table context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1187 Truncate germplasm.germplasm_trait table



-- truncate table
TRUNCATE germplasm.germplasm_trait;

-- reset sequence
SELECT SETVAL('germplasm.germplasm_trait_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM germplasm.germplasm_trait;



-- revert changes
--rollback INSERT INTO
--rollback     germplasm.germplasm_trait (
--rollback         germplasm_id,
--rollback         variable_id,
--rollback         data_value,
--rollback         data_qc_code,
--rollback         creator_id,
--rollback         creation_timestamp,
--rollback         modifier_id,
--rollback         modification_timestamp,
--rollback         is_void,
--rollback         notes,
--rollback         event_log
--rollback     )
--rollback SELECT
--rollback     germplasm_id,
--rollback     variable_id,
--rollback     data_value,
--rollback     data_qc_code,
--rollback     creator_id,
--rollback     creation_timestamp,
--rollback     modifier_id,
--rollback     modification_timestamp,
--rollback     is_void,
--rollback     platform.detach_text(notes, 'DB-1187 germplasm_trait'),
--rollback     event_log
--rollback FROM
--rollback     germplasm.germplasm_attribute
--rollback WHERE
--rollback     notes LIKE '%DB-1187 germplasm_trait%'
--rollback ;
