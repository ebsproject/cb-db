--liquibase formatted sql

--changeset postgres:add_stage_to_breeding_trial_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3183 EC:As a user, I should be able to see "On-Farm Trial" option when selecting stage of a Breeding Trial experiment



--update breeding trial basic info config
UPDATE platform.config
SET
    config_value = '{
        "Name": "Required experiment level metadata variables for Breeding Trial data process",
        "Values": [
            {
              "default": false,
              "disabled": true,
              "required": "required",
              "target_value": "",
              "target_column": "",
              "variable_type": "identification",
              "variable_abbrev": "EXPERIMENT_TYPE",
              "api_resource_sort": "",
              "api_resource_filter": "",
              "api_resource_method": "",
              "api_resource_endpoint": "",
              "secondary_target_column": ""
            },
            {
              "default": "RICE",
              "disabled": true,
              "required": "required",
              "target_value": "cropCode",
              "target_column": "cropDbId",
              "variable_type": "identification",
              "variable_abbrev": "CROP",
              "api_resource_sort": "sort=cropCode",
              "api_resource_filter": "",
              "api_resource_method": "GET",
              "api_resource_endpoint": "crops"
            },
            {
              "disabled": true,
              "required": "required",
              "target_value": "programCode",
              "target_column": "programDbId",
              "variable_type": "identification",
              "variable_abbrev": "PROGRAM",
              "api_resource_sort": "sort=programCode",
              "api_resource_filter": "",
              "api_resource_method": "GET",
              "api_resource_endpoint": "programs"
            },
            {
              "default": "EXPT-XXXX",
              "disabled": true,
              "required": "required",
              "target_value": "",
              "target_column": "",
              "variable_type": "identification",
              "variable_abbrev": "EXPERIMENT_CODE",
              "api_resource_sort": "",
              "api_resource_filter": "",
              "api_resource_method": "",
              "api_resource_endpoint": "",
              "secondary_target_column": ""
            },
            {
              "disabled": false,
              "required": "required",
              "target_value": "",
              "target_column": "",
              "variable_type": "identification",
              "variable_abbrev": "EXPERIMENT_NAME",
              "api_resource_sort": "",
              "api_resource_filter": "",
              "api_resource_method": "",
              "api_resource_endpoint": "",
              "secondary_target_column": ""
            },
            {
              "disabled": false,
              "required": "required",
              "target_value": "stageCode",
              "target_column": "stageDbId",
              "variable_type": "identification",
              "allowed_values": [
                "IYT",
                "OYT",
                "PYT",
                "AYT",
                "MET0",
                "MET1",
                "MET2",
                "RYT",
                "OFT"
              ],
              "variable_abbrev": "STAGE",
              "api_resource_sort": "sort=stageCode",
              "api_resource_filter": "",
              "api_resource_method": "POST",
              "api_resource_endpoint": "stages-search"
            },
            {
              "disabled": false,
              "required": "required",
              "target_value": "",
              "target_column": "",
              "variable_type": "identification",
              "variable_abbrev": "EXPERIMENT_YEAR",
              "api_resource_sort": "",
              "api_resource_filter": "",
              "api_resource_method": "",
              "api_resource_endpoint": "",
              "secondary_target_column": ""
            },
            {
              "disabled": false,
              "required": "required",
              "target_value": "seasonCode",
              "target_column": "seasonDbId",
              "variable_type": "identification",
              "variable_abbrev": "SEASON",
              "api_resource_sort": "sort=seasonCode",
              "api_resource_filter": "",
              "api_resource_method": "GET",
              "api_resource_endpoint": "seasons"
            },
            {
              "disabled": false,
              "required": "required",
              "target_value": "personName",
              "target_column": "stewardDbId",
              "variable_type": "identification",
              "variable_abbrev": "EXPERIMENT_STEWARD",
              "api_resource_sort": "sort=personName",
              "api_resource_filter": "",
              "api_resource_method": "GET",
              "api_resource_endpoint": "persons",
              "secondary_target_column": "personDbId"
            },
            {
              "disabled": false,
              "target_value": "pipelineCode",
              "target_column": "pipelineDbId",
              "variable_type": "identification",
              "variable_abbrev": "PIPELINE",
              "api_resource_sort": "sort=pipelineCode",
              "api_resource_filter": "",
              "api_resource_method": "GET",
              "api_resource_endpoint": "pipelines"
            },
            {
              "disabled": false,
              "target_value": "",
              "target_column": "",
              "variable_type": "identification",
              "variable_abbrev": "EXPERIMENT_OBJECTIVE",
              "api_resource_sort": "",
              "api_resource_filter": "",
              "api_resource_method": "",
              "api_resource_endpoint": "",
              "secondary_target_column": ""
            },
            {
              "disabled": false,
              "target_value": "",
              "target_column": "",
              "variable_type": "identification",
              "variable_abbrev": "PLANTING_SEASON",
              "api_resource_sort": "",
              "api_resource_filter": "",
              "api_resource_method": "",
              "api_resource_endpoint": "",
              "secondary_target_column": ""
            },
            {
              "disabled": false,
              "target_value": "projectCode",
              "target_column": "projectDbId",
              "variable_type": "identification",
              "variable_abbrev": "PROJECT",
              "api_resource_sort": "sort=projectCode",
              "api_resource_filter": "",
              "api_resource_method": "GET",
              "api_resource_endpoint": "projects"
            },
            {
              "disabled": false,
              "target_value": "value",
              "target_column": "",
              "variable_type": "identification",
              "allowed_values": [
                "Yield Trial",
                "Disease Trial",
                "Physiology Trial",
                "Other Trial"
              ],
              "variable_abbrev": "EXPERIMENT_SUB_TYPE",
              "api_resource_sort": "",
              "api_resource_filter": "",
              "api_resource_method": "",
              "api_resource_endpoint": "",
              "secondary_target_column": ""
            },
            {
              "disabled": false,
              "target_value": "value",
              "target_column": "",
              "variable_type": "identification",
              "allowed_values": [
                "First Year",
                "Second Year",
                "Re Test",
                "Germplasm Viability"
              ],
              "variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE",
              "api_resource_sort": "",
              "api_resource_filter": "",
              "api_resource_method": "",
              "api_resource_endpoint": "",
              "secondary_target_column": ""
            },
            {
              "disabled": false,
              "target_value": "",
              "target_column": "",
              "variable_type": "identification",
              "variable_abbrev": "DESCRIPTION",
              "api_resource_sort": "",
              "api_resource_filter": "",
              "api_resource_method": "",
              "api_resource_endpoint": "",
              "secondary_target_column": ""
            }
          ]
        }'
WHERE
    abbrev = 'BREEDING_TRIAL_BASIC_INFO_ACT_VAL'
;



--rollback UPDATE platform.config
--rollback SET
--rollback 	config_value = '{
--rollback 	  "Name": "Required experiment level metadata variables for Breeding Trial data process",
--rollback    "Values": [
--rollback    {
--rollback      "default": false,
--rollback      "disabled": true,
--rollback      "required": "required",
--rollback      "target_value": "",
--rollback      "target_column": "",
--rollback      "variable_type": "identification",
--rollback      "variable_abbrev": "EXPERIMENT_TYPE",
--rollback      "api_resource_sort": "",
--rollback      "api_resource_filter": "",
--rollback      "api_resource_method": "",
--rollback      "api_resource_endpoint": "",
--rollback      "secondary_target_column": ""
--rollback    },
--rollback    {
--rollback      "default": "RICE",
--rollback      "disabled": true,
--rollback      "required": "required",
--rollback      "target_value": "cropCode",
--rollback      "target_column": "cropDbId",
--rollback      "variable_type": "identification",
--rollback      "variable_abbrev": "CROP",
--rollback      "api_resource_sort": "sort=cropCode",
--rollback      "api_resource_filter": "",
--rollback      "api_resource_method": "GET",
--rollback      "api_resource_endpoint": "crops"
--rollback    },
--rollback    {
--rollback      "disabled": true,
--rollback      "required": "required",
--rollback      "target_value": "programCode",
--rollback      "target_column": "programDbId",
--rollback      "variable_type": "identification",
--rollback      "variable_abbrev": "PROGRAM",
--rollback      "api_resource_sort": "sort=programCode",
--rollback      "api_resource_filter": "",
--rollback      "api_resource_method": "GET",
--rollback      "api_resource_endpoint": "programs"
--rollback    },
--rollback    {
--rollback      "default": "EXPT-XXXX",
--rollback      "disabled": true,
--rollback      "required": "required",
--rollback      "target_value": "",
--rollback      "target_column": "",
--rollback      "variable_type": "identification",
--rollback      "variable_abbrev": "EXPERIMENT_CODE",
--rollback      "api_resource_sort": "",
--rollback      "api_resource_filter": "",
--rollback      "api_resource_method": "",
--rollback      "api_resource_endpoint": "",
--rollback      "secondary_target_column": ""
--rollback    },
--rollback    {
--rollback      "disabled": false,
--rollback      "required": "required",
--rollback      "target_value": "",
--rollback      "target_column": "",
--rollback      "variable_type": "identification",
--rollback      "variable_abbrev": "EXPERIMENT_NAME",
--rollback      "api_resource_sort": "",
--rollback      "api_resource_filter": "",
--rollback      "api_resource_method": "",
--rollback      "api_resource_endpoint": "",
--rollback      "secondary_target_column": ""
--rollback    },
--rollback    {
--rollback      "disabled": false,
--rollback      "required": "required",
--rollback      "target_value": "stageCode",
--rollback      "target_column": "stageDbId",
--rollback      "variable_type": "identification",
--rollback      "allowed_values": [
--rollback        "IYT",
--rollback        "OYT",
--rollback        "PYT",
--rollback        "AYT",
--rollback        "MET0",
--rollback        "MET1",
--rollback        "MET2",
--rollback        "RYT"
--rollback      ],
--rollback      "variable_abbrev": "STAGE",
--rollback      "api_resource_sort": "sort=stageCode",
--rollback      "api_resource_filter": "",
--rollback      "api_resource_method": "POST",
--rollback      "api_resource_endpoint": "stages-search"
--rollback    },
--rollback    {
--rollback      "disabled": false,
--rollback      "required": "required",
--rollback      "target_value": "",
--rollback      "target_column": "",
--rollback      "variable_type": "identification",
--rollback      "variable_abbrev": "EXPERIMENT_YEAR",
--rollback      "api_resource_sort": "",
--rollback      "api_resource_filter": "",
--rollback      "api_resource_method": "",
--rollback      "api_resource_endpoint": "",
--rollback      "secondary_target_column": ""
--rollback    },
--rollback    {
--rollback      "disabled": false,
--rollback      "required": "required",
--rollback      "target_value": "seasonCode",
--rollback      "target_column": "seasonDbId",
--rollback      "variable_type": "identification",
--rollback      "variable_abbrev": "SEASON",
--rollback      "api_resource_sort": "sort=seasonCode",
--rollback      "api_resource_filter": "",
--rollback      "api_resource_method": "GET",
--rollback      "api_resource_endpoint": "seasons"
--rollback    },
--rollback    {
--rollback      "disabled": false,
--rollback      "required": "required",
--rollback      "target_value": "personName",
--rollback      "target_column": "stewardDbId",
--rollback      "variable_type": "identification",
--rollback      "variable_abbrev": "EXPERIMENT_STEWARD",
--rollback      "api_resource_sort": "sort=personName",
--rollback      "api_resource_filter": "",
--rollback      "api_resource_method": "GET",
--rollback      "api_resource_endpoint": "persons",
--rollback      "secondary_target_column": "personDbId"
--rollback    },
--rollback    {
--rollback      "disabled": false,
--rollback      "target_value": "pipelineCode",
--rollback      "target_column": "pipelineDbId",
--rollback      "variable_type": "identification",
--rollback      "variable_abbrev": "PIPELINE",
--rollback      "api_resource_sort": "sort=pipelineCode",
--rollback      "api_resource_filter": "",
--rollback      "api_resource_method": "GET",
--rollback      "api_resource_endpoint": "pipelines"
--rollback    },
--rollback    {
--rollback      "disabled": false,
--rollback      "target_value": "",
--rollback      "target_column": "",
--rollback      "variable_type": "identification",
--rollback      "variable_abbrev": "EXPERIMENT_OBJECTIVE",
--rollback      "api_resource_sort": "",
--rollback      "api_resource_filter": "",
--rollback      "api_resource_method": "",
--rollback      "api_resource_endpoint": "",
--rollback      "secondary_target_column": ""
--rollback    },
--rollback    {
--rollback      "disabled": false,
--rollback      "target_value": "",
--rollback      "target_column": "",
--rollback      "variable_type": "identification",
--rollback      "variable_abbrev": "PLANTING_SEASON",
--rollback      "api_resource_sort": "",
--rollback      "api_resource_filter": "",
--rollback      "api_resource_method": "",
--rollback      "api_resource_endpoint": "",
--rollback      "secondary_target_column": ""
--rollback    },
--rollback    {
--rollback      "disabled": false,
--rollback      "target_value": "projectCode",
--rollback      "target_column": "projectDbId",
--rollback      "variable_type": "identification",
--rollback      "variable_abbrev": "PROJECT",
--rollback      "api_resource_sort": "sort=projectCode",
--rollback      "api_resource_filter": "",
--rollback      "api_resource_method": "GET",
--rollback      "api_resource_endpoint": "projects"
--rollback    },
--rollback    {
--rollback      "disabled": false,
--rollback      "target_value": "value",
--rollback      "target_column": "",
--rollback      "variable_type": "identification",
--rollback      "allowed_values": [
--rollback        "Yield Trial",
--rollback        "Disease Trial",
--rollback        "Physiology Trial",
--rollback        "Other Trial"
--rollback      ],
--rollback      "variable_abbrev": "EXPERIMENT_SUB_TYPE",
--rollback      "api_resource_sort": "",
--rollback      "api_resource_filter": "",
--rollback      "api_resource_method": "",
--rollback      "api_resource_endpoint": "",
--rollback      "secondary_target_column": ""
--rollback    },
--rollback    {
--rollback      "disabled": false,
--rollback      "target_value": "value",
--rollback      "target_column": "",
--rollback      "variable_type": "identification",
--rollback      "allowed_values": [
--rollback        "First Year",
--rollback        "Second Year",
--rollback        "Re Test",
--rollback        "Germplasm Viability"
--rollback      ],
--rollback      "variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE",
--rollback      "api_resource_sort": "",
--rollback      "api_resource_filter": "",
--rollback      "api_resource_method": "",
--rollback      "api_resource_endpoint": "",
--rollback      "secondary_target_column": ""
--rollback    },
--rollback    {
--rollback      "disabled": false,
--rollback      "target_value": "",
--rollback      "target_column": "",
--rollback      "variable_type": "identification",
--rollback      "variable_abbrev": "DESCRIPTION",
--rollback      "api_resource_sort": "",
--rollback      "api_resource_filter": "",
--rollback      "api_resource_method": "",
--rollback      "api_resource_endpoint": "",
--rollback      "secondary_target_column": ""
--rollback    }
--rollback  ]
--rollback }'
--rollback WHERE
--rollback 	abbrev = 'BREEDING_TRIAL_BASIC_INFO_ACT_VAL'
--rollback ;
