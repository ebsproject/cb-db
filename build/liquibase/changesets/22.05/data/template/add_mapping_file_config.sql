--liquibase formatted sql

--changeset postgres:add_mapping_file_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3219 Create configs for collaborator and default configs for Download Mapping file in CSV format



INSERT INTO
    platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
VALUES
    (
        'MAPPING_FILE_DEFAULT',
        'Mapping File Default',
        '{
            "DOWNLOAD_MAPPING_FILE_CSV_COLUMNS": [
                {
                    "attribute": "occurrenceName",
                    "abbrev": "OCCURRENCE_NAME"
                },
                {
                    "attribute": "siteDbId",
                    "abbrev": "SITE_ID"
                },
                {
                    "attribute": "site",
                    "abbrev": "SITE"
                },
                {
                    "attribute": "fieldDbId",
                    "abbrev": "FIELD_ID"
                },
                {
                    "attribute": "field",
                    "abbrev": "FIELD"
                },
                {
                    "attribute": "locationDbId",
                    "abbrev": "LOCATION_ID"
                },
                {
                    "attribute": "locationCode",
                    "abbrev": "LOCATION_CODE"
                },
                {
                    "attribute": "experimentYear",
                    "abbrev": "EXPERIMENT_YEAR"
                },
                {
                    "attribute": "experimentSeasonCode",
                    "abbrev": "SEASON_CODE"
                },
                {
                    "attribute": "experiment",
                    "abbrev": "EXPERIMENT_NAME"
                },
                {
                    "attribute": "experimentType",
                    "abbrev": "EXPERIMENT_TYPE"
                },
                {
                    "attribute": "experimentDesignType",
                    "abbrev": "EXPERIMENT_DESIGN_TYPE"
                },
                {
                    "attribute": "entryNumber",
                    "abbrev": "ENTNO"
                },
                {
                    "attribute": "entryName",
                    "abbrev": "GERMPLASM_NAME"
                },
                {
                    "attribute": "entryType",
                    "abbrev": "ENTRY_TYPE"
                },
                {
                    "attribute": "germplasmCode",
                    "abbrev": "GERMPLASM_CODE"
                },
                {
                    "attribute": "parentage",
                    "abbrev": "PARENTAGE"
                },
                {
                    "attribute": "seedName",
                    "abbrev": "SEED_NAME"
                },
                {
                    "attribute": "rep",
                    "abbrev": "REP"
                },
                {
                    "attribute": "plotCode",
                    "abbrev": "PLOT_CODE"
                },
                {
                    "attribute": "plotNumber",
                    "abbrev": "PLOTNO"
                },
                {
                    "attribute": "blockNumber",
                    "abbrev": "BLOCK_NO_CONT"
                },
                {
                    "attribute": "designX",
                    "abbrev": "DESIGN_X"
                },
                {
                    "attribute": "designY",
                    "abbrev": "DESIGN_Y"
                }
            ]
        }',
        'experiment_manager'
    ),
    (
        'MAPPING_FILE_COLLABORATOR',
        'Mapping File Collaborator',
        '{
            "DOWNLOAD_MAPPING_FILE_CSV_COLUMNS": [
                {
                    "attribute": "occurrenceName",
                    "abbrev": "OCCURRENCE_NAME"
                },
                {
                    "attribute": "siteDbId",
                    "abbrev": "SITE_ID"
                },
                {
                    "attribute": "site",
                    "abbrev": "SITE"
                },
                {
                    "attribute": "fieldDbId",
                    "abbrev": "FIELD_ID"
                },
                {
                    "attribute": "field",
                    "abbrev": "FIELD"
                },
                {
                    "attribute": "locationDbId",
                    "abbrev": "LOCATION_ID"
                },
                {
                    "attribute": "locationCode",
                    "abbrev": "LOCATION_CODE"
                },
                {
                    "attribute": "experimentYear",
                    "abbrev": "EXPERIMENT_YEAR"
                },
                {
                    "attribute": "experimentSeasonCode",
                    "abbrev": "SEASON_CODE"
                },
                {
                    "attribute": "experiment",
                    "abbrev": "EXPERIMENT_NAME"
                },
                {
                    "attribute": "experimentType",
                    "abbrev": "EXPERIMENT_TYPE"
                },
                {
                    "attribute": "experimentDesignType",
                    "abbrev": "EXPERIMENT_DESIGN_TYPE"
                },
                {
                    "attribute": "entryNumber",
                    "abbrev": "ENTNO"
                },
                {
                    "attribute": "entryType",
                    "abbrev": "ENTRY_TYPE"
                },
                {
                    "attribute": "seedName",
                    "abbrev": "SEED_NAME"
                },
                {
                    "attribute": "rep",
                    "abbrev": "REP"
                },
                {
                    "attribute": "plotCode",
                    "abbrev": "PLOT_CODE"
                },
                {
                    "attribute": "plotNumber",
                    "abbrev": "PLOTNO"
                },
                {
                    "attribute": "blockNumber",
                    "abbrev": "BLOCK_NO_CONT"
                },
                {
                    "attribute": "designX",
                    "abbrev": "DESIGN_X"
                },
                {
                    "attribute": "designY",
                    "abbrev": "DESIGN_Y"
                }
            ]
        }',
        'experiment_manager'
    );



--rollback DELETE FROM platform.config WHERE abbrev = 'MAPPING_FILE_DEFAULT';
--rollback DELETE FROM platform.config WHERE abbrev = 'MAPPING_FILE_COLLABORATOR';
