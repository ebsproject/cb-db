--liquibase formatted sql

--changeset postgres:add_cm_application context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1222 Add CM application



-- add CM application
INSERT INTO 
    platform.application (abbrev, label, action_label, icon, creator_id)
VALUES
    ('CROSS_MANAGER', 'Cross manager', 'Cross manager', 'repeat', 1)
;

INSERT INTO 
    platform.application_action (application_id, module, creator_id)
SELECT 
    id,
    'crossManager',
    1
FROM 
    platform.application
WHERE 
    abbrev = 'CROSS_MANAGER'
;



-- revert changes
--rollback DELETE FROM
--rollback     platform.application_action AS appact
--rollback USING
--rollback     platform.application AS app
--rollback WHERE
--rollback     app.abbrev = 'CROSS_MANAGER'
--rollback     AND app.id = appact.application_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     platform.application
--rollback WHERE
--rollback     abbrev = 'CROSS_MANAGER'
--rollback ;
