--liquibase formatted sql

--changeset postgres:update_shipment_variables context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1201 Update shipment variables



UPDATE
    master.variable
SET
    name = 'Origin',
    label = 'ORIGIN',
    description = 'Country where the line was originally bred or collected'
WHERE
    abbrev = 'ORIGIN'
;

UPDATE
    master.variable
SET
    name = 'Source Study',
    description = 'Study where the seeds where harvested from'
WHERE
    abbrev = 'SOURCE_STUDY'
;



--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     name = 'Origin',
--rollback     label = 'Origin',
--rollback     description = NULL
--rollback WHERE
--rollback     abbrev = 'ORIGIN'
--rollback ;
--rollback 
--rollback 
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     name = 'SOURCE_STUDY',
--rollback     description = 'Study where harvested seeds came from'
--rollback WHERE
--rollback     abbrev = 'SOURCE_STUDY'
--rollback ;
