--liquibase formatted sql

--changeset postgres:add_variable_file_status context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1155 Add variable FILE_STATUS



DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
    var_variable_set_id int;
    var_variable_set_member_order_number int;
    var_count_property_id int;
    var_count_method_id int;
    var_count_scale_id int;
    var_count_variable_set_id int;
    var_count_variable_set_member_id int;
BEGIN

    --PROPERTY

    SELECT count(id) FROM master.property WHERE ABBREV = 'FILE_STATUS' INTO var_count_property_id;
    IF var_count_property_id > 0 THEN
        SELECT id FROM master.property WHERE ABBREV = 'FILE_STATUS' INTO var_property_id;
    ELSE
        INSERT INTO
            master.property (abbrev,name,display_name) 
        VALUES 
            ('FILE_STATUS','FILE_STATUS','FILE_STATUS') 
        RETURNING id INTO var_property_id;
    END IF;

    --METHOD

    SELECT count(id) FROM master.method WHERE ABBREV = 'FILE_STATUS' INTO var_count_method_id;
    IF var_count_method_id > 0 THEN
        SELECT id FROM master.method WHERE ABBREV = 'FILE_STATUS' INTO var_method_id;
    ELSE
        INSERT INTO
            master.method (abbrev,name,description,formula) 
        VALUES 
            ('FILE_STATUS','File Status',NULL,NULL) 
        RETURNING id INTO var_method_id;
    END IF;

    --SCALE

    SELECT count(id) FROM master.scale WHERE ABBREV = 'FILE_STATUS' INTO var_count_scale_id;
    IF var_count_scale_id > 0 THEN
        SELECT id FROM master.scale WHERE ABBREV = 'FILE_STATUS' INTO var_scale_id;
    ELSE
        INSERT INTO
            master.scale (abbrev,name,unit,type,level) 
        VALUES 
            ('FILE_STATUS','File Status',NULL,'discrete','nominal') 
        RETURNING id INTO var_scale_id;
    END IF;

    --SCALE VALUE

    INSERT INTO 
        master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
    VALUES 
        (var_scale_id,'in queue',1,'in queue','in queue','FILE_STATUS_IN_QUEUE');
    INSERT INTO 
        master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
    VALUES 
        (var_scale_id,'validation in progress',2,'validation in progress','validation in progress','FILE_STATUS_VALIDATION_IN_PROGRESS');
    INSERT INTO 
        master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
    VALUES 
        (var_scale_id,'validation error',3,'validation error','validation error','FILE_STATUS_VALIDATION_ERROR');
    INSERT INTO 
        master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
    VALUES 
        (var_scale_id,'validated',4,'validated','validated','FILE_STATUS_VALIDATED');
    INSERT INTO 
        master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
    VALUES 
        (var_scale_id,'creation in progress',5,'creation in progress','creation in progress','FILE_STATUS_CREATION_IN_PROGRESS');
    INSERT INTO 
        master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
    VALUES 
        (var_scale_id,'creation failed',6,'creation failed','creation failed','FILE_STATUS_CREATION_FAILED');
    INSERT INTO 
        master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
    VALUES 
        (var_scale_id,'completed',7,'completed','completed','FILE_STATUS_COMPLETED');

    UPDATE master.scale SET min_value='in queue', max_value='completed' WHERE id=var_scale_id;

    --VARIABLE

        INSERT INTO
            master.variable (abbrev,name,description,label,data_type,not_null,type,usage,status,data_level,display_name) 
        VALUES 
            ('FILE_STATUS','File Status','Progress of the file upload transaction {in queue, validation in progress, validation error, validated, creation in progress, creation failed, completed}','FILE STATUS','character varying','true','metadata','application','active',NULL,'File Status') 
        RETURNING id INTO var_variable_id;

    --UPDATE PROPERTY_ID, METHOD_ID, SCALE_ID

    UPDATE master.variable SET property_id=var_property_id, method_id=var_method_id, scale_id=var_scale_id WHERE id=var_variable_id;

    --VARIABLE_SET_ID

    SELECT count(id) FROM master.variable_set WHERE ABBREV = 'GERMPLASM_INFO' INTO var_count_variable_set_id;
    IF var_count_variable_set_id > 0 THEN
        SELECT id FROM master.variable_set WHERE ABBREV = 'GERMPLASM_INFO' INTO var_variable_set_id;
    ELSE
        INSERT INTO
            master.variable_set (abbrev,name) 
        VALUES 
            ('GERMPLASM_INFO','Germplasm Info') 
        RETURNING id INTO var_variable_set_id;
    END IF;

    --GET THE LAST ORDER NUMBER

    SELECT count(id) FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id INTO var_count_variable_set_member_id;
    IF var_count_variable_set_member_id > 0 THEN
        SELECT max(order_number)+1 FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id GROUP BY variable_set_id INTO var_variable_set_member_order_number;
    ELSE
        var_variable_set_member_order_number = 1;
    END IF;

    --ADD VARIABLE SET MEMBER

    INSERT INTO master.variable_set_member (variable_set_id, variable_id, order_number) VALUES (var_variable_set_id, var_variable_id,var_variable_set_member_order_number  );


END;
$$



--rollback DELETE FROM master.scale_value WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'FILE_STATUS');

--rollback DELETE FROM master.scale WHERE id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'FILE_STATUS');

--rollback DELETE FROM master."property" WHERE id IN (SELECT property_id FROM master.variable WHERE abbrev = 'FILE_STATUS');

--rollback DELETE FROM master.method WHERE id IN (SELECT method_id FROM master.variable WHERE abbrev = 'FILE_STATUS');

--rollback DELETE FROM master.variable_set_member WHERE variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'FILE_STATUS');

--rollback DELETE FROM master.variable WHERE abbrev = 'FILE_STATUS';
