--liquibase formatted sql

--changeset postgres:add_new_germplasm_file_upload_variable_config_wheat context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3274 GS: Create new configuration record for germplasm creation via file upload


INSERT INTO
    platform.config(
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES (
    'GM_FILE_UPLOAD_VARIABLES_CONFIG_WHEAT_DEFAULT',
    'Germplasm File Upload variables configuration for WHEAT', 
    $${
        "values":[
            {
                "abbrev": "GERMPLASM_TYPE",
                "name": "Germplasm Type",
                "data_type": "string",
                "usage": "optional",
                "required": "false",
                "entity": "germplasm",
                "visible": "true"
            },
            {
                "abbrev": "GRAIN_COLOR",
                "name": "Grain color",
                "data_type": "string",
                "usage": "required",
                "required": "true",
                "entity": "germplasm",
                "visible": "true"
            },
            {
                "abbrev": "GROWTH_HABIT",
                "name": "Growth habit",
                "data_type": "string",
                "usage": "required",
                "required": "true",
                "entity": "germplasm",
                "visible": "true"
            },
            {
                "abbrev": "PROGRAM_CODE",
                "name": "Program code",
                "data_type": "string",
                "usage": "optional",
                "required": "false",
                "entity": "seed",
                "visible": "true"
            },
            {
                "abbrev": "HVDATE_CONT",
                "name": "Harvest Date",
                "data_type": "date",
                "usage": "optional",
                "required": "false",
                "entity": "seed",
                "visible": "true"
            },
            {
                "abbrev": "HV_METH_DISC",
                "name": "Harvest Method",
                "data_type": "string",
                "usage": "optional",
                "required": "false",
                "entity": "seed",
                "visible": "true"
            },
            {
                "abbrev": "DESCRIPTION",
                "name": "Description",
                "data_type": "string",
                "usage": "optional",
                "required": "false",
                "entity": "seed",
                "visible": "true"
            },
            {
                "abbrev": "MTA_STATUS",
                "name": "MTA Status",
                "data_type": "string",
                "usage": "optional",
                "required": "false",
                "entity": "seed",
                "visible": "true"
            },
            {
                "abbrev": "IP_STATUS",
                "name": "IP Status",
                "data_type": "string",
                "usage": "optional",
                "required": "false",
                "entity": "seed",
                "visible": "true"
            },
            {
                "abbrev": "SOURCE_ORGANIZATION",
                "name": "Source Organization",
                "data_type": "string",
                "usage": "optional",
                "required": "false",
                "entity": "seed",
                "visible": "true"
            },
            {
                "abbrev": "ORIGIN",
                "name": "Origin",
                "data_type": "string",
                "usage": "optional",
                "required": "false",
                "entity": "seed",
                "visible": "true"
            },
            {
                "abbrev": "SOURCE_STUDY",
                "name": "Source study",
                "data_type": "string",
                "usage": "optional",
                "required": "false",
                "entity": "seed",
                "visible": "true"
            },
            {
                "abbrev": "VOLUME",
                "name": "Package Quantity",
                "data_type": "float",
                "usage": "required",
                "required": "true",
                "entity": "package",
                "visible": "true"
            },
            {
                "abbrev": "PACKAGE_UNIT",
                "name": "Unit of measure",
                "data_type": "string",
                "usage": "required",
                "required": "true",
                "entity": "package",
                "visible": "true"
            }
        ]
    }$$,
    1,
    'File upload and data validation',
    1,
    'added by k.delarosa'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_WHEAT_DEFAULT';
