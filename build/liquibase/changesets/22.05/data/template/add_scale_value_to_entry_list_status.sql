--liquibase formatted sql

--changeset postgres:add_scale_value_to_entry_list_status context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1196 Add scale value to ENTRY_LIST_STATUS variable 



-- add scale value/s to a variable
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 1) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'ENTRY_LIST_STATUS'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('finalized', 'finalized', 'finalized', 'FINALIZED', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'ENTRY_LIST_STATUS'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN (
--rollback         'ENTRY_LIST_STATUS_FINALIZED'
--rollback     )
--rollback ;



--changeset postgres:update_check_constraint_of_entry_list_status_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1196 Update check constraint of entry_list_status column



-- drop check constraint
ALTER TABLE experiment.entry_list
    DROP CONSTRAINT entry_list_entry_list_status_chk
;

-- re-create check constraint
ALTER TABLE experiment.entry_list
    ADD CONSTRAINT entry_list_entry_list_status_chk
    CHECK (entry_list_status = ANY(ARRAY['draft', 'created', 'completed', 'finalized']))
;



-- revert changes
--rollback ALTER TABLE experiment.entry_list
--rollback     DROP CONSTRAINT entry_list_entry_list_status_chk
--rollback ;
--rollback 
--rollback ALTER TABLE experiment.entry_list
--rollback     ADD CONSTRAINT entry_list_entry_list_status_chk
--rollback     CHECK (entry_list_status = ANY(ARRAY['draft', 'created', 'completed']))
--rollback ;



--changeset postgres:update_entry_list_status_column_comment context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1196 Update entry_list_status column comment



-- update comment
COMMENT ON COLUMN experiment.entry_list.entry_list_status
    IS 'Entry List Status: Status of the entry list {draft, created, completed, finalized} [ENTLIST_STATUS]';



-- revert changes
--rollback COMMENT ON COLUMN experiment.entry_list.entry_list_status
--rollback     IS 'Entry List Status: Status of the entry list {draft, created, completed} [ENTLIST_STATUS]';
