--liquibase formatted sql

--changeset postgres:add_collaborator_role context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1165 Add COLLABORATOR role



INSERT INTO
    tenant.person_role
    (person_role_code, person_role_name, description, creator_id)
VALUES 
    ('COLLABORATOR', 'Collaborator', 'Partner responsible for recording experiment data at one or more experiment occurrences or locations', '1')
;



--rollback DELETE FROM tenant.person_role WHERE person_role_code='COLLABORATOR';