--liquibase formatted sql

--changeset postgres:fill_up_missing_package_id_in_entry context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1229 Fill up missing package_id in experiment.entry table



-- fill up package_id column
WITH t_ent AS (
    SELECT
        ent.id AS entry_id,
        t_pkg.id AS package_id
    FROM
        experiment.entry AS ent
        LEFT JOIN LATERAL (
            SELECT
                pkg.id
            FROM
                germplasm.seed AS sd
                INNER JOIN germplasm.package AS pkg
                    ON pkg.seed_id = sd.id
            WHERE
                sd.germplasm_id = ent.germplasm_id
                AND sd.is_void = FALSE
                AND pkg.is_void = FALSE
            LIMIT
                1
        ) AS t_pkg
            ON TRUE
    WHERE
        ent.package_id IS NULL
)
UPDATE
    experiment.entry AS ent
SET
    package_id = t.package_id,
    notes = platform.append_text(ent.notes, 'DB-1229 entry')
FROM
    t_ent AS t
WHERE
    ent.id = t.entry_id
;



-- revert changes
--rollback UPDATE
--rollback     experiment.entry AS ent
--rollback SET
--rollback     package_id = NULL,
--rollback     notes = platform.detach_text(ent.notes, 'DB-1229 entry')
--rollback WHERE
--rollback     ent.notes LIKE '%DB-1229 entry%'
--rollback ;



--changeset postgres:fill_up_missing_package_id_in_planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1229 Fill up missing package_id in experiment.planting_instruction table



-- fill up package_id column
UPDATE
    experiment.planting_instruction AS plin
SET
    package_id = ent.package_id,
    notes = platform.append_text(plin.notes, 'DB-1229 planting_instruction')
FROM
    experiment.entry AS ent
WHERE
    ent.id = plin.entry_id
    AND plin.package_id IS NULL
    AND ent.package_id IS NOT NULL
;



-- revert changes
--rollback UPDATE
--rollback     experiment.planting_instruction AS plin
--rollback SET
--rollback     package_id = NULL,
--rollback     notes = platform.detach_text(plin.notes, 'DB-1229 planting_instruction')
--rollback WHERE
--rollback     plin.notes LIKE '%DB-1229 planting_instruction%'
--rollback ;
