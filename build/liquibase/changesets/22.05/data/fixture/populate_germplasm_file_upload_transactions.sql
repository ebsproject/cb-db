--liquibase formatted sql

--changeset postgres:populate_germplasm_file_upload_table context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1170 Populate germplasm.file_upload table



-- populate table
INSERT INTO
   germplasm.file_upload (
       program_id,
       file_name,
       file_data,
       file_status,
       germplasm_count,
       seed_count,
       package_count,
       notes,
       creator_id
   )
SELECT
    prog.id AS program_id,
    t.file_name,
    '[]'::jsonb AS file_data,
    'completed' AS file_status,
    t.germplasm_count,
    t.seed_count,
    t.package_count,
    'DB-1170 file_upload' AS notes,
    prsn.id AS creator_id
FROM (
        VALUES
        ('IRSEA', 'GE_FILE_UPLOAD_IRSEA_1.csv', 100, 100, 100, 'a.ramos'),
        ('IRSEA', 'GE_FILE_UPLOAD_IRSEA_2.csv', 100, 100, 100, 'a.carlson'),
        ('IRSEA', 'GE_FILE_UPLOAD_IRSEA_3.csv', 50, 50, 50, 'a.ramos'),
        ('IRSEA', 'GE_FILE_UPLOAD_IRSEA_4.csv', 50, 50, 50, 'k.khadija'),
        ('IRSEA', 'GE_FILE_UPLOAD_IRSEA_5.csv', 30, 30, 30, 'a.carlson'),
        ('IRSEA', 'GE_FILE_UPLOAD_IRSEA_6.csv', 30, 0, 0, 'k.khadija'),
        ('KE', 'GE_FILE_UPLOAD_KE_1.csv', 50, 50, 50, 'faith.hamilton'),
        ('KE', 'GE_FILE_UPLOAD_KE_2.csv', 30, 0, 0, 'elly.donelly'),
        ('BW', 'GE_FILE_UPLOAD_BW_1.csv', 80, 80, 80, 'jennifer.allan'),
        ('BW', 'GE_FILE_UPLOAD_BW_2.csv', 50, 0, 0, 'nicola.costa')
    ) AS t (
        program_code,
        file_name,
        germplasm_count,
        seed_count,
        package_count,
        username
    )
    JOIN tenant.program AS prog
        ON prog.program_code = t.program_code
    JOIN tenant.person AS prsn
        ON prsn.username = t.username
;



-- revert changes
--rollback DELETE FROM
--rollback    germplasm.file_upload
--rollback WHERE
--rollback    notes LIKE '%DB-1170 file_upload%'
--rollback ;
--rollback 
--rollback SELECT SETVAL('germplasm.file_upload_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM germplasm.file_upload;



--changeset postgres:populate_germplasm_file_upload_germplasm_table context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1170 Populate germplasm.file_upload_germplasm table



-- populate table
INSERT INTO
   germplasm.file_upload_germplasm (
       file_upload_id,
       germplasm_id,
       seed_id,
       package_id,
       notes,
       creator_id
   )
SELECT
    t.*
FROM (
        SELECT
            DISTINCT ON (
                ge.germplasm_id
            )
            gefu.id AS file_upload_id,
            ge.germplasm_id,
            sd.seed_id,
            pkg.package_id,
            'DB-1170 file_upload_germplasm' AS notes,
            gefu.creator_id
        FROM
            germplasm.file_upload AS gefu
            INNER JOIN LATERAL (
                SELECT
                    ge.id AS germplasm_id
                FROM
                    germplasm.germplasm AS ge
                    JOIN tenant.crop AS crop
                        ON crop.id = ge.crop_id
                    JOIN tenant.crop_program AS cropprog
                        ON cropprog.crop_id = crop.id
                    JOIN tenant.program AS prog
                        ON prog.crop_program_id = cropprog.id
                WHERE
                    prog.id = gefu.program_id
                    AND ge.is_void = FALSE
                ORDER BY
                    RANDOM()
                LIMIT
                    gefu.germplasm_count
            ) AS ge
                ON TRUE
            LEFT JOIN LATERAL (
                SELECT
                    sd.id AS seed_id
                FROM
                    germplasm.seed AS sd
                WHERE
                    sd.germplasm_id = ge.germplasm_id
                    AND sd.is_void = FALSE
                LIMIT
                    (CASE
                        WHEN gefu.seed_count > 0 THEN 1
                        ELSE 0
                    END)
            ) AS sd
                ON TRUE
            LEFT JOIN LATERAL (
                SELECT
                    pkg.id AS package_id
                FROM
                    germplasm.package AS pkg
                WHERE
                    pkg.seed_id = sd.seed_id
                    AND pkg.is_void = FALSE
                LIMIT
                    (CASE
                        WHEN gefu.seed_count > 0 THEN 1
                        ELSE 0
                    END)
            ) AS pkg
                ON TRUE
        WHERE
            gefu.notes LIKE '%DB-1170 file_upload%'
    ) AS t
ORDER BY
    t.file_upload_id
;



-- revert changes
--rollback DELETE FROM
--rollback    germplasm.file_upload_germplasm
--rollback WHERE
--rollback    notes LIKE '%DB-1170 file_upload_germplasm%'
--rollback ;
--rollback 
--rollback SELECT SETVAL('germplasm.file_upload_germplasm_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM germplasm.file_upload_germplasm;



--changeset postgres:update_file_upload_counts context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1170 Update file_upload counts



-- update counts
UPDATE
    germplasm.file_upload AS gefu
SET
    germplasm_count = t_ge.germplasm_count,
    seed_count = t_sd.seed_count,
    package_count = t_pkg.package_count
FROM (
        SELECT
            gefuge.file_upload_id,
            count(*) AS germplasm_count
        FROM
            germplasm.file_upload_germplasm AS gefuge
        GROUP BY
            gefuge.file_upload_id
    ) AS t_ge,
    (
        SELECT
            gefuge.file_upload_id,
            count(*) AS seed_count
        FROM
            germplasm.file_upload_germplasm AS gefuge
        WHERE
            gefuge.seed_id IS NOT NULL
        GROUP BY
            gefuge.file_upload_id
    ) AS t_sd,
    (
        SELECT
            gefuge.file_upload_id,
            count(*) AS package_count
        FROM
            germplasm.file_upload_germplasm AS gefuge
        WHERE
            gefuge.package_id IS NOT NULL
        GROUP BY
            gefuge.file_upload_id
    ) AS t_pkg
WHERE
    t_ge.file_upload_id = gefu.id
    AND t_sd.file_upload_id = gefu.id
    AND t_pkg.file_upload_id = gefu.id
    AND gefu.notes LIKE '%DB-1170 file_upload%'
;



-- revert changes
--rollback UPDATE
--rollback     germplasm.file_upload AS gefu
--rollback SET
--rollback     germplasm_count = t.germplasm_count,
--rollback     seed_count = t.seed_count,
--rollback     package_count = t.package_count
--rollback FROM (
--rollback         VALUES
--rollback         ('GE_FILE_UPLOAD_IRSEA_1.csv', 100, 100, 100),
--rollback         ('GE_FILE_UPLOAD_IRSEA_2.csv', 100, 100, 100),
--rollback         ('GE_FILE_UPLOAD_IRSEA_3.csv', 50, 50, 50),
--rollback         ('GE_FILE_UPLOAD_IRSEA_4.csv', 50, 50, 50),
--rollback         ('GE_FILE_UPLOAD_IRSEA_5.csv', 30, 30, 30),
--rollback         ('GE_FILE_UPLOAD_IRSEA_6.csv', 30, 0, 0),
--rollback         ('GE_FILE_UPLOAD_KE_1.csv', 50, 50, 50),
--rollback         ('GE_FILE_UPLOAD_KE_2.csv', 30, 0, 0),
--rollback         ('GE_FILE_UPLOAD_BW_1.csv', 80, 80, 80),
--rollback         ('GE_FILE_UPLOAD_BW_2.csv', 50, 0, 0)
--rollback     ) AS t (
--rollback         file_name,
--rollback         germplasm_count,
--rollback         seed_count,
--rollback         package_count
--rollback     )
--rollback WHERE
--rollback     t.file_name = gefu.file_name
--rollback     AND gefu.notes LIKE '%DB-1170 file_upload%'
--rollback ;



--changeset postgres:populate_file_data_contents context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1170 Populate file_data contents



-- populate contents
UPDATE
    germplasm.file_upload AS gefu
SET
    file_data = t.file_data
FROM (
        SELECT
            t.file_upload_id,
            jsonb_agg(t.file_data) AS file_data
        FROM (
                SELECT
                    gefuge.file_upload_id,
                    ('{' || CONCAT_WS(
                        ',',
                        '"germplasm":' || row_to_json(ge.*)::TEXT,
                        '"seed":' || row_to_json(sd.*)::TEXT,
                        '"package":' || row_to_json(pkg.*)::TEXT
                    ) || '}')::jsonb AS file_data
                FROM
                    germplasm.file_upload_germplasm AS gefuge
                    INNER JOIN LATERAL (
                        SELECT
                            ge.designation,
                            ge.germplasm_name_type,
                            ge.parentage,
                            ge.generation,
                            ge.germplasm_state,
                            taxon.taxon_id,
                            ge.germplasm_type
                        FROM
                            germplasm.germplasm AS ge
                            JOIN germplasm.taxonomy AS taxon
                                ON taxon.id = ge.taxonomy_id
                            JOIN tenant.crop AS crop
                                ON crop.id = ge.crop_id
                        WHERE
                            ge.id = gefuge.germplasm_id
                    ) ge
                        ON TRUE
                    LEFT JOIN LATERAL (
                        SELECT
                            sd.seed_name,
                            prog.program_code,
                            sd.harvest_date,
                            sd.harvest_method,
                            sd.description
                        FROM
                            germplasm.seed AS sd
                            LEFT JOIN tenant.program AS prog
                                ON prog.id = sd.program_id
                        WHERE
                            sd.id = gefuge.seed_id
                    ) AS sd
                        ON TRUE
                    LEFT JOIN LATERAL (
                        SELECT
                            pkg.package_label,
                            prog.program_code,
                            pkg.package_status,
                            pkg.package_quantity,
                            pkg.package_unit
                        FROM
                            germplasm.package AS pkg
                            LEFT JOIN tenant.program AS prog
                                ON prog.id = pkg.program_id
                        WHERE
                            pkg.id = gefuge.package_id
                    ) AS pkg
                        ON TRUE
                ORDER BY
                    gefuge.file_upload_id,
                    gefuge.id
            ) AS t
        GROUP BY
            t.file_upload_id
    ) AS t
WHERE
    t.file_upload_id = gefu.id
;



-- revert changes
--rollback UPDATE
--rollback     germplasm.file_upload AS gefu
--rollback SET
--rollback     file_data = '[]'
--rollback WHERE
--rollback     gefu.notes LIKE '%DB-1170 file_upload%'
--rollback ;
