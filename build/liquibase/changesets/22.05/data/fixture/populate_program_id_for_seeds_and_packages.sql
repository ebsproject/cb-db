--liquibase formatted sql

--changeset postgres:populate_program_id_for_seeds context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1231 Populate program_id for seeds



UPDATE
    germplasm.seed AS s
SET
    program_id = p.id
FROM
    tenant.program p
WHERE
    p.program_code = 'IRSEA'
AND
    s.seed_name
IN
    (
        SELECT
            concat(t.designation,'-',g.id,'-', ROW_NUMBER() OVER ()) AS seed_name
        FROM
            germplasm.germplasm AS g
            INNER JOIN (
                VALUES
                    ('IRS0001'),
                    ('IRS0002'),
                    ('IRS0003'),
                    ('IRS0004'),
                    ('IRS0005'),
                    ('IRS0006'),
                    ('IRS0007'),
                    ('IRS0008'),
                    ('IRS0009'),
                    ('IRS0010'),
                    ('IRS0011'),
                    ('IRS0012'),
                    ('IRS0013'),
                    ('IRS0014'),
                    ('IRS0015'),
                    ('IRS0016'),
                    ('IRS0017'),
                    ('IRS0018'),
                    ('IRS0019'),
                    ('IRS0020'),
                    ('IRS0021'),
                    ('IRS0022'),
                    ('IRS0023'),
                    ('IRS0024'),
                    ('IRS0025'),
                    ('IRS0026'),
                    ('IRS0027'),
                    ('IRS0028'),
                    ('IRS0029'),
                    ('IRS0030'),
                    ('IRS0031'),
                    ('IRS0032'),
                    ('IRS0033'),
                    ('IRS0034'),
                    ('IRS0035'),
                    ('IRS0036'),
                    ('IRS0037'),
                    ('IRS0038'),
                    ('IRS0039'),
                    ('IRS0040'),
                    ('IRS0041'),
                    ('IRS0042'),
                    ('IRS0043'),
                    ('IRS0044'),
                    ('IRS0045'),
                    ('IRS0046'),
                    ('IRS0047'),
                    ('IRS0048'),
                    ('IRS0049'),
                    ('IRS0050'),
                    ('IRS0051'),
                    ('IRS0052'),
                    ('IRS0053'),
                    ('IRS0054'),
                    ('IRS0055'),
                    ('IRS0056'),
                    ('IRS0057'),
                    ('IRS0058'),
                    ('IRS0059'),
                    ('IRS0060'),
                    ('IRS0061'),
                    ('IRS0062'),
                    ('IRS0063'),
                    ('IRS0064'),
                    ('IRS0065'),
                    ('IRS0066'),
                    ('IRS0067'),
                    ('IRS0068'),
                    ('IRS0069'),
                    ('IRS0070'),
                    ('IRS0071'),
                    ('IRS0072'),
                    ('IRS0073'),
                    ('IRS0074'),
                    ('IRS0075'),
                    ('IRS0076'),
                    ('IRS0077'),
                    ('IRS0078'),
                    ('IRS0079'),
                    ('IRS0080'),
                    ('IRS0081'),
                    ('IRS0082'),
                    ('IRS0083'),
                    ('IRS0084'),
                    ('IRS0085'),
                    ('IRS0086'),
                    ('IRS0087'),
                    ('IRS0088'),
                    ('IRS0089'),
                    ('IRS0090'),
                    ('IRS0091'),
                    ('IRS0092'),
                    ('IRS0093'),
                    ('IRS0094'),
                    ('IRS0095'),
                    ('IRS0096'),
                    ('IRS0097'),
                    ('IRS0098'),
                    ('IRS0099'),
                    ('IRS0100')
            ) AS t (
                designation
            )
        ON g.germplasm_normalized_name = t.designation
    )
;



--rollback UPDATE
--rollback     germplasm.seed AS s
--rollback SET
--rollback     program_id = NULL
--rollback FROM
--rollback     tenant.program p
--rollback WHERE
--rollback     p.program_code = 'IRSEA'
--rollback AND
--rollback     s.seed_name
--rollback IN
--rollback     (
--rollback         SELECT
--rollback             concat(t.designation,'-',g.id,'-', ROW_NUMBER() OVER ()) AS seed_name
--rollback         FROM
--rollback             germplasm.germplasm AS g
--rollback             INNER JOIN (
--rollback                 VALUES
--rollback                     ('IRS0001'),
--rollback                     ('IRS0002'),
--rollback                     ('IRS0003'),
--rollback                     ('IRS0004'),
--rollback                     ('IRS0005'),
--rollback                     ('IRS0006'),
--rollback                     ('IRS0007'),
--rollback                     ('IRS0008'),
--rollback                     ('IRS0009'),
--rollback                     ('IRS0010'),
--rollback                     ('IRS0011'),
--rollback                     ('IRS0012'),
--rollback                     ('IRS0013'),
--rollback                     ('IRS0014'),
--rollback                     ('IRS0015'),
--rollback                     ('IRS0016'),
--rollback                     ('IRS0017'),
--rollback                     ('IRS0018'),
--rollback                     ('IRS0019'),
--rollback                     ('IRS0020'),
--rollback                     ('IRS0021'),
--rollback                     ('IRS0022'),
--rollback                     ('IRS0023'),
--rollback                     ('IRS0024'),
--rollback                     ('IRS0025'),
--rollback                     ('IRS0026'),
--rollback                     ('IRS0027'),
--rollback                     ('IRS0028'),
--rollback                     ('IRS0029'),
--rollback                     ('IRS0030'),
--rollback                     ('IRS0031'),
--rollback                     ('IRS0032'),
--rollback                     ('IRS0033'),
--rollback                     ('IRS0034'),
--rollback                     ('IRS0035'),
--rollback                     ('IRS0036'),
--rollback                     ('IRS0037'),
--rollback                     ('IRS0038'),
--rollback                     ('IRS0039'),
--rollback                     ('IRS0040'),
--rollback                     ('IRS0041'),
--rollback                     ('IRS0042'),
--rollback                     ('IRS0043'),
--rollback                     ('IRS0044'),
--rollback                     ('IRS0045'),
--rollback                     ('IRS0046'),
--rollback                     ('IRS0047'),
--rollback                     ('IRS0048'),
--rollback                     ('IRS0049'),
--rollback                     ('IRS0050'),
--rollback                     ('IRS0051'),
--rollback                     ('IRS0052'),
--rollback                     ('IRS0053'),
--rollback                     ('IRS0054'),
--rollback                     ('IRS0055'),
--rollback                     ('IRS0056'),
--rollback                     ('IRS0057'),
--rollback                     ('IRS0058'),
--rollback                     ('IRS0059'),
--rollback                     ('IRS0060'),
--rollback                     ('IRS0061'),
--rollback                     ('IRS0062'),
--rollback                     ('IRS0063'),
--rollback                     ('IRS0064'),
--rollback                     ('IRS0065'),
--rollback                     ('IRS0066'),
--rollback                     ('IRS0067'),
--rollback                     ('IRS0068'),
--rollback                     ('IRS0069'),
--rollback                     ('IRS0070'),
--rollback                     ('IRS0071'),
--rollback                     ('IRS0072'),
--rollback                     ('IRS0073'),
--rollback                     ('IRS0074'),
--rollback                     ('IRS0075'),
--rollback                     ('IRS0076'),
--rollback                     ('IRS0077'),
--rollback                     ('IRS0078'),
--rollback                     ('IRS0079'),
--rollback                     ('IRS0080'),
--rollback                     ('IRS0081'),
--rollback                     ('IRS0082'),
--rollback                     ('IRS0083'),
--rollback                     ('IRS0084'),
--rollback                     ('IRS0085'),
--rollback                     ('IRS0086'),
--rollback                     ('IRS0087'),
--rollback                     ('IRS0088'),
--rollback                     ('IRS0089'),
--rollback                     ('IRS0090'),
--rollback                     ('IRS0091'),
--rollback                     ('IRS0092'),
--rollback                     ('IRS0093'),
--rollback                     ('IRS0094'),
--rollback                     ('IRS0095'),
--rollback                     ('IRS0096'),
--rollback                     ('IRS0097'),
--rollback                     ('IRS0098'),
--rollback                     ('IRS0099'),
--rollback                     ('IRS0100')
--rollback             ) AS t (
--rollback                 designation
--rollback             )
--rollback         ON g.germplasm_normalized_name = t.designation
--rollback     )
--rollback ;



--changeset postgres:populate_program_id_for_packages context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1231 Populate program_id for packages



UPDATE
    germplasm.package AS pk
SET
    program_id = p.id
FROM
    tenant.program p
WHERE
    p.program_code = 'IRSEA'
AND
    pk.package_label
IN
    (
        SELECT
            concat(t.designation,'-',g.id,'-',ROW_NUMBER() OVER ()) AS package_label
        FROM
            germplasm.germplasm AS g
            INNER JOIN (
                VALUES
                    ('IRS0001'),
                    ('IRS0002'),
                    ('IRS0003'),
                    ('IRS0004'),
                    ('IRS0005'),
                    ('IRS0006'),
                    ('IRS0007'),
                    ('IRS0008'),
                    ('IRS0009'),
                    ('IRS0010'),
                    ('IRS0011'),
                    ('IRS0012'),
                    ('IRS0013'),
                    ('IRS0014'),
                    ('IRS0015'),
                    ('IRS0016'),
                    ('IRS0017'),
                    ('IRS0018'),
                    ('IRS0019'),
                    ('IRS0020'),
                    ('IRS0021'),
                    ('IRS0022'),
                    ('IRS0023'),
                    ('IRS0024'),
                    ('IRS0025'),
                    ('IRS0026'),
                    ('IRS0027'),
                    ('IRS0028'),
                    ('IRS0029'),
                    ('IRS0030'),
                    ('IRS0031'),
                    ('IRS0032'),
                    ('IRS0033'),
                    ('IRS0034'),
                    ('IRS0035'),
                    ('IRS0036'),
                    ('IRS0037'),
                    ('IRS0038'),
                    ('IRS0039'),
                    ('IRS0040'),
                    ('IRS0041'),
                    ('IRS0042'),
                    ('IRS0043'),
                    ('IRS0044'),
                    ('IRS0045'),
                    ('IRS0046'),
                    ('IRS0047'),
                    ('IRS0048'),
                    ('IRS0049'),
                    ('IRS0050'),
                    ('IRS0051'),
                    ('IRS0052'),
                    ('IRS0053'),
                    ('IRS0054'),
                    ('IRS0055'),
                    ('IRS0056'),
                    ('IRS0057'),
                    ('IRS0058'),
                    ('IRS0059'),
                    ('IRS0060'),
                    ('IRS0061'),
                    ('IRS0062'),
                    ('IRS0063'),
                    ('IRS0064'),
                    ('IRS0065'),
                    ('IRS0066'),
                    ('IRS0067'),
                    ('IRS0068'),
                    ('IRS0069'),
                    ('IRS0070'),
                    ('IRS0071'),
                    ('IRS0072'),
                    ('IRS0073'),
                    ('IRS0074'),
                    ('IRS0075'),
                    ('IRS0076'),
                    ('IRS0077'),
                    ('IRS0078'),
                    ('IRS0079'),
                    ('IRS0080'),
                    ('IRS0081'),
                    ('IRS0082'),
                    ('IRS0083'),
                    ('IRS0084'),
                    ('IRS0085'),
                    ('IRS0086'),
                    ('IRS0087'),
                    ('IRS0088'),
                    ('IRS0089'),
                    ('IRS0090'),
                    ('IRS0091'),
                    ('IRS0092'),
                    ('IRS0093'),
                    ('IRS0094'),
                    ('IRS0095'),
                    ('IRS0096'),
                    ('IRS0097'),
                    ('IRS0098'),
                    ('IRS0099'),
                    ('IRS0100')
            ) AS t (
                designation
            )
        ON g.germplasm_normalized_name = t.designation
    )
;



--rollback UPDATE
--rollback     germplasm.package AS pk
--rollback SET
--rollback     program_id = NULL
--rollback FROM
--rollback     tenant.program p
--rollback WHERE
--rollback     p.program_code = 'IRSEA'
--rollback AND
--rollback     pk.package_label
--rollback IN
--rollback     (
--rollback         SELECT
--rollback             concat(t.designation,'-',g.id,'-', ROW_NUMBER() OVER ()) AS package_label
--rollback         FROM
--rollback             germplasm.germplasm AS g
--rollback             INNER JOIN (
--rollback                 VALUES
--rollback                     ('IRS0001'),
--rollback                     ('IRS0002'),
--rollback                     ('IRS0003'),
--rollback                     ('IRS0004'),
--rollback                     ('IRS0005'),
--rollback                     ('IRS0006'),
--rollback                     ('IRS0007'),
--rollback                     ('IRS0008'),
--rollback                     ('IRS0009'),
--rollback                     ('IRS0010'),
--rollback                     ('IRS0011'),
--rollback                     ('IRS0012'),
--rollback                     ('IRS0013'),
--rollback                     ('IRS0014'),
--rollback                     ('IRS0015'),
--rollback                     ('IRS0016'),
--rollback                     ('IRS0017'),
--rollback                     ('IRS0018'),
--rollback                     ('IRS0019'),
--rollback                     ('IRS0020'),
--rollback                     ('IRS0021'),
--rollback                     ('IRS0022'),
--rollback                     ('IRS0023'),
--rollback                     ('IRS0024'),
--rollback                     ('IRS0025'),
--rollback                     ('IRS0026'),
--rollback                     ('IRS0027'),
--rollback                     ('IRS0028'),
--rollback                     ('IRS0029'),
--rollback                     ('IRS0030'),
--rollback                     ('IRS0031'),
--rollback                     ('IRS0032'),
--rollback                     ('IRS0033'),
--rollback                     ('IRS0034'),
--rollback                     ('IRS0035'),
--rollback                     ('IRS0036'),
--rollback                     ('IRS0037'),
--rollback                     ('IRS0038'),
--rollback                     ('IRS0039'),
--rollback                     ('IRS0040'),
--rollback                     ('IRS0041'),
--rollback                     ('IRS0042'),
--rollback                     ('IRS0043'),
--rollback                     ('IRS0044'),
--rollback                     ('IRS0045'),
--rollback                     ('IRS0046'),
--rollback                     ('IRS0047'),
--rollback                     ('IRS0048'),
--rollback                     ('IRS0049'),
--rollback                     ('IRS0050'),
--rollback                     ('IRS0051'),
--rollback                     ('IRS0052'),
--rollback                     ('IRS0053'),
--rollback                     ('IRS0054'),
--rollback                     ('IRS0055'),
--rollback                     ('IRS0056'),
--rollback                     ('IRS0057'),
--rollback                     ('IRS0058'),
--rollback                     ('IRS0059'),
--rollback                     ('IRS0060'),
--rollback                     ('IRS0061'),
--rollback                     ('IRS0062'),
--rollback                     ('IRS0063'),
--rollback                     ('IRS0064'),
--rollback                     ('IRS0065'),
--rollback                     ('IRS0066'),
--rollback                     ('IRS0067'),
--rollback                     ('IRS0068'),
--rollback                     ('IRS0069'),
--rollback                     ('IRS0070'),
--rollback                     ('IRS0071'),
--rollback                     ('IRS0072'),
--rollback                     ('IRS0073'),
--rollback                     ('IRS0074'),
--rollback                     ('IRS0075'),
--rollback                     ('IRS0076'),
--rollback                     ('IRS0077'),
--rollback                     ('IRS0078'),
--rollback                     ('IRS0079'),
--rollback                     ('IRS0080'),
--rollback                     ('IRS0081'),
--rollback                     ('IRS0082'),
--rollback                     ('IRS0083'),
--rollback                     ('IRS0084'),
--rollback                     ('IRS0085'),
--rollback                     ('IRS0086'),
--rollback                     ('IRS0087'),
--rollback                     ('IRS0088'),
--rollback                     ('IRS0089'),
--rollback                     ('IRS0090'),
--rollback                     ('IRS0091'),
--rollback                     ('IRS0092'),
--rollback                     ('IRS0093'),
--rollback                     ('IRS0094'),
--rollback                     ('IRS0095'),
--rollback                     ('IRS0096'),
--rollback                     ('IRS0097'),
--rollback                     ('IRS0098'),
--rollback                     ('IRS0099'),
--rollback                     ('IRS0100')
--rollback             ) AS t (
--rollback                 designation
--rollback             )
--rollback         ON g.germplasm_normalized_name = t.designation
--rollback     )
--rollback ;