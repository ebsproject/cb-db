--liquibase formatted sql

--changeset postgres:fix_gene_construct_germplasm_type context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1231 Fix gene_construct germplasm_type



UPDATE 
    germplasm.germplasm
SET
    germplasm_type = 'gene_construct'
WHERE
    germplasm_type = 'gene_constuct';



--rollback UPDATE 
--rollback     germplasm.germplasm
--rollback SET
--rollback     germplasm_type = 'gene_constuct'
--rollback WHERE
--rollback     germplasm_type = 'gene_construct';