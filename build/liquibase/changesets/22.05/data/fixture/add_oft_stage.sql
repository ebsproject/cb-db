--liquibase formatted sql

--changeset postgres:add_oft_stage context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1213 Add OFT stage



-- add stage
INSERT INTO
    tenant.stage (
        stage_code,
        stage_name,
        description,
        creator_id
    )
SELECT
    t.stage_code,
    t.stage_name,
    t.stage_name AS description,
    1 AS creator_id
FROM
    (
        VALUES
        ('OFT', 'On-Farm Trial')
        -- TODO: Add more stages here if needed
    ) AS t (
        stage_code,
        stage_name
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     tenant.stage
--rollback WHERE
--rollback     stage_code IN (
--rollback         'OFT'
--rollback     )
--rollback ;
--rollback 
--rollback SELECT SETVAL('tenant.stage_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM tenant.stage;
