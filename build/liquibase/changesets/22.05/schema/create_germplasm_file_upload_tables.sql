--liquibase formatted sql

--changeset postgres:create_germplasm_file_upload_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1167 Create germplasm.file_upload table



-- create table
CREATE TABLE germplasm.file_upload (
    -- primary key column
    id serial NOT NULL,
    
    -- columns
    program_id integer NOT NULL,
    file_name varchar(256) NOT NULL,
    file_data jsonb NOT NULL,
    file_status varchar(64) NOT NULL,
    germplasm_count integer NOT NULL DEFAULT 0,
    seed_count integer NOT NULL DEFAULT 0,
    package_count integer NOT NULL DEFAULT 0,
    error_log text,
    
    -- audit columns
    remarks text,
    creation_timestamp timestamptz NOT NULL DEFAULT now(),
    creator_id integer NOT NULL,
    modification_timestamp timestamptz,
    modifier_id integer,
    notes text,
    is_void boolean NOT NULL DEFAULT FALSE,
    event_log jsonb,
    
    -- primary key constraint
    CONSTRAINT file_upload_id_pk PRIMARY KEY (id),
    
    -- audit foreign key constraints
    CONSTRAINT file_upload_creator_id_fk FOREIGN KEY (creator_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT file_upload_modifier_id_fk FOREIGN KEY (modifier_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT
);

-- constraints
ALTER TABLE germplasm.file_upload
    ADD CONSTRAINT file_upload_program_id_fk
    FOREIGN KEY (program_id)
    REFERENCES tenant.program (id)
    ON UPDATE CASCADE ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT file_upload_program_id_fk
    ON germplasm.file_upload
    IS 'A file upload transaction can be uploaded in one program.'
;

ALTER TABLE germplasm.file_upload
    ADD CONSTRAINT file_upload_file_status_chk
    CHECK (file_status = ANY(ARRAY['in queue', 'validated', 'validation error', 'creation in progress', 'complete']))
;

-- table comment
COMMENT ON TABLE germplasm.file_upload
    IS 'File Upload: File upload transactions for creating germplasm records [GEFILEUPLOAD]';

-- column comments
COMMENT ON COLUMN germplasm.file_upload.program_id
    IS 'Program ID: Reference to the program where the file upload transaction is made [GEFILEUPLOAD_PROG_ID]';
COMMENT ON COLUMN germplasm.file_upload.file_name
    IS 'File Name: Name of the uploaded file [GEFILEUPLOAD_FILENAME]';
COMMENT ON COLUMN germplasm.file_upload.file_data
    IS 'File Data: Contents of the uploaded file [GEFILEUPLOAD_FILEDATA]';
COMMENT ON COLUMN germplasm.file_upload.file_status
    IS 'File Status: Progress of the file upload transaction {in queue, validated, validation error, creation in progress, complete} [GEFILEUPLOAD_FILESTATUS]';
COMMENT ON COLUMN germplasm.file_upload.germplasm_count
    IS 'Germplasm Count: Total number of germplasm created from the file upload transaction [GEFILEUPLOAD_GECOUNT]';
COMMENT ON COLUMN germplasm.file_upload.seed_count
    IS 'Seed Count: Total number of seeds created from the file upload transaction [GEFILEUPLOAD_SEEDCOUNT]';
COMMENT ON COLUMN germplasm.file_upload.package_count
    IS 'Package Count: Total number of packages created from the file upload transaction [GEFILEUPLOAD_PKGCOUNT]';
COMMENT ON COLUMN germplasm.file_upload.error_log
    IS 'Error Log: Error messages generated from the file upload transaction [GEFILEUPLOAD_ERRLOG]';

-- audit column comments
COMMENT ON COLUMN germplasm.file_upload.id
    IS 'File Upload ID: Database identifier of the record [GEFILEUPLOAD_ID]';
COMMENT ON COLUMN germplasm.file_upload.remarks
    IS 'Remarks: Additional notes to record [GEFILEUPLOAD_REMARKS]';
COMMENT ON COLUMN germplasm.file_upload.creation_timestamp
    IS 'Creation Timestamp: Timestamp when the record was created [GEFILEUPLOAD_CTSTAMP]';
COMMENT ON COLUMN germplasm.file_upload.creator_id
    IS 'Creator ID: Reference to the person who created the record [GEFILEUPLOAD_CPERSON]';
COMMENT ON COLUMN germplasm.file_upload.modification_timestamp
    IS 'Modification Timestamp: Timestamp when the record was last modified [GEFILEUPLOAD_MTSTAMP]';
COMMENT ON COLUMN germplasm.file_upload.modifier_id
    IS 'Modifier ID: Reference to the person who last modified the record [GEFILEUPLOAD_MPERSON]';
COMMENT ON COLUMN germplasm.file_upload.notes
    IS 'Notes: Technical details about the record [GEFILEUPLOAD_NOTES]';
COMMENT ON COLUMN germplasm.file_upload.is_void
    IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [GEFILEUPLOAD_ISVOID]';
COMMENT ON COLUMN germplasm.file_upload.event_log
    IS 'Event Log: Event Log: Historical transactions of the record [GEFILEUPLOAD_EVENTLOG]';

-- indices
CREATE INDEX file_upload_program_id_idx
    ON germplasm.file_upload USING btree (program_id)
;

CREATE INDEX file_upload_file_name_idx
    ON germplasm.file_upload USING btree (file_name)
;

CREATE INDEX file_upload_file_status_idx
    ON germplasm.file_upload USING btree (file_status)
;

-- audit indices
CREATE INDEX file_upload_creator_id_idx ON germplasm.file_upload USING btree (creator_id);
CREATE INDEX file_upload_modifier_id_idx ON germplasm.file_upload USING btree (modifier_id);
CREATE INDEX file_upload_is_void_idx ON germplasm.file_upload USING btree (is_void);

-- triggers
CREATE TRIGGER file_upload_event_log_tgr BEFORE INSERT OR UPDATE
    ON germplasm.file_upload FOR EACH ROW EXECUTE FUNCTION platform.log_record_event();



-- revert changes
--rollback DROP TABLE germplasm.file_upload;



--changeset postgres:create_germplasm_file_upload_germplasm_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1167 Create germplasm.file_upload_germplasm table



-- create table
CREATE TABLE germplasm.file_upload_germplasm (
    -- primary key column
    id serial NOT NULL,
    
    -- columns
    file_upload_id integer NOT NULL,
    germplasm_id integer NOT NULL,
    seed_id integer,
    package_id integer,
    
    -- audit columns
    remarks text,
    creation_timestamp timestamptz NOT NULL DEFAULT now(),
    creator_id integer NOT NULL,
    modification_timestamp timestamptz,
    modifier_id integer,
    notes text,
    is_void boolean NOT NULL DEFAULT FALSE,
    event_log jsonb,
    
    -- primary key constraint
    CONSTRAINT file_upload_germplasm_id_pk PRIMARY KEY (id),
    
    -- audit foreign key constraints
    CONSTRAINT file_upload_germplasm_creator_id_fk FOREIGN KEY (creator_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT file_upload_germplasm_modifier_id_fk FOREIGN KEY (modifier_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT
);

-- constraints
ALTER TABLE germplasm.file_upload_germplasm
    ADD CONSTRAINT file_upload_germplasm_file_upload_id_fk
    FOREIGN KEY (file_upload_id)
    REFERENCES germplasm.file_upload (id)
    ON UPDATE CASCADE ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT file_upload_germplasm_file_upload_id_fk
    ON germplasm.file_upload_germplasm
    IS 'A germplasm is created in a file upload transaction.'
;

ALTER TABLE germplasm.file_upload_germplasm
    ADD CONSTRAINT file_upload_germplasm_germplasm_id_fk
    FOREIGN KEY (germplasm_id)
    REFERENCES germplasm.germplasm (id)
    ON UPDATE CASCADE ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT file_upload_germplasm_germplasm_id_fk
    ON germplasm.file_upload_germplasm
    IS 'A germplasm created from a file upload transaction refers to a germplasm.'
;

ALTER TABLE germplasm.file_upload_germplasm
    ADD CONSTRAINT file_upload_germplasm_seed_id_fk
    FOREIGN KEY (seed_id)
    REFERENCES germplasm.seed (id)
    ON UPDATE CASCADE ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT file_upload_germplasm_seed_id_fk
    ON germplasm.file_upload_germplasm
    IS 'A seed of a germplasm created from a file upload transaction refers to a seed.'
;

ALTER TABLE germplasm.file_upload_germplasm
    ADD CONSTRAINT file_upload_germplasm_package_id_fk
    FOREIGN KEY (package_id)
    REFERENCES germplasm.package (id)
    ON UPDATE CASCADE ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT file_upload_germplasm_package_id_fk
    ON germplasm.file_upload_germplasm
    IS 'A package of a germplasm created from a file upload transaction refers to a package.'
;

-- table comment
COMMENT ON TABLE germplasm.file_upload_germplasm
    IS 'File Upload Germplasm: Germplasm created from the file upload transactions [GEFILEUPLOADGE]';

-- column comments
COMMENT ON COLUMN germplasm.file_upload_germplasm.file_upload_id
    IS 'File Upload ID: Reference to the file upload transaction where the germplasm is created [GEFILEUPLOADGE_GEFILEUPLOAD_ID]';
COMMENT ON COLUMN germplasm.file_upload_germplasm.germplasm_id
    IS 'Germplasm ID: Reference to the germplasm created from a file upload transaction [GEFILEUPLOADGE_GE_ID]';
COMMENT ON COLUMN germplasm.file_upload_germplasm.seed_id
    IS 'Seed ID: Reference to the seed of the germplasm created from a file upload transaction [GEFILEUPLOADGE_SEED_ID]';
COMMENT ON COLUMN germplasm.file_upload_germplasm.package_id
    IS 'Package ID: Reference to the package of the germplasm created from a file upload transaction [GEFILEUPLOADGE_PKG_ID]';

-- audit column comments
COMMENT ON COLUMN germplasm.file_upload_germplasm.id
    IS 'File Upload Germplasm ID: Database identifier of the record [GEFILEUPLOAD_ID]';
COMMENT ON COLUMN germplasm.file_upload_germplasm.remarks
    IS 'Remarks: Additional notes to record [GEFILEUPLOADGE_REMARKS]';
COMMENT ON COLUMN germplasm.file_upload_germplasm.creation_timestamp
    IS 'Creation Timestamp: Timestamp when the record was created [GEFILEUPLOADGE_CTSTAMP]';
COMMENT ON COLUMN germplasm.file_upload_germplasm.creator_id
    IS 'Creator ID: Reference to the person who created the record [GEFILEUPLOADGE_CPERSON]';
COMMENT ON COLUMN germplasm.file_upload_germplasm.modification_timestamp
    IS 'Modification Timestamp: Timestamp when the record was last modified [GEFILEUPLOADGE_MTSTAMP]';
COMMENT ON COLUMN germplasm.file_upload_germplasm.modifier_id
    IS 'Modifier ID: Reference to the person who last modified the record [GEFILEUPLOADGE_MPERSON]';
COMMENT ON COLUMN germplasm.file_upload_germplasm.notes
    IS 'Notes: Technical details about the record [GEFILEUPLOADGE_NOTES]';
COMMENT ON COLUMN germplasm.file_upload_germplasm.is_void
    IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [GEFILEUPLOADGE_ISVOID]';
COMMENT ON COLUMN germplasm.file_upload_germplasm.event_log
    IS 'Event Log: Event Log: Historical transactions of the record [GEFILEUPLOADGE_EVENTLOG]';

-- indices
CREATE INDEX file_upload_germplasm_file_upload_id_idx
    ON germplasm.file_upload_germplasm USING btree (file_upload_id)
;

CREATE INDEX file_upload_germplasm_germplasm_id_idx
    ON germplasm.file_upload_germplasm USING btree (germplasm_id)
;

CREATE INDEX file_upload_germplasm_seed_id_idx
    ON germplasm.file_upload_germplasm USING btree (seed_id)
;

CREATE INDEX file_upload_germplasm_package_id_idx
    ON germplasm.file_upload_germplasm USING btree (package_id)
;

-- audit indices
CREATE INDEX file_upload_germplasm_creator_id_idx ON germplasm.file_upload_germplasm USING btree (creator_id);
CREATE INDEX file_upload_germplasm_modifier_id_idx ON germplasm.file_upload_germplasm USING btree (modifier_id);
CREATE INDEX file_upload_germplasm_is_void_idx ON germplasm.file_upload_germplasm USING btree (is_void);

-- triggers
CREATE TRIGGER file_upload_germplasm_event_log_tgr BEFORE INSERT OR UPDATE
    ON germplasm.file_upload_germplasm FOR EACH ROW EXECUTE FUNCTION platform.log_record_event();



-- revert changes
--rollback DROP TABLE germplasm.file_upload_germplasm;
