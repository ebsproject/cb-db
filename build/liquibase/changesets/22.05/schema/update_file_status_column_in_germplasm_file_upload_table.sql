--liquibase formatted sql

--changeset postgres:update_file_status_check_constraint context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1204 Update file_status check constraint



-- drop check constraint
ALTER TABLE germplasm.file_upload
    DROP CONSTRAINT file_upload_file_status_chk
;

-- re-create check constraint
ALTER TABLE germplasm.file_upload
    ADD CONSTRAINT file_upload_file_status_chk
    CHECK (file_status = ANY(ARRAY['in queue', 'validation in progress', 'validation error', 'validated', 'creation in progress', 'creation failed', 'completed']))
;




-- revert changes
--rollback ALTER TABLE germplasm.file_upload
--rollback     DROP CONSTRAINT file_upload_file_status_chk
--rollback ;
--rollback 
--rollback ALTER TABLE germplasm.file_upload
--rollback     ADD CONSTRAINT file_upload_file_status_chk
--rollback     CHECK (file_status = ANY(ARRAY['in queue', 'validated', 'validation error', 'creation in progress', 'complete']))
--rollback ;



--changeset postgres:update_file_status_column_comment context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1204 Update file_status column comment



-- update comment
COMMENT ON COLUMN germplasm.file_upload.file_status
    IS 'File Status: Progress of the file upload transaction {in queue, validation in progress, validation error, validated, creation in progress, creation failed, completed} [GEFILEUPLOAD_FILESTATUS]';



-- revert changes
--rollback COMMENT ON COLUMN germplasm.file_upload.file_status
--rollback     IS 'File Status: Progress of the file upload transaction {in queue, validated, validation error, creation in progress, complete} [GEFILEUPLOAD_FILESTATUS]';
