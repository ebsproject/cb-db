--liquibase formatted sql

--changeset postgres:update_variable_foreign_key_constraints context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1200 Update variable reference key constraints


-- drop foreign key constraints
ALTER TABLE master.variable
    DROP CONSTRAINT variable_property_id_fkey
;

ALTER TABLE master.variable
    DROP CONSTRAINT variable_method_id_fkey
;

ALTER TABLE master.variable
    DROP CONSTRAINT variable_scale_id_fkey
;

-- create new foreign key constraints
ALTER TABLE master.variable
    ADD CONSTRAINT variable_property_id_fk
    FOREIGN KEY (property_id)
    REFERENCES master.property (id)
    ON UPDATE CASCADE ON DELETE SET NULL
;

ALTER TABLE master.variable
    ADD CONSTRAINT variable_method_id_fk
    FOREIGN KEY (method_id)
    REFERENCES master.method (id)
    ON UPDATE CASCADE ON DELETE SET NULL
;

ALTER TABLE master.variable
    ADD CONSTRAINT variable_scale_id_fk
    FOREIGN KEY (scale_id)
    REFERENCES master.scale (id)
    ON UPDATE CASCADE ON DELETE SET NULL
;



-- revert changes
--rollback ALTER TABLE master.variable ADD CONSTRAINT variable_property_id_fkey FOREIGN KEY (property_id) REFERENCES master."property"(id) ON UPDATE CASCADE ON DELETE CASCADE;
--rollback ALTER TABLE master.variable ADD CONSTRAINT variable_method_id_fkey FOREIGN KEY (method_id) REFERENCES master.method(id) ON UPDATE CASCADE ON DELETE CASCADE;
--rollback ALTER TABLE master.variable ADD CONSTRAINT variable_scale_id_fkey FOREIGN KEY (scale_id) REFERENCES master.scale(id) ON UPDATE CASCADE ON DELETE CASCADE;
--rollback ALTER TABLE master.variable DROP CONSTRAINT variable_property_id_fk;
--rollback ALTER TABLE master.variable DROP CONSTRAINT variable_method_id_fk;
--rollback ALTER TABLE master.variable DROP CONSTRAINT variable_scale_id_fk;
