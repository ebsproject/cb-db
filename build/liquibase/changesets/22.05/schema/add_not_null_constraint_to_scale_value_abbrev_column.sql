--liquibase formatted sql

--changeset postgres:add_not_null_constraint_to_scale_value_abbrev_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1179 Add not null constraint to abbrev column of master.scale_value table



-- set not null constraint
ALTER TABLE
    master.scale_value
ALTER COLUMN
    abbrev
    SET NOT NULL
;



-- revert changes
--rollback ALTER TABLE
--rollback     master.scale_value
--rollback ALTER COLUMN
--rollback     abbrev
--rollback     DROP NOT NULL
--rollback ;
