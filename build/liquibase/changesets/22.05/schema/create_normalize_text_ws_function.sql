--liquibase formatted sql

--changeset postgres:create_normalize_text_ws_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1179 Create platform.normalize_text_ws() function



-- create normalize_text function with default underscore separator
CREATE OR REPLACE FUNCTION platform.normalize_text_ws(input_text text, separator text DEFAULT '_')
    RETURNS text
    LANGUAGE sql
AS $FUNCTION$
    SELECT UPPER(REGEXP_REPLACE(input_text, '\s', separator, 'g'));
$FUNCTION$;



-- revert changes
--rollback DROP FUNCTION platform.normalize_text_ws(text, text);
