--liquibase formatted sql

--changeset postgres:add_access_data_column_to_occurrence_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1164 Add access_data column to experiment.occurrence table



-- add column
ALTER TABLE
    experiment.occurrence
ADD COLUMN
    access_data jsonb
;

COMMENT ON COLUMN experiment.occurrence.access_data
    IS 'Access Data: Privileges granted to persons and programs to read and/or write data to the record [OCC_ACCDATA]';



-- revert changes
--rollback ALTER TABLE
--rollback     experiment.occurrence
--rollback DROP COLUMN
--rollback     access_data
--rollback ;



--changeset postgres:add_index_access_data_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1164 Add index to access_data column



-- create index
CREATE INDEX occurrence_access_data_idx
    ON experiment.occurrence USING gin (access_data)
;



-- revert changes
--rollback DROP INDEX experiment.occurrence_access_data_idx;
