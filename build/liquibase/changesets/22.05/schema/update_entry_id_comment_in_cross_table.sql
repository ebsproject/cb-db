--liquibase formatted sql

--changeset postgres:update_entry_id_comment_in_cross_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1180 Update entry_id column comment in germplasm.cross table



-- update comment
COMMENT ON COLUMN germplasm.cross.entry_id
    IS 'Entry ID: Reference to the entry in the experiment where the cross is added as an entry (not required in the system and is only used for migrating legacy crosses which are added as entries in cross lists) [CROSS_ENTRY_ID]';



-- revert changes
--rollback COMMENT ON COLUMN germplasm.cross.entry_id
--rollback     IS 'Entry ID: Reference to the entry in the experiment where the cross is added as an entry [CROSS_ENTRY_ID]';
