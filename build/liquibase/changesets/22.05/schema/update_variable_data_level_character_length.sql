--liquibase formatted sql

--changeset postgres:update_variable_data_level_character_length context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1168 Update variable data_level character length



-- update character length
ALTER TABLE
    master.variable
ALTER COLUMN
    data_level
    SET DATA TYPE varchar(64)
;



-- revert changes
--rollback ALTER TABLE
--rollback     master.variable
--rollback ALTER COLUMN
--rollback     data_level
--rollback     SET DATA TYPE varchar(32)
--rollback ;
