--liquibase formatted sql

--changeset postgres:set_default_value_to_geospatial_object_code_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1131 Set default value to geospatial_code column in place.geospatial_object table



-- set default value
ALTER TABLE
    place.geospatial_object
ALTER COLUMN
    geospatial_object_code
    SET DEFAULT place.generate_code('geospatial_object')
;



-- revert changes
--rollback ALTER TABLE
--rollback     place.geospatial_object
--rollback ALTER COLUMN
--rollback     geospatial_object_code
--rollback     DROP DEFAULT
--rollback ;



--changeset postgres:set_default_value_to_facility_code_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1131 Set default value to facility_code column in place.facility



-- set default value
ALTER TABLE
    place.facility
ALTER COLUMN
    facility_code
    SET DEFAULT place.generate_code('facility')
;



-- revert changes
--rollback ALTER TABLE
--rollback     place.facility
--rollback ALTER COLUMN
--rollback     facility_code
--rollback     DROP DEFAULT
--rollback ;
