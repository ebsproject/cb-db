--liquibase formatted sql

--changeset postgres:add_new_list_type_constraints_to_platform.list context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-44 Add new list_type constraints to platform.list



ALTER TABLE 
    platform.list 
DROP CONSTRAINT 
    list_type_chk;

ALTER TABLE 
    platform.list 
ADD CONSTRAINT 
    list_type_chk 
CHECK (((type)::text = ANY (ARRAY['seed'::text, 'plot'::text, 'location'::text, 'germplasm'::text, 'study'::text, 'trait'::text, 'trait protocol'::text, 'package'::text, 'management protocol'::text, 'sites'::text, 'experiment'::text])));



--rollback ALTER TABLE 
--rollback     platform.list 
--rollback DROP CONSTRAINT 
--rollback     list_type_chk;

--rollback ALTER TABLE 
--rollback     platform.list 
--rollback ADD CONSTRAINT 
--rollback     list_type_chk 
--rollback CHECK (((type)::text = ANY (ARRAY['seed'::text, 'plot'::text, 'location'::text, 'germplasm'::text, 'study'::text, 'trait'::text, 'trait protocol'::text, 'package'::text, 'management protocol'::text])));