--liquibase formatted sql

--changeset postgres:add_column_list_sub_type_to_platform.list context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-42 Add column list_sub_type to platform.list



ALTER TABLE 
    platform.list ADD list_sub_type varchar NULL;
COMMENT ON COLUMN 
    platform.list.list_sub_type IS 'List sub type can be entry, population, sample or shipment';



--rollback ALTER TABLE platform.list DROP COLUMN list_sub_type;