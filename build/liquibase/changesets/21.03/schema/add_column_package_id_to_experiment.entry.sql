--liquibase formatted sql

--changeset postgres:add_column_package_id_to_experiment.entry context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-57 Add column package_id to experiment.entry



ALTER TABLE
	experiment.entry
ADD COLUMN
	package_id integer NULL,
ADD FOREIGN KEY 
    (package_id)
REFERENCES 
    germplasm.package (id);
    
COMMENT ON COLUMN 
    experiment.entry.package_id 
IS 
	'Package ID: Reference to the initial package where the entry will be coming from [ENTRY_PKG_ID]';

CREATE INDEX
	entry_package_id_idx
ON 
	experiment.entry
USING 
	btree (package_id);



--rollback ALTER TABLE experiment.entry
--rollback DROP COLUMN  package_id;