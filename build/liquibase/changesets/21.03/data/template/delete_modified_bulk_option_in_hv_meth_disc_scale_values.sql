--liquibase formatted sql

--changeset postgres:delete_modified_bulk_option_in_hv_meth_disc_scale_values context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-34 Delete "Modified Bulk" option in HV_METH_DISC scale values



DELETE FROM
    master.scale_value
WHERE
    scale_id = (SELECT scale_id FROM master.variable where abbrev = 'HV_METH_DISC')
AND
    value = 'Modified bulk'
;

UPDATE
    master.scale_value
SET
    order_number = order_number - 1
WHERE
    scale_id = (SELECT scale_id FROM master.variable where abbrev = 'HV_METH_DISC')
AND
    order_number
IN
    (
        11, 12, 13, 14, 15, 16, 17, 18
    )
;



-- rollback
--rollback INSERT INTO
--rollback     master.scale_value (scale_id, value, order_number, description, display_name, abbrev, creator_id)
--rollback VALUES 
--rollback     ((SELECT scale_id FROM master.variable where abbrev = 'HV_METH_DISC'), 'Modified bulk', 10, 'Modified bulk', 'Modified bulk', 'HV_METH_DISC_MODIFIED_BULK', 1)
--rollback ;
--rollback 
--rollback UPDATE
--rollback     master.scale_value
--rollback SET
--rollback     order_number = order_number + 1
--rollback WHERE
--rollback     scale_id = (SELECT scale_id FROM master.variable where abbrev = 'HV_METH_DISC')
--rollback AND
--rollback     order_number
--rollback IN
--rollback     (10, 11, 12, 13, 14, 15, 16, 17)
--rollback AND
--rollback     value
--rollback NOT IN
--rollback     ('Modified bulk')
--rollback ;