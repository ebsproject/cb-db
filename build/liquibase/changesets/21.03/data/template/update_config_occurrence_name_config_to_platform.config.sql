--liquibase formatted sql

--changeset postgres:update_config_occurrence_name_config_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-67 Update config OCCURRENCE_NAME_CONFIG in platform.config



UPDATE 
    platform.config 
SET 
    config_value = 
'{
    "pattern": [
        {
            "value": "EXPERIMENT_NAME",
            "is_variable": true
        },
        {
            "value": "-",
            "is_variable": false
        },
        {
            "value": "COUNTER",
            "max_value": "EXPERIMENT",
            "zero_padding": "leading",
            "digits": "3"
        }
    ]
}'
WHERE 
    abbrev = 'OCCURRENCE_NAME_CONFIG'
;



-- rollback
--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback '{
--rollback     "pattern": [
--rollback         {
--rollback             "value": "EXPERIMENT_CODE",
--rollback             "is_variable": true
--rollback         },
--rollback         {
--rollback             "value": "-",
--rollback             "is_variable": false
--rollback         },
--rollback         {
--rollback             "value": "SITE_CODE",
--rollback             "is_variable": true
--rollback         },
--rollback         {
--rollback             "value": "-",
--rollback             "is_variable": false
--rollback         },
--rollback         {
--rollback             "value": "COUNTER",
--rollback             "start_seq_no": "1",
--rollback             "zero_padding": "leading",
--rollback             "digits": "3"
--rollback         }
--rollback     ]
--rollback }'
--rollback WHERE 
--rollback     abbrev = 'OCCURRENCE_NAME_CONFIG';