--liquibase formatted sql

--changeset postgres:update_scale_values_for_design context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-39 Update scale values for DESIGN



UPDATE
    master.scale_value
SET
    value = 'Alpha-Lattice',
    description = 'Alpha-Lattice Design',
    display_name = 'Alpha-Lattice'
WHERE scale_id = (select scale_id from master.variable where abbrev = 'DESIGN')
    AND value = 'Alpha-lattice'
    AND is_void = FALSE
;

UPDATE
    master.scale_value
SET
    description = 'Row-Column Design'
WHERE scale_id = (select scale_id from master.variable where abbrev = 'DESIGN')
    AND value = 'Row-Column'
    AND is_void = FALSE
;

UPDATE
    master.scale_value
SET
    value = 'Latin Square Design'
WHERE scale_id = (select scale_id from master.variable where abbrev = 'DESIGN')
    AND value = 'Latin Square Desig'
    AND is_void = FALSE
;



-- rollback
--rollback UPDATE
--rollback     master.scale_value
--rollback SET
--rollback     value = 'Alpha-lattice',
--rollback     description = 'Alpha-lattice Design',
--rollback     display_name = 'Alpha-lattice'
--rollback WHERE scale_id = (select scale_id from master.variable where abbrev = 'DESIGN')
--rollback     AND value = 'Alpha-Lattice'
--rollback     AND is_void = false
--rollback ;
--rollback 
--rollback UPDATE
--rollback     master.scale_value
--rollback SET
--rollback     description = 'Row column Design'
--rollback WHERE scale_id = (select scale_id from master.variable where abbrev = 'DESIGN')
--rollback     AND value = 'Row-Column'
--rollback     AND is_void = false
--rollback ;
--rollback 
--rollback UPDATE
--rollback     master.scale_value
--rollback SET
--rollback     value = 'Latin Square Desig'
--rollback WHERE scale_id = (select scale_id from master.variable where abbrev = 'DESIGN')
--rollback     AND value = 'Latin Square Design'
--rollback     AND is_void = FALSE
--rollback ;