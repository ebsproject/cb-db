--liquibase formatted sql

--changeset postgres:add_config_experiment_name_config_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-67 Create configurations for experiment-related names p1



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id
    )
VALUES
    (
        'EXPERIMENT_NAME_CONFIG',
        'Experiment Name and Experiment Code Pattern',
    $$
    {
        "pattern": [
            {
                "value": "PROGRAM_CODE",
                "is_variable": true
            },
            {
                "value": "-",
                "is_variable": false
            },
            {
                "value": "STAGE_CODE",
                "is_variable": true
            },
            {
                "value": "-",
                "is_variable": false
            },
            {
                "value": "YEAR",
                "is_variable": true
            },
            {
                "value": "-",
                "is_variable": false
            },
            {
                "value": "SEASON_CODE",
                "is_variable": true
            },
            {
                "value": "-",
                "is_variable": false
            },
            {
                "value": "COUNTER",
                "max_value": "PROGRAM|STAGE|YEAR|SEASON",
                "zero_padding": "leading",
                "digits": "3"
            }
        ]
    }
    $$,
        1,
        'experiment_creation',
        1
    )
;



--rollback DELETE FROM platform.config WHERE abbrev = 'EXPERIMENT_NAME_CONFIG';