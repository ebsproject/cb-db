--liquibase formatted sql

--changeset postgres:update_config_harvest_manager_plot_browser_config_wheat_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-46 Update config HARVEST_MANAGER_PLOT_BROWSER_CONFIG_WHEAT in platform.config



UPDATE 
    platform.config 
SET 
    config_value = 
'
{
    "name": "HARVEST_MANAGER_PLOT_BROWSER_CONFIG_WHEAT",
    "values": [
        {
            "germplasm_states": {
                "fixed": {
                    "methods": [
                        "Bulk"
                    ],
                    "display_columns": [
                        "harvestDate",
                        "harvestMethod"
                    ]
                },
                "not_fixed": {
                    "methods": [
                        "Bulk",
                        "Selected bulk",
                        "Single Plant Selection",
                        "Individual spike"
                    ],
                    "display_columns": [
                        "harvestDate",
                        "harvestMethod",
                        "numericVar"
                    ]
                }
            }
        }
    ]
}
' 
WHERE abbrev = 'HARVEST_MANAGER_PLOT_BROWSER_CONFIG_WHEAT';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "name": "HARVEST_MANAGER_PLOT_BROWSER_CONFIG_WHEAT",
--rollback             "values": [
--rollback                 {
--rollback                     "germplasm_states": {
--rollback                         "fixed": {
--rollback                             "methods": [
--rollback                                 "Bulk"
--rollback                             ],
--rollback                             "display_columns": [
--rollback                                 "harvestDate",
--rollback                                 "harvestMethod"
--rollback                             ]
--rollback                         },
--rollback                         "not_fixed": {
--rollback                             "methods": [
--rollback                                 "Bulk",
--rollback                                 "Modified bulk",
--rollback                                 "Single Plant Selection",
--rollback                                 "Individual spike"
--rollback                             ],
--rollback                             "display_columns": [
--rollback                                 "harvestDate",
--rollback                                 "harvestMethod",
--rollback                                 "numericVar"
--rollback                             ]
--rollback                         }
--rollback                     }
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE abbrev = 'HARVEST_MANAGER_PLOT_BROWSER_CONFIG_WHEAT';