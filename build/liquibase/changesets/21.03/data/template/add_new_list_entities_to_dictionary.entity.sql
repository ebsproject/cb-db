--liquibase formatted sql

--changeset postgres:add_new_list_entities_to_dictionary.entity context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-52 Add new list entities to dictionary.entity



INSERT INTO 
    dictionary.entity (abbrev, name, creator_id, table_id)
VALUES
    ('SITE', 'site', 1, (SELECT id FROM dictionary.table WHERE abbrev = 'GEOSPATIAL_OBJECT'));



--rollback DELETE FROM dictionary.entity WHERE abbrev IN ('SITE');