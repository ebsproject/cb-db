--liquibase formatted sql

--changeset postgres:add_config_occurence_name_config_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-67 Create configurations for experiment-related names p2



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id
    )
VALUES
    (
        'OCCURRENCE_NAME_CONFIG',
        'Occurrence Name and Occurrence Code Pattern',
    $$
    {
        "pattern": [
            {
                "value": "EXPERIMENT_CODE",
                "is_variable": true
            },
            {
                "value": "-",
                "is_variable": false
            },
            {
                "value": "SITE_CODE",
                "is_variable": true
            },
            {
                "value": "-",
                "is_variable": false
            },
            {
                "value": "COUNTER",
                "start_seq_no": "1",
                "zero_padding": "leading",
                "digits": "3"
            }
        ]
    }
    $$,
        1,
        'experiment_creation',
        1
    )
;



--rollback DELETE FROM platform.config WHERE abbrev = 'OCCURRENCE_NAME_CONFIG';