--liquibase formatted sql

--changeset postgres:add_unkown_record_place.geospatial_object_table context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-40 Create an UNKNOWN record place.geospatial_object table



INSERT INTO
    place.geospatial_object (
        geospatial_object_code,
        geospatial_object_name,
        geospatial_object_type,
        geospatial_object_subtype,
        creator_id
    )
VALUES
    ('UNKNOWN', 'UNKNOWN', 'site', 'breeding location', 1)
;



--rollback DELETE FROM place.geospatial_object WHERE geospatial_object_code = 'UNKNOWN';