--liquibase formatted sql

--changeset postgres:update_config_default_dashboard_widgets_config_rice_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-46 Update config DEFAULT_DASHBOARD_WIDGETS in platform.config



UPDATE 
    platform.config
SET
    config_value = '{"col_1": [{"widget_id": 1}, {"widget_id": 4}], "col_2": [{"widget_id": 9}]}'
WHERE
    abbrev = 'DEFAULT_DASHBOARD_WIDGETS';



--rollback UPDATE 
--rollback     platform.config
--rollback SET
--rollback     config_value =
--rollback '
--rollback     {
--rollback         "col_1": [
--rollback             {
--rollback                 "widget_id": 1
--rollback             },
--rollback             {
--rollback                 "widget_id": 4
--rollback             }
--rollback         ],
--rollback         "col_2": [
--rollback             {
--rollback                 "widget_id": 2
--rollback             },
--rollback             {
--rollback                 "widget_id": 9
--rollback             }
--rollback         ]
--rollback     }
--rollback '
--rollback WHERE
--rollback     abbrev = 'DEFAULT_DASHBOARD_WIDGETS';