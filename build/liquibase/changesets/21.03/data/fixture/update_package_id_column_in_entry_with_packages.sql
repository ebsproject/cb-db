--liquibase formatted sql

--changeset postgres:update_package_id_column_in_entry_with_packages context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-155 Update package_id column of entry table with reference to packages



-- update package_id column
WITH t_packages AS (
    -- get package_id from planting_instruction
    SELECT
        plantinst.entry_id,
        array_agg(DISTINCT plantinst.package_id) AS package_id_list
    FROM
        experiment.entry AS ent
        INNER JOIN experiment.planting_instruction AS plantinst
            ON ent.id = plantinst.entry_id
    WHERE
        ent.package_id IS NULL
        AND plantinst.package_id IS NOT NULL
    GROUP BY
        plantinst.entry_id
)
-- update entry.package_id from planting_instruction 
UPDATE
    experiment.entry AS ent
SET
    package_id = t.package_id_list[1]
FROM
    t_packages AS t
WHERE
    ent.id = t.entry_id
    AND ent.package_id IS NULL
;



-- revert changes
--rollback UPDATE
--rollback     experiment.entry
--rollback SET
--rollback     package_id = NULL
--rollback WHERE
--rollback     package_id IS NOT NULL
--rollback ;
