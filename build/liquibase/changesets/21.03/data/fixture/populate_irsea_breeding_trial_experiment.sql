--liquibase formatted sql

--changeset postgres:populate_irsea_breeding_trial_experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate IRSEA breeding trial experiment



INSERT INTO
    experiment.experiment (
        program_id, pipeline_id, stage_id, project_id, experiment_year, season_id, planting_season, experiment_code, experiment_name,
        experiment_type, experiment_sub_type, experiment_sub_sub_type, experiment_design_type, experiment_status,
        steward_id, creator_id, is_void, data_process_id, crop_id
    )
SELECT
    (SELECT id FROM tenant.program WHERE program_code = 'IRSEA') AS program_id,
    (SELECT id FROM tenant.pipeline WHERE pipeline_code = 'IRSEA_PIPELINE') AS pipeline_id,
    (SELECT id FROM tenant.stage WHERE stage_code = 'AYT') AS stage_id,
    (SELECT id FROM tenant.project WHERE project_code = 'IRSEA_PROJECT') AS project_id,
    2021 AS experiment_year,
    (SELECT id FROM tenant.season WHERE season_code = 'DS') AS season_id,
    '2021DS' AS planting_season,
    experiment.generate_code('experiment') AS experiment_code,
    'IRSEA-AYT-2021-DS-1' AS experiment_name,
    'Breeding Trial' AS experiment_type,
    NULL AS experiment_sub_type,
    NULL AS experiment_sub_sub_type,
    'Alpha-lattice' AS experiment_design_type,
    'planted' AS experiment_status,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS steward_id,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS creator_id,
    FALSE AS is_void,
    (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_DATA_PROCESS') AS data_process_id,
    (SELECT id FROM tenant.crop WHERE crop_code = 'RICE') AS crop_id
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment
--rollback WHERE
--rollback     experiment_name = 'IRSEA-AYT-2021-DS-1'
--rollback ;



--changeset postgres:populate_irsea_rice_breeding_trial_in_tenant.protocol context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate IRSEA breeding trial experiment in tenant.protocol



INSERT INTO
    tenant.protocol (
        protocol_code, protocol_name, protocol_type, program_id, creator_id
    )
VALUES 
    ('MANAGEMENT_PROTOCOL_TMP','Management Protocol Tmp','management',(SELECT id FROM tenant.program WHERE program_code='IRSEA'), (SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ('TRAIT_PROTOCOL_TMP','Trait Protocol Tmp','trait',(SELECT id FROM tenant.program WHERE program_code='IRSEA'), (SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ('PLANTING_PROTOCOL_TMP','Planting Protocol Tmp','planting',(SELECT id FROM tenant.program WHERE program_code='IRSEA'), (SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ('HARVEST_PROTOCOL_TMP','Harvest Protocol Tmp','harvest',(SELECT id FROM tenant.program WHERE program_code='IRSEA'), (SELECT id FROM tenant.person WHERE username = 'nicola.costa'));



--rollback DELETE FROM tenant.protocol WHERE protocol_code IN ('MANAGEMENT_PROTOCOL_TMP','TRAIT_PROTOCOL_TMP','PLANTING_PROTOCOL_TMP','HARVEST_PROTOCOL_TMP');



--changeset postgres:populate_irsea_rice_breeding_trial_in_experiment.experiment_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate IRSEA breeding trial experiment in experiment.experiment_data



INSERT INTO
    experiment.experiment_data (
        experiment_id, variable_id, data_value, data_qc_code, protocol_id, creator_id
    )
VALUES
    ((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-AYT-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='MANAGEMENT_PROTOCOL_LIST_ID'),'1251','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Management Protocol Tmp'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-AYT-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='TRAIT_PROTOCOL_LIST_ID'),'1252','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Trait Protocol Tmp'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-AYT-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='ESTABLISHMENT'),'transplanted','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-AYT-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='PLANTING_TYPE'),'Flat','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-AYT-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='PLOT_TYPE'),'5R x 25H','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-AYT-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='ROWS_PER_PLOT_CONT'),'5','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-AYT-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='HILLS_PER_ROW_CONT'),'25','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-AYT-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='DIST_BET_ROWS'),'20','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-AYT-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='DIST_BET_HILLS'),'20','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-AYT-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='PLOT_LN_1'),'5','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-AYT-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='PLOT_WIDTH_RICE'),'1','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-AYT-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='PLOT_AREA_1'),'5','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-AYT-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='ALLEY_LENGTH'),'1','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-AYT-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='SEEDING_RATE'),'Normal','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-AYT-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='PROTOCOL_TARGET_LEVEL'),'occurrence','N',NULL,(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-AYT-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='HV_METH_DISC'),'Bulk','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Harvest Protocol Tmp'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'));



--rollback DELETE FROM experiment.experiment_data WHERE experiment_id = (SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-AYT-2021-DS-1');



--changeset postgres:populate_irsea_rice_breeding_trial_in_experiment.experiment_protocol context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate IRSEA breeding trial experiment in experiment.experiment_protocol



INSERT INTO
    experiment.experiment_protocol (
        experiment_id, protocol_id, order_number, creator_id
    )
VALUES
    ((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-AYT-2021-DS-1'),(SELECT id FROM tenant.protocol WHERE protocol_name='Management Protocol Tmp'),1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-AYT-2021-DS-1'),(SELECT id FROM tenant.protocol WHERE protocol_name='Trait Protocol Tmp'),2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-AYT-2021-DS-1'),(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp'),3,(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-AYT-2021-DS-1'),(SELECT id FROM tenant.protocol WHERE protocol_name='Harvest Protocol Tmp'),4,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'));



--rollback DELETE FROM experiment.experiment_protocol WHERE experiment_id = (SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-AYT-2021-DS-1');



--changeset postgres:populate_irsea_rice_breeding_trial_in_experiment.entry_list context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate IRSEA breeding trial in experiment.entry_list



INSERT INTO
    experiment.entry_list (
        entry_list_code, entry_list_name, entry_list_status, experiment_id, creator_id, is_void, entry_list_type
    )
SELECT
    experiment.generate_code('entry_list') AS entry_list_code,
    'IRSEA-AYT-2021-DS-1 Entry List' AS entry_list_name,
    'completed' AS entry_list_status,
    (SELECT id FROM experiment.experiment WHERE experiment_name = 'IRSEA-AYT-2021-DS-1') AS experiment_id,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS creator_id,
    FALSE AS is_void,
    'entry list' AS entry_list_type
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_list
--rollback WHERE
--rollback     entry_list_name = 'IRSEA-AYT-2021-DS-1 Entry List';



--changeset postgres:populate_irsea_breeding_trial_in_experiment.entry context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate IRSEA breeding trial in experiment.entry



INSERT INTO
   experiment.entry (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status,
       entry_list_id, germplasm_id, seed_id, creator_id, is_void
   )
SELECT
    t.entry_number AS entry_code,
    t.entry_number,
    t.designation AS entry_name,
    'entry' AS entry_type,
    NULL AS entry_role,
    'active' AS entry_status,
    entlist.id AS entry_list_id,
    ge.id AS germplasm_id,
    seed.id AS seed_id,
    person.id AS creator_id,
    FALSE AS is_void
FROM
    (
        VALUES        
        (1,'BRRI DHAN 55','301463573')
        ,(2,'BRRI DHAN 58','300816318')
        ,(3,'BG 34-11::IRGC 15782-1','300258398')
        ,(4,'DANAU LAUT TAWAR::C1','300280798')
        ,(5,'IR 77384-12-35-3-12-1-B::IRGC 117299-1','300258399')
        ,(6,'JINLING 78-102::IRGC 88421-1','300258400')
        ,(7,'KIRIMURUNGA::IRGC 15585-1','300258401')
        ,(8,'WAS 174-B-3-5::C1','300258402')
        ,(9,'WAS 200-B-B-1-1-1::C1','300258403')
        ,(10,'WAS 194-B-3-2-5::C1','4495045')
        ,(11,'DAW HAWM 48-1-26','4702834')
        ,(12,'UTRI MERAH','4702662')
        ,(13,'BALIMAU PUTIH','300258385')
        ,(14,'BINAM','300258386')
        ,(15,'FL 478','4702663')
        ,(16,'RATHU HEENATI','4442995')
    ) AS t (
        entry_number, designation, seed_name
    )
    INNER JOIN germplasm.germplasm AS ge
        ON t.designation = ge.designation
    INNER JOIN germplasm.seed AS seed
        ON ge.id = seed.germplasm_id
        AND t.seed_name = seed.seed_name
    INNER JOIN experiment.entry_list as entlist
        ON entlist.entry_list_name = 'IRSEA-AYT-2021-DS-1 Entry List'
    INNER JOIN tenant.person AS person
        ON person.username = 'nicola.costa'
ORDER BY
    t.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry AS e
--rollback USING
--rollback     experiment.entry_list AS el
--rollback WHERE
--rollback     e.entry_list_id = el.id
--rollback     AND el.entry_list_name = 'IRSEA-AYT-2021-DS-1 Entry List'
--rollback ;



--changeset postgres:populate_rice_breeding_trial_in_experiment.occurrence context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate rice breeding trial in experiment.occurrence



INSERT INTO
    experiment.occurrence (
        occurrence_code,
        occurrence_name,
        occurrence_status,
        experiment_id,
        site_id,
        field_id,
        rep_count,
        occurrence_number,
        creator_id
    )
SELECT
    experiment.generate_code('occurrence') AS occurrence_code,
    'IRSEA-AYT-2021-DS-1_OCC1' AS occurrence_name,
    'planted' AS occurrence_status,
    expt.id AS experiment_id,
    geo.id AS site_id,
    field.id AS field_id,
    NULL AS rep_count,
    1 AS occurrence_number,
    person.id AS creator_id
FROM
    experiment.experiment AS expt,
    place.geospatial_object AS geo,
    place.geospatial_object AS field,
    tenant.person AS person
WHERE
    expt.experiment_name = 'IRSEA-AYT-2021-DS-1'
    AND geo.geospatial_object_code = 'IRRIHQ'
    AND field.geospatial_object_code = 'IRRI_FARMS'
    AND person.username = 'nicola.costa'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence
--rollback WHERE
--rollback     occurrence_name = 'IRSEA-AYT-2021-DS-1_OCC1'
--rollback ;



--changeset postgres:populate_rice_breeding_trial_in_experiment.occurrence_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate rice breeding trial in experiment.occurrence_data



INSERT INTO
    experiment.occurrence_data (
        occurrence_id, variable_id, data_value, data_qc_code, creator_id
    )
VALUES  
    ((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM master.variable WHERE abbrev='CONTCT_PERSON_CONT'),'Allan, Jennifer','G',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'));



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence_data
--rollback WHERE
--rollback     occurrence_id = (SELECT id FROM experiment.occurrence WHERE occurrence_name = 'IRSEA-AYT-2021-DS-1_OCC1')
--rollback ;



--changeset postgres:populate_rice_breeding_trial_in_place.geospatial_object context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate rice breeding trial in place.geospatial_object



INSERT INTO
    place.geospatial_object (
        geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, parent_geospatial_object_id, root_geospatial_object_id
    )
VALUES
    (
        place.generate_code('geospatial_object'), 
        'IRSEA-AYT-2021-DS-1_LOC1', 
        'planting area', 
        'breeding_location', 
        '1', 
        (SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRI_FARMS'), 
        (SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRIHQ')
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object
--rollback WHERE
--rollback     geospatial_object_name = 'IRSEA-AYT-2021-DS-1_LOC1'
--rollback ;



--changeset postgres:populate_rice_breeding_trial_in_experiment.location context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate rice breeding trial in experiment.location



INSERT INTO
   experiment.location (
       location_code, location_name, location_status, location_type,
       location_year, season_id, location_number, site_id, field_id,
       steward_id, geospatial_object_id, creator_id
   )
SELECT
    experiment.generate_code('location') AS location_code,
    'IRSEA-AYT-2021-DS-1_LOC1' AS location_name,
    'planted' AS location_status,
    'planting area' AS location_type,
    2021 AS location_year,
    season.id AS season_id,
    1 AS location_number,
    site.id AS site_id,
    field.id AS field_id,
    person.id AS steward_id,
    geo.id AS geospatial_object_id,
    person.id AS creator_id
FROM
    tenant.person AS person,
    tenant.season AS season,
    place.geospatial_object AS geo,
    place.geospatial_object AS site,
    place.geospatial_object AS field
WHERE
    person.username = 'nicola.costa'
    AND season.season_code = 'DS'
    AND geo.geospatial_object_name = 'IRSEA-AYT-2021-DS-1_LOC1'
    AND site.geospatial_object_code = 'IRRIHQ'
    AND field.geospatial_object_code = 'IRRI_FARMS'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location
--rollback WHERE
--rollback     location_name = 'IRSEA-AYT-2021-DS-1_LOC1'
--rollback ;



--changeset postgres:populate_rice_breeding_trial_in_experiment.location_occurrence_group context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate rice breeding trial in experiment.location_occurrence_group



INSERT INTO 
    experiment.location_occurrence_group (
        location_id,occurrence_id,order_number,creator_id
    )
SELECT
    t.*
FROM
    (
      VALUES 
      (
          (SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),
          (SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),
          1,
          (SELECT id FROM tenant.person WHERE username = 'nicola.costa')
      )
    ) t;



--rollback DELETE FROM 
--rollback     experiment.location_occurrence_group
--rollback WHERE
--rollback     location_id = (SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1')
--rollback AND
--rollback     occurrence_id IN (SELECT id FROM experiment.occurrence WHERE occurrence_name IN ('IRSEA-AYT-2021-DS-1_OCC1'));



--changeset postgres:populate_rice_breeding_trial_in_experiment.plot context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate rice breeding trial in experiment.plot



INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, plot_order_number, pa_x, pa_y, field_x, field_y, plot_status, creator_id, block_number, harvest_status
   )
VALUES
    ((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='KIRIMURUNGA::IRGC 15585-1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'216',32,'plotType',2,32,1,NULL,32,1,32,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),2,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='WAS 200-B-B-1-1-1::C1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'215',31,'plotType',2,31,1,NULL,31,1,31,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),2,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='FL 478' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'214',30,'plotType',2,30,1,NULL,30,1,30,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),2,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='BRRI DHAN 58' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'213',29,'plotType',2,29,1,NULL,29,1,29,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),2,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='UTRI MERAH' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'212',28,'plotType',2,28,1,NULL,28,1,28,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),2,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='BINAM' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'211',27,'plotType',2,27,1,NULL,27,1,27,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),2,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='DANAU LAUT TAWAR::C1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'210',26,'plotType',2,26,1,NULL,26,1,26,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),2,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='DAW HAWM 48-1-26' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'209',25,'plotType',2,25,1,NULL,25,1,25,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),2,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='WAS 174-B-3-5::C1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'208',24,'plotType',2,24,1,NULL,24,1,24,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),1,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='BRRI DHAN 55' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'207',23,'plotType',2,23,1,NULL,23,1,23,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),1,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='RATHU HEENATI' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'206',22,'plotType',2,22,1,NULL,22,1,22,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),1,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='BALIMAU PUTIH' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'205',21,'plotType',2,21,1,NULL,21,1,21,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),1,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='JINLING 78-102::IRGC 88421-1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'204',20,'plotType',2,20,1,NULL,20,1,20,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),1,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='BG 34-11::IRGC 15782-1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'203',19,'plotType',2,19,1,NULL,19,1,19,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),1,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='IR 77384-12-35-3-12-1-B::IRGC 117299-1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'202',18,'plotType',2,18,1,NULL,18,1,18,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),1,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='WAS 194-B-3-2-5::C1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'201',17,'plotType',2,17,1,NULL,17,1,17,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),1,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='WAS 174-B-3-5::C1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'116',16,'plotType',1,16,1,NULL,16,1,16,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),2,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='KIRIMURUNGA::IRGC 15585-1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'115',15,'plotType',1,15,1,NULL,15,1,15,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),2,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='BG 34-11::IRGC 15782-1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'114',14,'plotType',1,14,1,NULL,14,1,14,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),2,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='RATHU HEENATI' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'113',13,'plotType',1,13,1,NULL,13,1,13,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),2,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='BRRI DHAN 58' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'112',12,'plotType',1,12,1,NULL,12,1,12,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),2,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='DAW HAWM 48-1-26' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'111',11,'plotType',1,11,1,NULL,11,1,11,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),2,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='JINLING 78-102::IRGC 88421-1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'110',10,'plotType',1,10,1,NULL,10,1,10,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),2,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='BINAM' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'109',9,'plotType',1,9,1,NULL,9,1,9,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),2,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='BRRI DHAN 55' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'108',8,'plotType',1,8,1,NULL,8,1,8,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),1,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='FL 478' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'107',7,'plotType',1,7,1,NULL,7,1,7,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),1,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='IR 77384-12-35-3-12-1-B::IRGC 117299-1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'106',6,'plotType',1,6,1,NULL,6,1,6,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),1,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='DANAU LAUT TAWAR::C1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'105',5,'plotType',1,5,1,NULL,5,1,5,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),1,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='WAS 200-B-B-1-1-1::C1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'104',4,'plotType',1,4,1,NULL,4,1,4,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),1,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='BALIMAU PUTIH' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'103',3,'plotType',1,3,1,NULL,3,1,3,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),1,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='WAS 194-B-3-2-5::C1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'102',2,'plotType',1,2,1,NULL,2,1,2,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),1,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-AYT-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='UTRI MERAH' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')),'101',1,'plotType',1,1,1,NULL,1,1,1,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),1,'NO_HARVEST')
    ;



-- revert changes OCC1
--rollback DELETE FROM
--rollback     experiment.plot AS plot
--rollback USING
--rollback     experiment.occurrence AS occ,
--rollback     experiment.location AS loc
--rollback WHERE
--rollback     plot.occurrence_id = occ.id
--rollback     AND occ.occurrence_name = 'IRSEA-AYT-2021-DS-1_OCC1'
--rollback     AND loc.location_name = 'IRSEA-AYT-2021-DS-1_LOC1'
--rollback ;



--changeset postgres:populate_rice_breeding_trial_in_experiment.planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate rice breeding trial in experiment.planting_instruction



INSERT INTO
   experiment.planting_instruction (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status, entry_id,
       plot_id, germplasm_id, seed_id, creator_id
   )
SELECT
    ent.entry_code,
    ent.entry_number,
    ent.entry_name,
    ent.entry_type,
    ent.entry_role,
    ent.entry_status,
    ent.id AS entry_id,
    plot.id AS plot_id,
    ent.germplasm_id,
    ent.seed_id,
    person.id AS creator_id
FROM
    experiment.entry_list AS entlist
    INNER JOIN experiment.entry AS ent
        ON entlist.id = ent.entry_list_id
     INNER JOIN experiment.plot AS plot
         ON plot.entry_id = ent.id
     INNER JOIN tenant.person AS person
         ON person.username = 'nicola.costa'
WHERE
    entlist.entry_list_name = 'IRSEA-AYT-2021-DS-1 Entry List'
ORDER BY
     plot.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.planting_instruction AS plantinst
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'IRSEA-AYT-2021-DS-1 Entry List';



--changeset postgres:populate_rice_breeding_trial_in_experiment.experiment_design context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate rice breeding trial in experiment.experiment_design



INSERT INTO
   experiment.experiment_design (
       occurrence_id, design_id, plot_id, block_type, block_value,
       block_level_number, creator_id, block_name
   )
VALUES
    ((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),1,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='UTRI MERAH' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '101' AND ep.plot_number = 1),'replication block',1,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),1,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='WAS 194-B-3-2-5::C1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '102' AND ep.plot_number = 2),'replication block',1,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),1,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='BALIMAU PUTIH' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '103' AND ep.plot_number = 3),'replication block',1,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),1,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='WAS 200-B-B-1-1-1::C1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '104' AND ep.plot_number = 4),'replication block',1,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),1,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='DANAU LAUT TAWAR::C1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '105' AND ep.plot_number = 5),'replication block',1,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),1,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='IR 77384-12-35-3-12-1-B::IRGC 117299-1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '106' AND ep.plot_number = 6),'replication block',1,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),1,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='FL 478' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '107' AND ep.plot_number = 7),'replication block',1,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),1,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='BRRI DHAN 55' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '108' AND ep.plot_number = 8),'replication block',1,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),1,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='BINAM' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '109' AND ep.plot_number = 9),'replication block',1,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),1,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='JINLING 78-102::IRGC 88421-1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '110' AND ep.plot_number = 10),'replication block',1,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),1,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='DAW HAWM 48-1-26' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '111' AND ep.plot_number = 11),'replication block',1,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),1,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='BRRI DHAN 58' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '112' AND ep.plot_number = 12),'replication block',1,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),1,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='RATHU HEENATI' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '113' AND ep.plot_number = 13),'replication block',1,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),1,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='BG 34-11::IRGC 15782-1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '114' AND ep.plot_number = 14),'replication block',1,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),1,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='KIRIMURUNGA::IRGC 15585-1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '115' AND ep.plot_number = 15),'replication block',1,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),1,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='WAS 174-B-3-5::C1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '116' AND ep.plot_number = 16),'replication block',1,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='WAS 194-B-3-2-5::C1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '201' AND ep.plot_number = 17),'replication block',2,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='IR 77384-12-35-3-12-1-B::IRGC 117299-1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '202' AND ep.plot_number = 18),'replication block',2,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='BG 34-11::IRGC 15782-1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '203' AND ep.plot_number = 19),'replication block',2,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='JINLING 78-102::IRGC 88421-1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '204' AND ep.plot_number = 20),'replication block',2,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='BALIMAU PUTIH' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '205' AND ep.plot_number = 21),'replication block',2,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='RATHU HEENATI' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '206' AND ep.plot_number = 22),'replication block',2,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='BRRI DHAN 55' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '207' AND ep.plot_number = 23),'replication block',2,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='WAS 174-B-3-5::C1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '208' AND ep.plot_number = 24),'replication block',2,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='DAW HAWM 48-1-26' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '209' AND ep.plot_number = 25),'replication block',2,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='DANAU LAUT TAWAR::C1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '210' AND ep.plot_number = 26),'replication block',2,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='BINAM' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '211' AND ep.plot_number = 27),'replication block',2,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='UTRI MERAH' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '212' AND ep.plot_number = 28),'replication block',2,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='BRRI DHAN 58' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '213' AND ep.plot_number = 29),'replication block',2,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='FL 478' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '214' AND ep.plot_number = 30),'replication block',2,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='WAS 200-B-B-1-1-1::C1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '215' AND ep.plot_number = 31),'replication block',2,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='KIRIMURUNGA::IRGC 15585-1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '216' AND ep.plot_number = 32),'replication block',2,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'replicate')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='UTRI MERAH' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '101' AND ep.plot_number = 1),'replication block',1,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='WAS 194-B-3-2-5::C1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '102' AND ep.plot_number = 2),'replication block',1,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='BALIMAU PUTIH' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '103' AND ep.plot_number = 3),'replication block',1,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='WAS 200-B-B-1-1-1::C1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '104' AND ep.plot_number = 4),'replication block',1,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='DANAU LAUT TAWAR::C1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '105' AND ep.plot_number = 5),'replication block',1,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='IR 77384-12-35-3-12-1-B::IRGC 117299-1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '106' AND ep.plot_number = 6),'replication block',1,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='FL 478' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '107' AND ep.plot_number = 7),'replication block',1,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='BRRI DHAN 55' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '108' AND ep.plot_number = 8),'replication block',1,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),3,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='BINAM' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '109' AND ep.plot_number = 9),'replication block',2,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),3,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='JINLING 78-102::IRGC 88421-1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '110' AND ep.plot_number = 10),'replication block',2,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),3,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='DAW HAWM 48-1-26' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '111' AND ep.plot_number = 11),'replication block',2,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),3,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='BRRI DHAN 58' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '112' AND ep.plot_number = 12),'replication block',2,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),3,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='RATHU HEENATI' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '113' AND ep.plot_number = 13),'replication block',2,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),3,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='BG 34-11::IRGC 15782-1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '114' AND ep.plot_number = 14),'replication block',2,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),3,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='KIRIMURUNGA::IRGC 15585-1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '115' AND ep.plot_number = 15),'replication block',2,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),3,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='WAS 174-B-3-5::C1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '116' AND ep.plot_number = 16),'replication block',2,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='WAS 194-B-3-2-5::C1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '201' AND ep.plot_number = 17),'replication block',1,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='IR 77384-12-35-3-12-1-B::IRGC 117299-1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '202' AND ep.plot_number = 18),'replication block',1,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='BG 34-11::IRGC 15782-1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '203' AND ep.plot_number = 19),'replication block',1,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='JINLING 78-102::IRGC 88421-1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '204' AND ep.plot_number = 20),'replication block',1,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='BALIMAU PUTIH' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '205' AND ep.plot_number = 21),'replication block',1,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='RATHU HEENATI' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '206' AND ep.plot_number = 22),'replication block',1,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='BRRI DHAN 55' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '207' AND ep.plot_number = 23),'replication block',1,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),2,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='WAS 174-B-3-5::C1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '208' AND ep.plot_number = 24),'replication block',1,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),3,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='DAW HAWM 48-1-26' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '209' AND ep.plot_number = 25),'replication block',2,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),3,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='DANAU LAUT TAWAR::C1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '210' AND ep.plot_number = 26),'replication block',2,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),3,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='BINAM' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '211' AND ep.plot_number = 27),'replication block',2,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),3,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='UTRI MERAH' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '212' AND ep.plot_number = 28),'replication block',2,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),3,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='BRRI DHAN 58' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '213' AND ep.plot_number = 29),'replication block',2,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),3,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='FL 478' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '214' AND ep.plot_number = 30),'replication block',2,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),3,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='WAS 200-B-B-1-1-1::C1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '215' AND ep.plot_number = 31),'replication block',2,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-AYT-2021-DS-1_OCC1'),3,(SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name='KIRIMURUNGA::IRGC 15585-1' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-AYT-2021-DS-1 Entry List')) AND ep.plot_code = '216' AND ep.plot_number = 32),'replication block',2,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),'block')
    ;



--rollback DELETE FROM experiment.experiment_design WHERE occurrence_id IN (SELECT id FROM experiment.occurrence WHERE occurrence_name IN ('IRSEA-AYT-2021-DS-1_OCC1'));