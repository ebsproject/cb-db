--liquibase formatted sql

--changeset postgres:populate_breeding_trial_for_af_experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-59 Populate breeding trial for AF experiment



INSERT INTO
    experiment.experiment (
        program_id, pipeline_id, stage_id, project_id, experiment_year, season_id, planting_season, experiment_code, experiment_name,
        experiment_type, experiment_sub_type, experiment_sub_sub_type, experiment_design_type, experiment_status,
        steward_id, creator_id, is_void, data_process_id, crop_id
    )
SELECT
    (SELECT id FROM tenant.program WHERE program_code = 'KE') AS program_id,
    NULL pipeline_id,
    (SELECT id FROM tenant.stage WHERE stage_code = 'IYT') AS stage_id,
    NULL project_id,
    2021 AS experiment_year,
    (SELECT id FROM tenant.season WHERE season_code = 'A') AS season_id,
    NULL AS planting_season,
    experiment.generate_code('experiment') AS experiment_code,
    'AF2021_001' AS experiment_name,
    'Breeding Trial' AS experiment_type,
    NULL AS experiment_sub_type,
    NULL AS experiment_sub_sub_type,
    'RCBD' AS experiment_design_type,
    'planted' AS experiment_status,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS steward_id,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS creator_id,
    FALSE AS is_void,
    (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_DATA_PROCESS') AS data_process_id,
    (SELECT id FROM tenant.crop WHERE crop_code = 'MAIZE') AS crop_id
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment
--rollback WHERE
--rollback     experiment_name = 'AF2021_001'
--rollback ;



--changeset postgres:populate_breeding_trial_for_af_in_tenant.protocol context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-59 Populate breeding trial for AF experiment in tenant.protocol



INSERT INTO
    tenant.protocol (
        protocol_code, protocol_name, protocol_type, program_id, creator_id
    )
VALUES 
    ('MANAGEMENT_PROTOCOL_TMP_AFBT','Management Protocol Tmp AFBT','management',(SELECT id FROM tenant.program WHERE program_code='KE'), (SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ('TRAIT_PROTOCOL_TMP_AFBT','Trait Protocol Tmp AFBT','trait',(SELECT id FROM tenant.program WHERE program_code='KE'), (SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ('PLANTING_PROTOCOL_TMP_AFBT','Planting Protocol Tmp AFBT','planting',(SELECT id FROM tenant.program WHERE program_code='KE'), (SELECT id FROM tenant.person WHERE username = 'nicola.costa'));



--rollback DELETE FROM tenant.protocol WHERE protocol_code IN ('MANAGEMENT_PROTOCOL_TMP_AFBT','TRAIT_PROTOCOL_TMP_AFBT','PLANTING_PROTOCOL_TMP_AFBT');



--changeset postgres:populate_breeding_trial_for_af_in_experiment.experiment_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-59 Populate breeding trial for AF experiment in experiment.experiment_data



INSERT INTO
    experiment.experiment_data (
        experiment_id, variable_id, data_value, data_qc_code, protocol_id, creator_id
    )
SELECT
    exp.id AS experiment_id,
    (SELECT id FROM master.variable WHERE abbrev=t.variable_abbrev) AS variable_id,
    t.data_value AS data_value,
    t.data_qc_code AS data_qc_code,
    (SELECT id FROM tenant.protocol WHERE protocol_name=t.protocol_name) AS protocol_id,
    psn.id AS creator_id
FROM
    (
        VALUES
            ('TRAIT_PROTOCOL_LIST_ID','1296','N','Trait Protocol Tmp AFBT'),
            ('ESTABLISHMENT','direct seeded','N','Planting Protocol Tmp AFBT'),
            ('PLANTING_TYPE','Flat','N','Planting Protocol Tmp AFBT'),
            ('PLOT_TYPE','Direct_seeded_unknown','N','Planting Protocol Tmp AFBT'),
            ('ROWS_PER_PLOT_CONT','2','N','Planting Protocol Tmp AFBT'),
            ('DIST_BET_ROWS','0.75','N','Planting Protocol Tmp AFBT'),
            ('PLOT_WIDTH','1.5','N','Planting Protocol Tmp AFBT'),
            ('PLOT_LN','2.5','N','Planting Protocol Tmp AFBT'),
            ('PLOT_AREA_2','3.75','N','Planting Protocol Tmp AFBT'),
            ('ALLEY_LENGTH','0.8','N','Planting Protocol Tmp AFBT'),
            ('SEEDING_RATE','Normal','N','Planting Protocol Tmp AFBT'),
            ('PLANTING_INSTRUCTIONS','30 seeds per row plot','N','Planting Protocol Tmp AFBT'),
            ('PROTOCOL_TARGET_LEVEL','occurrence','N',NULL)
    ) AS t (variable_abbrev, data_value, data_qc_code, protocol_name)
INNER JOIN
    experiment.experiment exp
ON
    exp.experiment_name = 'AF2021_001'
INNER JOIN
    tenant.person psn
ON
    psn.username = 'nicola.costa'



--rollback DELETE FROM experiment.experiment_data WHERE experiment_id = (SELECT id FROM experiment.experiment WHERE experiment_name='AF2021_001');



--changeset postgres:populate_breeding_trial_for_af_in_experiment.experiment_protocol context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-59 Populate breeding trial for AF experiment in experiment.experiment_protocol



INSERT INTO
    experiment.experiment_protocol (
        experiment_id, protocol_id, order_number, creator_id
    )
VALUES
    ((SELECT id FROM experiment.experiment WHERE experiment_name='AF2021_001'),(SELECT id FROM tenant.protocol WHERE protocol_name='Trait Protocol Tmp AFBT'),1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.experiment WHERE experiment_name='AF2021_001'),(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp AFBT'),2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
;



--rollback DELETE FROM experiment.experiment_protocol WHERE experiment_id = (SELECT id FROM experiment.experiment WHERE experiment_name='AF2021_001');



--changeset postgres:populate_breeding_trial_for_af_in_experiment.entry_list context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-59 Populate breeding trial for AF in experiment.entry_list



INSERT INTO
    experiment.entry_list (
        entry_list_code, entry_list_name, entry_list_status, experiment_id, creator_id, is_void, entry_list_type
    )
SELECT
    experiment.generate_code('entry_list') AS entry_list_code,
    'KE-IYT-2021-A Entry List' AS entry_list_name,
    'completed' AS entry_list_status,
    (SELECT id FROM experiment.experiment WHERE experiment_name = 'AF2021_001') AS experiment_id,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS creator_id,
    FALSE AS is_void,
    'entry list' AS entry_list_type
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_list
--rollback WHERE
--rollback     entry_list_name = 'KE-IYT-2021-A Entry List';



--changeset postgres:populate_breeding_trial_for_af_in_experiment.entry context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-59 Populate breeding trial for AF in experiment.entry



INSERT INTO
   experiment.entry (
       entry_code, entry_number, entry_name, entry_type, entry_class, entry_role, entry_status,
       entry_list_id, germplasm_id, seed_id, creator_id, is_void
   )
SELECT
    t.entry_number AS entry_code,
    t.entry_number,
    t.designation AS entry_name,
    t.entry_type AS entry_type,
    t.entry_class AS entry_class,
    NULL AS entry_role,
    'active' AS entry_status,
    entlist.id AS entry_list_id,
    ge.id AS germplasm_id,
    seed.id AS seed_id,
    person.id AS creator_id,
    FALSE AS is_void
FROM
    (
        VALUES        
            (1,'(LTL812/ABW52)-B-25-1-1-B','ST-KIB-18B-20-5','check','1'),
            (2,'(LTL812/ABT11)-B-16-1-1-B','ST-KIB-18B-20-36','check','1'),
            (3,'(LTL812/ABT11)-B-16-1-2-B','ST-KIB-18B-20-37','check','2'),
            (4,'(LTL812/ABT11)-B-51-1-2-B','ST-KIB-18B-20-39','check','2'),
            (5,'(HTL437/ABW52)-B-3-1-1-B','ST-KIB-18B-20-50','entry','1'),
            (6,'(HTL437/ABW52)-B-3-1-2-B','ST-KIB-18B-20-51','entry','1'),
            (7,'(HTL437/ABW52)-B-17-1-2-B','ST-KIB-18B-20-53','entry','1'),
            (8,'(HTL437/ABW52)-B-71-1-1-B','ST-KIB-18B-20-58','entry','1'),
            (9,'(HTL437/ABW52)-B-145-1-1-B','ST-KIB-18B-20-64','entry','1'),
            (10,'(HTL437/RK132)-B-9-1-1-B','ST-KIB-18B-20-66','entry','1'),
            (11,'(HTL437/RK132)-B-51-1-2-B','ST-KIB-18B-20-69','entry','1'),
            (12,'(LTL812/ABW52)-B-65-1-1-B','ST-KIB-18B-20-11','entry','1'),
            (13,'(HTL437/RK132)-B-86-1-1-B','ST-KIB-18B-20-70','entry','1'),
            (14,'(HTL437/RK132)-B-89-1-2-B','ST-KIB-18B-20-73','entry','1'),
            (15,'(HTL437/ABHB9)-B-3-1-1-B','ST-KIB-18B-20-80','entry','1'),
            (16,'(HTL437/ABHB9)-B-17-1-2-B','ST-KIB-18B-20-83','entry','1'),
            (17,'(HTL437/ABHB9)-B-31-1-1-B','ST-KIB-18B-20-84','entry','1'),
            (18,'(HTL437/ABHB9)-B-31-1-2-B','ST-KIB-18B-20-85','entry','1'),
            (19,'(HTL437/ABHB9)-B-73-1-2-B','ST-KIB-18B-20-87','entry','1'),
            (20,'(HTL437/ABHB9)-B-108-1-1-B','ST-KIB-18B-20-90','entry','1'),
            (21,'(HTL437/ABHB9)-B-119-1-1-B','ST-KIB-18B-20-92','entry','1'),
            (22,'(HTL437/ABHB9)-B-120-1-1-B','ST-KIB-18B-20-93','entry','1'),
            (23,'(LTL812/ABW52)-B-65-1-2-B','ST-KIB-18B-20-12','entry','1'),
            (24,'(HTL437/ABHB9)-B-168-1-1-B','ST-KIB-18B-20-95','entry','1'),
            (25,'(HTL437/ABHB9)-B-168-1-2-B','ST-KIB-18B-20-96','entry','2'),
            (26,'(HTL437/ABHB9)-B-183-1-2-B','ST-KIB-18B-20-100','entry','2'),
            (27,'(HTL437/ABP38)-B-157-1-1-B','ST-KIB-18B-20-103','entry','2'),
            (28,'(HTL437/ABP38)-B-157-1-2-B','ST-KIB-18B-20-104','entry','2'),
            (29,'(HTL437/ABP38)-B-178-1-1-B','ST-KIB-18B-20-105','entry','2'),
            (30,'(HTL437/ABP38)-B-178-1-2-B','ST-KIB-18B-20-106','entry','2'),
            (31,'(HTL437/ABP38)-B-188-1-2-B','ST-KIB-18B-20-108','entry','2'),
            (32,'(HTL437/ABP38)-B-189-1-2-B','ST-KIB-18B-20-110','entry','2'),
            (33,'(HTL437/ABW52//HTL437)-96-1-1-1-B','ST-KIB-18B-20-111','entry','2'),
            (34,'(LTL812/ABW52)-B-74-1-2-B','ST-KIB-18B-20-14','entry','2'),
            (35,'(HTL437/ABW52//HTL437)-96-1-1-2-B','ST-KIB-18B-20-113','entry','2'),
            (36,'(HTL437/ABW52//HTL437)-98-1-1-1-B','ST-KIB-18B-20-114','entry','2'),
            (37,'(HTL437/ABW52//HTL437)-98-1-1-2-B','ST-KIB-18B-20-115','entry','2'),
            (38,'(LTL812/ABW52)-B-113-1-2-B','ST-KIB-18B-20-16','entry','2'),
            (39,'(LTL812/ABW52)-B-124-1-2-B','ST-KIB-18B-20-18','entry','2'),
            (40,'(LTL812/RK198)-B-53-1-2-B','ST-KIB-18B-20-25','entry','2'),
            (41,'(LTL812/RK198)-B-112-1-2-B','ST-KIB-18B-20-33','entry','2'),
            (42,'(LTL812/ABT11)-B-8-1-2-B','ST-KIB-18B-20-35','entry','2')
    ) AS t (
        entry_number, designation, seed_name, entry_type, entry_class
    )
    INNER JOIN germplasm.germplasm AS ge
        ON t.designation = ge.designation
    INNER JOIN germplasm.seed AS seed
        ON ge.id = seed.germplasm_id
        AND t.seed_name = seed.seed_name
    INNER JOIN experiment.entry_list as entlist
        ON entlist.entry_list_name = 'KE-IYT-2021-A Entry List'
    INNER JOIN tenant.person AS person
        ON person.username = 'nicola.costa'
ORDER BY
    t.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry AS e
--rollback USING
--rollback     experiment.entry_list AS el
--rollback WHERE
--rollback     e.entry_list_id = el.id
--rollback     AND el.entry_list_name = 'KE-IYT-2021-A Entry List'
--rollback ;



--changeset postgres:populate_breeding_trial_for_af_in_experiment.occurrence context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-59 Populate breeding trial fo AF in experiment.occurrence



INSERT INTO
    experiment.occurrence (
        occurrence_code,
        occurrence_name,
        occurrence_status,
        experiment_id,
        site_id,
        field_id,
        rep_count,
        occurrence_number,
        creator_id
    )
VALUES 
    ('KE-NP_MM_DN-IYT-2021-A-1','AF2021_001#OCC-01','planted',(SELECT id FROM experiment.experiment WHERE experiment_name='AF2021_001'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='Dhanusa, Central, Nepal'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='Hardinath Farm'),NULL,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ('KE-IRRIHYD-IYT-2021-A-1','AF2021_001#OCC-02','planted',(SELECT id FROM experiment.experiment WHERE experiment_name='AF2021_001'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='IRRI South Asia Breeding Hub'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='ICRISAT-IRRIHYD'),NULL,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ('KE-IRRIHQ-IYT-2021-A-1','AF2021_001#OCC-03','planted',(SELECT id FROM experiment.experiment WHERE experiment_name='AF2021_001'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='IRRI, Los Baños, Laguna, Philippines'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='IRRI Farms'),NULL,3,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence
--rollback WHERE
--rollback     occurrence_name IN ('AF2021_001#OCC-01','AF2021_001#OCC-02','AF2021_001#OCC-03')
--rollback ;



-- --changeset postgres:populate_rice_breeding_trial_in_place.geospatial_object context:fixture splitStatements:false rollbackSplitStatements:false
-- --comment: DB-59 Populate rice breeding trial in place.geospatial_object



-- INSERT INTO
--     place.geospatial_object (
--         geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, parent_geospatial_object_id, root_geospatial_object_id
--     )
-- VALUES
--     (
--         place.generate_code('geospatial_object'), 
--         'IRSEA-AYT-2021-DS-1_LOC1', 
--         'planting area', 
--         'breeding_location', 
--         '1', 
--         (SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRI_FARMS'), 
--         (SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRIHQ')
--     )
-- ;



-- -- revert changes
-- --rollback DELETE FROM
-- --rollback     place.geospatial_object
-- --rollback WHERE
-- --rollback     geospatial_object_name = 'IRSEA-AYT-2021-DS-1_LOC1'
-- --rollback ;



--changeset postgres:populate_breeding_trial_for_af_in_experiment.location context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-59 Populate breeding trial for AF in experiment.location



INSERT INTO
   experiment.location (
       location_code, location_name, location_status, location_type,
       location_year, season_id, location_number, site_id, field_id,
       steward_id, geospatial_object_id, creator_id
   )
VALUES
    ('LOC-2021-A-7','Nepal-001','planted','planting area',2021,(SELECT id FROM tenant.season WHERE season_code='A'),7,(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='Dhanusa, Central, Nepal'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='Hardinath Farm'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='Dhanusa, Central, Nepal'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ('LOC-2021-A-8','IRRI_SouthAsia_001','planted','planting area',2021,(SELECT id FROM tenant.season WHERE season_code='A'),8,(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='IRRI South Asia Breeding Hub'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='ICRISAT-IRRIHYD'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='IRRI South Asia Breeding Hub'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ('LOC-2021-A-9','IRRI_LosBanos_001','planted','planting area',2021,(SELECT id FROM tenant.season WHERE season_code='A'),9,(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='IRRI, Los Baños, Laguna, Philippines'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='IRRI Farms'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='IRRI, Los Baños, Laguna, Philippines'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location
--rollback WHERE
--rollback     location_name IN ('IRRI_SouthAsia_001','Nepal-001','IRRI_LosBanos_001')
--rollback ;



--changeset postgres:populate_breeding_trial_for_af_in_experiment.location_occurrence_group context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-59 Populate breeding trial for AF in experiment.location_occurrence_group



INSERT INTO 
    experiment.location_occurrence_group (
        location_id,occurrence_id,order_number,creator_id
    )
VALUES
    ((SELECT id FROM experiment.location WHERE location_name='IRRI_SouthAsia_001'),(SELECT id FROM experiment.occurrence WHERE occurrence_name='AF2021_001#OCC-02'),1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
    ,((SELECT id FROM experiment.location WHERE location_name='Nepal-001'),(SELECT id FROM experiment.occurrence WHERE occurrence_name='AF2021_001#OCC-01'),1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
    ,((SELECT id FROM experiment.location WHERE location_name='IRRI_LosBanos_001'),(SELECT id FROM experiment.occurrence WHERE occurrence_name='AF2021_001#OCC-03'),1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
    ;



--rollback DELETE FROM 
--rollback     experiment.location_occurrence_group
--rollback WHERE
--rollback     location_id IN (SELECT id FROM experiment.location WHERE location_name IN ('IRRI_SouthAsia_001','Nepal-001','IRRI_LosBanos_001'))
--rollback AND
--rollback     occurrence_id IN (SELECT id FROM experiment.occurrence WHERE occurrence_name IN ('AF2021_001#OCC-02','AF2021_001#OCC-01','AF2021_001#OCC-03'));



--changeset postgres:populate_breeding_trial_for_af_in_experiment.plot context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-59 Populate breeding trial for AF in experiment.plot



--set 1
INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, pa_x, pa_y, field_x, field_y,
       block_number, plot_status, plot_qc_code, creator_id, harvest_status
   )
SELECT
    occ.id AS occurrence_id,
    loc.id AS location_id,
    ent.id AS entry_id,
    t.plot_code AS plot_code,
    t.plot_number AS plot_number,
    'plotType' AS plot_type,
    t.rep AS rep,
    t.design_x AS design_x,
    t.design_y AS design_y,
    t.pa_x AS pa_x,
    t.pa_y AS pa_y, 
    t.field_x AS field_x,  
    t.field_y AS field_y,
    t.block_number AS block_number,
    'active' AS plot_status,
    t.plot_qc_code AS plot_qc_code,
    person.id AS creator_id,
    'NO_HARVEST' AS harvest_status
FROM
    (
        VALUES  
            ('342',126,3,11,'(HTL437/RK132)-B-51-1-2-B',7,14,7,14,7,14,NULL,3),
            ('341',125,3,18,'(HTL437/ABHB9)-B-31-1-2-B',8,14,8,14,8,14,NULL,3),
            ('340',124,3,38,'(LTL812/ABW52)-B-113-1-2-B',9,14,9,14,9,14,NULL,3),
            ('339',123,3,23,'(LTL812/ABW52)-B-65-1-2-B',9,13,9,13,9,13,NULL,3),
            ('338',122,3,14,'(HTL437/RK132)-B-89-1-2-B',8,13,8,13,8,13,NULL,3),
            ('337',121,3,3,'(LTL812/ABT11)-B-16-1-2-B',7,13,7,13,7,13,NULL,3),
            ('336',120,3,36,'(HTL437/ABW52//HTL437)-98-1-1-1-B',7,12,7,12,7,12,NULL,3),
            ('335',119,3,42,'(LTL812/ABT11)-B-8-1-2-B',8,12,8,12,8,12,NULL,3),
            ('334',118,3,34,'(LTL812/ABW52)-B-74-1-2-B',9,12,9,12,9,12,NULL,3),
            ('333',117,3,2,'(LTL812/ABT11)-B-16-1-1-B',9,11,9,11,9,11,NULL,3),
            ('332',116,3,37,'(HTL437/ABW52//HTL437)-98-1-1-2-B',8,11,8,11,8,11,NULL,3),
            ('331',115,3,35,'(HTL437/ABW52//HTL437)-96-1-1-2-B',7,11,7,11,7,11,NULL,3),
            ('330',114,3,20,'(HTL437/ABHB9)-B-108-1-1-B',7,10,7,10,7,10,NULL,3),
            ('329',113,3,21,'(HTL437/ABHB9)-B-119-1-1-B',8,10,8,10,8,10,NULL,3),
            ('328',112,3,26,'(HTL437/ABHB9)-B-183-1-2-B',9,10,9,10,9,10,NULL,3),
            ('327',111,3,27,'(HTL437/ABP38)-B-157-1-1-B',9,9,9,9,9,9,NULL,3),
            ('326',110,3,32,'(HTL437/ABP38)-B-189-1-2-B',8,9,8,9,8,9,NULL,3),
            ('325',109,3,10,'(HTL437/RK132)-B-9-1-1-B',7,9,7,9,7,9,NULL,3),
            ('324',108,3,28,'(HTL437/ABP38)-B-157-1-2-B',7,8,7,8,7,8,NULL,3),
            ('323',107,3,41,'(LTL812/RK198)-B-112-1-2-B',8,8,8,8,8,8,NULL,3),
            ('322',106,3,25,'(HTL437/ABHB9)-B-168-1-2-B',9,8,9,8,9,8,NULL,3),
            ('321',105,3,39,'(LTL812/ABW52)-B-124-1-2-B',9,7,9,7,9,7,NULL,3),
            ('320',104,3,33,'(HTL437/ABW52//HTL437)-96-1-1-1-B',8,7,8,7,8,7,NULL,3),
            ('319',103,3,17,'(HTL437/ABHB9)-B-31-1-1-B',7,7,7,7,7,7,NULL,3),
            ('318',102,3,8,'(HTL437/ABW52)-B-71-1-1-B',7,6,7,6,7,6,NULL,3),
            ('317',101,3,7,'(HTL437/ABW52)-B-17-1-2-B',8,6,8,6,8,6,NULL,3),
            ('316',100,3,4,'(LTL812/ABT11)-B-51-1-2-B',9,6,9,6,9,6,NULL,3),
            ('315',99,3,24,'(HTL437/ABHB9)-B-168-1-1-B',9,5,9,5,9,5,NULL,3),
            ('314',98,3,31,'(HTL437/ABP38)-B-188-1-2-B',8,5,8,5,8,5,NULL,3),
            ('313',97,3,40,'(LTL812/RK198)-B-53-1-2-B',7,5,7,5,7,5,NULL,3),
            ('312',96,3,1,'(LTL812/ABW52)-B-25-1-1-B',7,4,7,4,7,4,NULL,3),
            ('311',95,3,6,'(HTL437/ABW52)-B-3-1-2-B',8,4,8,4,8,4,NULL,3),
            ('310',94,3,12,'(LTL812/ABW52)-B-65-1-1-B',9,4,9,4,9,4,NULL,3),
            ('309',93,3,29,'(HTL437/ABP38)-B-178-1-1-B',9,3,9,3,9,3,NULL,3),
            ('308',92,3,22,'(HTL437/ABHB9)-B-120-1-1-B',8,3,8,3,8,3,NULL,3),
            ('307',91,3,16,'(HTL437/ABHB9)-B-17-1-2-B',7,3,7,3,7,3,NULL,3),
            ('306',90,3,9,'(HTL437/ABW52)-B-145-1-1-B',7,2,7,2,7,2,NULL,3),
            ('305',89,3,19,'(HTL437/ABHB9)-B-73-1-2-B',8,2,8,2,8,2,NULL,3),
            ('304',88,3,30,'(HTL437/ABP38)-B-178-1-2-B',9,2,9,2,9,2,NULL,3),
            ('303',87,3,15,'(HTL437/ABHB9)-B-3-1-1-B',9,1,9,1,9,1,NULL,3),
            ('302',86,3,13,'(HTL437/RK132)-B-86-1-1-B',8,1,8,1,8,1,NULL,3),
            ('301',85,3,5,'(HTL437/ABW52)-B-3-1-1-B',7,1,7,1,7,1,NULL,3),
            ('242',84,2,20,'(HTL437/ABHB9)-B-108-1-1-B',4,14,4,14,4,14,NULL,2),
            ('241',83,2,18,'(HTL437/ABHB9)-B-31-1-2-B',5,14,5,14,5,14,NULL,2),
            ('240',82,2,29,'(HTL437/ABP38)-B-178-1-1-B',6,14,6,14,6,14,NULL,2),
            ('239',81,2,38,'(LTL812/ABW52)-B-113-1-2-B',6,13,6,13,6,13,NULL,2),
            ('238',80,2,21,'(HTL437/ABHB9)-B-119-1-1-B',5,13,5,13,5,13,NULL,2),
            ('237',79,2,24,'(HTL437/ABHB9)-B-168-1-1-B',4,13,4,13,4,13,NULL,2),
            ('236',78,2,11,'(HTL437/RK132)-B-51-1-2-B',4,12,4,12,4,12,NULL,2),
            ('235',77,2,39,'(LTL812/ABW52)-B-124-1-2-B',5,12,5,12,5,12,NULL,2),
            ('234',76,2,42,'(LTL812/ABT11)-B-8-1-2-B',6,12,6,12,6,12,NULL,2),
            ('233',75,2,9,'(HTL437/ABW52)-B-145-1-1-B',6,11,6,11,6,11,NULL,2),
            ('232',74,2,27,'(HTL437/ABP38)-B-157-1-1-B',5,11,5,11,5,11,NULL,2),
            ('231',73,2,1,'(LTL812/ABW52)-B-25-1-1-B',4,11,4,11,4,11,NULL,2),
            ('230',72,2,37,'(HTL437/ABW52//HTL437)-98-1-1-2-B',4,10,4,10,4,10,NULL,2),
            ('229',71,2,23,'(LTL812/ABW52)-B-65-1-2-B',5,10,5,10,5,10,NULL,2),
            ('228',70,2,30,'(HTL437/ABP38)-B-178-1-2-B',6,10,6,10,6,10,NULL,2),
            ('227',69,2,16,'(HTL437/ABHB9)-B-17-1-2-B',6,9,6,9,6,9,NULL,2),
            ('226',68,2,41,'(LTL812/RK198)-B-112-1-2-B',5,9,5,9,5,9,NULL,2),
            ('225',67,2,25,'(HTL437/ABHB9)-B-168-1-2-B',4,9,4,9,4,9,NULL,2),
            ('224',66,2,22,'(HTL437/ABHB9)-B-120-1-1-B',4,8,4,8,4,8,NULL,2),
            ('223',65,2,17,'(HTL437/ABHB9)-B-31-1-1-B',5,8,5,8,5,8,NULL,2),
            ('222',64,2,8,'(HTL437/ABW52)-B-71-1-1-B',6,8,6,8,6,8,NULL,2),
            ('221',63,2,32,'(HTL437/ABP38)-B-189-1-2-B',6,7,6,7,6,7,NULL,2),
            ('220',62,2,35,'(HTL437/ABW52//HTL437)-96-1-1-2-B',5,7,5,7,5,7,NULL,2),
            ('219',61,2,6,'(HTL437/ABW52)-B-3-1-2-B',4,7,4,7,4,7,NULL,2),
            ('218',60,2,31,'(HTL437/ABP38)-B-188-1-2-B',4,6,4,6,4,6,NULL,2),
            ('217',59,2,3,'(LTL812/ABT11)-B-16-1-2-B',5,6,5,6,5,6,NULL,2),
            ('216',58,2,7,'(HTL437/ABW52)-B-17-1-2-B',6,6,6,6,6,6,NULL,2),
            ('215',57,2,28,'(HTL437/ABP38)-B-157-1-2-B',6,5,6,5,6,5,NULL,2),
            ('214',56,2,4,'(LTL812/ABT11)-B-51-1-2-B',5,5,5,5,5,5,NULL,2),
            ('213',55,2,34,'(LTL812/ABW52)-B-74-1-2-B',4,5,4,5,4,5,NULL,2),
            ('212',54,2,13,'(HTL437/RK132)-B-86-1-1-B',4,4,4,4,4,4,NULL,2),
            ('211',53,2,33,'(HTL437/ABW52//HTL437)-96-1-1-1-B',5,4,5,4,5,4,NULL,2),
            ('210',52,2,2,'(LTL812/ABT11)-B-16-1-1-B',6,4,6,4,6,4,NULL,2),
            ('209',51,2,36,'(HTL437/ABW52//HTL437)-98-1-1-1-B',6,3,6,3,6,3,NULL,2),
            ('208',50,2,26,'(HTL437/ABHB9)-B-183-1-2-B',5,3,5,3,5,3,NULL,2),
            ('207',49,2,15,'(HTL437/ABHB9)-B-3-1-1-B',4,3,4,3,4,3,NULL,2),
            ('206',48,2,12,'(LTL812/ABW52)-B-65-1-1-B',4,2,4,2,4,2,NULL,2),
            ('205',47,2,40,'(LTL812/RK198)-B-53-1-2-B',5,2,5,2,5,2,NULL,2),
            ('204',46,2,19,'(HTL437/ABHB9)-B-73-1-2-B',6,2,6,2,6,2,NULL,2),
            ('203',45,2,10,'(HTL437/RK132)-B-9-1-1-B',6,1,6,1,6,1,NULL,2),
            ('202',44,2,5,'(HTL437/ABW52)-B-3-1-1-B',5,1,5,1,5,1,NULL,2),
            ('201',43,2,14,'(HTL437/RK132)-B-89-1-2-B',4,1,4,1,4,1,NULL,2),
            ('142',42,1,18,'(HTL437/ABHB9)-B-31-1-2-B',1,14,1,14,1,14,NULL,1),
            ('141',41,1,31,'(HTL437/ABP38)-B-188-1-2-B',2,14,2,14,2,14,NULL,1),
            ('140',40,1,24,'(HTL437/ABHB9)-B-168-1-1-B',3,14,3,14,3,14,NULL,1),
            ('139',39,1,35,'(HTL437/ABW52//HTL437)-96-1-1-2-B',3,13,3,13,3,13,NULL,1),
            ('138',38,1,41,'(LTL812/RK198)-B-112-1-2-B',2,13,2,13,2,13,NULL,1),
            ('137',37,1,40,'(LTL812/RK198)-B-53-1-2-B',1,13,1,13,1,13,NULL,1),
            ('136',36,1,7,'(HTL437/ABW52)-B-17-1-2-B',1,12,1,12,1,12,NULL,1),
            ('135',35,1,3,'(LTL812/ABT11)-B-16-1-2-B',2,12,2,12,2,12,NULL,1),
            ('134',34,1,28,'(HTL437/ABP38)-B-157-1-2-B',3,12,3,12,3,12,NULL,1),
            ('133',33,1,34,'(LTL812/ABW52)-B-74-1-2-B',3,11,3,11,3,11,NULL,1),
            ('132',32,1,19,'(HTL437/ABHB9)-B-73-1-2-B',2,11,2,11,2,11,NULL,1),
            ('131',31,1,22,'(HTL437/ABHB9)-B-120-1-1-B',1,11,1,11,1,11,NULL,1),
            ('130',30,1,26,'(HTL437/ABHB9)-B-183-1-2-B',1,10,1,10,1,10,NULL,1),
            ('129',29,1,30,'(HTL437/ABP38)-B-178-1-2-B',2,10,2,10,2,10,NULL,1),
            ('128',28,1,23,'(LTL812/ABW52)-B-65-1-2-B',3,10,3,10,3,10,NULL,1),
            ('127',27,1,25,'(HTL437/ABHB9)-B-168-1-2-B',3,9,3,9,3,9,NULL,1),
            ('126',26,1,20,'(HTL437/ABHB9)-B-108-1-1-B',2,9,2,9,2,9,NULL,1),
            ('125',25,1,6,'(HTL437/ABW52)-B-3-1-2-B',1,9,1,9,1,9,NULL,1),
            ('124',24,1,2,'(LTL812/ABT11)-B-16-1-1-B',1,8,1,8,1,8,NULL,1),
            ('123',23,1,12,'(LTL812/ABW52)-B-65-1-1-B',2,8,2,8,2,8,NULL,1),
            ('122',22,1,21,'(HTL437/ABHB9)-B-119-1-1-B',3,8,3,8,3,8,NULL,1),
            ('121',21,1,8,'(HTL437/ABW52)-B-71-1-1-B',3,7,3,7,3,7,NULL,1),
            ('120',20,1,5,'(HTL437/ABW52)-B-3-1-1-B',2,7,2,7,2,7,NULL,1),
            ('119',19,1,10,'(HTL437/RK132)-B-9-1-1-B',1,7,1,7,1,7,NULL,1),
            ('118',18,1,9,'(HTL437/ABW52)-B-145-1-1-B',1,6,1,6,1,6,NULL,1),
            ('117',17,1,37,'(HTL437/ABW52//HTL437)-98-1-1-2-B',2,6,2,6,2,6,NULL,1),
            ('116',16,1,11,'(HTL437/RK132)-B-51-1-2-B',3,6,3,6,3,6,NULL,1),
            ('115',15,1,29,'(HTL437/ABP38)-B-178-1-1-B',3,5,3,5,3,5,NULL,1),
            ('114',14,1,38,'(LTL812/ABW52)-B-113-1-2-B',2,5,2,5,2,5,NULL,1),
            ('113',13,1,4,'(LTL812/ABT11)-B-51-1-2-B',1,5,1,5,1,5,NULL,1),
            ('112',12,1,17,'(HTL437/ABHB9)-B-31-1-1-B',1,4,1,4,1,4,NULL,1),
            ('111',11,1,32,'(HTL437/ABP38)-B-189-1-2-B',2,4,2,4,2,4,NULL,1),
            ('110',10,1,16,'(HTL437/ABHB9)-B-17-1-2-B',3,4,3,4,3,4,NULL,1),
            ('109',9,1,39,'(LTL812/ABW52)-B-124-1-2-B',3,3,3,3,3,3,NULL,1),
            ('108',8,1,14,'(HTL437/RK132)-B-89-1-2-B',2,3,2,3,2,3,NULL,1),
            ('107',7,1,42,'(LTL812/ABT11)-B-8-1-2-B',1,3,1,3,1,3,NULL,1),
            ('106',6,1,36,'(HTL437/ABW52//HTL437)-98-1-1-1-B',1,2,1,2,1,2,NULL,1),
            ('105',5,1,27,'(HTL437/ABP38)-B-157-1-1-B',2,2,2,2,2,2,NULL,1),
            ('104',4,1,15,'(HTL437/ABHB9)-B-3-1-1-B',3,2,3,2,3,2,NULL,1),
            ('103',3,1,1,'(LTL812/ABW52)-B-25-1-1-B',3,1,3,1,3,1,NULL,1),
            ('102',2,1,33,'(HTL437/ABW52//HTL437)-96-1-1-1-B',2,1,2,1,2,1,NULL,1),
            ('101',1,1,13,'(HTL437/RK132)-B-86-1-1-B',1,1,1,1,1,1,NULL,1)
    ) AS t (
        plot_code, plot_number, rep, entry_number, designation, design_x, design_y, pa_x, pa_y, field_x, field_y, plot_qc_code, block_number
    )
    INNER JOIN experiment.entry AS ent
        ON t.entry_number = ent.entry_number
        AND t.designation = ent.entry_name
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
        AND entlist.entry_list_name = 'KE-IYT-2021-A Entry List'
    INNER JOIN experiment.occurrence AS occ
        ON occ.occurrence_name = 'AF2021_001#OCC-02'
    INNER JOIN experiment.location AS loc
        ON loc.location_name = 'IRRI_SouthAsia_001'
    INNER JOIN tenant.person AS person
        ON person.username = 'nicola.costa'
ORDER BY
    t.plot_number
;

--set 2
INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, pa_x, pa_y, field_x, field_y,
       block_number, plot_status, plot_qc_code, creator_id, harvest_status
   )
SELECT
    occ.id AS occurrence_id,
    loc.id AS location_id,
    ent.id AS entry_id,
    t.plot_code AS plot_code,
    t.plot_number AS plot_number,
    'plotType' AS plot_type,
    t.rep AS rep,
    t.design_x AS design_x,
    t.design_y AS design_y,
    t.pa_x AS pa_x,
    t.pa_y AS pa_y, 
    t.field_x AS field_x,  
    t.field_y AS field_y,
    t.block_number AS block_number,
    'active' AS plot_status,
    t.plot_qc_code AS plot_qc_code,
    person.id AS creator_id,
    'NO_HARVEST' AS harvest_status
FROM
    (
        VALUES   
            ('342',126,3,32,'(HTL437/ABP38)-B-189-1-2-B',7,14,7,14,7,14,NULL,3),
            ('341',125,3,21,'(HTL437/ABHB9)-B-119-1-1-B',8,14,8,14,8,14,NULL,3),
            ('340',124,3,11,'(HTL437/RK132)-B-51-1-2-B',9,14,9,14,9,14,NULL,3),
            ('339',123,3,19,'(HTL437/ABHB9)-B-73-1-2-B',9,13,9,13,9,13,NULL,3),
            ('338',122,3,4,'(LTL812/ABT11)-B-51-1-2-B',8,13,8,13,8,13,NULL,3),
            ('337',121,3,8,'(HTL437/ABW52)-B-71-1-1-B',7,13,7,13,7,13,NULL,3),
            ('336',120,3,24,'(HTL437/ABHB9)-B-168-1-1-B',7,12,7,12,7,12,NULL,3),
            ('335',119,3,2,'(LTL812/ABT11)-B-16-1-1-B',8,12,8,12,8,12,NULL,3),
            ('334',118,3,1,'(LTL812/ABW52)-B-25-1-1-B',9,12,9,12,9,12,NULL,3),
            ('333',117,3,40,'(LTL812/RK198)-B-53-1-2-B',9,11,9,11,9,11,NULL,3),
            ('332',116,3,37,'(HTL437/ABW52//HTL437)-98-1-1-2-B',8,11,8,11,8,11,NULL,3),
            ('331',115,3,13,'(HTL437/RK132)-B-86-1-1-B',7,11,7,11,7,11,NULL,3),
            ('330',114,3,29,'(HTL437/ABP38)-B-178-1-1-B',7,10,7,10,7,10,NULL,3),
            ('329',113,3,30,'(HTL437/ABP38)-B-178-1-2-B',8,10,8,10,8,10,NULL,3),
            ('328',112,3,41,'(LTL812/RK198)-B-112-1-2-B',9,10,9,10,9,10,NULL,3),
            ('327',111,3,5,'(HTL437/ABW52)-B-3-1-1-B',9,9,9,9,9,9,NULL,3),
            ('326',110,3,34,'(LTL812/ABW52)-B-74-1-2-B',8,9,8,9,8,9,NULL,3),
            ('325',109,3,28,'(HTL437/ABP38)-B-157-1-2-B',7,9,7,9,7,9,NULL,3),
            ('324',108,3,39,'(LTL812/ABW52)-B-124-1-2-B',7,8,7,8,7,8,NULL,3),
            ('323',107,3,20,'(HTL437/ABHB9)-B-108-1-1-B',8,8,8,8,8,8,NULL,3),
            ('322',106,3,10,'(HTL437/RK132)-B-9-1-1-B',9,8,9,8,9,8,NULL,3),
            ('321',105,3,9,'(HTL437/ABW52)-B-145-1-1-B',9,7,9,7,9,7,NULL,3),
            ('320',104,3,3,'(LTL812/ABT11)-B-16-1-2-B',8,7,8,7,8,7,NULL,3),
            ('319',103,3,35,'(HTL437/ABW52//HTL437)-96-1-1-2-B',7,7,7,7,7,7,NULL,3),
            ('318',102,3,15,'(HTL437/ABHB9)-B-3-1-1-B',7,6,7,6,7,6,NULL,3),
            ('317',101,3,18,'(HTL437/ABHB9)-B-31-1-2-B',8,6,8,6,8,6,NULL,3),
            ('316',100,3,31,'(HTL437/ABP38)-B-188-1-2-B',9,6,9,6,9,6,NULL,3),
            ('315',99,3,42,'(LTL812/ABT11)-B-8-1-2-B',9,5,9,5,9,5,NULL,3),
            ('314',98,3,22,'(HTL437/ABHB9)-B-120-1-1-B',8,5,8,5,8,5,NULL,3),
            ('313',97,3,33,'(HTL437/ABW52//HTL437)-96-1-1-1-B',7,5,7,5,7,5,NULL,3),
            ('312',96,3,6,'(HTL437/ABW52)-B-3-1-2-B',7,4,7,4,7,4,NULL,3),
            ('311',95,3,16,'(HTL437/ABHB9)-B-17-1-2-B',8,4,8,4,8,4,NULL,3),
            ('310',94,3,23,'(LTL812/ABW52)-B-65-1-2-B',9,4,9,4,9,4,NULL,3),
            ('309',93,3,25,'(HTL437/ABHB9)-B-168-1-2-B',9,3,9,3,9,3,NULL,3),
            ('308',92,3,17,'(HTL437/ABHB9)-B-31-1-1-B',8,3,8,3,8,3,NULL,3),
            ('307',91,3,36,'(HTL437/ABW52//HTL437)-98-1-1-1-B',7,3,7,3,7,3,NULL,3),
            ('306',90,3,26,'(HTL437/ABHB9)-B-183-1-2-B',7,2,7,2,7,2,NULL,3),
            ('305',89,3,27,'(HTL437/ABP38)-B-157-1-1-B',8,2,8,2,8,2,NULL,3),
            ('304',88,3,7,'(HTL437/ABW52)-B-17-1-2-B',9,2,9,2,9,2,NULL,3),
            ('303',87,3,14,'(HTL437/RK132)-B-89-1-2-B',9,1,9,1,9,1,NULL,3),
            ('302',86,3,12,'(LTL812/ABW52)-B-65-1-1-B',8,1,8,1,8,1,NULL,3),
            ('301',85,3,38,'(LTL812/ABW52)-B-113-1-2-B',7,1,7,1,7,1,NULL,3),
            ('242',84,2,38,'(LTL812/ABW52)-B-113-1-2-B',4,14,4,14,4,14,NULL,2),
            ('241',83,2,20,'(HTL437/ABHB9)-B-108-1-1-B',5,14,5,14,5,14,NULL,2),
            ('240',82,2,25,'(HTL437/ABHB9)-B-168-1-2-B',6,14,6,14,6,14,NULL,2),
            ('239',81,2,37,'(HTL437/ABW52//HTL437)-98-1-1-2-B',6,13,6,13,6,13,NULL,2),
            ('238',80,2,17,'(HTL437/ABHB9)-B-31-1-1-B',5,13,5,13,5,13,NULL,2),
            ('237',79,2,39,'(LTL812/ABW52)-B-124-1-2-B',4,13,4,13,4,13,NULL,2),
            ('236',78,2,32,'(HTL437/ABP38)-B-189-1-2-B',4,12,4,12,4,12,NULL,2),
            ('235',77,2,27,'(HTL437/ABP38)-B-157-1-1-B',5,12,5,12,5,12,NULL,2),
            ('234',76,2,18,'(HTL437/ABHB9)-B-31-1-2-B',6,12,6,12,6,12,NULL,2),
            ('233',75,2,13,'(HTL437/RK132)-B-86-1-1-B',6,11,6,11,6,11,NULL,2),
            ('232',74,2,14,'(HTL437/RK132)-B-89-1-2-B',5,11,5,11,5,11,NULL,2),
            ('231',73,2,7,'(HTL437/ABW52)-B-17-1-2-B',4,11,4,11,4,11,NULL,2),
            ('230',72,2,30,'(HTL437/ABP38)-B-178-1-2-B',4,10,4,10,4,10,NULL,2),
            ('229',71,2,12,'(LTL812/ABW52)-B-65-1-1-B',5,10,5,10,5,10,NULL,2),
            ('228',70,2,11,'(HTL437/RK132)-B-51-1-2-B',6,10,6,10,6,10,NULL,2),
            ('227',69,2,10,'(HTL437/RK132)-B-9-1-1-B',6,9,6,9,6,9,NULL,2),
            ('226',68,2,1,'(LTL812/ABW52)-B-25-1-1-B',5,9,5,9,5,9,NULL,2),
            ('225',67,2,41,'(LTL812/RK198)-B-112-1-2-B',4,9,4,9,4,9,NULL,2),
            ('224',66,2,42,'(LTL812/ABT11)-B-8-1-2-B',4,8,4,8,4,8,NULL,2),
            ('223',65,2,3,'(LTL812/ABT11)-B-16-1-2-B',5,8,5,8,5,8,NULL,2),
            ('222',64,2,34,'(LTL812/ABW52)-B-74-1-2-B',6,8,6,8,6,8,NULL,2),
            ('221',63,2,2,'(LTL812/ABT11)-B-16-1-1-B',6,7,6,7,6,7,NULL,2),
            ('220',62,2,15,'(HTL437/ABHB9)-B-3-1-1-B',5,7,5,7,5,7,NULL,2),
            ('219',61,2,24,'(HTL437/ABHB9)-B-168-1-1-B',4,7,4,7,4,7,NULL,2),
            ('218',60,2,35,'(HTL437/ABW52//HTL437)-96-1-1-2-B',4,6,4,6,4,6,NULL,2),
            ('217',59,2,19,'(HTL437/ABHB9)-B-73-1-2-B',5,6,5,6,5,6,NULL,2),
            ('216',58,2,9,'(HTL437/ABW52)-B-145-1-1-B',6,6,6,6,6,6,NULL,2),
            ('215',57,2,5,'(HTL437/ABW52)-B-3-1-1-B',6,5,6,5,6,5,NULL,2),
            ('214',56,2,22,'(HTL437/ABHB9)-B-120-1-1-B',5,5,5,5,5,5,NULL,2),
            ('213',55,2,8,'(HTL437/ABW52)-B-71-1-1-B',4,5,4,5,4,5,NULL,2),
            ('212',54,2,40,'(LTL812/RK198)-B-53-1-2-B',4,4,4,4,4,4,NULL,2),
            ('211',53,2,28,'(HTL437/ABP38)-B-157-1-2-B',5,4,5,4,5,4,NULL,2),
            ('210',52,2,29,'(HTL437/ABP38)-B-178-1-1-B',6,4,6,4,6,4,NULL,2),
            ('209',51,2,6,'(HTL437/ABW52)-B-3-1-2-B',6,3,6,3,6,3,NULL,2),
            ('208',50,2,36,'(HTL437/ABW52//HTL437)-98-1-1-1-B',5,3,5,3,5,3,NULL,2),
            ('207',49,2,21,'(HTL437/ABHB9)-B-119-1-1-B',4,3,4,3,4,3,NULL,2),
            ('206',48,2,23,'(LTL812/ABW52)-B-65-1-2-B',4,2,4,2,4,2,NULL,2),
            ('205',47,2,4,'(LTL812/ABT11)-B-51-1-2-B',5,2,5,2,5,2,NULL,2),
            ('204',46,2,33,'(HTL437/ABW52//HTL437)-96-1-1-1-B',6,2,6,2,6,2,NULL,2),
            ('203',45,2,26,'(HTL437/ABHB9)-B-183-1-2-B',6,1,6,1,6,1,NULL,2),
            ('202',44,2,31,'(HTL437/ABP38)-B-188-1-2-B',5,1,5,1,5,1,NULL,2),
            ('201',43,2,16,'(HTL437/ABHB9)-B-17-1-2-B',4,1,4,1,4,1,NULL,2),
            ('142',42,1,31,'(HTL437/ABP38)-B-188-1-2-B',1,14,1,14,1,14,NULL,1),
            ('141',41,1,36,'(HTL437/ABW52//HTL437)-98-1-1-1-B',2,14,2,14,2,14,NULL,1),
            ('140',40,1,8,'(HTL437/ABW52)-B-71-1-1-B',3,14,3,14,3,14,NULL,1),
            ('139',39,1,38,'(LTL812/ABW52)-B-113-1-2-B',3,13,3,13,3,13,NULL,1),
            ('138',38,1,28,'(HTL437/ABP38)-B-157-1-2-B',2,13,2,13,2,13,NULL,1),
            ('137',37,1,29,'(HTL437/ABP38)-B-178-1-1-B',1,13,1,13,1,13,NULL,1),
            ('136',36,1,10,'(HTL437/RK132)-B-9-1-1-B',1,12,1,12,1,12,NULL,1),
            ('135',35,1,6,'(HTL437/ABW52)-B-3-1-2-B',2,12,2,12,2,12,NULL,1),
            ('134',34,1,34,'(LTL812/ABW52)-B-74-1-2-B',3,12,3,12,3,12,NULL,1),
            ('133',33,1,2,'(LTL812/ABT11)-B-16-1-1-B',3,11,3,11,3,11,NULL,1),
            ('132',32,1,32,'(HTL437/ABP38)-B-189-1-2-B',2,11,2,11,2,11,NULL,1),
            ('131',31,1,30,'(HTL437/ABP38)-B-178-1-2-B',1,11,1,11,1,11,NULL,1),
            ('130',30,1,1,'(LTL812/ABW52)-B-25-1-1-B',1,10,1,10,1,10,NULL,1),
            ('129',29,1,11,'(HTL437/RK132)-B-51-1-2-B',2,10,2,10,2,10,NULL,1),
            ('128',28,1,24,'(HTL437/ABHB9)-B-168-1-1-B',3,10,3,10,3,10,NULL,1),
            ('127',27,1,7,'(HTL437/ABW52)-B-17-1-2-B',3,9,3,9,3,9,NULL,1),
            ('126',26,1,16,'(HTL437/ABHB9)-B-17-1-2-B',2,9,2,9,2,9,NULL,1),
            ('125',25,1,35,'(HTL437/ABW52//HTL437)-96-1-1-2-B',1,9,1,9,1,9,NULL,1),
            ('124',24,1,42,'(LTL812/ABT11)-B-8-1-2-B',1,8,1,8,1,8,NULL,1),
            ('123',23,1,39,'(LTL812/ABW52)-B-124-1-2-B',2,8,2,8,2,8,NULL,1),
            ('122',22,1,23,'(LTL812/ABW52)-B-65-1-2-B',3,8,3,8,3,8,NULL,1),
            ('121',21,1,33,'(HTL437/ABW52//HTL437)-96-1-1-1-B',3,7,3,7,3,7,NULL,1),
            ('120',20,1,20,'(HTL437/ABHB9)-B-108-1-1-B',2,7,2,7,2,7,NULL,1),
            ('119',19,1,19,'(HTL437/ABHB9)-B-73-1-2-B',1,7,1,7,1,7,NULL,1),
            ('118',18,1,12,'(LTL812/ABW52)-B-65-1-1-B',1,6,1,6,1,6,NULL,1),
            ('117',17,1,40,'(LTL812/RK198)-B-53-1-2-B',2,6,2,6,2,6,NULL,1),
            ('116',16,1,26,'(HTL437/ABHB9)-B-183-1-2-B',3,6,3,6,3,6,NULL,1),
            ('115',15,1,22,'(HTL437/ABHB9)-B-120-1-1-B',3,5,3,5,3,5,NULL,1),
            ('114',14,1,41,'(LTL812/RK198)-B-112-1-2-B',2,5,2,5,2,5,NULL,1),
            ('113',13,1,17,'(HTL437/ABHB9)-B-31-1-1-B',1,5,1,5,1,5,NULL,1),
            ('112',12,1,37,'(HTL437/ABW52//HTL437)-98-1-1-2-B',1,4,1,4,1,4,NULL,1),
            ('111',11,1,15,'(HTL437/ABHB9)-B-3-1-1-B',2,4,2,4,2,4,NULL,1),
            ('110',10,1,3,'(LTL812/ABT11)-B-16-1-2-B',3,4,3,4,3,4,NULL,1),
            ('109',9,1,13,'(HTL437/RK132)-B-86-1-1-B',3,3,3,3,3,3,NULL,1),
            ('108',8,1,27,'(HTL437/ABP38)-B-157-1-1-B',2,3,2,3,2,3,NULL,1),
            ('107',7,1,18,'(HTL437/ABHB9)-B-31-1-2-B',1,3,1,3,1,3,NULL,1),
            ('106',6,1,5,'(HTL437/ABW52)-B-3-1-1-B',1,2,1,2,1,2,NULL,1),
            ('105',5,1,21,'(HTL437/ABHB9)-B-119-1-1-B',2,2,2,2,2,2,NULL,1),
            ('104',4,1,9,'(HTL437/ABW52)-B-145-1-1-B',3,2,3,2,3,2,NULL,1),
            ('103',3,1,14,'(HTL437/RK132)-B-89-1-2-B',3,1,3,1,3,1,NULL,1),
            ('102',2,1,25,'(HTL437/ABHB9)-B-168-1-2-B',2,1,2,1,2,1,NULL,1),
            ('101',1,1,4,'(LTL812/ABT11)-B-51-1-2-B',1,1,1,1,1,1,NULL,1)
    ) AS t (
        plot_code, plot_number, rep, entry_number, designation, design_x, design_y, pa_x, pa_y, field_x, field_y, plot_qc_code, block_number
    )
    INNER JOIN experiment.entry AS ent
        ON t.entry_number = ent.entry_number
        AND t.designation = ent.entry_name
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
        AND entlist.entry_list_name = 'KE-IYT-2021-A Entry List'
    INNER JOIN experiment.occurrence AS occ
        ON occ.occurrence_name = 'AF2021_001#OCC-01'
    INNER JOIN experiment.location AS loc
        ON loc.location_name = 'Nepal-001'
    INNER JOIN tenant.person AS person
        ON person.username = 'nicola.costa'
ORDER BY
    t.plot_number
;

--set 3
INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, pa_x, pa_y, field_x, field_y,
       block_number, plot_status, plot_qc_code, creator_id, harvest_status
   )
SELECT
    occ.id AS occurrence_id,
    loc.id AS location_id,
    ent.id AS entry_id,
    t.plot_code AS plot_code,
    t.plot_number AS plot_number,
    'plotType' AS plot_type,
    t.rep AS rep,
    t.design_x AS design_x,
    t.design_y AS design_y,
    t.pa_x AS pa_x,
    t.pa_y AS pa_y, 
    t.field_x AS field_x,  
    t.field_y AS field_y,
    t.block_number AS block_number,
    'active' AS plot_status,
    t.plot_qc_code AS plot_qc_code,
    person.id AS creator_id,
    'NO_HARVEST' AS harvest_status
FROM
    (
        VALUES     
            ('342',126,3,34,'(LTL812/ABW52)-B-74-1-2-B',7,14,7,14,7,14,NULL,3),
            ('341',125,3,40,'(LTL812/RK198)-B-53-1-2-B',8,14,8,14,8,14,NULL,3),
            ('340',124,3,41,'(LTL812/RK198)-B-112-1-2-B',9,14,9,14,9,14,NULL,3),
            ('339',123,3,19,'(HTL437/ABHB9)-B-73-1-2-B',9,13,9,13,9,13,NULL,3),
            ('338',122,3,33,'(HTL437/ABW52//HTL437)-96-1-1-1-B',8,13,8,13,8,13,NULL,3),
            ('337',121,3,12,'(LTL812/ABW52)-B-65-1-1-B',7,13,7,13,7,13,NULL,3),
            ('336',120,3,17,'(HTL437/ABHB9)-B-31-1-1-B',7,12,7,12,7,12,NULL,3),
            ('335',119,3,16,'(HTL437/ABHB9)-B-17-1-2-B',8,12,8,12,8,12,NULL,3),
            ('334',118,3,24,'(HTL437/ABHB9)-B-168-1-1-B',9,12,9,12,9,12,NULL,3),
            ('333',117,3,15,'(HTL437/ABHB9)-B-3-1-1-B',9,11,9,11,9,11,NULL,3),
            ('332',116,3,42,'(LTL812/ABT11)-B-8-1-2-B',8,11,8,11,8,11,NULL,3),
            ('331',115,3,23,'(LTL812/ABW52)-B-65-1-2-B',7,11,7,11,7,11,NULL,3),
            ('330',114,3,7,'(HTL437/ABW52)-B-17-1-2-B',7,10,7,10,7,10,NULL,3),
            ('329',113,3,38,'(LTL812/ABW52)-B-113-1-2-B',8,10,8,10,8,10,NULL,3),
            ('328',112,3,1,'(LTL812/ABW52)-B-25-1-1-B',9,10,9,10,9,10,NULL,3),
            ('327',111,3,9,'(HTL437/ABW52)-B-145-1-1-B',9,9,9,9,9,9,NULL,3),
            ('326',110,3,35,'(HTL437/ABW52//HTL437)-96-1-1-2-B',8,9,8,9,8,9,NULL,3),
            ('325',109,3,30,'(HTL437/ABP38)-B-178-1-2-B',7,9,7,9,7,9,NULL,3),
            ('324',108,3,36,'(HTL437/ABW52//HTL437)-98-1-1-1-B',7,8,7,8,7,8,NULL,3),
            ('323',107,3,37,'(HTL437/ABW52//HTL437)-98-1-1-2-B',8,8,8,8,8,8,NULL,3),
            ('322',106,3,32,'(HTL437/ABP38)-B-189-1-2-B',9,8,9,8,9,8,NULL,3),
            ('321',105,3,8,'(HTL437/ABW52)-B-71-1-1-B',9,7,9,7,9,7,NULL,3),
            ('320',104,3,3,'(LTL812/ABT11)-B-16-1-2-B',8,7,8,7,8,7,NULL,3),
            ('319',103,3,18,'(HTL437/ABHB9)-B-31-1-2-B',7,7,7,7,7,7,NULL,3),
            ('318',102,3,11,'(HTL437/RK132)-B-51-1-2-B',7,6,7,6,7,6,NULL,3),
            ('317',101,3,25,'(HTL437/ABHB9)-B-168-1-2-B',8,6,8,6,8,6,NULL,3),
            ('316',100,3,13,'(HTL437/RK132)-B-86-1-1-B',9,6,9,6,9,6,NULL,3),
            ('315',99,3,39,'(LTL812/ABW52)-B-124-1-2-B',9,5,9,5,9,5,NULL,3),
            ('314',98,3,10,'(HTL437/RK132)-B-9-1-1-B',8,5,8,5,8,5,NULL,3),
            ('313',97,3,26,'(HTL437/ABHB9)-B-183-1-2-B',7,5,7,5,7,5,NULL,3),
            ('312',96,3,5,'(HTL437/ABW52)-B-3-1-1-B',7,4,7,4,7,4,NULL,3),
            ('311',95,3,4,'(LTL812/ABT11)-B-51-1-2-B',8,4,8,4,8,4,NULL,3),
            ('310',94,3,29,'(HTL437/ABP38)-B-178-1-1-B',9,4,9,4,9,4,NULL,3),
            ('309',93,3,28,'(HTL437/ABP38)-B-157-1-2-B',9,3,9,3,9,3,NULL,3),
            ('308',92,3,20,'(HTL437/ABHB9)-B-108-1-1-B',8,3,8,3,8,3,NULL,3),
            ('307',91,3,22,'(HTL437/ABHB9)-B-120-1-1-B',7,3,7,3,7,3,NULL,3),
            ('306',90,3,2,'(LTL812/ABT11)-B-16-1-1-B',7,2,7,2,7,2,NULL,3),
            ('305',89,3,27,'(HTL437/ABP38)-B-157-1-1-B',8,2,8,2,8,2,NULL,3),
            ('304',88,3,21,'(HTL437/ABHB9)-B-119-1-1-B',9,2,9,2,9,2,NULL,3),
            ('303',87,3,6,'(HTL437/ABW52)-B-3-1-2-B',9,1,9,1,9,1,NULL,3),
            ('302',86,3,14,'(HTL437/RK132)-B-89-1-2-B',8,1,8,1,8,1,NULL,3),
            ('301',85,3,31,'(HTL437/ABP38)-B-188-1-2-B',7,1,7,1,7,1,NULL,3),
            ('242',84,2,16,'(HTL437/ABHB9)-B-17-1-2-B',4,14,4,14,4,14,NULL,2),
            ('241',83,2,42,'(LTL812/ABT11)-B-8-1-2-B',5,14,5,14,5,14,NULL,2),
            ('240',82,2,17,'(HTL437/ABHB9)-B-31-1-1-B',6,14,6,14,6,14,NULL,2),
            ('239',81,2,19,'(HTL437/ABHB9)-B-73-1-2-B',6,13,6,13,6,13,NULL,2),
            ('238',80,2,4,'(LTL812/ABT11)-B-51-1-2-B',5,13,5,13,5,13,NULL,2),
            ('237',79,2,25,'(HTL437/ABHB9)-B-168-1-2-B',4,13,4,13,4,13,NULL,2),
            ('236',78,2,24,'(HTL437/ABHB9)-B-168-1-1-B',4,12,4,12,4,12,NULL,2),
            ('235',77,2,29,'(HTL437/ABP38)-B-178-1-1-B',5,12,5,12,5,12,NULL,2),
            ('234',76,2,7,'(HTL437/ABW52)-B-17-1-2-B',6,12,6,12,6,12,NULL,2),
            ('233',75,2,36,'(HTL437/ABW52//HTL437)-98-1-1-1-B',6,11,6,11,6,11,NULL,2),
            ('232',74,2,1,'(LTL812/ABW52)-B-25-1-1-B',5,11,5,11,5,11,NULL,2),
            ('231',73,2,33,'(HTL437/ABW52//HTL437)-96-1-1-1-B',4,11,4,11,4,11,NULL,2),
            ('230',72,2,10,'(HTL437/RK132)-B-9-1-1-B',4,10,4,10,4,10,NULL,2),
            ('229',71,2,13,'(HTL437/RK132)-B-86-1-1-B',5,10,5,10,5,10,NULL,2),
            ('228',70,2,40,'(LTL812/RK198)-B-53-1-2-B',6,10,6,10,6,10,NULL,2),
            ('227',69,2,2,'(LTL812/ABT11)-B-16-1-1-B',6,9,6,9,6,9,NULL,2),
            ('226',68,2,27,'(HTL437/ABP38)-B-157-1-1-B',5,9,5,9,5,9,NULL,2),
            ('225',67,2,31,'(HTL437/ABP38)-B-188-1-2-B',4,9,4,9,4,9,NULL,2),
            ('224',66,2,8,'(HTL437/ABW52)-B-71-1-1-B',4,8,4,8,4,8,NULL,2),
            ('223',65,2,37,'(HTL437/ABW52//HTL437)-98-1-1-2-B',5,8,5,8,5,8,NULL,2),
            ('222',64,2,32,'(HTL437/ABP38)-B-189-1-2-B',6,8,6,8,6,8,NULL,2),
            ('221',63,2,35,'(HTL437/ABW52//HTL437)-96-1-1-2-B',6,7,6,7,6,7,NULL,2),
            ('220',62,2,6,'(HTL437/ABW52)-B-3-1-2-B',5,7,5,7,5,7,NULL,2),
            ('219',61,2,12,'(LTL812/ABW52)-B-65-1-1-B',4,7,4,7,4,7,NULL,2),
            ('218',60,2,21,'(HTL437/ABHB9)-B-119-1-1-B',4,6,4,6,4,6,NULL,2),
            ('217',59,2,15,'(HTL437/ABHB9)-B-3-1-1-B',5,6,5,6,5,6,NULL,2),
            ('216',58,2,28,'(HTL437/ABP38)-B-157-1-2-B',6,6,6,6,6,6,NULL,2),
            ('215',57,2,23,'(LTL812/ABW52)-B-65-1-2-B',6,5,6,5,6,5,NULL,2),
            ('214',56,2,30,'(HTL437/ABP38)-B-178-1-2-B',5,5,5,5,5,5,NULL,2),
            ('213',55,2,41,'(LTL812/RK198)-B-112-1-2-B',4,5,4,5,4,5,NULL,2),
            ('212',54,2,34,'(LTL812/ABW52)-B-74-1-2-B',4,4,4,4,4,4,NULL,2),
            ('211',53,2,20,'(HTL437/ABHB9)-B-108-1-1-B',5,4,5,4,5,4,NULL,2),
            ('210',52,2,14,'(HTL437/RK132)-B-89-1-2-B',6,4,6,4,6,4,NULL,2),
            ('209',51,2,3,'(LTL812/ABT11)-B-16-1-2-B',6,3,6,3,6,3,NULL,2),
            ('208',50,2,11,'(HTL437/RK132)-B-51-1-2-B',5,3,5,3,5,3,NULL,2),
            ('207',49,2,5,'(HTL437/ABW52)-B-3-1-1-B',4,3,4,3,4,3,NULL,2),
            ('206',48,2,18,'(HTL437/ABHB9)-B-31-1-2-B',4,2,4,2,4,2,NULL,2),
            ('205',47,2,9,'(HTL437/ABW52)-B-145-1-1-B',5,2,5,2,5,2,NULL,2),
            ('204',46,2,22,'(HTL437/ABHB9)-B-120-1-1-B',6,2,6,2,6,2,NULL,2),
            ('203',45,2,39,'(LTL812/ABW52)-B-124-1-2-B',6,1,6,1,6,1,NULL,2),
            ('202',44,2,26,'(HTL437/ABHB9)-B-183-1-2-B',5,1,5,1,5,1,NULL,2),
            ('201',43,2,38,'(LTL812/ABW52)-B-113-1-2-B',4,1,4,1,4,1,NULL,2),
            ('142',42,1,42,'(LTL812/ABT11)-B-8-1-2-B',1,14,1,14,1,14,NULL,1),
            ('141',41,1,9,'(HTL437/ABW52)-B-145-1-1-B',2,14,2,14,2,14,NULL,1),
            ('140',40,1,6,'(HTL437/ABW52)-B-3-1-2-B',3,14,3,14,3,14,NULL,1),
            ('139',39,1,3,'(LTL812/ABT11)-B-16-1-2-B',3,13,3,13,3,13,NULL,1),
            ('138',38,1,36,'(HTL437/ABW52//HTL437)-98-1-1-1-B',2,13,2,13,2,13,NULL,1),
            ('137',37,1,33,'(HTL437/ABW52//HTL437)-96-1-1-1-B',1,13,1,13,1,13,NULL,1),
            ('136',36,1,16,'(HTL437/ABHB9)-B-17-1-2-B',1,12,1,12,1,12,NULL,1),
            ('135',35,1,22,'(HTL437/ABHB9)-B-120-1-1-B',2,12,2,12,2,12,NULL,1),
            ('134',34,1,14,'(HTL437/RK132)-B-89-1-2-B',3,12,3,12,3,12,NULL,1),
            ('133',33,1,35,'(HTL437/ABW52//HTL437)-96-1-1-2-B',3,11,3,11,3,11,NULL,1),
            ('132',32,1,27,'(HTL437/ABP38)-B-157-1-1-B',2,11,2,11,2,11,NULL,1),
            ('131',31,1,13,'(HTL437/RK132)-B-86-1-1-B',1,11,1,11,1,11,NULL,1),
            ('130',30,1,15,'(HTL437/ABHB9)-B-3-1-1-B',1,10,1,10,1,10,NULL,1),
            ('129',29,1,25,'(HTL437/ABHB9)-B-168-1-2-B',2,10,2,10,2,10,NULL,1),
            ('128',28,1,18,'(HTL437/ABHB9)-B-31-1-2-B',3,10,3,10,3,10,NULL,1),
            ('127',27,1,38,'(LTL812/ABW52)-B-113-1-2-B',3,9,3,9,3,9,NULL,1),
            ('126',26,1,8,'(HTL437/ABW52)-B-71-1-1-B',2,9,2,9,2,9,NULL,1),
            ('125',25,1,10,'(HTL437/RK132)-B-9-1-1-B',1,9,1,9,1,9,NULL,1),
            ('124',24,1,28,'(HTL437/ABP38)-B-157-1-2-B',1,8,1,8,1,8,NULL,1),
            ('123',23,1,24,'(HTL437/ABHB9)-B-168-1-1-B',2,8,2,8,2,8,NULL,1),
            ('122',22,1,40,'(LTL812/RK198)-B-53-1-2-B',3,8,3,8,3,8,NULL,1),
            ('121',21,1,31,'(HTL437/ABP38)-B-188-1-2-B',3,7,3,7,3,7,NULL,1),
            ('120',20,1,21,'(HTL437/ABHB9)-B-119-1-1-B',2,7,2,7,2,7,NULL,1),
            ('119',19,1,11,'(HTL437/RK132)-B-51-1-2-B',1,7,1,7,1,7,NULL,1),
            ('118',18,1,29,'(HTL437/ABP38)-B-178-1-1-B',1,6,1,6,1,6,NULL,1),
            ('117',17,1,32,'(HTL437/ABP38)-B-189-1-2-B',2,6,2,6,2,6,NULL,1),
            ('116',16,1,5,'(HTL437/ABW52)-B-3-1-1-B',3,6,3,6,3,6,NULL,1),
            ('115',15,1,30,'(HTL437/ABP38)-B-178-1-2-B',3,5,3,5,3,5,NULL,1),
            ('114',14,1,26,'(HTL437/ABHB9)-B-183-1-2-B',2,5,2,5,2,5,NULL,1),
            ('113',13,1,17,'(HTL437/ABHB9)-B-31-1-1-B',1,5,1,5,1,5,NULL,1),
            ('112',12,1,23,'(LTL812/ABW52)-B-65-1-2-B',1,4,1,4,1,4,NULL,1),
            ('111',11,1,12,'(LTL812/ABW52)-B-65-1-1-B',2,4,2,4,2,4,NULL,1),
            ('110',10,1,41,'(LTL812/RK198)-B-112-1-2-B',3,4,3,4,3,4,NULL,1),
            ('109',9,1,20,'(HTL437/ABHB9)-B-108-1-1-B',3,3,3,3,3,3,NULL,1),
            ('108',8,1,2,'(LTL812/ABT11)-B-16-1-1-B',2,3,2,3,2,3,NULL,1),
            ('107',7,1,1,'(LTL812/ABW52)-B-25-1-1-B',1,3,1,3,1,3,NULL,1),
            ('106',6,1,19,'(HTL437/ABHB9)-B-73-1-2-B',1,2,1,2,1,2,NULL,1),
            ('105',5,1,4,'(LTL812/ABT11)-B-51-1-2-B',2,2,2,2,2,2,NULL,1),
            ('104',4,1,39,'(LTL812/ABW52)-B-124-1-2-B',3,2,3,2,3,2,NULL,1),
            ('103',3,1,7,'(HTL437/ABW52)-B-17-1-2-B',3,1,3,1,3,1,NULL,1),
            ('102',2,1,37,'(HTL437/ABW52//HTL437)-98-1-1-2-B',2,1,2,1,2,1,NULL,1),
            ('101',1,1,34,'(LTL812/ABW52)-B-74-1-2-B',1,1,1,1,1,1,NULL,1)
    ) AS t (
        plot_code, plot_number, rep, entry_number, designation, design_x, design_y, pa_x, pa_y, field_x, field_y, plot_qc_code, block_number
    )
    INNER JOIN experiment.entry AS ent
        ON t.entry_number = ent.entry_number
        AND t.designation = ent.entry_name
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
        AND entlist.entry_list_name = 'KE-IYT-2021-A Entry List'
    INNER JOIN experiment.occurrence AS occ
        ON occ.occurrence_name = 'AF2021_001#OCC-03'
    INNER JOIN experiment.location AS loc
        ON loc.location_name = 'IRRI_LosBanos_001'
    INNER JOIN tenant.person AS person
        ON person.username = 'nicola.costa'
ORDER BY
    t.plot_number
;



-- revert changes OCC
--rollback DELETE FROM
--rollback     experiment.plot AS plot
--rollback USING
--rollback     experiment.occurrence AS occ,
--rollback     experiment.location AS loc
--rollback WHERE
--rollback     plot.occurrence_id = occ.id
--rollback     AND occ.occurrence_name IN ('AF2021_001#OCC-02','AF2021_001#OCC-01', 'AF2021_001#OCC-03')
--rollback     AND loc.location_name IN ('IRRI_SouthAsia_001','Nepal-001','IRRI_LosBanos_001')
--rollback ;



--changeset postgres:populate_breeding_trial_for_af_in_experiment.planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-59 Populate breeding trial for AF in experiment.planting_instruction



INSERT INTO
   experiment.planting_instruction (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status, entry_id,
       plot_id, germplasm_id, seed_id, creator_id
   )
SELECT
    ent.entry_code,
    ent.entry_number,
    ent.entry_name,
    ent.entry_type,
    ent.entry_role,
    ent.entry_status,
    ent.id AS entry_id,
    plot.id AS plot_id,
    ent.germplasm_id,
    ent.seed_id,
    person.id AS creator_id
FROM
    experiment.entry_list AS entlist
    INNER JOIN experiment.entry AS ent
        ON entlist.id = ent.entry_list_id
     INNER JOIN experiment.plot AS plot
         ON plot.entry_id = ent.id
     INNER JOIN tenant.person AS person
         ON person.username = 'nicola.costa'
WHERE
    entlist.entry_list_name = 'KE-IYT-2021-A Entry List'
ORDER BY
     plot.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.planting_instruction AS plantinst
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'KE-IYT-2021-A Entry List';



--changeset postgres:populate_breeding_trial_for_af_in_experiment.experiment_design context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-59 Populate breeding trial for AF in experiment.experiment_design



INSERT INTO
   experiment.experiment_design (
       occurrence_id, design_id, plot_id, block_type, block_value,
       block_level_number, creator_id, block_name
   )
SELECT
    occ.id AS occurrence_id,
    t.design_id AS design_id,
    (SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name=t.entry_name AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name=els.entry_list_name)) AND ep.plot_code = t.plot_code AND ep.plot_number = t.plot_number AND ep.occurrence_id = (SELECT id FROM experiment.occurrence WHERE occurrence_name=occ.occurrence_name)) AS plot_id,
    t.block_type AS block_type,
    t.block_value AS block_value,
    t.block_level_number AS block_level_number,
    psn.id AS creator_id,
    t.block_name AS block_name
FROM
    (
        VALUES
            (1,'(HTL437/RK132)-B-86-1-1-B','101',1,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52//HTL437)-96-1-1-1-B','102',2,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-25-1-1-B','103',3,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-3-1-1-B','104',4,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-157-1-1-B','105',5,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52//HTL437)-98-1-1-1-B','106',6,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABT11)-B-8-1-2-B','107',7,'replication block',1,1,'replicate'),
            (1,'(HTL437/RK132)-B-89-1-2-B','108',8,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-124-1-2-B','109',9,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-17-1-2-B','110',10,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-189-1-2-B','111',11,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-31-1-1-B','112',12,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABT11)-B-51-1-2-B','113',13,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-113-1-2-B','114',14,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-178-1-1-B','115',15,'replication block',1,1,'replicate'),
            (1,'(HTL437/RK132)-B-51-1-2-B','116',16,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52//HTL437)-98-1-1-2-B','117',17,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52)-B-145-1-1-B','118',18,'replication block',1,1,'replicate'),
            (1,'(HTL437/RK132)-B-9-1-1-B','119',19,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52)-B-3-1-1-B','120',20,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52)-B-71-1-1-B','121',21,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-119-1-1-B','122',22,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-65-1-1-B','123',23,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABT11)-B-16-1-1-B','124',24,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52)-B-3-1-2-B','125',25,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-108-1-1-B','126',26,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-168-1-2-B','127',27,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-65-1-2-B','128',28,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-178-1-2-B','129',29,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-183-1-2-B','130',30,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-120-1-1-B','131',31,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-73-1-2-B','132',32,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-74-1-2-B','133',33,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-157-1-2-B','134',34,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABT11)-B-16-1-2-B','135',35,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52)-B-17-1-2-B','136',36,'replication block',1,1,'replicate'),
            (1,'(LTL812/RK198)-B-53-1-2-B','137',37,'replication block',1,1,'replicate'),
            (1,'(LTL812/RK198)-B-112-1-2-B','138',38,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52//HTL437)-96-1-1-2-B','139',39,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-168-1-1-B','140',40,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-188-1-2-B','141',41,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-31-1-2-B','142',42,'replication block',1,1,'replicate'),
            (2,'(HTL437/RK132)-B-89-1-2-B','201',43,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52)-B-3-1-1-B','202',44,'replication block',2,1,'replicate'),
            (2,'(HTL437/RK132)-B-9-1-1-B','203',45,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-73-1-2-B','204',46,'replication block',2,1,'replicate'),
            (2,'(LTL812/RK198)-B-53-1-2-B','205',47,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-65-1-1-B','206',48,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-3-1-1-B','207',49,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-183-1-2-B','208',50,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52//HTL437)-98-1-1-1-B','209',51,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABT11)-B-16-1-1-B','210',52,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52//HTL437)-96-1-1-1-B','211',53,'replication block',2,1,'replicate'),
            (2,'(HTL437/RK132)-B-86-1-1-B','212',54,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-74-1-2-B','213',55,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABT11)-B-51-1-2-B','214',56,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-157-1-2-B','215',57,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52)-B-17-1-2-B','216',58,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABT11)-B-16-1-2-B','217',59,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-188-1-2-B','218',60,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52)-B-3-1-2-B','219',61,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52//HTL437)-96-1-1-2-B','220',62,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-189-1-2-B','221',63,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52)-B-71-1-1-B','222',64,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-31-1-1-B','223',65,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-120-1-1-B','224',66,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-168-1-2-B','225',67,'replication block',2,1,'replicate'),
            (2,'(LTL812/RK198)-B-112-1-2-B','226',68,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-17-1-2-B','227',69,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-178-1-2-B','228',70,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-65-1-2-B','229',71,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52//HTL437)-98-1-1-2-B','230',72,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-25-1-1-B','231',73,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-157-1-1-B','232',74,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52)-B-145-1-1-B','233',75,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABT11)-B-8-1-2-B','234',76,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-124-1-2-B','235',77,'replication block',2,1,'replicate'),
            (2,'(HTL437/RK132)-B-51-1-2-B','236',78,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-168-1-1-B','237',79,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-119-1-1-B','238',80,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-113-1-2-B','239',81,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-178-1-1-B','240',82,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-31-1-2-B','241',83,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-108-1-1-B','242',84,'replication block',2,1,'replicate'),
            (3,'(HTL437/ABW52)-B-3-1-1-B','301',85,'replication block',3,1,'replicate'),
            (3,'(HTL437/RK132)-B-86-1-1-B','302',86,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-3-1-1-B','303',87,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-178-1-2-B','304',88,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-73-1-2-B','305',89,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52)-B-145-1-1-B','306',90,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-17-1-2-B','307',91,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-120-1-1-B','308',92,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-178-1-1-B','309',93,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-65-1-1-B','310',94,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52)-B-3-1-2-B','311',95,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-25-1-1-B','312',96,'replication block',3,1,'replicate'),
            (3,'(LTL812/RK198)-B-53-1-2-B','313',97,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-188-1-2-B','314',98,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-168-1-1-B','315',99,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABT11)-B-51-1-2-B','316',100,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52)-B-17-1-2-B','317',101,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52)-B-71-1-1-B','318',102,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-31-1-1-B','319',103,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52//HTL437)-96-1-1-1-B','320',104,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-124-1-2-B','321',105,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-168-1-2-B','322',106,'replication block',3,1,'replicate'),
            (3,'(LTL812/RK198)-B-112-1-2-B','323',107,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-157-1-2-B','324',108,'replication block',3,1,'replicate'),
            (3,'(HTL437/RK132)-B-9-1-1-B','325',109,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-189-1-2-B','326',110,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-157-1-1-B','327',111,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-183-1-2-B','328',112,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-119-1-1-B','329',113,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-108-1-1-B','330',114,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52//HTL437)-96-1-1-2-B','331',115,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52//HTL437)-98-1-1-2-B','332',116,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABT11)-B-16-1-1-B','333',117,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-74-1-2-B','334',118,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABT11)-B-8-1-2-B','335',119,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52//HTL437)-98-1-1-1-B','336',120,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABT11)-B-16-1-2-B','337',121,'replication block',3,1,'replicate'),
            (3,'(HTL437/RK132)-B-89-1-2-B','338',122,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-65-1-2-B','339',123,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-113-1-2-B','340',124,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-31-1-2-B','341',125,'replication block',3,1,'replicate'),
            (3,'(HTL437/RK132)-B-51-1-2-B','342',126,'replication block',3,1,'replicate')
) AS t (design_id, entry_name, plot_code, plot_number, block_type, block_value, block_level_number, block_name)
INNER JOIN
    experiment.occurrence occ
ON
    occ.occurrence_name='AF2021_001#OCC-02'
INNER JOIN
    experiment.entry_list els
ON
    els.entry_list_name = 'KE-IYT-2021-A Entry List'
INNER JOIN
     tenant.person psn
ON
    psn.username = 'nicola.costa';

--set 2
INSERT INTO
   experiment.experiment_design (
       occurrence_id, design_id, plot_id, block_type, block_value,
       block_level_number, creator_id, block_name
   )
SELECT
    occ.id AS occurrence_id,
    t.design_id AS design_id,
    (SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name=t.entry_name AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name=els.entry_list_name)) AND ep.plot_code = t.plot_code AND ep.plot_number = t.plot_number AND ep.occurrence_id = (SELECT id FROM experiment.occurrence WHERE occurrence_name=occ.occurrence_name)) AS plot_id,
    t.block_type AS block_type,
    t.block_value AS block_value,
    t.block_level_number AS block_level_number,
    psn.id AS creator_id,
    t.block_name AS block_name
FROM
    (
        VALUES
            (1,'(LTL812/ABT11)-B-51-1-2-B','101',1,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-168-1-2-B','102',2,'replication block',1,1,'replicate'),
            (1,'(HTL437/RK132)-B-89-1-2-B','103',3,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52)-B-145-1-1-B','104',4,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-119-1-1-B','105',5,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52)-B-3-1-1-B','106',6,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-31-1-2-B','107',7,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-157-1-1-B','108',8,'replication block',1,1,'replicate'),
            (1,'(HTL437/RK132)-B-86-1-1-B','109',9,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABT11)-B-16-1-2-B','110',10,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-3-1-1-B','111',11,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52//HTL437)-98-1-1-2-B','112',12,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-31-1-1-B','113',13,'replication block',1,1,'replicate'),
            (1,'(LTL812/RK198)-B-112-1-2-B','114',14,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-120-1-1-B','115',15,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-183-1-2-B','116',16,'replication block',1,1,'replicate'),
            (1,'(LTL812/RK198)-B-53-1-2-B','117',17,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-65-1-1-B','118',18,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-73-1-2-B','119',19,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-108-1-1-B','120',20,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52//HTL437)-96-1-1-1-B','121',21,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-65-1-2-B','122',22,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-124-1-2-B','123',23,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABT11)-B-8-1-2-B','124',24,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52//HTL437)-96-1-1-2-B','125',25,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-17-1-2-B','126',26,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52)-B-17-1-2-B','127',27,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-168-1-1-B','128',28,'replication block',1,1,'replicate'),
            (1,'(HTL437/RK132)-B-51-1-2-B','129',29,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-25-1-1-B','130',30,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-178-1-2-B','131',31,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-189-1-2-B','132',32,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABT11)-B-16-1-1-B','133',33,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-74-1-2-B','134',34,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52)-B-3-1-2-B','135',35,'replication block',1,1,'replicate'),
            (1,'(HTL437/RK132)-B-9-1-1-B','136',36,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-178-1-1-B','137',37,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-157-1-2-B','138',38,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-113-1-2-B','139',39,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52)-B-71-1-1-B','140',40,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52//HTL437)-98-1-1-1-B','141',41,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-188-1-2-B','142',42,'replication block',1,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-17-1-2-B','201',43,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-188-1-2-B','202',44,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-183-1-2-B','203',45,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52//HTL437)-96-1-1-1-B','204',46,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABT11)-B-51-1-2-B','205',47,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-65-1-2-B','206',48,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-119-1-1-B','207',49,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52//HTL437)-98-1-1-1-B','208',50,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52)-B-3-1-2-B','209',51,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-178-1-1-B','210',52,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-157-1-2-B','211',53,'replication block',2,1,'replicate'),
            (2,'(LTL812/RK198)-B-53-1-2-B','212',54,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52)-B-71-1-1-B','213',55,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-120-1-1-B','214',56,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52)-B-3-1-1-B','215',57,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52)-B-145-1-1-B','216',58,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-73-1-2-B','217',59,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52//HTL437)-96-1-1-2-B','218',60,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-168-1-1-B','219',61,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-3-1-1-B','220',62,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABT11)-B-16-1-1-B','221',63,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-74-1-2-B','222',64,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABT11)-B-16-1-2-B','223',65,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABT11)-B-8-1-2-B','224',66,'replication block',2,1,'replicate'),
            (2,'(LTL812/RK198)-B-112-1-2-B','225',67,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-25-1-1-B','226',68,'replication block',2,1,'replicate'),
            (2,'(HTL437/RK132)-B-9-1-1-B','227',69,'replication block',2,1,'replicate'),
            (2,'(HTL437/RK132)-B-51-1-2-B','228',70,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-65-1-1-B','229',71,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-178-1-2-B','230',72,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52)-B-17-1-2-B','231',73,'replication block',2,1,'replicate'),
            (2,'(HTL437/RK132)-B-89-1-2-B','232',74,'replication block',2,1,'replicate'),
            (2,'(HTL437/RK132)-B-86-1-1-B','233',75,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-31-1-2-B','234',76,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-157-1-1-B','235',77,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-189-1-2-B','236',78,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-124-1-2-B','237',79,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-31-1-1-B','238',80,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52//HTL437)-98-1-1-2-B','239',81,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-168-1-2-B','240',82,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-108-1-1-B','241',83,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-113-1-2-B','242',84,'replication block',2,1,'replicate'),
            (3,'(LTL812/ABW52)-B-113-1-2-B','301',85,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-65-1-1-B','302',86,'replication block',3,1,'replicate'),
            (3,'(HTL437/RK132)-B-89-1-2-B','303',87,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52)-B-17-1-2-B','304',88,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-157-1-1-B','305',89,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-183-1-2-B','306',90,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52//HTL437)-98-1-1-1-B','307',91,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-31-1-1-B','308',92,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-168-1-2-B','309',93,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-65-1-2-B','310',94,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-17-1-2-B','311',95,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52)-B-3-1-2-B','312',96,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52//HTL437)-96-1-1-1-B','313',97,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-120-1-1-B','314',98,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABT11)-B-8-1-2-B','315',99,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-188-1-2-B','316',100,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-31-1-2-B','317',101,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-3-1-1-B','318',102,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52//HTL437)-96-1-1-2-B','319',103,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABT11)-B-16-1-2-B','320',104,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52)-B-145-1-1-B','321',105,'replication block',3,1,'replicate'),
            (3,'(HTL437/RK132)-B-9-1-1-B','322',106,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-108-1-1-B','323',107,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-124-1-2-B','324',108,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-157-1-2-B','325',109,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-74-1-2-B','326',110,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52)-B-3-1-1-B','327',111,'replication block',3,1,'replicate'),
            (3,'(LTL812/RK198)-B-112-1-2-B','328',112,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-178-1-2-B','329',113,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-178-1-1-B','330',114,'replication block',3,1,'replicate'),
            (3,'(HTL437/RK132)-B-86-1-1-B','331',115,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52//HTL437)-98-1-1-2-B','332',116,'replication block',3,1,'replicate'),
            (3,'(LTL812/RK198)-B-53-1-2-B','333',117,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-25-1-1-B','334',118,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABT11)-B-16-1-1-B','335',119,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-168-1-1-B','336',120,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52)-B-71-1-1-B','337',121,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABT11)-B-51-1-2-B','338',122,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-73-1-2-B','339',123,'replication block',3,1,'replicate'),
            (3,'(HTL437/RK132)-B-51-1-2-B','340',124,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-119-1-1-B','341',125,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-189-1-2-B','342',126,'replication block',3,1,'replicate')
) AS t (design_id, entry_name, plot_code, plot_number, block_type, block_value, block_level_number, block_name)
INNER JOIN
    experiment.occurrence occ
ON
    occ.occurrence_name='AF2021_001#OCC-01'
INNER JOIN
    experiment.entry_list els
ON
    els.entry_list_name = 'KE-IYT-2021-A Entry List'
INNER JOIN
     tenant.person psn
ON
    psn.username = 'nicola.costa';

--set 3
INSERT INTO
   experiment.experiment_design (
       occurrence_id, design_id, plot_id, block_type, block_value,
       block_level_number, creator_id, block_name
   )
SELECT
    occ.id AS occurrence_id,
    t.design_id AS design_id,
    (SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name=t.entry_name AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name=els.entry_list_name)) AND ep.plot_code = t.plot_code AND ep.plot_number = t.plot_number AND ep.occurrence_id = (SELECT id FROM experiment.occurrence WHERE occurrence_name=occ.occurrence_name)) AS plot_id,
    t.block_type AS block_type,
    t.block_value AS block_value,
    t.block_level_number AS block_level_number,
    psn.id AS creator_id,
    t.block_name AS block_name
FROM
    (
        VALUES  
            (1,'(LTL812/ABW52)-B-74-1-2-B','101',1,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52//HTL437)-98-1-1-2-B','102',2,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52)-B-17-1-2-B','103',3,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-124-1-2-B','104',4,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABT11)-B-51-1-2-B','105',5,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-73-1-2-B','106',6,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-25-1-1-B','107',7,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABT11)-B-16-1-1-B','108',8,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-108-1-1-B','109',9,'replication block',1,1,'replicate'),
            (1,'(LTL812/RK198)-B-112-1-2-B','110',10,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-65-1-1-B','111',11,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-65-1-2-B','112',12,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-31-1-1-B','113',13,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-183-1-2-B','114',14,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-178-1-2-B','115',15,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52)-B-3-1-1-B','116',16,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-189-1-2-B','117',17,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-178-1-1-B','118',18,'replication block',1,1,'replicate'),
            (1,'(HTL437/RK132)-B-51-1-2-B','119',19,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-119-1-1-B','120',20,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-188-1-2-B','121',21,'replication block',1,1,'replicate'),
            (1,'(LTL812/RK198)-B-53-1-2-B','122',22,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-168-1-1-B','123',23,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-157-1-2-B','124',24,'replication block',1,1,'replicate'),
            (1,'(HTL437/RK132)-B-9-1-1-B','125',25,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52)-B-71-1-1-B','126',26,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-113-1-2-B','127',27,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-31-1-2-B','128',28,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-168-1-2-B','129',29,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-3-1-1-B','130',30,'replication block',1,1,'replicate'),
            (1,'(HTL437/RK132)-B-86-1-1-B','131',31,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-157-1-1-B','132',32,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52//HTL437)-96-1-1-2-B','133',33,'replication block',1,1,'replicate'),
            (1,'(HTL437/RK132)-B-89-1-2-B','134',34,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-120-1-1-B','135',35,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-17-1-2-B','136',36,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52//HTL437)-96-1-1-1-B','137',37,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52//HTL437)-98-1-1-1-B','138',38,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABT11)-B-16-1-2-B','139',39,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52)-B-3-1-2-B','140',40,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52)-B-145-1-1-B','141',41,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABT11)-B-8-1-2-B','142',42,'replication block',1,1,'replicate'),
            (2,'(LTL812/ABW52)-B-113-1-2-B','201',43,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-183-1-2-B','202',44,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-124-1-2-B','203',45,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-120-1-1-B','204',46,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52)-B-145-1-1-B','205',47,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-31-1-2-B','206',48,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52)-B-3-1-1-B','207',49,'replication block',2,1,'replicate'),
            (2,'(HTL437/RK132)-B-51-1-2-B','208',50,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABT11)-B-16-1-2-B','209',51,'replication block',2,1,'replicate'),
            (2,'(HTL437/RK132)-B-89-1-2-B','210',52,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-108-1-1-B','211',53,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-74-1-2-B','212',54,'replication block',2,1,'replicate'),
            (2,'(LTL812/RK198)-B-112-1-2-B','213',55,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-178-1-2-B','214',56,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-65-1-2-B','215',57,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-157-1-2-B','216',58,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-3-1-1-B','217',59,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-119-1-1-B','218',60,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-65-1-1-B','219',61,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52)-B-3-1-2-B','220',62,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52//HTL437)-96-1-1-2-B','221',63,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-189-1-2-B','222',64,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52//HTL437)-98-1-1-2-B','223',65,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52)-B-71-1-1-B','224',66,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-188-1-2-B','225',67,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-157-1-1-B','226',68,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABT11)-B-16-1-1-B','227',69,'replication block',2,1,'replicate'),
            (2,'(LTL812/RK198)-B-53-1-2-B','228',70,'replication block',2,1,'replicate'),
            (2,'(HTL437/RK132)-B-86-1-1-B','229',71,'replication block',2,1,'replicate'),
            (2,'(HTL437/RK132)-B-9-1-1-B','230',72,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52//HTL437)-96-1-1-1-B','231',73,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-25-1-1-B','232',74,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52//HTL437)-98-1-1-1-B','233',75,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52)-B-17-1-2-B','234',76,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-178-1-1-B','235',77,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-168-1-1-B','236',78,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-168-1-2-B','237',79,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABT11)-B-51-1-2-B','238',80,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-73-1-2-B','239',81,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-31-1-1-B','240',82,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABT11)-B-8-1-2-B','241',83,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-17-1-2-B','242',84,'replication block',2,1,'replicate'),
            (3,'(HTL437/ABP38)-B-188-1-2-B','301',85,'replication block',3,1,'replicate'),
            (3,'(HTL437/RK132)-B-89-1-2-B','302',86,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52)-B-3-1-2-B','303',87,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-119-1-1-B','304',88,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-157-1-1-B','305',89,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABT11)-B-16-1-1-B','306',90,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-120-1-1-B','307',91,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-108-1-1-B','308',92,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-157-1-2-B','309',93,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-178-1-1-B','310',94,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABT11)-B-51-1-2-B','311',95,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52)-B-3-1-1-B','312',96,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-183-1-2-B','313',97,'replication block',3,1,'replicate'),
            (3,'(HTL437/RK132)-B-9-1-1-B','314',98,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-124-1-2-B','315',99,'replication block',3,1,'replicate'),
            (3,'(HTL437/RK132)-B-86-1-1-B','316',100,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-168-1-2-B','317',101,'replication block',3,1,'replicate'),
            (3,'(HTL437/RK132)-B-51-1-2-B','318',102,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-31-1-2-B','319',103,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABT11)-B-16-1-2-B','320',104,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52)-B-71-1-1-B','321',105,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-189-1-2-B','322',106,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52//HTL437)-98-1-1-2-B','323',107,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52//HTL437)-98-1-1-1-B','324',108,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-178-1-2-B','325',109,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52//HTL437)-96-1-1-2-B','326',110,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52)-B-145-1-1-B','327',111,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-25-1-1-B','328',112,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-113-1-2-B','329',113,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52)-B-17-1-2-B','330',114,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-65-1-2-B','331',115,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABT11)-B-8-1-2-B','332',116,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-3-1-1-B','333',117,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-168-1-1-B','334',118,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-17-1-2-B','335',119,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-31-1-1-B','336',120,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-65-1-1-B','337',121,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52//HTL437)-96-1-1-1-B','338',122,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-73-1-2-B','339',123,'replication block',3,1,'replicate'),
            (3,'(LTL812/RK198)-B-112-1-2-B','340',124,'replication block',3,1,'replicate'),
            (3,'(LTL812/RK198)-B-53-1-2-B','341',125,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-74-1-2-B','342',126,'replication block',3,1,'replicate')
) AS t (design_id, entry_name, plot_code, plot_number, block_type, block_value, block_level_number, block_name)
INNER JOIN
    experiment.occurrence occ
ON
    occ.occurrence_name='AF2021_001#OCC-03'
INNER JOIN
    experiment.entry_list els
ON
    els.entry_list_name = 'KE-IYT-2021-A Entry List'
INNER JOIN
     tenant.person psn
ON
    psn.username = 'nicola.costa';



--rollback DELETE FROM experiment.experiment_design WHERE occurrence_id IN (SELECT id FROM experiment.occurrence WHERE occurrence_name IN ('AF2021_001#OCC-01','AF2021_001#OCC-02','AF2021_001#OCC-03'));