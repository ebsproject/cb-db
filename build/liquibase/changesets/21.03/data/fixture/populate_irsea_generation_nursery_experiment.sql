--liquibase formatted sql

--changeset postgres:populate_irsea_generation_nursery_experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate IRSEA generation experiment



INSERT INTO
    experiment.experiment (
        program_id, pipeline_id, stage_id, project_id, experiment_year, season_id, planting_season, experiment_code, experiment_name,
        experiment_type, experiment_sub_type, experiment_sub_sub_type, experiment_design_type, experiment_status,
        steward_id, creator_id, is_void, data_process_id, crop_id
    )
SELECT
    (SELECT id FROM tenant.program WHERE program_code = 'IRSEA') AS program_id,
    NULL AS pipeline_id,
    (SELECT id FROM tenant.stage WHERE stage_code = 'F2') AS stage_id,
    NULL AS project_id,
    2021 AS experiment_year,
    (SELECT id FROM tenant.season WHERE season_code = 'DS') AS season_id,
    '2021DS' AS planting_season,
    experiment.generate_code('experiment') AS experiment_code,
    'IRSEA-F2-2021-DS-1' AS experiment_name,
    'Generation Nursery' AS experiment_type,
    NULL AS experiment_sub_type,
    NULL AS experiment_sub_sub_type,
    'Systematic Arrangement' AS experiment_design_type,
    'planted' AS experiment_status,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS steward_id,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS creator_id,
    FALSE AS is_void,
    (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_DATA_PROCESS') AS data_process_id,
    (SELECT id FROM tenant.crop WHERE crop_code = 'RICE') AS crop_id
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment
--rollback WHERE
--rollback     experiment_name = 'IRSEA-F2-2021-DS-1'
--rollback ;



--changeset postgres:populate_irsea_rice_generation_nursery_in_tenant.protocol context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate IRSEA generation nursery experiment in tenant.protocol



INSERT INTO
    tenant.protocol (
        protocol_code, protocol_name, protocol_type, program_id, creator_id
    )
VALUES 
    ('TRAIT_PROTOCOL_TMPGN','Trait Protocol Tmp GN','trait',(SELECT id FROM tenant.program WHERE program_code='IRSEA'), (SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ('MANAGEMENT_PROTOCOL_TMPGN','Management Protocol Tmp GN','management',(SELECT id FROM tenant.program WHERE program_code='IRSEA'), (SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ('PLANTING_PROTOCOL_TMPGN','Planting Protocol Tmp GN','planting',(SELECT id FROM tenant.program WHERE program_code='IRSEA'), (SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ('HARVEST_PROTOCOL_TMPGN','Harvest Protocol Tmp GN','harvest',(SELECT id FROM tenant.program WHERE program_code='IRSEA'), (SELECT id FROM tenant.person WHERE username = 'nicola.costa'));



--rollback DELETE FROM tenant.protocol WHERE protocol_code IN ('TRAIT_PROTOCOL_TMPGN','MANAGEMENT_PROTOCOL_TMPGN','PLANTING_PROTOCOL_TMPGN','HARVEST_PROTOCOL_TMPGN');



--changeset postgres:populate_irsea_rice_generation_nursery_in_experiment.experiment_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate IRSEA generation nursery experiment in experiment.experiment_data



INSERT INTO
    experiment.experiment_data (
        experiment_id, variable_id, data_value, data_qc_code, protocol_id, creator_id
    )
VALUES
    ((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-F2-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='TRAIT_PROTOCOL_LIST_ID'),'1296','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Trait Protocol Tmp GN'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
    ,((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-F2-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='MANAGEMENT_PROTOCOL_LIST_ID'),'1297','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Management Protocol Tmp GN'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
    ,((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-F2-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='ESTABLISHMENT'),'transplanted','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp GN'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
    ,((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-F2-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='PLANTING_TYPE'),'Flat','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp GN'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
    ,((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-F2-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='PLOT_TYPE'),'1R x 12H','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp GN'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
    ,((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-F2-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='ROWS_PER_PLOT_CONT'),'1','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp GN'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
    ,((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-F2-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='HILLS_PER_ROW_CONT'),'12','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp GN'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
    ,((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-F2-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='DIST_BET_ROWS'),'20','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp GN'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
    ,((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-F2-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='DIST_BET_HILLS'),'20','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp GN'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
    ,((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-F2-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='PLOT_LN_1'),'5','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp GN'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
    ,((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-F2-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='PLOT_WIDTH_RICE'),'0.2','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp GN'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
    ,((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-F2-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='PLOT_AREA_1'),'1','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp GN'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
    ,((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-F2-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='ALLEY_LENGTH'),'1','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp GN'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
    ,((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-F2-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='SEEDING_RATE'),'Normal','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp GN'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
    ,((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-F2-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='PROTOCOL_TARGET_LEVEL'),'occurrence','N',(SELECT id FROM tenant.protocol WHERE protocol_name=''),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
    ,((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-F2-2021-DS-1'),(SELECT id FROM master.variable WHERE abbrev='HV_METH_DISC'),'Single Plant Selection','N',(SELECT id FROM tenant.protocol WHERE protocol_name='Harvest Protocol Tmp GN'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
    ;



--rollback DELETE FROM experiment.experiment_data WHERE experiment_id = (SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-F2-2021-DS-1');



--changeset postgres:populate_irsea_rice_generation_nursery_in_experiment.experiment_protocol context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate IRSEA generation nursery experiment in experiment.experiment_protocol



INSERT INTO
    experiment.experiment_protocol (
        experiment_id, protocol_id, order_number, creator_id
    )
VALUES
    ((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-F2-2021-DS-1'),(SELECT id FROM tenant.protocol WHERE protocol_name='Trait Protocol Tmp GN'),1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
    ,((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-F2-2021-DS-1'),(SELECT id FROM tenant.protocol WHERE protocol_name='Management Protocol Tmp GN'),2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
    ,((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-F2-2021-DS-1'),(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp GN'),3,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
    ,((SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-F2-2021-DS-1'),(SELECT id FROM tenant.protocol WHERE protocol_name='Harvest Protocol Tmp GN'),4,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
    ;



--rollback DELETE FROM experiment.experiment_protocol WHERE experiment_id = (SELECT id FROM experiment.experiment WHERE experiment_name='IRSEA-F2-2021-DS-1');



--changeset postgres:populate_irsea_rice_generation_nursery_in_experiment.entry_list context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate IRSEA generation nursery in experiment.entry_list



INSERT INTO
    experiment.entry_list (
        entry_list_code, entry_list_name, entry_list_status, experiment_id, creator_id, is_void, entry_list_type
    )
SELECT
    experiment.generate_code('entry_list') AS entry_list_code,
    'IRSEA-F2-2021-DS-1 Entry List' AS entry_list_name,
    'completed' AS entry_list_status,
    (SELECT id FROM experiment.experiment WHERE experiment_name = 'IRSEA-F2-2021-DS-1') AS experiment_id,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS creator_id,
    FALSE AS is_void,
    'entry list' AS entry_list_type
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_list
--rollback WHERE
--rollback     entry_list_name = 'IRSEA-F2-2021-DS-1 Entry List';



--changeset postgres:populate_irsea_generation_nursery_in_experiment.entry context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate IRSEA generation nursery in experiment.entry



INSERT INTO
   experiment.entry (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status,
       entry_list_id, germplasm_id, seed_id, creator_id, is_void
   )
SELECT
    t.entry_number AS entry_code,
    t.entry_number,
    t.designation AS entry_name,
    'entry' AS entry_type,
    NULL AS entry_role,
    'active' AS entry_status,
    entlist.id AS entry_list_id,
    ge.id AS germplasm_id,
    seed.id AS seed_id,
    person.id AS creator_id,
    FALSE AS is_void
FROM
    (
        VALUES
        (1,'IR 121395-B','300220641')
        ,(2,'IR 121397-B','300220643')
        ,(3,'IR 121398-B','300220644')
        ,(4,'IR 121399-B','300220645')
        ,(5,'IR 121400-B','300220646')
        ,(6,'IR 121402-B','300220648')
        ,(7,'IR 121403-B','300220649')
        ,(8,'IR 121404-B','300220650')
    ) AS t (
        entry_number, designation, seed_name
    )
    INNER JOIN germplasm.germplasm AS ge
        ON t.designation = ge.designation
    INNER JOIN germplasm.seed AS seed
        ON ge.id = seed.germplasm_id
        AND t.seed_name = seed.seed_name
    INNER JOIN experiment.entry_list as entlist
        ON entlist.entry_list_name = 'IRSEA-F2-2021-DS-1 Entry List'
    INNER JOIN tenant.person AS person
        ON person.username = 'nicola.costa'
ORDER BY
    t.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry AS e
--rollback USING
--rollback     experiment.entry_list AS el
--rollback WHERE
--rollback     e.entry_list_id = el.id
--rollback     AND el.entry_list_name = 'IRSEA-F2-2021-DS-1 Entry List'
--rollback ;



--changeset postgres:populate_rice_generation_nursery_in_experiment.occurrence context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate rice generation_nursery in experiment.occurrence



INSERT INTO
    experiment.occurrence (
        occurrence_code,
        occurrence_name,
        occurrence_status,
        experiment_id,
        site_id,
        field_id,
        rep_count,
        occurrence_number,
        creator_id
    )
SELECT
    experiment.generate_code('occurrence') AS occurrence_code,
    'IRSEA-F2-2021-DS-1_OCC1' AS occurrence_name,
    'planted' AS occurrence_status,
    expt.id AS experiment_id,
    geo.id AS site_id,
    field.id AS field_id,
    1 AS rep_count,
    2 AS occurrence_number,
    person.id AS creator_id
FROM
    experiment.experiment AS expt,
    place.geospatial_object AS geo,
    place.geospatial_object AS field,
    tenant.person AS person
WHERE
    expt.experiment_name = 'IRSEA-F2-2021-DS-1'
    AND geo.geospatial_object_code = 'IRRIHQ'
    AND field.geospatial_object_code = 'IRRI_FARMS'
    AND person.username = 'nicola.costa'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence
--rollback WHERE
--rollback     occurrence_name = 'IRSEA-F2-2021-DS-1_OCC1'
--rollback ;



--changeset postgres:populate_rice_generation_nursery_in_experiment.occurrence_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate rice generation nursery in experiment.occurrence_data



INSERT INTO
    experiment.occurrence_data (
        occurrence_id, variable_id, data_value, data_qc_code, creator_id
    )
VALUES  
    ((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-F2-2021-DS-1_OCC1'),(SELECT id FROM master.variable WHERE abbrev='CONTCT_PERSON_CONT'),'Allan, Jennifer','G',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'));



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence_data
--rollback WHERE
--rollback     occurrence_id = (SELECT id FROM experiment.occurrence WHERE occurrence_name = 'IRSEA-F2-2021-DS-1_OCC1')
--rollback ;



--changeset postgres:populate_rice_generation_nursery_in_place.geospatial_object context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate rice generation nursery in place.geospatial_object



INSERT INTO
    place.geospatial_object (
        geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, parent_geospatial_object_id, root_geospatial_object_id
    )
VALUES
    (
        place.generate_code('geospatial_object'), 
        'IRSEA-F2-2021-DS-1_LOC1', 
        'planting area', 
        'breeding_location', 
        '1', 
        (SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRI_FARMS'), 
        (SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRIHQ')
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object
--rollback WHERE
--rollback     geospatial_object_name = 'IRSEA-F2-2021-DS-1_LOC1'
--rollback ;



--changeset postgres:populate_rice_generation_nursery_in_experiment.location context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate rice generation nursery in experiment.location



INSERT INTO
   experiment.location (
       location_code, location_name, location_status, location_type,
       location_year, season_id, location_number, site_id, field_id,
       steward_id, geospatial_object_id, creator_id
   )
SELECT
    experiment.generate_code('location') AS location_code,
    'IRSEA-F2-2021-DS-1_LOC1' AS location_name,
    'planted' AS location_status,
    'planting area' AS location_type,
    2021 AS location_year,
    season.id AS season_id,
    1 AS location_number,
    site.id AS site_id,
    field.id AS field_id,
    person.id AS steward_id,
    geo.id AS geospatial_object_id,
    person.id AS creator_id
FROM
    tenant.person AS person,
    tenant.season AS season,
    place.geospatial_object AS geo,
    place.geospatial_object AS site,
    place.geospatial_object AS field
WHERE
    person.username = 'nicola.costa'
    AND season.season_code = 'DS'
    AND geo.geospatial_object_name = 'IRSEA-F2-2021-DS-1_LOC1'
    AND site.geospatial_object_code = 'IRRIHQ'
    AND field.geospatial_object_code = 'IRRI_FARMS'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location
--rollback WHERE
--rollback     location_name = 'IRSEA-F2-2021-DS-1_LOC1'
--rollback ;



--changeset postgres:populate_rice_generation_nursery_in_experiment.location_occurrence_group context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate rice generation nursery in experiment.location_occurrence_group



INSERT INTO 
    experiment.location_occurrence_group (
        location_id,occurrence_id,order_number,creator_id
    )
SELECT
    t.*
FROM
    (
      VALUES 
      (
          (SELECT id FROM experiment.location WHERE location_name='IRSEA-F2-2021-DS-1_LOC1'),
          (SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-F2-2021-DS-1_OCC1'),
          1,
          (SELECT id FROM tenant.person WHERE username = 'nicola.costa')
      )
    ) t;



--rollback DELETE FROM 
--rollback     experiment.location_occurrence_group
--rollback WHERE
--rollback     location_id = (SELECT id FROM experiment.location WHERE location_name='IRSEA-F2-2021-DS-1_LOC1')
--rollback AND
--rollback     occurrence_id IN (SELECT id FROM experiment.occurrence WHERE occurrence_name IN ('IRSEA-F2-2021-DS-1_OCC1'));



--changeset postgres:populate_rice_generation_nursery_in_experiment.plot context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate rice generation nursery in experiment.plot



INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, plot_order_number, pa_x, pa_y, field_x, field_y, plot_status, creator_id, block_number, harvest_status
   )
VALUES
   ((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-F2-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-F2-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='IR 121404-B' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-F2-2021-DS-1 Entry List')),'8',8,'plot',1,8,1,NULL,8,1,8,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),1,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-F2-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-F2-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='IR 121403-B' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-F2-2021-DS-1 Entry List')),'7',7,'plot',1,7,1,NULL,7,1,7,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),1,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-F2-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-F2-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='IR 121402-B' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-F2-2021-DS-1 Entry List')),'6',6,'plot',1,6,1,NULL,6,1,6,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),1,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-F2-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-F2-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='IR 121400-B' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-F2-2021-DS-1 Entry List')),'5',5,'plot',1,5,1,NULL,5,1,5,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),1,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-F2-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-F2-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='IR 121399-B' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-F2-2021-DS-1 Entry List')),'4',4,'plot',1,4,1,NULL,4,1,4,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),1,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-F2-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-F2-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='IR 121398-B' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-F2-2021-DS-1 Entry List')),'3',3,'plot',1,3,1,NULL,3,1,3,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),1,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-F2-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-F2-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='IR 121397-B' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-F2-2021-DS-1 Entry List')),'2',2,'plot',1,2,1,NULL,2,1,2,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),1,'NO_HARVEST')
    ,((SELECT id FROM experiment.occurrence WHERE occurrence_name='IRSEA-F2-2021-DS-1_OCC1'),(SELECT id FROM experiment.location WHERE location_name='IRSEA-F2-2021-DS-1_LOC1'),(SELECT id FROM experiment.entry WHERE entry_name='IR 121395-B' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='IRSEA-F2-2021-DS-1 Entry List')),'1',1,'plot',1,1,1,NULL,1,1,1,1,'active',(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),1,'NO_HARVEST')
    ;



-- revert changes OCC1
--rollback DELETE FROM
--rollback     experiment.plot AS plot
--rollback USING
--rollback     experiment.occurrence AS occ,
--rollback     experiment.location AS loc
--rollback WHERE
--rollback     plot.occurrence_id = occ.id
--rollback     AND occ.occurrence_name = 'IRSEA-F2-2021-DS-1_OCC1'
--rollback     AND loc.location_name = 'IRSEA-F2-2021-DS-1_LOC1'
--rollback ;



--changeset postgres:populate_rice_generation_nursery_in_experiment.planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-41 Populate rice generation nursery in experiment.planting_instruction



INSERT INTO
   experiment.planting_instruction (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status, entry_id,
       plot_id, germplasm_id, seed_id, creator_id
   )
SELECT
    ent.entry_code,
    ent.entry_number,
    ent.entry_name,
    ent.entry_type,
    ent.entry_role,
    ent.entry_status,
    ent.id AS entry_id,
    plot.id AS plot_id,
    ent.germplasm_id,
    ent.seed_id,
    person.id AS creator_id
FROM
    experiment.entry_list AS entlist
    INNER JOIN experiment.entry AS ent
        ON entlist.id = ent.entry_list_id
     INNER JOIN experiment.plot AS plot
         ON plot.entry_id = ent.id
     INNER JOIN tenant.person AS person
         ON person.username = 'nicola.costa'
WHERE
    entlist.entry_list_name = 'IRSEA-F2-2021-DS-1 Entry List'
ORDER BY
     plot.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.planting_instruction AS plantinst
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'IRSEA-F2-2021-DS-1 Entry List';