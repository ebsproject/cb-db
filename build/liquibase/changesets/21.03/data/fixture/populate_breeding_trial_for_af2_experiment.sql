--liquibase formatted sql

--changeset postgres:populate_breeding_trial_for_af2_experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-59 Populate breeding trial for AF 2 experiment



INSERT INTO
    experiment.experiment (
        program_id, pipeline_id, stage_id, project_id, experiment_year, season_id, planting_season, experiment_code, experiment_name,
        experiment_type, experiment_sub_type, experiment_sub_sub_type, experiment_design_type, experiment_status,
        steward_id, creator_id, is_void, data_process_id, crop_id
    )
SELECT
    (SELECT id FROM tenant.program WHERE program_code = 'KE') AS program_id,
    (SELECT id FROM tenant.pipeline WHERE pipeline_code = 'GWP_PIPELINE') AS pipeline_id,
    (SELECT id FROM tenant.stage WHERE stage_code = 'AYT') AS stage_id,
    NULL project_id,
    2021 AS experiment_year,
    (SELECT id FROM tenant.season WHERE season_code = 'WS') AS season_id,
    NULL AS planting_season,
    experiment.generate_code('experiment') AS experiment_code,
    'AF2021_002' AS experiment_name,
    'Breeding Trial' AS experiment_type,
    NULL AS experiment_sub_type,
    NULL AS experiment_sub_sub_type,
    'Alpha-lattice' AS experiment_design_type,
    'planted' AS experiment_status,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS steward_id,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS creator_id,
    FALSE AS is_void,
    (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_DATA_PROCESS') AS data_process_id,
    (SELECT id FROM tenant.crop WHERE crop_code = 'MAIZE') AS crop_id
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment
--rollback WHERE
--rollback     experiment_name = 'AF2021_002'
--rollback ;



--changeset postgres:populate_breeding_trial_for_af2_in_tenant.protocol context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-59 Populate breeding trial for AF2 experiment in tenant.protocol



INSERT INTO
    tenant.protocol (
        protocol_code, protocol_name, protocol_type, program_id, creator_id
    )
VALUES 
    ('TRAIT_PROTOCOL_TMP_AF2BT','Trait Protocol Tmp AF2BT','trait',(SELECT id FROM tenant.program WHERE program_code='KE'), (SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ('MANAGEMENT_PROTOCOL_TMP_AF2BT','Management Protocol Tmp AF2BT','management',(SELECT id FROM tenant.program WHERE program_code='KE'), (SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ('PLANTING_PROTOCOL_TMP_AF2BT','Planting Protocol Tmp AF2BT','planting',(SELECT id FROM tenant.program WHERE program_code='KE'), (SELECT id FROM tenant.person WHERE username = 'nicola.costa'));



--rollback DELETE FROM tenant.protocol WHERE protocol_code IN ('TRAIT_PROTOCOL_TMP_AF2BT','MANAGEMENT_PROTOCOL_TMP_AF2BT','PLANTING_PROTOCOL_TMP_AF2BT');



--changeset postgres:populate_breeding_trial_for_af2_in_experiment.experiment_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-59 Populate breeding trial for AF2 experiment in experiment.experiment_data



INSERT INTO
    experiment.experiment_data (
        experiment_id, variable_id, data_value, data_qc_code, protocol_id, creator_id
    )
SELECT
    exp.id AS experiment_id,
    (SELECT id FROM master.variable WHERE abbrev=t.variable_abbrev) AS variable_id,
    t.data_value AS data_value,
    t.data_qc_code AS data_qc_code,
    (SELECT id FROM tenant.protocol WHERE protocol_name=t.protocol_name) AS protocol_id,
    psn.id AS creator_id
FROM
    (
        VALUES
            ('TRAIT_PROTOCOL_LIST_ID','1453','N','Trait Protocol Tmp AF2BT'),
            ('ESTABLISHMENT','direct seeded','N','Planting Protocol Tmp AF2BT'),
            ('PLANTING_TYPE','Flat','N','Planting Protocol Tmp AF2BT'),
            ('PLOT_TYPE','Direct_seeded_unknown','N','Planting Protocol Tmp AF2BT'),
            ('ROWS_PER_PLOT_CONT','2','N','Planting Protocol Tmp AF2BT'),
            ('DIST_BET_ROWS','0.8','N','Planting Protocol Tmp AF2BT'),
            ('PLOT_WIDTH','1.6','N','Planting Protocol Tmp AF2BT'),
            ('PLOT_LN','2','N','Planting Protocol Tmp AF2BT'),
            ('PLOT_AREA_2','3.2','N','Planting Protocol Tmp AF2BT'),
            ('ALLEY_LENGTH','0.8','N','Planting Protocol Tmp AF2BT'),
            ('SEEDING_RATE','Normal','N','Planting Protocol Tmp AF2BT'),
            ('PROTOCOL_TARGET_LEVEL','occurrence','N',NULL)
    ) AS t (variable_abbrev, data_value, data_qc_code, protocol_name)
INNER JOIN
    experiment.experiment exp
ON
    exp.experiment_name = 'AF2021_002'
INNER JOIN
    tenant.person psn
ON
    psn.username = 'nicola.costa'



--rollback DELETE FROM experiment.experiment_data WHERE experiment_id = (SELECT id FROM experiment.experiment WHERE experiment_name='AF2021_002');



--changeset postgres:populate_breeding_trial_for_af2_in_experiment.experiment_protocol context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-59 Populate breeding trial for AF2 experiment in experiment.experiment_protocol



INSERT INTO
    experiment.experiment_protocol (
        experiment_id, protocol_id, order_number, creator_id
    )
VALUES
    ((SELECT id FROM experiment.experiment WHERE experiment_name='AF2021_002'),(SELECT id FROM tenant.protocol WHERE protocol_name='Trait Protocol Tmp AF2BT'),1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.experiment WHERE experiment_name='AF2021_002'),(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp AF2BT'),2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
;



--rollback DELETE FROM experiment.experiment_protocol WHERE experiment_id = (SELECT id FROM experiment.experiment WHERE experiment_name='AF2021_002');



--changeset postgres:populate_breeding_trial_for_af2_in_experiment.entry_list context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-59 Populate breeding trial for AF2 in experiment.entry_list



INSERT INTO
    experiment.entry_list (
        entry_list_code, entry_list_name, entry_list_status, experiment_id, creator_id, is_void, entry_list_type
    )
SELECT
    experiment.generate_code('entry_list') AS entry_list_code,
    'KE-AYT-2021-WS Entry List' AS entry_list_name,
    'completed' AS entry_list_status,
    (SELECT id FROM experiment.experiment WHERE experiment_name = 'AF2021_002') AS experiment_id,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS creator_id,
    FALSE AS is_void,
    'entry list' AS entry_list_type
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_list
--rollback WHERE
--rollback     entry_list_name = 'KE-AYT-2021-WS Entry List';



--changeset postgres:populate_breeding_trial_for_af2_in_experiment.entry context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-59 Populate breeding trial for AF2 in experiment.entry



INSERT INTO
   experiment.entry (
       entry_code, entry_number, entry_name, entry_type, entry_class, entry_role, entry_status,
       entry_list_id, germplasm_id, seed_id, creator_id, is_void
   )
SELECT
    t.entry_number AS entry_code,
    t.entry_number,
    t.designation AS entry_name,
    t.entry_type AS entry_type,
    t.entry_class AS entry_class,
    NULL AS entry_role,
    'active' AS entry_status,
    entlist.id AS entry_list_id,
    ge.id AS germplasm_id,
    seed.id AS seed_id,
    person.id AS creator_id,
    FALSE AS is_void
FROM
    (
        VALUES        
            (1,'(LTL812/ABW52)-B-25-1-1-B','ST-KIB-18B-20-5','check',NULL),
            (2,'(LTL812/ABT11)-B-16-1-1-B','ST-KIB-18B-20-36','check',NULL),
            (3,'(LTL812/ABT11)-B-16-1-2-B','ST-KIB-18B-20-37','check',NULL),
            (4,'(LTL812/ABT11)-B-51-1-2-B','ST-KIB-18B-20-39','check',NULL),
            (5,'(LTL812/ABW52)-B-65-1-1-B','ST-KIB-18B-20-11','entry',NULL),
            (6,'(HTL437/RK132)-B-89-1-2-B','ST-KIB-18B-20-73','entry',NULL),
            (7,'(HTL437/ABHB9)-B-3-1-1-B','ST-KIB-18B-20-80','entry',NULL),
            (8,'(HTL437/ABHB9)-B-17-1-2-B','ST-KIB-18B-20-83','entry',NULL),
            (9,'(HTL437/ABHB9)-B-31-1-1-B','ST-KIB-18B-20-84','entry',NULL),
            (10,'(HTL437/ABHB9)-B-31-1-2-B','ST-KIB-18B-20-85','entry',NULL),
            (11,'(HTL437/ABHB9)-B-73-1-2-B','ST-KIB-18B-20-87','entry',NULL),
            (12,'(HTL437/ABHB9)-B-108-1-1-B','ST-KIB-18B-20-90','entry',NULL),
            (13,'(HTL437/ABHB9)-B-119-1-1-B','ST-KIB-18B-20-92','entry',NULL),
            (14,'(HTL437/ABHB9)-B-120-1-1-B','ST-KIB-18B-20-93','entry',NULL),
            (15,'(LTL812/ABW52)-B-65-1-2-B','ST-KIB-18B-20-12','entry',NULL),
            (16,'(HTL437/ABHB9)-B-168-1-1-B','ST-KIB-18B-20-95','entry',NULL),
            (17,'(HTL437/ABHB9)-B-168-1-2-B','ST-KIB-18B-20-96','entry',NULL),
            (18,'(HTL437/ABHB9)-B-183-1-2-B','ST-KIB-18B-20-100','entry',NULL),
            (19,'(HTL437/ABP38)-B-157-1-1-B','ST-KIB-18B-20-103','entry',NULL),
            (20,'(HTL437/ABP38)-B-157-1-2-B','ST-KIB-18B-20-104','entry',NULL),
            (21,'(HTL437/ABP38)-B-178-1-1-B','ST-KIB-18B-20-105','entry',NULL),
            (22,'(HTL437/ABP38)-B-178-1-2-B','ST-KIB-18B-20-106','entry',NULL),
            (23,'(HTL437/ABP38)-B-188-1-2-B','ST-KIB-18B-20-108','entry',NULL),
            (24,'(HTL437/ABP38)-B-189-1-2-B','ST-KIB-18B-20-110','entry',NULL),
            (25,'(HTL437/ABW52//HTL437)-96-1-1-1-B','ST-KIB-18B-20-111','entry',NULL),
            (26,'(LTL812/ABW52)-B-74-1-2-B','ST-KIB-18B-20-14','entry',NULL),
            (27,'(HTL437/ABW52//HTL437)-96-1-1-2-B','ST-KIB-18B-20-113','entry',NULL),
            (28,'(HTL437/ABW52//HTL437)-98-1-1-1-B','ST-KIB-18B-20-114','entry',NULL),
            (29,'(HTL437/ABW52//HTL437)-98-1-1-2-B','ST-KIB-18B-20-115','entry',NULL),
            (30,'(LTL812/ABW52)-B-113-1-2-B','ST-KIB-18B-20-16','entry',NULL),
            (31,'(LTL812/ABW52)-B-124-1-2-B','ST-KIB-18B-20-18','entry',NULL),
            (32,'(LTL812/RK198)-B-53-1-2-B','ST-KIB-18B-20-25','entry',NULL),
            (33,'(LTL812/RK198)-B-112-1-2-B','ST-KIB-18B-20-33','entry',NULL),
            (34,'(LTL812/ABT11)-B-8-1-2-B','ST-KIB-18B-20-35','entry',NULL),
            (35,'(ABDHL0089/ABLTI0136)@220-B','WE-KIB-17B-22-411','entry',NULL),
            (36,'(ABLTI0139/ABDHL120918)@95-B','WE-KIB-17B-26-374','entry',NULL),
            (37,'(ABLTI0139/ABDHL120918)@246-B','WE-KIB-17B-26-537','entry',NULL),
            (38,'(ABLTI0139/ABDHL120918)@373-B','WE-KIB-17B-26-673','entry',NULL),
            (39,'(ABLTI0139/ABDHL120918)@393-B','WE-KIB-17B-26-696','entry',NULL),
            (40,'(ABLTI0139/ABDHL120918)@479-B','WE-KIB-17B-26-786','entry',NULL),
            (41,'(ABLTI0139/ABDHL120918)@553-B','WE-KIB-17B-26-866','entry',NULL),
            (42,'(ABLTI0139/ABDHL120918)@653-B','WE-KIB-17B-26-972','entry',NULL),
            (43,'(ABLTI0139/ABDHL120918)@299-B','WE-KIB-17B-26-593','entry',NULL),
            (44,'CML547','WE-KIB-18A-1-531','entry',NULL),
            (45,'((CML537/KS523-5)-B)@10-B','WE-KIB-18A-3-758','entry',NULL),
            (46,'(ABDHL0089/ABDHL120668)@19-B','WE-KIB-17B-22-472','entry',NULL),
            (47,'((CML442/KS23-6)-B)@103-B','WE-KIB-18A-3-719','entry',NULL),
            (48,'CLYN261','WE-KIB-17A-41-38','entry',NULL),
            (49,'CLRCY034','WE-KIB-17A-41-36','entry',NULL),
            (50,'CML495','WE-KIB-17A-71-7','entry',NULL),
            (51,'(CML494/OFP9)-12-2-1-1-2-B*5','WE-KIB-17B-51-4','entry',NULL),
            (52,'(CML494/OFP9)-12-2-1-1-1-B*4','WE-KIB-17B-14-5','entry',NULL),
            (53,'CML463','WE-KIB-18A-7-25','entry',NULL),
            (54,'(ABDHL0089/ABDHL120668)@195-B','WE-KIB-17B-22-656','entry',NULL),
            (55,'(ABDHL0089/ABDHL120918)@52-B','WE-KIB-17B-24-54','entry',NULL),
            (56,'(ABDHL0089/ABDHL120918)@63-B','WE-KIB-17B-24-65','entry',NULL),
            (57,'(ABDHL0089/ABDHL120918)@200-B','WE-KIB-17B-24-208','entry',NULL),
            (58,'(ABLTI0139/CML543)@140-B','WE-KIB-17B-23-859','entry',NULL),
            (59,'(ABLTI0139/ABLTI0335)@14-B','WE-KIB-17B-26-14','entry',NULL),
            (60,'(ABLTI0139/ABLTI0335)@107-B','WE-KIB-17B-26-112','entry',NULL)
    ) AS t (
        entry_number, designation, seed_name, entry_type, entry_class
    )
    INNER JOIN germplasm.germplasm AS ge
        ON t.designation = ge.designation
    INNER JOIN germplasm.seed AS seed
        ON ge.id = seed.germplasm_id
        AND t.seed_name = seed.seed_name
    INNER JOIN experiment.entry_list as entlist
        ON entlist.entry_list_name = 'KE-AYT-2021-WS Entry List'
    INNER JOIN tenant.person AS person
        ON person.username = 'nicola.costa'
ORDER BY
    t.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry AS e
--rollback USING
--rollback     experiment.entry_list AS el
--rollback WHERE
--rollback     e.entry_list_id = el.id
--rollback     AND el.entry_list_name = 'KE-AYT-2021-WS Entry List'
--rollback ;



--changeset postgres:populate_breeding_trial_for_af2_in_experiment.occurrence context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-59 Populate breeding trial fo AF2 in experiment.occurrence



INSERT INTO
    experiment.occurrence (
        occurrence_code,
        occurrence_name,
        occurrence_status,
        experiment_id,
        site_id,
        field_id,
        rep_count,
        occurrence_number,
        creator_id
    )
VALUES 
    ('KE-MZ_ZA_NI-AYT-2021-WS-1','AF2021_002#OCC-01','planted',(SELECT id FROM experiment.experiment WHERE experiment_name='AF2021_002'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='Nicoadala, Zambezia, Mozambique'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name=''),NULL,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ('KE-BR-AYT-2021-WS-1','AF2021_002#OCC-02','planted',(SELECT id FROM experiment.experiment WHERE experiment_name='AF2021_002'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='Brazil'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name=''),NULL,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ('KE-KE-AYT-2021-WS-1','AF2021_002#OCC-03','planted',(SELECT id FROM experiment.experiment WHERE experiment_name='AF2021_002'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='Kenya'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name=''),NULL,3,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
    ;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence
--rollback WHERE
--rollback     occurrence_name IN ('AF2021_002#OCC-01','AF2021_002#OCC-02','AF2021_002#OCC-03')
--rollback ;



-- --changeset postgres:populate_rice_breeding_trial_in_place.geospatial_object context:fixture splitStatements:false rollbackSplitStatements:false
-- --comment: DB-59 Populate rice breeding trial in place.geospatial_object



-- INSERT INTO
--     place.geospatial_object (
--         geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, parent_geospatial_object_id, root_geospatial_object_id
--     )
-- VALUES
--     (
--         place.generate_code('geospatial_object'), 
--         'IRSEA-AYT-2021-DS-1_LOC1', 
--         'planting area', 
--         'breeding_location', 
--         '1', 
--         (SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRI_FARMS'), 
--         (SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRIHQ')
--     )
-- ;



-- -- revert changes
-- --rollback DELETE FROM
-- --rollback     place.geospatial_object
-- --rollback WHERE
-- --rollback     geospatial_object_name = 'IRSEA-AYT-2021-DS-1_LOC1'
-- --rollback ;



--changeset postgres:populate_breeding_trial_for_af2_in_experiment.location context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-59 Populate breeding trial for AF2 in experiment.location



INSERT INTO
   experiment.location (
       location_code, location_name, location_status, location_type,
       location_year, season_id, location_number, site_id, field_id,
       steward_id, geospatial_object_id, creator_id
   )
VALUES
    ('LOC-2021-WS-11','MZ_001','planted','planting area',2021,(SELECT id FROM tenant.season WHERE season_code='WS'),11,(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='Nicoadala, Zambezia, Mozambique'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name=''),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='Nicoadala, Zambezia, Mozambique'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ('LOC-2021-WS-12','KE_001','planted','planting area',2021,(SELECT id FROM tenant.season WHERE season_code='WS'),12,(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='Kenya'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name=''),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='Kenya'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ('LOC-2021-WS-13','BRA_001','planted','planting area',2021,(SELECT id FROM tenant.season WHERE season_code='WS'),13,(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='Brazil'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name=''),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='Brazil'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location
--rollback WHERE
--rollback     location_name IN ('MZ_001','KE_001','BRA_001')
--rollback ;



--changeset postgres:populate_breeding_trial_for_af2_in_experiment.location_occurrence_group context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-59 Populate breeding trial for AF2 in experiment.location_occurrence_group



INSERT INTO 
    experiment.location_occurrence_group (
        location_id,occurrence_id,order_number,creator_id
    )
VALUES
    ((SELECT id FROM experiment.location WHERE location_name='MZ_001'),(SELECT id FROM experiment.occurrence WHERE occurrence_name='AF2021_002#OCC-01'),1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.location WHERE location_name='KE_001'),(SELECT id FROM experiment.occurrence WHERE occurrence_name='AF2021_002#OCC-03'),1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.location WHERE location_name='BRA_001'),(SELECT id FROM experiment.occurrence WHERE occurrence_name='AF2021_002#OCC-02'),1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
;



--rollback DELETE FROM 
--rollback     experiment.location_occurrence_group
--rollback WHERE
--rollback     location_id IN (SELECT id FROM experiment.location WHERE location_name IN ('MZ_001','KE_001','BRA_001'))
--rollback AND
--rollback     occurrence_id IN (SELECT id FROM experiment.occurrence WHERE occurrence_name IN ('AF2021_002#OCC-01','AF2021_002#OCC-03','AF2021_002#OCC-02'));



--changeset postgres:populate_breeding_trial_for_af2_in_experiment.plot context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-59 Populate breeding trial for AF2 in experiment.plot



--set 1
INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, pa_x, pa_y, field_x, field_y,
       block_number, plot_status, plot_qc_code, creator_id, harvest_status
   )
SELECT
    occ.id AS occurrence_id,
    loc.id AS location_id,
    ent.id AS entry_id,
    t.plot_code AS plot_code,
    t.plot_number AS plot_number,
    'plotType' AS plot_type,
    t.rep AS rep,
    t.design_x AS design_x,
    t.design_y AS design_y,
    t.pa_x AS pa_x,
    t.pa_y AS pa_y, 
    t.field_x AS field_x,  
    t.field_y AS field_y,
    t.block_number AS block_number,
    'active' AS plot_status,
    t.plot_qc_code AS plot_qc_code,
    person.id AS creator_id,
    'NO_HARVEST' AS harvest_status
FROM
    (
        VALUES  
            ('360',180,3,44,'CML547',18,10,18,10,18,10,NULL,10),
            ('359',179,3,60,'(ABLTI0139/ABLTI0335)@107-B',17,10,17,10,17,10,NULL,10),
            ('358',178,3,57,'(ABDHL0089/ABDHL120918)@200-B',16,10,16,10,16,10,NULL,10),
            ('357',177,3,39,'(ABLTI0139/ABDHL120918)@393-B',15,10,15,10,15,10,NULL,10),
            ('356',176,3,52,'(CML494/OFP9)-12-2-1-1-1-B*4',14,10,14,10,14,10,NULL,10),
            ('355',175,3,9,'(HTL437/ABHB9)-B-31-1-1-B',13,10,13,10,13,10,NULL,10),
            ('354',174,3,18,'(HTL437/ABHB9)-B-183-1-2-B',18,9,18,9,18,9,NULL,9),
            ('353',173,3,59,'(ABLTI0139/ABLTI0335)@14-B',17,9,17,9,17,9,NULL,9),
            ('352',172,3,10,'(HTL437/ABHB9)-B-31-1-2-B',16,9,16,9,16,9,NULL,9),
            ('351',171,3,40,'(ABLTI0139/ABDHL120918)@479-B',15,9,15,9,15,9,NULL,9),
            ('350',170,3,11,'(HTL437/ABHB9)-B-73-1-2-B',14,9,14,9,14,9,NULL,9),
            ('349',169,3,45,'((CML537/KS523-5)-B)@10-B',13,9,13,9,13,9,NULL,9),
            ('348',168,3,33,'(LTL812/RK198)-B-112-1-2-B',18,8,18,8,18,8,NULL,8),
            ('347',167,3,53,'CML463',17,8,17,8,17,8,NULL,8),
            ('346',166,3,50,'CML495',16,8,16,8,16,8,NULL,8),
            ('345',165,3,35,'(ABDHL0089/ABLTI0136)@220-B',15,8,15,8,15,8,NULL,8),
            ('344',164,3,32,'(LTL812/RK198)-B-53-1-2-B',14,8,14,8,14,8,NULL,8),
            ('343',163,3,23,'(HTL437/ABP38)-B-188-1-2-B',13,8,13,8,13,8,NULL,8),
            ('342',162,3,15,'(LTL812/ABW52)-B-65-1-2-B',18,7,18,7,18,7,NULL,7),
            ('341',161,3,17,'(HTL437/ABHB9)-B-168-1-2-B',17,7,17,7,17,7,NULL,7),
            ('340',160,3,12,'(HTL437/ABHB9)-B-108-1-1-B',16,7,16,7,16,7,NULL,7),
            ('339',159,3,54,'(ABDHL0089/ABDHL120668)@195-B',15,7,15,7,15,7,NULL,7),
            ('338',158,3,47,'((CML442/KS23-6)-B)@103-B',14,7,14,7,14,7,NULL,7),
            ('337',157,3,46,'(ABDHL0089/ABDHL120668)@19-B',13,7,13,7,13,7,NULL,7),
            ('336',156,3,2,'(LTL812/ABT11)-B-16-1-1-B',18,6,18,6,18,6,NULL,6),
            ('335',155,3,20,'(HTL437/ABP38)-B-157-1-2-B',17,6,17,6,17,6,NULL,6),
            ('334',154,3,1,'(LTL812/ABW52)-B-25-1-1-B',16,6,16,6,16,6,NULL,6),
            ('333',153,3,22,'(HTL437/ABP38)-B-178-1-2-B',15,6,15,6,15,6,NULL,6),
            ('332',152,3,30,'(LTL812/ABW52)-B-113-1-2-B',14,6,14,6,14,6,NULL,6),
            ('331',151,3,16,'(HTL437/ABHB9)-B-168-1-1-B',13,6,13,6,13,6,NULL,6),
            ('330',150,3,7,'(HTL437/ABHB9)-B-3-1-1-B',18,5,18,5,18,5,NULL,5),
            ('329',149,3,36,'(ABLTI0139/ABDHL120918)@95-B',17,5,17,5,17,5,NULL,5),
            ('328',148,3,24,'(HTL437/ABP38)-B-189-1-2-B',16,5,16,5,16,5,NULL,5),
            ('327',147,3,14,'(HTL437/ABHB9)-B-120-1-1-B',15,5,15,5,15,5,NULL,5),
            ('326',146,3,5,'(LTL812/ABW52)-B-65-1-1-B',14,5,14,5,14,5,NULL,5),
            ('325',145,3,6,'(HTL437/RK132)-B-89-1-2-B',13,5,13,5,13,5,NULL,5),
            ('324',144,3,8,'(HTL437/ABHB9)-B-17-1-2-B',18,4,18,4,18,4,NULL,4),
            ('323',143,3,48,'CLYN261',17,4,17,4,17,4,NULL,4),
            ('322',142,3,21,'(HTL437/ABP38)-B-178-1-1-B',16,4,16,4,16,4,NULL,4),
            ('321',141,3,31,'(LTL812/ABW52)-B-124-1-2-B',15,4,15,4,15,4,NULL,4),
            ('320',140,3,3,'(LTL812/ABT11)-B-16-1-2-B',14,4,14,4,14,4,NULL,4),
            ('319',139,3,58,'(ABLTI0139/CML543)@140-B',13,4,13,4,13,4,NULL,4),
            ('318',138,3,38,'(ABLTI0139/ABDHL120918)@373-B',18,3,18,3,18,3,NULL,3),
            ('317',137,3,55,'(ABDHL0089/ABDHL120918)@52-B',17,3,17,3,17,3,NULL,3),
            ('316',136,3,19,'(HTL437/ABP38)-B-157-1-1-B',16,3,16,3,16,3,NULL,3),
            ('315',135,3,27,'(HTL437/ABW52//HTL437)-96-1-1-2-B',15,3,15,3,15,3,NULL,3),
            ('314',134,3,43,'(ABLTI0139/ABDHL120918)@299-B',14,3,14,3,14,3,NULL,3),
            ('313',133,3,4,'(LTL812/ABT11)-B-51-1-2-B',13,3,13,3,13,3,NULL,3),
            ('312',132,3,49,'CLRCY034',18,2,18,2,18,2,NULL,2),
            ('311',131,3,25,'(HTL437/ABW52//HTL437)-96-1-1-1-B',17,2,17,2,17,2,NULL,2),
            ('310',130,3,26,'(LTL812/ABW52)-B-74-1-2-B',16,2,16,2,16,2,NULL,2),
            ('309',129,3,42,'(ABLTI0139/ABDHL120918)@653-B',15,2,15,2,15,2,NULL,2),
            ('308',128,3,37,'(ABLTI0139/ABDHL120918)@246-B',14,2,14,2,14,2,NULL,2),
            ('307',127,3,29,'(HTL437/ABW52//HTL437)-98-1-1-2-B',13,2,13,2,13,2,NULL,2),
            ('306',126,3,51,'(CML494/OFP9)-12-2-1-1-2-B*5',18,1,18,1,18,1,NULL,1),
            ('305',125,3,13,'(HTL437/ABHB9)-B-119-1-1-B',17,1,17,1,17,1,NULL,1),
            ('304',124,3,28,'(HTL437/ABW52//HTL437)-98-1-1-1-B',16,1,16,1,16,1,NULL,1),
            ('303',123,3,34,'(LTL812/ABT11)-B-8-1-2-B',15,1,15,1,15,1,NULL,1),
            ('302',122,3,56,'(ABDHL0089/ABDHL120918)@63-B',14,1,14,1,14,1,NULL,1),
            ('301',121,3,41,'(ABLTI0139/ABDHL120918)@553-B',13,1,13,1,13,1,NULL,1),
            ('260',120,2,56,'(ABDHL0089/ABDHL120918)@63-B',12,10,12,10,12,10,NULL,10),
            ('259',119,2,45,'((CML537/KS523-5)-B)@10-B',11,10,11,10,11,10,NULL,10),
            ('258',118,2,5,'(LTL812/ABW52)-B-65-1-1-B',10,10,10,10,10,10,NULL,10),
            ('257',117,2,46,'(ABDHL0089/ABDHL120668)@19-B',9,10,9,10,9,10,NULL,10),
            ('256',116,2,40,'(ABLTI0139/ABDHL120918)@479-B',8,10,8,10,8,10,NULL,10),
            ('255',115,2,19,'(HTL437/ABP38)-B-157-1-1-B',7,10,7,10,7,10,NULL,10),
            ('254',114,2,44,'CML547',12,9,12,9,12,9,NULL,9),
            ('253',113,2,43,'(ABLTI0139/ABDHL120918)@299-B',11,9,11,9,11,9,NULL,9),
            ('252',112,2,21,'(HTL437/ABP38)-B-178-1-1-B',10,9,10,9,10,9,NULL,9),
            ('251',111,2,22,'(HTL437/ABP38)-B-178-1-2-B',9,9,9,9,9,9,NULL,9),
            ('250',110,2,23,'(HTL437/ABP38)-B-188-1-2-B',8,9,8,9,8,9,NULL,9),
            ('249',109,2,53,'CML463',7,9,7,9,7,9,NULL,9),
            ('248',108,2,25,'(HTL437/ABW52//HTL437)-96-1-1-1-B',12,8,12,8,12,8,NULL,8),
            ('247',107,2,18,'(HTL437/ABHB9)-B-183-1-2-B',11,8,11,8,11,8,NULL,8),
            ('246',106,2,54,'(ABDHL0089/ABDHL120668)@195-B',10,8,10,8,10,8,NULL,8),
            ('245',105,2,10,'(HTL437/ABHB9)-B-31-1-2-B',9,8,9,8,9,8,NULL,8),
            ('244',104,2,48,'CLYN261',8,8,8,8,8,8,NULL,8),
            ('243',103,2,2,'(LTL812/ABT11)-B-16-1-1-B',7,8,7,8,7,8,NULL,8),
            ('242',102,2,4,'(LTL812/ABT11)-B-51-1-2-B',12,7,12,7,12,7,NULL,7),
            ('241',101,2,60,'(ABLTI0139/ABLTI0335)@107-B',11,7,11,7,11,7,NULL,7),
            ('240',100,2,16,'(HTL437/ABHB9)-B-168-1-1-B',10,7,10,7,10,7,NULL,7),
            ('239',99,2,37,'(ABLTI0139/ABDHL120918)@246-B',9,7,9,7,9,7,NULL,7),
            ('238',98,2,50,'CML495',8,7,8,7,8,7,NULL,7),
            ('237',97,2,34,'(LTL812/ABT11)-B-8-1-2-B',7,7,7,7,7,7,NULL,7),
            ('236',96,2,3,'(LTL812/ABT11)-B-16-1-2-B',12,6,12,6,12,6,NULL,6),
            ('235',95,2,38,'(ABLTI0139/ABDHL120918)@373-B',11,6,11,6,11,6,NULL,6),
            ('234',94,2,7,'(HTL437/ABHB9)-B-3-1-1-B',10,6,10,6,10,6,NULL,6),
            ('233',93,2,41,'(ABLTI0139/ABDHL120918)@553-B',9,6,9,6,9,6,NULL,6),
            ('232',92,2,49,'CLRCY034',8,6,8,6,8,6,NULL,6),
            ('231',91,2,11,'(HTL437/ABHB9)-B-73-1-2-B',7,6,7,6,7,6,NULL,6),
            ('230',90,2,42,'(ABLTI0139/ABDHL120918)@653-B',12,5,12,5,12,5,NULL,5),
            ('229',89,2,58,'(ABLTI0139/CML543)@140-B',11,5,11,5,11,5,NULL,5),
            ('228',88,2,52,'(CML494/OFP9)-12-2-1-1-1-B*4',10,5,10,5,10,5,NULL,5),
            ('227',87,2,12,'(HTL437/ABHB9)-B-108-1-1-B',9,5,9,5,9,5,NULL,5),
            ('226',86,2,13,'(HTL437/ABHB9)-B-119-1-1-B',8,5,8,5,8,5,NULL,5),
            ('225',85,2,35,'(ABDHL0089/ABLTI0136)@220-B',7,5,7,5,7,5,NULL,5),
            ('224',84,2,33,'(LTL812/RK198)-B-112-1-2-B',12,4,12,4,12,4,NULL,4),
            ('223',83,2,57,'(ABDHL0089/ABDHL120918)@200-B',11,4,11,4,11,4,NULL,4),
            ('222',82,2,27,'(HTL437/ABW52//HTL437)-96-1-1-2-B',10,4,10,4,10,4,NULL,4),
            ('221',81,2,36,'(ABLTI0139/ABDHL120918)@95-B',9,4,9,4,9,4,NULL,4),
            ('220',80,2,1,'(LTL812/ABW52)-B-25-1-1-B',8,4,8,4,8,4,NULL,4),
            ('219',79,2,51,'(CML494/OFP9)-12-2-1-1-2-B*5',7,4,7,4,7,4,NULL,4),
            ('218',78,2,6,'(HTL437/RK132)-B-89-1-2-B',12,3,12,3,12,3,NULL,3),
            ('217',77,2,24,'(HTL437/ABP38)-B-189-1-2-B',11,3,11,3,11,3,NULL,3),
            ('216',76,2,29,'(HTL437/ABW52//HTL437)-98-1-1-2-B',10,3,10,3,10,3,NULL,3),
            ('215',75,2,30,'(LTL812/ABW52)-B-113-1-2-B',9,3,9,3,9,3,NULL,3),
            ('214',74,2,31,'(LTL812/ABW52)-B-124-1-2-B',8,3,8,3,8,3,NULL,3),
            ('213',73,2,59,'(ABLTI0139/ABLTI0335)@14-B',7,3,7,3,7,3,NULL,3),
            ('212',72,2,28,'(HTL437/ABW52//HTL437)-98-1-1-1-B',12,2,12,2,12,2,NULL,2),
            ('211',71,2,55,'(ABDHL0089/ABDHL120918)@52-B',11,2,11,2,11,2,NULL,2),
            ('210',70,2,9,'(HTL437/ABHB9)-B-31-1-1-B',10,2,10,2,10,2,NULL,2),
            ('209',69,2,15,'(LTL812/ABW52)-B-65-1-2-B',9,2,9,2,9,2,NULL,2),
            ('208',68,2,32,'(LTL812/RK198)-B-53-1-2-B',8,2,8,2,8,2,NULL,2),
            ('207',67,2,14,'(HTL437/ABHB9)-B-120-1-1-B',7,2,7,2,7,2,NULL,2),
            ('206',66,2,39,'(ABLTI0139/ABDHL120918)@393-B',12,1,12,1,12,1,NULL,1),
            ('205',65,2,17,'(HTL437/ABHB9)-B-168-1-2-B',11,1,11,1,11,1,NULL,1),
            ('204',64,2,26,'(LTL812/ABW52)-B-74-1-2-B',10,1,10,1,10,1,NULL,1),
            ('203',63,2,8,'(HTL437/ABHB9)-B-17-1-2-B',9,1,9,1,9,1,NULL,1),
            ('202',62,2,20,'(HTL437/ABP38)-B-157-1-2-B',8,1,8,1,8,1,NULL,1),
            ('201',61,2,47,'((CML442/KS23-6)-B)@103-B',7,1,7,1,7,1,NULL,1),
            ('160',60,1,23,'(HTL437/ABP38)-B-188-1-2-B',6,10,6,10,6,10,NULL,10),
            ('159',59,1,47,'((CML442/KS23-6)-B)@103-B',5,10,5,10,5,10,NULL,10),
            ('158',58,1,10,'(HTL437/ABHB9)-B-31-1-2-B',4,10,4,10,4,10,NULL,10),
            ('157',57,1,55,'(ABDHL0089/ABDHL120918)@52-B',3,10,3,10,3,10,NULL,10),
            ('156',56,1,42,'(ABLTI0139/ABDHL120918)@653-B',2,10,2,10,2,10,NULL,10),
            ('155',55,1,51,'(CML494/OFP9)-12-2-1-1-2-B*5',1,10,1,10,1,10,NULL,10),
            ('154',54,1,34,'(LTL812/ABT11)-B-8-1-2-B',6,9,6,9,6,9,NULL,9),
            ('153',53,1,38,'(ABLTI0139/ABDHL120918)@373-B',5,9,5,9,5,9,NULL,9),
            ('152',52,1,25,'(HTL437/ABW52//HTL437)-96-1-1-1-B',4,9,4,9,4,9,NULL,9),
            ('151',51,1,31,'(LTL812/ABW52)-B-124-1-2-B',3,9,3,9,3,9,NULL,9),
            ('150',50,1,57,'(ABDHL0089/ABDHL120918)@200-B',2,9,2,9,2,9,NULL,9),
            ('149',49,1,9,'(HTL437/ABHB9)-B-31-1-1-B',1,9,1,9,1,9,NULL,9),
            ('148',48,1,58,'(ABLTI0139/CML543)@140-B',6,8,6,8,6,8,NULL,8),
            ('147',47,1,50,'CML495',5,8,5,8,5,8,NULL,8),
            ('146',46,1,29,'(HTL437/ABW52//HTL437)-98-1-1-2-B',4,8,4,8,4,8,NULL,8),
            ('145',45,1,17,'(HTL437/ABHB9)-B-168-1-2-B',3,8,3,8,3,8,NULL,8),
            ('144',44,1,11,'(HTL437/ABHB9)-B-73-1-2-B',2,8,2,8,2,8,NULL,8),
            ('143',43,1,36,'(ABLTI0139/ABDHL120918)@95-B',1,8,1,8,1,8,NULL,8),
            ('142',42,1,30,'(LTL812/ABW52)-B-113-1-2-B',6,7,6,7,6,7,NULL,7),
            ('141',41,1,26,'(LTL812/ABW52)-B-74-1-2-B',5,7,5,7,5,7,NULL,7),
            ('140',40,1,32,'(LTL812/RK198)-B-53-1-2-B',4,7,4,7,4,7,NULL,7),
            ('139',39,1,56,'(ABDHL0089/ABDHL120918)@63-B',3,7,3,7,3,7,NULL,7),
            ('138',38,1,13,'(HTL437/ABHB9)-B-119-1-1-B',2,7,2,7,2,7,NULL,7),
            ('137',37,1,2,'(LTL812/ABT11)-B-16-1-1-B',1,7,1,7,1,7,NULL,7),
            ('136',36,1,28,'(HTL437/ABW52//HTL437)-98-1-1-1-B',6,6,6,6,6,6,NULL,6),
            ('135',35,1,54,'(ABDHL0089/ABDHL120668)@195-B',5,6,5,6,5,6,NULL,6),
            ('134',34,1,60,'(ABLTI0139/ABLTI0335)@107-B',4,6,4,6,4,6,NULL,6),
            ('133',33,1,27,'(HTL437/ABW52//HTL437)-96-1-1-2-B',3,6,3,6,3,6,NULL,6),
            ('132',32,1,15,'(LTL812/ABW52)-B-65-1-2-B',2,6,2,6,2,6,NULL,6),
            ('131',31,1,5,'(LTL812/ABW52)-B-65-1-1-B',1,6,1,6,1,6,NULL,6),
            ('130',30,1,43,'(ABLTI0139/ABDHL120918)@299-B',6,5,6,5,6,5,NULL,5),
            ('129',29,1,20,'(HTL437/ABP38)-B-157-1-2-B',5,5,5,5,5,5,NULL,5),
            ('128',28,1,52,'(CML494/OFP9)-12-2-1-1-1-B*4',4,5,4,5,4,5,NULL,5),
            ('127',27,1,46,'(ABDHL0089/ABDHL120668)@19-B',3,5,3,5,3,5,NULL,5),
            ('126',26,1,59,'(ABLTI0139/ABLTI0335)@14-B',2,5,2,5,2,5,NULL,5),
            ('125',25,1,49,'CLRCY034',1,5,1,5,1,5,NULL,5),
            ('124',24,1,14,'(HTL437/ABHB9)-B-120-1-1-B',6,4,6,4,6,4,NULL,4),
            ('123',23,1,4,'(LTL812/ABT11)-B-51-1-2-B',5,4,5,4,5,4,NULL,4),
            ('122',22,1,21,'(HTL437/ABP38)-B-178-1-1-B',4,4,4,4,4,4,NULL,4),
            ('121',21,1,18,'(HTL437/ABHB9)-B-183-1-2-B',3,4,3,4,3,4,NULL,4),
            ('120',20,1,41,'(ABLTI0139/ABDHL120918)@553-B',2,4,2,4,2,4,NULL,4),
            ('119',19,1,16,'(HTL437/ABHB9)-B-168-1-1-B',1,4,1,4,1,4,NULL,4),
            ('118',18,1,1,'(LTL812/ABW52)-B-25-1-1-B',6,3,6,3,6,3,NULL,3),
            ('117',17,1,33,'(LTL812/RK198)-B-112-1-2-B',5,3,5,3,5,3,NULL,3),
            ('116',16,1,45,'((CML537/KS523-5)-B)@10-B',4,3,4,3,4,3,NULL,3),
            ('115',15,1,7,'(HTL437/ABHB9)-B-3-1-1-B',3,3,3,3,3,3,NULL,3),
            ('114',14,1,48,'CLYN261',2,3,2,3,2,3,NULL,3),
            ('113',13,1,39,'(ABLTI0139/ABDHL120918)@393-B',1,3,1,3,1,3,NULL,3),
            ('112',12,1,8,'(HTL437/ABHB9)-B-17-1-2-B',6,2,6,2,6,2,NULL,2),
            ('111',11,1,40,'(ABLTI0139/ABDHL120918)@479-B',5,2,5,2,5,2,NULL,2),
            ('110',10,1,12,'(HTL437/ABHB9)-B-108-1-1-B',4,2,4,2,4,2,NULL,2),
            ('109',9,1,6,'(HTL437/RK132)-B-89-1-2-B',3,2,3,2,3,2,NULL,2),
            ('108',8,1,44,'CML547',2,2,2,2,2,2,NULL,2),
            ('107',7,1,53,'CML463',1,2,1,2,1,2,NULL,2),
            ('106',6,1,35,'(ABDHL0089/ABLTI0136)@220-B',6,1,6,1,6,1,NULL,1),
            ('105',5,1,24,'(HTL437/ABP38)-B-189-1-2-B',5,1,5,1,5,1,NULL,1),
            ('104',4,1,3,'(LTL812/ABT11)-B-16-1-2-B',4,1,4,1,4,1,NULL,1),
            ('103',3,1,22,'(HTL437/ABP38)-B-178-1-2-B',3,1,3,1,3,1,NULL,1),
            ('102',2,1,19,'(HTL437/ABP38)-B-157-1-1-B',2,1,2,1,2,1,NULL,1),
            ('101',1,1,37,'(ABLTI0139/ABDHL120918)@246-B',1,1,1,1,1,1,NULL,1)
    ) AS t (
        plot_code, plot_number, rep, entry_number, designation, design_x, design_y, pa_x, pa_y, field_x, field_y, plot_qc_code, block_number
    )
    INNER JOIN experiment.entry AS ent
        ON t.entry_number = ent.entry_number
        AND t.designation = ent.entry_name
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
        AND entlist.entry_list_name = 'KE-AYT-2021-WS Entry List'
    INNER JOIN experiment.occurrence AS occ
        ON occ.occurrence_name = 'AF2021_002#OCC-01'
    INNER JOIN experiment.location AS loc
        ON loc.location_name = 'MZ_001'
    INNER JOIN tenant.person AS person
        ON person.username = 'nicola.costa'
ORDER BY
    t.plot_number
;

--set 2
INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, pa_x, pa_y, field_x, field_y,
       block_number, plot_status, plot_qc_code, creator_id, harvest_status
   )
SELECT
    occ.id AS occurrence_id,
    loc.id AS location_id,
    ent.id AS entry_id,
    t.plot_code AS plot_code,
    t.plot_number AS plot_number,
    'plotType' AS plot_type,
    t.rep AS rep,
    t.design_x AS design_x,
    t.design_y AS design_y,
    t.pa_x AS pa_x,
    t.pa_y AS pa_y, 
    t.field_x AS field_x,  
    t.field_y AS field_y,
    t.block_number AS block_number,
    'active' AS plot_status,
    t.plot_qc_code AS plot_qc_code,
    person.id AS creator_id,
    'NO_HARVEST' AS harvest_status
FROM
    (
        VALUES      
            ('360',180,3,50,'CML495',18,10,18,10,18,10,NULL,10),
            ('359',179,3,39,'(ABLTI0139/ABDHL120918)@393-B',17,10,17,10,17,10,NULL,10),
            ('358',178,3,20,'(HTL437/ABP38)-B-157-1-2-B',16,10,16,10,16,10,NULL,10),
            ('357',177,3,53,'CML463',15,10,15,10,15,10,NULL,10),
            ('356',176,3,37,'(ABLTI0139/ABDHL120918)@246-B',14,10,14,10,14,10,NULL,10),
            ('355',175,3,54,'(ABDHL0089/ABDHL120668)@195-B',13,10,13,10,13,10,NULL,10),
            ('354',174,3,2,'(LTL812/ABT11)-B-16-1-1-B',18,9,18,9,18,9,NULL,9),
            ('353',173,3,27,'(HTL437/ABW52//HTL437)-96-1-1-2-B',17,9,17,9,17,9,NULL,9),
            ('352',172,3,14,'(HTL437/ABHB9)-B-120-1-1-B',16,9,16,9,16,9,NULL,9),
            ('351',171,3,47,'((CML442/KS23-6)-B)@103-B',15,9,15,9,15,9,NULL,9),
            ('350',170,3,19,'(HTL437/ABP38)-B-157-1-1-B',14,9,14,9,14,9,NULL,9),
            ('349',169,3,40,'(ABLTI0139/ABDHL120918)@479-B',13,9,13,9,13,9,NULL,9),
            ('348',168,3,52,'(CML494/OFP9)-12-2-1-1-1-B*4',18,8,18,8,18,8,NULL,8),
            ('347',167,3,6,'(HTL437/RK132)-B-89-1-2-B',17,8,17,8,17,8,NULL,8),
            ('346',166,3,46,'(ABDHL0089/ABDHL120668)@19-B',16,8,16,8,16,8,NULL,8),
            ('345',165,3,11,'(HTL437/ABHB9)-B-73-1-2-B',15,8,15,8,15,8,NULL,8),
            ('344',164,3,26,'(LTL812/ABW52)-B-74-1-2-B',14,8,14,8,14,8,NULL,8),
            ('343',163,3,22,'(HTL437/ABP38)-B-178-1-2-B',13,8,13,8,13,8,NULL,8),
            ('342',162,3,33,'(LTL812/RK198)-B-112-1-2-B',18,7,18,7,18,7,NULL,7),
            ('341',161,3,60,'(ABLTI0139/ABLTI0335)@107-B',17,7,17,7,17,7,NULL,7),
            ('340',160,3,29,'(HTL437/ABW52//HTL437)-98-1-1-2-B',16,7,16,7,16,7,NULL,7),
            ('339',159,3,7,'(HTL437/ABHB9)-B-3-1-1-B',15,7,15,7,15,7,NULL,7),
            ('338',158,3,30,'(LTL812/ABW52)-B-113-1-2-B',14,7,14,7,14,7,NULL,7),
            ('337',157,3,1,'(LTL812/ABW52)-B-25-1-1-B',13,7,13,7,13,7,NULL,7),
            ('336',156,3,34,'(LTL812/ABT11)-B-8-1-2-B',18,6,18,6,18,6,NULL,6),
            ('335',155,3,49,'CLRCY034',17,6,17,6,17,6,NULL,6),
            ('334',154,3,17,'(HTL437/ABHB9)-B-168-1-2-B',16,6,16,6,16,6,NULL,6),
            ('333',153,3,23,'(HTL437/ABP38)-B-188-1-2-B',15,6,15,6,15,6,NULL,6),
            ('332',152,3,10,'(HTL437/ABHB9)-B-31-1-2-B',14,6,14,6,14,6,NULL,6),
            ('331',151,3,41,'(ABLTI0139/ABDHL120918)@553-B',13,6,13,6,13,6,NULL,6),
            ('330',150,3,35,'(ABDHL0089/ABLTI0136)@220-B',18,5,18,5,18,5,NULL,5),
            ('329',149,3,24,'(HTL437/ABP38)-B-189-1-2-B',17,5,17,5,17,5,NULL,5),
            ('328',148,3,18,'(HTL437/ABHB9)-B-183-1-2-B',16,5,16,5,16,5,NULL,5),
            ('327',147,3,57,'(ABDHL0089/ABDHL120918)@200-B',15,5,15,5,15,5,NULL,5),
            ('326',146,3,42,'(ABLTI0139/ABDHL120918)@653-B',14,5,14,5,14,5,NULL,5),
            ('325',145,3,32,'(LTL812/RK198)-B-53-1-2-B',13,5,13,5,13,5,NULL,5),
            ('324',144,3,45,'((CML537/KS523-5)-B)@10-B',18,4,18,4,18,4,NULL,4),
            ('323',143,3,9,'(HTL437/ABHB9)-B-31-1-1-B',17,4,17,4,17,4,NULL,4),
            ('322',142,3,3,'(LTL812/ABT11)-B-16-1-2-B',16,4,16,4,16,4,NULL,4),
            ('321',141,3,21,'(HTL437/ABP38)-B-178-1-1-B',15,4,15,4,15,4,NULL,4),
            ('320',140,3,15,'(LTL812/ABW52)-B-65-1-2-B',14,4,14,4,14,4,NULL,4),
            ('319',139,3,8,'(HTL437/ABHB9)-B-17-1-2-B',13,4,13,4,13,4,NULL,4),
            ('318',138,3,56,'(ABDHL0089/ABDHL120918)@63-B',18,3,18,3,18,3,NULL,3),
            ('317',137,3,31,'(LTL812/ABW52)-B-124-1-2-B',17,3,17,3,17,3,NULL,3),
            ('316',136,3,25,'(HTL437/ABW52//HTL437)-96-1-1-1-B',16,3,16,3,16,3,NULL,3),
            ('315',135,3,4,'(LTL812/ABT11)-B-51-1-2-B',15,3,15,3,15,3,NULL,3),
            ('314',134,3,55,'(ABDHL0089/ABDHL120918)@52-B',14,3,14,3,14,3,NULL,3),
            ('313',133,3,43,'(ABLTI0139/ABDHL120918)@299-B',13,3,13,3,13,3,NULL,3),
            ('312',132,3,58,'(ABLTI0139/CML543)@140-B',18,2,18,2,18,2,NULL,2),
            ('311',131,3,12,'(HTL437/ABHB9)-B-108-1-1-B',17,2,17,2,17,2,NULL,2),
            ('310',130,3,36,'(ABLTI0139/ABDHL120918)@95-B',16,2,16,2,16,2,NULL,2),
            ('309',129,3,59,'(ABLTI0139/ABLTI0335)@14-B',15,2,15,2,15,2,NULL,2),
            ('308',128,3,38,'(ABLTI0139/ABDHL120918)@373-B',14,2,14,2,14,2,NULL,2),
            ('307',127,3,5,'(LTL812/ABW52)-B-65-1-1-B',13,2,13,2,13,2,NULL,2),
            ('306',126,3,16,'(HTL437/ABHB9)-B-168-1-1-B',18,1,18,1,18,1,NULL,1),
            ('305',125,3,28,'(HTL437/ABW52//HTL437)-98-1-1-1-B',17,1,17,1,17,1,NULL,1),
            ('304',124,3,13,'(HTL437/ABHB9)-B-119-1-1-B',16,1,16,1,16,1,NULL,1),
            ('303',123,3,48,'CLYN261',15,1,15,1,15,1,NULL,1),
            ('302',122,3,44,'CML547',14,1,14,1,14,1,NULL,1),
            ('301',121,3,51,'(CML494/OFP9)-12-2-1-1-2-B*5',13,1,13,1,13,1,NULL,1),
            ('260',120,2,27,'(HTL437/ABW52//HTL437)-96-1-1-2-B',12,10,12,10,12,10,NULL,10),
            ('259',119,2,40,'(ABLTI0139/ABDHL120918)@479-B',11,10,11,10,11,10,NULL,10),
            ('258',118,2,30,'(LTL812/ABW52)-B-113-1-2-B',10,10,10,10,10,10,NULL,10),
            ('257',117,2,31,'(LTL812/ABW52)-B-124-1-2-B',9,10,9,10,9,10,NULL,10),
            ('256',116,2,59,'(ABLTI0139/ABLTI0335)@14-B',8,10,8,10,8,10,NULL,10),
            ('255',115,2,17,'(HTL437/ABHB9)-B-168-1-2-B',7,10,7,10,7,10,NULL,10),
            ('254',114,2,25,'(HTL437/ABW52//HTL437)-96-1-1-1-B',12,9,12,9,12,9,NULL,9),
            ('253',113,2,6,'(HTL437/RK132)-B-89-1-2-B',11,9,11,9,11,9,NULL,9),
            ('252',112,2,12,'(HTL437/ABHB9)-B-108-1-1-B',10,9,10,9,10,9,NULL,9),
            ('251',111,2,7,'(HTL437/ABHB9)-B-3-1-1-B',9,9,9,9,9,9,NULL,9),
            ('250',110,2,3,'(LTL812/ABT11)-B-16-1-2-B',8,9,8,9,8,9,NULL,9),
            ('249',109,2,4,'(LTL812/ABT11)-B-51-1-2-B',7,9,7,9,7,9,NULL,9),
            ('248',108,2,54,'(ABDHL0089/ABDHL120668)@195-B',12,8,12,8,12,8,NULL,8),
            ('247',107,2,38,'(ABLTI0139/ABDHL120918)@373-B',11,8,11,8,11,8,NULL,8),
            ('246',106,2,48,'CLYN261',10,8,10,8,10,8,NULL,8),
            ('245',105,2,15,'(LTL812/ABW52)-B-65-1-2-B',9,8,9,8,9,8,NULL,8),
            ('244',104,2,23,'(HTL437/ABP38)-B-188-1-2-B',8,8,8,8,8,8,NULL,8),
            ('243',103,2,33,'(LTL812/RK198)-B-112-1-2-B',7,8,7,8,7,8,NULL,8),
            ('242',102,2,9,'(HTL437/ABHB9)-B-31-1-1-B',12,7,12,7,12,7,NULL,7),
            ('241',101,2,11,'(HTL437/ABHB9)-B-73-1-2-B',11,7,11,7,11,7,NULL,7),
            ('240',100,2,29,'(HTL437/ABW52//HTL437)-98-1-1-2-B',10,7,10,7,10,7,NULL,7),
            ('239',99,2,57,'(ABDHL0089/ABDHL120918)@200-B',9,7,9,7,9,7,NULL,7),
            ('238',98,2,44,'CML547',8,7,8,7,8,7,NULL,7),
            ('237',97,2,20,'(HTL437/ABP38)-B-157-1-2-B',7,7,7,7,7,7,NULL,7),
            ('236',96,2,43,'(ABLTI0139/ABDHL120918)@299-B',12,6,12,6,12,6,NULL,6),
            ('235',95,2,14,'(HTL437/ABHB9)-B-120-1-1-B',11,6,11,6,11,6,NULL,6),
            ('234',94,2,32,'(LTL812/RK198)-B-53-1-2-B',10,6,10,6,10,6,NULL,6),
            ('233',93,2,37,'(ABLTI0139/ABDHL120918)@246-B',9,6,9,6,9,6,NULL,6),
            ('232',92,2,24,'(HTL437/ABP38)-B-189-1-2-B',8,6,8,6,8,6,NULL,6),
            ('231',91,2,5,'(LTL812/ABW52)-B-65-1-1-B',7,6,7,6,7,6,NULL,6),
            ('230',90,2,22,'(HTL437/ABP38)-B-178-1-2-B',12,5,12,5,12,5,NULL,5),
            ('229',89,2,10,'(HTL437/ABHB9)-B-31-1-2-B',11,5,11,5,11,5,NULL,5),
            ('228',88,2,47,'((CML442/KS23-6)-B)@103-B',10,5,10,5,10,5,NULL,5),
            ('227',87,2,8,'(HTL437/ABHB9)-B-17-1-2-B',9,5,9,5,9,5,NULL,5),
            ('226',86,2,56,'(ABDHL0089/ABDHL120918)@63-B',8,5,8,5,8,5,NULL,5),
            ('225',85,2,28,'(HTL437/ABW52//HTL437)-98-1-1-1-B',7,5,7,5,7,5,NULL,5),
            ('224',84,2,55,'(ABDHL0089/ABDHL120918)@52-B',12,4,12,4,12,4,NULL,4),
            ('223',83,2,13,'(HTL437/ABHB9)-B-119-1-1-B',11,4,11,4,11,4,NULL,4),
            ('222',82,2,2,'(LTL812/ABT11)-B-16-1-1-B',10,4,10,4,10,4,NULL,4),
            ('221',81,2,58,'(ABLTI0139/CML543)@140-B',9,4,9,4,9,4,NULL,4),
            ('220',80,2,53,'CML463',8,4,8,4,8,4,NULL,4),
            ('219',79,2,35,'(ABDHL0089/ABLTI0136)@220-B',7,4,7,4,7,4,NULL,4),
            ('218',78,2,51,'(CML494/OFP9)-12-2-1-1-2-B*5',12,3,12,3,12,3,NULL,3),
            ('217',77,2,50,'CML495',11,3,11,3,11,3,NULL,3),
            ('216',76,2,45,'((CML537/KS523-5)-B)@10-B',10,3,10,3,10,3,NULL,3),
            ('215',75,2,46,'(ABDHL0089/ABDHL120668)@19-B',9,3,9,3,9,3,NULL,3),
            ('214',74,2,60,'(ABLTI0139/ABLTI0335)@107-B',8,3,8,3,8,3,NULL,3),
            ('213',73,2,36,'(ABLTI0139/ABDHL120918)@95-B',7,3,7,3,7,3,NULL,3),
            ('212',72,2,34,'(LTL812/ABT11)-B-8-1-2-B',12,2,12,2,12,2,NULL,2),
            ('211',71,2,41,'(ABLTI0139/ABDHL120918)@553-B',11,2,11,2,11,2,NULL,2),
            ('210',70,2,26,'(LTL812/ABW52)-B-74-1-2-B',10,2,10,2,10,2,NULL,2),
            ('209',69,2,39,'(ABLTI0139/ABDHL120918)@393-B',9,2,9,2,9,2,NULL,2),
            ('208',68,2,42,'(ABLTI0139/ABDHL120918)@653-B',8,2,8,2,8,2,NULL,2),
            ('207',67,2,16,'(HTL437/ABHB9)-B-168-1-1-B',7,2,7,2,7,2,NULL,2),
            ('206',66,2,19,'(HTL437/ABP38)-B-157-1-1-B',12,1,12,1,12,1,NULL,1),
            ('205',65,2,18,'(HTL437/ABHB9)-B-183-1-2-B',11,1,11,1,11,1,NULL,1),
            ('204',64,2,21,'(HTL437/ABP38)-B-178-1-1-B',10,1,10,1,10,1,NULL,1),
            ('203',63,2,49,'CLRCY034',9,1,9,1,9,1,NULL,1),
            ('202',62,2,52,'(CML494/OFP9)-12-2-1-1-1-B*4',8,1,8,1,8,1,NULL,1),
            ('201',61,2,1,'(LTL812/ABW52)-B-25-1-1-B',7,1,7,1,7,1,NULL,1),
            ('160',60,1,28,'(HTL437/ABW52//HTL437)-98-1-1-1-B',6,10,6,10,6,10,NULL,10),
            ('159',59,1,3,'(LTL812/ABT11)-B-16-1-2-B',5,10,5,10,5,10,NULL,10),
            ('158',58,1,57,'(ABDHL0089/ABDHL120918)@200-B',4,10,4,10,4,10,NULL,10),
            ('157',57,1,34,'(LTL812/ABT11)-B-8-1-2-B',3,10,3,10,3,10,NULL,10),
            ('156',56,1,60,'(ABLTI0139/ABLTI0335)@107-B',2,10,2,10,2,10,NULL,10),
            ('155',55,1,55,'(ABDHL0089/ABDHL120918)@52-B',1,10,1,10,1,10,NULL,10),
            ('154',54,1,52,'(CML494/OFP9)-12-2-1-1-1-B*4',6,9,6,9,6,9,NULL,9),
            ('153',53,1,39,'(ABLTI0139/ABDHL120918)@393-B',5,9,5,9,5,9,NULL,9),
            ('152',52,1,41,'(ABLTI0139/ABDHL120918)@553-B',4,9,4,9,4,9,NULL,9),
            ('151',51,1,56,'(ABDHL0089/ABDHL120918)@63-B',3,9,3,9,3,9,NULL,9),
            ('150',50,1,38,'(ABLTI0139/ABDHL120918)@373-B',2,9,2,9,2,9,NULL,9),
            ('149',49,1,29,'(HTL437/ABW52//HTL437)-98-1-1-2-B',1,9,1,9,1,9,NULL,9),
            ('148',48,1,31,'(LTL812/ABW52)-B-124-1-2-B',6,8,6,8,6,8,NULL,8),
            ('147',47,1,2,'(LTL812/ABT11)-B-16-1-1-B',5,8,5,8,5,8,NULL,8),
            ('146',46,1,46,'(ABDHL0089/ABDHL120668)@19-B',4,8,4,8,4,8,NULL,8),
            ('145',45,1,21,'(HTL437/ABP38)-B-178-1-1-B',3,8,3,8,3,8,NULL,8),
            ('144',44,1,32,'(LTL812/RK198)-B-53-1-2-B',2,8,2,8,2,8,NULL,8),
            ('143',43,1,33,'(LTL812/RK198)-B-112-1-2-B',1,8,1,8,1,8,NULL,8),
            ('142',42,1,23,'(HTL437/ABP38)-B-188-1-2-B',6,7,6,7,6,7,NULL,7),
            ('141',41,1,22,'(HTL437/ABP38)-B-178-1-2-B',5,7,5,7,5,7,NULL,7),
            ('140',40,1,19,'(HTL437/ABP38)-B-157-1-1-B',4,7,4,7,4,7,NULL,7),
            ('139',39,1,50,'CML495',3,7,3,7,3,7,NULL,7),
            ('138',38,1,4,'(LTL812/ABT11)-B-51-1-2-B',2,7,2,7,2,7,NULL,7),
            ('137',37,1,24,'(HTL437/ABP38)-B-189-1-2-B',1,7,1,7,1,7,NULL,7),
            ('136',36,1,45,'((CML537/KS523-5)-B)@10-B',6,6,6,6,6,6,NULL,6),
            ('135',35,1,18,'(HTL437/ABHB9)-B-183-1-2-B',5,6,5,6,5,6,NULL,6),
            ('134',34,1,12,'(HTL437/ABHB9)-B-108-1-1-B',4,6,4,6,4,6,NULL,6),
            ('133',33,1,54,'(ABDHL0089/ABDHL120668)@195-B',3,6,3,6,3,6,NULL,6),
            ('132',32,1,16,'(HTL437/ABHB9)-B-168-1-1-B',2,6,2,6,2,6,NULL,6),
            ('131',31,1,47,'((CML442/KS23-6)-B)@103-B',1,6,1,6,1,6,NULL,6),
            ('130',30,1,20,'(HTL437/ABP38)-B-157-1-2-B',6,5,6,5,6,5,NULL,5),
            ('129',29,1,30,'(LTL812/ABW52)-B-113-1-2-B',5,5,5,5,5,5,NULL,5),
            ('128',28,1,49,'CLRCY034',4,5,4,5,4,5,NULL,5),
            ('127',27,1,14,'(HTL437/ABHB9)-B-120-1-1-B',3,5,3,5,3,5,NULL,5),
            ('126',26,1,15,'(LTL812/ABW52)-B-65-1-2-B',2,5,2,5,2,5,NULL,5),
            ('125',25,1,36,'(ABLTI0139/ABDHL120918)@95-B',1,5,1,5,1,5,NULL,5),
            ('124',24,1,8,'(HTL437/ABHB9)-B-17-1-2-B',6,4,6,4,6,4,NULL,4),
            ('123',23,1,6,'(HTL437/RK132)-B-89-1-2-B',5,4,5,4,5,4,NULL,4),
            ('122',22,1,13,'(HTL437/ABHB9)-B-119-1-1-B',4,4,4,4,4,4,NULL,4),
            ('121',21,1,59,'(ABLTI0139/ABLTI0335)@14-B',3,4,3,4,3,4,NULL,4),
            ('120',20,1,43,'(ABLTI0139/ABDHL120918)@299-B',2,4,2,4,2,4,NULL,4),
            ('119',19,1,42,'(ABLTI0139/ABDHL120918)@653-B',1,4,1,4,1,4,NULL,4),
            ('118',18,1,1,'(LTL812/ABW52)-B-25-1-1-B',6,3,6,3,6,3,NULL,3),
            ('117',17,1,5,'(LTL812/ABW52)-B-65-1-1-B',5,3,5,3,5,3,NULL,3),
            ('116',16,1,26,'(LTL812/ABW52)-B-74-1-2-B',4,3,4,3,4,3,NULL,3),
            ('115',15,1,58,'(ABLTI0139/CML543)@140-B',3,3,3,3,3,3,NULL,3),
            ('114',14,1,10,'(HTL437/ABHB9)-B-31-1-2-B',2,3,2,3,2,3,NULL,3),
            ('113',13,1,9,'(HTL437/ABHB9)-B-31-1-1-B',1,3,1,3,1,3,NULL,3),
            ('112',12,1,27,'(HTL437/ABW52//HTL437)-96-1-1-2-B',6,2,6,2,6,2,NULL,2),
            ('111',11,1,44,'CML547',5,2,5,2,5,2,NULL,2),
            ('110',10,1,53,'CML463',4,2,4,2,4,2,NULL,2),
            ('109',9,1,51,'(CML494/OFP9)-12-2-1-1-2-B*5',3,2,3,2,3,2,NULL,2),
            ('108',8,1,7,'(HTL437/ABHB9)-B-3-1-1-B',2,2,2,2,2,2,NULL,2),
            ('107',7,1,17,'(HTL437/ABHB9)-B-168-1-2-B',1,2,1,2,1,2,NULL,2),
            ('106',6,1,48,'CLYN261',6,1,6,1,6,1,NULL,1),
            ('105',5,1,37,'(ABLTI0139/ABDHL120918)@246-B',5,1,5,1,5,1,NULL,1),
            ('104',4,1,25,'(HTL437/ABW52//HTL437)-96-1-1-1-B',4,1,4,1,4,1,NULL,1),
            ('103',3,1,11,'(HTL437/ABHB9)-B-73-1-2-B',3,1,3,1,3,1,NULL,1),
            ('102',2,1,35,'(ABDHL0089/ABLTI0136)@220-B',2,1,2,1,2,1,NULL,1),
            ('101',1,1,40,'(ABLTI0139/ABDHL120918)@479-B',1,1,1,1,1,1,NULL,1)
    ) AS t (
        plot_code, plot_number, rep, entry_number, designation, design_x, design_y, pa_x, pa_y, field_x, field_y, plot_qc_code, block_number
    )
    INNER JOIN experiment.entry AS ent
        ON t.entry_number = ent.entry_number
        AND t.designation = ent.entry_name
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
        AND entlist.entry_list_name = 'KE-AYT-2021-WS Entry List'
    INNER JOIN experiment.occurrence AS occ
        ON occ.occurrence_name = 'AF2021_002#OCC-03'
    INNER JOIN experiment.location AS loc
        ON loc.location_name = 'KE_001'
    INNER JOIN tenant.person AS person
        ON person.username = 'nicola.costa'
ORDER BY
    t.plot_number
;

--set 3
INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, pa_x, pa_y, field_x, field_y,
       block_number, plot_status, plot_qc_code, creator_id, harvest_status
   )
SELECT
    occ.id AS occurrence_id,
    loc.id AS location_id,
    ent.id AS entry_id,
    t.plot_code AS plot_code,
    t.plot_number AS plot_number,
    'plotType' AS plot_type,
    t.rep AS rep,
    t.design_x AS design_x,
    t.design_y AS design_y,
    t.pa_x AS pa_x,
    t.pa_y AS pa_y, 
    t.field_x AS field_x,  
    t.field_y AS field_y,
    t.block_number AS block_number,
    'active' AS plot_status,
    t.plot_qc_code AS plot_qc_code,
    person.id AS creator_id,
    'NO_HARVEST' AS harvest_status
FROM
    (
        VALUES         
            ('360',180,3,20,'(HTL437/ABP38)-B-157-1-2-B',18,10,18,10,18,10,NULL,10),
            ('359',179,3,15,'(LTL812/ABW52)-B-65-1-2-B',17,10,17,10,17,10,NULL,10),
            ('358',178,3,13,'(HTL437/ABHB9)-B-119-1-1-B',16,10,16,10,16,10,NULL,10),
            ('357',177,3,6,'(HTL437/RK132)-B-89-1-2-B',15,10,15,10,15,10,NULL,10),
            ('356',176,3,27,'(HTL437/ABW52//HTL437)-96-1-1-2-B',14,10,14,10,14,10,NULL,10),
            ('355',175,3,26,'(LTL812/ABW52)-B-74-1-2-B',13,10,13,10,13,10,NULL,10),
            ('354',174,3,43,'(ABLTI0139/ABDHL120918)@299-B',18,9,18,9,18,9,NULL,9),
            ('353',173,3,23,'(HTL437/ABP38)-B-188-1-2-B',17,9,17,9,17,9,NULL,9),
            ('352',172,3,2,'(LTL812/ABT11)-B-16-1-1-B',16,9,16,9,16,9,NULL,9),
            ('351',171,3,56,'(ABDHL0089/ABDHL120918)@63-B',15,9,15,9,15,9,NULL,9),
            ('350',170,3,52,'(CML494/OFP9)-12-2-1-1-1-B*4',14,9,14,9,14,9,NULL,9),
            ('349',169,3,18,'(HTL437/ABHB9)-B-183-1-2-B',13,9,13,9,13,9,NULL,9),
            ('348',168,3,17,'(HTL437/ABHB9)-B-168-1-2-B',18,8,18,8,18,8,NULL,8),
            ('347',167,3,54,'(ABDHL0089/ABDHL120668)@195-B',17,8,17,8,17,8,NULL,8),
            ('346',166,3,33,'(LTL812/RK198)-B-112-1-2-B',16,8,16,8,16,8,NULL,8),
            ('345',165,3,8,'(HTL437/ABHB9)-B-17-1-2-B',15,8,15,8,15,8,NULL,8),
            ('344',164,3,29,'(HTL437/ABW52//HTL437)-98-1-1-2-B',14,8,14,8,14,8,NULL,8),
            ('343',163,3,51,'(CML494/OFP9)-12-2-1-1-2-B*5',13,8,13,8,13,8,NULL,8),
            ('342',162,3,9,'(HTL437/ABHB9)-B-31-1-1-B',18,7,18,7,18,7,NULL,7),
            ('341',161,3,34,'(LTL812/ABT11)-B-8-1-2-B',17,7,17,7,17,7,NULL,7),
            ('340',160,3,50,'CML495',16,7,16,7,16,7,NULL,7),
            ('339',159,3,48,'CLYN261',15,7,15,7,15,7,NULL,7),
            ('338',158,3,58,'(ABLTI0139/CML543)@140-B',14,7,14,7,14,7,NULL,7),
            ('337',157,3,16,'(HTL437/ABHB9)-B-168-1-1-B',13,7,13,7,13,7,NULL,7),
            ('336',156,3,28,'(HTL437/ABW52//HTL437)-98-1-1-1-B',18,6,18,6,18,6,NULL,6),
            ('335',155,3,10,'(HTL437/ABHB9)-B-31-1-2-B',17,6,17,6,17,6,NULL,6),
            ('334',154,3,30,'(LTL812/ABW52)-B-113-1-2-B',16,6,16,6,16,6,NULL,6),
            ('333',153,3,22,'(HTL437/ABP38)-B-178-1-2-B',15,6,15,6,15,6,NULL,6),
            ('332',152,3,46,'(ABDHL0089/ABDHL120668)@19-B',14,6,14,6,14,6,NULL,6),
            ('331',151,3,41,'(ABLTI0139/ABDHL120918)@553-B',13,6,13,6,13,6,NULL,6),
            ('330',150,3,47,'((CML442/KS23-6)-B)@103-B',18,5,18,5,18,5,NULL,5),
            ('329',149,3,57,'(ABDHL0089/ABDHL120918)@200-B',17,5,17,5,17,5,NULL,5),
            ('328',148,3,49,'CLRCY034',16,5,16,5,16,5,NULL,5),
            ('327',147,3,60,'(ABLTI0139/ABLTI0335)@107-B',15,5,15,5,15,5,NULL,5),
            ('326',146,3,31,'(LTL812/ABW52)-B-124-1-2-B',14,5,14,5,14,5,NULL,5),
            ('325',145,3,25,'(HTL437/ABW52//HTL437)-96-1-1-1-B',13,5,13,5,13,5,NULL,5),
            ('324',144,3,5,'(LTL812/ABW52)-B-65-1-1-B',18,4,18,4,18,4,NULL,4),
            ('323',143,3,3,'(LTL812/ABT11)-B-16-1-2-B',17,4,17,4,17,4,NULL,4),
            ('322',142,3,59,'(ABLTI0139/ABLTI0335)@14-B',16,4,16,4,16,4,NULL,4),
            ('321',141,3,35,'(ABDHL0089/ABLTI0136)@220-B',15,4,15,4,15,4,NULL,4),
            ('320',140,3,4,'(LTL812/ABT11)-B-51-1-2-B',14,4,14,4,14,4,NULL,4),
            ('319',139,3,53,'CML463',13,4,13,4,13,4,NULL,4),
            ('318',138,3,19,'(HTL437/ABP38)-B-157-1-1-B',18,3,18,3,18,3,NULL,3),
            ('317',137,3,24,'(HTL437/ABP38)-B-189-1-2-B',17,3,17,3,17,3,NULL,3),
            ('316',136,3,14,'(HTL437/ABHB9)-B-120-1-1-B',16,3,16,3,16,3,NULL,3),
            ('315',135,3,40,'(ABLTI0139/ABDHL120918)@479-B',15,3,15,3,15,3,NULL,3),
            ('314',134,3,44,'CML547',14,3,14,3,14,3,NULL,3),
            ('313',133,3,32,'(LTL812/RK198)-B-53-1-2-B',13,3,13,3,13,3,NULL,3),
            ('312',132,3,21,'(HTL437/ABP38)-B-178-1-1-B',18,2,18,2,18,2,NULL,2),
            ('311',131,3,11,'(HTL437/ABHB9)-B-73-1-2-B',17,2,17,2,17,2,NULL,2),
            ('310',130,3,45,'((CML537/KS523-5)-B)@10-B',16,2,16,2,16,2,NULL,2),
            ('309',129,3,1,'(LTL812/ABW52)-B-25-1-1-B',15,2,15,2,15,2,NULL,2),
            ('308',128,3,7,'(HTL437/ABHB9)-B-3-1-1-B',14,2,14,2,14,2,NULL,2),
            ('307',127,3,36,'(ABLTI0139/ABDHL120918)@95-B',13,2,13,2,13,2,NULL,2),
            ('306',126,3,55,'(ABDHL0089/ABDHL120918)@52-B',18,1,18,1,18,1,NULL,1),
            ('305',125,3,38,'(ABLTI0139/ABDHL120918)@373-B',17,1,17,1,17,1,NULL,1),
            ('304',124,3,37,'(ABLTI0139/ABDHL120918)@246-B',16,1,16,1,16,1,NULL,1),
            ('303',123,3,39,'(ABLTI0139/ABDHL120918)@393-B',15,1,15,1,15,1,NULL,1),
            ('302',122,3,12,'(HTL437/ABHB9)-B-108-1-1-B',14,1,14,1,14,1,NULL,1),
            ('301',121,3,42,'(ABLTI0139/ABDHL120918)@653-B',13,1,13,1,13,1,NULL,1),
            ('260',120,2,1,'(LTL812/ABW52)-B-25-1-1-B',12,10,12,10,12,10,NULL,10),
            ('259',119,2,18,'(HTL437/ABHB9)-B-183-1-2-B',11,10,11,10,11,10,NULL,10),
            ('258',118,2,14,'(HTL437/ABHB9)-B-120-1-1-B',10,10,10,10,10,10,NULL,10),
            ('257',117,2,17,'(HTL437/ABHB9)-B-168-1-2-B',9,10,9,10,9,10,NULL,10),
            ('256',116,2,48,'CLYN261',8,10,8,10,8,10,NULL,10),
            ('255',115,2,53,'CML463',7,10,7,10,7,10,NULL,10),
            ('254',114,2,27,'(HTL437/ABW52//HTL437)-96-1-1-2-B',12,9,12,9,12,9,NULL,9),
            ('253',113,2,58,'(ABLTI0139/CML543)@140-B',11,9,11,9,11,9,NULL,9),
            ('252',112,2,51,'(CML494/OFP9)-12-2-1-1-2-B*5',10,9,10,9,10,9,NULL,9),
            ('251',111,2,35,'(ABDHL0089/ABLTI0136)@220-B',9,9,9,9,9,9,NULL,9),
            ('250',110,2,46,'(ABDHL0089/ABDHL120668)@19-B',8,9,8,9,8,9,NULL,9),
            ('249',109,2,57,'(ABDHL0089/ABDHL120918)@200-B',7,9,7,9,7,9,NULL,9),
            ('248',108,2,2,'(LTL812/ABT11)-B-16-1-1-B',12,8,12,8,12,8,NULL,8),
            ('247',107,2,45,'((CML537/KS523-5)-B)@10-B',11,8,11,8,11,8,NULL,8),
            ('246',106,2,9,'(HTL437/ABHB9)-B-31-1-1-B',10,8,10,8,10,8,NULL,8),
            ('245',105,2,49,'CLRCY034',9,8,9,8,9,8,NULL,8),
            ('244',104,2,15,'(LTL812/ABW52)-B-65-1-2-B',8,8,8,8,8,8,NULL,8),
            ('243',103,2,19,'(HTL437/ABP38)-B-157-1-1-B',7,8,7,8,7,8,NULL,8),
            ('242',102,2,8,'(HTL437/ABHB9)-B-17-1-2-B',12,7,12,7,12,7,NULL,7),
            ('241',101,2,30,'(LTL812/ABW52)-B-113-1-2-B',11,7,11,7,11,7,NULL,7),
            ('240',100,2,39,'(ABLTI0139/ABDHL120918)@393-B',10,7,10,7,10,7,NULL,7),
            ('239',99,2,32,'(LTL812/RK198)-B-53-1-2-B',9,7,9,7,9,7,NULL,7),
            ('238',98,2,4,'(LTL812/ABT11)-B-51-1-2-B',8,7,8,7,8,7,NULL,7),
            ('237',97,2,13,'(HTL437/ABHB9)-B-119-1-1-B',7,7,7,7,7,7,NULL,7),
            ('236',96,2,16,'(HTL437/ABHB9)-B-168-1-1-B',12,6,12,6,12,6,NULL,6),
            ('235',95,2,54,'(ABDHL0089/ABDHL120668)@195-B',11,6,11,6,11,6,NULL,6),
            ('234',94,2,31,'(LTL812/ABW52)-B-124-1-2-B',10,6,10,6,10,6,NULL,6),
            ('233',93,2,6,'(HTL437/RK132)-B-89-1-2-B',9,6,9,6,9,6,NULL,6),
            ('232',92,2,40,'(ABLTI0139/ABDHL120918)@479-B',8,6,8,6,8,6,NULL,6),
            ('231',91,2,43,'(ABLTI0139/ABDHL120918)@299-B',7,6,7,6,7,6,NULL,6),
            ('230',90,2,22,'(HTL437/ABP38)-B-178-1-2-B',12,5,12,5,12,5,NULL,5),
            ('229',89,2,37,'(ABLTI0139/ABDHL120918)@246-B',11,5,11,5,11,5,NULL,5),
            ('228',88,2,7,'(HTL437/ABHB9)-B-3-1-1-B',10,5,10,5,10,5,NULL,5),
            ('227',87,2,50,'CML495',9,5,9,5,9,5,NULL,5),
            ('226',86,2,52,'(CML494/OFP9)-12-2-1-1-1-B*4',8,5,8,5,8,5,NULL,5),
            ('225',85,2,60,'(ABLTI0139/ABLTI0335)@107-B',7,5,7,5,7,5,NULL,5),
            ('224',84,2,44,'CML547',12,4,12,4,12,4,NULL,4),
            ('223',83,2,11,'(HTL437/ABHB9)-B-73-1-2-B',11,4,11,4,11,4,NULL,4),
            ('222',82,2,33,'(LTL812/RK198)-B-112-1-2-B',10,4,10,4,10,4,NULL,4),
            ('221',81,2,12,'(HTL437/ABHB9)-B-108-1-1-B',9,4,9,4,9,4,NULL,4),
            ('220',80,2,10,'(HTL437/ABHB9)-B-31-1-2-B',8,4,8,4,8,4,NULL,4),
            ('219',79,2,24,'(HTL437/ABP38)-B-189-1-2-B',7,4,7,4,7,4,NULL,4),
            ('218',78,2,36,'(ABLTI0139/ABDHL120918)@95-B',12,3,12,3,12,3,NULL,3),
            ('217',77,2,28,'(HTL437/ABW52//HTL437)-98-1-1-1-B',11,3,11,3,11,3,NULL,3),
            ('216',76,2,34,'(LTL812/ABT11)-B-8-1-2-B',10,3,10,3,10,3,NULL,3),
            ('215',75,2,29,'(HTL437/ABW52//HTL437)-98-1-1-2-B',9,3,9,3,9,3,NULL,3),
            ('214',74,2,26,'(LTL812/ABW52)-B-74-1-2-B',8,3,8,3,8,3,NULL,3),
            ('213',73,2,55,'(ABDHL0089/ABDHL120918)@52-B',7,3,7,3,7,3,NULL,3),
            ('212',72,2,59,'(ABLTI0139/ABLTI0335)@14-B',12,2,12,2,12,2,NULL,2),
            ('211',71,2,42,'(ABLTI0139/ABDHL120918)@653-B',11,2,11,2,11,2,NULL,2),
            ('210',70,2,20,'(HTL437/ABP38)-B-157-1-2-B',10,2,10,2,10,2,NULL,2),
            ('209',69,2,41,'(ABLTI0139/ABDHL120918)@553-B',9,2,9,2,9,2,NULL,2),
            ('208',68,2,38,'(ABLTI0139/ABDHL120918)@373-B',8,2,8,2,8,2,NULL,2),
            ('207',67,2,5,'(LTL812/ABW52)-B-65-1-1-B',7,2,7,2,7,2,NULL,2),
            ('206',66,2,25,'(HTL437/ABW52//HTL437)-96-1-1-1-B',12,1,12,1,12,1,NULL,1),
            ('205',65,2,21,'(HTL437/ABP38)-B-178-1-1-B',11,1,11,1,11,1,NULL,1),
            ('204',64,2,56,'(ABDHL0089/ABDHL120918)@63-B',10,1,10,1,10,1,NULL,1),
            ('203',63,2,3,'(LTL812/ABT11)-B-16-1-2-B',9,1,9,1,9,1,NULL,1),
            ('202',62,2,47,'((CML442/KS23-6)-B)@103-B',8,1,8,1,8,1,NULL,1),
            ('201',61,2,23,'(HTL437/ABP38)-B-188-1-2-B',7,1,7,1,7,1,NULL,1),
            ('160',60,1,19,'(HTL437/ABP38)-B-157-1-1-B',6,10,6,10,6,10,NULL,10),
            ('159',59,1,16,'(HTL437/ABHB9)-B-168-1-1-B',5,10,5,10,5,10,NULL,10),
            ('158',58,1,27,'(HTL437/ABW52//HTL437)-96-1-1-2-B',4,10,4,10,4,10,NULL,10),
            ('157',57,1,49,'CLRCY034',3,10,3,10,3,10,NULL,10),
            ('156',56,1,29,'(HTL437/ABW52//HTL437)-98-1-1-2-B',2,10,2,10,2,10,NULL,10),
            ('155',55,1,56,'(ABDHL0089/ABDHL120918)@63-B',1,10,1,10,1,10,NULL,10),
            ('154',54,1,48,'CLYN261',6,9,6,9,6,9,NULL,9),
            ('153',53,1,4,'(LTL812/ABT11)-B-51-1-2-B',5,9,5,9,5,9,NULL,9),
            ('152',52,1,9,'(HTL437/ABHB9)-B-31-1-1-B',4,9,4,9,4,9,NULL,9),
            ('151',51,1,10,'(HTL437/ABHB9)-B-31-1-2-B',3,9,3,9,3,9,NULL,9),
            ('150',50,1,20,'(HTL437/ABP38)-B-157-1-2-B',2,9,2,9,2,9,NULL,9),
            ('149',49,1,37,'(ABLTI0139/ABDHL120918)@246-B',1,9,1,9,1,9,NULL,9),
            ('148',48,1,36,'(ABLTI0139/ABDHL120918)@95-B',6,8,6,8,6,8,NULL,8),
            ('147',47,1,43,'(ABLTI0139/ABDHL120918)@299-B',5,8,5,8,5,8,NULL,8),
            ('146',46,1,38,'(ABLTI0139/ABDHL120918)@373-B',4,8,4,8,4,8,NULL,8),
            ('145',45,1,50,'CML495',3,8,3,8,3,8,NULL,8),
            ('144',44,1,53,'CML463',2,8,2,8,2,8,NULL,8),
            ('143',43,1,15,'(LTL812/ABW52)-B-65-1-2-B',1,8,1,8,1,8,NULL,8),
            ('142',42,1,24,'(HTL437/ABP38)-B-189-1-2-B',6,7,6,7,6,7,NULL,7),
            ('141',41,1,52,'(CML494/OFP9)-12-2-1-1-1-B*4',5,7,5,7,5,7,NULL,7),
            ('140',40,1,59,'(ABLTI0139/ABLTI0335)@14-B',4,7,4,7,4,7,NULL,7),
            ('139',39,1,23,'(HTL437/ABP38)-B-188-1-2-B',3,7,3,7,3,7,NULL,7),
            ('138',38,1,26,'(LTL812/ABW52)-B-74-1-2-B',2,7,2,7,2,7,NULL,7),
            ('137',37,1,25,'(HTL437/ABW52//HTL437)-96-1-1-1-B',1,7,1,7,1,7,NULL,7),
            ('136',36,1,45,'((CML537/KS523-5)-B)@10-B',6,6,6,6,6,6,NULL,6),
            ('135',35,1,32,'(LTL812/RK198)-B-53-1-2-B',5,6,5,6,5,6,NULL,6),
            ('134',34,1,57,'(ABDHL0089/ABDHL120918)@200-B',4,6,4,6,4,6,NULL,6),
            ('133',33,1,17,'(HTL437/ABHB9)-B-168-1-2-B',3,6,3,6,3,6,NULL,6),
            ('132',32,1,7,'(HTL437/ABHB9)-B-3-1-1-B',2,6,2,6,2,6,NULL,6),
            ('131',31,1,5,'(LTL812/ABW52)-B-65-1-1-B',1,6,1,6,1,6,NULL,6),
            ('130',30,1,41,'(ABLTI0139/ABDHL120918)@553-B',6,5,6,5,6,5,NULL,5),
            ('129',29,1,14,'(HTL437/ABHB9)-B-120-1-1-B',5,5,5,5,5,5,NULL,5),
            ('128',28,1,21,'(HTL437/ABP38)-B-178-1-1-B',4,5,4,5,4,5,NULL,5),
            ('127',27,1,39,'(ABLTI0139/ABDHL120918)@393-B',3,5,3,5,3,5,NULL,5),
            ('126',26,1,35,'(ABDHL0089/ABLTI0136)@220-B',2,5,2,5,2,5,NULL,5),
            ('125',25,1,34,'(LTL812/ABT11)-B-8-1-2-B',1,5,1,5,1,5,NULL,5),
            ('124',24,1,47,'((CML442/KS23-6)-B)@103-B',6,4,6,4,6,4,NULL,4),
            ('123',23,1,33,'(LTL812/RK198)-B-112-1-2-B',5,4,5,4,5,4,NULL,4),
            ('122',22,1,60,'(ABLTI0139/ABLTI0335)@107-B',4,4,4,4,4,4,NULL,4),
            ('121',21,1,58,'(ABLTI0139/CML543)@140-B',3,4,3,4,3,4,NULL,4),
            ('120',20,1,42,'(ABLTI0139/ABDHL120918)@653-B',2,4,2,4,2,4,NULL,4),
            ('119',19,1,6,'(HTL437/RK132)-B-89-1-2-B',1,4,1,4,1,4,NULL,4),
            ('118',18,1,31,'(LTL812/ABW52)-B-124-1-2-B',6,3,6,3,6,3,NULL,3),
            ('117',17,1,3,'(LTL812/ABT11)-B-16-1-2-B',5,3,5,3,5,3,NULL,3),
            ('116',16,1,30,'(LTL812/ABW52)-B-113-1-2-B',4,3,4,3,4,3,NULL,3),
            ('115',15,1,55,'(ABDHL0089/ABDHL120918)@52-B',3,3,3,3,3,3,NULL,3),
            ('114',14,1,2,'(LTL812/ABT11)-B-16-1-1-B',2,3,2,3,2,3,NULL,3),
            ('113',13,1,46,'(ABDHL0089/ABDHL120668)@19-B',1,3,1,3,1,3,NULL,3),
            ('112',12,1,18,'(HTL437/ABHB9)-B-183-1-2-B',6,2,6,2,6,2,NULL,2),
            ('111',11,1,8,'(HTL437/ABHB9)-B-17-1-2-B',5,2,5,2,5,2,NULL,2),
            ('110',10,1,40,'(ABLTI0139/ABDHL120918)@479-B',4,2,4,2,4,2,NULL,2),
            ('109',9,1,11,'(HTL437/ABHB9)-B-73-1-2-B',3,2,3,2,3,2,NULL,2),
            ('108',8,1,22,'(HTL437/ABP38)-B-178-1-2-B',2,2,2,2,2,2,NULL,2),
            ('107',7,1,12,'(HTL437/ABHB9)-B-108-1-1-B',1,2,1,2,1,2,NULL,2),
            ('106',6,1,44,'CML547',6,1,6,1,6,1,NULL,1),
            ('105',5,1,28,'(HTL437/ABW52//HTL437)-98-1-1-1-B',5,1,5,1,5,1,NULL,1),
            ('104',4,1,13,'(HTL437/ABHB9)-B-119-1-1-B',4,1,4,1,4,1,NULL,1),
            ('103',3,1,51,'(CML494/OFP9)-12-2-1-1-2-B*5',3,1,3,1,3,1,NULL,1),
            ('102',2,1,54,'(ABDHL0089/ABDHL120668)@195-B',2,1,2,1,2,1,NULL,1),
            ('101',1,1,1,'(LTL812/ABW52)-B-25-1-1-B',1,1,1,1,1,1,NULL,1)
    ) AS t (
        plot_code, plot_number, rep, entry_number, designation, design_x, design_y, pa_x, pa_y, field_x, field_y, plot_qc_code, block_number
    )
    INNER JOIN experiment.entry AS ent
        ON t.entry_number = ent.entry_number
        AND t.designation = ent.entry_name
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
        AND entlist.entry_list_name = 'KE-AYT-2021-WS Entry List'
    INNER JOIN experiment.occurrence AS occ
        ON occ.occurrence_name = 'AF2021_002#OCC-02'
    INNER JOIN experiment.location AS loc
        ON loc.location_name = 'BRA_001'
    INNER JOIN tenant.person AS person
        ON person.username = 'nicola.costa'
ORDER BY
    t.plot_number
;



-- revert changes OCC
--rollback DELETE FROM
--rollback     experiment.plot AS plot
--rollback USING
--rollback     experiment.occurrence AS occ,
--rollback     experiment.location AS loc
--rollback WHERE
--rollback     plot.occurrence_id = occ.id
--rollback     AND occ.occurrence_name IN ('AF2021_002#OCC-01','AF2021_002#OCC-03', 'AF2021_002#OCC-02')
--rollback     AND loc.location_name IN ('MZ_001','KE_001','BRA_001')
--rollback ;



--changeset postgres:populate_breeding_trial_for_af2_in_experiment.planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-59 Populate breeding trial for AF2 in experiment.planting_instruction



INSERT INTO
   experiment.planting_instruction (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status, entry_id,
       plot_id, germplasm_id, seed_id, creator_id
   )
SELECT
    ent.entry_code,
    ent.entry_number,
    ent.entry_name,
    ent.entry_type,
    ent.entry_role,
    ent.entry_status,
    ent.id AS entry_id,
    plot.id AS plot_id,
    ent.germplasm_id,
    ent.seed_id,
    person.id AS creator_id
FROM
    experiment.entry_list AS entlist
    INNER JOIN experiment.entry AS ent
        ON entlist.id = ent.entry_list_id
     INNER JOIN experiment.plot AS plot
         ON plot.entry_id = ent.id
     INNER JOIN tenant.person AS person
         ON person.username = 'nicola.costa'
WHERE
    entlist.entry_list_name = 'KE-AYT-2021-WS Entry List'
ORDER BY
     plot.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.planting_instruction AS plantinst
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'KE-AYT-2021-WS Entry List';



--changeset postgres:populate_breeding_trial_for_af2_in_experiment.experiment_design context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-59 Populate breeding trial for AF2 in experiment.experiment_design



--set 1
INSERT INTO
   experiment.experiment_design (
       occurrence_id, design_id, plot_id, block_type, block_value,
       block_level_number, creator_id, block_name
   )
SELECT
    occ.id AS occurrence_id,
    t.design_id AS design_id,
    (SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name=t.entry_name AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name=els.entry_list_name)) AND ep.plot_code = t.plot_code AND ep.plot_number = t.plot_number AND ep.occurrence_id = (SELECT id FROM experiment.occurrence WHERE occurrence_name=occ.occurrence_name)) AS plot_id,
    t.block_type AS block_type,
    t.block_value AS block_value,
    t.block_level_number AS block_level_number,
    psn.id AS creator_id,
    t.block_name AS block_name
FROM
    (
        VALUES
            (1,'(ABLTI0139/ABDHL120918)@246-B','101',1,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-157-1-1-B','102',2,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-178-1-2-B','103',3,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABT11)-B-16-1-2-B','104',4,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-189-1-2-B','105',5,'replication block',1,1,'replicate'),
            (1,'(ABDHL0089/ABLTI0136)@220-B','106',6,'replication block',1,1,'replicate'),
            (1,'CML463','107',7,'replication block',1,1,'replicate'),
            (1,'CML547','108',8,'replication block',1,1,'replicate'),
            (1,'(HTL437/RK132)-B-89-1-2-B','109',9,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-108-1-1-B','110',10,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABDHL120918)@479-B','111',11,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-17-1-2-B','112',12,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABDHL120918)@393-B','113',13,'replication block',1,1,'replicate'),
            (1,'CLYN261','114',14,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-3-1-1-B','115',15,'replication block',1,1,'replicate'),
            (1,'((CML537/KS523-5)-B)@10-B','116',16,'replication block',1,1,'replicate'),
            (1,'(LTL812/RK198)-B-112-1-2-B','117',17,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-25-1-1-B','118',18,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-168-1-1-B','119',19,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABDHL120918)@553-B','120',20,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-183-1-2-B','121',21,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-178-1-1-B','122',22,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABT11)-B-51-1-2-B','123',23,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-120-1-1-B','124',24,'replication block',1,1,'replicate'),
            (1,'CLRCY034','125',25,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABLTI0335)@14-B','126',26,'replication block',1,1,'replicate'),
            (1,'(ABDHL0089/ABDHL120668)@19-B','127',27,'replication block',1,1,'replicate'),
            (1,'(CML494/OFP9)-12-2-1-1-1-B*4','128',28,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-157-1-2-B','129',29,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABDHL120918)@299-B','130',30,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-65-1-1-B','131',31,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-65-1-2-B','132',32,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52//HTL437)-96-1-1-2-B','133',33,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABLTI0335)@107-B','134',34,'replication block',1,1,'replicate'),
            (1,'(ABDHL0089/ABDHL120668)@195-B','135',35,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52//HTL437)-98-1-1-1-B','136',36,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABT11)-B-16-1-1-B','137',37,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-119-1-1-B','138',38,'replication block',1,1,'replicate'),
            (1,'(ABDHL0089/ABDHL120918)@63-B','139',39,'replication block',1,1,'replicate'),
            (1,'(LTL812/RK198)-B-53-1-2-B','140',40,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-74-1-2-B','141',41,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-113-1-2-B','142',42,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABDHL120918)@95-B','143',43,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-73-1-2-B','144',44,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-168-1-2-B','145',45,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52//HTL437)-98-1-1-2-B','146',46,'replication block',1,1,'replicate'),
            (1,'CML495','147',47,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/CML543)@140-B','148',48,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-31-1-1-B','149',49,'replication block',1,1,'replicate'),
            (1,'(ABDHL0089/ABDHL120918)@200-B','150',50,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-124-1-2-B','151',51,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52//HTL437)-96-1-1-1-B','152',52,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABDHL120918)@373-B','153',53,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABT11)-B-8-1-2-B','154',54,'replication block',1,1,'replicate'),
            (1,'(CML494/OFP9)-12-2-1-1-2-B*5','155',55,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABDHL120918)@653-B','156',56,'replication block',1,1,'replicate'),
            (1,'(ABDHL0089/ABDHL120918)@52-B','157',57,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-31-1-2-B','158',58,'replication block',1,1,'replicate'),
            (1,'((CML442/KS23-6)-B)@103-B','159',59,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-188-1-2-B','160',60,'replication block',1,1,'replicate'),
            (2,'((CML442/KS23-6)-B)@103-B','201',61,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-157-1-2-B','202',62,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-17-1-2-B','203',63,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-74-1-2-B','204',64,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-168-1-2-B','205',65,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABDHL120918)@393-B','206',66,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-120-1-1-B','207',67,'replication block',2,1,'replicate'),
            (2,'(LTL812/RK198)-B-53-1-2-B','208',68,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-65-1-2-B','209',69,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-31-1-1-B','210',70,'replication block',2,1,'replicate'),
            (2,'(ABDHL0089/ABDHL120918)@52-B','211',71,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52//HTL437)-98-1-1-1-B','212',72,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABLTI0335)@14-B','213',73,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-124-1-2-B','214',74,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-113-1-2-B','215',75,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52//HTL437)-98-1-1-2-B','216',76,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-189-1-2-B','217',77,'replication block',2,1,'replicate'),
            (2,'(HTL437/RK132)-B-89-1-2-B','218',78,'replication block',2,1,'replicate'),
            (2,'(CML494/OFP9)-12-2-1-1-2-B*5','219',79,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-25-1-1-B','220',80,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABDHL120918)@95-B','221',81,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52//HTL437)-96-1-1-2-B','222',82,'replication block',2,1,'replicate'),
            (2,'(ABDHL0089/ABDHL120918)@200-B','223',83,'replication block',2,1,'replicate'),
            (2,'(LTL812/RK198)-B-112-1-2-B','224',84,'replication block',2,1,'replicate'),
            (2,'(ABDHL0089/ABLTI0136)@220-B','225',85,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-119-1-1-B','226',86,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-108-1-1-B','227',87,'replication block',2,1,'replicate'),
            (2,'(CML494/OFP9)-12-2-1-1-1-B*4','228',88,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/CML543)@140-B','229',89,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABDHL120918)@653-B','230',90,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-73-1-2-B','231',91,'replication block',2,1,'replicate'),
            (2,'CLRCY034','232',92,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABDHL120918)@553-B','233',93,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-3-1-1-B','234',94,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABDHL120918)@373-B','235',95,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABT11)-B-16-1-2-B','236',96,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABT11)-B-8-1-2-B','237',97,'replication block',2,1,'replicate'),
            (2,'CML495','238',98,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABDHL120918)@246-B','239',99,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-168-1-1-B','240',100,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABLTI0335)@107-B','241',101,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABT11)-B-51-1-2-B','242',102,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABT11)-B-16-1-1-B','243',103,'replication block',2,1,'replicate'),
            (2,'CLYN261','244',104,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-31-1-2-B','245',105,'replication block',2,1,'replicate'),
            (2,'(ABDHL0089/ABDHL120668)@195-B','246',106,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-183-1-2-B','247',107,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52//HTL437)-96-1-1-1-B','248',108,'replication block',2,1,'replicate'),
            (2,'CML463','249',109,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-188-1-2-B','250',110,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-178-1-2-B','251',111,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-178-1-1-B','252',112,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABDHL120918)@299-B','253',113,'replication block',2,1,'replicate'),
            (2,'CML547','254',114,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-157-1-1-B','255',115,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABDHL120918)@479-B','256',116,'replication block',2,1,'replicate'),
            (2,'(ABDHL0089/ABDHL120668)@19-B','257',117,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-65-1-1-B','258',118,'replication block',2,1,'replicate'),
            (2,'((CML537/KS523-5)-B)@10-B','259',119,'replication block',2,1,'replicate'),
            (2,'(ABDHL0089/ABDHL120918)@63-B','260',120,'replication block',2,1,'replicate'),
            (3,'(ABLTI0139/ABDHL120918)@553-B','301',121,'replication block',3,1,'replicate'),
            (3,'(ABDHL0089/ABDHL120918)@63-B','302',122,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABT11)-B-8-1-2-B','303',123,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52//HTL437)-98-1-1-1-B','304',124,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-119-1-1-B','305',125,'replication block',3,1,'replicate'),
            (3,'(CML494/OFP9)-12-2-1-1-2-B*5','306',126,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52//HTL437)-98-1-1-2-B','307',127,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABDHL120918)@246-B','308',128,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABDHL120918)@653-B','309',129,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-74-1-2-B','310',130,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52//HTL437)-96-1-1-1-B','311',131,'replication block',3,1,'replicate'),
            (3,'CLRCY034','312',132,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABT11)-B-51-1-2-B','313',133,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABDHL120918)@299-B','314',134,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52//HTL437)-96-1-1-2-B','315',135,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-157-1-1-B','316',136,'replication block',3,1,'replicate'),
            (3,'(ABDHL0089/ABDHL120918)@52-B','317',137,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABDHL120918)@373-B','318',138,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/CML543)@140-B','319',139,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABT11)-B-16-1-2-B','320',140,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-124-1-2-B','321',141,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-178-1-1-B','322',142,'replication block',3,1,'replicate'),
            (3,'CLYN261','323',143,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-17-1-2-B','324',144,'replication block',3,1,'replicate'),
            (3,'(HTL437/RK132)-B-89-1-2-B','325',145,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-65-1-1-B','326',146,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-120-1-1-B','327',147,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-189-1-2-B','328',148,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABDHL120918)@95-B','329',149,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-3-1-1-B','330',150,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-168-1-1-B','331',151,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-113-1-2-B','332',152,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-178-1-2-B','333',153,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-25-1-1-B','334',154,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-157-1-2-B','335',155,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABT11)-B-16-1-1-B','336',156,'replication block',3,1,'replicate'),
            (3,'(ABDHL0089/ABDHL120668)@19-B','337',157,'replication block',3,1,'replicate'),
            (3,'((CML442/KS23-6)-B)@103-B','338',158,'replication block',3,1,'replicate'),
            (3,'(ABDHL0089/ABDHL120668)@195-B','339',159,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-108-1-1-B','340',160,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-168-1-2-B','341',161,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-65-1-2-B','342',162,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-188-1-2-B','343',163,'replication block',3,1,'replicate'),
            (3,'(LTL812/RK198)-B-53-1-2-B','344',164,'replication block',3,1,'replicate'),
            (3,'(ABDHL0089/ABLTI0136)@220-B','345',165,'replication block',3,1,'replicate'),
            (3,'CML495','346',166,'replication block',3,1,'replicate'),
            (3,'CML463','347',167,'replication block',3,1,'replicate'),
            (3,'(LTL812/RK198)-B-112-1-2-B','348',168,'replication block',3,1,'replicate'),
            (3,'((CML537/KS523-5)-B)@10-B','349',169,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-73-1-2-B','350',170,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABDHL120918)@479-B','351',171,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-31-1-2-B','352',172,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABLTI0335)@14-B','353',173,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-183-1-2-B','354',174,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-31-1-1-B','355',175,'replication block',3,1,'replicate'),
            (3,'(CML494/OFP9)-12-2-1-1-1-B*4','356',176,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABDHL120918)@393-B','357',177,'replication block',3,1,'replicate'),
            (3,'(ABDHL0089/ABDHL120918)@200-B','358',178,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABLTI0335)@107-B','359',179,'replication block',3,1,'replicate'),
            (3,'CML547','360',180,'replication block',3,1,'replicate'),
            (4,'(ABLTI0139/ABDHL120918)@246-B','101',1,'replication block',1,2,'block'),
            (4,'(HTL437/ABP38)-B-157-1-1-B','102',2,'replication block',1,2,'block'),
            (4,'(HTL437/ABP38)-B-178-1-2-B','103',3,'replication block',1,2,'block'),
            (4,'(LTL812/ABT11)-B-16-1-2-B','104',4,'replication block',1,2,'block'),
            (4,'(HTL437/ABP38)-B-189-1-2-B','105',5,'replication block',1,2,'block'),
            (4,'(ABDHL0089/ABLTI0136)@220-B','106',6,'replication block',1,2,'block'),
            (5,'CML463','107',7,'replication block',2,2,'block'),
            (5,'CML547','108',8,'replication block',2,2,'block'),
            (5,'(HTL437/RK132)-B-89-1-2-B','109',9,'replication block',2,2,'block'),
            (5,'(HTL437/ABHB9)-B-108-1-1-B','110',10,'replication block',2,2,'block'),
            (5,'(ABLTI0139/ABDHL120918)@479-B','111',11,'replication block',2,2,'block'),
            (5,'(HTL437/ABHB9)-B-17-1-2-B','112',12,'replication block',2,2,'block'),
            (6,'(ABLTI0139/ABDHL120918)@393-B','113',13,'replication block',3,2,'block'),
            (6,'CLYN261','114',14,'replication block',3,2,'block'),
            (6,'(HTL437/ABHB9)-B-3-1-1-B','115',15,'replication block',3,2,'block'),
            (6,'((CML537/KS523-5)-B)@10-B','116',16,'replication block',3,2,'block'),
            (6,'(LTL812/RK198)-B-112-1-2-B','117',17,'replication block',3,2,'block'),
            (6,'(LTL812/ABW52)-B-25-1-1-B','118',18,'replication block',3,2,'block'),
            (7,'(HTL437/ABHB9)-B-168-1-1-B','119',19,'replication block',4,2,'block'),
            (7,'(ABLTI0139/ABDHL120918)@553-B','120',20,'replication block',4,2,'block'),
            (7,'(HTL437/ABHB9)-B-183-1-2-B','121',21,'replication block',4,2,'block'),
            (7,'(HTL437/ABP38)-B-178-1-1-B','122',22,'replication block',4,2,'block'),
            (7,'(LTL812/ABT11)-B-51-1-2-B','123',23,'replication block',4,2,'block'),
            (7,'(HTL437/ABHB9)-B-120-1-1-B','124',24,'replication block',4,2,'block'),
            (8,'CLRCY034','125',25,'replication block',5,2,'block'),
            (8,'(ABLTI0139/ABLTI0335)@14-B','126',26,'replication block',5,2,'block'),
            (8,'(ABDHL0089/ABDHL120668)@19-B','127',27,'replication block',5,2,'block'),
            (8,'(CML494/OFP9)-12-2-1-1-1-B*4','128',28,'replication block',5,2,'block'),
            (8,'(HTL437/ABP38)-B-157-1-2-B','129',29,'replication block',5,2,'block'),
            (8,'(ABLTI0139/ABDHL120918)@299-B','130',30,'replication block',5,2,'block'),
            (9,'(LTL812/ABW52)-B-65-1-1-B','131',31,'replication block',6,2,'block'),
            (9,'(LTL812/ABW52)-B-65-1-2-B','132',32,'replication block',6,2,'block'),
            (9,'(HTL437/ABW52//HTL437)-96-1-1-2-B','133',33,'replication block',6,2,'block'),
            (9,'(ABLTI0139/ABLTI0335)@107-B','134',34,'replication block',6,2,'block'),
            (9,'(ABDHL0089/ABDHL120668)@195-B','135',35,'replication block',6,2,'block'),
            (9,'(HTL437/ABW52//HTL437)-98-1-1-1-B','136',36,'replication block',6,2,'block'),
            (10,'(LTL812/ABT11)-B-16-1-1-B','137',37,'replication block',7,2,'block'),
            (10,'(HTL437/ABHB9)-B-119-1-1-B','138',38,'replication block',7,2,'block'),
            (10,'(ABDHL0089/ABDHL120918)@63-B','139',39,'replication block',7,2,'block'),
            (10,'(LTL812/RK198)-B-53-1-2-B','140',40,'replication block',7,2,'block'),
            (10,'(LTL812/ABW52)-B-74-1-2-B','141',41,'replication block',7,2,'block'),
            (10,'(LTL812/ABW52)-B-113-1-2-B','142',42,'replication block',7,2,'block'),
            (11,'(ABLTI0139/ABDHL120918)@95-B','143',43,'replication block',8,2,'block'),
            (11,'(HTL437/ABHB9)-B-73-1-2-B','144',44,'replication block',8,2,'block'),
            (11,'(HTL437/ABHB9)-B-168-1-2-B','145',45,'replication block',8,2,'block'),
            (11,'(HTL437/ABW52//HTL437)-98-1-1-2-B','146',46,'replication block',8,2,'block'),
            (11,'CML495','147',47,'replication block',8,2,'block'),
            (11,'(ABLTI0139/CML543)@140-B','148',48,'replication block',8,2,'block'),
            (12,'(HTL437/ABHB9)-B-31-1-1-B','149',49,'replication block',9,2,'block'),
            (12,'(ABDHL0089/ABDHL120918)@200-B','150',50,'replication block',9,2,'block'),
            (12,'(LTL812/ABW52)-B-124-1-2-B','151',51,'replication block',9,2,'block'),
            (12,'(HTL437/ABW52//HTL437)-96-1-1-1-B','152',52,'replication block',9,2,'block'),
            (12,'(ABLTI0139/ABDHL120918)@373-B','153',53,'replication block',9,2,'block'),
            (12,'(LTL812/ABT11)-B-8-1-2-B','154',54,'replication block',9,2,'block'),
            (13,'(CML494/OFP9)-12-2-1-1-2-B*5','155',55,'replication block',10,2,'block'),
            (13,'(ABLTI0139/ABDHL120918)@653-B','156',56,'replication block',10,2,'block'),
            (13,'(ABDHL0089/ABDHL120918)@52-B','157',57,'replication block',10,2,'block'),
            (13,'(HTL437/ABHB9)-B-31-1-2-B','158',58,'replication block',10,2,'block'),
            (13,'((CML442/KS23-6)-B)@103-B','159',59,'replication block',10,2,'block'),
            (13,'(HTL437/ABP38)-B-188-1-2-B','160',60,'replication block',10,2,'block'),
            (4,'((CML442/KS23-6)-B)@103-B','201',61,'replication block',1,2,'block'),
            (4,'(HTL437/ABP38)-B-157-1-2-B','202',62,'replication block',1,2,'block'),
            (4,'(HTL437/ABHB9)-B-17-1-2-B','203',63,'replication block',1,2,'block'),
            (4,'(LTL812/ABW52)-B-74-1-2-B','204',64,'replication block',1,2,'block'),
            (4,'(HTL437/ABHB9)-B-168-1-2-B','205',65,'replication block',1,2,'block'),
            (4,'(ABLTI0139/ABDHL120918)@393-B','206',66,'replication block',1,2,'block'),
            (5,'(HTL437/ABHB9)-B-120-1-1-B','207',67,'replication block',2,2,'block'),
            (5,'(LTL812/RK198)-B-53-1-2-B','208',68,'replication block',2,2,'block'),
            (5,'(LTL812/ABW52)-B-65-1-2-B','209',69,'replication block',2,2,'block'),
            (5,'(HTL437/ABHB9)-B-31-1-1-B','210',70,'replication block',2,2,'block'),
            (5,'(ABDHL0089/ABDHL120918)@52-B','211',71,'replication block',2,2,'block'),
            (5,'(HTL437/ABW52//HTL437)-98-1-1-1-B','212',72,'replication block',2,2,'block'),
            (6,'(ABLTI0139/ABLTI0335)@14-B','213',73,'replication block',3,2,'block'),
            (6,'(LTL812/ABW52)-B-124-1-2-B','214',74,'replication block',3,2,'block'),
            (6,'(LTL812/ABW52)-B-113-1-2-B','215',75,'replication block',3,2,'block'),
            (6,'(HTL437/ABW52//HTL437)-98-1-1-2-B','216',76,'replication block',3,2,'block'),
            (6,'(HTL437/ABP38)-B-189-1-2-B','217',77,'replication block',3,2,'block'),
            (6,'(HTL437/RK132)-B-89-1-2-B','218',78,'replication block',3,2,'block'),
            (7,'(CML494/OFP9)-12-2-1-1-2-B*5','219',79,'replication block',4,2,'block'),
            (7,'(LTL812/ABW52)-B-25-1-1-B','220',80,'replication block',4,2,'block'),
            (7,'(ABLTI0139/ABDHL120918)@95-B','221',81,'replication block',4,2,'block'),
            (7,'(HTL437/ABW52//HTL437)-96-1-1-2-B','222',82,'replication block',4,2,'block'),
            (7,'(ABDHL0089/ABDHL120918)@200-B','223',83,'replication block',4,2,'block'),
            (7,'(LTL812/RK198)-B-112-1-2-B','224',84,'replication block',4,2,'block'),
            (8,'(ABDHL0089/ABLTI0136)@220-B','225',85,'replication block',5,2,'block'),
            (8,'(HTL437/ABHB9)-B-119-1-1-B','226',86,'replication block',5,2,'block'),
            (8,'(HTL437/ABHB9)-B-108-1-1-B','227',87,'replication block',5,2,'block'),
            (8,'(CML494/OFP9)-12-2-1-1-1-B*4','228',88,'replication block',5,2,'block'),
            (8,'(ABLTI0139/CML543)@140-B','229',89,'replication block',5,2,'block'),
            (8,'(ABLTI0139/ABDHL120918)@653-B','230',90,'replication block',5,2,'block'),
            (9,'(HTL437/ABHB9)-B-73-1-2-B','231',91,'replication block',6,2,'block'),
            (9,'CLRCY034','232',92,'replication block',6,2,'block'),
            (9,'(ABLTI0139/ABDHL120918)@553-B','233',93,'replication block',6,2,'block'),
            (9,'(HTL437/ABHB9)-B-3-1-1-B','234',94,'replication block',6,2,'block'),
            (9,'(ABLTI0139/ABDHL120918)@373-B','235',95,'replication block',6,2,'block'),
            (9,'(LTL812/ABT11)-B-16-1-2-B','236',96,'replication block',6,2,'block'),
            (10,'(LTL812/ABT11)-B-8-1-2-B','237',97,'replication block',7,2,'block'),
            (10,'CML495','238',98,'replication block',7,2,'block'),
            (10,'(ABLTI0139/ABDHL120918)@246-B','239',99,'replication block',7,2,'block'),
            (10,'(HTL437/ABHB9)-B-168-1-1-B','240',100,'replication block',7,2,'block'),
            (10,'(ABLTI0139/ABLTI0335)@107-B','241',101,'replication block',7,2,'block'),
            (10,'(LTL812/ABT11)-B-51-1-2-B','242',102,'replication block',7,2,'block'),
            (11,'(LTL812/ABT11)-B-16-1-1-B','243',103,'replication block',8,2,'block'),
            (11,'CLYN261','244',104,'replication block',8,2,'block'),
            (11,'(HTL437/ABHB9)-B-31-1-2-B','245',105,'replication block',8,2,'block'),
            (11,'(ABDHL0089/ABDHL120668)@195-B','246',106,'replication block',8,2,'block'),
            (11,'(HTL437/ABHB9)-B-183-1-2-B','247',107,'replication block',8,2,'block'),
            (11,'(HTL437/ABW52//HTL437)-96-1-1-1-B','248',108,'replication block',8,2,'block'),
            (12,'CML463','249',109,'replication block',9,2,'block'),
            (12,'(HTL437/ABP38)-B-188-1-2-B','250',110,'replication block',9,2,'block'),
            (12,'(HTL437/ABP38)-B-178-1-2-B','251',111,'replication block',9,2,'block'),
            (12,'(HTL437/ABP38)-B-178-1-1-B','252',112,'replication block',9,2,'block'),
            (12,'(ABLTI0139/ABDHL120918)@299-B','253',113,'replication block',9,2,'block'),
            (12,'CML547','254',114,'replication block',9,2,'block'),
            (13,'(HTL437/ABP38)-B-157-1-1-B','255',115,'replication block',10,2,'block'),
            (13,'(ABLTI0139/ABDHL120918)@479-B','256',116,'replication block',10,2,'block'),
            (13,'(ABDHL0089/ABDHL120668)@19-B','257',117,'replication block',10,2,'block'),
            (13,'(LTL812/ABW52)-B-65-1-1-B','258',118,'replication block',10,2,'block'),
            (13,'((CML537/KS523-5)-B)@10-B','259',119,'replication block',10,2,'block'),
            (13,'(ABDHL0089/ABDHL120918)@63-B','260',120,'replication block',10,2,'block'),
            (4,'(ABLTI0139/ABDHL120918)@553-B','301',121,'replication block',1,2,'block'),
            (4,'(ABDHL0089/ABDHL120918)@63-B','302',122,'replication block',1,2,'block'),
            (4,'(LTL812/ABT11)-B-8-1-2-B','303',123,'replication block',1,2,'block'),
            (4,'(HTL437/ABW52//HTL437)-98-1-1-1-B','304',124,'replication block',1,2,'block'),
            (4,'(HTL437/ABHB9)-B-119-1-1-B','305',125,'replication block',1,2,'block'),
            (4,'(CML494/OFP9)-12-2-1-1-2-B*5','306',126,'replication block',1,2,'block'),
            (5,'(HTL437/ABW52//HTL437)-98-1-1-2-B','307',127,'replication block',2,2,'block'),
            (5,'(ABLTI0139/ABDHL120918)@246-B','308',128,'replication block',2,2,'block'),
            (5,'(ABLTI0139/ABDHL120918)@653-B','309',129,'replication block',2,2,'block'),
            (5,'(LTL812/ABW52)-B-74-1-2-B','310',130,'replication block',2,2,'block'),
            (5,'(HTL437/ABW52//HTL437)-96-1-1-1-B','311',131,'replication block',2,2,'block'),
            (5,'CLRCY034','312',132,'replication block',2,2,'block'),
            (6,'(LTL812/ABT11)-B-51-1-2-B','313',133,'replication block',3,2,'block'),
            (6,'(ABLTI0139/ABDHL120918)@299-B','314',134,'replication block',3,2,'block'),
            (6,'(HTL437/ABW52//HTL437)-96-1-1-2-B','315',135,'replication block',3,2,'block'),
            (6,'(HTL437/ABP38)-B-157-1-1-B','316',136,'replication block',3,2,'block'),
            (6,'(ABDHL0089/ABDHL120918)@52-B','317',137,'replication block',3,2,'block'),
            (6,'(ABLTI0139/ABDHL120918)@373-B','318',138,'replication block',3,2,'block'),
            (7,'(ABLTI0139/CML543)@140-B','319',139,'replication block',4,2,'block'),
            (7,'(LTL812/ABT11)-B-16-1-2-B','320',140,'replication block',4,2,'block'),
            (7,'(LTL812/ABW52)-B-124-1-2-B','321',141,'replication block',4,2,'block'),
            (7,'(HTL437/ABP38)-B-178-1-1-B','322',142,'replication block',4,2,'block'),
            (7,'CLYN261','323',143,'replication block',4,2,'block'),
            (7,'(HTL437/ABHB9)-B-17-1-2-B','324',144,'replication block',4,2,'block'),
            (8,'(HTL437/RK132)-B-89-1-2-B','325',145,'replication block',5,2,'block'),
            (8,'(LTL812/ABW52)-B-65-1-1-B','326',146,'replication block',5,2,'block'),
            (8,'(HTL437/ABHB9)-B-120-1-1-B','327',147,'replication block',5,2,'block'),
            (8,'(HTL437/ABP38)-B-189-1-2-B','328',148,'replication block',5,2,'block'),
            (8,'(ABLTI0139/ABDHL120918)@95-B','329',149,'replication block',5,2,'block'),
            (8,'(HTL437/ABHB9)-B-3-1-1-B','330',150,'replication block',5,2,'block'),
            (9,'(HTL437/ABHB9)-B-168-1-1-B','331',151,'replication block',6,2,'block'),
            (9,'(LTL812/ABW52)-B-113-1-2-B','332',152,'replication block',6,2,'block'),
            (9,'(HTL437/ABP38)-B-178-1-2-B','333',153,'replication block',6,2,'block'),
            (9,'(LTL812/ABW52)-B-25-1-1-B','334',154,'replication block',6,2,'block'),
            (9,'(HTL437/ABP38)-B-157-1-2-B','335',155,'replication block',6,2,'block'),
            (9,'(LTL812/ABT11)-B-16-1-1-B','336',156,'replication block',6,2,'block'),
            (10,'(ABDHL0089/ABDHL120668)@19-B','337',157,'replication block',7,2,'block'),
            (10,'((CML442/KS23-6)-B)@103-B','338',158,'replication block',7,2,'block'),
            (10,'(ABDHL0089/ABDHL120668)@195-B','339',159,'replication block',7,2,'block'),
            (10,'(HTL437/ABHB9)-B-108-1-1-B','340',160,'replication block',7,2,'block'),
            (10,'(HTL437/ABHB9)-B-168-1-2-B','341',161,'replication block',7,2,'block'),
            (10,'(LTL812/ABW52)-B-65-1-2-B','342',162,'replication block',7,2,'block'),
            (11,'(HTL437/ABP38)-B-188-1-2-B','343',163,'replication block',8,2,'block'),
            (11,'(LTL812/RK198)-B-53-1-2-B','344',164,'replication block',8,2,'block'),
            (11,'(ABDHL0089/ABLTI0136)@220-B','345',165,'replication block',8,2,'block'),
            (11,'CML495','346',166,'replication block',8,2,'block'),
            (11,'CML463','347',167,'replication block',8,2,'block'),
            (11,'(LTL812/RK198)-B-112-1-2-B','348',168,'replication block',8,2,'block'),
            (12,'((CML537/KS523-5)-B)@10-B','349',169,'replication block',9,2,'block'),
            (12,'(HTL437/ABHB9)-B-73-1-2-B','350',170,'replication block',9,2,'block'),
            (12,'(ABLTI0139/ABDHL120918)@479-B','351',171,'replication block',9,2,'block'),
            (12,'(HTL437/ABHB9)-B-31-1-2-B','352',172,'replication block',9,2,'block'),
            (12,'(ABLTI0139/ABLTI0335)@14-B','353',173,'replication block',9,2,'block'),
            (12,'(HTL437/ABHB9)-B-183-1-2-B','354',174,'replication block',9,2,'block'),
            (13,'(HTL437/ABHB9)-B-31-1-1-B','355',175,'replication block',10,2,'block'),
            (13,'(CML494/OFP9)-12-2-1-1-1-B*4','356',176,'replication block',10,2,'block'),
            (13,'(ABLTI0139/ABDHL120918)@393-B','357',177,'replication block',10,2,'block'),
            (13,'(ABDHL0089/ABDHL120918)@200-B','358',178,'replication block',10,2,'block'),
            (13,'(ABLTI0139/ABLTI0335)@107-B','359',179,'replication block',10,2,'block'),
            (13,'CML547','360',180,'replication block',10,2,'block')
) AS t (design_id, entry_name, plot_code, plot_number, block_type, block_value, block_level_number, block_name)
INNER JOIN
    experiment.occurrence occ
ON
    occ.occurrence_name='AF2021_002#OCC-01'
INNER JOIN
    experiment.entry_list els
ON
    els.entry_list_name = 'KE-AYT-2021-WS Entry List'
INNER JOIN
     tenant.person psn
ON
    psn.username = 'nicola.costa';

--set 2
INSERT INTO
   experiment.experiment_design (
       occurrence_id, design_id, plot_id, block_type, block_value,
       block_level_number, creator_id, block_name
   )
SELECT
    occ.id AS occurrence_id,
    t.design_id AS design_id,
    (SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name=t.entry_name AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name=els.entry_list_name)) AND ep.plot_code = t.plot_code AND ep.plot_number = t.plot_number AND ep.occurrence_id = (SELECT id FROM experiment.occurrence WHERE occurrence_name=occ.occurrence_name)) AS plot_id,
    t.block_type AS block_type,
    t.block_value AS block_value,
    t.block_level_number AS block_level_number,
    psn.id AS creator_id,
    t.block_name AS block_name
FROM
    (
        VALUES     
            (1,'(ABLTI0139/ABDHL120918)@479-B','101',1,'replication block',1,1,'replicate'),
            (1,'(ABDHL0089/ABLTI0136)@220-B','102',2,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-73-1-2-B','103',3,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52//HTL437)-96-1-1-1-B','104',4,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABDHL120918)@246-B','105',5,'replication block',1,1,'replicate'),
            (1,'CLYN261','106',6,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-168-1-2-B','107',7,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-3-1-1-B','108',8,'replication block',1,1,'replicate'),
            (1,'(CML494/OFP9)-12-2-1-1-2-B*5','109',9,'replication block',1,1,'replicate'),
            (1,'CML463','110',10,'replication block',1,1,'replicate'),
            (1,'CML547','111',11,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52//HTL437)-96-1-1-2-B','112',12,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-31-1-1-B','113',13,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-31-1-2-B','114',14,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/CML543)@140-B','115',15,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-74-1-2-B','116',16,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-65-1-1-B','117',17,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-25-1-1-B','118',18,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABDHL120918)@653-B','119',19,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABDHL120918)@299-B','120',20,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABLTI0335)@14-B','121',21,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-119-1-1-B','122',22,'replication block',1,1,'replicate'),
            (1,'(HTL437/RK132)-B-89-1-2-B','123',23,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-17-1-2-B','124',24,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABDHL120918)@95-B','125',25,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-65-1-2-B','126',26,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-120-1-1-B','127',27,'replication block',1,1,'replicate'),
            (1,'CLRCY034','128',28,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-113-1-2-B','129',29,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-157-1-2-B','130',30,'replication block',1,1,'replicate'),
            (1,'((CML442/KS23-6)-B)@103-B','131',31,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-168-1-1-B','132',32,'replication block',1,1,'replicate'),
            (1,'(ABDHL0089/ABDHL120668)@195-B','133',33,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-108-1-1-B','134',34,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-183-1-2-B','135',35,'replication block',1,1,'replicate'),
            (1,'((CML537/KS523-5)-B)@10-B','136',36,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-189-1-2-B','137',37,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABT11)-B-51-1-2-B','138',38,'replication block',1,1,'replicate'),
            (1,'CML495','139',39,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-157-1-1-B','140',40,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-178-1-2-B','141',41,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-188-1-2-B','142',42,'replication block',1,1,'replicate'),
            (1,'(LTL812/RK198)-B-112-1-2-B','143',43,'replication block',1,1,'replicate'),
            (1,'(LTL812/RK198)-B-53-1-2-B','144',44,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-178-1-1-B','145',45,'replication block',1,1,'replicate'),
            (1,'(ABDHL0089/ABDHL120668)@19-B','146',46,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABT11)-B-16-1-1-B','147',47,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-124-1-2-B','148',48,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52//HTL437)-98-1-1-2-B','149',49,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABDHL120918)@373-B','150',50,'replication block',1,1,'replicate'),
            (1,'(ABDHL0089/ABDHL120918)@63-B','151',51,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABDHL120918)@553-B','152',52,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABDHL120918)@393-B','153',53,'replication block',1,1,'replicate'),
            (1,'(CML494/OFP9)-12-2-1-1-1-B*4','154',54,'replication block',1,1,'replicate'),
            (1,'(ABDHL0089/ABDHL120918)@52-B','155',55,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABLTI0335)@107-B','156',56,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABT11)-B-8-1-2-B','157',57,'replication block',1,1,'replicate'),
            (1,'(ABDHL0089/ABDHL120918)@200-B','158',58,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABT11)-B-16-1-2-B','159',59,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52//HTL437)-98-1-1-1-B','160',60,'replication block',1,1,'replicate'),
            (2,'(LTL812/ABW52)-B-25-1-1-B','201',61,'replication block',2,1,'replicate'),
            (2,'(CML494/OFP9)-12-2-1-1-1-B*4','202',62,'replication block',2,1,'replicate'),
            (2,'CLRCY034','203',63,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-178-1-1-B','204',64,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-183-1-2-B','205',65,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-157-1-1-B','206',66,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-168-1-1-B','207',67,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABDHL120918)@653-B','208',68,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABDHL120918)@393-B','209',69,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-74-1-2-B','210',70,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABDHL120918)@553-B','211',71,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABT11)-B-8-1-2-B','212',72,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABDHL120918)@95-B','213',73,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABLTI0335)@107-B','214',74,'replication block',2,1,'replicate'),
            (2,'(ABDHL0089/ABDHL120668)@19-B','215',75,'replication block',2,1,'replicate'),
            (2,'((CML537/KS523-5)-B)@10-B','216',76,'replication block',2,1,'replicate'),
            (2,'CML495','217',77,'replication block',2,1,'replicate'),
            (2,'(CML494/OFP9)-12-2-1-1-2-B*5','218',78,'replication block',2,1,'replicate'),
            (2,'(ABDHL0089/ABLTI0136)@220-B','219',79,'replication block',2,1,'replicate'),
            (2,'CML463','220',80,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/CML543)@140-B','221',81,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABT11)-B-16-1-1-B','222',82,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-119-1-1-B','223',83,'replication block',2,1,'replicate'),
            (2,'(ABDHL0089/ABDHL120918)@52-B','224',84,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52//HTL437)-98-1-1-1-B','225',85,'replication block',2,1,'replicate'),
            (2,'(ABDHL0089/ABDHL120918)@63-B','226',86,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-17-1-2-B','227',87,'replication block',2,1,'replicate'),
            (2,'((CML442/KS23-6)-B)@103-B','228',88,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-31-1-2-B','229',89,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-178-1-2-B','230',90,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-65-1-1-B','231',91,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-189-1-2-B','232',92,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABDHL120918)@246-B','233',93,'replication block',2,1,'replicate'),
            (2,'(LTL812/RK198)-B-53-1-2-B','234',94,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-120-1-1-B','235',95,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABDHL120918)@299-B','236',96,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-157-1-2-B','237',97,'replication block',2,1,'replicate'),
            (2,'CML547','238',98,'replication block',2,1,'replicate'),
            (2,'(ABDHL0089/ABDHL120918)@200-B','239',99,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52//HTL437)-98-1-1-2-B','240',100,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-73-1-2-B','241',101,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-31-1-1-B','242',102,'replication block',2,1,'replicate'),
            (2,'(LTL812/RK198)-B-112-1-2-B','243',103,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-188-1-2-B','244',104,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-65-1-2-B','245',105,'replication block',2,1,'replicate'),
            (2,'CLYN261','246',106,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABDHL120918)@373-B','247',107,'replication block',2,1,'replicate'),
            (2,'(ABDHL0089/ABDHL120668)@195-B','248',108,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABT11)-B-51-1-2-B','249',109,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABT11)-B-16-1-2-B','250',110,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-3-1-1-B','251',111,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-108-1-1-B','252',112,'replication block',2,1,'replicate'),
            (2,'(HTL437/RK132)-B-89-1-2-B','253',113,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52//HTL437)-96-1-1-1-B','254',114,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-168-1-2-B','255',115,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABLTI0335)@14-B','256',116,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-124-1-2-B','257',117,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-113-1-2-B','258',118,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABDHL120918)@479-B','259',119,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52//HTL437)-96-1-1-2-B','260',120,'replication block',2,1,'replicate'),
            (3,'(CML494/OFP9)-12-2-1-1-2-B*5','301',121,'replication block',3,1,'replicate'),
            (3,'CML547','302',122,'replication block',3,1,'replicate'),
            (3,'CLYN261','303',123,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-119-1-1-B','304',124,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52//HTL437)-98-1-1-1-B','305',125,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-168-1-1-B','306',126,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-65-1-1-B','307',127,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABDHL120918)@373-B','308',128,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABLTI0335)@14-B','309',129,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABDHL120918)@95-B','310',130,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-108-1-1-B','311',131,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/CML543)@140-B','312',132,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABDHL120918)@299-B','313',133,'replication block',3,1,'replicate'),
            (3,'(ABDHL0089/ABDHL120918)@52-B','314',134,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABT11)-B-51-1-2-B','315',135,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52//HTL437)-96-1-1-1-B','316',136,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-124-1-2-B','317',137,'replication block',3,1,'replicate'),
            (3,'(ABDHL0089/ABDHL120918)@63-B','318',138,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-17-1-2-B','319',139,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-65-1-2-B','320',140,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-178-1-1-B','321',141,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABT11)-B-16-1-2-B','322',142,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-31-1-1-B','323',143,'replication block',3,1,'replicate'),
            (3,'((CML537/KS523-5)-B)@10-B','324',144,'replication block',3,1,'replicate'),
            (3,'(LTL812/RK198)-B-53-1-2-B','325',145,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABDHL120918)@653-B','326',146,'replication block',3,1,'replicate'),
            (3,'(ABDHL0089/ABDHL120918)@200-B','327',147,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-183-1-2-B','328',148,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-189-1-2-B','329',149,'replication block',3,1,'replicate'),
            (3,'(ABDHL0089/ABLTI0136)@220-B','330',150,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABDHL120918)@553-B','331',151,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-31-1-2-B','332',152,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-188-1-2-B','333',153,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-168-1-2-B','334',154,'replication block',3,1,'replicate'),
            (3,'CLRCY034','335',155,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABT11)-B-8-1-2-B','336',156,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-25-1-1-B','337',157,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-113-1-2-B','338',158,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-3-1-1-B','339',159,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52//HTL437)-98-1-1-2-B','340',160,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABLTI0335)@107-B','341',161,'replication block',3,1,'replicate'),
            (3,'(LTL812/RK198)-B-112-1-2-B','342',162,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-178-1-2-B','343',163,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-74-1-2-B','344',164,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-73-1-2-B','345',165,'replication block',3,1,'replicate'),
            (3,'(ABDHL0089/ABDHL120668)@19-B','346',166,'replication block',3,1,'replicate'),
            (3,'(HTL437/RK132)-B-89-1-2-B','347',167,'replication block',3,1,'replicate'),
            (3,'(CML494/OFP9)-12-2-1-1-1-B*4','348',168,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABDHL120918)@479-B','349',169,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-157-1-1-B','350',170,'replication block',3,1,'replicate'),
            (3,'((CML442/KS23-6)-B)@103-B','351',171,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-120-1-1-B','352',172,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52//HTL437)-96-1-1-2-B','353',173,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABT11)-B-16-1-1-B','354',174,'replication block',3,1,'replicate'),
            (3,'(ABDHL0089/ABDHL120668)@195-B','355',175,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABDHL120918)@246-B','356',176,'replication block',3,1,'replicate'),
            (3,'CML463','357',177,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-157-1-2-B','358',178,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABDHL120918)@393-B','359',179,'replication block',3,1,'replicate'),
            (3,'CML495','360',180,'replication block',3,1,'replicate'),
            (4,'(ABLTI0139/ABDHL120918)@479-B','101',1,'replication block',1,2,'block'),
            (4,'(ABDHL0089/ABLTI0136)@220-B','102',2,'replication block',1,2,'block'),
            (4,'(HTL437/ABHB9)-B-73-1-2-B','103',3,'replication block',1,2,'block'),
            (4,'(HTL437/ABW52//HTL437)-96-1-1-1-B','104',4,'replication block',1,2,'block'),
            (4,'(ABLTI0139/ABDHL120918)@246-B','105',5,'replication block',1,2,'block'),
            (4,'CLYN261','106',6,'replication block',1,2,'block'),
            (5,'(HTL437/ABHB9)-B-168-1-2-B','107',7,'replication block',2,2,'block'),
            (5,'(HTL437/ABHB9)-B-3-1-1-B','108',8,'replication block',2,2,'block'),
            (5,'(CML494/OFP9)-12-2-1-1-2-B*5','109',9,'replication block',2,2,'block'),
            (5,'CML463','110',10,'replication block',2,2,'block'),
            (5,'CML547','111',11,'replication block',2,2,'block'),
            (5,'(HTL437/ABW52//HTL437)-96-1-1-2-B','112',12,'replication block',2,2,'block'),
            (6,'(HTL437/ABHB9)-B-31-1-1-B','113',13,'replication block',3,2,'block'),
            (6,'(HTL437/ABHB9)-B-31-1-2-B','114',14,'replication block',3,2,'block'),
            (6,'(ABLTI0139/CML543)@140-B','115',15,'replication block',3,2,'block'),
            (6,'(LTL812/ABW52)-B-74-1-2-B','116',16,'replication block',3,2,'block'),
            (6,'(LTL812/ABW52)-B-65-1-1-B','117',17,'replication block',3,2,'block'),
            (6,'(LTL812/ABW52)-B-25-1-1-B','118',18,'replication block',3,2,'block'),
            (7,'(ABLTI0139/ABDHL120918)@653-B','119',19,'replication block',4,2,'block'),
            (7,'(ABLTI0139/ABDHL120918)@299-B','120',20,'replication block',4,2,'block'),
            (7,'(ABLTI0139/ABLTI0335)@14-B','121',21,'replication block',4,2,'block'),
            (7,'(HTL437/ABHB9)-B-119-1-1-B','122',22,'replication block',4,2,'block'),
            (7,'(HTL437/RK132)-B-89-1-2-B','123',23,'replication block',4,2,'block'),
            (7,'(HTL437/ABHB9)-B-17-1-2-B','124',24,'replication block',4,2,'block'),
            (8,'(ABLTI0139/ABDHL120918)@95-B','125',25,'replication block',5,2,'block'),
            (8,'(LTL812/ABW52)-B-65-1-2-B','126',26,'replication block',5,2,'block'),
            (8,'(HTL437/ABHB9)-B-120-1-1-B','127',27,'replication block',5,2,'block'),
            (8,'CLRCY034','128',28,'replication block',5,2,'block'),
            (8,'(LTL812/ABW52)-B-113-1-2-B','129',29,'replication block',5,2,'block'),
            (8,'(HTL437/ABP38)-B-157-1-2-B','130',30,'replication block',5,2,'block'),
            (9,'((CML442/KS23-6)-B)@103-B','131',31,'replication block',6,2,'block'),
            (9,'(HTL437/ABHB9)-B-168-1-1-B','132',32,'replication block',6,2,'block'),
            (9,'(ABDHL0089/ABDHL120668)@195-B','133',33,'replication block',6,2,'block'),
            (9,'(HTL437/ABHB9)-B-108-1-1-B','134',34,'replication block',6,2,'block'),
            (9,'(HTL437/ABHB9)-B-183-1-2-B','135',35,'replication block',6,2,'block'),
            (9,'((CML537/KS523-5)-B)@10-B','136',36,'replication block',6,2,'block'),
            (10,'(HTL437/ABP38)-B-189-1-2-B','137',37,'replication block',7,2,'block'),
            (10,'(LTL812/ABT11)-B-51-1-2-B','138',38,'replication block',7,2,'block'),
            (10,'CML495','139',39,'replication block',7,2,'block'),
            (10,'(HTL437/ABP38)-B-157-1-1-B','140',40,'replication block',7,2,'block'),
            (10,'(HTL437/ABP38)-B-178-1-2-B','141',41,'replication block',7,2,'block'),
            (10,'(HTL437/ABP38)-B-188-1-2-B','142',42,'replication block',7,2,'block'),
            (11,'(LTL812/RK198)-B-112-1-2-B','143',43,'replication block',8,2,'block'),
            (11,'(LTL812/RK198)-B-53-1-2-B','144',44,'replication block',8,2,'block'),
            (11,'(HTL437/ABP38)-B-178-1-1-B','145',45,'replication block',8,2,'block'),
            (11,'(ABDHL0089/ABDHL120668)@19-B','146',46,'replication block',8,2,'block'),
            (11,'(LTL812/ABT11)-B-16-1-1-B','147',47,'replication block',8,2,'block'),
            (11,'(LTL812/ABW52)-B-124-1-2-B','148',48,'replication block',8,2,'block'),
            (12,'(HTL437/ABW52//HTL437)-98-1-1-2-B','149',49,'replication block',9,2,'block'),
            (12,'(ABLTI0139/ABDHL120918)@373-B','150',50,'replication block',9,2,'block'),
            (12,'(ABDHL0089/ABDHL120918)@63-B','151',51,'replication block',9,2,'block'),
            (12,'(ABLTI0139/ABDHL120918)@553-B','152',52,'replication block',9,2,'block'),
            (12,'(ABLTI0139/ABDHL120918)@393-B','153',53,'replication block',9,2,'block'),
            (12,'(CML494/OFP9)-12-2-1-1-1-B*4','154',54,'replication block',9,2,'block'),
            (13,'(ABDHL0089/ABDHL120918)@52-B','155',55,'replication block',10,2,'block'),
            (13,'(ABLTI0139/ABLTI0335)@107-B','156',56,'replication block',10,2,'block'),
            (13,'(LTL812/ABT11)-B-8-1-2-B','157',57,'replication block',10,2,'block'),
            (13,'(ABDHL0089/ABDHL120918)@200-B','158',58,'replication block',10,2,'block'),
            (13,'(LTL812/ABT11)-B-16-1-2-B','159',59,'replication block',10,2,'block'),
            (13,'(HTL437/ABW52//HTL437)-98-1-1-1-B','160',60,'replication block',10,2,'block'),
            (4,'(LTL812/ABW52)-B-25-1-1-B','201',61,'replication block',1,2,'block'),
            (4,'(CML494/OFP9)-12-2-1-1-1-B*4','202',62,'replication block',1,2,'block'),
            (4,'CLRCY034','203',63,'replication block',1,2,'block'),
            (4,'(HTL437/ABP38)-B-178-1-1-B','204',64,'replication block',1,2,'block'),
            (4,'(HTL437/ABHB9)-B-183-1-2-B','205',65,'replication block',1,2,'block'),
            (4,'(HTL437/ABP38)-B-157-1-1-B','206',66,'replication block',1,2,'block'),
            (5,'(HTL437/ABHB9)-B-168-1-1-B','207',67,'replication block',2,2,'block'),
            (5,'(ABLTI0139/ABDHL120918)@653-B','208',68,'replication block',2,2,'block'),
            (5,'(ABLTI0139/ABDHL120918)@393-B','209',69,'replication block',2,2,'block'),
            (5,'(LTL812/ABW52)-B-74-1-2-B','210',70,'replication block',2,2,'block'),
            (5,'(ABLTI0139/ABDHL120918)@553-B','211',71,'replication block',2,2,'block'),
            (5,'(LTL812/ABT11)-B-8-1-2-B','212',72,'replication block',2,2,'block'),
            (6,'(ABLTI0139/ABDHL120918)@95-B','213',73,'replication block',3,2,'block'),
            (6,'(ABLTI0139/ABLTI0335)@107-B','214',74,'replication block',3,2,'block'),
            (6,'(ABDHL0089/ABDHL120668)@19-B','215',75,'replication block',3,2,'block'),
            (6,'((CML537/KS523-5)-B)@10-B','216',76,'replication block',3,2,'block'),
            (6,'CML495','217',77,'replication block',3,2,'block'),
            (6,'(CML494/OFP9)-12-2-1-1-2-B*5','218',78,'replication block',3,2,'block'),
            (7,'(ABDHL0089/ABLTI0136)@220-B','219',79,'replication block',4,2,'block'),
            (7,'CML463','220',80,'replication block',4,2,'block'),
            (7,'(ABLTI0139/CML543)@140-B','221',81,'replication block',4,2,'block'),
            (7,'(LTL812/ABT11)-B-16-1-1-B','222',82,'replication block',4,2,'block'),
            (7,'(HTL437/ABHB9)-B-119-1-1-B','223',83,'replication block',4,2,'block'),
            (7,'(ABDHL0089/ABDHL120918)@52-B','224',84,'replication block',4,2,'block'),
            (8,'(HTL437/ABW52//HTL437)-98-1-1-1-B','225',85,'replication block',5,2,'block'),
            (8,'(ABDHL0089/ABDHL120918)@63-B','226',86,'replication block',5,2,'block'),
            (8,'(HTL437/ABHB9)-B-17-1-2-B','227',87,'replication block',5,2,'block'),
            (8,'((CML442/KS23-6)-B)@103-B','228',88,'replication block',5,2,'block'),
            (8,'(HTL437/ABHB9)-B-31-1-2-B','229',89,'replication block',5,2,'block'),
            (8,'(HTL437/ABP38)-B-178-1-2-B','230',90,'replication block',5,2,'block'),
            (9,'(LTL812/ABW52)-B-65-1-1-B','231',91,'replication block',6,2,'block'),
            (9,'(HTL437/ABP38)-B-189-1-2-B','232',92,'replication block',6,2,'block'),
            (9,'(ABLTI0139/ABDHL120918)@246-B','233',93,'replication block',6,2,'block'),
            (9,'(LTL812/RK198)-B-53-1-2-B','234',94,'replication block',6,2,'block'),
            (9,'(HTL437/ABHB9)-B-120-1-1-B','235',95,'replication block',6,2,'block'),
            (9,'(ABLTI0139/ABDHL120918)@299-B','236',96,'replication block',6,2,'block'),
            (10,'(HTL437/ABP38)-B-157-1-2-B','237',97,'replication block',7,2,'block'),
            (10,'CML547','238',98,'replication block',7,2,'block'),
            (10,'(ABDHL0089/ABDHL120918)@200-B','239',99,'replication block',7,2,'block'),
            (10,'(HTL437/ABW52//HTL437)-98-1-1-2-B','240',100,'replication block',7,2,'block'),
            (10,'(HTL437/ABHB9)-B-73-1-2-B','241',101,'replication block',7,2,'block'),
            (10,'(HTL437/ABHB9)-B-31-1-1-B','242',102,'replication block',7,2,'block'),
            (11,'(LTL812/RK198)-B-112-1-2-B','243',103,'replication block',8,2,'block'),
            (11,'(HTL437/ABP38)-B-188-1-2-B','244',104,'replication block',8,2,'block'),
            (11,'(LTL812/ABW52)-B-65-1-2-B','245',105,'replication block',8,2,'block'),
            (11,'CLYN261','246',106,'replication block',8,2,'block'),
            (11,'(ABLTI0139/ABDHL120918)@373-B','247',107,'replication block',8,2,'block'),
            (11,'(ABDHL0089/ABDHL120668)@195-B','248',108,'replication block',8,2,'block'),
            (12,'(LTL812/ABT11)-B-51-1-2-B','249',109,'replication block',9,2,'block'),
            (12,'(LTL812/ABT11)-B-16-1-2-B','250',110,'replication block',9,2,'block'),
            (12,'(HTL437/ABHB9)-B-3-1-1-B','251',111,'replication block',9,2,'block'),
            (12,'(HTL437/ABHB9)-B-108-1-1-B','252',112,'replication block',9,2,'block'),
            (12,'(HTL437/RK132)-B-89-1-2-B','253',113,'replication block',9,2,'block'),
            (12,'(HTL437/ABW52//HTL437)-96-1-1-1-B','254',114,'replication block',9,2,'block'),
            (13,'(HTL437/ABHB9)-B-168-1-2-B','255',115,'replication block',10,2,'block'),
            (13,'(ABLTI0139/ABLTI0335)@14-B','256',116,'replication block',10,2,'block'),
            (13,'(LTL812/ABW52)-B-124-1-2-B','257',117,'replication block',10,2,'block'),
            (13,'(LTL812/ABW52)-B-113-1-2-B','258',118,'replication block',10,2,'block'),
            (13,'(ABLTI0139/ABDHL120918)@479-B','259',119,'replication block',10,2,'block'),
            (13,'(HTL437/ABW52//HTL437)-96-1-1-2-B','260',120,'replication block',10,2,'block'),
            (4,'(CML494/OFP9)-12-2-1-1-2-B*5','301',121,'replication block',1,2,'block'),
            (4,'CML547','302',122,'replication block',1,2,'block'),
            (4,'CLYN261','303',123,'replication block',1,2,'block'),
            (4,'(HTL437/ABHB9)-B-119-1-1-B','304',124,'replication block',1,2,'block'),
            (4,'(HTL437/ABW52//HTL437)-98-1-1-1-B','305',125,'replication block',1,2,'block'),
            (4,'(HTL437/ABHB9)-B-168-1-1-B','306',126,'replication block',1,2,'block'),
            (5,'(LTL812/ABW52)-B-65-1-1-B','307',127,'replication block',2,2,'block'),
            (5,'(ABLTI0139/ABDHL120918)@373-B','308',128,'replication block',2,2,'block'),
            (5,'(ABLTI0139/ABLTI0335)@14-B','309',129,'replication block',2,2,'block'),
            (5,'(ABLTI0139/ABDHL120918)@95-B','310',130,'replication block',2,2,'block'),
            (5,'(HTL437/ABHB9)-B-108-1-1-B','311',131,'replication block',2,2,'block'),
            (5,'(ABLTI0139/CML543)@140-B','312',132,'replication block',2,2,'block'),
            (6,'(ABLTI0139/ABDHL120918)@299-B','313',133,'replication block',3,2,'block'),
            (6,'(ABDHL0089/ABDHL120918)@52-B','314',134,'replication block',3,2,'block'),
            (6,'(LTL812/ABT11)-B-51-1-2-B','315',135,'replication block',3,2,'block'),
            (6,'(HTL437/ABW52//HTL437)-96-1-1-1-B','316',136,'replication block',3,2,'block'),
            (6,'(LTL812/ABW52)-B-124-1-2-B','317',137,'replication block',3,2,'block'),
            (6,'(ABDHL0089/ABDHL120918)@63-B','318',138,'replication block',3,2,'block'),
            (7,'(HTL437/ABHB9)-B-17-1-2-B','319',139,'replication block',4,2,'block'),
            (7,'(LTL812/ABW52)-B-65-1-2-B','320',140,'replication block',4,2,'block'),
            (7,'(HTL437/ABP38)-B-178-1-1-B','321',141,'replication block',4,2,'block'),
            (7,'(LTL812/ABT11)-B-16-1-2-B','322',142,'replication block',4,2,'block'),
            (7,'(HTL437/ABHB9)-B-31-1-1-B','323',143,'replication block',4,2,'block'),
            (7,'((CML537/KS523-5)-B)@10-B','324',144,'replication block',4,2,'block'),
            (8,'(LTL812/RK198)-B-53-1-2-B','325',145,'replication block',5,2,'block'),
            (8,'(ABLTI0139/ABDHL120918)@653-B','326',146,'replication block',5,2,'block'),
            (8,'(ABDHL0089/ABDHL120918)@200-B','327',147,'replication block',5,2,'block'),
            (8,'(HTL437/ABHB9)-B-183-1-2-B','328',148,'replication block',5,2,'block'),
            (8,'(HTL437/ABP38)-B-189-1-2-B','329',149,'replication block',5,2,'block'),
            (8,'(ABDHL0089/ABLTI0136)@220-B','330',150,'replication block',5,2,'block'),
            (9,'(ABLTI0139/ABDHL120918)@553-B','331',151,'replication block',6,2,'block'),
            (9,'(HTL437/ABHB9)-B-31-1-2-B','332',152,'replication block',6,2,'block'),
            (9,'(HTL437/ABP38)-B-188-1-2-B','333',153,'replication block',6,2,'block'),
            (9,'(HTL437/ABHB9)-B-168-1-2-B','334',154,'replication block',6,2,'block'),
            (9,'CLRCY034','335',155,'replication block',6,2,'block'),
            (9,'(LTL812/ABT11)-B-8-1-2-B','336',156,'replication block',6,2,'block'),
            (10,'(LTL812/ABW52)-B-25-1-1-B','337',157,'replication block',7,2,'block'),
            (10,'(LTL812/ABW52)-B-113-1-2-B','338',158,'replication block',7,2,'block'),
            (10,'(HTL437/ABHB9)-B-3-1-1-B','339',159,'replication block',7,2,'block'),
            (10,'(HTL437/ABW52//HTL437)-98-1-1-2-B','340',160,'replication block',7,2,'block'),
            (10,'(ABLTI0139/ABLTI0335)@107-B','341',161,'replication block',7,2,'block'),
            (10,'(LTL812/RK198)-B-112-1-2-B','342',162,'replication block',7,2,'block'),
            (11,'(HTL437/ABP38)-B-178-1-2-B','343',163,'replication block',8,2,'block'),
            (11,'(LTL812/ABW52)-B-74-1-2-B','344',164,'replication block',8,2,'block'),
            (11,'(HTL437/ABHB9)-B-73-1-2-B','345',165,'replication block',8,2,'block'),
            (11,'(ABDHL0089/ABDHL120668)@19-B','346',166,'replication block',8,2,'block'),
            (11,'(HTL437/RK132)-B-89-1-2-B','347',167,'replication block',8,2,'block'),
            (11,'(CML494/OFP9)-12-2-1-1-1-B*4','348',168,'replication block',8,2,'block'),
            (12,'(ABLTI0139/ABDHL120918)@479-B','349',169,'replication block',9,2,'block'),
            (12,'(HTL437/ABP38)-B-157-1-1-B','350',170,'replication block',9,2,'block'),
            (12,'((CML442/KS23-6)-B)@103-B','351',171,'replication block',9,2,'block'),
            (12,'(HTL437/ABHB9)-B-120-1-1-B','352',172,'replication block',9,2,'block'),
            (12,'(HTL437/ABW52//HTL437)-96-1-1-2-B','353',173,'replication block',9,2,'block'),
            (12,'(LTL812/ABT11)-B-16-1-1-B','354',174,'replication block',9,2,'block'),
            (13,'(ABDHL0089/ABDHL120668)@195-B','355',175,'replication block',10,2,'block'),
            (13,'(ABLTI0139/ABDHL120918)@246-B','356',176,'replication block',10,2,'block'),
            (13,'CML463','357',177,'replication block',10,2,'block'),
            (13,'(HTL437/ABP38)-B-157-1-2-B','358',178,'replication block',10,2,'block'),
            (13,'(ABLTI0139/ABDHL120918)@393-B','359',179,'replication block',10,2,'block'),
            (13,'CML495','360',180,'replication block',10,2,'block')
) AS t (design_id, entry_name, plot_code, plot_number, block_type, block_value, block_level_number, block_name)
INNER JOIN
    experiment.occurrence occ
ON
    occ.occurrence_name='AF2021_002#OCC-03'
INNER JOIN
    experiment.entry_list els
ON
    els.entry_list_name = 'KE-AYT-2021-WS Entry List'
INNER JOIN
     tenant.person psn
ON
    psn.username = 'nicola.costa';

--set 3
INSERT INTO
   experiment.experiment_design (
       occurrence_id, design_id, plot_id, block_type, block_value,
       block_level_number, creator_id, block_name
   )
SELECT
    occ.id AS occurrence_id,
    t.design_id AS design_id,
    (SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name=t.entry_name AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name=els.entry_list_name)) AND ep.plot_code = t.plot_code AND ep.plot_number = t.plot_number AND ep.occurrence_id = (SELECT id FROM experiment.occurrence WHERE occurrence_name=occ.occurrence_name)) AS plot_id,
    t.block_type AS block_type,
    t.block_value AS block_value,
    t.block_level_number AS block_level_number,
    psn.id AS creator_id,
    t.block_name AS block_name
FROM
    (
        VALUES         
            (1,'(LTL812/ABW52)-B-25-1-1-B','101',1,'replication block',1,1,'replicate'),
            (1,'(ABDHL0089/ABDHL120668)@195-B','102',2,'replication block',1,1,'replicate'),
            (1,'(CML494/OFP9)-12-2-1-1-2-B*5','103',3,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-119-1-1-B','104',4,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52//HTL437)-98-1-1-1-B','105',5,'replication block',1,1,'replicate'),
            (1,'CML547','106',6,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-108-1-1-B','107',7,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-178-1-2-B','108',8,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-73-1-2-B','109',9,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABDHL120918)@479-B','110',10,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-17-1-2-B','111',11,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-183-1-2-B','112',12,'replication block',1,1,'replicate'),
            (1,'(ABDHL0089/ABDHL120668)@19-B','113',13,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABT11)-B-16-1-1-B','114',14,'replication block',1,1,'replicate'),
            (1,'(ABDHL0089/ABDHL120918)@52-B','115',15,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-113-1-2-B','116',16,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABT11)-B-16-1-2-B','117',17,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-124-1-2-B','118',18,'replication block',1,1,'replicate'),
            (1,'(HTL437/RK132)-B-89-1-2-B','119',19,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABDHL120918)@653-B','120',20,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/CML543)@140-B','121',21,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABLTI0335)@107-B','122',22,'replication block',1,1,'replicate'),
            (1,'(LTL812/RK198)-B-112-1-2-B','123',23,'replication block',1,1,'replicate'),
            (1,'((CML442/KS23-6)-B)@103-B','124',24,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABT11)-B-8-1-2-B','125',25,'replication block',1,1,'replicate'),
            (1,'(ABDHL0089/ABLTI0136)@220-B','126',26,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABDHL120918)@393-B','127',27,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-178-1-1-B','128',28,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-120-1-1-B','129',29,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABDHL120918)@553-B','130',30,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-65-1-1-B','131',31,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-3-1-1-B','132',32,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-168-1-2-B','133',33,'replication block',1,1,'replicate'),
            (1,'(ABDHL0089/ABDHL120918)@200-B','134',34,'replication block',1,1,'replicate'),
            (1,'(LTL812/RK198)-B-53-1-2-B','135',35,'replication block',1,1,'replicate'),
            (1,'((CML537/KS523-5)-B)@10-B','136',36,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52//HTL437)-96-1-1-1-B','137',37,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-74-1-2-B','138',38,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-188-1-2-B','139',39,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABLTI0335)@14-B','140',40,'replication block',1,1,'replicate'),
            (1,'(CML494/OFP9)-12-2-1-1-1-B*4','141',41,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-189-1-2-B','142',42,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABW52)-B-65-1-2-B','143',43,'replication block',1,1,'replicate'),
            (1,'CML463','144',44,'replication block',1,1,'replicate'),
            (1,'CML495','145',45,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABDHL120918)@373-B','146',46,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABDHL120918)@299-B','147',47,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABDHL120918)@95-B','148',48,'replication block',1,1,'replicate'),
            (1,'(ABLTI0139/ABDHL120918)@246-B','149',49,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-157-1-2-B','150',50,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-31-1-2-B','151',51,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-31-1-1-B','152',52,'replication block',1,1,'replicate'),
            (1,'(LTL812/ABT11)-B-51-1-2-B','153',53,'replication block',1,1,'replicate'),
            (1,'CLYN261','154',54,'replication block',1,1,'replicate'),
            (1,'(ABDHL0089/ABDHL120918)@63-B','155',55,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52//HTL437)-98-1-1-2-B','156',56,'replication block',1,1,'replicate'),
            (1,'CLRCY034','157',57,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABW52//HTL437)-96-1-1-2-B','158',58,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABHB9)-B-168-1-1-B','159',59,'replication block',1,1,'replicate'),
            (1,'(HTL437/ABP38)-B-157-1-1-B','160',60,'replication block',1,1,'replicate'),
            (2,'(HTL437/ABP38)-B-188-1-2-B','201',61,'replication block',2,1,'replicate'),
            (2,'((CML442/KS23-6)-B)@103-B','202',62,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABT11)-B-16-1-2-B','203',63,'replication block',2,1,'replicate'),
            (2,'(ABDHL0089/ABDHL120918)@63-B','204',64,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-178-1-1-B','205',65,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52//HTL437)-96-1-1-1-B','206',66,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-65-1-1-B','207',67,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABDHL120918)@373-B','208',68,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABDHL120918)@553-B','209',69,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-157-1-2-B','210',70,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABDHL120918)@653-B','211',71,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABLTI0335)@14-B','212',72,'replication block',2,1,'replicate'),
            (2,'(ABDHL0089/ABDHL120918)@52-B','213',73,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-74-1-2-B','214',74,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52//HTL437)-98-1-1-2-B','215',75,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABT11)-B-8-1-2-B','216',76,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52//HTL437)-98-1-1-1-B','217',77,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABDHL120918)@95-B','218',78,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-189-1-2-B','219',79,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-31-1-2-B','220',80,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-108-1-1-B','221',81,'replication block',2,1,'replicate'),
            (2,'(LTL812/RK198)-B-112-1-2-B','222',82,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-73-1-2-B','223',83,'replication block',2,1,'replicate'),
            (2,'CML547','224',84,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABLTI0335)@107-B','225',85,'replication block',2,1,'replicate'),
            (2,'(CML494/OFP9)-12-2-1-1-1-B*4','226',86,'replication block',2,1,'replicate'),
            (2,'CML495','227',87,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-3-1-1-B','228',88,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABDHL120918)@246-B','229',89,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-178-1-2-B','230',90,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABDHL120918)@299-B','231',91,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABDHL120918)@479-B','232',92,'replication block',2,1,'replicate'),
            (2,'(HTL437/RK132)-B-89-1-2-B','233',93,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-124-1-2-B','234',94,'replication block',2,1,'replicate'),
            (2,'(ABDHL0089/ABDHL120668)@195-B','235',95,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-168-1-1-B','236',96,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-119-1-1-B','237',97,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABT11)-B-51-1-2-B','238',98,'replication block',2,1,'replicate'),
            (2,'(LTL812/RK198)-B-53-1-2-B','239',99,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/ABDHL120918)@393-B','240',100,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-113-1-2-B','241',101,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-17-1-2-B','242',102,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABP38)-B-157-1-1-B','243',103,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-65-1-2-B','244',104,'replication block',2,1,'replicate'),
            (2,'CLRCY034','245',105,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-31-1-1-B','246',106,'replication block',2,1,'replicate'),
            (2,'((CML537/KS523-5)-B)@10-B','247',107,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABT11)-B-16-1-1-B','248',108,'replication block',2,1,'replicate'),
            (2,'(ABDHL0089/ABDHL120918)@200-B','249',109,'replication block',2,1,'replicate'),
            (2,'(ABDHL0089/ABDHL120668)@19-B','250',110,'replication block',2,1,'replicate'),
            (2,'(ABDHL0089/ABLTI0136)@220-B','251',111,'replication block',2,1,'replicate'),
            (2,'(CML494/OFP9)-12-2-1-1-2-B*5','252',112,'replication block',2,1,'replicate'),
            (2,'(ABLTI0139/CML543)@140-B','253',113,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABW52//HTL437)-96-1-1-2-B','254',114,'replication block',2,1,'replicate'),
            (2,'CML463','255',115,'replication block',2,1,'replicate'),
            (2,'CLYN261','256',116,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-168-1-2-B','257',117,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-120-1-1-B','258',118,'replication block',2,1,'replicate'),
            (2,'(HTL437/ABHB9)-B-183-1-2-B','259',119,'replication block',2,1,'replicate'),
            (2,'(LTL812/ABW52)-B-25-1-1-B','260',120,'replication block',2,1,'replicate'),
            (3,'(ABLTI0139/ABDHL120918)@653-B','301',121,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-108-1-1-B','302',122,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABDHL120918)@393-B','303',123,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABDHL120918)@246-B','304',124,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABDHL120918)@373-B','305',125,'replication block',3,1,'replicate'),
            (3,'(ABDHL0089/ABDHL120918)@52-B','306',126,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABDHL120918)@95-B','307',127,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-3-1-1-B','308',128,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-25-1-1-B','309',129,'replication block',3,1,'replicate'),
            (3,'((CML537/KS523-5)-B)@10-B','310',130,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-73-1-2-B','311',131,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-178-1-1-B','312',132,'replication block',3,1,'replicate'),
            (3,'(LTL812/RK198)-B-53-1-2-B','313',133,'replication block',3,1,'replicate'),
            (3,'CML547','314',134,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABDHL120918)@479-B','315',135,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-120-1-1-B','316',136,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-189-1-2-B','317',137,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-157-1-1-B','318',138,'replication block',3,1,'replicate'),
            (3,'CML463','319',139,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABT11)-B-51-1-2-B','320',140,'replication block',3,1,'replicate'),
            (3,'(ABDHL0089/ABLTI0136)@220-B','321',141,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABLTI0335)@14-B','322',142,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABT11)-B-16-1-2-B','323',143,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-65-1-1-B','324',144,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52//HTL437)-96-1-1-1-B','325',145,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-124-1-2-B','326',146,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABLTI0335)@107-B','327',147,'replication block',3,1,'replicate'),
            (3,'CLRCY034','328',148,'replication block',3,1,'replicate'),
            (3,'(ABDHL0089/ABDHL120918)@200-B','329',149,'replication block',3,1,'replicate'),
            (3,'((CML442/KS23-6)-B)@103-B','330',150,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABDHL120918)@553-B','331',151,'replication block',3,1,'replicate'),
            (3,'(ABDHL0089/ABDHL120668)@19-B','332',152,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-178-1-2-B','333',153,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-113-1-2-B','334',154,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-31-1-2-B','335',155,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52//HTL437)-98-1-1-1-B','336',156,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-168-1-1-B','337',157,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/CML543)@140-B','338',158,'replication block',3,1,'replicate'),
            (3,'CLYN261','339',159,'replication block',3,1,'replicate'),
            (3,'CML495','340',160,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABT11)-B-8-1-2-B','341',161,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-31-1-1-B','342',162,'replication block',3,1,'replicate'),
            (3,'(CML494/OFP9)-12-2-1-1-2-B*5','343',163,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52//HTL437)-98-1-1-2-B','344',164,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-17-1-2-B','345',165,'replication block',3,1,'replicate'),
            (3,'(LTL812/RK198)-B-112-1-2-B','346',166,'replication block',3,1,'replicate'),
            (3,'(ABDHL0089/ABDHL120668)@195-B','347',167,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-168-1-2-B','348',168,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-183-1-2-B','349',169,'replication block',3,1,'replicate'),
            (3,'(CML494/OFP9)-12-2-1-1-1-B*4','350',170,'replication block',3,1,'replicate'),
            (3,'(ABDHL0089/ABDHL120918)@63-B','351',171,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABT11)-B-16-1-1-B','352',172,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-188-1-2-B','353',173,'replication block',3,1,'replicate'),
            (3,'(ABLTI0139/ABDHL120918)@299-B','354',174,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-74-1-2-B','355',175,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABW52//HTL437)-96-1-1-2-B','356',176,'replication block',3,1,'replicate'),
            (3,'(HTL437/RK132)-B-89-1-2-B','357',177,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABHB9)-B-119-1-1-B','358',178,'replication block',3,1,'replicate'),
            (3,'(LTL812/ABW52)-B-65-1-2-B','359',179,'replication block',3,1,'replicate'),
            (3,'(HTL437/ABP38)-B-157-1-2-B','360',180,'replication block',3,1,'replicate'),
            (4,'(LTL812/ABW52)-B-25-1-1-B','101',1,'replication block',1,2,'block'),
            (4,'(ABDHL0089/ABDHL120668)@195-B','102',2,'replication block',1,2,'block'),
            (4,'(CML494/OFP9)-12-2-1-1-2-B*5','103',3,'replication block',1,2,'block'),
            (4,'(HTL437/ABHB9)-B-119-1-1-B','104',4,'replication block',1,2,'block'),
            (4,'(HTL437/ABW52//HTL437)-98-1-1-1-B','105',5,'replication block',1,2,'block'),
            (4,'CML547','106',6,'replication block',1,2,'block'),
            (5,'(HTL437/ABHB9)-B-108-1-1-B','107',7,'replication block',2,2,'block'),
            (5,'(HTL437/ABP38)-B-178-1-2-B','108',8,'replication block',2,2,'block'),
            (5,'(HTL437/ABHB9)-B-73-1-2-B','109',9,'replication block',2,2,'block'),
            (5,'(ABLTI0139/ABDHL120918)@479-B','110',10,'replication block',2,2,'block'),
            (5,'(HTL437/ABHB9)-B-17-1-2-B','111',11,'replication block',2,2,'block'),
            (5,'(HTL437/ABHB9)-B-183-1-2-B','112',12,'replication block',2,2,'block'),
            (6,'(ABDHL0089/ABDHL120668)@19-B','113',13,'replication block',3,2,'block'),
            (6,'(LTL812/ABT11)-B-16-1-1-B','114',14,'replication block',3,2,'block'),
            (6,'(ABDHL0089/ABDHL120918)@52-B','115',15,'replication block',3,2,'block'),
            (6,'(LTL812/ABW52)-B-113-1-2-B','116',16,'replication block',3,2,'block'),
            (6,'(LTL812/ABT11)-B-16-1-2-B','117',17,'replication block',3,2,'block'),
            (6,'(LTL812/ABW52)-B-124-1-2-B','118',18,'replication block',3,2,'block'),
            (7,'(HTL437/RK132)-B-89-1-2-B','119',19,'replication block',4,2,'block'),
            (7,'(ABLTI0139/ABDHL120918)@653-B','120',20,'replication block',4,2,'block'),
            (7,'(ABLTI0139/CML543)@140-B','121',21,'replication block',4,2,'block'),
            (7,'(ABLTI0139/ABLTI0335)@107-B','122',22,'replication block',4,2,'block'),
            (7,'(LTL812/RK198)-B-112-1-2-B','123',23,'replication block',4,2,'block'),
            (7,'((CML442/KS23-6)-B)@103-B','124',24,'replication block',4,2,'block'),
            (8,'(LTL812/ABT11)-B-8-1-2-B','125',25,'replication block',5,2,'block'),
            (8,'(ABDHL0089/ABLTI0136)@220-B','126',26,'replication block',5,2,'block'),
            (8,'(ABLTI0139/ABDHL120918)@393-B','127',27,'replication block',5,2,'block'),
            (8,'(HTL437/ABP38)-B-178-1-1-B','128',28,'replication block',5,2,'block'),
            (8,'(HTL437/ABHB9)-B-120-1-1-B','129',29,'replication block',5,2,'block'),
            (8,'(ABLTI0139/ABDHL120918)@553-B','130',30,'replication block',5,2,'block'),
            (9,'(LTL812/ABW52)-B-65-1-1-B','131',31,'replication block',6,2,'block'),
            (9,'(HTL437/ABHB9)-B-3-1-1-B','132',32,'replication block',6,2,'block'),
            (9,'(HTL437/ABHB9)-B-168-1-2-B','133',33,'replication block',6,2,'block'),
            (9,'(ABDHL0089/ABDHL120918)@200-B','134',34,'replication block',6,2,'block'),
            (9,'(LTL812/RK198)-B-53-1-2-B','135',35,'replication block',6,2,'block'),
            (9,'((CML537/KS523-5)-B)@10-B','136',36,'replication block',6,2,'block'),
            (10,'(HTL437/ABW52//HTL437)-96-1-1-1-B','137',37,'replication block',7,2,'block'),
            (10,'(LTL812/ABW52)-B-74-1-2-B','138',38,'replication block',7,2,'block'),
            (10,'(HTL437/ABP38)-B-188-1-2-B','139',39,'replication block',7,2,'block'),
            (10,'(ABLTI0139/ABLTI0335)@14-B','140',40,'replication block',7,2,'block'),
            (10,'(CML494/OFP9)-12-2-1-1-1-B*4','141',41,'replication block',7,2,'block'),
            (10,'(HTL437/ABP38)-B-189-1-2-B','142',42,'replication block',7,2,'block'),
            (11,'(LTL812/ABW52)-B-65-1-2-B','143',43,'replication block',8,2,'block'),
            (11,'CML463','144',44,'replication block',8,2,'block'),
            (11,'CML495','145',45,'replication block',8,2,'block'),
            (11,'(ABLTI0139/ABDHL120918)@373-B','146',46,'replication block',8,2,'block'),
            (11,'(ABLTI0139/ABDHL120918)@299-B','147',47,'replication block',8,2,'block'),
            (11,'(ABLTI0139/ABDHL120918)@95-B','148',48,'replication block',8,2,'block'),
            (12,'(ABLTI0139/ABDHL120918)@246-B','149',49,'replication block',9,2,'block'),
            (12,'(HTL437/ABP38)-B-157-1-2-B','150',50,'replication block',9,2,'block'),
            (12,'(HTL437/ABHB9)-B-31-1-2-B','151',51,'replication block',9,2,'block'),
            (12,'(HTL437/ABHB9)-B-31-1-1-B','152',52,'replication block',9,2,'block'),
            (12,'(LTL812/ABT11)-B-51-1-2-B','153',53,'replication block',9,2,'block'),
            (12,'CLYN261','154',54,'replication block',9,2,'block'),
            (13,'(ABDHL0089/ABDHL120918)@63-B','155',55,'replication block',10,2,'block'),
            (13,'(HTL437/ABW52//HTL437)-98-1-1-2-B','156',56,'replication block',10,2,'block'),
            (13,'CLRCY034','157',57,'replication block',10,2,'block'),
            (13,'(HTL437/ABW52//HTL437)-96-1-1-2-B','158',58,'replication block',10,2,'block'),
            (13,'(HTL437/ABHB9)-B-168-1-1-B','159',59,'replication block',10,2,'block'),
            (13,'(HTL437/ABP38)-B-157-1-1-B','160',60,'replication block',10,2,'block'),
            (4,'(HTL437/ABP38)-B-188-1-2-B','201',61,'replication block',1,2,'block'),
            (4,'((CML442/KS23-6)-B)@103-B','202',62,'replication block',1,2,'block'),
            (4,'(LTL812/ABT11)-B-16-1-2-B','203',63,'replication block',1,2,'block'),
            (4,'(ABDHL0089/ABDHL120918)@63-B','204',64,'replication block',1,2,'block'),
            (4,'(HTL437/ABP38)-B-178-1-1-B','205',65,'replication block',1,2,'block'),
            (4,'(HTL437/ABW52//HTL437)-96-1-1-1-B','206',66,'replication block',1,2,'block'),
            (5,'(LTL812/ABW52)-B-65-1-1-B','207',67,'replication block',2,2,'block'),
            (5,'(ABLTI0139/ABDHL120918)@373-B','208',68,'replication block',2,2,'block'),
            (5,'(ABLTI0139/ABDHL120918)@553-B','209',69,'replication block',2,2,'block'),
            (5,'(HTL437/ABP38)-B-157-1-2-B','210',70,'replication block',2,2,'block'),
            (5,'(ABLTI0139/ABDHL120918)@653-B','211',71,'replication block',2,2,'block'),
            (5,'(ABLTI0139/ABLTI0335)@14-B','212',72,'replication block',2,2,'block'),
            (6,'(ABDHL0089/ABDHL120918)@52-B','213',73,'replication block',3,2,'block'),
            (6,'(LTL812/ABW52)-B-74-1-2-B','214',74,'replication block',3,2,'block'),
            (6,'(HTL437/ABW52//HTL437)-98-1-1-2-B','215',75,'replication block',3,2,'block'),
            (6,'(LTL812/ABT11)-B-8-1-2-B','216',76,'replication block',3,2,'block'),
            (6,'(HTL437/ABW52//HTL437)-98-1-1-1-B','217',77,'replication block',3,2,'block'),
            (6,'(ABLTI0139/ABDHL120918)@95-B','218',78,'replication block',3,2,'block'),
            (7,'(HTL437/ABP38)-B-189-1-2-B','219',79,'replication block',4,2,'block'),
            (7,'(HTL437/ABHB9)-B-31-1-2-B','220',80,'replication block',4,2,'block'),
            (7,'(HTL437/ABHB9)-B-108-1-1-B','221',81,'replication block',4,2,'block'),
            (7,'(LTL812/RK198)-B-112-1-2-B','222',82,'replication block',4,2,'block'),
            (7,'(HTL437/ABHB9)-B-73-1-2-B','223',83,'replication block',4,2,'block'),
            (7,'CML547','224',84,'replication block',4,2,'block'),
            (8,'(ABLTI0139/ABLTI0335)@107-B','225',85,'replication block',5,2,'block'),
            (8,'(CML494/OFP9)-12-2-1-1-1-B*4','226',86,'replication block',5,2,'block'),
            (8,'CML495','227',87,'replication block',5,2,'block'),
            (8,'(HTL437/ABHB9)-B-3-1-1-B','228',88,'replication block',5,2,'block'),
            (8,'(ABLTI0139/ABDHL120918)@246-B','229',89,'replication block',5,2,'block'),
            (8,'(HTL437/ABP38)-B-178-1-2-B','230',90,'replication block',5,2,'block'),
            (9,'(ABLTI0139/ABDHL120918)@299-B','231',91,'replication block',6,2,'block'),
            (9,'(ABLTI0139/ABDHL120918)@479-B','232',92,'replication block',6,2,'block'),
            (9,'(HTL437/RK132)-B-89-1-2-B','233',93,'replication block',6,2,'block'),
            (9,'(LTL812/ABW52)-B-124-1-2-B','234',94,'replication block',6,2,'block'),
            (9,'(ABDHL0089/ABDHL120668)@195-B','235',95,'replication block',6,2,'block'),
            (9,'(HTL437/ABHB9)-B-168-1-1-B','236',96,'replication block',6,2,'block'),
            (10,'(HTL437/ABHB9)-B-119-1-1-B','237',97,'replication block',7,2,'block'),
            (10,'(LTL812/ABT11)-B-51-1-2-B','238',98,'replication block',7,2,'block'),
            (10,'(LTL812/RK198)-B-53-1-2-B','239',99,'replication block',7,2,'block'),
            (10,'(ABLTI0139/ABDHL120918)@393-B','240',100,'replication block',7,2,'block'),
            (10,'(LTL812/ABW52)-B-113-1-2-B','241',101,'replication block',7,2,'block'),
            (10,'(HTL437/ABHB9)-B-17-1-2-B','242',102,'replication block',7,2,'block'),
            (11,'(HTL437/ABP38)-B-157-1-1-B','243',103,'replication block',8,2,'block'),
            (11,'(LTL812/ABW52)-B-65-1-2-B','244',104,'replication block',8,2,'block'),
            (11,'CLRCY034','245',105,'replication block',8,2,'block'),
            (11,'(HTL437/ABHB9)-B-31-1-1-B','246',106,'replication block',8,2,'block'),
            (11,'((CML537/KS523-5)-B)@10-B','247',107,'replication block',8,2,'block'),
            (11,'(LTL812/ABT11)-B-16-1-1-B','248',108,'replication block',8,2,'block'),
            (12,'(ABDHL0089/ABDHL120918)@200-B','249',109,'replication block',9,2,'block'),
            (12,'(ABDHL0089/ABDHL120668)@19-B','250',110,'replication block',9,2,'block'),
            (12,'(ABDHL0089/ABLTI0136)@220-B','251',111,'replication block',9,2,'block'),
            (12,'(CML494/OFP9)-12-2-1-1-2-B*5','252',112,'replication block',9,2,'block'),
            (12,'(ABLTI0139/CML543)@140-B','253',113,'replication block',9,2,'block'),
            (12,'(HTL437/ABW52//HTL437)-96-1-1-2-B','254',114,'replication block',9,2,'block'),
            (13,'CML463','255',115,'replication block',10,2,'block'),
            (13,'CLYN261','256',116,'replication block',10,2,'block'),
            (13,'(HTL437/ABHB9)-B-168-1-2-B','257',117,'replication block',10,2,'block'),
            (13,'(HTL437/ABHB9)-B-120-1-1-B','258',118,'replication block',10,2,'block'),
            (13,'(HTL437/ABHB9)-B-183-1-2-B','259',119,'replication block',10,2,'block'),
            (13,'(LTL812/ABW52)-B-25-1-1-B','260',120,'replication block',10,2,'block'),
            (4,'(ABLTI0139/ABDHL120918)@653-B','301',121,'replication block',1,2,'block'),
            (4,'(HTL437/ABHB9)-B-108-1-1-B','302',122,'replication block',1,2,'block'),
            (4,'(ABLTI0139/ABDHL120918)@393-B','303',123,'replication block',1,2,'block'),
            (4,'(ABLTI0139/ABDHL120918)@246-B','304',124,'replication block',1,2,'block'),
            (4,'(ABLTI0139/ABDHL120918)@373-B','305',125,'replication block',1,2,'block'),
            (4,'(ABDHL0089/ABDHL120918)@52-B','306',126,'replication block',1,2,'block'),
            (5,'(ABLTI0139/ABDHL120918)@95-B','307',127,'replication block',2,2,'block'),
            (5,'(HTL437/ABHB9)-B-3-1-1-B','308',128,'replication block',2,2,'block'),
            (5,'(LTL812/ABW52)-B-25-1-1-B','309',129,'replication block',2,2,'block'),
            (5,'((CML537/KS523-5)-B)@10-B','310',130,'replication block',2,2,'block'),
            (5,'(HTL437/ABHB9)-B-73-1-2-B','311',131,'replication block',2,2,'block'),
            (5,'(HTL437/ABP38)-B-178-1-1-B','312',132,'replication block',2,2,'block'),
            (6,'(LTL812/RK198)-B-53-1-2-B','313',133,'replication block',3,2,'block'),
            (6,'CML547','314',134,'replication block',3,2,'block'),
            (6,'(ABLTI0139/ABDHL120918)@479-B','315',135,'replication block',3,2,'block'),
            (6,'(HTL437/ABHB9)-B-120-1-1-B','316',136,'replication block',3,2,'block'),
            (6,'(HTL437/ABP38)-B-189-1-2-B','317',137,'replication block',3,2,'block'),
            (6,'(HTL437/ABP38)-B-157-1-1-B','318',138,'replication block',3,2,'block'),
            (7,'CML463','319',139,'replication block',4,2,'block'),
            (7,'(LTL812/ABT11)-B-51-1-2-B','320',140,'replication block',4,2,'block'),
            (7,'(ABDHL0089/ABLTI0136)@220-B','321',141,'replication block',4,2,'block'),
            (7,'(ABLTI0139/ABLTI0335)@14-B','322',142,'replication block',4,2,'block'),
            (7,'(LTL812/ABT11)-B-16-1-2-B','323',143,'replication block',4,2,'block'),
            (7,'(LTL812/ABW52)-B-65-1-1-B','324',144,'replication block',4,2,'block'),
            (8,'(HTL437/ABW52//HTL437)-96-1-1-1-B','325',145,'replication block',5,2,'block'),
            (8,'(LTL812/ABW52)-B-124-1-2-B','326',146,'replication block',5,2,'block'),
            (8,'(ABLTI0139/ABLTI0335)@107-B','327',147,'replication block',5,2,'block'),
            (8,'CLRCY034','328',148,'replication block',5,2,'block'),
            (8,'(ABDHL0089/ABDHL120918)@200-B','329',149,'replication block',5,2,'block'),
            (8,'((CML442/KS23-6)-B)@103-B','330',150,'replication block',5,2,'block'),
            (9,'(ABLTI0139/ABDHL120918)@553-B','331',151,'replication block',6,2,'block'),
            (9,'(ABDHL0089/ABDHL120668)@19-B','332',152,'replication block',6,2,'block'),
            (9,'(HTL437/ABP38)-B-178-1-2-B','333',153,'replication block',6,2,'block'),
            (9,'(LTL812/ABW52)-B-113-1-2-B','334',154,'replication block',6,2,'block'),
            (9,'(HTL437/ABHB9)-B-31-1-2-B','335',155,'replication block',6,2,'block'),
            (9,'(HTL437/ABW52//HTL437)-98-1-1-1-B','336',156,'replication block',6,2,'block'),
            (10,'(HTL437/ABHB9)-B-168-1-1-B','337',157,'replication block',7,2,'block'),
            (10,'(ABLTI0139/CML543)@140-B','338',158,'replication block',7,2,'block'),
            (10,'CLYN261','339',159,'replication block',7,2,'block'),
            (10,'CML495','340',160,'replication block',7,2,'block'),
            (10,'(LTL812/ABT11)-B-8-1-2-B','341',161,'replication block',7,2,'block'),
            (10,'(HTL437/ABHB9)-B-31-1-1-B','342',162,'replication block',7,2,'block'),
            (11,'(CML494/OFP9)-12-2-1-1-2-B*5','343',163,'replication block',8,2,'block'),
            (11,'(HTL437/ABW52//HTL437)-98-1-1-2-B','344',164,'replication block',8,2,'block'),
            (11,'(HTL437/ABHB9)-B-17-1-2-B','345',165,'replication block',8,2,'block'),
            (11,'(LTL812/RK198)-B-112-1-2-B','346',166,'replication block',8,2,'block'),
            (11,'(ABDHL0089/ABDHL120668)@195-B','347',167,'replication block',8,2,'block'),
            (11,'(HTL437/ABHB9)-B-168-1-2-B','348',168,'replication block',8,2,'block'),
            (12,'(HTL437/ABHB9)-B-183-1-2-B','349',169,'replication block',9,2,'block'),
            (12,'(CML494/OFP9)-12-2-1-1-1-B*4','350',170,'replication block',9,2,'block'),
            (12,'(ABDHL0089/ABDHL120918)@63-B','351',171,'replication block',9,2,'block'),
            (12,'(LTL812/ABT11)-B-16-1-1-B','352',172,'replication block',9,2,'block'),
            (12,'(HTL437/ABP38)-B-188-1-2-B','353',173,'replication block',9,2,'block'),
            (12,'(ABLTI0139/ABDHL120918)@299-B','354',174,'replication block',9,2,'block'),
            (13,'(LTL812/ABW52)-B-74-1-2-B','355',175,'replication block',10,2,'block'),
            (13,'(HTL437/ABW52//HTL437)-96-1-1-2-B','356',176,'replication block',10,2,'block'),
            (13,'(HTL437/RK132)-B-89-1-2-B','357',177,'replication block',10,2,'block'),
            (13,'(HTL437/ABHB9)-B-119-1-1-B','358',178,'replication block',10,2,'block'),
            (13,'(LTL812/ABW52)-B-65-1-2-B','359',179,'replication block',10,2,'block'),
            (13,'(HTL437/ABP38)-B-157-1-2-B','360',180,'replication block',10,2,'block')
) AS t (design_id, entry_name, plot_code, plot_number, block_type, block_value, block_level_number, block_name)
INNER JOIN
    experiment.occurrence occ
ON
    occ.occurrence_name='AF2021_002#OCC-02'
INNER JOIN
    experiment.entry_list els
ON
    els.entry_list_name = 'KE-AYT-2021-WS Entry List'
INNER JOIN
     tenant.person psn
ON
    psn.username = 'nicola.costa';



--rollback DELETE FROM experiment.experiment_design WHERE occurrence_id IN (SELECT id FROM experiment.occurrence WHERE occurrence_name IN ('AF2021_002#OCC-01','AF2021_002#OCC-02','AF2021_002#OCC-03'));