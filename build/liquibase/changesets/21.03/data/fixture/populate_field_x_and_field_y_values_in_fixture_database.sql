--liquibase formatted sql

--changeset postgres:populate_field_x_and_field_y_values_in_fixture_database context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-152 Populate Field X and Field Y values in fixture database



UPDATE
    experiment.plot
SET
    field_x = design_x,
    field_y = design_y
WHERE
    field_x IS NULL
    AND field_y IS NULL
AND
    design_x IS NOT NULL
    AND design_y IS NOT NULL;



-- rollback
--rollback UPDATE
--rollback     experiment.plot
--rollback SET
--rollback     field_x = NULL,
--rollback     field_y = NULL
--rollback WHERE
--rollback     occurrence_id 
--rollback IN (
--rollback     2468,    1798,    1037,    1521,    1489,
--rollback     2335,    1989,    3384,    1855,    1362,
--rollback     826,    2846,    2314,    1799,    1797,
--rollback     2466,    1840,    3377,    1567,    1003,
--rollback     1825,    1397,    236,    919,    836,
--rollback     2740,    1408,    1193,    2632,    1036,
--rollback     2795,    390,    2179,    2738,    1085,
--rollback     3382,    3389,    700,    1771,    2225,
--rollback     1004,    843,    2332,    1361,    1016,
--rollback     1129,    2702,    1367,    1313,    1015,
--rollback     1124,    1789,    1218,    2313,    721,
--rollback     1100,    2181,    705,    2842,    1988,
--rollback     1841,    2837,    1028,    3028,    698,
--rollback     2059,    1201,    3386,    1314,    233,
--rollback     1115,    1990,    1383,    1746,    2026,
--rollback     231,    340,    2248,    1785,    1572,
--rollback     1982,    1603,    1358,    2183,    714,
--rollback     2412,    1047,    2166,    1786,    230,
--rollback     1381,    1005,    359,    1360,    1888,
--rollback     1769,    1099,    1763,    2323,    1105,
--rollback     1061,    3390,    1382,    1405,    1200,
--rollback     2177,    2739,    557,    228,    2701,
--rollback     1991,    3026,    1800,    1889,    2338,
--rollback     2249,    2675,    511,    1861,    378,
--rollback     2178,    1414,    170,    1788,    3378,
--rollback     916,    473,    323,    1413,    737,
--rollback     2247,    1697,    2232,    1384,    821,
--rollback     715,    235,    3391,    2233,    1396,
--rollback     1110,    903,    716,    2608,    3041,
--rollback     2336,    3383,    2442,    2235,    3379,
--rollback     1527,    3392,    232,    1520,    3044,
--rollback     1981,    2324,    1378,    1765,    1796,
--rollback     907,    1380,    1766,    2465,    720,
--rollback     3385,    2942,    1764,    1217,    894,
--rollback     2797,    205,    2676,    1411,    706,
--rollback     895,    1956,    1377,    1017,    1794,
--rollback     1114,    2467,    2527,    2531,    2312,
--rollback     524,    3380,    3043,    3024,    1793,
--rollback     2255,    893,    463,    385,    3381,
--rollback     1073,    206,    1389,    2134,    1801,
--rollback     1412,    2184,    2224,    2254,    1792,
--rollback     1621,    3388,    421,    1868,    3387,
--rollback     234,    3030,    1191,    2359,    1131,
--rollback     1125,    885,    1955
--rollback     )
--rollback AND
--rollback     design_x IS NOT NULL
--rollback     AND design_y IS NOT NULL
--rollback AND
--rollback     field_x IS NOT NULL
--rollback     AND field_y IS NOT NULL;
