--liquibase formatted sql

--changeset postgres:hm_dcp_001_update_data_terminal_create_transaction_dataset context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-4439 CB-DC: Allow uploading of harvest data to DCP crosses



CREATE OR REPLACE FUNCTION data_terminal.create_transaction_dataset(
	var_transaction_id integer,
	var_user_id integer,
	var_dataset json,
	var_variables text[])
    RETURNS character varying
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$

DECLARE
    tableName text; 
    columnString text; 
    items RECORD; 
    locationId integer;	
    insertedCount integer;	
    updatedCount integer; 
    insertedCrossCount integer;	
    updatedCrossCount integer; 
    insertedNotHarvestedCount integer;
    insertedCrossNotHarvestedCount integer;
begin
    insertedCrossCount := 0;	
    updatedCrossCount := 0; 
    updatedCount := 0;
    insertedCount := 0;
    tableName := 'dataset_'||var_transaction_id||'_'||var_user_id;
    columnString := 'plot_id, cross_id, '|| array_to_string(var_variables, ',');

    SELECT location_id INTO locationId FROM data_terminal."transaction" t WHERE id = var_transaction_id;
    
    
    execute '
    CREATE TEMP TABLE dataset_'||var_transaction_id||'_'||var_user_id||'(
        id serial NOT NULL,
        plot_id text,
        cross_id text, 
        '||(SELECT string_agg(a.concat,',') FROM (SELECT concat(unnest(var_variables),' text'))a)||'
    ) ON COMMIT DROP
    ';

    
    execute ' 
        WITH data AS (
            SELECT 
            '''||var_dataset||'''
            ::jsonb AS jsondata
        )
        INSERT INTO '||tableName ||'('|| columnString || ') 
        SELECT 
            (elems->>''plot_id'')::int as plot_id,
            (elems->>''cross_id'')::int as cross_id,
            '||(SELECT STRING_AGG(b.concat, ',') from (SELECT CONCAT('(elems->>''',unnest(var_variables),''')::text as ',unnest(var_variables)))b )||'
        FROM 
            data,
            jsonb_array_elements(jsondata) AS elems
    ';
    
    
    FOR items IN EXECUTE format('SELECT * FROM %I WHERE plot_id IS NULL AND cross_id IS NULL ORDER BY id ASC', tableName)
    LOOP
        return 'Record '|| items.id || ' does not have PLOT_ID or CROSS_ID.';
    END LOOP;
   
    
    FOR items IN EXECUTE format('SELECT * FROM %I WHERE plot_id IS NOT NULL ORDER BY id ASC', tableName)
    LOOP
        if  (items.cross_id !~ E'^\\d+$') then 
            return 'Record '|| items.id || ' must be an integer';

        elsif NOT EXISTS(SELECT 1 FROM experiment.plot WHERE location_id = locationId AND id = items.plot_id::integer AND is_void = FALSE) then 
            return 'Record '|| items.id || ' with PLOT_ID of '|| items.plot_id || ' is not a plot of location.';

        end if;
    END LOOP;
    
    
    EXECUTE (
    SELECT '
        WITH rows AS (
            UPDATE data_terminal.transaction_dataset td
            SET 
                is_suppressed = FALSE,
                value = a.value,
                modification_timestamp = now(),
                modifier_id = '|| var_user_id ||'
                
            FROM
            (
                SELECT 
                    DISTINCT ON (u.variable,u.val,t.plot_id)
                    '|| var_transaction_id ||' AS transaction_id,
                    u.val AS value,
                    '||var_user_id||' AS creator_id,
                    ''plot_data'' AS entity,
                    t.plot_id::integer AS entity_id,
                    (SELECT id FROM master.variable WHERE abbrev = upper(u.variable)) AS variable_id
                FROM  
                    ' || tableName || ' t
                        LEFT JOIN LATERAL ( VALUES ' || string_agg(format('(''%s'', t.%I)', tempVar, tempVar), ', ') || ') u(variable, val) ON true,
                    master.variable v
                WHERE 
                    lower(v.abbrev) = u.variable
                    AND trim(u.val) <> ''''
                    AND plot_id IS NOT NULL
                    AND NOT EXISTS(
                      SELECT 1 
                      FROM
                          experiment.plot_data pd
                      WHERE 
                          t.plot_id::int = pd.plot_id
                          AND pd.data_value = u.val
                          AND pd.variable_id = v.id
                          AND pd.is_void = FALSE
                    )
            )a
            WHERE 
                td.entity_id = a.entity_id
                AND td.transaction_id = '||var_transaction_id||'
                AND td.variable_id = a.variable_id
                AND td.entity = ''plot_data''
                AND td.value IS NOT NULL 
                AND td.value <> a.value
                AND td.is_void = FALSE
            returning id 
        ) SELECT count(1) FROM rows' 
    FROM   
          (SELECT unnest(var_variables) except SELECT unnest(array['plot_id']) )t(tempVar)
      ) INTO updatedCount;

    
    EXECUTE (
    SELECT '
        WITH rows AS (
            UPDATE data_terminal.transaction_dataset td
            SET 
                is_suppressed = FALSE,
                value = a.value,
                modification_timestamp = now(),
                modifier_id = '|| var_user_id ||'
                
            FROM
            (
                SELECT 
                    DISTINCT ON (u.variable,u.val,t.cross_id)
                    '|| var_transaction_id ||' AS transaction_id,
                    u.val AS value,
                    '||var_user_id||' AS creator_id,
                    ''cross_data'' AS entity,
                    t.cross_id::integer AS entity_id,
                    (SELECT id FROM master.variable WHERE abbrev = upper(u.variable)) AS variable_id
                FROM  
                    ' || tableName || ' t
                        LEFT JOIN LATERAL ( VALUES ' || string_agg(format('(''%s'', t.%I)', tempVar, tempVar), ', ') || ') u(variable, val) ON true,
                    master.variable v
                WHERE 
                    lower(v.abbrev) = u.variable
                    AND trim(u.val) <> ''''
                    AND cross_id IS NOT NULL
                    AND NOT EXISTS(
                      SELECT 1 
                      FROM
                          germplasm.cross_data cd
                      WHERE 
                          t.cross_id::int = cd.cross_id
                          AND cd.data_value = u.val
                          AND cd.variable_id = v.id
                          AND cd.is_void = FALSE
                    )
            )a
            WHERE 
                td.entity_id = a.entity_id
                AND td.transaction_id = '||var_transaction_id||'
                AND td.variable_id = a.variable_id
                AND td.entity = ''cross_data''
                AND td.value IS NOT NULL 
                AND td.value <> a.value
                AND td.is_void = FALSE
            returning id 
        ) SELECT count(1) FROM rows' 
    FROM   
          (SELECT unnest(var_variables) except SELECT unnest(array['cross_id']) )t(tempVar)
      ) INTO updatedCrossCount;
         
    EXECUTE (
    SELECT '
        WITH rows AS (
          INSERT INTO data_terminal.transaction_dataset 
            (transaction_id, data_unit, variable_id, value , creator_id, entity, entity_id , 
              is_void, is_generated, is_suppressed, notes)
            SELECT 
                DISTINCT ON (u.variable,value,t.plot_id)
                '|| var_transaction_id ||' AS transaction_id,
                ''plot'' AS data_unit,
                (SELECT id FROM master.variable WHERE abbrev = upper(u.variable)) AS variable_id, 
                u.val AS value,
                '||var_user_id||' AS creator_id,
                ''plot_data'' AS entity,
                t.plot_id::integer AS entity_id,
                FALSE AS is_void,
                FALSE AS is_generated,
                FALSE AS is_suppressed,
		        ''uploaded''
            FROM  
                ' || tableName || ' t
                    LEFT JOIN LATERAL ( VALUES ' || string_agg(format('(''%s'', t.%I)', y, y), ', ') || ') u(variable, val) ON true
                    LEFT JOIN experiment.plot p ON p.id = t.plot_id::int,
                master.variable v
            WHERE 
                lower(v.abbrev) = u.variable
                AND v.abbrev IN (''HV_METH_DISC'', ''HVDATE_CONT'', ''NO_OF_PLANTS'', ''SPECIFIC_PLANT'', ''PANNO_SEL'', ''NO_OF_EARS'')
                AND plot_id IS NOT NULL
                AND NOT EXISTS(
                    SELECT 1 
                    FROM
                        data_terminal.transaction_dataset td
                    WHERE 
                        td.transaction_id= '||var_transaction_id||'
                        AND t.plot_id::int = td.entity_id
                        AND td.value IS NOT NULL 
                        AND td.value = u.val
                        AND td.variable_id = v.id
                        AND td.is_void = FALSE
                        AND td.entity = ''plot_data''
                ) AND NOT EXISTS(
                    SELECT 1 
                    FROM
                        experiment.plot_data pd
                    WHERE 
                        t.plot_id::int = pd.plot_id
                        AND pd.data_value = u.val
                        AND pd.variable_id = v.id
                        AND pd.is_void = FALSE
                )
                AND trim(u.val) <> ''''
            RETURNING id 
        ) SELECT count(1) FROM rows' 
    FROM   
        (SELECT unnest(var_variables) except SELECT unnest(array['plot_id']) )t(y)
    )INTO insertedNotHarvestedCount;
    EXECUTE (
    SELECT '
        WITH rows AS (
          INSERT INTO data_terminal.transaction_dataset 
            (transaction_id, data_unit, variable_id, value , creator_id, entity, entity_id , 
              is_void, is_generated, is_suppressed, notes)
            SELECT 
                DISTINCT ON (u.variable,value,t.plot_id)
                '|| var_transaction_id ||' AS transaction_id,
                ''plot'' AS data_unit,
                (SELECT id FROM master.variable WHERE abbrev = upper(u.variable)) AS variable_id, 
                u.val AS value,
                '||var_user_id||' AS creator_id,
                ''plot_data'' AS entity,
                t.plot_id::integer AS entity_id,
                FALSE AS is_void,
                FALSE AS is_generated,
                FALSE AS is_suppressed,
		        ''uploaded''
            FROM  
                ' || tableName || ' t
                    LEFT JOIN LATERAL ( VALUES ' || string_agg(format('(''%s'', t.%I)', y, y), ', ') || ') u(variable, val) ON true
                    LEFT JOIN experiment.plot p ON p.id = t.plot_id::int,
                master.variable v
                
            WHERE 
                lower(v.abbrev) = u.variable
                AND v.abbrev NOT IN (''HV_METH_DISC'', ''HVDATE_CONT'', ''NO_OF_PLANTS'', ''SPECIFIC_PLANT'', ''PANNO_SEL'', ''NO_OF_EARS'')
                AND plot_id IS NOT NULL
                AND NOT EXISTS(
                    SELECT 1 
                    FROM
                        data_terminal.transaction_dataset td
                    WHERE 
                        td.transaction_id= '||var_transaction_id||'
                        AND t.plot_id::int = td.entity_id
                        AND td.value IS NOT NULL 
                        AND td.value = u.val
                        AND td.variable_id = v.id
                        AND td.is_void = FALSE
                        AND td.entity = ''plot_data''
                ) AND NOT EXISTS(
                    SELECT 1 
                    FROM
                        experiment.plot_data pd
                    WHERE 
                        t.plot_id::int = pd.plot_id
                        AND pd.data_value = u.val
                        AND pd.variable_id = v.id
                        AND pd.is_void = FALSE
                )
                AND trim(u.val) <> ''''
            RETURNING id 
        ) SELECT count(1) FROM rows' 
    FROM   
        (SELECT unnest(var_variables) except SELECT unnest(array['plot_id']) )t(y)
    )INTO insertedCount;

   EXECUTE (
    SELECT '
        WITH rows AS (
          INSERT INTO data_terminal.transaction_dataset 
            (transaction_id, data_unit, variable_id, value , creator_id, entity, entity_id , 
              is_void, is_generated, is_suppressed, notes)
            SELECT 
                DISTINCT ON (u.variable,value,t.cross_id)
                '|| var_transaction_id ||' AS transaction_id,
                ''cross'' AS data_unit,
                (SELECT id FROM master.variable WHERE abbrev = upper(u.variable)) AS variable_id, 
                u.val AS value,
                '||var_user_id||' AS creator_id,
                ''cross_data'' AS entity,
                t.cross_id::integer AS entity_id,
                FALSE AS is_void,
                FALSE AS is_generated,
                FALSE AS is_suppressed,
	   			''uploaded''
            FROM  
                ' || tableName || ' t
                    LEFT   JOIN LATERAL ( VALUES ' || string_agg(format('(''%s'', t.%I)', y, y), ', ') || ') u(variable, val) ON true
                    LEFT JOIN germplasm.cross AS crosses ON crosses.id = t.cross_id::int
                    LEFT JOIN germplasm.cross_parent female 
                        ON female.cross_id = crosses.id AND female.parent_role = ''female''
                            AND female.is_void = FALSE
                    LEFT JOIN germplasm.cross_parent male 
                        ON male.cross_id = crosses.id AND male.parent_role = ''male''
                            AND male.is_void = FALSE
                    LEFT JOIN germplasm.germplasm female_germplasm ON female_germplasm.id = female.germplasm_id
                    LEFT JOIN germplasm.germplasm male_germplasm ON male_germplasm.id = male.germplasm_id
                    LEFT JOIN tenant.crop female_crop ON female_crop.id = female_germplasm.crop_id
                    LEFT JOIN tenant.crop male_crop ON male_crop.id = male_germplasm.crop_id,
                master.variable v
            WHERE 
                lower(v.abbrev) = u.variable
                AND crosses.harvest_status <> ''COMPLETED''
                AND 
                (
                    (
                        crosses.cross_method = ''selfing''
                        AND v.abbrev IN (''DATE_CROSSED'', ''HV_METH_DISC'', 
                        ''NO_OF_SEED'', ''HVDATE_CONT'',
                        ''NO_OF_PLANTS'', ''PANNO_SEL'', 
                        ''SPECIFIC_PLANT'', ''NO_OF_EARS'')
                    ) OR
                    (
                        female_crop.crop_code = ''RICE'' AND male_crop.crop_code = ''RICE''
                        AND v.abbrev IN (''DATE_CROSSED'', ''HV_METH_DISC'', 
                        ''NO_OF_SEED'', ''HVDATE_CONT'')
                    ) OR
                    (
                        female_crop.crop_code IN (''MAIZE'',''WHEAT'',''COWPEA'',''SOYBEAN'',''CHICKPEA'',''GROUNDNUT'',''SORGHUM'',''FINGERMILLET'',''PEARLMILLET'',''PIGEONPEA'') 
                        AND male_crop.crop_code IN (''MAIZE'',''WHEAT'',''COWPEA'',''SOYBEAN'',''CHICKPEA'',''GROUNDNUT'',''SORGHUM'',''FINGERMILLET'',''PEARLMILLET'',''PIGEONPEA'')
                        AND v.abbrev IN (''HV_METH_DISC'', ''HVDATE_CONT'')
                    )
 
                )
                AND t.cross_id IS NOT NULL
                AND NOT EXISTS(
                    SELECT 1 
                    FROM
                        data_terminal.transaction_dataset td
                    WHERE 
                        td.transaction_id= '||var_transaction_id||'
                        AND t.cross_id::int = td.entity_id
                        AND td.value IS NOT NULL 
                        AND td.value = u.val
                        AND td.variable_id = v.id
                        AND td.is_void = FALSE
                        AND td.entity = ''cross_data''
                ) AND NOT EXISTS(
                    SELECT 1 
                    FROM
                        germplasm.cross_data cd
                    WHERE 
                        t.cross_id::int = cd.cross_id
                        AND cd.data_value = u.val
                        AND cd.variable_id = v.id
                        AND cd.is_void = FALSE
                )
                AND trim(u.val) <> ''''
            RETURNING id 
        ) SELECT count(1) FROM rows' 
    FROM   
        (SELECT unnest(var_variables) except SELECT unnest(array['cross_id']) )t(y)
    )INTO insertedCrossNotHarvestedCount;
    
    EXECUTE (
    SELECT '
        WITH rows AS (
          INSERT INTO data_terminal.transaction_dataset 
            (transaction_id, data_unit, variable_id, value , creator_id, entity, entity_id , 
              is_void, is_generated, is_suppressed, notes)
            SELECT 
                DISTINCT ON (u.variable,value,t.cross_id)
                '|| var_transaction_id ||' AS transaction_id,
                ''cross'' AS data_unit,
                (SELECT id FROM master.variable WHERE abbrev = upper(u.variable)) AS variable_id, 
                u.val AS value,
                '||var_user_id||' AS creator_id,
                ''cross_data'' AS entity,
                t.cross_id::integer AS entity_id,
                FALSE AS is_void,
                FALSE AS is_generated,
                FALSE AS is_suppressed,
		        ''uploaded''
            FROM  
                ' || tableName || ' t
                    LEFT JOIN LATERAL ( VALUES ' || string_agg(format('(''%s'', t.%I)', y, y), ', ') || ') u(variable, val) ON true
                    LEFT JOIN germplasm.cross AS crosses ON crosses.id = t.cross_id::int
                    LEFT JOIN germplasm.cross_parent female 
                        ON female.cross_id = crosses.id AND female.parent_role = ''female''
                            AND female.is_void = FALSE
                    LEFT JOIN germplasm.cross_parent male 
                        ON male.cross_id = crosses.id AND male.parent_role = ''male''
                            AND male.is_void = FALSE
                    LEFT JOIN germplasm.germplasm female_germplasm ON female_germplasm.id = female.germplasm_id
                    LEFT JOIN germplasm.germplasm male_germplasm ON male_germplasm.id = male.germplasm_id
                    LEFT JOIN tenant.crop female_crop ON female_crop.id = female_germplasm.crop_id
                    LEFT JOIN tenant.crop male_crop ON male_crop.id = male_germplasm.crop_id,
                master.variable v
            WHERE 
                lower(v.abbrev) = u.variable
                AND crosses.harvest_status <> ''COMPLETED''
                AND 
                (
                    (
                        crosses.cross_method = ''selfing''
                        AND v.abbrev NOT IN (''DATE_CROSSED'', ''HV_METH_DISC'', 
                        ''NO_OF_SEED'', ''HVDATE_CONT'',
                        ''NO_OF_PLANTS'', ''PANNO_SEL'', 
                        ''SPECIFIC_PLANT'', ''NO_OF_EARS'')
                    ) OR
                    (
                        female_crop.crop_code = ''RICE'' AND male_crop.crop_code = ''RICE''
                        AND v.abbrev NOT IN (''DATE_CROSSED'', ''HV_METH_DISC'', 
                        ''NO_OF_SEED'', ''HVDATE_CONT'')
                    ) OR
                    (
                        female_crop.crop_code IN (''MAIZE'',''WHEAT'',''COWPEA'',''SOYBEAN'',''CHICKPEA'',''GROUNDNUT'',''SORGHUM'',''FINGERMILLET'',''PEARLMILLET'',''PIGEONPEA'') 
                        AND male_crop.crop_code IN (''MAIZE'',''WHEAT'',''COWPEA'',''SOYBEAN'',''CHICKPEA'',''GROUNDNUT'',''SORGHUM'',''FINGERMILLET'',''PEARLMILLET'',''PIGEONPEA'')
                        AND v.abbrev NOT IN (''HV_METH_DISC'', ''HVDATE_CONT'')
                    )
                    
 
                )
                AND t.cross_id IS NOT NULL
                AND NOT EXISTS(
                    SELECT 1 
                    FROM
                        data_terminal.transaction_dataset td
                    WHERE 
                        td.transaction_id= '||var_transaction_id||'
                        AND t.cross_id::int = td.entity_id
                        AND td.value IS NOT NULL 
                        AND td.value = u.val
                        AND td.variable_id = v.id
                        AND td.is_void = FALSE
                        AND td.entity = ''cross_data''
                ) AND NOT EXISTS(
                    SELECT 1 
                    FROM
                        germplasm.cross_data cd
                    WHERE 
                        t.cross_id::int = cd.cross_id
                        AND cd.data_value = u.val
                        AND cd.variable_id = v.id
                        AND cd.is_void = FALSE
                )
                AND trim(u.val) <> ''''
            RETURNING id 
        ) SELECT count(1) FROM rows' 
    FROM   
        (SELECT unnest(var_variables) except SELECT unnest(array['cross_id']) )t(y)
    )INTO insertedCrossCount;
          
    return insertedCount + updatedCount + insertedCrossCount + updatedCrossCount+ insertedCrossNotHarvestedCount + insertedNotHarvestedCount;
END;
$BODY$;

ALTER FUNCTION data_terminal.create_transaction_dataset(integer, integer, json, text[])
    OWNER TO postgres;

GRANT EXECUTE ON FUNCTION data_terminal.create_transaction_dataset(integer, integer, json, text[]) TO PUBLIC;

GRANT EXECUTE ON FUNCTION data_terminal.create_transaction_dataset(integer, integer, json, text[]) TO postgres;

GRANT EXECUTE ON FUNCTION data_terminal.create_transaction_dataset(integer, integer, json, text[]) TO scheduler;



--rollback CREATE OR REPLACE FUNCTION data_terminal.create_transaction_dataset(
--rollback 	var_transaction_id integer,
--rollback 	var_user_id integer,
--rollback 	var_dataset json,
--rollback 	var_variables text[])
--rollback     RETURNS character varying
--rollback     LANGUAGE 'plpgsql'
--rollback     COST 100
--rollback     VOLATILE PARALLEL UNSAFE
--rollback AS $BODY$
--rollback 
--rollback DECLARE
--rollback     tableName text; 
--rollback     columnString text; 
--rollback     items RECORD; 
--rollback     locationId integer;	
--rollback     insertedCount integer;	
--rollback     updatedCount integer; 
--rollback     insertedCrossCount integer;	
--rollback     updatedCrossCount integer; 
--rollback     insertedNotHarvestedCount integer;
--rollback     insertedCrossNotHarvestedCount integer;
--rollback begin
--rollback     insertedCrossCount := 0;	
--rollback     updatedCrossCount := 0; 
--rollback     updatedCount := 0;
--rollback     insertedCount := 0;
--rollback     tableName := 'dataset_'||var_transaction_id||'_'||var_user_id;
--rollback     columnString := 'plot_id, cross_id, '|| array_to_string(var_variables, ',');
--rollback 
--rollback     SELECT location_id INTO locationId FROM data_terminal."transaction" t WHERE id = var_transaction_id;
--rollback     
--rollback     
--rollback     execute '
--rollback     CREATE TEMP TABLE dataset_'||var_transaction_id||'_'||var_user_id||'(
--rollback         id serial NOT NULL,
--rollback         plot_id text,
--rollback         cross_id text, 
--rollback         '||(SELECT string_agg(a.concat,',') FROM (SELECT concat(unnest(var_variables),' text'))a)||'
--rollback     ) ON COMMIT DROP
--rollback     ';
--rollback 
--rollback     
--rollback     execute ' 
--rollback         WITH data AS (
--rollback             SELECT 
--rollback             '''||var_dataset||'''
--rollback             ::jsonb AS jsondata
--rollback         )
--rollback         INSERT INTO '||tableName ||'('|| columnString || ') 
--rollback         SELECT 
--rollback             (elems->>''plot_id'')::int as plot_id,
--rollback             (elems->>''cross_id'')::int as cross_id,
--rollback             '||(SELECT STRING_AGG(b.concat, ',') from (SELECT CONCAT('(elems->>''',unnest(var_variables),''')::text as ',unnest(var_variables)))b )||'
--rollback         FROM 
--rollback             data,
--rollback             jsonb_array_elements(jsondata) AS elems
--rollback     ';
--rollback     
--rollback     
--rollback     FOR items IN EXECUTE format('SELECT * FROM %I WHERE plot_id IS NULL AND cross_id IS NULL ORDER BY id ASC', tableName)
--rollback     LOOP
--rollback         return 'Record '|| items.id || ' does not have PLOT_ID or CROSS_ID.';
--rollback     END LOOP;
--rollback    
--rollback     
--rollback     FOR items IN EXECUTE format('SELECT * FROM %I WHERE plot_id IS NOT NULL ORDER BY id ASC', tableName)
--rollback     LOOP
--rollback         if  (items.cross_id !~ E'^\\d+$') then 
--rollback             return 'Record '|| items.id || ' must be an integer';
--rollback 
--rollback         elsif NOT EXISTS(SELECT 1 FROM experiment.plot WHERE location_id = locationId AND id = items.plot_id::integer AND is_void = FALSE) then 
--rollback             return 'Record '|| items.id || ' with PLOT_ID of '|| items.plot_id || ' is not a plot of location.';
--rollback 
--rollback         end if;
--rollback     END LOOP;
--rollback     
--rollback     
--rollback     EXECUTE (
--rollback     SELECT '
--rollback         WITH rows AS (
--rollback             UPDATE data_terminal.transaction_dataset td
--rollback             SET 
--rollback                 is_suppressed = FALSE,
--rollback                 value = a.value,
--rollback                 modification_timestamp = now(),
--rollback                 modifier_id = '|| var_user_id ||'
--rollback                 
--rollback             FROM
--rollback             (
--rollback                 SELECT 
--rollback                     DISTINCT ON (u.variable,u.val,t.plot_id)
--rollback                     '|| var_transaction_id ||' AS transaction_id,
--rollback                     u.val AS value,
--rollback                     '||var_user_id||' AS creator_id,
--rollback                     ''plot_data'' AS entity,
--rollback                     t.plot_id::integer AS entity_id,
--rollback                     (SELECT id FROM master.variable WHERE abbrev = upper(u.variable)) AS variable_id
--rollback                 FROM  
--rollback                     ' || tableName || ' t
--rollback                         LEFT JOIN LATERAL ( VALUES ' || string_agg(format('(''%s'', t.%I)', tempVar, tempVar), ', ') || ') u(variable, val) ON true,
--rollback                     master.variable v
--rollback                 WHERE 
--rollback                     lower(v.abbrev) = u.variable
--rollback                     AND trim(u.val) <> ''''
--rollback                     AND plot_id IS NOT NULL
--rollback                     AND NOT EXISTS(
--rollback                       SELECT 1 
--rollback                       FROM
--rollback                           experiment.plot_data pd
--rollback                       WHERE 
--rollback                           t.plot_id::int = pd.plot_id
--rollback                           AND pd.data_value = u.val
--rollback                           AND pd.variable_id = v.id
--rollback                           AND pd.is_void = FALSE
--rollback                     )
--rollback             )a
--rollback             WHERE 
--rollback                 td.entity_id = a.entity_id
--rollback                 AND td.transaction_id = '||var_transaction_id||'
--rollback                 AND td.variable_id = a.variable_id
--rollback                 AND td.entity = ''plot_data''
--rollback                 AND td.value IS NOT NULL 
--rollback                 AND td.value <> a.value
--rollback                 AND td.is_void = FALSE
--rollback             returning id 
--rollback         ) SELECT count(1) FROM rows' 
--rollback     FROM   
--rollback           (SELECT unnest(var_variables) except SELECT unnest(array['plot_id']) )t(tempVar)
--rollback       ) INTO updatedCount;
--rollback 
--rollback     
--rollback     EXECUTE (
--rollback     SELECT '
--rollback         WITH rows AS (
--rollback             UPDATE data_terminal.transaction_dataset td
--rollback             SET 
--rollback                 is_suppressed = FALSE,
--rollback                 value = a.value,
--rollback                 modification_timestamp = now(),
--rollback                 modifier_id = '|| var_user_id ||'
--rollback                 
--rollback             FROM
--rollback             (
--rollback                 SELECT 
--rollback                     DISTINCT ON (u.variable,u.val,t.cross_id)
--rollback                     '|| var_transaction_id ||' AS transaction_id,
--rollback                     u.val AS value,
--rollback                     '||var_user_id||' AS creator_id,
--rollback                     ''cross_data'' AS entity,
--rollback                     t.cross_id::integer AS entity_id,
--rollback                     (SELECT id FROM master.variable WHERE abbrev = upper(u.variable)) AS variable_id
--rollback                 FROM  
--rollback                     ' || tableName || ' t
--rollback                         LEFT JOIN LATERAL ( VALUES ' || string_agg(format('(''%s'', t.%I)', tempVar, tempVar), ', ') || ') u(variable, val) ON true,
--rollback                     master.variable v
--rollback                 WHERE 
--rollback                     lower(v.abbrev) = u.variable
--rollback                     AND trim(u.val) <> ''''
--rollback                     AND cross_id IS NOT NULL
--rollback                     AND NOT EXISTS(
--rollback                       SELECT 1 
--rollback                       FROM
--rollback                           germplasm.cross_data cd
--rollback                       WHERE 
--rollback                           t.cross_id::int = cd.cross_id
--rollback                           AND cd.data_value = u.val
--rollback                           AND cd.variable_id = v.id
--rollback                           AND cd.is_void = FALSE
--rollback                     )
--rollback             )a
--rollback             WHERE 
--rollback                 td.entity_id = a.entity_id
--rollback                 AND td.transaction_id = '||var_transaction_id||'
--rollback                 AND td.variable_id = a.variable_id
--rollback                 AND td.entity = ''cross_data''
--rollback                 AND td.value IS NOT NULL 
--rollback                 AND td.value <> a.value
--rollback                 AND td.is_void = FALSE
--rollback             returning id 
--rollback         ) SELECT count(1) FROM rows' 
--rollback     FROM   
--rollback           (SELECT unnest(var_variables) except SELECT unnest(array['cross_id']) )t(tempVar)
--rollback       ) INTO updatedCrossCount;
--rollback          
--rollback     EXECUTE (
--rollback     SELECT '
--rollback         WITH rows AS (
--rollback           INSERT INTO data_terminal.transaction_dataset 
--rollback             (transaction_id, data_unit, variable_id, value , creator_id, entity, entity_id , 
--rollback               is_void, is_generated, is_suppressed, notes)
--rollback             SELECT 
--rollback                 DISTINCT ON (u.variable,value,t.plot_id)
--rollback                 '|| var_transaction_id ||' AS transaction_id,
--rollback                 ''plot'' AS data_unit,
--rollback                 (SELECT id FROM master.variable WHERE abbrev = upper(u.variable)) AS variable_id, 
--rollback                 u.val AS value,
--rollback                 '||var_user_id||' AS creator_id,
--rollback                 ''plot_data'' AS entity,
--rollback                 t.plot_id::integer AS entity_id,
--rollback                 FALSE AS is_void,
--rollback                 FALSE AS is_generated,
--rollback                 FALSE AS is_suppressed,
--rollback 		        ''uploaded''
--rollback             FROM  
--rollback                 ' || tableName || ' t
--rollback                     LEFT JOIN LATERAL ( VALUES ' || string_agg(format('(''%s'', t.%I)', y, y), ', ') || ') u(variable, val) ON true
--rollback                     LEFT JOIN experiment.plot p ON p.id = t.plot_id::int,
--rollback                 master.variable v
--rollback             WHERE 
--rollback                 lower(v.abbrev) = u.variable
--rollback                 AND v.abbrev IN (''HV_METH_DISC'', ''HVDATE_CONT'', ''NO_OF_PLANTS'', ''SPECIFIC_PLANT'', ''PANNO_SEL'', ''NO_OF_EARS'')
--rollback                 AND p.harvest_status <> ''COMPLETED''
--rollback                 AND plot_id IS NOT NULL
--rollback                 AND NOT EXISTS(
--rollback                     SELECT 1 
--rollback                     FROM
--rollback                         data_terminal.transaction_dataset td
--rollback                     WHERE 
--rollback                         td.transaction_id= '||var_transaction_id||'
--rollback                         AND t.plot_id::int = td.entity_id
--rollback                         AND td.value IS NOT NULL 
--rollback                         AND td.value = u.val
--rollback                         AND td.variable_id = v.id
--rollback                         AND td.is_void = FALSE
--rollback                         AND td.entity = ''plot_data''
--rollback                 ) AND NOT EXISTS(
--rollback                     SELECT 1 
--rollback                     FROM
--rollback                         experiment.plot_data pd
--rollback                     WHERE 
--rollback                         t.plot_id::int = pd.plot_id
--rollback                         AND pd.data_value = u.val
--rollback                         AND pd.variable_id = v.id
--rollback                         AND pd.is_void = FALSE
--rollback                 )
--rollback                 AND trim(u.val) <> ''''
--rollback             RETURNING id 
--rollback         ) SELECT count(1) FROM rows' 
--rollback     FROM   
--rollback         (SELECT unnest(var_variables) except SELECT unnest(array['plot_id']) )t(y)
--rollback     )INTO insertedNotHarvestedCount;
--rollback     EXECUTE (
--rollback     SELECT '
--rollback         WITH rows AS (
--rollback           INSERT INTO data_terminal.transaction_dataset 
--rollback             (transaction_id, data_unit, variable_id, value , creator_id, entity, entity_id , 
--rollback               is_void, is_generated, is_suppressed, notes)
--rollback             SELECT 
--rollback                 DISTINCT ON (u.variable,value,t.plot_id)
--rollback                 '|| var_transaction_id ||' AS transaction_id,
--rollback                 ''plot'' AS data_unit,
--rollback                 (SELECT id FROM master.variable WHERE abbrev = upper(u.variable)) AS variable_id, 
--rollback                 u.val AS value,
--rollback                 '||var_user_id||' AS creator_id,
--rollback                 ''plot_data'' AS entity,
--rollback                 t.plot_id::integer AS entity_id,
--rollback                 FALSE AS is_void,
--rollback                 FALSE AS is_generated,
--rollback                 FALSE AS is_suppressed,
--rollback 		        ''uploaded''
--rollback             FROM  
--rollback                 ' || tableName || ' t
--rollback                     LEFT JOIN LATERAL ( VALUES ' || string_agg(format('(''%s'', t.%I)', y, y), ', ') || ') u(variable, val) ON true
--rollback                     LEFT JOIN experiment.plot p ON p.id = t.plot_id::int,
--rollback                 master.variable v
--rollback                 
--rollback             WHERE 
--rollback                 lower(v.abbrev) = u.variable
--rollback                 AND v.abbrev NOT IN (''HV_METH_DISC'', ''HVDATE_CONT'', ''NO_OF_PLANTS'', ''SPECIFIC_PLANT'', ''PANNO_SEL'', ''NO_OF_EARS'')
--rollback                 AND plot_id IS NOT NULL
--rollback                 AND NOT EXISTS(
--rollback                     SELECT 1 
--rollback                     FROM
--rollback                         data_terminal.transaction_dataset td
--rollback                     WHERE 
--rollback                         td.transaction_id= '||var_transaction_id||'
--rollback                         AND t.plot_id::int = td.entity_id
--rollback                         AND td.value IS NOT NULL 
--rollback                         AND td.value = u.val
--rollback                         AND td.variable_id = v.id
--rollback                         AND td.is_void = FALSE
--rollback                         AND td.entity = ''plot_data''
--rollback                 ) AND NOT EXISTS(
--rollback                     SELECT 1 
--rollback                     FROM
--rollback                         experiment.plot_data pd
--rollback                     WHERE 
--rollback                         t.plot_id::int = pd.plot_id
--rollback                         AND pd.data_value = u.val
--rollback                         AND pd.variable_id = v.id
--rollback                         AND pd.is_void = FALSE
--rollback                 )
--rollback                 AND trim(u.val) <> ''''
--rollback             RETURNING id 
--rollback         ) SELECT count(1) FROM rows' 
--rollback     FROM   
--rollback         (SELECT unnest(var_variables) except SELECT unnest(array['plot_id']) )t(y)
--rollback     )INTO insertedCount;
--rollback 
--rollback    EXECUTE (
--rollback     SELECT '
--rollback         WITH rows AS (
--rollback           INSERT INTO data_terminal.transaction_dataset 
--rollback             (transaction_id, data_unit, variable_id, value , creator_id, entity, entity_id , 
--rollback               is_void, is_generated, is_suppressed, notes)
--rollback             SELECT 
--rollback                 DISTINCT ON (u.variable,value,t.cross_id)
--rollback                 '|| var_transaction_id ||' AS transaction_id,
--rollback                 ''cross'' AS data_unit,
--rollback                 (SELECT id FROM master.variable WHERE abbrev = upper(u.variable)) AS variable_id, 
--rollback                 u.val AS value,
--rollback                 '||var_user_id||' AS creator_id,
--rollback                 ''cross_data'' AS entity,
--rollback                 t.cross_id::integer AS entity_id,
--rollback                 FALSE AS is_void,
--rollback                 FALSE AS is_generated,
--rollback                 FALSE AS is_suppressed,
--rollback 	   			''uploaded''
--rollback             FROM  
--rollback                 ' || tableName || ' t
--rollback                     LEFT   JOIN LATERAL ( VALUES ' || string_agg(format('(''%s'', t.%I)', y, y), ', ') || ') u(variable, val) ON true
--rollback                     LEFT JOIN germplasm.cross AS crosses ON crosses.id = t.cross_id::int
--rollback                     LEFT JOIN germplasm.cross_parent female 
--rollback                         ON female.cross_id = crosses.id AND female.parent_role = ''female''
--rollback                             AND female.is_void = FALSE
--rollback                     LEFT JOIN germplasm.cross_parent male 
--rollback                         ON male.cross_id = crosses.id AND male.parent_role = ''male''
--rollback                             AND male.is_void = FALSE
--rollback                     LEFT JOIN germplasm.germplasm female_germplasm ON female_germplasm.id = female.germplasm_id
--rollback                     LEFT JOIN germplasm.germplasm male_germplasm ON male_germplasm.id = male.germplasm_id
--rollback                     LEFT JOIN tenant.crop female_crop ON female_crop.id = female_germplasm.crop_id
--rollback                     LEFT JOIN tenant.crop male_crop ON male_crop.id = male_germplasm.crop_id,
--rollback                 master.variable v
--rollback             WHERE 
--rollback                 lower(v.abbrev) = u.variable
--rollback                 AND crosses.harvest_status <> ''COMPLETED''
--rollback                 AND 
--rollback                 (
--rollback                     (
--rollback                         crosses.cross_method = ''selfing''
--rollback                         AND v.abbrev IN (''DATE_CROSSED'', ''HV_METH_DISC'', 
--rollback                         ''NO_OF_SEED'', ''HVDATE_CONT'',
--rollback                         ''NO_OF_PLANTS'', ''PANNO_SEL'', 
--rollback                         ''SPECIFIC_PLANT'', ''NO_OF_EARS'')
--rollback                     ) OR
--rollback                     (
--rollback                         female_crop.crop_code = ''RICE'' AND male_crop.crop_code = ''RICE''
--rollback                         AND v.abbrev IN (''DATE_CROSSED'', ''HV_METH_DISC'', 
--rollback                         ''NO_OF_SEED'', ''HVDATE_CONT'')
--rollback                     ) OR
--rollback                     (
--rollback                         female_crop.crop_code IN (''MAIZE'',''WHEAT'',''COWPEA'',''SOYBEAN'') 
--rollback                         AND male_crop.crop_code IN (''MAIZE'',''WHEAT'',''COWPEA'',''SOYBEAN'')
--rollback                         AND v.abbrev IN (''HV_METH_DISC'', ''HVDATE_CONT'')
--rollback                     )
--rollback  
--rollback                 )
--rollback                 AND t.cross_id IS NOT NULL
--rollback                 AND NOT EXISTS(
--rollback                     SELECT 1 
--rollback                     FROM
--rollback                         data_terminal.transaction_dataset td
--rollback                     WHERE 
--rollback                         td.transaction_id= '||var_transaction_id||'
--rollback                         AND t.cross_id::int = td.entity_id
--rollback                         AND td.value IS NOT NULL 
--rollback                         AND td.value = u.val
--rollback                         AND td.variable_id = v.id
--rollback                         AND td.is_void = FALSE
--rollback                         AND td.entity = ''cross_data''
--rollback                 ) AND NOT EXISTS(
--rollback                     SELECT 1 
--rollback                     FROM
--rollback                         germplasm.cross_data cd
--rollback                     WHERE 
--rollback                         t.cross_id::int = cd.cross_id
--rollback                         AND cd.data_value = u.val
--rollback                         AND cd.variable_id = v.id
--rollback                         AND cd.is_void = FALSE
--rollback                 )
--rollback                 AND trim(u.val) <> ''''
--rollback             RETURNING id 
--rollback         ) SELECT count(1) FROM rows' 
--rollback     FROM   
--rollback         (SELECT unnest(var_variables) except SELECT unnest(array['cross_id']) )t(y)
--rollback     )INTO insertedCrossNotHarvestedCount;
--rollback     
--rollback     EXECUTE (
--rollback     SELECT '
--rollback         WITH rows AS (
--rollback           INSERT INTO data_terminal.transaction_dataset 
--rollback             (transaction_id, data_unit, variable_id, value , creator_id, entity, entity_id , 
--rollback               is_void, is_generated, is_suppressed, notes)
--rollback             SELECT 
--rollback                 DISTINCT ON (u.variable,value,t.cross_id)
--rollback                 '|| var_transaction_id ||' AS transaction_id,
--rollback                 ''cross'' AS data_unit,
--rollback                 (SELECT id FROM master.variable WHERE abbrev = upper(u.variable)) AS variable_id, 
--rollback                 u.val AS value,
--rollback                 '||var_user_id||' AS creator_id,
--rollback                 ''cross_data'' AS entity,
--rollback                 t.cross_id::integer AS entity_id,
--rollback                 FALSE AS is_void,
--rollback                 FALSE AS is_generated,
--rollback                 FALSE AS is_suppressed,
--rollback 		        ''uploaded''
--rollback             FROM  
--rollback                 ' || tableName || ' t
--rollback                     LEFT JOIN LATERAL ( VALUES ' || string_agg(format('(''%s'', t.%I)', y, y), ', ') || ') u(variable, val) ON true
--rollback                     LEFT JOIN germplasm.cross AS crosses ON crosses.id = t.cross_id::int
--rollback                     LEFT JOIN germplasm.cross_parent female 
--rollback                         ON female.cross_id = crosses.id AND female.parent_role = ''female''
--rollback                             AND female.is_void = FALSE
--rollback                     LEFT JOIN germplasm.cross_parent male 
--rollback                         ON male.cross_id = crosses.id AND male.parent_role = ''male''
--rollback                             AND male.is_void = FALSE
--rollback                     LEFT JOIN germplasm.germplasm female_germplasm ON female_germplasm.id = female.germplasm_id
--rollback                     LEFT JOIN germplasm.germplasm male_germplasm ON male_germplasm.id = male.germplasm_id
--rollback                     LEFT JOIN tenant.crop female_crop ON female_crop.id = female_germplasm.crop_id
--rollback                     LEFT JOIN tenant.crop male_crop ON male_crop.id = male_germplasm.crop_id,
--rollback                 master.variable v
--rollback             WHERE 
--rollback                 lower(v.abbrev) = u.variable
--rollback                 AND crosses.harvest_status <> ''COMPLETED''
--rollback                 AND 
--rollback                 (
--rollback                     (
--rollback                         crosses.cross_method = ''selfing''
--rollback                         AND v.abbrev NOT IN (''DATE_CROSSED'', ''HV_METH_DISC'', 
--rollback                         ''NO_OF_SEED'', ''HVDATE_CONT'',
--rollback                         ''NO_OF_PLANTS'', ''PANNO_SEL'', 
--rollback                         ''SPECIFIC_PLANT'', ''NO_OF_EARS'')
--rollback                     ) OR
--rollback                     (
--rollback                         female_crop.crop_code = ''RICE'' AND male_crop.crop_code = ''RICE''
--rollback                         AND v.abbrev NOT IN (''DATE_CROSSED'', ''HV_METH_DISC'', 
--rollback                         ''NO_OF_SEED'', ''HVDATE_CONT'')
--rollback                     ) OR
--rollback                     (
--rollback                         female_crop.crop_code IN (''MAIZE'',''WHEAT'',''COWPEA'',''SOYBEAN'') 
--rollback                         AND male_crop.crop_code IN (''MAIZE'',''WHEAT'',''COWPEA'',''SOYBEAN'')
--rollback                         AND v.abbrev NOT IN (''HV_METH_DISC'', ''HVDATE_CONT'')
--rollback                     )
--rollback                     
--rollback  
--rollback                 )
--rollback                 AND t.cross_id IS NOT NULL
--rollback                 AND NOT EXISTS(
--rollback                     SELECT 1 
--rollback                     FROM
--rollback                         data_terminal.transaction_dataset td
--rollback                     WHERE 
--rollback                         td.transaction_id= '||var_transaction_id||'
--rollback                         AND t.cross_id::int = td.entity_id
--rollback                         AND td.value IS NOT NULL 
--rollback                         AND td.value = u.val
--rollback                         AND td.variable_id = v.id
--rollback                         AND td.is_void = FALSE
--rollback                         AND td.entity = ''cross_data''
--rollback                 ) AND NOT EXISTS(
--rollback                     SELECT 1 
--rollback                     FROM
--rollback                         germplasm.cross_data cd
--rollback                     WHERE 
--rollback                         t.cross_id::int = cd.cross_id
--rollback                         AND cd.data_value = u.val
--rollback                         AND cd.variable_id = v.id
--rollback                         AND cd.is_void = FALSE
--rollback                 )
--rollback                 AND trim(u.val) <> ''''
--rollback             RETURNING id 
--rollback         ) SELECT count(1) FROM rows' 
--rollback     FROM   
--rollback         (SELECT unnest(var_variables) except SELECT unnest(array['cross_id']) )t(y)
--rollback     )INTO insertedCrossCount;
--rollback           
--rollback     return insertedCount + updatedCount + insertedCrossCount + updatedCrossCount+ insertedCrossNotHarvestedCount + insertedNotHarvestedCount;
--rollback END;
--rollback $BODY$;
--rollback 
--rollback ALTER FUNCTION data_terminal.create_transaction_dataset(integer, integer, json, text[])
--rollback     OWNER TO postgres;
--rollback 
--rollback GRANT EXECUTE ON FUNCTION data_terminal.create_transaction_dataset(integer, integer, json, text[]) TO PUBLIC;
--rollback 
--rollback GRANT EXECUTE ON FUNCTION data_terminal.create_transaction_dataset(integer, integer, json, text[]) TO postgres;
--rollback 
--rollback GRANT EXECUTE ON FUNCTION data_terminal.create_transaction_dataset(integer, integer, json, text[]) TO scheduler;
