--liquibase formatted sql

--changeset postgres:populate_occurrence_design_type_based_on_experiment_design_type context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:CONTINUE onError:HALT
--precondition-sql-check expectedResult:1 SELECT CASE EXISTS(SELECT COUNT(id) FROM experiment.occurrence WHERE is_void = FALSE AND occurrence_design_type IS NULL) WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-4141 CB-DB-EM: Create changesets to populate occurrence.occurrence_design_type



UPDATE
    experiment.occurrence occurrence
SET
    occurrence_design_type = experiment.experiment_design_type
FROM
    experiment.experiment experiment
WHERE
    occurrence.is_void = FALSE AND
	occurrence_design_type IS NULL AND
	experiment.id = occurrence.experiment_id;



--rollback UPDATE
--rollback     experiment.occurrence
--rollback SET
--rollback     occurrence_design_type = NULL
--rollback WHERE
--rollback     is_void = FALSE;