--liquibase formatted sql

--changeset postgres:hm_dcp_008_update_germplasm_name_config context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-4439 CB-HM: Update germplasm name pattern for single cross



--update germplasm name config
UPDATE platform.config
SET
    config_value = $$
        {
            "PLOT": {
                "default": {
                    "not_fixed": {
                        "default": {
                            "bulk": [
                                {
                                    "type": "field",
                                    "entity": "plot",
                                    "field_name": "germplasmDesignation",
                                    "order_number": 0
                                },
                                {
                                    "type": "delimiter",
                                    "value": "-",
                                    "order_number": 1
                                },
                                {
                                    "type": "free-text",
                                    "value": "B",
                                    "order_number": 0
                                }
                            ],
                            "default": [
                                {
                                    "type": "field",
                                    "entity": "plot",
                                    "field_name": "germplasmDesignation",
                                    "order_number": 0
                                }
                            ],
                            "single plant": [
                                {
                                    "type": "field",
                                    "entity": "plot",
                                    "field_name": "germplasmDesignation",
                                    "order_number": 0
                                },
                                {
                                    "type": "delimiter",
                                    "value": "-",
                                    "order_number": 1
                                },
                                {
                                    "type": "counter",
                                    "order_number": 2
                                }
                            ]
                        }
                    }
                }
            },
            "CROSS": {
                "default": {
                    "default": {
                        "default": {
                            "default": [
                                {
                                    "type": "free-text",
                                    "value": "",
                                    "order_number": 0
                                }
                            ]
                        }
                    }
                },
                "single cross": {
                    "default": {
                        "default": {
                            "bulk": [
                                {
                                    "type": "field",
                                    "entity": "cross",
                                    "field_name": "experimentYearYY",
                                    "order_number": 0
                                },
                                {
                                    "type": "free-text",
                                    "value": "X",
                                    "order_number": 1
                                },
                                {
                                    "type": "field",
                                    "entity": "cross",
                                    "field_name": "programCode",
                                    "order_number": 2
                                },
                                {
                                    "type": "field",
                                    "entity": "cross",
                                    "field_name": "femaleParentPipelineCode",
                                    "order_number": 3
                                },
                                {
                                    "type": "counter",
                                    "max_digits": 3,
                                    "leading_zero": "yes",
                                    "order_number": 4
                                }
                            ],
                            "default": [
                                {
                                    "type": "field",
                                    "entity": "cross",
                                    "field_name": "crossName",
                                    "order_number": 0
                                }
                            ]
                        }
                    }
                }
            },
            "harvest_mode": {
                "cross_method": {
                    "germplasm_state": {
                        "germplasm_type": {
                            "harvest_method": [
                                {
                                    "type": "free-text",
                                    "value": "ABC",
                                    "order_number": 0
                                },
                                {
                                    "type": "field",
                                    "entity": "<entity>",
                                    "field_name": "<field_name>",
                                    "order_number": 1
                                },
                                {
                                    "type": "delimiter",
                                    "value": "-",
                                    "order_number": 1
                                },
                                {
                                    "type": "counter",
                                    "order_number": 3
                                },
                                {
                                    "type": "db-sequence",
                                    "schema": "<schema>",
                                    "order_number": 4,
                                    "sequence_name": "<sequence_name>"
                                },
                                {
                                    "type": "free-text-repeater",
                                    "value": "ABC",
                                    "minimum": "2",
                                    "delimiter": "*",
                                    "order_number": 5
                                }
                            ]
                        }
                    }
                }
            }
        }
    $$
WHERE
    abbrev IN (
        'HM_NAME_PATTERN_GERMPLASM_CHICKPEA_DEFAULT',
        'HM_NAME_PATTERN_GERMPLASM_GROUNDNUT_DEFAULT',
        'HM_NAME_PATTERN_GERMPLASM_SORGHUM_DEFAULT',
        'HM_NAME_PATTERN_GERMPLASM_FINGERMILLET_DEFAULT',
        'HM_NAME_PATTERN_GERMPLASM_PEARLMILLET_DEFAULT',
        'HM_NAME_PATTERN_GERMPLASM_PIGEONPEA_DEFAULT'
    )
;



--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $$
--rollback         {
--rollback             "PLOT": {
--rollback                 "default": {
--rollback                     "not_fixed": {
--rollback                         "default": {
--rollback                             "bulk": [
--rollback                                 {
--rollback                                     "type": "field",
--rollback                                     "entity": "plot",
--rollback                                     "field_name": "germplasmDesignation",
--rollback                                     "order_number": 0
--rollback                                 },
--rollback                                 {
--rollback                                     "type": "delimiter",
--rollback                                     "value": "-",
--rollback                                     "order_number": 1
--rollback                                 },
--rollback                                 {
--rollback                                     "type": "free-text",
--rollback                                     "value": "B",
--rollback                                     "order_number": 0
--rollback                                 }
--rollback                             ],
--rollback                             "default": [
--rollback                                 {
--rollback                                     "type": "field",
--rollback                                     "entity": "plot",
--rollback                                     "field_name": "germplasmDesignation",
--rollback                                     "order_number": 0
--rollback                                 }
--rollback                             ],
--rollback                             "single plant": [
--rollback                                 {
--rollback                                     "type": "field",
--rollback                                     "entity": "plot",
--rollback                                     "field_name": "germplasmDesignation",
--rollback                                     "order_number": 0
--rollback                                 },
--rollback                                 {
--rollback                                     "type": "delimiter",
--rollback                                     "value": "-",
--rollback                                     "order_number": 1
--rollback                                 },
--rollback                                 {
--rollback                                     "type": "counter",
--rollback                                     "order_number": 2
--rollback                                 }
--rollback                             ]
--rollback                         }
--rollback                     }
--rollback                 }
--rollback             },
--rollback             "CROSS": {
--rollback                 "default": {
--rollback                     "default": {
--rollback                         "default": {
--rollback                             "default": [
--rollback                                 {
--rollback                                     "type": "free-text",
--rollback                                     "value": "",
--rollback                                     "order_number": 0
--rollback                                 }
--rollback                             ]
--rollback                         }
--rollback                     }
--rollback                 },
--rollback                 "single cross": {
--rollback                     "default": {
--rollback                         "default": {
--rollback                             "bulk": [
--rollback                                 {
--rollback                                     "type": "field",
--rollback                                     "entity": "cross",
--rollback                                     "field_name": "experimentYearYY",
--rollback                                     "order_number": 0
--rollback                                 },
--rollback                                 {
--rollback                                     "type": "free-text",
--rollback                                     "value": "X",
--rollback                                     "order_number": 1
--rollback                                 },
--rollback                                 {
--rollback                                     "type": "field",
--rollback                                     "entity": "cross",
--rollback                                     "field_name": "<TBD: program code>",
--rollback                                     "order_number": 2
--rollback                                 },
--rollback                                 {
--rollback                                     "type": "field",
--rollback                                     "entity": "cross",
--rollback                                     "field_name": "<TBD: pipeline code>",
--rollback                                     "order_number": 3
--rollback                                 },
--rollback                                 {
--rollback                                     "type": "counter",
--rollback                                     "max_digits": 3,
--rollback                                     "leading_zero": "yes",
--rollback                                     "order_number": 4
--rollback                                 }
--rollback                             ],
--rollback                             "default": [
--rollback                                 {
--rollback                                     "type": "field",
--rollback                                     "entity": "cross",
--rollback                                     "field_name": "crossName",
--rollback                                     "order_number": 0
--rollback                                 }
--rollback                             ]
--rollback                         }
--rollback                     }
--rollback                 },
--rollback                 "three-way cross": {
--rollback                     "default": {
--rollback                         "default": {
--rollback                             "bulk": [
--rollback                                 {
--rollback                                     "type": "field",
--rollback                                     "entity": "cross",
--rollback                                     "field_name": "experimentYearYY",
--rollback                                     "order_number": 0
--rollback                                 },
--rollback                                 {
--rollback                                     "type": "free-text",
--rollback                                     "value": "X",
--rollback                                     "order_number": 1
--rollback                                 },
--rollback                                 {
--rollback                                     "type": "field",
--rollback                                     "entity": "cross",
--rollback                                     "field_name": "<TBD: program code>",
--rollback                                     "order_number": 2
--rollback                                 },
--rollback                                 {
--rollback                                     "type": "field",
--rollback                                     "entity": "cross",
--rollback                                     "field_name": "<TBD: pipeline code>",
--rollback                                     "order_number": 3
--rollback                                 },
--rollback                                 {
--rollback                                     "type": "counter",
--rollback                                     "max_digits": 3,
--rollback                                     "leading_zero": "yes",
--rollback                                     "order_number": 4
--rollback                                 }
--rollback                             ],
--rollback                             "default": [
--rollback                                 {
--rollback                                     "type": "field",
--rollback                                     "entity": "cross",
--rollback                                     "field_name": "crossName",
--rollback                                     "order_number": 0
--rollback                                 }
--rollback                             ]
--rollback                         }
--rollback                     }
--rollback                 }
--rollback             },
--rollback             "harvest_mode": {
--rollback                 "cross_method": {
--rollback                     "germplasm_state": {
--rollback                         "germplasm_type": {
--rollback                             "harvest_method": [
--rollback                                 {
--rollback                                     "type": "free-text",
--rollback                                     "value": "ABC",
--rollback                                     "order_number": 0
--rollback                                 },
--rollback                                 {
--rollback                                     "type": "field",
--rollback                                     "entity": "<entity>",
--rollback                                     "field_name": "<field_name>",
--rollback                                     "order_number": 1
--rollback                                 },
--rollback                                 {
--rollback                                     "type": "delimiter",
--rollback                                     "value": "-",
--rollback                                     "order_number": 1
--rollback                                 },
--rollback                                 {
--rollback                                     "type": "counter",
--rollback                                     "order_number": 3
--rollback                                 },
--rollback                                 {
--rollback                                     "type": "db-sequence",
--rollback                                     "schema": "<schema>",
--rollback                                     "order_number": 4,
--rollback                                     "sequence_name": "<sequence_name>"
--rollback                                 },
--rollback                                 {
--rollback                                     "type": "free-text-repeater",
--rollback                                     "value": "ABC",
--rollback                                     "minimum": "2",
--rollback                                     "delimiter": "*",
--rollback                                     "order_number": 5
--rollback                                 }
--rollback                             ]
--rollback                         }
--rollback                     }
--rollback                 }
--rollback             }
--rollback         }
--rollback     $$
--rollback WHERE
--rollback     abbrev IN (
--rollback         'HM_NAME_PATTERN_GERMPLASM_CHICKPEA_DEFAULT',
--rollback         'HM_NAME_PATTERN_GERMPLASM_GROUNDNUT_DEFAULT',
--rollback         'HM_NAME_PATTERN_GERMPLASM_SORGHUM_DEFAULT',
--rollback         'HM_NAME_PATTERN_GERMPLASM_FINGERMILLET_DEFAULT',
--rollback         'HM_NAME_PATTERN_GERMPLASM_PEARLMILLET_DEFAULT',
--rollback         'HM_NAME_PATTERN_GERMPLASM_PIGEONPEA_DEFAULT'
--rollback     )
--rollback ;