--liquibase formatted sql

--changeset postgres:update_plot_id_scale context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3964: CB-DC: Update post_harvest_row_1_settings_config: Update_plot_id_scale



DO $$
DECLARE
    var_scale_id int;
BEGIN
    -- check if PLOT_ID scale exists
    SELECT id FROM master.scale WHERE abbrev = 'PLOT_ID' INTO var_scale_id;
    IF var_scale_id IS NULL THEN
        INSERT INTO
            master.scale
            (abbrev,
                name,
                level,
                creator_id,
                notes
            )
        VALUES
            (
                'PLOT_ID',
                'Plot ID',
                'nominal',
                (
                    SELECT 
                        id
                    FROM
                        tenant.person
                    WHERE 
                        person_name = 'EBS, Admin'
                ),
                'BDS-3964 CB-DB: Rectify variable PLOT_ID scale values'
            )
        RETURNING id INTO var_scale_id;
    END IF;

    UPDATE
        master.variable
    SET
        scale_id = var_scale_id
    WHERE
        abbrev = 'PLOT_ID';
END      
$$

--rollback DELETE FROM
--rollback     master.scale s
--rollback USING
--rollback     master.variable v
--rollback WHERE
--rollback     v.abbrev = 'PLOT_ID'
--rollback     AND s.id = v.scale_id;
--rollback 
--rollback UPDATE
--rollback     master.variable
--rollback SET 
--rollback     scale_id = null
--rollback WHERE
--rollback     abbrev = 'PLOT_ID';