--liquibase formatted sql

--changeset postgres:hm_dcp_003_add_harvest_manager_configs_sorghum context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-4440 CB-HM DB: Add HM configurations for SORGHUM crop


-- Insert HM_DATA_BROWSER_CONFIG_SORGHUM
INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_DATA_BROWSER_CONFIG_SORGHUM',
        'HM Data Browser Config for Sorghum',
        $$			
            {
                "default": {
                    "default": {
                        "default": {
                            "input_columns": [],
                            "numeric_variables": [],
                            "additional_required_variables": []
                        }
                    }
                },
                "CROSS_METHOD_SELFING": {
                    "fixed": {
                        "default": {
                            "input_columns": [
                                {
                                    "abbrev": "HVDATE_CONT",
                                    "required": false,
                                    "column_name": "harvestDate",
                                    "placeholder": "Harvest Date",
                                    "retrieve_scale": false
                                },
                                {
                                    "abbrev": "HV_METH_DISC",
                                    "required": true,
                                    "column_name": "harvestMethod",
                                    "placeholder": "Harvest Method",
                                    "retrieve_scale": false
                                },
                                {
                                    "abbrev": "<none>",
                                    "required": null,
                                    "column_name": "numericVar",
                                    "placeholder": "Not applicable",
                                    "retrieve_scale": false
                                }
                            ],
                            "numeric_variables": [
                                {
                                    "min": null,
                                    "type": "number",
                                    "abbrev": "<none>",
                                    "sub_type": "single_int",
                                    "field_name": "<none>",
                                    "placeholder": "Not applicable",
                                    "harvest_methods": [
                                        "",
                                        "HV_METH_DISC_BULK"
                                    ],
                                    "harvest_methods_optional": [ ]
                                },
                                {
                                    "min": 0,
                                    "type": "number",
                                    "abbrev": "NO_OF_PLANTS",
                                    "sub_type": "single_int",
                                    "field_name": "noOfPlant",
                                    "placeholder": "No. of plants",
                                    "harvest_methods": [ ],
                                    "harvest_methods_optional": [
                                        "HV_METH_DISC_BULK"
                                    ]
                                }
                            ],
                            "additional_required_variables": []
                        }
                    },
                    "default": {
                        "default": {
                            "input_columns": [
                                {
                                    "abbrev": "HVDATE_CONT",
                                    "required": false,
                                    "column_name": "harvestDate",
                                    "placeholder": "Harvest Date",
                                    "retrieve_scale": false
                                },
                                {
                                    "abbrev": "HV_METH_DISC",
                                    "required": true,
                                    "column_name": "harvestMethod",
                                    "placeholder": "Harvest Method",
                                    "retrieve_scale": false
                                },
                                {
                                    "abbrev": "<none>",
                                    "required": null,
                                    "column_name": "numericVar",
                                    "placeholder": "Not applicable",
                                    "retrieve_scale": false
                                }
                            ],
                            "numeric_variables": [
                                {
                                    "min": null,
                                    "type": "number",
                                    "abbrev": "<none>",
                                    "sub_type": "single_int",
                                    "field_name": "<none>",
                                    "placeholder": "Not applicable",
                                    "harvest_methods": [],
                                    "harvest_methods_optional": [
                                        "HV_METH_DISC_BULK"
                                    ]
                                }
                            ],
                            "additional_required_variables": []
                        }
                    },
                    "unknown": {
                        "default": {
                            "input_columns": [
                                {
                                    "abbrev": "HVDATE_CONT",
                                    "required": true,
                                    "column_name": "harvestDate",
                                    "placeholder": "Harvest Date",
                                    "retrieve_scale": false
                                },
                                {
                                    "abbrev": "HV_METH_DISC",
                                    "required": true,
                                    "column_name": "harvestMethod",
                                    "placeholder": "Harvest Method",
                                    "retrieve_scale": false
                                },
                                {
                                    "abbrev": "<none>",
                                    "required": null,
                                    "column_name": "numericVar",
                                    "placeholder": "Not applicable",
                                    "retrieve_scale": false
                                }
                            ],
                            "numeric_variables": [
                                {
                                    "min": null,
                                    "type": "number",
                                    "abbrev": "<none>",
                                    "sub_type": "single_int",
                                    "field_name": "<none>",
                                    "placeholder": "Not applicable",
                                    "harvest_methods": [
                                        ""
                                    ],
                                    "harvest_methods_optional": []
                                }
                            ],
                            "additional_required_variables": []
                        }
                    },
                    "not_fixed": {
                        "default": {
                            "input_columns": [
                                {
                                    "abbrev": "HVDATE_CONT",
                                    "required": false,
                                    "column_name": "harvestDate",
                                    "placeholder": "Harvest Date",
                                    "retrieve_scale": false
                                },
                                {
                                    "abbrev": "HV_METH_DISC",
                                    "required": true,
                                    "column_name": "harvestMethod",
                                    "placeholder": "Harvest Method",
                                    "retrieve_scale": false
                                },
                                {
                                    "abbrev": "<none>",
                                    "required": null,
                                    "column_name": "numericVar",
                                    "placeholder": "Not applicable",
                                    "retrieve_scale": false
                                }
                            ],
                            "numeric_variables": [
                                {
                                    "min": null,
                                    "type": "number",
                                    "abbrev": "<none>",
                                    "sub_type": "single_int",
                                    "field_name": "<none>",
                                    "placeholder": "Not applicable",
                                    "harvest_methods": [
                                        "",
                                        "HV_METH_DISC_BULK"
                                    ],
                                    "harvest_methods_optional": [ ]
                                },
                                {
                                    "min": 0,
                                    "type": "number",
                                    "abbrev": "NO_OF_PLANTS",
                                    "sub_type": "single_int",
                                    "field_name": "noOfPlant",
                                    "placeholder": "No. of plants",
                                    "harvest_methods": [
                                        "HV_METH_DISC_SINGLE_PLANT"
                                    ],
                                    "harvest_methods_optional": [
                                        "HV_METH_DISC_BULK"
                                    ]
                                }
                            ],
                            "additional_required_variables": []
                        }
                    }
                },
                "CROSS_METHOD_SINGLE_CROSS": {
                    "default": {
                        "default": {
                            "input_columns": [
                                {
                                    "abbrev": "HVDATE_CONT",
                                    "required": false,
                                    "column_name": "harvestDate",
                                    "placeholder": "Harvest Date",
                                    "retrieve_scale": false
                                },
                                {
                                    "abbrev": "HV_METH_DISC",
                                    "required": true,
                                    "column_name": "harvestMethod",
                                    "placeholder": "Harvest Method",
                                    "retrieve_scale": false
                                },
                                {
                                    "abbrev": "<none>",
                                    "required": null,
                                    "column_name": "numericVar",
                                    "placeholder": "Not applicable",
                                    "retrieve_scale": false
                                }
                            ],
                            "numeric_variables": [
                                {
                                    "min": null,
                                    "type": "number",
                                    "abbrev": "<none>",
                                    "sub_type": "single_int",
                                    "field_name": "<none>",
                                    "placeholder": "Not applicable",
                                    "harvest_methods": [
                                        "",
                                        "HV_METH_DISC_BULK"
                                    ],
                                    "harvest_methods_optional": [ ]
                                },
                                {
                                    "min": 0,
                                    "type": "number",
                                    "abbrev": "NO_OF_PLANTS",
                                    "sub_type": "single_int",
                                    "field_name": "noOfPlant",
                                    "placeholder": "No. of plants",
                                    "harvest_methods": [ ],
                                    "harvest_methods_optional": [
                                        "HV_METH_DISC_BULK"
                                    ]
                                }
                            ],
                            "additional_required_variables": []
                        }
                    }
                }
            }
        $$,
        1,
        'harvest_manager',
        (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),
        'BDS-4440 CB-HM DB: Add HM configurations for Sorghum crop'
    )
;

-- Insert HM_NAME_PATTERN_GERMPLASM_SORGHUM_DEFAULT
INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_NAME_PATTERN_GERMPLASM_SORGHUM_DEFAULT',
        'Harvest Manager Sorghum Germplasm Name Pattern-Default',
        $$			
            {
                "PLOT": {
                    "default": {
                        "not_fixed": {
                            "default": {
                                "bulk": [
                                    {
                                        "type": "field",
                                        "entity": "plot",
                                        "field_name": "germplasmDesignation",
                                        "order_number": 0
                                    },
                                    {
                                        "type": "delimiter",
                                        "value": "-",
                                        "order_number": 1
                                    },
                                    {
                                        "type": "free-text",
                                        "value": "B",
                                        "order_number": 0
                                    }
                                ],
                                "default": [
                                    {
                                        "type": "field",
                                        "entity": "plot",
                                        "field_name": "germplasmDesignation",
                                        "order_number": 0
                                    }
                                ],
                                "single plant": [
                                    {
                                        "type": "field",
                                        "entity": "plot",
                                        "field_name": "germplasmDesignation",
                                        "order_number": 0
                                    },
                                    {
                                        "type": "delimiter",
                                        "value": "-",
                                        "order_number": 1
                                    },
                                    {
                                        "type": "counter",
                                        "order_number": 2
                                    }
                                ]
                            }
                        }
                    }
                },
                "CROSS": {
                    "default": {
                        "default": {
                            "default": {
                                "default": [
                                    {
                                        "type": "free-text",
                                        "value": "",
                                        "order_number": 0
                                    }
                                ]
                            }
                        }
                    },
                    "single cross": {
                        "default": {
                            "default": {
                                "bulk": [
                                    {
                                        "type": "field",
                                        "entity": "cross",
                                        "field_name": "experimentYearYY",
                                        "order_number": 0
                                    },
                                    {
                                        "type": "free-text",
                                        "value": "X",
                                        "order_number": 1
                                    },
                                    {
                                        "type": "field",
                                        "entity": "cross",
                                        "field_name": "<TBD: program code>",
                                        "order_number": 2
                                    },
                                    {
                                        "type": "field",
                                        "entity": "cross",
                                        "field_name": "<TBD: pipeline code>",
                                        "order_number": 3
                                    },
                                    {
                                        "type": "counter",
                                        "max_digits": 3,
                                        "leading_zero": "yes",
                                        "order_number": 4
                                    }
                                ],
                                "default": [
                                    {
                                        "type": "field",
                                        "entity": "cross",
                                        "field_name": "crossName",
                                        "order_number": 0
                                    }
                                ]
                            }
                        }
                    },
                    "three-way cross": {
                        "default": {
                            "default": {
                                "bulk": [
                                    {
                                        "type": "field",
                                        "entity": "cross",
                                        "field_name": "experimentYearYY",
                                        "order_number": 0
                                    },
                                    {
                                        "type": "free-text",
                                        "value": "X",
                                        "order_number": 1
                                    },
                                    {
                                        "type": "field",
                                        "entity": "cross",
                                        "field_name": "<TBD: program code>",
                                        "order_number": 2
                                    },
                                    {
                                        "type": "field",
                                        "entity": "cross",
                                        "field_name": "<TBD: pipeline code>",
                                        "order_number": 3
                                    },
                                    {
                                        "type": "counter",
                                        "max_digits": 3,
                                        "leading_zero": "yes",
                                        "order_number": 4
                                    }
                                ],
                                "default": [
                                    {
                                        "type": "field",
                                        "entity": "cross",
                                        "field_name": "crossName",
                                        "order_number": 0
                                    }
                                ]
                            }
                        }
                    }
                },
                "harvest_mode": {
                    "cross_method": {
                        "germplasm_state": {
                            "germplasm_type": {
                                "harvest_method": [
                                    {
                                        "type": "free-text",
                                        "value": "ABC",
                                        "order_number": 0
                                    },
                                    {
                                        "type": "field",
                                        "entity": "<entity>",
                                        "field_name": "<field_name>",
                                        "order_number": 1
                                    },
                                    {
                                        "type": "delimiter",
                                        "value": "-",
                                        "order_number": 1
                                    },
                                    {
                                        "type": "counter",
                                        "order_number": 3
                                    },
                                    {
                                        "type": "db-sequence",
                                        "schema": "<schema>",
                                        "order_number": 4,
                                        "sequence_name": "<sequence_name>"
                                    },
                                    {
                                        "type": "free-text-repeater",
                                        "value": "ABC",
                                        "minimum": "2",
                                        "delimiter": "*",
                                        "order_number": 5
                                    }
                                ]
                            }
                        }
                    }
                }
            } 
        $$,
        1,
        'harvest_manager',
        (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),
        'BDS-4440 CB-HM DB: Add HM configurations for Sorghum crop'
    )
;

-- Insert HM_NAME_PATTERN_SEED_SORGHUM_DEFAULT
INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_NAME_PATTERN_SEED_SORGHUM_DEFAULT',
        'Harvest Manager Sorghum Seed Name Pattern-Default',
        $$			
            {
                "PLOT": {
                    "default": {
                        "default": {
                            "default": {
                                "default": {
                                    "single_occurrence": [
                                        {
                                            "type": "field",
                                            "entity": "plot",
                                            "field_name": "originSiteCode",
                                            "order_number": 0
                                        },
                                        {
                                            "type": "field",
                                            "entity": "plot",
                                            "field_name": "experimentYearYY",
                                            "order_number": 1
                                        },
                                        {
                                            "type": "field",
                                            "entity": "plot",
                                            "field_name": "experimentSeasonCode",
                                            "order_number": 2
                                        },
                                        {
                                            "type": "delimiter",
                                            "value": "-",
                                            "order_number": 3
                                        },
                                        {
                                            "type": "field",
                                            "entity": "plot",
                                            "field_name": "experimentName",
                                            "order_number": 4
                                        },
                                        {
                                            "type": "delimiter",
                                            "value": "-",
                                            "order_number": 5
                                        },
                                        {
                                            "type": "field",
                                            "entity": "plot",
                                            "field_name": "entryNumber",
                                            "order_number": 6
                                        }
                                    ],
                                    "multiple_occurrence": [
                                        {
                                            "type": "field",
                                            "entity": "plot",
                                            "field_name": "originSiteCode",
                                            "order_number": 0
                                        },
                                        {
                                            "type": "field",
                                            "entity": "plot",
                                            "field_name": "experimentYearYY",
                                            "order_number": 1
                                        },
                                        {
                                            "type": "field",
                                            "entity": "plot",
                                            "field_name": "experimentSeasonCode",
                                            "order_number": 2
                                        },
                                        {
                                            "type": "delimiter",
                                            "value": "-",
                                            "order_number": 3
                                        },
                                        {
                                            "type": "field",
                                            "entity": "plot",
                                            "field_name": "occurrenceName",
                                            "order_number": 4
                                        },
                                        {
                                            "type": "delimiter",
                                            "value": "-",
                                            "order_number": 5
                                        },
                                        {
                                            "type": "field",
                                            "entity": "plot",
                                            "field_name": "entryNumber",
                                            "order_number": 6
                                        }
                                    ]
                                }
                            }
                        }
                    }
                },
                "CROSS": {
                    "default": {
                        "default": {
                            "default": {
                                "default": {
                                    "same_nursery": [
                                        {
                                            "type": "field",
                                            "entity": "cross",
                                            "field_name": "originSiteCode",
                                            "order_number": 0
                                        },
                                        {
                                            "type": "field",
                                            "entity": "cross",
                                            "field_name": "experimentYearYY",
                                            "order_number": 1
                                        },
                                        {
                                            "type": "field",
                                            "entity": "cross",
                                            "field_name": "experimentSeasonCode",
                                            "order_number": 2
                                        },
                                        {
                                            "type": "delimiter",
                                            "value": "-",
                                            "order_number": 3
                                        },
                                        {
                                            "type": "field",
                                            "entity": "femaleCrossParent",
                                            "field_name": "sourceExperimentName",
                                            "order_number": 4
                                        },
                                        {
                                            "type": "delimiter",
                                            "value": "-",
                                            "order_number": 5
                                        },
                                        {
                                            "type": "field",
                                            "entity": "femaleCrossParent",
                                            "field_name": "entryNumber",
                                            "order_number": 6
                                        },
                                        {
                                            "type": "delimiter",
                                            "value": "/",
                                            "order_number": 7
                                        },
                                        {
                                            "type": "field",
                                            "entity": "maleCrossParent",
                                            "field_name": "entryNumber",
                                            "order_number": 8
                                        }
                                    ],
                                    "different_nurseries": [
                                        {
                                            "type": "field",
                                            "entity": "cross",
                                            "field_name": "originSiteCode",
                                            "order_number": 0
                                        },
                                        {
                                            "type": "field",
                                            "entity": "cross",
                                            "field_name": "experimentYearYY",
                                            "order_number": 1
                                        },
                                        {
                                            "type": "field",
                                            "entity": "cross",
                                            "field_name": "experimentSeasonCode",
                                            "order_number": 2
                                        },
                                        {
                                            "type": "delimiter",
                                            "value": "-",
                                            "order_number": 3
                                        },
                                        {
                                            "type": "field",
                                            "entity": "femaleCrossParent",
                                            "field_name": "sourceExperimentName",
                                            "order_number": 4
                                        },
                                        {
                                            "type": "delimiter",
                                            "value": "-",
                                            "order_number": 5
                                        },
                                        {
                                            "type": "field",
                                            "entity": "femaleCrossParent",
                                            "field_name": "entryNumber",
                                            "order_number": 6
                                        },
                                        {
                                            "type": "delimiter",
                                            "value": "/",
                                            "order_number": 7
                                        },
                                        {
                                            "type": "field",
                                            "entity": "maleCrossParent",
                                            "field_name": "sourceExperimentName",
                                            "order_number": 8
                                        },
                                        {
                                            "type": "delimiter",
                                            "value": "-",
                                            "order_number": 9
                                        },
                                        {
                                            "type": "field",
                                            "entity": "maleCrossParent",
                                            "field_name": "entryNumber",
                                            "order_number": 10
                                        }
                                    ]
                                }
                            }
                        }
                    }
                },
                "harvest_mode": {
                    "cross_method": {
                        "germplasm_state": {
                            "germplasm_type": {
                                "harvest_method": [
                                    {
                                        "type": "free-text",
                                        "value": "ABC",
                                        "order_number": 0
                                    },
                                    {
                                        "type": "field",
                                        "entity": "<entity>",
                                        "field_name": "<field_name>",
                                        "order_number": 1
                                    },
                                    {
                                        "type": "delimiter",
                                        "value": "-",
                                        "order_number": 1
                                    },
                                    {
                                        "type": "counter",
                                        "order_number": 3
                                    },
                                    {
                                        "type": "db-sequence",
                                        "schema": "<schema>",
                                        "order_number": 4,
                                        "sequence_name": "<sequence_name>"
                                    }
                                ]
                            }
                        }
                    }
                }
            }
        $$,
        1,
        'harvest_manager',
        (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),
        'BDS-4440 CB-HM DB: Add HM configurations for Sorghum crop'
    )
;

-- Insert HM_LABEL_PATTERN_PACKAGE_SORGHUM_DEFAULT
INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_LABEL_PATTERN_PACKAGE_SORGHUM_DEFAULT',
        'Harvest Manager Sorghum Package Label Pattern-Default',
        $$			
            {
                "PLOT": {
                    "default": {
                        "default": {
                            "default": {
                                "default": [
                                    {
                                        "type": "field",
                                        "entity": "plot",
                                        "field_name": "harvestedPackagePrefix",
                                        "order_number": 0
                                    },
                                    {
                                        "type": "delimiter",
                                        "value": "-",
                                        "order_number": 1
                                    },
                                    {
                                        "type": "counter",
                                        "max_digits": 4,
                                        "leading_zero": "yes",
                                        "order_number": 2
                                    }
                                ]
                            }
                        }
                    }
                },
                "CROSS": {
                    "default": {
                        "default": {
                            "default": {
                                "default": [
                                    {
                                        "type": "field",
                                        "entity": "cross",
                                        "field_name": "harvestedPackagePrefix",
                                        "order_number": 0
                                    },
                                    {
                                        "type": "delimiter",
                                        "value": "-",
                                        "order_number": 1
                                    },
                                    {
                                        "type": "counter",
                                        "max_digits": 4,
                                        "leading_zero": "yes",
                                        "order_number": 2
                                    }
                                ]
                            }
                        }
                    }
                },
                "harvest_mode": {
                    "cross_method": {
                        "germplasm_state": {
                            "germplasm_type": {
                                "harvest_method": [
                                    {
                                        "type": "free-text",
                                        "value": "ABC",
                                        "order_number": 0
                                    },
                                    {
                                        "type": "field",
                                        "entity": "<entity>",
                                        "field_name": "<field_name>",
                                        "order_number": 1
                                    },
                                    {
                                        "type": "delimiter",
                                        "value": "-",
                                        "order_number": 1
                                    },
                                    {
                                        "type": "counter",
                                        "order_number": 3
                                    },
                                    {
                                        "type": "db-sequence",
                                        "schema": "<schema>",
                                        "order_number": 4,
                                        "sequence_name": "<sequence_name>"
                                    }
                                ]
                            }
                        }
                    }
                }
            }
        $$,
        1,
        'harvest_manager',
        (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),
        'BDS-4440 CB-HM DB: Add HM configurations for Sorghum crop'
    )
;

-- Insert HARVESTED_PACKAGE_PREFIX_PATTERN_SORGHUM_DEFAULT
INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HARVESTED_PACKAGE_PREFIX_PATTERN_SORGHUM_DEFAULT',
        'HM: Harvested Package Prefix Pattern Sorghum Default',
        $$			
            {
                "pattern": [
                    {
                        "type": "field",
                        "field_name": "programCode",
                        "order_number": 0
                    },
                    {
                        "type": "field",
                        "field_name": "experimentYearYY",
                        "order_number": 1
                    },
                    {
                        "type": "field",
                        "field_name": "experimentSeasonCode",
                        "order_number": 2
                    }
                ]
            }
        $$,
        1,
        'harvest_manager',
        (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),
        'BDS-4440 CB-HM DB: Add HM configurations for Sorghum crop'
    )
;




--rollback DELETE FROM platform.config WHERE abbrev = 'HARVESTED_PACKAGE_PREFIX_PATTERN_SORGHUM_DEFAULT';

--rollback DELETE FROM platform.config WHERE abbrev = 'HM_LABEL_PATTERN_PACKAGE_SORGHUM_DEFAULT';

--rollback DELETE FROM platform.config WHERE abbrev = 'HM_NAME_PATTERN_SEED_SORGHUM_DEFAULT';

--rollback DELETE FROM platform.config WHERE abbrev = 'HM_NAME_PATTERN_GERMPLASM_SORGHUM_DEFAULT';

--rollback DELETE FROM platform.config WHERE abbrev = 'HM_DATA_BROWSER_CONFIG_SORGHUM';