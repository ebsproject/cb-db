--liquibase formatted sql

--changeset postgres:update_post_harvest_row_1_configs_2411 context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3964: CB-DC: Update update_post_harvest_row_1_configs_2411: Add plot ID



UPDATE
    platform.config
SET
    config_value = $${
        "Name": "Default configuration for the first row in the post-harvest data collection page",
        "Values": [
            {
                "disabled": true,
                "include_form": true,
                "target_value": "plotDbId",
                "target_column": "plotDbId",
                "variable_type": "metadata",
                "variable_abbrev": "PLOT_ID"
            },
            {
                "disabled": true,
                "include_form": true,
                "target_value": "programCode",
                "target_column": "programDbId",
                "variable_type": "metadata",
                "variable_abbrev": "PROGRAM"
            },
            {
                "disabled": true,
                "include_form": true,
                "target_value": "experimentYear",
                "target_column": "experimentYear",
                "variable_abbrev": "EXPERIMENT_YEAR"
            }
        ]
    }$$
WHERE
    abbrev = 'DC_GLOBAL_POST_HARVEST_ROW_1_SETTINGS';

UPDATE
    platform.config
SET
    config_value = $${
        "Name": "Program IRSEA configuration for the first row in the post-harvest data collection page",
        "Values": [
            {
                "disabled": true,
                "include_form": true,
                "target_value": "plotDbId",
                "target_column": "plotDbId",
                "variable_type": "metadata",
                "variable_abbrev": "PLOT_ID"
            },
            {
                "disabled": true,
                "include_form": true,
                "target_value": "programCode",
                "target_column": "programDbId",
                "variable_type": "metadata",
                "variable_abbrev": "PROGRAM"
            },
            {
                "disabled": true,
                "include_form": true,
                "target_value": "experimentYear",
                "target_column": "experimentYear",
                "variable_abbrev": "EXPERIMENT_YEAR"
            },
            {
                "disabled": true,
                "include_form": true,
                "target_value": "seasonCode",
                "target_column": "seasonDbId",
                "variable_type": "metadata",
                "variable_abbrev": "SEASON"
            },
            {
                "default": "ZES Team",
                "disabled": false,
                "target_value": "data_value",
                "target_column": "occurrenceDataDbId",
                "variable_type": "metadata",
                "variable_abbrev": "POST_HARVEST_UNIT",
                "entity_data_type": "occurrence-data"
            },
            {
                "default": "",
                "disabled": false,
                "target_value": "data_value",
                "target_column": "occurrenceDataDbId",
                "variable_type": "metadata",
                "variable_abbrev": "POST_HARVEST_COMPLETION_DATE",
                "entity_data_type": "occurrence-data"
            }
        ]
    }$$
WHERE
    abbrev = 'DC_PROGRAM_IRSEA_POST_HARVEST_ROW_1_SETTINGS';

UPDATE
    platform.config
SET
    config_value = $${
        "Name": "Program KE configuration for the first row in the post-harvest data collection page",
        "Values": [
            {
                "disabled": true,
                "include_form": true,
                "target_value": "plotDbId",
                "target_column": "plotDbId",
                "variable_type": "metadata",
                "variable_abbrev": "PLOT_ID"
            },
            {
                "disabled": true,
                "include_form": true,
                "target_value": "programCode",
                "target_column": "programDbId",
                "variable_type": "metadata",
                "variable_abbrev": "PROGRAM"
            },
            {
                "disabled": true,
                "include_form": true,
                "target_value": "seasonCode",
                "target_column": "seasonDbId",
                "variable_type": "metadata",
                "variable_abbrev": "SEASON"
            }
        ]
    }$$
WHERE
    abbrev = 'DC_PROGRAM_KE_POST_HARVEST_ROW_1_SETTINGS';

UPDATE
    platform.config
SET
    config_value = $${
        "Name": "Program BW-CIMMYT configuration for the first row in the post-harvest data collection page",
        "Values": [
            {
                "disabled": true,
                "include_form": true,
                "target_value": "plotDbId",
                "target_column": "plotDbId",
                "variable_type": "metadata",
                "variable_abbrev": "PLOT_ID"
            },
            {
                "disabled": true,
                "include_form": true,
                "target_value": "programCode",
                "target_column": "programDbId",
                "variable_type": "metadata",
                "variable_abbrev": "PROGRAM"
            },
            {
                "disabled": true,
                "include_form": true,
                "target_value": "experimentYear",
                "target_column": "experimentYear",
                "variable_abbrev": "EXPERIMENT_YEAR"
            }
        ]
    }$$
WHERE
    abbrev = 'DC_PROGRAM_BW-CIMMYT_POST_HARVEST_ROW_1_SETTINGS';

UPDATE
    platform.config
SET
    config_value = $${
        "Name": "Role collaborator configuration for the first row in the post-harvest data collection page",
        "Values": [
            {
                "disabled": true,
                "include_form": true,
                "target_value": "plotDbId",
                "target_column": "plotDbId",
                "variable_type": "metadata",
                "variable_abbrev": "PLOT_ID"
            },
            {
                "disabled": true,
                "include_form": true,
                "target_value": "programCode",
                "target_column": "programDbId",
                "variable_type": "metadata",
                "variable_abbrev": "PROGRAM"
            },
            {
                "disabled": true,
                "include_form": true,
                "target_value": "experimentYear",
                "target_column": "experimentYear",
                "variable_abbrev": "EXPERIMENT_YEAR"
            },
            {
                "disabled": true,
                "include_form": true,
                "target_value": "seasonCode",
                "target_column": "seasonDbId",
                "variable_type": "metadata",
                "variable_abbrev": "SEASON"
            }
        ]
    }$$
WHERE
    abbrev = 'DC_ROLE_COLLABORATOR_POST_HARVEST_ROW_1_SETTINGS';



--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Default configuration for the first row in the post-harvest data collection page",
--rollback         "Values": [
--rollback             {
--rollback                 "disabled": true,
--rollback                 "include_form": true,
--rollback                 "target_value": "programCode",
--rollback                 "target_column": "programDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "PROGRAM"
--rollback             },
--rollback             {
--rollback                 "disabled": true,
--rollback                 "include_form": true,
--rollback                 "target_value": "experimentYear",
--rollback                 "target_column": "experimentYear",
--rollback                 "variable_abbrev": "EXPERIMENT_YEAR"
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_GLOBAL_POST_HARVEST_ROW_1_SETTINGS';
--rollback 
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Program IRSEA configuration for the first row in the post-harvest data collection page",
--rollback         "Values": [
--rollback             {
--rollback                 "disabled": true,
--rollback                 "include_form": true,
--rollback                 "target_value": "programCode",
--rollback                 "target_column": "programDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "PROGRAM"
--rollback             },
--rollback             {
--rollback                 "disabled": true,
--rollback                 "include_form": true,
--rollback                 "target_value": "experimentYear",
--rollback                 "target_column": "experimentYear",
--rollback                 "variable_abbrev": "EXPERIMENT_YEAR"
--rollback             },
--rollback             {
--rollback                 "disabled": true,
--rollback                 "include_form": true,
--rollback                 "target_value": "seasonCode",
--rollback                 "target_column": "seasonDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "SEASON"
--rollback             },
--rollback             {
--rollback                 "default": "ZES Team",
--rollback                 "disabled": false,
--rollback                 "target_value": "data_value",
--rollback                 "target_column": "occurrenceDataDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "POST_HARVEST_UNIT",
--rollback                 "entity_data_type": "occurrence-data"
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "target_value": "data_value",
--rollback                 "target_column": "occurrenceDataDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "POST_HARVEST_COMPLETION_DATE",
--rollback                 "entity_data_type": "occurrence-data"
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_PROGRAM_IRSEA_POST_HARVEST_ROW_1_SETTINGS';
--rollback 
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Program KE configuration for the first row in the post-harvest data collection page",
--rollback         "Values": [
--rollback             {
--rollback                 "disabled": true,
--rollback                 "include_form": true,
--rollback                 "target_value": "programCode",
--rollback                 "target_column": "programDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "PROGRAM"
--rollback             },
--rollback             {
--rollback                 "disabled": true,
--rollback                 "include_form": true,
--rollback                 "target_value": "seasonCode",
--rollback                 "target_column": "seasonDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "SEASON"
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_PROGRAM_KE_POST_HARVEST_ROW_1_SETTINGS';
--rollback 
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Program BW-CIMMYT configuration for the first row in the post-harvest data collection page",
--rollback         "Values": [
--rollback             {
--rollback                 "disabled": true,
--rollback                 "include_form": true,
--rollback                 "target_value": "programCode",
--rollback                 "target_column": "programDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "PROGRAM"
--rollback             },
--rollback             {
--rollback                 "disabled": true,
--rollback                 "include_form": true,
--rollback                 "target_value": "experimentYear",
--rollback                 "target_column": "experimentYear",
--rollback                 "variable_abbrev": "EXPERIMENT_YEAR"
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_PROGRAM_BW-CIMMYT_POST_HARVEST_ROW_1_SETTINGS';
--rollback 
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Role collaborator configuration for the first row in the post-harvest data collection page",
--rollback         "Values": [
--rollback             {
--rollback                 "disabled": true,
--rollback                 "include_form": true,
--rollback                 "target_value": "programCode",
--rollback                 "target_column": "programDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "PROGRAM"
--rollback             },
--rollback             {
--rollback                 "disabled": true,
--rollback                 "include_form": true,
--rollback                 "target_value": "experimentYear",
--rollback                 "target_column": "experimentYear",
--rollback                 "variable_abbrev": "EXPERIMENT_YEAR"
--rollback             },
--rollback             {
--rollback                 "disabled": true,
--rollback                 "include_form": true,
--rollback                 "target_value": "seasonCode",
--rollback                 "target_column": "seasonDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "SEASON"
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_ROLE_COLLABORATOR_POST_HARVEST_ROW_1_SETTINGS';