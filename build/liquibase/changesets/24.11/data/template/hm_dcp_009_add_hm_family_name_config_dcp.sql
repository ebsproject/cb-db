--liquibase formatted sql

--changeset postgres:hm_dcp_009_add_hm_family_name_config_dcp context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-4439 HM DB: Add configurations for Family Names for DCP



INSERT INTO
    platform.config(
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES 
(
    'HM_NAME_PATTERN_FAMILY_CHICKPEA_DEFAULT',
    'Harvest Manager Chickpea Family Name Pattern-Default', 
    $${
        "PLOT": {
            "default": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "free-text",
                                "value": "XXXXX",
                                "order_number": 0
                            }
                        ]
                    }
                }
            }
        },
        "CROSS": {
            "default": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "field",
                                "entity": "germplasm",
                                "field_name": "designation",
                                "order_number": 0
                            }
                        ]
                    }
                }
            }
        },
        "harvest_mode": {
            "cross_method": {
                "germplasm_state": {
                    "germplasm_type": {
                        "harvest_method": [
                            {
                                "type": "free-text",
                                "value": "ABC",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "<entity>",
                                "field_name": "<field_name>",
                                "order_number": 1
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 3
                            },
                            {
                                "type": "db-sequence",
                                "schema": "<schema>",
                                "order_number": 4,
                                "sequence_name": "<sequence_name>"
                            }
                        ]
                    }
                }
            }
        }
    }$$,
    1,
    'harvest_manager',
    (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),
    'BDS-4439 HM DB: Add configurations for Family Names for DCP'
),
(
    'HM_NAME_PATTERN_FAMILY_GROUNDNUT_DEFAULT',
    'Harvest Manager Groundnut Family Name Pattern-Default', 
    $${
        "PLOT": {
            "default": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "free-text",
                                "value": "XXXXX",
                                "order_number": 0
                            }
                        ]
                    }
                }
            }
        },
        "CROSS": {
            "default": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "field",
                                "entity": "germplasm",
                                "field_name": "designation",
                                "order_number": 0
                            }
                        ]
                    }
                }
            }
        },
        "harvest_mode": {
            "cross_method": {
                "germplasm_state": {
                    "germplasm_type": {
                        "harvest_method": [
                            {
                                "type": "free-text",
                                "value": "ABC",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "<entity>",
                                "field_name": "<field_name>",
                                "order_number": 1
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 3
                            },
                            {
                                "type": "db-sequence",
                                "schema": "<schema>",
                                "order_number": 4,
                                "sequence_name": "<sequence_name>"
                            }
                        ]
                    }
                }
            }
        }
    }$$,
    1,
    'harvest_manager',
    (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),
    'BDS-4439 HM DB: Add configurations for Family Names for DCP'
),
(
    'HM_NAME_PATTERN_FAMILY_SORGHUM_DEFAULT',
    'Harvest Manager Sorghum Family Name Pattern-Default', 
    $${
        "PLOT": {
            "default": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "free-text",
                                "value": "XXXXX",
                                "order_number": 0
                            }
                        ]
                    }
                }
            }
        },
        "CROSS": {
            "default": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "field",
                                "entity": "germplasm",
                                "field_name": "designation",
                                "order_number": 0
                            }
                        ]
                    }
                }
            }
        },
        "harvest_mode": {
            "cross_method": {
                "germplasm_state": {
                    "germplasm_type": {
                        "harvest_method": [
                            {
                                "type": "free-text",
                                "value": "ABC",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "<entity>",
                                "field_name": "<field_name>",
                                "order_number": 1
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 3
                            },
                            {
                                "type": "db-sequence",
                                "schema": "<schema>",
                                "order_number": 4,
                                "sequence_name": "<sequence_name>"
                            }
                        ]
                    }
                }
            }
        }
    }$$,
    1,
    'harvest_manager',
    (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),
    'BDS-4439 HM DB: Add configurations for Family Names for DCP'
),
(
    'HM_NAME_PATTERN_FAMILY_FINGERMILLET_DEFAULT',
    'Harvest Manager Finger Millet Family Name Pattern-Default', 
    $${
        "PLOT": {
            "default": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "free-text",
                                "value": "XXXXX",
                                "order_number": 0
                            }
                        ]
                    }
                }
            }
        },
        "CROSS": {
            "default": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "field",
                                "entity": "germplasm",
                                "field_name": "designation",
                                "order_number": 0
                            }
                        ]
                    }
                }
            }
        },
        "harvest_mode": {
            "cross_method": {
                "germplasm_state": {
                    "germplasm_type": {
                        "harvest_method": [
                            {
                                "type": "free-text",
                                "value": "ABC",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "<entity>",
                                "field_name": "<field_name>",
                                "order_number": 1
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 3
                            },
                            {
                                "type": "db-sequence",
                                "schema": "<schema>",
                                "order_number": 4,
                                "sequence_name": "<sequence_name>"
                            }
                        ]
                    }
                }
            }
        }
    }$$,
    1,
    'harvest_manager',
    (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),
    'BDS-4439 HM DB: Add configurations for Family Names for DCP'
),
(
    'HM_NAME_PATTERN_FAMILY_PEARLMILLET_DEFAULT',
    'Harvest Manager Pearl Millet Family Name Pattern-Default', 
    $${
        "PLOT": {
            "default": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "free-text",
                                "value": "XXXXX",
                                "order_number": 0
                            }
                        ]
                    }
                }
            }
        },
        "CROSS": {
            "default": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "field",
                                "entity": "germplasm",
                                "field_name": "designation",
                                "order_number": 0
                            }
                        ]
                    }
                }
            }
        },
        "harvest_mode": {
            "cross_method": {
                "germplasm_state": {
                    "germplasm_type": {
                        "harvest_method": [
                            {
                                "type": "free-text",
                                "value": "ABC",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "<entity>",
                                "field_name": "<field_name>",
                                "order_number": 1
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 3
                            },
                            {
                                "type": "db-sequence",
                                "schema": "<schema>",
                                "order_number": 4,
                                "sequence_name": "<sequence_name>"
                            }
                        ]
                    }
                }
            }
        }
    }$$,
    1,
    'harvest_manager',
    (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),
    'BDS-4439 HM DB: Add configurations for Family Names for DCP'
),
(
    'HM_NAME_PATTERN_FAMILY_PIGEONPEA_DEFAULT',
    'Harvest Manager Pigeon Pea Family Name Pattern-Default', 
    $${
        "PLOT": {
            "default": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "free-text",
                                "value": "XXXXX",
                                "order_number": 0
                            }
                        ]
                    }
                }
            }
        },
        "CROSS": {
            "default": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "field",
                                "entity": "germplasm",
                                "field_name": "designation",
                                "order_number": 0
                            }
                        ]
                    }
                }
            }
        },
        "harvest_mode": {
            "cross_method": {
                "germplasm_state": {
                    "germplasm_type": {
                        "harvest_method": [
                            {
                                "type": "free-text",
                                "value": "ABC",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "<entity>",
                                "field_name": "<field_name>",
                                "order_number": 1
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 3
                            },
                            {
                                "type": "db-sequence",
                                "schema": "<schema>",
                                "order_number": 4,
                                "sequence_name": "<sequence_name>"
                            }
                        ]
                    }
                }
            }
        }
    }$$,
    1,
    'harvest_manager',
    (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),
    'BDS-4439 HM DB: Add configurations for Family Names for DCP'
);



--rollback DELETE FROM
--rollback     platform.config
--rollback WHERE
--rollback     abbrev IN (
--rollback         'HM_NAME_PATTERN_FAMILY_CHICKPEA_DEFAULT',
--rollback         'HM_NAME_PATTERN_FAMILY_GROUNDNUT_DEFAULT',
--rollback         'HM_NAME_PATTERN_FAMILY_SORGHUM_DEFAULT',
--rollback         'HM_NAME_PATTERN_FAMILY_FINGERMILLET_DEFAULT',
--rollback         'HM_NAME_PATTERN_FAMILY_PEARLMILLET_DEFAULT',
--rollback         'HM_NAME_PATTERN_FAMILY_PIGEONPEA_DEFAULT'
--rollback     );