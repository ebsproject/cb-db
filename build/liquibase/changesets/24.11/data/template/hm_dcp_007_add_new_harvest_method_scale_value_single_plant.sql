--liquibase formatted sql

--changeset postgres:hm_dcp_007_add_new_harvest_method_scale_value_single_plant context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-4409 CB-HM DB: Add new harvest method scale value for single plant


-- add scale value to HV_METH_DISC variable
INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, creator_id, scale_value_status)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.creator_id,
    scalval.scale_value_status
FROM
    master.variable AS var,
    (
        VALUES
        ('HV_METH_DISC_SINGLE_PLANT', 'Single Plant', 24, 'Single Plant', 'Single Plant', (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1), NULL)
    ) AS scalval (
        abbrev, value, order_number, description, display_name, creator_id, scale_value_status
    )
WHERE
    var.abbrev = 'HV_METH_DISC'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN ('HV_METH_DISC_SINGLE_PLANT')
--rollback ;