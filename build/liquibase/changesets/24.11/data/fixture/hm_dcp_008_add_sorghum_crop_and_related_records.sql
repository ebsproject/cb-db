--liquibase formatted sql

--changeset postgres:hm_dcp_008_add_sorghum_crop_and_related_records context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-4441 CB-HM DB: Add new crop and other related records for SORGHUM


-- Insert new crop record for SORGHUM
INSERT INTO
    tenant.crop
    (crop_code, crop_name, description, creator_id)
VALUES
    ('SORGHUM', 'Sorghum', 'Sorghum crop', (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new crop program record for SORGHUM
INSERT INTO
    tenant.crop_program
    (crop_program_code, crop_program_name, description, organization_id, crop_id, creator_id)
VALUES
    ('SORGHUM_PROG', 'Sorghum Program', 'Sorghum Program', (SELECT id FROM tenant.organization WHERE organization_code='CIMMYT_DRYLAND_CROPS' LIMIT 1), (SELECT id FROM tenant.crop WHERE crop_code='SORGHUM' LIMIT 1), (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new program record for SORGHUM
INSERT INTO 
    tenant.program
    (program_code, program_name, program_type, program_status, description, crop_program_id, creator_id)
VALUES 
    ('SRG', 'Sorghum Breeding Program', 'breeding', 'active', 'Sorghum Breeding Program', (SELECT id FROM tenant.crop_program WHERE crop_program_code='SORGHUM_PROG' LIMIT 1), (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new team record for SORGHUM
INSERT INTO 
    tenant.team
    (team_code, team_name, description, creator_id)
VALUES 
    ('SRG_TEAM', 'Sorghum Breeding Team', 'Sorghum Breeding Team', (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new team program record for SORGHUM
INSERT INTO 
    tenant.program_team
    (program_id, team_id, order_number, creator_id)
VALUES 
    ((SELECT id FROM tenant.program WHERE program_code='SRG' LIMIT 1), (SELECT id FROM tenant.team WHERE team_code='SRG_TEAM' LIMIT 1), 1, (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new pipeline record for SORGHUM
INSERT INTO
    tenant.pipeline
    (pipeline_code, pipeline_name, pipeline_status, creator_id)
VALUES
    ('SRG_PIPELINE', 'Sorghum Pipeline', 'active', (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));    

-- Insert new project record for SORGHUM
INSERT INTO
    tenant.project
    (project_code, project_name, project_status, program_id, pipeline_id, leader_id, creator_id, description, notes)
VALUES
    ('SORGHUM_PROJECT', 'Sorghum Project', 'active', (SELECT id FROM tenant.program WHERE program_code='SRG' LIMIT 1), (SELECT id FROM tenant.pipeline WHERE pipeline_code='SRG_PIPELINE' LIMIT 1), (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1), (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1), 'Sorghum Project', 'Sorghum Project');

-- Insert new taxonomy record for SORGHUM
INSERT INTO 
    germplasm.taxonomy
(taxon_id, taxonomy_name, description, crop_id, creator_id)
VALUES 
    ('41680', 'Sorghum bicolor L.', 'Sorghum bicolor L. (Sorghum)', (SELECT id FROM tenant.crop WHERE crop_code='SORGHUM'), (SELECT id FROM tenant.person WHERE username='admin'));

--rollback DELETE FROM germplasm.taxonomy WHERE taxon_id = '41680';

--rollback DELETE FROM tenant.project WHERE project_code = 'SORGHUM_PROJECT';

--rollback DELETE FROM tenant.pipeline WHERE pipeline_code = 'SRG_PIPELINE';

--rollback    DELETE
--rollback    FROM tenant.program_team
--rollback    WHERE (program_id IN (SELECT id FROM tenant.program WHERE program_code IN ('SRG')))
--rollback     AND order_number = 1;

--rollback DELETE FROM tenant.team WHERE team_code = 'SRG_TEAM';

--rollback DELETE FROM tenant.program WHERE program_code = 'SRG';

--rollback DELETE FROM tenant.crop_program WHERE crop_program_code = 'SORGHUM_PROG';

--rollback DELETE FROM tenant.crop WHERE crop_code = 'SORGHUM';