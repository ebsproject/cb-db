--liquibase formatted sql

--changeset postgres:hm_dcp_014_add_pearlmillet_crop_and_related_records context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-4441 CB-HM DB: Add new crop and other related records for PEARL MILLET


-- Insert new crop record for PEARL MILLET
INSERT INTO
    tenant.crop
    (crop_code, crop_name, description, creator_id)
VALUES
    ('PEARLMILLET', 'Pearl Millet', 'Pearl Millet crop', (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new crop program record for PEARL MILLET
INSERT INTO
    tenant.crop_program
    (crop_program_code, crop_program_name, description, organization_id, crop_id, creator_id)
VALUES
    ('PEARLMILLET_PROG', 'Pearl Millet Program', 'Pearl Millet Program', (SELECT id FROM tenant.organization WHERE organization_code='CIMMYT_DRYLAND_CROPS' LIMIT 1), (SELECT id FROM tenant.crop WHERE crop_code='PEARLMILLET' LIMIT 1), (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new program record for PEARL MILLET
INSERT INTO 
    tenant.program
    (program_code, program_name, program_type, program_status, description, crop_program_id, creator_id)
VALUES 
    ('PML', 'Pearl Millet Breeding Program', 'breeding', 'active', 'Pearl Millet Breeding Program', (SELECT id FROM tenant.crop_program WHERE crop_program_code='PEARLMILLET_PROG' LIMIT 1), (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new team record for PEARL MILLET
INSERT INTO 
    tenant.team
    (team_code, team_name, description, creator_id)
VALUES 
    ('PML_TEAM', 'Pearl Millet Breeding Team', 'Pearl Millet Breeding Team', (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new team program record for PEARL MILLET
INSERT INTO 
    tenant.program_team
    (program_id, team_id, order_number, creator_id)
VALUES 
    ((SELECT id FROM tenant.program WHERE program_code='PML' LIMIT 1), (SELECT id FROM tenant.team WHERE team_code='PML_TEAM' LIMIT 1), 1, (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new pipeline record for PEARL MILLET
INSERT INTO
    tenant.pipeline
    (pipeline_code, pipeline_name, pipeline_status, creator_id)
VALUES
    ('PML_PIPELINE', 'Pearl Millet Pipeline', 'active', (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));    

-- Insert new project record for PEARL MILLET
INSERT INTO
    tenant.project
    (project_code, project_name, project_status, program_id, pipeline_id, leader_id, creator_id, description, notes)
VALUES
    ('PEARLMILLET_PROJECT', 'Pearl Millet Project', 'active', (SELECT id FROM tenant.program WHERE program_code='PML' LIMIT 1), (SELECT id FROM tenant.pipeline WHERE pipeline_code='PML_PIPELINE' LIMIT 1), (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1), (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1), 'Pearl Millet Project', 'Pearl Millet Project');

-- Insert new taxonomy record for PEARL MILLET
INSERT INTO 
    germplasm.taxonomy
(taxon_id, taxonomy_name, description, crop_id, creator_id)
VALUES 
    ('41700', 'Pennisetum glaucum L.', 'Pennisetum glaucum L. (Pearl Millet)', (SELECT id FROM tenant.crop WHERE crop_code='PEARLMILLET'), (SELECT id FROM tenant.person WHERE username='admin'));

--rollback DELETE FROM germplasm.taxonomy WHERE taxon_id = '41700';

--rollback DELETE FROM tenant.project WHERE project_code = 'PEARLMILLET_PROJECT';

--rollback DELETE FROM tenant.pipeline WHERE pipeline_code = 'PML_PIPELINE';

--rollback    DELETE
--rollback    FROM tenant.program_team
--rollback    WHERE (program_id IN (SELECT id FROM tenant.program WHERE program_code IN ('PML')))
--rollback     AND order_number = 1;

--rollback DELETE FROM tenant.team WHERE team_code = 'PML_TEAM';

--rollback DELETE FROM tenant.program WHERE program_code = 'PML';

--rollback DELETE FROM tenant.crop_program WHERE crop_program_code = 'PEARLMILLET_PROG';

--rollback DELETE FROM tenant.crop WHERE crop_code = 'PEARLMILLET';