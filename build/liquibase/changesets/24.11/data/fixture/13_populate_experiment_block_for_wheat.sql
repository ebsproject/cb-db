--liquibase formatted sql

--changeset postgres:13_populate_experiment_block_for_wheat context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2442 CB DB - Extract international Wheat nursery data from test environment.



INSERT INTO experiment.experiment_block
(experiment_block_code, experiment_block_name, experiment_id, parent_experiment_block_id, order_number, block_type, no_of_blocks, no_of_ranges, no_of_cols, no_of_reps, plot_numbering_order, starting_corner, creator_id, is_void, notes)
VALUES
('VOIDED-309-309-47', 'Nursery block 1', (select ex.id from experiment.experiment ex where ex.experiment_name ='BW-CIMMYT-BRE-2024-B-002'), NULL, 1, 'parent', 0, NULL, NULL, NULL, NULL, NULL, (select p.id from tenant.person p where p.username = 'admin'), true, NULL),
('BW-CIMMYT-BRE-2024-B-002|EO-1', 'Entry order block 1', (select ex.id from experiment.experiment ex where ex.experiment_name ='BW-CIMMYT-BRE-2024-B-002'), NULL, 1, 'parent', 0, 0, 0, 1, 'Column serpentine', 'Top Left', (select p.id from tenant.person p where p.username = 'admin'), false, '[]');


--rollback DELETE FROM experiment.experiment_block
--rollback WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE experiment_name IN (
--rollback 'BW-CIMMYT-BRE-2024-B-002'
--rollback ));