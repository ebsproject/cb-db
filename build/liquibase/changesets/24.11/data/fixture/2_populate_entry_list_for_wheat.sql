--liquibase formatted sql

--changeset postgres:2_populate_entry_list_for_wheat context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2442 CB DB - Extract international Wheat nursery data from test environment.

INSERT INTO experiment.entry_list
(entry_list_code, entry_list_name, description, entry_list_status, experiment_id, creator_id, is_void, notes, entry_list_type, cross_count, experiment_year, season_id, site_id, crop_id, program_id)
VALUES
((experiment.generate_code('entry_list')), 'BW-CIMMYT-BRE-2024-B-002', NULL, 'completed', (select ex.id from experiment.experiment ex where ex.experiment_name ='BW-CIMMYT-BRE-2024-B-002'), (select p.id from tenant.person p where p.username = 'admin'), false, NULL, 'entry list', 0, NULL, (select s.id from tenant.season s where s.season_code = ''), (select g.id from place.geospatial_object g where g.geospatial_object_code = ''), (select c.id from tenant.crop c where c.crop_code = 'WHEAT'), (select pr.id from tenant.program pr where pr.program_name = 'BW Wheat Breeding Program')),
((experiment.generate_code('entry_list')), 'BW-CIMMYT-AYT-2023-DS-002', NULL, 'completed', (select ex.id from experiment.experiment ex where ex.experiment_name ='BW-CIMMYT-AYT-2023-DS-002'), (select p.id from tenant.person p where p.username = 'admin'), false, NULL, 'entry list', 0, NULL, (select s.id from tenant.season s where s.season_code = ''), (select g.id from place.geospatial_object g where g.geospatial_object_code = ''), (select c.id from tenant.crop c where c.crop_code = 'WHEAT'), (select pr.id from tenant.program pr where pr.program_name = 'BW Wheat Breeding Program'));

--rollback DELETE FROM
--rollback experiment.entry_list
--rollback WHERE
--rollback entry_list_name = 'BW-CIMMYT-BRE-2024-B-002';
--rollback DELETE FROM
--rollback experiment.entry_list
--rollback WHERE
--rollback entry_list_name = 'BW-CIMMYT-AYT-2023-DS-002';