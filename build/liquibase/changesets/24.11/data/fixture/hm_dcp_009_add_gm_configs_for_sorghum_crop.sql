--liquibase formatted sql

--changeset postgres:hm_dcp_009_add_gm_configs_for_sorghum_crop context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-4441 CB-HM DB: Add GM configs for Sorghum crop



-- Insert GM_FILE_UPLOAD_VARIABLES_CONFIG_SORGHUM_DEFAULT config
INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'GM_FILE_UPLOAD_VARIABLES_CONFIG_SORGHUM_DEFAULT',
        'Germplasm File Upload variables configuration for Sorghum',
        $$			
            {
                "values": [
                    {
                        "name": "Germplasm Type",
                        "type": "column",
                        "view": {
                            "visible": "true",
                            "entities": [
                                "germplasm"
                            ]
                        },
                        "usage": "optional",
                        "abbrev": "GERMPLASM_TYPE",
                        "entity": "germplasm",
                        "required": "false",
                        "api_field": "germplasmType",
                        "data_type": "string",
                        "retrieve_db_id": "false",
                        "retrieve_endpoint": "",
                        "retrieve_api_value_field": "",
                        "retrieve_api_search_field": ""
                    },
                    {
                        "name": "Grain Color",
                        "type": "attribute",
                        "view": {
                            "visible": "false",
                            "entities": []
                        },
                        "usage": "optional",
                        "abbrev": "GRAIN_COLOR",
                        "entity": "germplasm",
                        "required": "false",
                        "api_field": "dataValue",
                        "data_type": "string",
                        "retrieve_db_id": "false",
                        "retrieve_endpoint": "",
                        "retrieve_api_value_field": "",
                        "retrieve_api_search_field": ""
                    },
                    {
                        "name": "Heterotic Group",
                        "type": "attribute",
                        "view": {
                            "visible": "false",
                            "entities": []
                        },
                        "usage": "optional",
                        "abbrev": "HETEROTIC_GROUP",
                        "entity": "germplasm",
                        "required": "false",
                        "api_field": "dataValue",
                        "data_type": "string",
                        "retrieve_db_id": "false",
                        "retrieve_endpoint": "",
                        "retrieve_api_value_field": "",
                        "retrieve_api_search_field": ""
                    },
                    {
                        "name": "DH First Increase Completed",
                        "type": "attribute",
                        "view": {
                            "visible": "false",
                            "entities": []
                        },
                        "usage": "optional",
                        "abbrev": "DH_FIRST_INCREASE_COMPLETED",
                        "entity": "germplasm",
                        "required": "false",
                        "api_field": "dataValue",
                        "data_type": "string",
                        "retrieve_db_id": "false",
                        "retrieve_endpoint": "",
                        "retrieve_api_value_field": "",
                        "retrieve_api_search_field": ""
                    },
                    {
                        "name": "Program Code",
                        "type": "column",
                        "view": {
                            "visible": "true",
                            "entities": [
                                "seed"
                            ]
                        },
                        "usage": "optional",
                        "abbrev": "PROGRAM_CODE",
                        "entity": "seed",
                        "required": "false",
                        "api_field": "programDbId",
                        "data_type": "string",
                        "retrieve_db_id": "true",
                        "retrieve_endpoint": "programs",
                        "retrieve_api_value_field": "programDbId",
                        "retrieve_api_search_field": "programCode"
                    },
                    {
                        "name": "Harvest Date",
                        "type": "column",
                        "view": {
                            "visible": "true",
                            "entities": [
                                "seed"
                            ]
                        },
                        "usage": "optional",
                        "abbrev": "HVDATE_CONT",
                        "entity": "seed",
                        "required": "false",
                        "api_field": "harvestDate",
                        "data_type": "date",
                        "retrieve_db_id": "false",
                        "retrieve_endpoint": "",
                        "retrieve_api_value_field": "",
                        "retrieve_api_search_field": ""
                    },
                    {
                        "name": "Harvest Method",
                        "type": "column",
                        "view": {
                            "visible": "true",
                            "entities": [
                                "seed"
                            ]
                        },
                        "usage": "optional",
                        "abbrev": "HV_METH_DISC",
                        "entity": "seed",
                        "required": "false",
                        "api_field": "harvestMethod",
                        "data_type": "string",
                        "retrieve_db_id": "false",
                        "retrieve_endpoint": "",
                        "retrieve_api_value_field": "",
                        "retrieve_api_search_field": ""
                    },
                    {
                        "name": "Description",
                        "type": "column",
                        "view": {
                            "visible": "true",
                            "entities": [
                                "seed"
                            ]
                        },
                        "usage": "optional",
                        "abbrev": "DESCRIPTION",
                        "entity": "seed",
                        "required": "false",
                        "api_field": "description",
                        "data_type": "string",
                        "retrieve_db_id": "false",
                        "retrieve_endpoint": "",
                        "retrieve_api_value_field": "",
                        "retrieve_api_search_field": ""
                    },
                    {
                        "name": "MTA Status",
                        "type": "attribute",
                        "view": {
                            "visible": "false",
                            "entities": []
                        },
                        "usage": "optional",
                        "abbrev": "MTA_STATUS",
                        "entity": "seed",
                        "required": "false",
                        "api_field": "dataValue",
                        "data_type": "string",
                        "retrieve_db_id": "false",
                        "retrieve_endpoint": "",
                        "retrieve_api_value_field": "",
                        "retrieve_api_search_field": ""
                    },
                    {
                        "name": "IP Status",
                        "type": "attribute",
                        "view": {
                            "visible": "false",
                            "entities": []
                        },
                        "usage": "optional",
                        "abbrev": "IP_STATUS",
                        "entity": "seed",
                        "required": "false",
                        "api_field": "dataValue",
                        "data_type": "string",
                        "retrieve_db_id": "false",
                        "retrieve_endpoint": "",
                        "retrieve_api_value_field": "",
                        "retrieve_api_search_field": ""
                    },
                    {
                        "name": "Source Organization",
                        "type": "attribute",
                        "view": {
                            "visible": "false",
                            "entities": []
                        },
                        "usage": "optional",
                        "abbrev": "SOURCE_ORGANIZATION",
                        "entity": "seed",
                        "required": "false",
                        "api_field": "dataValue",
                        "data_type": "string",
                        "retrieve_db_id": "false",
                        "retrieve_endpoint": "",
                        "retrieve_api_value_field": "",
                        "retrieve_api_search_field": ""
                    },
                    {
                        "name": "Origin",
                        "type": "attribute",
                        "view": {
                            "visible": "false",
                            "entities": []
                        },
                        "usage": "optional",
                        "abbrev": "ORIGIN",
                        "entity": "seed",
                        "required": "false",
                        "api_field": "dataValue",
                        "data_type": "string",
                        "retrieve_db_id": "false",
                        "retrieve_endpoint": "",
                        "retrieve_api_value_field": "",
                        "retrieve_api_search_field": ""
                    },
                    {
                        "name": "Source Study",
                        "type": "attribute",
                        "view": {
                            "visible": "false",
                            "entities": []
                        },
                        "usage": "optional",
                        "abbrev": "SOURCE_STUDY",
                        "entity": "seed",
                        "required": "false",
                        "api_field": "dataValue",
                        "data_type": "string",
                        "retrieve_db_id": "false",
                        "retrieve_endpoint": "",
                        "retrieve_api_value_field": "",
                        "retrieve_api_search_field": ""
                    },
                    {
                        "name": "Source Harvest Year",
                        "type": "attribute",
                        "view": {
                            "visible": "false",
                            "entities": []
                        },
                        "usage": "optional",
                        "abbrev": "SOURCE_HARV_YEAR",
                        "entity": "seed",
                        "required": "false",
                        "api_field": "dataValue",
                        "data_type": "string",
                        "retrieve_db_id": "false",
                        "retrieve_endpoint": "",
                        "retrieve_api_value_field": "",
                        "retrieve_api_search_field": ""
                    },
                    {
                        "name": "MTA Number",
                        "type": "attribute",
                        "view": {
                            "visible": "false",
                            "entities": []
                        },
                        "usage": "optional",
                        "abbrev": "MTA_NUMBER",
                        "entity": "seed",
                        "required": "false",
                        "api_field": "dataValue",
                        "data_type": "string",
                        "retrieve_db_id": "false",
                        "retrieve_endpoint": "",
                        "retrieve_api_value_field": "",
                        "retrieve_api_search_field": ""
                    },
                    {
                        "name": "Import Attribute",
                        "type": "attribute",
                        "view": {
                            "visible": "false",
                            "entities": []
                        },
                        "usage": "optional",
                        "abbrev": "IMPORT_ATTRIBUTE",
                        "entity": "seed",
                        "required": "false",
                        "api_field": "dataValue",
                        "data_type": "string",
                        "retrieve_db_id": "false",
                        "retrieve_endpoint": "",
                        "retrieve_api_value_field": "",
                        "retrieve_api_search_field": ""
                    }
                ]
            }
        $$,
        1,
        'File upload, data validation, and creation of records',
        (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),
        'BDS-4441 CB-HM DB: Add GM configs for Sorghum crop'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='GM_FILE_UPLOAD_VARIABLES_CONFIG_SORGHUM_DEFAULT';