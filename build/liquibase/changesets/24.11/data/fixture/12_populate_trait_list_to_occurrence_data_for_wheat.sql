--liquibase formatted sql

--changeset postgres:12_populate_trait_list_to_occurrence_data_for_wheat context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2442 CB DB - Extract international Wheat nursery data from test environment.


INSERT INTO 
    experiment.occurrence_data
        (occurrence_id,variable_id,data_value,data_qc_code,creator_id,notes,protocol_id)
SELECT 
	occur.id AS occurrence_id,
	(SELECT id FROM master.variable WHERE abbrev = 'TRAIT_PROTOCOL_LIST_ID') AS variable_id,
	(SELECT id FROM platform.list WHERE abbrev IN (SELECT 'TRAIT_PROTOCOL_' ||occur.occurrence_code from experiment.occurrence where id = occur.id )) AS data_value,
	'G',
	(SELECT id FROM tenant.person WHERE username ='admin') AS creator_id,
	'Inserted via liquibase changeset (BDS-2442)' notes,
	(SELECT id FROM tenant.protocol WHERE protocol_code  IN (SELECT 'TRAIT_PROTOCOL_' || exp.experiment_code FROM experiment.experiment exp WHERE id = occur.experiment_id)) AS protocol_id
FROM
	experiment.occurrence occur
WHERE
	occur.experiment_id in (select id from experiment.experiment e where e.experiment_name in ('BW-CIMMYT-BRE-2024-B-002','BW-CIMMYT-AYT-2023-DS-002'));


--rollback DELETE FROM experiment.occurrence_data 
--rollback WHERE
--rollback variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'TRAIT_PROTOCOL_LIST_ID') 
--rollback AND
--rollback notes = 'Inserted via liquibase changeset (BDS-2442)'