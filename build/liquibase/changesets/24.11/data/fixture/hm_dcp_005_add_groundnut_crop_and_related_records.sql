--liquibase formatted sql

--changeset postgres:hm_dcp_005_add_groundnut_crop_and_related_records context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-4441 CB-HM DB: Add new crop and other related records for GROUNDNUT


-- Insert new crop record for GROUNDNUT
INSERT INTO
    tenant.crop
    (crop_code, crop_name, description, creator_id)
VALUES
    ('GROUNDNUT', 'Groundnut', 'Groundnut crop', (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new crop program record for GROUNDNUT
INSERT INTO
    tenant.crop_program
    (crop_program_code, crop_program_name, description, organization_id, crop_id, creator_id)
VALUES
    ('GROUNDNUT_PROG', 'Groundnut Program', 'Groundnut Program', (SELECT id FROM tenant.organization WHERE organization_code='CIMMYT_DRYLAND_CROPS' LIMIT 1), (SELECT id FROM tenant.crop WHERE crop_code='GROUNDNUT' LIMIT 1), (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new program record for GROUNDNUT
INSERT INTO 
    tenant.program
    (program_code, program_name, program_type, program_status, description, crop_program_id, creator_id)
VALUES 
    ('GNT', 'Groundnut Breeding Program', 'breeding', 'active', 'Groundnut Breeding Program', (SELECT id FROM tenant.crop_program WHERE crop_program_code='GROUNDNUT_PROG' LIMIT 1), (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new team record for GROUNDNUT
INSERT INTO 
    tenant.team
    (team_code, team_name, description, creator_id)
VALUES 
    ('GNT_TEAM', 'Groundnut Breeding Team', 'Groundnut Breeding Team', (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new team program record for GROUNDNUT
INSERT INTO 
    tenant.program_team
    (program_id, team_id, order_number, creator_id)
VALUES 
    ((SELECT id FROM tenant.program WHERE program_code='GNT' LIMIT 1), (SELECT id FROM tenant.team WHERE team_code='GNT_TEAM' LIMIT 1), 1, (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new pipeline record for GROUNDNUT
INSERT INTO
    tenant.pipeline
    (pipeline_code, pipeline_name, pipeline_status, creator_id)
VALUES
    ('GNT_PIPELINE', 'Groundnut Pipeline', 'active', (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));    

-- Insert new project record for GROUNDNUT
INSERT INTO
    tenant.project
    (project_code, project_name, project_status, program_id, pipeline_id, leader_id, creator_id, description, notes)
VALUES
    ('GROUNDNUT_PROJECT', 'Groundnut Project', 'active', (SELECT id FROM tenant.program WHERE program_code='GNT' LIMIT 1), (SELECT id FROM tenant.pipeline WHERE pipeline_code='GNT_PIPELINE' LIMIT 1), (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1), (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1), 'Groundnut Project', 'Groundnut Project');

-- Insert new taxonomy record for GROUNDNUT
INSERT INTO 
    germplasm.taxonomy
(taxon_id, taxonomy_name, description, crop_id, creator_id)
VALUES 
    ('41670', 'Arachis hypogaea L.', 'Arachis hypogaea L. (Groundnut)', (SELECT id FROM tenant.crop WHERE crop_code='GROUNDNUT'), (SELECT id FROM tenant.person WHERE username='admin'));

--rollback DELETE FROM germplasm.taxonomy WHERE taxon_id = '41670';

--rollback DELETE FROM tenant.project WHERE project_code = 'GROUNDNUT_PROJECT';

--rollback DELETE FROM tenant.pipeline WHERE pipeline_code = 'GNT_PIPELINE';

--rollback    DELETE
--rollback    FROM tenant.program_team
--rollback    WHERE (program_id IN (SELECT id FROM tenant.program WHERE program_code IN ('GNT')))
--rollback     AND order_number = 1;

--rollback DELETE FROM tenant.team WHERE team_code = 'GNT_TEAM';

--rollback DELETE FROM tenant.program WHERE program_code = 'GNT';

--rollback DELETE FROM tenant.crop_program WHERE crop_program_code = 'GROUNDNUT_PROG';

--rollback DELETE FROM tenant.crop WHERE crop_code = 'GROUNDNUT';