--liquibase formatted sql

--changeset postgres:1_3_populate_experiment_for_wheat context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2442 CB DB - Extract international Wheat nursery data from test environment.


INSERT INTO experiment.experiment
(program_id, pipeline_id, stage_id, project_id, experiment_year, season_id, planting_season, experiment_code, experiment_name, experiment_objective, experiment_type, experiment_sub_type, experiment_sub_sub_type, experiment_design_type, experiment_status, description, steward_id, creator_id, is_void, notes, data_process_id, crop_id, remarks)
VALUES
((select pr.id from tenant.program pr where pr.program_name = 'BW Wheat Breeding Program'), (select p2.id from tenant.pipeline p2 where p2.pipeline_code = 'GWP_PIPELINE'), (select s.id from tenant.stage s where s.stage_code = 'BRE'), (select p3.id from tenant.project p3 where p3.project_code = 'BW_PROJECT'), 2024, (select se.id from tenant.season se where se.season_code = 'B'), NULL, (experiment.generate_code('experiment')), 'BW-CIMMYT-BRE-2024-B-002', 'To test creating an experiment with large number of occurrences. ', 'Observation', NULL, NULL, 'Entry Order', 'created', NULL, (select p.id from tenant.person p where p.username = 'admin'), (select p.id from tenant.person p where p.username = 'admin'), false, NULL, (select i.id from master.item i where i.abbrev = 'OBSERVATION_DATA_PROCESS'), (select c.id from tenant.crop c where c.crop_code = 'WHEAT'), NULL),
((select pr.id from tenant.program pr where pr.program_name = 'BW Wheat Breeding Program'), (select p2.id from tenant.pipeline p2 where p2.pipeline_code = ''), (select s.id from tenant.stage s where s.stage_code = 'AYT'), (select p3.id from tenant.project p3 where p3.project_code = ''), 2023, (select se.id from tenant.season se where se.season_code = 'DS'), NULL, (experiment.generate_code('experiment')), 'BW-CIMMYT-AYT-2023-DS-002', NULL, 'Breeding Trial', NULL, NULL, 'Alpha-Lattice', 'created', NULL, (select p.id from tenant.person p where p.username = 'admin'), (select p.id from tenant.person p where p.username = 'admin'), false, NULL, (select i.id from master.item i where i.abbrev = 'BREEDING_TRIAL_DATA_PROCESS'), (select c.id from tenant.crop c where c.crop_code = 'WHEAT'), NULL);

--rollback DELETE FROM
--rollback experiment.experiment
--rollback WHERE
--rollback experiment_name = 'BW-CIMMYT-BRE-2024-B-002';
--rollback DELETE FROM
--rollback experiment.experiment
--rollback WHERE
--rollback experiment_name = 'BW-CIMMYT-AYT-2023-DS-002';