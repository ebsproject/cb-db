--liquibase formatted sql

--changeset postgres:hm_dcp_004_add_ec_configs_for_chickpea_crop context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-4441 CB-HM DB: Add EC configs for Chickpea crop



-- Insert PLOT_TYPE_CHICKPEA_UNKNOWN config
INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'PLOT_TYPE_CHICKPEA_UNKNOWN',
        'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_CHICKPEA_UNKNOWN',
        $$			
            {
                "Name": "Required experiment level protocol plot type variables",
                "Values": [
                    {
                        "default": false,
                        "disabled": false,
                        "required": "required",
                        "variable_abbrev": "ROWS_PER_PLOT_CONT"
                    },
                    {
                        "unit": "m",
                        "default": false,
                        "disabled": false,
                        "required": "required",
                        "variable_abbrev": "DIST_BET_ROWS"
                    },
                    {
                        "default": false,
                        "computed": "computed",
                        "disabled": false,
                        "required": "required",
                        "variable_abbrev": "PLOT_WIDTH"
                    },
                    {
                        "unit": "m",
                        "default": false,
                        "computed": "computed",
                        "disabled": false,
                        "required": "required",
                        "variable_abbrev": "PLOT_LN"
                    },
                    {
                        "unit": "sqm",
                        "default": false,
                        "computed": "computed",
                        "disabled": true,
                        "required": "required",
                        "variable_abbrev": "PLOT_AREA_2"
                    },
                    {
                        "default": false,
                        "disabled": false,
                        "required": "required",
                        "variable_abbrev": "ALLEY_LENGTH"
                    },
                    {
                        "unit": "m",
                        "default": false,
                        "disabled": false,
                        "required": false,
                        "allow_new_val": true,
                        "variable_abbrev": "SEEDING_RATE"
                    },
                    {
                        "default": false,
                        "disabled": false,
                        "variable_abbrev": "PLANTING_INSTRUCTIONS"
                    }
                ]
            }
        $$,
        1,
        'experiment_creation',
        (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),
        'BDS-4441 CB-HM DB: Add EC configs for Chickpea crop'
    )
;


-- Insert CHICKPEA_PLOT_TYPE config
INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'CHICKPEA_PLOT_TYPE',
        'Experiment Protocol Plot types for Chickpea with no selected planting type',
        $$			
            {
            "Name": "Required experiment level protocol plot type variables",
            "Values": [
                {
                    "default": false,
                    "disabled": false,
                    "required": "required",
                    "field_label": "Plot Type",
                    "order_number": 1,
                    "variable_abbrev": "PLOT_TYPE",
                    "field_description": "Plot Type",
                    "plot_type_abbrevs": [
                        "PLOT_TYPE_2R5M",
                        "PLOT_TYPE_MAIZE_UNKNOWN"
                    ]
                }
            ]
        }
        $$,
        1,
        'experiment_creation',
        (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),
        'BDS-4441 CB-HM DB: Add EC configs for Chickpea crop'
    )
;




--rollback DELETE FROM platform.config WHERE abbrev IN ('PLOT_TYPE_CHICKPEA_UNKNOWN', 'CHICKPEA_PLOT_TYPE');