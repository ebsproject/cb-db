--liquibase formatted sql

--changeset postgres:hm_dcp_011_add_fingermillet_crop_and_related_records context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-4441 CB-HM DB: Add new crop and other related records for FINGER MILLET


-- Insert new crop record for FINGER MILLET
INSERT INTO
    tenant.crop
    (crop_code, crop_name, description, creator_id)
VALUES
    ('FINGERMILLET', 'Finger Millet', 'Finger Millet crop', (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new crop program record for FINGER MILLET
INSERT INTO
    tenant.crop_program
    (crop_program_code, crop_program_name, description, organization_id, crop_id, creator_id)
VALUES
    ('FINGERMILLET_PROG', 'Finger Millet Program', 'Finger Millet Program', (SELECT id FROM tenant.organization WHERE organization_code='CIMMYT_DRYLAND_CROPS' LIMIT 1), (SELECT id FROM tenant.crop WHERE crop_code='FINGERMILLET' LIMIT 1), (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new program record for FINGER MILLET
INSERT INTO 
    tenant.program
    (program_code, program_name, program_type, program_status, description, crop_program_id, creator_id)
VALUES 
    ('FMT', 'Finger Millet Breeding Program', 'breeding', 'active', 'Finger Millet Breeding Program', (SELECT id FROM tenant.crop_program WHERE crop_program_code='FINGERMILLET_PROG' LIMIT 1), (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new team record for FINGER MILLET
INSERT INTO 
    tenant.team
    (team_code, team_name, description, creator_id)
VALUES 
    ('FMT_TEAM', 'Finger Millet Breeding Team', 'Finger Millet Breeding Team', (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new team program record for FINGER MILLET
INSERT INTO 
    tenant.program_team
    (program_id, team_id, order_number, creator_id)
VALUES 
    ((SELECT id FROM tenant.program WHERE program_code='FMT' LIMIT 1), (SELECT id FROM tenant.team WHERE team_code='FMT_TEAM' LIMIT 1), 1, (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new pipeline record for FINGER MILLET
INSERT INTO
    tenant.pipeline
    (pipeline_code, pipeline_name, pipeline_status, creator_id)
VALUES
    ('FMT_PIPELINE', 'Finger Millet Pipeline', 'active', (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));    

-- Insert new project record for FINGER MILLET
INSERT INTO
    tenant.project
    (project_code, project_name, project_status, program_id, pipeline_id, leader_id, creator_id, description, notes)
VALUES
    ('FINGERMILLET_PROJECT', 'Finger Millet Project', 'active', (SELECT id FROM tenant.program WHERE program_code='FMT' LIMIT 1), (SELECT id FROM tenant.pipeline WHERE pipeline_code='FMT_PIPELINE' LIMIT 1), (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1), (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1), 'Finger Millet Project', 'Finger Millet Project');

-- Insert new taxonomy record for FINGER MILLET
INSERT INTO 
    germplasm.taxonomy
(taxon_id, taxonomy_name, description, crop_id, creator_id)
VALUES 
    ('41690', 'Eleusine coracana L.', 'Eleusine coracana L. (Finger Millet)', (SELECT id FROM tenant.crop WHERE crop_code='FINGERMILLET'), (SELECT id FROM tenant.person WHERE username='admin'));

--rollback DELETE FROM germplasm.taxonomy WHERE taxon_id = '41690';

--rollback DELETE FROM tenant.project WHERE project_code = 'FINGERMILLET_PROJECT';

--rollback DELETE FROM tenant.pipeline WHERE pipeline_code = 'FMT_PIPELINE';

--rollback    DELETE
--rollback    FROM tenant.program_team
--rollback    WHERE (program_id IN (SELECT id FROM tenant.program WHERE program_code IN ('FMT')))
--rollback     AND order_number = 1;

--rollback DELETE FROM tenant.team WHERE team_code = 'FMT_TEAM';

--rollback DELETE FROM tenant.program WHERE program_code = 'FMT';

--rollback DELETE FROM tenant.crop_program WHERE crop_program_code = 'FINGERMILLET_PROG';

--rollback DELETE FROM tenant.crop WHERE crop_code = 'FINGERMILLET';