--liquibase formatted sql

--changeset postgres:hm_dcp_017_add_pigeonpea_crop_and_related_records context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-4441 CB-HM DB: Add new crop and other related records for PIGEON PEA


-- Insert new crop record for PIGEON PEA
INSERT INTO
    tenant.crop
    (crop_code, crop_name, description, creator_id)
VALUES
    ('PIGEONPEA', 'Pigeon Pea', 'Pigeon Pea crop', (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new crop program record for PIGEON PEA
INSERT INTO
    tenant.crop_program
    (crop_program_code, crop_program_name, description, organization_id, crop_id, creator_id)
VALUES
    ('PIGEONPEA_PROG', 'Pigeon Pea Program', 'Pigeon Pea Program', (SELECT id FROM tenant.organization WHERE organization_code='CIMMYT_DRYLAND_CROPS' LIMIT 1), (SELECT id FROM tenant.crop WHERE crop_code='PIGEONPEA' LIMIT 1), (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new program record for PIGEON PEA
INSERT INTO 
    tenant.program
    (program_code, program_name, program_type, program_status, description, crop_program_id, creator_id)
VALUES 
    ('PGP', 'Pigeon Pea Breeding Program', 'breeding', 'active', 'Pigeon Pea Breeding Program', (SELECT id FROM tenant.crop_program WHERE crop_program_code='PIGEONPEA_PROG' LIMIT 1), (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new team record for PIGEON PEA
INSERT INTO 
    tenant.team
    (team_code, team_name, description, creator_id)
VALUES 
    ('PGP_TEAM', 'Pigeon Pea Breeding Team', 'Pigeon Pea Breeding Team', (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new team program record for PIGEON PEA
INSERT INTO 
    tenant.program_team
    (program_id, team_id, order_number, creator_id)
VALUES 
    ((SELECT id FROM tenant.program WHERE program_code='PGP' LIMIT 1), (SELECT id FROM tenant.team WHERE team_code='PGP_TEAM' LIMIT 1), 1, (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));

-- Insert new pipeline record for PIGEON PEA
INSERT INTO
    tenant.pipeline
    (pipeline_code, pipeline_name, pipeline_status, creator_id)
VALUES
    ('PGP_PIPELINE', 'Pigeon Pea Pipeline', 'active', (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));    

-- Insert new project record for PIGEON PEA
INSERT INTO
    tenant.project
    (project_code, project_name, project_status, program_id, pipeline_id, leader_id, creator_id, description, notes)
VALUES
    ('PIGEONPEA_PROJECT', 'Pigeon Pea Project', 'active', (SELECT id FROM tenant.program WHERE program_code='PGP' LIMIT 1), (SELECT id FROM tenant.pipeline WHERE pipeline_code='PGP_PIPELINE' LIMIT 1), (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1), (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1), 'Pigeon Pea Project', 'Pigeon Pea Project');

-- Insert new taxonomy record for PIGEON PEA
INSERT INTO 
    germplasm.taxonomy
(taxon_id, taxonomy_name, description, crop_id, creator_id)
VALUES 
    ('41710', 'Cajanus cajan L.', 'Cajanus cajan L. (Pigeon Pea)', (SELECT id FROM tenant.crop WHERE crop_code='PIGEONPEA'), (SELECT id FROM tenant.person WHERE username='admin'));

--rollback DELETE FROM germplasm.taxonomy WHERE taxon_id = '41710';

--rollback DELETE FROM tenant.project WHERE project_code = 'PIGEONPEA_PROJECT';

--rollback DELETE FROM tenant.pipeline WHERE pipeline_code = 'PGP_PIPELINE';

--rollback    DELETE
--rollback    FROM tenant.program_team
--rollback    WHERE (program_id IN (SELECT id FROM tenant.program WHERE program_code IN ('PGP')))
--rollback     AND order_number = 1;

--rollback DELETE FROM tenant.team WHERE team_code = 'PGP_TEAM';

--rollback DELETE FROM tenant.program WHERE program_code = 'PGP';

--rollback DELETE FROM tenant.crop_program WHERE crop_program_code = 'PIGEONPEA_PROG';

--rollback DELETE FROM tenant.crop WHERE crop_code = 'PIGEONPEA';