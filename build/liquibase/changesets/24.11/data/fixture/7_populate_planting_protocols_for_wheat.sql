--liquibase formatted sql

--changeset postgres:7_populate_planting_protocols_for_wheat context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2442 CB DB - Extract international Wheat nursery data from test environment.



INSERT INTO  tenant.protocol 
	(protocol_code,protocol_name,protocol_type,description,program_id,creator_id,notes)
SELECT
	    'PLANTING_PROTOCOL_'||ex.experiment_code AS protocol_code,
	    'Planting Protocol Exp'||substring(ex.experiment_code, 4) AS protocol_name,
	    'planting' AS protocol_type,
	    null AS description,
	    ex.program_id,
	    (SELECT id FROM tenant.person WHERE username ='admin') AS creator_id,
		'Inserted via liquibase changeset (BDS-2442)' notes
	FROM
	   	experiment.experiment ex
	   	WHERE
		ex.experiment_name in ('BW-CIMMYT-BRE-2024-B-002','BW-CIMMYT-AYT-2023-DS-002');



--rollback DELETE FROM
--rollback     tenant.protocol
--rollback WHERE
--rollback 		protocol_code ILIKE 'PLANTING_PROTOCOL_%'
--rollback AND
--rollback 		notes = 'Inserted via liquibase changeset (BDS-2442)';