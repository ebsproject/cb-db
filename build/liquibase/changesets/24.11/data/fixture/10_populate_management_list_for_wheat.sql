--liquibase formatted sql

--changeset postgres:10_populate_management_list_for_wheat context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2442 CB DB - Extract international Wheat nursery data from test environment.



INSERT INTO platform.list 
    (abbrev, name, display_name, type, entity_id, creator_id, notes, list_sub_type)
SELECT
	'MANAGEMENT_PROTOCOL_' || occur.occurrence_code AS abbrev,													
	occur.occurrence_name|| ' Management Protocol (' || (SELECT experiment_code from experiment.experiment where id = occur.experiment_id) || ')' AS name,
	occur.occurrence_name|| ' Management Protocol (' || (SELECT experiment_code from experiment.experiment where id = occur.experiment_id) || ')' AS display_name,
	'variable' AS type,
	(SELECT id FROM "dictionary".entity WHERE abbrev = 'VARIABLE') AS entity_id,
	(SELECT id FROM tenant.person WHERE username ='admin') AS creator_id,
	'Inserted via liquibase changeset (BDS-2442)' notes,
	'management protocol' AS list_sub_type
FROM 
	experiment.occurrence occur 
WHERE
	occur.experiment_id IN (SELECT id from experiment.experiment where experiment_name in ('BW-CIMMYT-BRE-2024-B-002','BW-CIMMYT-AYT-2023-DS-002'));



--rollback DELETE FROM
--rollback     platform.list
--rollback WHERE
--rollback (name ILIKE 'BW-CIMMYT-BRE-2024-B-002%' or name ILIKE 'BW-CIMMYT-AYT-2023-DS-002%')
--rollback AND
--rollback notes = 'Inserted via liquibase changeset (BDS-2442)'



