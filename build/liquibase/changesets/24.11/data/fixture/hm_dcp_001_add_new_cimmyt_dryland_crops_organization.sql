--liquibase formatted sql

--changeset postgres:hm_dcp_001_add_new_cimmyt_dryland_crops_organization context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-4441 CB-HM DB: Add new organization for CIMMYT Dryland Crops


-- Insert new organization for CIMMYT Dryland Crops
INSERT INTO
    tenant.organization
    (organization_code, organization_name, description, creator_id)
VALUES
    ('CIMMYT_DRYLAND_CROPS', 'CIMMYT Dryland Crops', 'CIMMYT Dryland Crops', 1);



--rollback DELETE FROM tenant.organization WHERE organization_code = 'CIMMYT_DRYLAND_CROPS';