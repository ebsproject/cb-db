--liquibase formatted sql

--changeset postgres:update_variables_AYLD_KG_CONT_usage context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Update usage value of variable AYLD_KG_CONT



UPDATE master.variable
SET
 usage = 'occurrence',
 data_level = 'plot'
WHERE abbrev = 'AYLD_KG_CONT'
;



--rollback UPDATE master.variable
--rollback SET
--rollback  usage = 'application',
--rollback  data_level = 'occurrence'
--rollback WHERE abbrev = 'AYLD_KG_CONT'
--rollback ;