--liquibase formatted sql

--changeset postgres:add_GYLD_DSR_KG_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert GYLD_DSR_KG Formula



-- GYLD_DSR_KG
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('GYLD_DSR_KG', 'GYLD_DSR_KG', 'Grain Yield per kg/Ha for DSR with gaps', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Grain Yield per kg/Ha for DSR with gaps', 'active', '1', 'Grain Yield per kg/Ha for DSR with gaps','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'GYLD_DSR_KG' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('GYLD_DSR_KG', 'Grain Yield per kg/Ha for DSR with gaps', 'Grain Yield per kg/Ha for DSR with gaps') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('GYLD_DSR_KG_METHOD', 'Grain Yield per kg/Ha for DSR with gaps method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('GYLD_DSR_KG_SCALE', 'Grain Yield per kg/Ha for DSR with gaps scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('GYLD_DSR_KG=(ADJAYLD_G_CONT/HV_AREA_DSR_SQM)*10', (SELECT id FROM master.variable WHERE abbrev = 'GYLD_DSR_KG'), (SELECT id FROM master.method WHERE abbrev = 'GYLD_DSR_KG_METHOD'), 'plot', '
    master.formula_gyld_dsr_kg(ayld_g_cont, hv_area_dsr_sqm)', NULL, '
    create or replace function master.formula_gyld_dsr_kg(
				ayld_g_cont float,
                hv_area_dsr_sqm float
			) returns float as
			$body$
			declare
			  gyld_dsr_kg float;
              local_ayld_g_cont float;
              local_hv_area_dsr_sqm float;
			begin
				
gyld_dsr_kg=(ayld_g_cont/hv_area_dsr_sqm)*10;
				
				return round(gyld_dsr_kg::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('ADJAYLD_G_CONT', 'HV_AREA_DSR_SQM') -- must be the same order as defined in the database function
    AND var.abbrev = 'GYLD_DSR_KG'
;

CREATE OR REPLACE FUNCTION master.formula_gyld_dsr_kg(
				ayld_g_cont float,
                hv_area_dsr_sqm float
			) RETURNS float as
			$body$
			DECLARE
			  gyld_dsr_kg float;
              local_ayld_g_cont float;
              local_hv_area_dsr_sqm float;
			BEGIN
				
gyld_dsr_kg=(ayld_g_cont/hv_area_dsr_sqm)*10;
				
				RETURN round(gyld_dsr_kg::numeric, 3);
			END
			$body$
			language plpgsql;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('GYLD_DSR_KG');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'GYLD_DSR_KG=(ADJAYLD_G_CONT/HV_AREA_DSR_SQM)*10');
--rollback DELETE FROM master.formula WHERE formula IN ('GYLD_DSR_KG=(ADJAYLD_G_CONT/HV_AREA_DSR_SQM)*10');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('GYLD_DSR_KG_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('GYLD_DSR_KG');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('AGYLD_DSR_KG_SCALE');
--rollback DROP FUNCTION master.formula_gyld_dsr_kg;



--changeset postgres:add_HV_AREA_DSR_SQM_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert HV_AREA_DSR_SQM Formula



-- HV_AREA_DSR_SQM
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('HV_AREA_DSR_SQM', 'HV_AREA_DSR_SQM', 'HV_AREA_DSR_SQM', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'HV_AREA_DSR_SQM', 'active', '1', 'HV_AREA_DSR_SQM','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'HV_AREA_DSR_SQM' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('HV_AREA_DSR_SQM', 'HV_AREA_DSR_SQM', 'HV_AREA_DSR_SQM') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('HV_AREA_DSR_SQM_METHOD', 'HV_AREA_DSR_SQM')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('HV_AREA_DSR_SQM_SCALE', 'HV_AREA_DSR_SQM', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('HV_AREA_DSR_SQM = PLOT_AREA_DSR_SQM - (BIOMASS_AREA_SQM + TOT_PLTGAP_AREA_SQM)', (SELECT id FROM master.variable WHERE abbrev = 'HV_AREA_DSR_SQM'), (SELECT id FROM master.method WHERE abbrev = 'HV_AREA_DSR_SQM_METHOD'), 'plot', '
    master.formula_hv_area_dsr_sqm(plot_area_dsr_sqm, biomass_area_sqm,tot_pltgap_area_sqm)', NULL, '
    create or replace function master.formula_hv_area_dsr_sqm(
				plot_area_dsr_sqm float,
                biomass_area_sqm float,
                tot_pltgap_area_sqm float
			) returns float as
			$body$
			declare
			  hv_area_dsr_sqm float;
              local_plot_area_dsr_sqm float;
              local_biomass_area_sqm float;
              local_tot_pltgap_area_sqm float
			begin
				
hv_area_dsr_sqm=plot_area_dsr_sqm - (biomass_area_sqm + tot_pltgap_area_sqm);
				
				return round(hv_area_dsr_sqm::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('PLOT_AREA_DSR_SQM','BIOMASS_AREA_SQM','TOT_PLTGAP_AREA_SQM') -- must be the same order as defined in the database function
    AND var.abbrev = 'HV_AREA_DSR_SQM'
;

create or replace function master.formula_hv_area_dsr_sqm(
				plot_area_dsr_sqm float,
                biomass_area_sqm float,
                tot_pltgap_area_sqm float
			) returns float as
			$body$
			declare
			  hv_area_dsr_sqm float;
              local_plot_area_dsr_sqm float;
              local_biomass_area_sqm float;
              local_tot_pltgap_area_sqm float;
			begin
				
hv_area_dsr_sqm=plot_area_dsr_sqm - (biomass_area_sqm + tot_pltgap_area_sqm);
				
				return round(hv_area_dsr_sqm::numeric, 3);
			end
			$body$
			language plpgsql;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('HV_AREA_DSR_SQM');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'GYLD_DSR_KG=(ADJAYLD_G_CONT/HV_AREA_DSR_SQM)*10');
--rollback DELETE FROM master.formula WHERE formula IN ('HV_AREA_DSR_SQM = PLOT_AREA_DSR_SQM - (BIOMASS_AREA_SQM + TOT_PLTGAP_AREA_SQM)');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('HV_AREA_DSR_SQM_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('HV_AREA_DSR_SQM');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('HV_AREA_DSR_SQM_SCALE');
--rollback DROP FUNCTION master.formula_hv_area_dsr_sqm;



--changeset postgres:add_HVHILL_PCT_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert HVHILL_PCT Formula



-- HVHILL_PCT
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('HVHILL_PCT', 'HVHILL_PCT', 'HVHILL_PCT', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'HVHILL_PCT', 'active', '1', 'HVHILL_PCT','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'HVHILL_PCT' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('HVHILL_PCT', 'HVHILL_PCT', 'HVHILL_PCT') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('HVHILL_PCT_METHOD', 'HVHILL_PCT method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('HVHILL_PCT', 'HVHILL_PCT scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('HVHILL_PCT = (HVHILL_CONT/TOTAL_HILL_CONT::float) * 100', (SELECT id FROM master.variable WHERE abbrev = 'HVHILL_PCT'), (SELECT id FROM master.method WHERE abbrev = 'HVHILL_PCT_METHOD'), 'plot', '
    master.formula_hvhill_pct(hvhill_cont, total_hill_cont)', NULL, '
    create or replace function master.formula_hvhill_pct(
				hvhill_cont float,
                total_hill_cont float
			) returns float as
			$body$
			declare
			  hvhill_pct float;
              local_hvhill_cont float;
              local_total_hill_cont float;
			begin
				
hvhill_pct = (hvhill_cont/total_hill_cont::float) * 100;
				
				return round(hvhill_pct::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('HVHILL_CONT', 'TOTAL_HILL_CONT') -- must be the same order as defined in the database function
    AND var.abbrev = 'HVHILL_PCT'
;

CREATE OR REPLACE FUNCTION master.formula_hvhill_pct(
				hvhill_cont float,
                total_hill_cont float
			) RETURNS float as
			$body$
			DECLARE
			  hvhill_pct float;
              local_hvhill_cont float;
              local_total_hill_cont float;
			BEGIN
				
hvhill_pct = (hvhill_cont/total_hill_cont::float) * 100;
				
				RETURN round(hvhill_pct::numeric, 3);
			END
			$body$
			language plpgsql;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('HVHILL_PCT');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'HVHILL_PCT = (HVHILL_CONT/TOTAL_HILL_CONT::float) * 100');
--rollback DELETE FROM master.formula WHERE formula IN ('HVHILL_PCT = (HVHILL_CONT/TOTAL_HILL_CONT::float) * 100');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('HVHILL_PCT_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('HVHILL_PCT');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('HVHILL_PCT_SCALE');
--rollback DROP FUNCTION master.formula_hvhill_pct;



--changeset postgres:add_PLOT_AREA_DSR_SQM_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert PLOT_AREA_DSR_SQM Formula



-- PLOT_AREA_DSR_SQM
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('PLOT_AREA_DSR_SQM', 'PLOT_AREA_DSR_SQM', 'PLOT_AREA_DSR_SQM', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'PLOT_AREA_DSR_SQM', 'active', '1', 'PLOT_AREA_DSR_SQM','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'PLOT_AREA_DSR_SQM' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('PLOT_AREA_DSR_SQM', 'PLOT_AREA_DSR_SQM', 'PLOT_AREA_DSR_SQM') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('PLOT_AREA_DSR_SQM_METHOD', 'PLOT_AREA_DSR_SQM method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('PLOT_AREA_DSR_SQM_SCALE', 'PLOT_AREA_DSR_SQM scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('PLOT_AREA_DSR_SQM = DIST_BET_ROWS * ROW_LENGTH_CONT * ROWS_PER_PLOT_CONT', (SELECT id FROM master.variable WHERE abbrev = 'PLOT_AREA_DSR_SQM'), (SELECT id FROM master.method WHERE abbrev = 'PLOT_AREA_DSR_SQM_METHOD'), 'plot', '
    master.formula_plot_area_dsr_sqm(dist_bet_rows, row_length_cont, rows_per_plot_cont)', NULL, '
    create or replace function master.formula_plot_area_dsr_sqm(
                dist_bet_rows float,
                row_length_cont  float,
                rows_per_plot_cont float
			) returns float as
			$body$
			declare
			  plot_area_dsr_sqm float;
              local_dist_bet_rows float;
              local_length_cont float;
              local_rows_per_plot_cont float;
			begin
				
plot_area_dsr_sqm = dist_bet_rows * row_length_cont * rows_per_plot_cont;
				
				return round(plot_area_dsr_sqm::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('DIST_BET_ROWS', 'ROW_LENGTH_CONT', 'ROWS_PER_PLOT_CONT') -- must be the same order as defined in the database function
    AND var.abbrev = 'PLOT_AREA_DSR_SQM'
;

CREATE OR REPLACE FUNCTION master.formula_plot_area_dsr_sqm(
				plot_area_dsr_sqm float,
                dist_bet_rows float,
                row_length_cont  float,
                rows_per_plot_cont float
			) RETURNS float as
			$body$
			DECLARE
			  plot_area_dsr_sqm float;
              local_dist_bet_rows float;
              local_length_cont float;
              local_rows_per_plot_cont float;
			BEGIN
				
plot_area_dsr_sqm = dist_bet_rows * row_length_cont * rows_per_plot_cont;
				
				RETURN round(plot_area_dsr_sqm::numeric, 3);
			END
			$body$
			language plpgsql;





--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('PLOT_AREA_DSR_SQM');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'PLOT_AREA_DSR_SQM = DIST_BET_ROWS * ROW_LENGTH_CONT * ROWS_PER_PLOT_CONT');
--rollback DELETE FROM master.formula WHERE formula IN ('PLOT_AREA_DSR_SQM = DIST_BET_ROWS * ROW_LENGTH_CONT * ROWS_PER_PLOT_CONT');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('PLOT_AREA_DSR_SQM_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('PLOT_AREA_DSR_SQM');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('PLOT_AREA_DSR_SQM_SCALE');
--rollback DROP FUNCTION master.formula_plot_area_dsr_sqm;



--changeset postgres:add_SUB_SURVIVAL_PCT_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert SUB_SURVIVAL_PCT Formula            



-- SUB_SURVIVAL_PCT
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('SUB_SURVIVAL_PCT', 'PercentSub_Survival_Fld', 'Percent plant survival (S)', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Percent plant survival (S)', 'active', '1', 'Percent plant survival (S)','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'SUB_SURVIVAL_PCT' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('SUB_SURVIVAL_PCT', 'PercentSub_Survival_Fld', 'Percent plant survival (S)') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('SUB_SURVIVAL_PCT_METHOD', 'Percent plant survival (S) method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('SUB_SURVIVAL_PCT_SCALE', 'Percent plant survival (S) scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('SUB_SURVIVAL_PCT = SUB_21DAD/TOTAL_HILL_CONT::float * 100', (SELECT id FROM master.variable WHERE abbrev = 'SUB_SURVIVAL_PCT'), (SELECT id FROM master.method WHERE abbrev = 'SUB_SURVIVAL_PCT_METHOD'), 'plot', '
    master.formula_sub_survival_pct(sub_21dad, total_hill_cont)', NULL, '
    create or replace function master.formula_sub_survival_pct(
                sub_21dad float,
                total_hill_cont float
			) returns float as
			$body$
			declare
			  sub_survival_pct float;
              local_sub_21dad float;
              local_total_hill_cont float;
			begin
				
sub_survival_pct = sub_21dad/total_hill_cont::float * 100;
				
				return round(sub_survival_pct::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('SUB_21DAD', 'TOTAL_HILL_CONT') -- must be the same order as defined in the database function
    AND var.abbrev = 'SUB_SURVIVAL_PCT'
;

CREATE OR REPLACE FUNCTION master.formula_sub_survival_pct(
                sub_21dad float,
                total_hill_cont float
			) RETURNS float as
			$body$
			DECLARE
			  sub_survival_pct float;
              local_sub_21dad float;
              local_total_hill_cont float;
			BEGIN
				
sub_survival_pct = sub_21dad/total_hill_cont::float * 100;
				
				RETURN round(sub_survival_pct::numeric, 3);
			END
			$body$
			language plpgsql;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('SUB_SURVIVAL_PCT');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'SUB_SURVIVAL_PCT = SUB_21DAD/TOTAL_HILL_CONT::float * 100');
--rollback DELETE FROM master.formula WHERE formula IN ('SUB_SURVIVAL_PCT = SUB_21DAD/TOTAL_HILL_CONT::float * 100');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('SUB_SURVIVAL_PCT_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('SUB_SURVIVAL_PCT');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('SUB_SURVIVAL_PCT_SCALE');
--rollback DROP FUNCTION master.formula_sub_survival_pct;



--changeset postgres:add_TOT_PLTGAP_AREA_SQM_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert TOT_PLTGAP_AREA_SQM Formula  



-- TOT_PLTGAP_AREA_SQM
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('TOT_PLTGAP_AREA_SQM', 'TOT_PLTGAP_AREA_SQM', 'TOT_PLTGAP_AREA_SQM', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'TOT_PLTGAP_AREA_SQM', 'active', '1', 'TOT_PLTGAP_AREA_SQM','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'TOT_PLTGAP_AREA_SQM' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('TOT_PLTGAP_AREA_SQM', 'TOT_PLTGAP_AREA_SQM', 'TOT_PLTGAP_AREA_SQM') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('TOT_PLTGAP_AREA_SQM_METHOD', 'TOT_PLTGAP_AREA_SQM method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('TOT_PLTGAP_AREA_SQM_SCALE', 'TOT_PLTGAP_AREA_SQM scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('TOT_PLTGAP_AREA_SQM = (TOT_PLT_GAP/100) * (DIST_BET_ROWS/100)', (SELECT id FROM master.variable WHERE abbrev = 'TOT_PLTGAP_AREA_SQM'), (SELECT id FROM master.method WHERE abbrev = 'TOT_PLTGAP_AREA_SQM_METHOD'), 'plot', '
    master.formula_tot_pltgap_area_sqm(ayld_g_cont, hv_area_dsr_sqm)', NULL, '
    create or replace function master.formula_tot_pltgap_area_sqm(
				tot_plt_gap float,
                dist_bet_rows float
			) returns float as
			$body$
			declare
			  tot_pltgap_area_sqm float;
              local_tot_plt_gap float;
              local_dist_bet_rowsm float;
			begin
				
tot_pltgap_area_sqm = (tot_plt_gap/100) * (dist_bet_rows/100);
				
				return round(tot_pltgap_area_sqm::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('TOT_PLT_GAP', 'DIST_BET_ROWS') -- must be the same order as defined in the database function
    AND var.abbrev = 'TOT_PLTGAP_AREA_SQM'
;

CREATE OR REPLACE FUNCTION master.formula_tot_pltgap_area_sqm(
				tot_plt_gap float,
                dist_bet_rows float
			) RETURNS float as
			$body$
			DECLARE
			  tot_pltgap_area_sqm float;
              local_tot_plt_gap float;
              local_dist_bet_rowsm float;
			BEGIN
				
tot_pltgap_area_sqm = (tot_plt_gap/100) * (dist_bet_rows/100);
				
				RETURN round(tot_pltgap_area_sqm::numeric, 3);
			END
			$body$
			language plpgsql;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('TOT_PLTGAP_AREA_SQM');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'TOT_PLTGAP_AREA_SQM = (TOT_PLT_GAP/100) * (DIST_BET_ROWS/100)');
--rollback DELETE FROM master.formula WHERE formula IN ('TOT_PLTGAP_AREA_SQM = (TOT_PLT_GAP/100) * (DIST_BET_ROWS/100)');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('TOT_PLTGAP_AREA_SQM_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('TOT_PLTGAP_AREA_SQM');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('TOT_PLTGAP_AREA_SQM_SCALE');
--rollback DROP FUNCTION master.formula_tot_pltgap_area_sqm;



--changeset postgres:add_YLD_CONT4_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert YLD_CONT4 Formula  



-- YLD_CONT4
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('YLD_CONT4', 'YLD_CONT4', 'YLD_CONT4', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'YLD_CONT4', 'active', '1', 'YLD_CONT4','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'YLD_CONT4' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('YLD_CONT4', 'YLD_CONT4', 'YLD_CONT4') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('YLD_CONT4_METHOD', 'YLD_CONT4 method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('YLD_CONT4_SCALE', 'YLD_CONT4 scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('YLD_CONT4 = AYLD_CONT * MF_CONT * 10/ADJHVAREA_CONT2', (SELECT id FROM master.variable WHERE abbrev = 'YLD_CONT4'), (SELECT id FROM master.method WHERE abbrev = 'YLD_CONT4_METHOD'), 'plot', '
    master.formula_yld_cont4(ayld_cont, mf_cont, adjhvarea_cont2)', NULL, '
    create or replace function master.formula_yld_cont4(
				ayld_cont float,
                mf_cont float,
                adjhvarea_cont2 float
			) returns float as
			$body$
			declare
			  YLD_CONT4 float;
              local_ayld_g_cont float;
              local_hv_area_dsr_sqm float;
			begin
				
YLD_CONT4=(ayld_g_cont/hv_area_dsr_sqm)*10;
				
				return round(YLD_CONT4::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('AYLD_CONT', 'MF_CONT','ADJHVAREA_CONT2') -- must be the same order as defined in the database function
    AND var.abbrev = 'YLD_CONT4'
;

CREATE OR REPLACE FUNCTION master.formula_yld_cont4(
				ayld_g_cont float,
                hv_area_dsr_sqm float
			) RETURNS float as
			$body$
			DECLARE
			  YLD_CONT4 float;
              local_ayld_g_cont float;
              local_hv_area_dsr_sqm float;
			BEGIN
				
YLD_CONT4=(ayld_g_cont/hv_area_dsr_sqm)*10;
				
				RETURN round(YLD_CONT4::numeric, 3);
			END
			$body$
			language plpgsql;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('YLD_CONT4');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'YLD_CONT4 = AYLD_CONT * MF_CONT * 10/ADJHVAREA_CONT2');
--rollback DELETE FROM master.formula WHERE formula IN ('YLD_CONT4 = AYLD_CONT * MF_CONT * 10/ADJHVAREA_CONT2');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('YLD_CONT4_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('YLD_CONT4');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('YLD_CONT4_SCALE');
--rollback DROP FUNCTION master.formula_yld_cont4;



--changeset postgres:add_YLD_DSR_TON_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert YLD_DSR_TON Formula  



-- YLD_DSR_TON
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('YLD_DSR_TON', 'GYLD_DSR_TON', 'Grain Yield in ton/ha for DSR with gaps', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Grain Yield in ton/ha for DSR with gaps', 'active', '1', 'Grain Yield in ton/ha for DSR with gaps','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'YLD_DSR_TON' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('YLD_DSR_TON', 'GYLD_DSR_TON', 'Grain Yield in ton/ha for DSR with gaps') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('YLD_DSR_TON_METHOD', 'Grain Yield in ton/ha for DSR with gaps method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('YLD_DSR_TON_SCALE', 'Grain Yield in ton/ha for DSR with gaps scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('YLD_DSR_TON = (ADJAYLD_G_CONT/(((DIST_BET_ROWS/100) * ROW_LENGTH_CONT * ROWS_PER_PLOT_CONT) -(BIOMASS_AREA_SQM + TOT_PLTGAP_AREA_SQM))) / 100', (SELECT id FROM master.variable WHERE abbrev = 'YLD_DSR_TON'), (SELECT id FROM master.method WHERE abbrev = 'YLD_DSR_TON_METHOD'), 'plot', '
    master.formula_yld_dsr_ton(adjayld_g_cont, dist_bet_rows, row_length_cont, rows_per_plot_cont, biomass_area_sqm, tot_pltgap_area_sqm)', null, '
    create or replace function master.formula_YLD_DSR_TON(
				adjayld_g_cont float,
                dist_bet_rows float,
                row_length_cont float,
                rows_per_plot_cont float,
                biomass_area_sqm float,
                tot_pltgap_area_sqm float
			) returns float as
			$body$
			declare
			  yld_dsr_ton float;
              local_adjayld_g_cont float;
              local_dist_bet_rows float;
              local_row_length_cont float;
              local_rows_per_plot_cont float;
              local_biomass_area_sqm float;
              local_tot_pltgap_area_sqm float;
			begin
				
yld_dsr_ton = (adjayld_g_cont/(((dist_bet_rows/100) * row_length_cont * rows_per_plot_cont) -(biomass_area_sqm + tot_pltgap_area_sqm))) / 100;
				
				return round(yld_dsr_ton::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('ADJAYLD_G_CONT', 'DIST_BET_ROWS', 'ROW_LENGTH_CONT', 'ROWS_PER_PLOT_CONT', 'BIOMASS_AREA_SQM', 'TOT_PLTGAP_AREA_SQM') -- must be the same order as defined in the database function
    AND var.abbrev = 'YLD_DSR_TON'
;

CREATE OR REPLACE FUNCTION master.formula_yld_dsr_ton(
				adjayld_g_cont float,
                dist_bet_rows float,
                row_length_cont float,
                rows_per_plot_cont float,
                biomass_area_sqm float,
                tot_pltgap_area_sqm float
			) RETURNS float as
			$body$
			DECLARE
			  yld_dsr_ton float;
              local_adjayld_g_cont float;
              local_dist_bet_rows float;
              local_row_length_cont float;
              local_rows_per_plot_cont float;
              local_biomass_area_sqm float;
              local_tot_pltgap_area_sqm float;
			BEGIN
				
yld_dsr_ton = (adjayld_g_cont/(((dist_bet_rows/100) * row_length_cont * rows_per_plot_cont) -(biomass_area_sqm + tot_pltgap_area_sqm))) / 100;
				
				RETURN round(yld_dsr_ton::numeric, 3);
			END
			$body$
			language plpgsql;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('YLD_DSR_TON');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'YLD_DSR_TON = (ADJAYLD_G_CONT/(((DIST_BET_ROWS/100) * ROW_LENGTH_CONT * ROWS_PER_PLOT_CONT) -(BIOMASS_AREA_SQM + TOT_PLTGAP_AREA_SQM))) / 100');
--rollback DELETE FROM master.formula WHERE formula IN ('YLD_DSR_TON = (ADJAYLD_G_CONT/(((DIST_BET_ROWS/100) * ROW_LENGTH_CONT * ROWS_PER_PLOT_CONT) -(BIOMASS_AREA_SQM + TOT_PLTGAP_AREA_SQM))) / 100');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('YLD_DSR_TON_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('YLD_DSR_TON');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('YLD_DSR_TON_SCALE');
--rollback DROP FUNCTION master.formula_yld_dsr_ton;



--changeset postgres:add_GW_CMP_KG_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert GW_CMP_KG Formula



-- GW_CMP_KG
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('GW_CMP_KG', 'GW_CMP_KG', 'Grain weight', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Total weight of shelled grain computed from the weight all of the ears harvested in a plot multiplied by the shelling percentage and reported in kilograms; the shelling percentage can be an estimate based on a constant value or it can be based by calculating a specific shelling percentage per plot', 'active', '1', 'Grain weight','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'GW_CMP_KG' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('GW_CMP_KG', 'Grain weight', 'Grain Yield per kg/Ha for DSR with gaps') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('GW_CMP_KG_METHOD', 'Grain weight method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('GW_CMP_KG_SCALE', 'Grain weight scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('GW_CMP_KG=EW_M_KG*0.8', (SELECT id FROM master.variable WHERE abbrev = 'GW_CMP_KG'), (SELECT id FROM master.method WHERE abbrev = 'GW_CMP_KG_METHOD'), 'plot', '
    master.formula_gw_cmp_kg(ew_m_kg)', NULL, '
    create or replace function master.formula_gw_cmp_kg(
				ew_m_kg float
			) returns float as
			$body$
			declare
			  gw_cmp_kg float;
              local_ew_m_kg float;
			begin
				
gw_cmp_kg=(ayld_g_cont/hv_area_dsr_sqm)*10;
				
				return round(gw_cmp_kg::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('EW_M_KG') -- must be the same order as defined in the database function
    AND var.abbrev = 'GW_CMP_KG'
;

CREATE OR REPLACE FUNCTION master.formula_gw_cmp_kg(
				ew_m_kg float
			) RETURNS float as
			$body$
			DECLARE
			  gw_cmp_kg float;
              local_ew_m_kg float;
			BEGIN
				
gw_cmp_kg=(ayld_g_cont/hv_area_dsr_sqm)*10;
				
				RETURN round(gw_cmp_kg::numeric, 3);
			END
			$body$
			language plpgsql;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('GW_CMP_KG');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'GW_CMP_KG=EW_M_KG*0.8');
--rollback DELETE FROM master.formula WHERE formula IN ('GW_CMP_KG=EW_M_KG*0.8');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('GW_CMP_KG_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('GW_CMP_KG');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('GW_CMP_KG_SCALE');
--rollback DROP FUNCTION master.formula_gw_cmp_kg;            



--changeset postgres:add_GY_CMP_THA_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert GY_CMP_THA Formula



-- GY_CMP_THA
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('GY_CMP_THA', 'GY_CMP_THA', 'Grain yield', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Grain yield calculated in tons/hectare; this could be derived from a measured or computed grain weight and the harvested plot area', 'active', '1', 'Grain yield','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'GY_CMP_THA' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('GY_CMP_THA', 'Grain yield', 'Grain yield') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('GY_CMP_THA_METHOD', 'Grain yield')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('GY_CMP_THA_SCALE', 'Grain yield', NULL, NULL, 0, 30)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('GY_CMP_THA = (GW_CMP_KG)*(10/PLOT_AREA_HARVESTED)', (SELECT id FROM master.variable WHERE abbrev = 'GY_CMP_THA'), (SELECT id FROM master.method WHERE abbrev = 'GY_CMP_THA_METHOD'), 'plot', '
    master.formula_gy_cmp_tha(gw_m_kg, plot_area_harvested)', NULL, '
    create or replace function master.formula_gy_cmp_tha(
				gw_m_kg float,
                plot_area_harvested float
			) returns float as
			$body$
			declare
			  gy_cmp_tha float;
              local_gw_m_kg float;
              local_plot_area_harvested float;
			begin
				
gy_cmp_tha = (gw_cmp_kg)*(10/plot_area_harvested);
				
				return round(gy_cmp_tha::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('GW_CMP_KG', 'PLOT_AREA_HARVESTED') -- must be the same order as defined in the database function
    AND var.abbrev = 'GY_CMP_THA'
;

CREATE OR REPLACE FUNCTION master.formula_gy_cmp_tha(
				gw_m_kg float,
                plot_area_harvested float
			) RETURNS float as
			$body$
			DECLARE
			  gy_cmp_tha float;
              local_gw_m_kg float;
              local_plot_area_harvested float;
			BEGIN
				
gy_cmp_tha = (gw_cmp_kg)*(10/plot_area_harvested);
				
				RETURN round(gy_cmp_tha::numeric, 3);
			END
			$body$
			language plpgsql;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('GY_CMP_THA');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'GY_CMP_THA = (GW_CMP_KG)*(10/PLOT_AREA_HARVESTED)');
--rollback DELETE FROM master.formula WHERE formula IN ('GY_CMP_THA = (GW_CMP_KG)*(10/PLOT_AREA_HARVESTED)');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('GY_CMP_THA_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('GY_CMP_THA');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('GY_CMP_THA_SCALE');
--rollback DROP FUNCTION master.formula_gy_cmp_tha;



--changeset postgres:add_GY_MOI15_CMP_THA_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert GY_MOI15_CMP_THA Formula



-- GY_MOI15_CMP_THA
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('GY_MOI15_CMP_THA', 'GY_MOI15_CMP_THA', 'Grain yield', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Moisture-adjusted (to 15% moisture) grain yield calculated in tons/hectare', 'active', '1', 'Grain Yield','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'GY_MOI15_CMP_THA' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('GY_MOI15_CMP_THA', 'Grain yield', 'Moisture-adjusted (to 15% moisture) grain yield calculated in tons/hectare') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('GY_MOI15_CMP_THA_METHOD', 'Grain yield method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('GY_MOI15_CMP_THA_SCALE', 'Grain yield scale', NULL, NULL, 0, 30)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('GY_MOI15_CMP_THA=GY_CMP_THA*(100-GMOI_M_PCT)/(100-15)', (SELECT id FROM master.variable WHERE abbrev = 'GY_MOI15_CMP_THA'), (SELECT id FROM master.method WHERE abbrev = 'GY_MOI15_CMP_THA_METHOD'), 'plot', '
    master.formula_gy_moi15_cmp_tha(gy_cmp_tha, gmoi_m_pct)', NULL, '
    create or replace function master.formula_gy_moi15_cmp_tha(
				gy_cmp_tha float,
                gmoi_m_pct float
			) returns float as
			$body$
			declare
			  gy_moi15_cmp_tha float;
              local_gy_cmp_tha float;
              local_gmoi_m_pct float;
			begin
				
gy_moi15_cmp_tha=gy_cmp_tha*(100-gmoi_m_pct)/(100-15);
				
				return round(gy_moi15_cmp_tha::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('GY_CMP_THA', 'GMOI_M_PCT') -- must be the same order as defined in the database function
    AND var.abbrev = 'GY_MOI15_CMP_THA'
;

CREATE OR REPLACE FUNCTION master.formula_gy_moi15_cmp_tha(
				gy_cmp_tha float,
                gmoi_m_pct float
			) RETURNS float as
			$body$
			DECLARE
			  gy_moi15_cmp_tha float;
              local_gy_cmp_tha float;
              local_gmoi_m_pct float;
			BEGIN
				
gy_moi15_cmp_tha=gy_cmp_tha*(100-gmoi_m_pct)/(100-15);
				
				RETURN round(gy_moi15_cmp_tha::numeric, 3);
			end
			$body$
			language plpgsql;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('GY_MOI15_CMP_THA');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'GY_MOI15_CMP_THA=GY_CMP_THA*(100-GMOI_M_PCT)/(100-15)');
--rollback DELETE FROM master.formula WHERE formula IN ('GY_MOI15_CMP_THA=GY_CMP_THA*(100-GMOI_M_PCT)/(100-15)');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('GY_MOI15_CMP_THA_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('GY_MOI15_CMP_THA');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('GY_MOI15_CMP_THA_SCALE');
--rollback DROP FUNCTION master.formula_gy_moi15_cmp_tha;



--changeset postgres:add_GY_MOI13_5_CMP_THA_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert GY_MOI13_5_CMP_THA Formula



-- GY_MOI13_5_CMP_THA
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('GY_MOI13_5_CMP_THA', 'GY_MOI13_5_CMP_THA', 'Grain yield', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Moisture-adjusted (to 13.5% moisture) grain yield calculated in tons/hectare', 'active', '1', 'Moisture-adjusted (to 13.5% moisture) grain yield calculated in tons/hectare','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'GY_MOI13_5_CMP_THA' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('GY_MOI13_5_CMP_THA', 'Grain yield', 'Moisture-adjusted (to 13.5% moisture) grain yield calculated in tons/hectare') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('GY_MOI13_5_CMP_THA_METHOD', 'Grain yield method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('GY_MOI13_5_CMP_THA_SCALE', 'Grain yield scale', NULL, NULL, 0, 30)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('GY_MOI13_5_CMP_THA=GY_CMP_THA*(100-GMOI_M_PCT)/(100-13.5)', (SELECT id FROM master.variable WHERE abbrev = 'GY_MOI13_5_CMP_THA'), (SELECT id FROM master.method WHERE abbrev = 'GY_MOI13_5_CMP_THA_METHOD'), 'plot', '
    master.formula_gy_moi13_5_cmp_tha(gy_cmp_tha, gmoi_m_pct)', NULL, '
    create or replace function master.formula_gy_moi13_5_cmp_tha(
				gy_cmp_tha float,
                gmoi_m_pct float
			) returns float as
			$body$
			declare
			  gy_moi13_5_cmp_tha float;
              local_gy_cmp_tha float;
              local_gmoi_m_pct float;
			begin
				
gy_moi13_5_cmp_tha=gy_cmp_tha*(100-gmoi_m_pct)/(100-13.5);
				
				return round(gy_moi13_5_cmp_tha::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('GY_CMP_THA', 'GMOI_M_PCT') -- must be the same order as defined in the database function
    AND var.abbrev = 'GY_MOI13_5_CMP_THA'
;

CREATE OR REPLACE FUNCTION master.formula_gy_moi13_5_cmp_tha(
				gy_cmp_tha float,
                gmoi_m_pct float
			) RETURNS float as
			$body$
			DECLARE
			  gy_moi13_5_cmp_tha float;
              local_gy_cmp_tha float;
              local_gmoi_m_pct float;
			BEGIN
				
gy_moi13_5_cmp_tha=gy_cmp_tha*(100-gmoi_m_pct)/(100-13.5);
				
				RETURN round(gy_moi13_5_cmp_tha::numeric, 3);
			END
			$body$
			language plpgsql;   



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('GY_MOI13_5_CMP_THA');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'GY_MOI13_5_CMP_THA=GY_CMP_THA*(100-GMOI_M_PCT)/(100-13.5)');
--rollback DELETE FROM master.formula WHERE formula IN ('GY_MOI13_5_CMP_THA=GY_CMP_THA*(100-GMOI_M_PCT)/(100-13.5)');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('GY_MOI13_5_CMP_THA_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('GY_MOI13_5_CMP_THA');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('GY_MOI13_5_CMP_THA_SCALE');
--rollback DROP FUNCTION master.formula_gy_moi13_5_cmp_tha;            



--changeset postgres:add_GW_M_KG_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert GW_M_KG Formula



-- GW_M_KG
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('GW_M_KG', 'GW_M_KG', 'Grain weight', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Total weight of shelled grain from all of the ears harvested in a plot measured in kilograms', 'active', '1', 'Grain weight','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'GW_M_KG' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('GW_M_KG', 'Grain weight', 'Grain Yield per kg/Ha for DSR with gaps') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('GW_M_KG_METHOD', 'Grain weight method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('GW_M_KG_SCALE', 'Grain weight scale', NULL, NULL, 0, 30)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('GW_M_KG=GW_M_G/1000', (SELECT id FROM master.variable WHERE abbrev = 'GW_M_KG'), (SELECT id FROM master.method WHERE abbrev = 'GW_M_KG_METHOD'), 'plot', '
    master.formula_gw_m_kg(gw_m_g)', NULL, '
    create or replace function master.formula_gw_m_kg(
				gw_m_g float
			) returns float as
			$body$
			declare
			  gw_m_kg float;
              local_gw_m_g float;
			begin
				
gw_m_kg=gw_m_g/1000;
				
				return round(gw_m_kg::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('GW_M_G') -- must be the same order as defined in the database function
    AND var.abbrev = 'GW_M_KG'
;

CREATE OR REPLACE FUNCTION master.formula_gw_m_kg(
				gw_m_g float
			) RETURNS float as
			$body$
			DECLARE
			  gw_m_kg float;
              local_gw_m_g float;
			BEGIN
				
gw_m_kg=gw_m_g/1000;
				
				RETURN round(gw_m_kg::numeric, 3);
			END
			$body$
			language plpgsql;


--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('GW_M_KG');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'GW_M_KG=GW_M_G/1000');
--rollback DELETE FROM master.formula WHERE formula IN ('GW_M_KG=GW_M_G/1000');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('GW_M_KG_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('GW_M_KG');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('GW_M_KG_SCALE');
--rollback DROP FUNCTION master.formula_gw_m_kg;



--changeset postgres:add_GW_SUB_M_KG_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert GW_SUB_M_KG Formula



-- GW_SUB_M_KG
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('GW_SUB_M_KG', 'GW_SUB_M_KG', 'Grain weight subsample', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Total weight of shelled grain from a subset of ears harvested from a plot measured in kilograms; used to calculate a shelling percentage per plot', 'active', '1', 'Grain weight subsample','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'GW_SUB_M_KG' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('GW_SUB_M_KG', 'Grain weight subsample', 'Grain weight subsample') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('GW_SUB_M_KG_METHOD', 'Grain weight subsample method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('GW_SUB_M_KG_SCALE', 'Grain weight subsample scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('GW_SUB_M_KG = GW_SUB_M_G/1000', (SELECT id FROM master.variable WHERE abbrev = 'GW_SUB_M_KG'), (SELECT id FROM master.method WHERE abbrev = 'GW_SUB_M_KG_METHOD'), 'plot', '
    master.formula_gw_sub_m_kg(gw_sub_m_g)', NULL, '
    create or replace function master.formula_gw_sub_m_kg(
				gw_sub_m_g float
			) returns float as
			$body$
			declare
			  gw_sub_m_kg float;
              local_gw_sub_m_g float;
			begin
				
gw_sub_m_kg = gw_sub_m_g/1000;
				
				return round(gw_sub_m_kg::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('GW_SUB_M_G') -- must be the same order as defined in the database function
    AND var.abbrev = 'GW_SUB_M_KG'
;

CREATE OR REPLACE FUNCTION master.formula_gw_sub_m_kg(
				gw_sub_m_g float
			) RETURNS float as
			$body$
			DECLARE
			  gw_sub_m_kg float;
              local_gw_sub_m_g float;
			BEGIN
				
gw_sub_m_kg = gw_sub_m_g/1000;
				
				RETURN round(gw_sub_m_kg::numeric, 3);
			END
			$body$
			language plpgsql;   



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('GW_SUB_M_KG');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'GW_SUB_M_KG = GW_SUB_M_G/1000');
--rollback DELETE FROM master.formula WHERE formula IN ('GW_SUB_M_KG = GW_SUB_M_G/1000');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('GW_SUB_M_KG_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('GW_SUB_M_KG');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('GW_SUB_M_KG_SCALE');
--rollback DROP FUNCTION master.formula_gw_sub_m_kg;            



--changeset postgres:add_EW_M_KG_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert EW_M_KG Formula              



-- EW_M_KG
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('EW_M_KG', 'EW_M_KG', 'Ear weight', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Total weight of all of the ears harvested in a plot inclusive of grain and cob measured in kilograms', 'active', '1', 'Ear weight','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'EW_M_KG' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('EW_M_KG', 'Ear weight', 'Ear weight') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('EW_M_KG_METHOD', 'Ear weight method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('EW_M_KG_SCALE', 'Ear weight scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('EW_M_KG = EW_M_G/1000', (SELECT id FROM master.variable WHERE abbrev = 'EW_M_KG'), (SELECT id FROM master.method WHERE abbrev = 'EW_M_KG_METHOD'), 'plot', '
    master.formula_ew_m_kg(ew_m_g)', NULL, '
    create or replace function master.formula_ew_m_kg(
				ew_m_g float
			) returns float as
			$body$
			declare
			  ew_m_kg float;
              local_ew_m_g float;
			begin
				
ew_m_kg = ew_m_g/1000;
				
				return round(ew_m_kg::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('EW_M_G') -- must be the same order as defined in the database function
    AND var.abbrev = 'EW_M_KG'
;

CREATE OR REPLACE FUNCTION master.formula_ew_m_kg(
				ew_m_g float
			) RETURNS float as
			$body$
			DECLARE
			  ew_m_kg float;
              local_ew_m_g float;
			BEGIN
				
ew_m_kg = ew_m_g/1000;
				
				RETURN round(ew_m_kg::numeric, 3);
			END
			$body$
			language plpgsql;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('EW_M_KG');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'EW_M_KG = EW_M_G/1000');
--rollback DELETE FROM master.formula WHERE formula IN ('EW_M_KG = EW_M_G/1000');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('EW_M_KG_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('EW_M_KG');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('EW_M_KG_SCALE');
--rollback DROP FUNCTION master.formula_ew_m_kg;            



--changeset postgres:add_EW_SUB_M_KG_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert EW_SUB_M_KG Formula  



-- EW_SUB_M_KG
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('EW_SUB_M_KG', 'EW_SUB_M_KG', 'Ear weight subsample', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Total weight of a subset of ears harvested from a plot measured in grams; used to calculate a shelling percentage per plot in combination with GW_SUB', 'active', '1', 'Ear weight subsample','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'EW_SUB_M_KG' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('EW_SUB_M_KG', 'Ear weight subsample', 'Ear weight subsample') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('EW_SUB_M_KG_METHOD', 'Ear weight subsample method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('EW_SUB_M_KG_SCALE', 'Ear weight subsample scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('EW_SUB_M_KG = EW_SUB_M_G/1000', (SELECT id FROM master.variable WHERE abbrev = 'EW_SUB_M_KG'), (SELECT id FROM master.method WHERE abbrev = 'EW_SUB_M_KG_METHOD'), 'plot', '
    master.formula_ew_sub_m_kg(ew_sub_m_g)', NULL, '
    create or replace function master.formula_ew_sub_m_kg(
				ew_sub_m_g float
			) returns float as
			$body$
			declare
			  ew_sub_m_kg float;
              local_ew_sub_m_g float;
			begin
				
ew_sub_m_kg = ew_sub_m_g/1000;
				
				return round(ew_sub_m_kg::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('EW_SUB_M_G') -- must be the same order as defined in the database function
    AND var.abbrev = 'EW_SUB_M_KG'
;

CREATE OR REPLACE FUNCTION master.formula_ew_sub_m_kg(
				ew_sub_m_g float
			) RETURNS float as
			$body$
			DECLARE
			  ew_sub_m_kg float;
              local_ew_sub_m_g float;
			BEGIN
				
ew_sub_m_kg = ew_sub_m_g/1000;
				
				RETURN round(ew_sub_m_kg::numeric, 3);
			END
			$body$
			language plpgsql;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('EW_SUB_M_KG');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'EW_SUB_M_KG = EW_SUB_M_G/1000');
--rollback DELETE FROM master.formula WHERE formula IN ('EW_SUB_M_KG = EW_SUB_M_G/1000');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('EW_SUB_M_KG_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('EW_SUB_M_KG');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('EW_SUB_M_KG_SCALE');
--rollback DROP FUNCTION master.formula_ew_sub_m_kg;            



--changeset postgres:add_SHELL_CMP_PCT_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert SHELL_CMP_PCT Formula              


-- SHELL_CMP_PCT
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('SHELL_CMP_PCT', 'SHELL_CMP_PCT', 'Shelling percentage', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Shelling percentage', 'active', '1', 'Shelling percentage','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'SHELL_CMP_PCT' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('SHELL_CMP_PCT', 'Shelling percentage', 'Shelling percentage') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('SHELL_CMP_PCT_METHOD', 'Shelling percentage method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('SHELL_CMP_PCT_SCALE', 'Shelling percentage scale', NULL, NULL, 0, 100)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('SHELL_CMP_PCT=(GW_SUB_M_KG/EW_SUB_M_KG)*100', (SELECT id FROM master.variable WHERE abbrev = 'SHELL_CMP_PCT'), (SELECT id FROM master.method WHERE abbrev = 'SHELL_CMP_PCT_METHOD'), 'plot', '
    master.formula_shell_cmp_pct(gw_sub_m_kg, ew_sub_m_kg)', NULL, '
    create or replace function master.formula_shell_cmp_pct(
				gw_sub_m_kg float,
                ew_sub_m_kg float
			) returns float as
			$body$
			declare
			  shell_cmp_pct float;
              local_gw_sub_m_kg float;
              local_ew_sub_m_kg float;
			begin
				
shell_cmp_pct=(gw_sub_m_kg/ew_sub_m_kg)*100;
				
				return round(shell_cmp_pct::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('GW_SUB_M_KG', 'EW_SUB_M_KG') -- must be the same order as defined in the database function
    AND var.abbrev = 'SHELL_CMP_PCT'
;

CREATE OR REPLACE FUNCTION master.formula_shell_cmp_pct(
				gw_sub_m_kg float,
                ew_sub_m_kg float
			) RETURNS float as
			$body$
			DECLARE
			  shell_cmp_pct float;
              local_gw_sub_m_kg float;
              local_ew_sub_m_kg float;
			BEGIN
				
shell_cmp_pct=(gw_sub_m_kg/ew_sub_m_kg)*100;
				
				RETURN round(shell_cmp_pct::numeric, 3);
			END
			$body$
			language plpgsql;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('SHELL_CMP_PCT');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'SHELL_CMP_PCT=(GW_SUB_M_KG/EW_SUB_M_KG)*100');
--rollback DELETE FROM master.formula WHERE formula IN ('SHELL_CMP_PCT=(GW_SUB_M_KG/EW_SUB_M_KG)*100');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('SHELL_CMP_PCT_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('SHELL_CMP_PCT');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('SHELL_CMP_PCT_SCALE');
--rollback DROP FUNCTION master.formula_shell_cmp_pct;            



--changeset postgres:add_GMOI_M_PCT_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert GMOI_M_PCT Formula  



-- GMOI_M_PCT
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('GMOI_M_PCT', 'GMOI_M_PCT', 'Harvested plot area', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Measured Harvested plot area percentage', 'active', '1', 'Harvested plot area','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'GMOI_M_PCT' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('GMOI_M_PCT', 'Harvested plot area', 'Harvested plot area') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('GMOI_M_PCT_METHOD', 'Harvested plot area method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('GMOI_M_PCT_SCALE', 'Harvested plot area scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('GMOI_M_PCT');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('GMOI_M_PCT_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('GMOI_M_PCT');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('GMOI_M_PCTSCALE');      



--changeset postgres:add_PLOT_AREA_HARVESTED_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert PLOT_AREA_HARVESTED Formula  



-- PLOT_AREA_HARVESTED
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('PLOT_AREA_HARVESTED', 'PLOT_AREA_HARVESTED', 'Harvested plot area', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Currently, this is manually input into the system by the user; it is measured in m2 (square meters)', 'active', '1', 'Grain moisture','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'PLOT_AREA_HARVESTED' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('PLOT_AREA_HARVESTED', 'Harvested plot area', 'Harvested plot area') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('PLOT_AREA_HARVESTED_METHOD', 'Harvested plot area method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('PLOT_AREA_HARVESTED_SCALE', 'Harvested plot area scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('PLOT_AREA_HARVESTED');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('PLOT_AREA_HARVESTED_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('PLOT_AREA_HARVESTED');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('PLOT_AREA_HARVESTED_SCALE');      



--changeset postgres:add_GY_CALC_KGHA_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert GY_CALC_KGHA Formula  



-- GY_CALC_KGHA
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('GY_CALC_KGHA', 'GY_Calc_kgha', 'GY_Calc_kgha', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Grain yield', 'active', '1', 'Grain yield','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'GY_CALC_KGHA' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('GY_CALC_KGHA', 'GY_Calc_kgha', 'Grain yield') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('GY_CALC_KGHA_METHOD', 'Grain yield method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('GY_CALC_KGHA_SCALE', 'Grain yield scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('GY_CALC_KGHA = GY_CALC_KGHA/1000', (SELECT id FROM master.variable WHERE abbrev = 'GY_CALC_KGHA'), (SELECT id FROM master.method WHERE abbrev = 'GY_CALC_KGHA_METHOD'), 'plot', '
    master.formula_gy_calc_kgha(gy_calc_kgha)', NULL, '
    create or replace function master.formula_gy_calc_kgha(
				gy_calc_kgha float
			) returns float as
			$body$
			declare
			  gy_calc_kgha float;
              local_gy_calc_kgha float;
			begin
				
gy_calc_kgha = gy_calc_kgha/1000;
				
				return round(gy_calc_kgha::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('GY_CALC_KGHA') -- must be the same order as defined in the database function
    AND var.abbrev = 'GY_CALC_KGHA'
;

CREATE OR REPLACE FUNCTION master.formula_gy_calc_kgha(
				gy_calc_kgha float
			) RETURNS float as
			$body$
			DECLARE
			  gy_calc_kgha float;
              local_gy_calc_kgha float;
			BEGIN
				
gy_calc_kgha = gy_calc_kgha/1000;
				
				RETURN round(gy_calc_kgha::numeric, 3);
			END
			$body$
			language plpgsql;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('GY_CALC_KGHA');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'GY_CALC_KGHA = GY_CALC_KGHA/1000');
--rollback DELETE FROM master.formula WHERE formula IN ('GY_CALC_KGHA = GY_CALC_KGHA/1000');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('GY_CALC_KGHA_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('GY_CALC_KGHA');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('GY_CALC_KGHA_SCALE');
--rollback DROP FUNCTION master.formula_gy_calc_kgha;         



--changeset postgres:add_GY_CALC_GM2_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert GY_CALC_GM2 Formula 



-- GY_CALC_GM2
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('GY_CALC_GM2', 'GY_CALC_GM2', 'GY_Calc_gm2', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Grain yield', 'active', '1', 'Grain yield','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'GY_CALC_GM2' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('GY_CALC_GM2', 'GY_Calc_gm2', 'Grain yield') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('GY_CALC_GM2_METHOD', 'GY_Calc_gm2 method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('GY_CALC_GM2_SCALE', 'GY_Calc_gm2 scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('GY_CALC_GM2 = GY_CALC_GM2/100', (SELECT id FROM master.variable WHERE abbrev = 'GY_CALC_GM2'), (SELECT id FROM master.method WHERE abbrev = 'GY_CALC_GM2_METHOD'), 'plot', '
    master.formula_gy_calc_gm2(gy_calc_gm2)', NULL, '
    create or replace function master.formula_gy_calc_gm2(
				gy_calc_gm2 float
			) returns float as
			$body$
			declare
			  gy_calc_gm2 float;
              local_gy_calc_gm2 float;
			begin
				
gy_calc_gm2 = gy_calc_gm2/100;
				
				return round(gy_calc_gm2::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('GY_CALC_GM2') -- must be the same order as defined in the database function
    AND var.abbrev = 'GY_CALC_GM2'
;

CREATE OR REPLACE FUNCTION master.formula_gy_calc_gm2(
				gy_calc_gm2 float
			) RETURNS float as
			$body$
			DECLARE
			  gy_calc_gm2 float;
              local_gy_calc_gm2 float;
			BEGIN
				
gy_calc_gm2 = gy_calc_gm2/100;
				
				RETURN round(gy_calc_gm2::numeric, 3);
			END
			$body$
			language plpgsql;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('GY_CALC_GM2');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'GY_CALC_GM2 = GY_CALC_GM2/100');
--rollback DELETE FROM master.formula WHERE formula IN ('GY_CALC_GM2 = GY_CALC_GM2/100');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('GY_CALC_GM2_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('GY_CALC_GM2');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('GY_CALC_GM2_SCALE');
--rollback DROP FUNCTION master.formula_gy_calc_gm2;         



--changeset postgres:add_GY_CALC_DAHA_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert GY_CALC_DAHA Formula 



-- GY_CALC_DAHA
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('GY_CALC_DAHA', 'GY_CALC_DAHA', 'GY_Calc_daha', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Grain yield', 'active', '1', 'Grain yield','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'GY_CALC_DAHA' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('GY_CALC_DAHA', 'GY_Calc_daha', 'Grain yield') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('GY_CALC_DAHA_METHOD', 'GY_Calc_daha method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('GY_CALC_DAHA_SCALE', 'GY_Calc_daha scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('GY_CALC_DAHA = GY_CALC_DAHA/100', (SELECT id FROM master.variable WHERE abbrev = 'GY_CALC_DAHA'), (SELECT id FROM master.method WHERE abbrev = 'GY_CALC_DAHA_METHOD'), 'plot', '
    master.formula_gy_calc_daha(gy_calc_daha)', NULL, '
    create or replace function master.formula_gy_calc_daha(
				gy_calc_daha float
			) returns float as
			$body$
			declare
			  gy_calc_daha float;
              Local_gy_calc_daha float;
			begin
				
gy_calc_daha = gy_calc_daha/100;
				
				return round(gy_calc_daha::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('GY_CALC_DAHA') -- must be the same order as defined in the database function
    AND var.abbrev = 'GY_CALC_DAHA'
;

CREATE OR REPLACE FUNCTION master.formula_gy_calc_daha(
				gy_calc_daha float
			) RETURNS float as
			$body$
			DECLARE
			  gy_calc_daha float;
              local_gy_calc_daha float;
			BEGIN
				
gy_calc_daha = gy_calc_daha/100;
				
				RETURN round(gy_calc_daha::numeric, 3);
			END
			$body$
			language plpgsql;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('GY_CALC_DAHA');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'GY_CALC_DAHA = GY_CALC_DAHA/100');
--rollback DELETE FROM master.formula WHERE formula IN ('GY_CALC_DAHA = GY_CALC_DAHA/100');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('GY_CALC_DAHA_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('GY_CALC_DAHA');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('GY_CALC_DAHA_SCALE');
--rollback DROP FUNCTION master.formula_gy_calc_daha;         



--changeset postgres:add_PH_M_CM_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert PH_M_CM Formula 



-- PH_M_CM
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('PH_M_CM', 'PH_M_CM', 'PH_M_cm', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Plant height', 'active', '1', 'Plant height','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'PH_M_CM' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('PH_M_CM', 'PH_M_cm', 'PH_M_cm') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('PH_M_CM_METHOD', 'PH_M_cm method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('PH_M_CM_SCALE', 'PH_M_cm scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('PH_M_CM');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('PH_M_CMMETHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('PH_M_CM');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('GPH_M_CM_SCALE');   



--changeset postgres:add_PH_M_M_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert PH_M_M Formula 



-- PH_M_M
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('PH_M_M', 'PH_M_M', 'PH_M_m', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Plant height', 'active', '1', 'Plant height','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'PH_M_M' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('PH_M_M', 'PH_M_m', 'PH_M_m') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('PH_M_M_METHOD', 'PH_M_m method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('PH_M_M_SCALE', 'PH_M_m scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('PH_M_M = PH_M_M/100', (SELECT id FROM master.variable WHERE abbrev = 'PH_M_M'), (SELECT id FROM master.method WHERE abbrev = 'PH_M_M_METHOD'), 'plot', '
    master.formula_ph_m_m(ph_m_m)', NULL, '
    create or replace function master.formula_ph_m_m(
				ph_m_m float
			) returns float as
			$body$
			declare
			  ph_m_m float;
              local_ph_m_m float;
			begin
				
ph_m_m = ph_m_m/100;
				
				return round(ph_m_m::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev In ('PH_M_M') -- must be the same order as defined in the database function
    AND var.abbrev = 'PH_M_M'
;

CREATE OR REPLACE FUNCTION master.formula_ph_m_m(
				ph_m_m float
			) RETURNS float as
			$body$
			DECLARE
			  ph_m_m float;
              local_ph_m_m float;
			BEGIN
				
ph_m_m = ph_m_m/100;
				
				RETURN round(ph_m_m::numeric, 3);
			END
			$body$
			language plpgsql;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('PH_M_M');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'PH_M_M = PH_M_M/100');
--rollback DELETE FROM master.formula WHERE formula IN ('PH_M_M = PH_M_M/100');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('PH_M_M_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('PH_M_M');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('GPH_M_M_SCALE');
--rollback DROP FUNCTION master.formula_ph_m_m;         



--changeset postgres:add_EMER_DATE_YMD_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert EMER_DATE_YMD Formula 



-- EMER_DATE_YMD
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('EMER_DATE_YMD', 'EMER_DATE_YMD', 'Emergence Date', 'date', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Emergence time', 'active', '1', 'Emergence time','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'EMER_DATE_YMD' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('EMER_DATE_YMD', 'Emergence Date', 'Emergence Date') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('EMER_DATE_YMD_METHOD', 'Emergence Date method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('EMER_DATE_YMD_SCALE', 'Emergence Date scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('EMER_DATE_YMD');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('EMER_DATE_YMD_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('EMER_DATE_YMD');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('EMER_DATE_YMD_SCALE');     



--changeset postgres:add_EMER_DTO_DAY_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert EMER_DTO_DAY Formula



-- EMER_DTO_DAY
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('EMER_DTO_DAY', 'EMER_DTO_DAY', 'Emer_dto_day', 'integer', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Emergence time', 'active', '1', 'Emergence time','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'EMER_DTO_DAY' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('EMER_DTO_DAY', 'Emer_dto_day', 'Emer_dto_day') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('EMER_DTO_DAY_METHOD', 'Emer_dto_day method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('EMER_DTO_DAY_SCALE', 'Emer_dto_day scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('EMER_DTO_DAY = PLANTING_DATE - EMER_DATE_YMD', (SELECT id FROM master.variable WHERE abbrev = 'EMER_DTO_DAY'), (SELECT id FROM master.method WHERE abbrev = 'EMER_DTO_DAY_METHOD'), 'plot', '
    master.formula_emer_dto_day(emer_dto_day,planting_date,emer_date_ymd)', NULL, '
    create or replace function master.formula_emer_dto_day(
				planting_date date,
                emer_date_ymd date
			) returns float as
			$body$
			declare
              emer_dto_day date;
			  planting_date date;
              emer_date_ymd date;
			begin
				
emer_dto_day = planting_date - emer_date_ymd;
				
				return round(emer_dto_day::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev In ('PLANTING_DATE','EMER_DATE_YMD') -- must be the same order as defined in the database function
    AND var.abbrev = 'EMER_DTO_DAY'
;

CREATE OR REPLACE FUNCTION master.formula_emer_dto_day(
				planting_date date,
                emer_date_ymd date
			) RETURNS float as
			$body$
			DECLARE
              emer_dto_day date;
			  planting_date date;
              emer_date_ymd date;
			BEGIN
				
EMER_DTO_DAY = PLANTING_DATE - EMER_DATE_YMD;
				
				RETURN round(emer_dto_day::numeric, 3);
			END
			$body$
			language plpgsql;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('EMER_DTO_DAY');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'EMER_DTO_DAY = PLANTING_DATE - EMER_DATE_YMD');
--rollback DELETE FROM master.formula WHERE formula IN ('EMER_DTO_DAY = PLANTING_DATE - EMER_DATE_YMD');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('EMER_DTO_DAY_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('EMER_DTO_DAY');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('EMER_DTO_DAY_SCALE');
--rollback DROP FUNCTION master.formula_emer_dto_day;     



--changeset postgres:add_BOOT_DATEINIT_YMD_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert BOOT_DATEINIT_YMD Formula



-- BOOT_DATEINIT_YMD

DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('BOOT_DATEINIT_YMD', 'BOOT_DATEINIT_YMD', 'BOOT_DATEINIT_YMD', 'date', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Booting time', 'active', '1', 'Booting time','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'BOOT_DATEINIT_YMD' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('BOOT_DATEINIT_YMD', 'BOOT_DATEINIT_YMD', 'BOOT_DATEINIT_YMD') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('BOOT_DATEINIT_YMD_METHOD', 'BOOT_DATEINIT_YMD method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('BOOT_DATEINIT_YMD_SCALE', 'BOOT_DATEINIT_YMD scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('BOOT_DATEINIT_YMD');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('BOOT_DATEINIT_YMD_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('BOOT_DATEINIT_YMD');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('BOOT_DATEINIT_YMD_SCALE');  



--changeset postgres:add_BOOT_DTOINIT_DAY_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert BOOT_DTOINIT_DAY Formula



-- BOOT_DTOINIT_DAY

DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('BOOT_DTOINIT_DAY', 'BOOT_DTOINIT_DAY', 'Boot_dtoInit_day', 'integer', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Booting time', 'active', '1', 'Booting time','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'Boot_dtoInit_day' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('BOOT_DTOINIT_DAY', 'BOOT_DTOINIT_DAY', 'Boot_dtoInit_day') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('BOOT_DTOINIT_DAY_METHOD', 'BOOT_DTOINIT_DAY method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('BOOT_DTOINIT_DAY_SCALE', 'BOOT_DTOINIT_DAY scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('BOOT_DTOINIT_DAY= EMER_DATE_YMD - BOOT_DATEINIT_YMD', (SELECT id FROM master.variable WHERE abbrev = 'BOOT_DTOINIT_DAY'), (SELECT id FROM master.method WHERE abbrev = 'BOOT_DTOINIT_DAY_METHOD'), 'plot', '
    master.formula_boot_dtoinit_day(boot_dtoinit_day,emer_date_ymd,boot_dateinit_ymd)', NULL, '
    create or replace function master.formula_boot_dtoinit_day(
				emer_date_ymd date,
                boot_dateinit_ymd date
			) returns float as
			$body$
			declare
              boot_dtoinit_day date;
			  emer_date_ymd date;
              boot_dateinit_ymd date;
			begin
				
boot_dtoinit_day= emer_date_ymd - boot_dateinit_ymd;
				
				return round(boot_dtoinit_day::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev In ('EMER_DATE_YMD','BOOT_DATEINIT_YMD') -- must be the same order as defined in the database function
    AND var.abbrev = 'BOOT_DTOINIT_DAY'
;

CREATE OR REPLACE FUNCTION master.formula_boot_dtoinit_day(
				emer_date_ymd date,
                boot_dateinit_ymd date
			) RETURNS float as
			$body$
			DECLARE
              boot_dtoinit_day date;
			  emer_date_ymd date;
              boot_dateinit_ymd date;
			BEGIN
				
boot_dtoinit_day= emer_date_ymd - boot_dateinit_ymd;
				
				RETURN round(boot_dtoinit_day::numeric, 3);
			END
			$body$
			language plpgsql;


--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('BOOT_DTOINIT_DAY');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'BOOT_DTOINIT_DAY= EMER_DATE_YMD - BOOT_DATEINIT_YMD');
--rollback DELETE FROM master.formula WHERE formula IN ('BOOT_DTOINIT_DAY= EMER_DATE_YMD - BOOT_DATEINIT_YMD');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('BOOT_DTOINIT_DAY_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('BOOT_DTOINIT_DAY');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('BOOT_DTOINIT_DAY_SCALE');
--rollback DROP FUNCTION master.formula_boot_dtoinit_day;     



--changeset postgres:add_BOOT_DATE_YMD_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert BOOT_DATE_YMD Formula  



-- BOOT_DATE_YMD
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('BOOT_DATE_YMD', 'BOOT_DATE_YMD', 'Boot_date_ymd', 'date', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Booting time', 'active', '1', 'Booting time','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'BOOT_DATE_YMD' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('BOOT_DATE_YMD', 'BOOT_DATE_YMD', 'Boot_date_ymd') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('BOOT_DATE_YMD_METHOD', 'Boot_date_ymd method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('BOOT_DATE_YMD_SCALE', 'Boot_date_ymd scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('BOOT_DATE_YMD');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('BOOT_DATE_YMD_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('BOOT_DATE_YMD');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('BOOT_DATE_YMD_SCALE');  



--changeset postgres:add_BOOT_DTO_DAY_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert BOOT_DTO_DAY Formula  



-- BOOT_DTO_DAY
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('BOOT_DTO_DAY', 'BOOT_DTO_DAY', 'Boot_dto_day', 'integer', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Booting time', 'active', '1', 'Booting time','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'BOOT_DTO_DAY' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('BOOT_DTO_DAY', 'BOOT_DTO_DAY', 'Boot_dto_day') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('BOOT_DTO_DAY_METHOD', 'Boot_dto_day method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('BOOT_DTO_DAY_SCALE', 'Boot_dto_day scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('BOOT_DTO_DAY = BOOT_DATE_YMD - EMER_DATE_YMD', (SELECT id FROM master.variable WHERE abbrev = 'BOOT_DTO_DAY'), (SELECT id FROM master.method WHERE abbrev = 'BOOT_DTO_DAY_METHOD'), 'plot', '
    master.formula_boot_dto_day(boot_dto_day,boot_date_ymd,emer_date_ymd)', NULL, '
    create or replace function master.formula_hd_dto_day(
				boot_dto_day date,
                boot_date_ymd date,
                emer_date_ymd date
			) returns float as
			$body$
			declare
			  boot_dto_day date;
              local_boot_date_ymd date;
              local_emer_date_ymd date;

			begin
				
boot_dto_day = boot_date_ymd - emer_date_ymd;
				
				return round(boot_dto_day::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev in ('BOOT_DATE_YMD','EMER_DATE_YMD') -- must be the same order as defined in the database function
    AND var.abbREV = 'BOOT_DTO_DAY'
;

CREATE OR REPLACE FUNCTION master.formula_boot_dto_day(
				boot_dto_day date,
                boot_date_ymd date,
                emer_date_ymd date
			) RETURNS float as
			$body$
			DECLARE
			  boot_dto_day date;
              local_boot_date_ymd date;
              local_emer_date_ymd date;

			BEGIN
				
boot_dto_day = boot_date_ymd - emer_date_ymd;
				
				RETURN round(boot_dto_day::numeric, 3);
			END
			$body$
			language plpgsql;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('BOOT_DTO_DAY');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'BOOT_DTO_DAY = BOOT_DATE_YMD - EMER_DATE_YMD');
--rollback DELETE FROM master.formula WHERE formula IN ('BOOT_DTO_DAY = BOOT_DATE_YMD - EMER_DATE_YMD');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('BOOT_DTO_DAY_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('BOOT_DTO_DAY');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('BOOT_DTO_DAY_SCALE');
--rollback DROP FUNCTION master.formula_boot_dto_day;   



--changeset postgres:add_HD_DATE_YMD_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert HD_DATE_YMD Formula  



-- HD_DATE_YMD
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('HD_DATE_YMD', 'HD_DATE_YMD', 'Hd_dto_ymd', 'date', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Heading time', 'active', '1', 'Heading time','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'HD_DATE_YMD' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('HD_DATE_YMD', 'HD_DATE_YMD', 'Hd_dto_ymd') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('HD_DATE_YMD_METHOD', 'Hd_dto_ymd method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('HD_DATE_YMD_SCALE', 'Hd_dto_ymd scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('HD_DATE_YMD');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('HD_DATE_YMD_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('HD_DATE_YMD');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('HD_DATE_YMD_SCALE');   



--changeset postgres:add_HD_DTO_DAY_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert HD_DTO_DAY Formula  



-- HD_DTO_DAY
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('HD_DTO_DAY', 'HD_DTO_DAY', 'Hd_dto_day', 'integer', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Heading time', 'active', '1', 'Heading time','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'HD_DTO_DAY' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('HD_DTO_DAY', 'HD_DTO_DAY', 'Hd_dto_day') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('HD_DTO_DAY_METHOD', 'Hd_dto_day method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('HD_DTO_DAY_SCALE', 'Hd_dto_day scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('HD_DTO_DAY = HD_DATE_YMD - EMER_DATE_YMD', (SELECT id FROM master.variable WHERE abbrev = 'HD_DTO_DAY'), (SELECT id FROM master.method WHERE abbrev = 'HD_DTO_DAY_METHOD'), 'plot', '
    master.formula_hd_dto_day(hd_dto_day,hd_date_ymd,emer_date_ymd)', NULL, '
                hd_date_ymd date,
                emer_date_ymd date
			) returns float as
			$body$
			declare
			  hd_dto_day date;
              local_hd_date_ymd date;
              local_emer_date_ymd date;

			begin
				
hd_dto_day = hd_date_ymd - emer_date_ymd;
				
				return round(hd_dto_day::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev in ('HD_DATE_YMD','EMER_DATE_YMD') -- must be the same order as defined in the database function
    AND var.abbREV = 'HD_DTO_DAY'
;

CREATE OR REPLACE FUNCTION master.formula_hd_dto_day(
                hd_date_ymd date,
                emer_date_ymd date
			) RETURNS float as
			$body$
			DECLARE
			  hd_dto_day date;
              local_hd_date_ymd date;
              local_emer_date_ymd date;

			BEGIN
				
hd_dto_day = hd_date_ymd - emer_date_ymd;
				
				RETURN round(hd_dto_day::numeric, 3);
			END
			$body$
			language plpgsql;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('HD_DTO_DAY');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'HD_DTO_DAY = HD_DATE_YMD - EMER_DATE_YMD');
--rollback DELETE FROM master.formula WHERE formula IN ('HD_DTO_DAY = HD_DATE_YMD - EMER_DATE_YMD');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('HD_DTO_DAY_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('HD_DTO_DAY');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('HD_DTO_DAY_SCALE');
--rollback DROP FUNCTION master.formula_hd_dto_day;     



--changeset postgres:add_MAT_DATE_YMD_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert MAT_DATE_YMD Formula 



-- MAT_DATE_YMD
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('MAT_DATE_YMD', 'MAT_DATE_YMD', 'Mat_date_ymd', 'date', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Maturity time', 'active', '1', 'Maturity time','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'MAT_DATE_YMD' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('MAT_DATE_YMD', 'MAT_DATE_YMD', 'Mat_date_ymd') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('MAT_DATE_YMD_METHOD', 'Mat_date_ymd method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('MAT_DATE_YMD_SCALE', 'Mat_date_ymd scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('MAT_DATE_YMD');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('MAT_DATE_YMD_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('MAT_DATE_YMD');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('MAT_DATE_YMD_SCALE');



--changeset postgres:add_MAT_DTO_DAY_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert MAT_DTO_DAY Formula 



-- MAT_DTO_DAY
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('MAT_DTO_DAY', 'MAT_DTO_DAY', 'Mat_dto_day', 'integer', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Maturity time', 'active', '1', 'Maturity time','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'MAT_DTO_DAY' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('MAT_DTO_DAY', 'MAT_DTO_DAY', 'Mat_dto_day') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('MAT_DTO_DAY_METHOD', 'Mat_dto_day method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('MAT_DTO_DAY_SCALE', 'Mat_dto_day scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('MAT_DTO_DAY = MAT_DATE_YMD - EMER_DATE_YMD', (SELECT id FROM master.variable WHERE abbrev = 'MAT_DTO_DAY'), (SELECT id FROM master.method WHERE abbrev = 'MAT_DTO_DAY_METHOD'), 'plot', '
    master.formula_mat_dto_day(mat_dto_day,mat_date_ymd,emer_date_ymd)', NULL, '
    create or replace function master.formula_mat_dto_day(
                mat_date_ymd date,
                emer_date_ymd date
			) returns float as
			$body$
			declare
			  mat_dto_day date;
              local_mat_date_ymd date;
              local_emer_date_ymd date;

			begin
				
mat_dto_day = mat_date_ymd - emer_date_ymd;
				
				return round(mat_dto_day::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev in ('MAT_DATE_YMD','EMER_DATE_YMD') -- must be the same order as defined in the database function
    AND var.abbREV = 'MAT_DTO_DAY'
;

CREATE OR REPLACE FUNCTION master.formula_mat_dto_day(
                mat_date_ymd date,
                emer_date_ymd date
			) RETURNS float as
			$body$
			DECLARE
			  mat_dto_day date;
              local_mat_date_ymd date;
              local_emer_date_ymd date;

			BEGIN
				
mat_dto_day = mat_date_ymd - emer_date_ymd;
				
				RETURN round(mat_dto_day::numeric, 3);
			END
			$body$
			language plpgsql;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('MAT_DTO_DAY');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'MAT_DTO_DAY = MAT_DATE_YMD - EMER_DATE_YMD');
--rollback DELETE FROM master.formula WHERE formula IN ('MAT_DTO_DAY = MAT_DATE_YMD - EMER_DATE_YMD');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('MAT_DTO_DAY_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('MAT_DTO_DAY');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('MAT_DTO_DAY_SCALE');
--rollback DROP FUNCTION master.formula_mat_dto_day;     



--changeset postgres:add_TSPL_DATE_YMD_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert TSPL_DATE_YMD Formula 



-- TSPL_DATE_YMD
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('TSPL_DATE_YMD', 'TSPL_DATE_YMD', 'TSpl_date_ymd', 'date', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Terminal spikelet time', 'active', '1', 'Terminal spikelet time','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'TSPL_DATE_YMD' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('TSPL_DATE_YMD', 'TSPL_DATE_YMD', 'TSpl_date_ymd') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('TSPL_DATE_YMD_METHOD', 'TSpl_date_ymd method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('TSPL_DATE_YMD_SCALE', 'TSpl_date_ymd scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('TSPL_DATE_YMD');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('TSPL_DATE_YMD_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('TSPL_DATE_YMD');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('TSPL_DATE_YMD_SCALE');



--changeset postgres:add_TSPL_DTO_DAY_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert TSPL_DTO_DAY Formula 



-- TSPL_DTO_DAY
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('TSPL_DTO_DAY', 'TSPL_DTO_DAY', 'TSpl_dto_day', 'integer', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Terminal spikelet time', 'active', '1', 'Terminal spikelet time','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'TSPL_DTO_DAY' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('TSPL_DTO_DAY', 'TSPL_DTO_DAY', 'TSpl_dto_day') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('TSPL_DTO_DAY_METHOD', 'TSpl_dto_day method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('TSPL_DTO_DAY_SCALE', 'TSpl_dto_day scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('TSPL_DTO_DAY = TSPL_DATE_YMD - EMER_DATE_YMD', (SELECT id FROM master.variable WHERE abbrev = 'TSPL_DTO_DAY'), (SELECT id FROM master.method WHERE abbrev = 'TSPL_DTO_DAY_METHOD'), 'plot', '
    master.formula_tspl_dto_dayy(tspl_dto_day,tspl_date_ymd,emer_date_ymd)', NULL, '
    create or replace function master.formula_tspl_dto_day(
                tspl_date_ymd date,
                emer_date_ymd date
			) returns float as
			$body$
			declare
			  tspl_dto_day date;
              local_tspl_date_ymd date;
              local_emer_date_ymd date;

			begin
				
tspl_dto_day = tspl_date_ymd - emer_date_ymd;
				
				return round(tspl_dto_day::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev in ('TSPL_DATE_YMD','EMER_DATE_YMD') -- must be the same order as defined in the database function
    AND var.abbREV = 'TSPL_DTO_DAY'
;

CREATE OR REPLACE FUNCTION master.formula_tspl_dto_day(
                tspl_date_ymd date,
                emer_date_ymd date
			) RETURNS float as
			$body$
			DECLARE
			  tspl_dto_day date;
              local_tspl_date_ymd date;
              local_emer_date_ymd date;

			BEGIN
				
tspl_dto_day = tspl_date_ymd - emer_date_ymd;
				
				RETURN round(tspl_dto_day::numeric, 3);
			END
			$body$
			language plpgsql;




--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('TSPL_DTO_DAY');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'TSPL_DTO_DAY = TSPL_DATE_YMD - EMER_DATE_YMD');
--rollback DELETE FROM master.formula WHERE formula IN ('TSPL_DTO_DAY = TSPL_DATE_YMD - EMER_DATE_YMD');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('TSPL_DTO_DAY_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('TSPL_DTO_DAY');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('TSPL_DTO_DAY_SCALE');
--rollback DROP FUNCTION master.formula_tspl_dto_day;              