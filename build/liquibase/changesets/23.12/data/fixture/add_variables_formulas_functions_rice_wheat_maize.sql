--liquibase formatted sql

--changeset postgres:add_MF_CONT_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert MF_CONT Formula



-- MF_CONT
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('MF_CONT', 'MF', 'Moisture factor', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Moisture factor', 'active', '1', 'Moisture factor','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'MF' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('MF_CONT', 'Moisture factor', 'Moisture factor') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    -- INSERT INTO
    --    master.method (abbrev, name) 
    -- VALUES
    --    ('MF_CONT_METHOD', 'Moisture factor')
    -- RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('MF_CONT_SCALE', 'Moisture factor', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id)
    VALUES ('MF_CONT = (100 - MC_CONT) / 86', (SELECT id FROM master.variable WHERE abbrev = 'MF_CONT'), (SELECT id FROM master.method WHERE abbrev = 'MF_CONT_METHOD'), 'plot', 'master.formula_mf_cont(mc_cont)', NULL, 'create or replace function master.formula_mf_cont(
                mc_cont float
			) returns float as
			$body$
			declare
			  mf_cont float;
			begin
				
            mf_cont=(100-mc_cont)/86;
				
				return round(mf_cont::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '1');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('MC_CONT') -- must be the same order as defined in the database function
    AND var.abbrev = 'MF_CONT'
;

create or replace function master.formula_mf_cont(
                mc_cont float
			) returns float as
			$body$
			declare
			  mf_cont float;
			begin
				
            mf_cont=(100-mc_cont)/86;
				
				return round(mf_cont::numeric, 3);
			end
			$body$
			language plpgsql;

select master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'MF_CONT = (100 - MC_CONT) / 86');
--rollback DELETE FROM master.formula WHERE formula IN ('MF_CONT = (100 - MC_CONT) / 86');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('MF_CONT');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('MF_CONT_SCALE');
--rollback DROP FUNCTION master.formula_mf_cont;



--changeset postgres:add_ADJAYLD_G_CONT_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert ADJAYLD_G_CONT Formula



-- ADJAYLD_G_CONT
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('ADJAYLD_G_CONT', 'AYLD_G_MC', 'Plot yield in grams adjusted to 14% MC', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Plot yield in grams adjusted to 14% MC', 'active', '1', 'Plot yield in grams adjusted to 14% MC','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'ADJAYLD_G_CONT' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('ADJAYLD_G_CONT', 'Plot yield in grams adjusted to 14% MC', 'Plot yield in grams adjusted to 14% MC') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('ADJAYLD_G_CONT_METHOD', 'Plot yield in grams adjusted to 14% MC method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('ADJAYLD_G_CONT_SCALE', 'Plot yield in grams adjusted to 14% MC scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('ADJAYLD_G_CONT=AYLD_CONT*MF_CONT', (SELECT id FROM master.variable WHERE abbrev = 'ADJAYLD_G_CONT'), (SELECT id FROM master.method WHERE abbrev = 'ADJAYLD_G_CONT_METHOD'), 'plot', 'master.formula_adjayld_g_cont(ayld_cont, mf_cont)', NULL, 'create or replace function master.formula_adjayld_g_cont(
				ayld_cont float,
                mf_cont float
			) returns float as
			$body$
			declare
				adjayld_g_cont float;
              local_ayld_cont float;
              local_mf_cont float;
			begin
				
            adjayld_g_cont=ayld_cont*mf_cont;
				
				return round(adjayld_g_cont::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('AYLD_CONT', 'MF_CONT') -- must be the same order as defined in the database function
    AND var.abbrev = 'ADJAYLD_G_CONT'
;

create or replace function master.formula_adjayld_g_cont(
				ayld_cont float,
                mf_cont float
			) returns float as
			$body$
			declare
				adjayld_g_cont float;
              local_ayld_cont float;
              local_mf_cont float;
			begin
				
adjayld_g_cont=ayld_cont*mf_cont;
				
				return round(adjayld_g_cont::numeric, 3);
			end
			$body$
			language plpgsql;

select master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('ADJAYLD_G_CONT');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'ADJAYLD_G_CONT=AYLD_CONT*MF_CONT');
--rollback DELETE FROM master.formula WHERE formula IN ('ADJAYLD_G_CONT=AYLD_CONT*MF_CONT');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('ADJAYLD_G_CONT_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('ADJAYLD_G_CONT');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('ADJAYLD_G_CONT_SCALE');
--rollback DROP FUNCTION master.formula_adjayld_g_cont;



--changeset postgres:add_AYLD_KG_CONT_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert AYLD_KG_CONT Formula 



-- AYLD_KG_CONT
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('AYLD_KG_CONT', 'AYLD_KG_CONT', 'AYLD_kg_CONT', 'float', false, 'observation', 'occurrence', 'application', 'AYLD_kg_CONT', 'active', '1', 'AYLD_kg_CONT', NULL)
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'AYLD_KG_CONT' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('AYLD_KG_CONT', 'AYLD_kg_CONT', 'AYLD_kg_CONT') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('AYLD_KG_CONT_METHOD', 'AYLD_kg_CONT method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('AYLD_KG_CONT_SCALE', 'AYLD_kg_CONT scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('AYLD_KG_CONT');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('AYLD_KG_CONT_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('AYLD_KG_CONT');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('AYLD_KG_CONT_SCALE');



--changeset postgres:add_ADJAYLD_KG_CONT_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert ADJAYLD_KG_CONT Formula



-- ADJAYLD_KG_CONT
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('ADJAYLD_KG_CONT', 'AYLD_KG_MC', 'Plot yield in kilograms adjusted to 14% MC', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Plot yield in kilograms adjusted to 14% MC', 'active', '1', 'Plot yield in kilograms adjusted to 14% MC','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'ADJAYLD_G_CONT' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('ADJAYLD_KG_CONT', 'Plot yield in kilograms adjusted to 14% MC', 'Plot yield in kilograms adjusted to 14% MC') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('ADJAYLD_KG_CONT_METHOD', 'Plot yield in kilograms adjusted to 14% MC method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('ADJAYLD_KG_CONT_SCALE', 'Plot yield in kilograms adjusted to 14% MC scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id)
    VALUES ('ADJAYLD_KG_CONT = AYLD_KG_CONT * MF_CONT', (SELECT id FROM master.variable WHERE abbrev = 'ADJAYLD_KG_CONT'), (SELECT id FROM master.method WHERE abbrev = 'ADJAYLD_KG_CONT_METHOD'), 'plot', 'master.formula_adjayld_kg_cont(ayld_kg_cont, mf_cont)', NULL, 'create or replace function master.formula_adjayld_kg_cont(
				ayld_kg_cont float,
                mf_cont float
			) returns float as
			$body$
			declare
				adjayld_kg_cont float;
			begin
				
            adjayld_kg_cont=ayld_kg_cont*mf_cont;
				
				return round(adjayld_kg_cont::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '1');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('AYLD_KG_CONT', 'MF_CONT') -- must be the same order as defined in the database function
    AND var.abbrev = 'ADJAYLD_KG_CONT'
;

create or replace function master.formula_adjayld_kg_cont(
				ayld_kg_cont float,
                mf_cont float
			) returns float as
			$body$
			declare
				adjayld_kg_cont float;
			begin
				
            adjayld_kg_cont=ayld_kg_cont*mf_cont;
				
				return round(adjayld_kg_cont::numeric, 3);
			end
			$body$
			language plpgsql;

select master.populate_order_number_for_master_formula_parameter();


--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('ADJAYLD_KG_CONT');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'ADJAYLD_KG_CONT = AYLD_KG_CONT * MF_CONT');
--rollback DELETE FROM master.formula WHERE formula IN ('ADJAYLD_KG_CONT = AYLD_KG_CONT * MF_CONT');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('ADJAYLD_KG_CONT_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('ADJAYLD_KG_CONT');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('ADJAYLD_KG_CONT_SCALE');
--rollback DROP FUNCTION master.formula_adjayld_kg_cont;



--changeset postgres:add_ADJAYLD_KG_CONT4_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert ADJAYLD_KG_CONT4 Formula



-- ADJAYLD_KG_CONT4
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('ADJAYLD_KG_CONT4', 'Corrected PlotYield(KG)_MC', 'Corrected Plot Yield in Kg adjusted to 14%MC', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Corrected Plot Yield in Kg adjusted to 14%MC', 'active', '1', 'Corrected Plot Yield in Kg adjusted to 14%MC','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'ADJAYLD_KG_CONT4' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('ADJAYLD_KG_CONT4', 'Corrected Plot Yield in Kg adjusted to 14%MC', 'Corrected Plot Yield in Kg adjusted to 14%MC') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('ADJAYLD_KG_CONT4_METHOD', 'Corrected Plot Yield in Kg adjusted to 14%MC method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('ADJAYLD_KG_CONT4_SCALE', 'Corrected Plot Yield in Kg adjusted to 14%MC scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('ADJAYLD_KG_CONT4 = ( AYLD_KG_CONT/HVHILL_CONT) * TOTAL_HILL_CONT *MF_CONT', (SELECT id FROM master.variable WHERE abbrev = 'ADJAYLD_KG_CONT4'), (SELECT id FROM master.method WHERE abbrev = 'ADJAYLD_KG_CONT4_METHOD'), 'plot', 'master.formula_adjayld_kg_cont4(ayld_kg_cont, hvhill_cont, total_hill_cont, mf_cont)', NULL, 'create or replace function master.formula_adjayld_kg_cont4(
                ayld_kg_cont float,
                hvhill_cont int,
                total_hill_cont int,
                mf_cont float
            ) returns float as
            $body$
            declare
                adjayld_kg_cont4 float;
              local_ayld_kg_cont float;
              local_hvhill_cont int;
              local_total_hill_cont int;
              local_mf_cont float;
            begin
                
            adjayld_kg_cont4 = ( ayld_kg_cont/hvhill_cont) * total_hill_cont *mf_cont;
                
                return round(adjayld_kg_cont4::numeric, 3);
            end
            $body$
            language plpgsql;', '3', '18', '2016-06-14 16:47:19.287753');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('AYLD_KG_CONT', 'HVHILL_CONT','TOTAL_HILL_CONT','MF_CONT') -- must be the same order as defined in the database function
    AND var.abbrev = 'ADJAYLD_KG_CONT4'
;

create or replace function master.formula_adjayld_kg_cont4(
                ayld_kg_cont float,
                hvhill_cont int,
                total_hill_cont int,
                mf_cont float
            ) returns float as
            $body$
            declare
                adjayld_kg_cont4 float;
              local_ayld_kg_cont float;
              local_hvhill_cont int;
              local_total_hill_cont int;
              local_mf_cont float;
            begin
                
adjayld_kg_cont4 = ( ayld_kg_cont/hvhill_cont) * total_hill_cont *mf_cont;
                
                return round(adjayld_kg_cont4::numeric, 3);
            end
            $body$
            language plpgsql;

select master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('ADJAYLD_KG_CONT4');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'ADJAYLD_KG_CONT4 = ( AYLD_KG_CONT/HVHILL_CONT) * TOTAL_HILL_CONT *MF_CONT');
--rollback DELETE FROM master.formula WHERE formula IN ('ADJAYLD_KG_CONT4 = ( AYLD_KG_CONT/HVHILL_CONT) * TOTAL_HILL_CONT *MF_CONT');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('ADJAYLD_KG_CONT4_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('ADJAYLD_KG_CONT4');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('ADJAYLD_KG_CONT4_SCALE');
--rollback DROP FUNCTION master.formula_adjayld_kg_cont4;



--changeset postgres:add_ADJYLD3_CONT_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert ADJYLD3_CONT Formula



-- ADJYLD3_CONT
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('ADJYLD3_CONT', 'ADJYLD3_MET', 'Adjusted plot yield in kg/ha for MET', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Adjusted plot yield in kg/ha for MET', 'active', '1', 'Adjusted plot yield in kg/ha for MET','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'ADJYLD3_CONT' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('ADJYLD3_CONT', 'Adjusted plot yield in kg/ha for MET', 'Adjusted plot yield in kg/ha for MET') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('ADJYLD3_CONT_METHOD', 'Adjusted plot yield in kg/ha for MET method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('ADJYLD3_CONT_SCALE', 'Adjusted plot yield in kg/ha for MET scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('ADJYLD3_CONT = (AYLD_CONT/HVHILL_CONT) * TOTAL_HILL_CONT * MF_CONT', (SELECT id FROM master.variable WHERE abbrev = 'ADJYLD3_CONT'), (SELECT id FROM master.method WHERE abbrev = 'ADJYLD3_CONT_METHOD' limit 1), 'plot', 'master.formula_adjyld3_cont(ayld_cont, hvhill_cont, total_hill_cont, mf_cont)', NULL, 'create or replace function master.formula_adjyld3_cont(
                ayld_cont float,
                hvhill_cont int,
                total_hill_cont int,
                mf_cont float
            ) returns float as
            $body$
            declare
                adjyld3_cont float;
              local_ayld_cont float;
              local_hvhill_cont int;
              local_total_hill_cont int;
              local_mf_cont float;
            begin
                
            adjyld3_cont = (ayld_cont/hvhill_cont) * total_hill_cont * mf_cont;
                
                return round(adjyld3_cont::numeric, 3);
            end
            $body$
            language plpgsql;', '3', '19', '2015-03-24 02:06:14.048405');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('AYLD_CONT', 'HVHILL_CONT','TOTAL_HILL_CONT','MF_CONT') -- must be the same order as defined in the database function
    AND var.abbrev = 'ADJYLD3_CONT'
;

create or replace function master.formula_adjyld3_cont(
                ayld_cont float,
                hvhill_cont int,
                total_hill_cont int,
                mf_cont float
            ) returns float as
            $body$
            declare
                adjyld3_cont float;
              local_ayld_cont float;
              local_hvhill_cont int;
              local_total_hill_cont int;
              local_mf_cont float;
            begin
                
adjyld3_cont = (ayld_cont/hvhill_cont) * total_hill_cont * mf_cont;
                
                return round(adjyld3_cont::numeric, 3);
            end
            $body$
            language plpgsql;

select master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('ADJYLD3_CONT');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'ADJYLD3_CONT = (AYLD_CONT/HVHILL_CONT) * TOTAL_HILL_CONT * MF_CONT');
--rollback DELETE FROM master.formula WHERE formula IN ('ADJYLD3_CONT = (AYLD_CONT/HVHILL_CONT) * TOTAL_HILL_CONT * MF_CONT');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('ADJYLD3_CONT_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('ADJYLD3_CONT');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('ADJYLD3_CONT_SCALE');
--rollback DROP FUNCTION master.formula_adjyld3_cont;


--changeset postgres:add_AREA_FACT_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert AREA_FACT Formula



-- AREA_FACT
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('AREA_FACT', 'AREA FACTOR', 'Area Factor', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Area Factor', 'active', '1', 'Area Factor','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'AREA_FACT' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('AREA_FACT', 'Area Factor', 'Area Factor') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('AREA_FACT_METHOD', 'Area Factor method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('AREA_FACT_SCALE', 'Area Factor scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('AREA_FACT = 10/HV_AREA_SQM', (SELECT id FROM master.variable WHERE abbrev = 'AREA_FACT'), (SELECT id FROM master.method WHERE abbrev = 'AREA_FACT_METHOD' limit 1), 'plot', 'master.formula_area_fact(hv_area_sqm)', NULL, 'create or replace function master.formula_area_fact(
                hv_area_sqm float
            ) returns integer as
            $body$
            declare
                area_fact integer;
              local_hv_area_sqm float;
            begin
                
            area_fact = 10/hv_area_sqm;
                
                return area_fact;
            end
            $body$
            language plpgsql;', NULL, '19', '2017-10-18 11:40:48.868719');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('HV_AREA_SQM') -- must be the same order as defined in the database function
    AND var.abbrev = 'AREA_FACT'
;

create or replace function master.formula_area_fact(
                hv_area_sqm float
            ) returns integer as
            $body$
            declare
                area_fact integer;
              local_hv_area_sqm float;
            begin
                
area_fact = 10/hv_area_sqm;
                
                return area_fact;
            end
            $body$
            language plpgsql;

select master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('AREA_FACT');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'AREA_FACT = 10/HV_AREA_SQM');
--rollback DELETE FROM master.formula WHERE formula IN ('AREA_FACT = 10/HV_AREA_SQM');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('AREA_FACT_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('AREA_FACT');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('AREA_FACT_SCALE');
--rollback DROP FUNCTION master.formula_area_fact;



--changeset postgres:add_DISEASE_INDEX_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert DISEASE_INDEX Formula



-- DISEASE_INDEX
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('DISEASE_INDEX', 'Disease Index Greenhouse Testing', 'Disease Index Greenhouse Testing', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Disease Index Greenhouse Testing', 'active', '1', 'Disease Index Greenhouse Testing','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'DISEASE_INDEX' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('DISEASE_INDEX', 'Disease Index Greenhouse Testing', 'Disease Index Greenhouse Testing') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('DISEASE_INDEX_METHOD', 'Disease Index Greenhouse Testing method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('DISEASE_INDEX_SCALE', 'Disease Index Greenhouse Testing scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('DISEASE_INDEX = ((3*PLANT_TESTED_SCORE3)+(5*PLANT_TESTED_SCORE5)+(7*PLANT_TESTED_SCORE7)+(9*PLANT_TESTED_SCORE9))/TOTAL_PLANT_TESTED::float', (SELECT id FROM master.variable WHERE abbrev = 'DISEASE_INDEX'), (SELECT id FROM master.method WHERE abbrev = 'DISEASE_INDEX_METHOD' limit 1), 'plot', 'master.formula_disease_index(plant_tested_score3, plant_tested_score5, plant_tested_score7, plant_tested_score9, total_plant_tested)', NULL, 'create or replace function master.formula_disease_index(
                plant_tested_score3 int,
                plant_tested_score5 int,
                plant_tested_score7 int,
                plant_tested_score9 int,
                total_plant_tested int
            ) returns float as
            $body$
            declare
                disease_index float;
              local_plant_tested_score3 int;
              local_plant_tested_score5 int;
              local_plant_tested_score7 int;
              local_plant_tested_score9 int;
              local_total_plant_tested int;
            begin
                
            disease_index = ((3*plant_tested_score3)+(5*plant_tested_score5)+(7*plant_tested_score7)+(9*plant_tested_score9))/total_plant_tested;
                
                return round(disease_index::numeric, 2);
            end
            $body$
            language plpgsql;', '2', '19', '2016-06-24 13:54:47.015532');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('PLANT_TESTED_SCORE3', 'PLANT_TESTED_SCORE5', 'PLANT_TESTED_SCORE7', 'PLANT_TESTED_SCORE9', 'TOTAL_PLANT_TESTED') -- must be the same order as defined in the database function
    AND var.abbrev = 'DISEASE_INDEX'
;

create or replace function master.formula_disease_index(
                plant_tested_score3 int,
                plant_tested_score5 int,
                plant_tested_score7 int,
                plant_tested_score9 int,
                total_plant_tested int
            ) returns float as
            $body$
            declare
                disease_index float;
              local_plant_tested_score3 int;
              local_plant_tested_score5 int;
              local_plant_tested_score7 int;
              local_plant_tested_score9 int;
              local_total_plant_tested int;
            begin
                
disease_index = ((3*plant_tested_score3)+(5*plant_tested_score5)+(7*plant_tested_score7)+(9*plant_tested_score9))/total_plant_tested;
                
                return round(disease_index::numeric, 2);
            end
            $body$
            language plpgsql;

select master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('DISEASE_INDEX');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'DISEASE_INDEX = ((3*PLANT_TESTED_SCORE3)+(5*PLANT_TESTED_SCORE5)+(7*PLANT_TESTED_SCORE7)+(9*PLANT_TESTED_SCORE9))/TOTAL_PLANT_TESTED::float');
--rollback DELETE FROM master.formula WHERE formula IN ('DISEASE_INDEX = ((3*PLANT_TESTED_SCORE3)+(5*PLANT_TESTED_SCORE5)+(7*PLANT_TESTED_SCORE7)+(9*PLANT_TESTED_SCORE9))/TOTAL_PLANT_TESTED::float');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('DISEASE_INDEX_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('DISEASE_INDEX');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('DISEASE_INDEX_SCALE');
--rollback DROP FUNCTION master.formula_disease_index;



--changeset postgres:add_HI_CONT_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert HI_CONT Formula



-- HI_CONT
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('HI_CONT', 'harvest index', 'harvest index', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'harvest index', 'active', '1', 'harvest index','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'HI_CONT' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('HI_CONT', 'harvest index', 'harvest index') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('HI_CONT_METHOD', 'harvest index method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('HI_CONT_SCALE', 'harvest index scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id)
    VALUES ('HI_CONT = GRNWT_BM_CONT / TOTBM_CONT', (SELECT id FROM master.variable WHERE abbrev = 'HI_CONT'), (SELECT id FROM master.method WHERE abbrev = 'HI_CONT_METHOD' limit 1), 'plot', 'master.formula_hi_cont(grnwt_bm_cont, totbm_cont)', NULL, 'create or replace function master.formula_hi_cont(
                grnwt_bm_cont float, 
                totbm_cont float
            ) returns float as
            $body$
            declare
                hi_cont float;
                local_grnwt_bm_cont float;
                local_totbm_cont float;
            begin
                
            hi_cont = grnwt_bm_count/totbm_count;
                
                return round(hi_cont::numeric, 3);
            end
            $body$
            language plpgsql;', '3', '1');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('GRNWT_BM_CONT', 'TOTBM_CONT') -- must be the same order as defined in the database function
    AND var.abbrev = 'HI_CONT'
;

create or replace function master.formula_hi_cont(
                grnwt_bm_cont float, 
                totbm_cont float
            ) returns float as
            $body$
            declare
                hi_cont float;
                local_grnwt_bm_cont float;
                local_totbm_cont float;
            begin
                
hi_cont = grnwt_bm_count/totbm_count;
                
                return round(hi_cont::numeric, 3);
            end
            $body$
            language plpgsql;

select master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('HI_CONT');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'HI_CONT = GRNWT_BM_CONT / TOTBM_CONT');
--rollback DELETE FROM master.formula WHERE formula IN ('HI_CONT = GRNWT_BM_CONT / TOTBM_CONT');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('HI_CONT_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('HI_CONT');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('HI_CONT_SCALE');
--rollback DROP FUNCTION master.formula_hi_cont;



--changeset postgres:add_MISS_HILL_CONT_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert MISS_HILL_CONT Formula



-- MISS_HILL_CONT
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('MISS_HILL_CONT', 'MISS HILL', 'MISS HILL', 'integer', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Missing hill', 'active', '1', 'Missing hill','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'MISS_HILL_CONT' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('MISS_HILL_CONT', 'MISS HILL', 'MISS HILL') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('MISS_HILL_CONT_METHOD', 'MISS HILL method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('MISS_HILL_CONT_SCALE', 'MISS HILL scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('MISS_HILL_CONT = TOTAL_HILL_CONT - HVHILL_CONT', (SELECT id FROM master.variable WHERE abbrev = 'MISS_HILL_CONT'), (SELECT id FROM master.method WHERE abbrev = 'MISS_HILL_CONT_METHOD' limit 1), 'plot', 'master.formula_miss_hill_cont(total_hill_cont, hvhill_cont)', NULL, 'create or replace function master.formula_miss_hill_cont(
                total_hill_cont integer,
                hvhill_cont integer
            ) returns float as
            $body$
            declare
                miss_hill_cont float;
            begin
                miss_hill_cont = total_hill_cont - hvhill_cont;
                
                return miss_hill_cont;
            end
            $body$
            language plpgsql;', NULL, '6', '2014-09-15 15:05:10');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('TOTAL_HILL_CONT', 'HVHILL_CONT') -- must be the same order as defined in the database function
    AND var.abbrev = 'MISS_HILL_CONT'
;

create or replace function master.formula_miss_hill_cont(
                total_hill_cont integer,
                hvhill_cont integer
            ) returns float as
            $body$
            declare
                miss_hill_cont float;
            begin
                miss_hill_cont = total_hill_cont - hvhill_cont;
                
                return miss_hill_cont;
            end
            $body$
            language plpgsql;

select master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('MISS_HILL_CONT');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'MISS_HILL_CONT = TOTAL_HILL_CONT - HVHILL_CONT');
--rollback DELETE FROM master.formula WHERE formula IN ('MISS_HILL_CONT = TOTAL_HILL_CONT - HVHILL_CONT');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('MISS_HILL_CONT_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('MISS_HILL_CONT');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('MISS_HILL_CONT_SCALE');
--rollback DROP FUNCTION master.formula_miss_hill_cont;



--changeset postgres:add_STRWWT_CONT_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert STRWWT_CONT Formula



-- STRWWT_CONT
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('STRWWT_CONT', 'STRWWT GM', 'STRWWT GM', 'integer', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Straw weight', 'active', '1', 'Straw weight','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'STRWWT_CONT' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('STRWWT_CONT', 'STRWWT GM', 'STRWWT GM') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('STRWWT_CONT_METHOD', 'STRWWT GM method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('STRWWT_CONT_SCALE', 'STRWWT GM scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id)
    VALUES ('STRWWT_CONT = TOTBM_CONT - GRNWT_BM_CONT', (SELECT id FROM master.variable WHERE abbrev = 'STRWWT_CONT'), (SELECT id FROM master.method WHERE abbrev = 'STRWWT_CONT_METHOD' limit 1), 'plot', 'master.formula_strwwt_cont(totbm_cont, grnwt_bm_cont)', NULL, 'create or replace function master.formula_strwwt_cont(
                totbm_cont integer,
                grnwt_bm_cont integer
            ) returns integer as
            $body$
            declare
               strwwt_cont integer;
            begin
                strwwt_cont = totbm_cont - grnwt_bm_cont;
                
                return strwwt_cont;
            end
            $body$
            language plpgsql;', NULL, '1');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('TOTBM_CONT', 'GRNWT_BM_CONT') -- must be the same order as defined in the database function
    AND var.abbrev = 'STRWWT_CONT'
;

create or replace function master.formula_strwwt_cont(
                totbm_cont integer,
                grnwt_bm_cont integer
            ) returns integer as
            $body$
            declare
               strwwt_cont integer;
            begin
                strwwt_cont = totbm_cont - grnwt_bm_cont;
                
                return strwwt_cont;
            end
            $body$
            language plpgsql;

select master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('STRWWT_CONT');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'STRWWT_CONT = TOTBM_CONT - GRNWT_BM_CONT');
--rollback DELETE FROM master.formula WHERE formula IN ('STRWWT_CONT = TOTBM_CONT - GRNWT_BM_CONT');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('STRWWT_CONT_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('STRWWT_CONT');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('STRWWT_CONT_SCALE');
--rollback DROP FUNCTION master.formula_strwwt_cont;



--changeset postgres:add_STRWWTBM_CONT_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert STRWWTBM_CONT Formula



-- STRWWTBM_CONT
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('STRWWTBM_CONT', 'STRWWT BM G/SQM', 'STRWWT BM G/SQM', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Straw weight biomass in g/sqm', 'active', '1', 'Straw weight biomass in g/sqm','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'STRWWTBM_CONT' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('STRWWTBM_CONT', 'STRWWT BM G/SQM', 'STRWWT BM G/SQM') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('STRWWTBM_CONT_METHOD', 'STRWWT BM G/SQM method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('STRWWTBM_CONT_SCALE', 'STRWWT BM G/SQM scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id)
    VALUES ('STRWWTBM_CONT=TOTBM_CONT1-GRNWTBM_CONT', (SELECT id FROM master.variable WHERE abbrev = 'STRWWTBM_CONT'), (SELECT id FROM master.method WHERE abbrev = 'STRWWTBM_CONT_METHOD' limit 1), 'plot', 'master.formula_strwwtbm_cont(totbm_cont1, grnwtbm_cont)', NULL, 'create or replace function master.formula_strwwtbm_cont(
                totbm_cont1 float,
                grnwtbm_cont float
            ) returns integer as
            $body$
            declare
               strwwtbm_cont integer;
            begin
                strwwtbm_cont = totbm_cont - grnwt_bm_cont;
                
                return round(strwwtbm_cont::numeric, 3);
            end
            $body$
            language plpgsql;', '3', '1');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('TOTBM_CONT1', 'GRNWTBM_CONT') -- must be the same order as defined in the database function
    AND var.abbrev = 'STRWWTBM_CONT'
;

create or replace function master.formula_strwwtbm_cont(
                totbm_cont1 float,
                grnwtbm_cont float
            ) returns integer as
            $body$
            declare
               strwwtbm_cont integer;
            begin
                strwwtbm_cont = totbm_cont - grnwt_bm_cont;
                
                return round(strwwtbm_cont::numeric, 3);
            end
            $body$
            language plpgsql;

select master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('STRWWTBM_CONT');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'STRWWTBM_CONT=TOTBM_CONT1-GRNWTBM_CONT');
--rollback DELETE FROM master.formula WHERE formula IN ('STRWWTBM_CONT=TOTBM_CONT1-GRNWTBM_CONT');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('STRWWTBM_CONT_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('STRWWTBM_CONT');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('STRWWTBM_CONT_SCALE');
--rollback DROP FUNCTION master.formula_strwwtbm_cont;



--changeset postgres:add_TOTBM_CONT_KG_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert TOTBM_CONT_KG Formula



-- TOTBM_CONT_KG
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('TOTBM_CONT_KG', 'TOTBM_KG', 'TOTBM_KG', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Total biomass in Kg', 'active', '1', 'Total biomass in Kg','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'TOTBM_CONT_KG' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('TOTBM_CONT_KG', 'TOTBM_KG', 'TOTBM_KG') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('TOTBM_CONT_KG_METHOD', 'TOTBM_KG method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('TOTBM_CONT_KG_SCALE', 'TOTBM_KG scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id)
    VALUES ('TOTBM_CONT_KG = TOTBM_CONT / 1000.0', (SELECT id FROM master.variable WHERE abbrev = 'TOTBM_CONT_KG'), (SELECT id FROM master.method WHERE abbrev = 'TOTBM_CONT_KG_METHOD' limit 1), 'plot', 'master.formula_totbm_cont_kg(totbm_cont)', NULL, 'create or replace function master.formula_totbm_cont_kg(
                totbm_cont float
            ) returns float as
            $body$
            declare
               totbm_cont_kg float;
            begin
                totbm_cont_kg = totbm_cont / 1000.0;
                
                return round(totbm_cont_kg::numeric, 3);
            end
            $body$
            language plpgsql;', '3', '1');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('TOTBM_CONT') -- must be the same order as defined in the database function
    AND var.abbrev = 'TOTBM_CONT_KG'
;

create or replace function master.formula_totbm_cont_kg(
                totbm_cont float
            ) returns float as
            $body$
            declare
               totbm_cont_kg float;
            begin
                totbm_cont_kg = totbm_cont / 1000.0;
                
                return round(totbm_cont::numeric, 3);
            end
            $body$
            language plpgsql;

select master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('TOTBM_CONT_KG');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'TOTBM_CONT_KG = TOTBM_CONT / 1000.0');
--rollback DELETE FROM master.formula WHERE formula IN ('TOTBM_CONT_KG = TOTBM_CONT / 1000.0');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('TOTBM_CONT_KG_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('TOTBM_CONT_KG');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('TOTBM_CONT_KG_SCALE');
--rollback DROP FUNCTION master.formula_totbm_cont_kg;



--changeset postgres:add_TOTBM_CONT1_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert TOTBM_CONT1 Formula



-- TOTBM_CONT1
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('TOTBM_CONT1', 'TOTAL BM G/SQM', 'TOTAL BM G/SQM', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Total biomass in g/sqm', 'active', '1', 'Total biomass in g/sqm','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'TOTBM_CONT1' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('TOTBM_CONT1', 'TOTAL BM G/SQM', 'TOTAL BM G/SQM') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('TOTBM_CONT1_METHOD', 'TOTAL BM G/SQM method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('TOTBM_CONT1_SCALE', 'TOTAL BM G/SQM scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id)
    VALUES ('TOTBM_CONT1=STRWWTBM_CONT+GRNWTBM_CONT', (SELECT id FROM master.variable WHERE abbrev = 'TOTBM_CONT1'), (SELECT id FROM master.method WHERE abbrev = 'TOTBM_CONT1_METHOD' limit 1), 'plot', 'master.formula_totbm_cont1(strwwtbm_cont, grnwtbm_cont)', NULL, 'create or replace function master.formula_totbm_cont1(
                strwwtbm_cont float, 
                grnwtbm_cont float
            ) returns float as
            $body$
            declare
               totbm_cont1 float;
            begin
                totbm_cont1 = strwwtbm_cont + grnwtbm_cont;
                
                return round(totbm_cont1::numeric, 3);
            end
            $body$
            language plpgsql;', '3', '1');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('STRWWTBM_CONT','GRNWTBM_CONT') -- must be the same order as defined in the database function
    AND var.abbrev = 'TOTBM_CONT1'
;

create or replace function master.formula_totbm_cont1(
                strwwtbm_cont float, 
                grnwtbm_cont float
            ) returns float as
            $body$
            declare
               totbm_cont1 float;
            begin
                totbm_cont1 = strwwtbm_cont + grnwtbm_cont;
                
                return round(totbm_cont1::numeric, 3);
            end
            $body$
            language plpgsql;

select master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('TOTBM_CONT1');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'TOTBM_CONT1=STRWWTBM_CONT+GRNWTBM_CONT');
--rollback DELETE FROM master.formula WHERE formula IN ('TOTBM_CONT1=STRWWTBM_CONT+GRNWTBM_CONT');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('TOTBM_CONT1_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('TOTBM_CONT1');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('TOTBM_CONT1_SCALE');
--rollback DROP FUNCTION master.formula_totbm_cont1;



--changeset postgres:add_YLD_0_CONT1_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert YLD_0_CONT1 Formula



-- YLD_0_CONT1
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('YLD_0_CONT1', 'YLD_0_CONT1', 'YLD_0_CONT1', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Plot yield in kg/ha without MC', 'active', '1', 'Plot yield in kg/ha without MC','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'YLD_0_CONT1' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('YLD_0_CONT1', 'YLD_0_CONT1', 'YLD_0_CONT1') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('YLD_0_CONT1_METHOD', 'YLD_0_CONT1 method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('YLD_0_CONT1_SCALE', 'YLD_0_CONT1 scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('YLD_0_CONT1 = AYLD_CONT * 10 / ADJHVAREA_CONT1', (SELECT id FROM master.variable WHERE abbrev = 'YLD_0_CONT1'), (SELECT id FROM master.method WHERE abbrev = 'YLD_0_CONT1_METHOD' limit 1), 'plot', 'master.formula_yld_0_cont1(ayld_cont, adjhvarea_cont1)', NULL, 'create or replace function master.formula_yld_0_cont1(
                ayld_cont float,
                adjhvarea_cont1 float
            ) returns float as
            $body$
            declare
                yld_0_cont1 float;
            begin
                yld_0_cont1 = ayld_cont * 10 / adjhvarea_cont1;
                
                return yld_0_cont1;
            end
            $body$
            language plpgsql;', NULL, '6', '2014-10-05 03:37:42.013231');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('AYLD_CONT','ADJHVAREA_CONT1') -- must be the same order as defined in the database function
    AND var.abbrev = 'YLD_0_CONT1'
;

create or replace function master.formula_yld_0_cont1(
                ayld_cont float,
                adjhvarea_cont1 float
            ) returns float as
            $body$
            declare
                yld_0_cont1 float;
            begin
                yld_0_cont1 = ayld_cont * 10 / adjhvarea_cont1;
                
                return yld_0_cont1;
            end
            $body$
            language plpgsql;

select master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('YLD_0_CONT1');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'YLD_0_CONT1 = AYLD_CONT * 10 / ADJHVAREA_CONT1');
--rollback DELETE FROM master.formula WHERE formula IN ('YLD_0_CONT1 = AYLD_CONT * 10 / ADJHVAREA_CONT1');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('YLD_0_CONT1_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('YLD_0_CONT1');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('YLD_0_CONT1_SCALE');
--rollback DROP FUNCTION master.formula_yld_0_cont1;



--changeset postgres:add_YLD_0_CONT2_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert YLD_0_CONT2 Formula



-- YLD_0_CONT2
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('YLD_0_CONT2', 'YLD_0_CONT2', 'YLD_0_CONT2', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Plot yield in kg/ha computed using EPS and without moisture', 'active', '1', 'Plot yield in kg/ha computed using EPS and without moisture','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'YLD_0_CONT2' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('YLD_0_CONT2', 'YLD_0_CONT2', 'YLD_0_CONT2') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('YLD_0_CONT2_METHOD', 'YLD_0_CONT2 method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('YLD_0_CONT2_SCALE', 'YLD_0_CONT2 scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('YLD_0_CONT2 = AYLD_CONT * 10 / HV_AREA_SQM', (SELECT id FROM master.variable WHERE abbrev = 'YLD_0_CONT2'), (SELECT id FROM master.method WHERE abbrev = 'YLD_0_CONT2_METHOD' limit 1), 'plot', 'master.formula_yld_0_cont2(ayld_cont, hv_area_sqm)', NULL, 'create or replace function master.formula_yld_0_cont2(
                ayld_cont float,
                hv_area_sqm float
            ) returns float as
            $body$
            declare
                yld_0_cont2 float;
            begin
                yld_0_cont2 = ayld_cont * 10 / hv_area_sqm;
                
                return yld_0_cont2;
            end
            $body$
            language plpgsql;', NULL, '19', '2014-10-16 01:52:05.393105');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('AYLD_CONT','HV_AREA_SQM') -- must be the same order as defined in the database function
    AND var.abbrev = 'YLD_0_CONT2'
;

create or replace function master.formula_yld_0_cont2(
                ayld_cont float,
                hv_area_sqm float
            ) returns float as
            $body$
            declare
                yld_0_cont2 float;
            begin
                yld_0_cont2 = ayld_cont * 10 / hv_area_sqm;
                
                return yld_0_cont2;
            end
            $body$
            language plpgsql;

select master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('YLD_0_CONT2');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'YLD_0_CONT1 = AYLD_CONT * 10 / ADJHVAREA_CONT1');
--rollback DELETE FROM master.formula WHERE formula IN ('YLD_0_CONT2 = AYLD_CONT * 10 / HV_AREA_SQM');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('YLD_0_CONT2_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('YLD_0_CONT2');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('YLD_0_CONT2_SCALE');
--rollback DROP FUNCTION master.formula_yld_0_cont2;



--changeset postgres:add_YLD_0_CONT3_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert YLD_0_CONT3 Formula



-- YLD_0_CONT3
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('YLD_0_CONT3', 'GYKGPHA in kg/ha', 'GYKGPHA in kg/ha', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'GYKGPHA in kg/ha(Direct Seeded)', 'active', '1', 'GYKGPHA in kg/ha(Direct Seeded)','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'YLD_0_CONT3' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('YLD_0_CONT3', 'GYKGPHA in kg/ha', 'GYKGPHA in kg/ha') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('YLD_0_CONT3_METHOD', 'GYKGPHA in kg/ha method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('YLD_0_CONT3_SCALE', 'GYKGPHA in kg/ha scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id)
    VALUES ('YLD_0_CONT3 = AYLD_CONT * 10 / ADJHVAREA_CONT2', (SELECT id FROM master.variable WHERE abbrev = 'YLD_0_CONT3'), (SELECT id FROM master.method WHERE abbrev = 'YLD_0_CONT3_METHOD' limit 1), 'plot', 'master.formula_yld_0_cont3(ayld_cont, adjhvarea_cont2)', NULL, 'create or replace function master.formula_yld_0_cont3(
                ayld_cont float,
                adjhvarea_cont2 float
            ) returns float as
            $body$
            declare
                yld_0_cont3 float;
            begin
                yld_0_cont3 = ayld_cont * 10 / adjhvarea_cont2;
                
                return yld_0_cont3;
            end
            $body$
            language plpgsql;', NULL, '1');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('AYLD_CONT','ADJHVAREA_CONT2') -- must be the same order as defined in the database function
    AND var.abbrev = 'YLD_0_CONT3'
;

create or replace function master.formula_yld_0_cont3(
                ayld_cont float,
                adjhvarea_cont2 float
            ) returns float as
            $body$
            declare
                yld_0_cont3 float;
            begin
                yld_0_cont3 = ayld_cont * 10 / adjhvarea_cont2;
                
                return yld_0_cont3;
            end
            $body$
            language plpgsql;

select master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('YLD_0_CONT3');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'YLD_0_CONT3 = AYLD_CONT * 10 / ADJHVAREA_CONT2');
--rollback DELETE FROM master.formula WHERE formula IN ('YLD_0_CONT3 = AYLD_CONT * 10 / ADJHVAREA_CONT2');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('YLD_0_CONT3_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('YLD_0_CONT3');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('YLD_0_CONT3_SCALE');
--rollback DROP FUNCTION master.formula_yld_0_cont3;



--changeset postgres:add_YLD_0_CONT4_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert YLD_0_CONT4 Formula



-- YLD_0_CONT4
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('YLD_0_CONT4', 'GYKGPHA in kg/ha with TPL used in DRT & RFSA', 'GYKGPHA in kg/ha with TPL used in DRT & RFSA', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'GYKGPHA in kg/ha without MC but with TPL used by DRT and RFSA', 'active', '1', 'GYKGPHA in kg/ha without MC but with TPL used by DRT and RFSA','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'YLD_0_CONT4' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('YLD_0_CONT4', 'GYKGPHA in kg/ha with TPL used in DRT & RFSA', 'GYKGPHA in kg/ha with TPL used in DRT & RFSA') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('YLD_0_CONT4_METHOD', 'GYKGPHA in kg/ha with TPL used in DRT & RFSA method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('YLD_0_CONT4_SCALE', 'GYKGPHA in kg/ha with TPL used in DRT & RFSA scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id)
    VALUES ('YLD_0_CONT4 = TPLYLD_CONT_PS*10/ADJHVAREA_CONT1', (SELECT id FROM master.variable WHERE abbrev = 'YLD_0_CONT4'), (SELECT id FROM master.method WHERE abbrev = 'YLD_0_CONT4_METHOD' limit 1), 'plot', 'master.formula_yld_0_cont4(tplyld_cont_ps, adjhvarea_cont1)', NULL, 'create or replace function master.formula_yld_0_cont4(
                tplyld_cont_ps float, 
                adjhvarea_cont1 float
            ) returns float as
            $body$
            declare
                yld_0_cont4 float;
            begin
                yld_0_cont4 = tplyld_cont_ps * 10 / adjhvarea_cont1;
                
                return yld_0_cont4;
            end
            $body$
            language plpgsql;', NULL, '1');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('TPLYLD_CONT_PS','ADJHVAREA_CONT1') -- must be the same order as defined in the database function
    AND var.abbrev = 'YLD_0_CONT4'
;

create or replace function master.formula_yld_0_cont4(
                tplyld_cont_ps float, 
                adjhvarea_cont1 float
            ) returns float as
            $body$
            declare
                yld_0_cont4 float;
            begin
                yld_0_cont4 = tplyld_cont_ps * 10 / adjhvarea_cont1;
                
                return yld_0_cont4;
            end
            $body$
            language plpgsql;

select master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('YLD_0_CONT4');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'YLD_0_CONT4 = TPLYLD_CONT_PS*10/ADJHVAREA_CONT1');
--rollback DELETE FROM master.formula WHERE formula IN ('YLD_0_CONT4 = TPLYLD_CONT_PS*10/ADJHVAREA_CONT1');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('YLD_0_CONT4_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('YLD_0_CONT4');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('YLD_0_CONT4_SCALE');
--rollback DROP FUNCTION master.formula_yld_0_cont4;



--changeset postgres:add_YLD_0_CONT5_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert YLD_0_CONT5 Formula



-- YLD_0_CONT5
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('YLD_0_CONT5', 'GYKGPHA in kg/ha with TPL used in DRT & RFSA', 'GYKGPHA in kg/ha with TPL used in DRT & RFSA', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'GYKGPHA in kg/ha without MC but with TPL using different formula used by DRT and RFSA', 'active', '1', 'GYKGPHA in kg/ha without MC but with TPL using different formula used by DRT and RFSA','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'YLD_0_CONT5' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('YLD_0_CONT5', 'GYKGPHA in kg/ha with TPL used in DRT & RFSA', 'GYKGPHA in kg/ha with TPL used in DRT & RFSA') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('YLD_0_CONT5_METHOD', 'GYKGPHA in kg/ha with TPL used in DRT & RFSA method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('YLD_0_CONT5_SCALE', 'GYKGPHA in kg/ha with TPL used in DRT & RFSA scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id)
    VALUES ('YLD_0_CONT5 = TPLYLD_CONT_PS *10/HV_AREA_SQM', (SELECT id FROM master.variable WHERE abbrev = 'YLD_0_CONT5'), (SELECT id FROM master.method WHERE abbrev = 'YLD_0_CONT5_METHOD' limit 1), 'plot', 'master.formula_yld_0_cont5(tplyld_cont_ps, hv_area_sqm)', NULL, 'create or replace function master.formula_yld_0_cont5(
                tplyld_cont_ps float, 
                hv_area_sqm float
            ) returns float as
            $body$
            declare
                yld_0_cont5 float;
            begin
                yld_0_cont5 = tplyld_cont_ps * 10 / hv_area_sqm;
                
                return yld_0_cont5;
            end
            $body$
            language plpgsql;', NULL, '1');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('TPLYLD_CONT_PS','HV_AREA_SQM') -- must be the same order as defined in the database function
    AND var.abbrev = 'YLD_0_CONT5'
;

create or replace function master.formula_yld_0_cont5(
                tplyld_cont_ps float, 
                hv_area_sqm float
            ) returns float as
            $body$
            declare
                yld_0_cont5 float;
            begin
                yld_0_cont5 = tplyld_cont_ps * 10 / hv_area_sqm;
                
                return yld_0_cont5;
            end
            $body$
            language plpgsql;

select master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('YLD_0_CONT5');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'YLD_0_CONT5 = TPLYLD_CONT_PS *10/HV_AREA_SQM');
--rollback DELETE FROM master.formula WHERE formula IN ('YLD_0_CONT5 = TPLYLD_CONT_PS *10/HV_AREA_SQM');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('YLD_0_CONT5_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('YLD_0_CONT5');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('YLD_0_CONT5_SCALE');
--rollback DROP FUNCTION master.formula_yld_0_cont5;



--changeset postgres:add_YLD_0_CONT6_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert YLD_0_CONT6 Formula



-- YLD_0_CONT6
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('YLD_0_CONT6', 'GYKGPHA6', 'GYKGPHA6', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'GYKGPHA in kg/ha without MC in DSR used by DRT', 'active', '1', 'GYKGPHA in kg/ha without MC in DSR used by DRT','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'YLD_0_CONT6' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('YLD_0_CONT6', 'GYKGPHA6', 'GYKGPHA6') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('YLD_0_CONT6_METHOD', 'GYKGPHA6 method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('YLD_0_CONT6_SCALE', 'GYKGPHA6 scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id)
    VALUES ('YLD_0_CONT6=(AYLD_CONT/ADJHVAREA_CONT3) * 10', (SELECT id FROM master.variable WHERE abbrev = 'YLD_0_CONT6'), (SELECT id FROM master.method WHERE abbrev = 'YLD_0_CONT6_METHOD' limit 1), 'plot', 'master.formula_yld_0_cont6(ayld_cont, adjhvarea_cont3)', NULL, 'create or replace function master.formula_yld_0_cont6(
                ayld_cont float, 
                adjhvarea_cont3 float 
            ) returns float as
            $body$
            declare
                yld_0_cont6 float;
            begin
                yld_0_cont6 = (ayld_cont / adjhvarea_cont3) * 10;
                
                return yld_0_cont6;
            end
            $body$
            language plpgsql;', NULL, '1');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('AYLD_CONT','ADJHVAREA_CONT3') -- must be the same order as defined in the database function
    AND var.abbrev = 'YLD_0_CONT6'
;

create or replace function master.formula_yld_0_cont6(
                ayld_cont float, 
                adjhvarea_cont3 float 
            ) returns float as
            $body$
            declare
                yld_0_cont6 float;
            begin
                yld_0_cont6 = (ayld_cont / adjhvarea_cont3) * 10;
                
                return yld_0_cont6;
            end
            $body$
            language plpgsql;

select master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('YLD_0_CONT6');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'YLD_0_CONT6=(AYLD_CONT/ADJHVAREA_CONT3) * 10');
--rollback DELETE FROM master.formula WHERE formula IN ('YLD_0_CONT6=(AYLD_CONT/ADJHVAREA_CONT3) * 10');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('YLD_0_CONT6_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('YLD_0_CONT6');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('YLD_0_CONT6_SCALE');
--rollback DROP FUNCTION master.formula_yld_0_cont6;



--changeset postgres:add_YLD_0_CONT7_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert YLD_0_CONT7 Formula



-- YLD_0_CONT7
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('YLD_0_CONT7', 'GYKGPHA in kg/ha with TPL used in DSR', 'GYKGPHA in kg/ha with TPL used in DSR', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'GYKGPHA in kg/ha without MC but with TPL used in DSR', 'active', '1', 'GYKGPHA in kg/ha without MC but with TPL used in DSR','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'YLD_0_CONT7' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('YLD_0_CONT7', 'GYKGPHA in kg/ha with TPL used in DSR', 'GYKGPHA in kg/ha with TPL used in DSR') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('YLD_0_CONT7_METHOD', 'GYKGPHA in kg/ha with TPL used in DSR method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('YLD_0_CONT7_SCALE', 'GYKGPHA in kg/ha with TPL used in DSR scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id)
    VALUES ('YLD_0_CONT7=TPLYLD_CONT_PS *10/ADJHVAREA_CONT3', (SELECT id FROM master.variable WHERE abbrev = 'YLD_0_CONT7'), (SELECT id FROM master.method WHERE abbrev = 'YLD_0_CONT7_METHOD' limit 1), 'plot', 'master.formula_yld_0_cont7(tplyld_cont_ps, adjhvarea_cont3)', NULL, 'create or replace function master.formula_yld_0_cont7(
                tplyld_cont_ps float, 
                adjhvarea_cont3 float 
            ) returns float as
            $body$
            declare
                yld_0_cont7 float;
            begin
                yld_0_cont7 = tplyld_cont_ps * 10 / adjhvarea_cont3;
                
                return yld_0_cont7;
            end
            $body$
            language plpgsql;', NULL, '1');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('AYLD_CONT','ADJHVAREA_CONT3') -- must be the same order as defined in the database function
    AND var.abbrev = 'YLD_0_CONT7'
;

create or replace function master.formula_yld_0_cont7(
                tplyld_cont_ps float, 
                adjhvarea_cont3 float 
            ) returns float as
            $body$
            declare
                yld_0_cont7 float;
            begin
                yld_0_cont7 = tplyld_cont_ps * 10 / adjhvarea_cont3;
                
                return yld_0_cont7;
            end
            $body$
            language plpgsql;

select master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('YLD_0_CONT7');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'YLD_0_CONT7=TPLYLD_CONT_PS *10/ADJHVAREA_CONT3');
--rollback DELETE FROM master.formula WHERE formula IN ('YLD_0_CONT7=TPLYLD_CONT_PS *10/ADJHVAREA_CONT3');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('YLD_0_CONT7_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('YLD_0_CONT7');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('YLD_0_CONT7_SCALE');
--rollback DROP FUNCTION master.formula_yld_0_cont7;



--changeset postgres:add_YLD_CONT_TON2_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert YLD_CONT_TON2 Formula



-- YLD_CONT_TON2
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('YLD_CONT_TON2', 'YLD_TON_MET', 'YLD_TON_MET', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Plot yield in ton/ha for MET', 'active', '1', 'Plot yield in ton/ha for MET','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'YLD_CONT_TON2' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('YLD_CONT_TON2', 'YLD_TON_MET', 'YLD_TON_MET') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('YLD_CONT_TON2_METHOD', 'YLD_TON_MET method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('YLD_CONT_TON2_SCALE', 'YLD_TON_MET scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('YLD_CONT_TON2 = ADJYLD3_CONT / 1000', (SELECT id FROM master.variable WHERE abbrev = 'YLD_CONT_TON2'), (SELECT id FROM master.method WHERE abbrev = 'YLD_CONT_TON2_METHOD' limit 1), 'plot', 'master.formula_yld_cont_ton2(adjyld3_cont)', NULL, 'create or replace function master.formula_yld_cont_ton2(
                adjyld3_cont float
            ) returns float as
            $body$
            declare
                yld_cont_ton2 float;
              local_adjyld3_cont float;
            begin
                
yld_cont_ton2 = adjyld3_cont / 1000;
                
                return round(yld_cont_ton2::numeric, 2);
            end
            $body$
            language plpgsql;', '2', '19', '2014-12-02 01:11:01.254032');    

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('ADJYLD3_CONT') -- must be the same order as defined in the database function
    AND var.abbrev = 'YLD_CONT_TON2'
;

create or replace function master.formula_yld_cont_ton2(
                adjyld3_cont float
            ) returns float as
            $body$
            declare
                yld_cont_ton2 float;
              local_adjyld3_cont float;
            begin
                
yld_cont_ton2 = adjyld3_cont / 1000;
                
                return round(yld_cont_ton2::numeric, 2);
            end
            $body$
            language plpgsql;

select master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('YLD_CONT_TON2');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'YLD_CONT_TON2 = ADJYLD3_CONT / 1000');
--rollback DELETE FROM master.formula WHERE formula IN ('YLD_CONT_TON2 = ADJYLD3_CONT / 1000');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('YLD_CONT_TON2_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('YLD_CONT_TON2');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('YLD_CONT_TON2_SCALE');
--rollback DROP FUNCTION master.formula_yld_cont_ton2;



--changeset postgres:add_YLD_CONT_TON5_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert YLD_CONT_TON5 Formula



-- YLD_CONT_TON5
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('YLD_CONT_TON5', 'YLD_TON5', 'YLD_TON5', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Grain yield in ton/ha5', 'active', '1', 'Grain yield in ton/ha5','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'YLD_CONT_TON5' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('YLD_CONT_TON5', 'YLD_TON5', 'YLD_TON5') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('YLD_CONT_TON5_METHOD', 'YLD_TON5 method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('YLD_CONT_TON5_SCALE', 'YLD_TON5 scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('YLD_CONT_TON5 = ADJAYLD_KG_CONT * 10/HV_AREA_SQM', (SELECT id FROM master.variable WHERE abbrev = 'YLD_CONT_TON5'), (SELECT id FROM master.method WHERE abbrev = 'YLD_CONT_TON5_METHOD' limit 1), 'plot', 'master.formula_yld_cont_ton5(adjayld_kg_cont, hv_area_sqm)', NULL, 'create or replace function master.formula_yld_cont_ton5(
                adjayld_kg_cont float,
                hv_area_sqm float
            ) returns float as
            $body$
            declare
                yld_cont_ton5 float;
              local_adjayld_kg_cont float;
              local_hv_area_sqm float;
            begin
                
yld_cont_ton5 = adjayld_kg_cont * 10/hv_area_sqm;
                
                return round(yld_cont_ton5::numeric, 2);
            end
            $body$
            language plpgsql;', '2', '19', '2015-05-25 11:14:33.379237');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('ADJAYLD_KG_CONT','HV_AREA_SQM') -- must be the same order as defined in the database function
    AND var.abbrev = 'YLD_CONT_TON5'
;

create or replace function master.formula_yld_cont_ton5(
                adjayld_kg_cont float,
                hv_area_sqm float
            ) returns float as
            $body$
            declare
                yld_cont_ton5 float;
              local_adjayld_kg_cont float;
              local_hv_area_sqm float;
            begin
                
yld_cont_ton5 = adjayld_kg_cont * 10/hv_area_sqm;
                
                return round(yld_cont_ton5::numeric, 2);
            end
            $body$
            language plpgsql;     

select master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('YLD_CONT_TON5');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'YLD_CONT_TON5 = ADJAYLD_KG_CONT * 10/HV_AREA_SQM');
--rollback DELETE FROM master.formula WHERE formula IN ('YLD_CONT_TON5 = ADJAYLD_KG_CONT * 10/HV_AREA_SQM');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('YLD_CONT_TON5_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('YLD_CONT_TON5');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('YLD_CONT_TON5_SCALE');
--rollback DROP FUNCTION master.formula_yld_cont_ton5;



--changeset postgres:add_YLD_CONT_TON6_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert YLD_CONT_TON6 Formula



-- YLD_CONT_TON6
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('YLD_CONT_TON6', 'YLD_TON6', 'YLD_TON6', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Grain yield in ton/ha', 'active', '1', 'Grain yield in ton/ha','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'YLD_CONT_TON6' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('YLD_CONT_TON6', 'YLD_TON6', 'YLD_TON6') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('YLD_CONT_TON6_METHOD', 'YLD_TON6 method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('YLD_CONT_TON6_SCALE', 'YLD_TON6 scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('YLD_CONT_TON6  =  ADJAYLD_KG_CONT4 * 10/HV_AREA_SQM', (SELECT id FROM master.variable WHERE abbrev = 'YLD_CONT_TON6'), (SELECT id FROM master.method WHERE abbrev = 'YLD_CONT_TON6_METHOD'), 'plot', 'master.formula_yld_cont_ton6(adjayld_kg_cont4, hv_area_sqm)', NULL, 'create or replace function master.formula_yld_cont_ton6(
                adjayld_kg_cont4 float,
                hv_area_sqm float
            ) returns float as
            $body$
            declare
                yld_cont_ton6 float;
              local_adjayld_kg_cont4 float;
              local_hv_area_sqm float;
            begin
                
yld_cont_ton6  =  adjayld_kg_cont4 * 10/hv_area_sqm;
                
                return round(yld_cont_ton6::numeric, 2);
            end
            $body$
            language plpgsql;', '2', '18', '2016-06-14 17:03:56.263145');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('ADJAYLD_KG_CONT4','HV_AREA_SQM') -- must be the same order as defined in the database function
    AND var.abbrev = 'YLD_CONT_TON6'
;

create or replace function master.formula_yld_cont_ton6(
                adjayld_kg_cont4 float,
                hv_area_sqm float
            ) returns float as
            $body$
            declare
                yld_cont_ton6 float;
              local_adjayld_kg_cont4 float;
              local_hv_area_sqm float;
            begin
                
yld_cont_ton6  =  adjayld_kg_cont4 * 10/hv_area_sqm;
                
                return round(yld_cont_ton6::numeric, 2);
            end
            $body$
            language plpgsql;       

select master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('YLD_CONT_TON6');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'YLD_CONT_TON6  =  ADJAYLD_KG_CONT4 * 10/HV_AREA_SQM');
--rollback DELETE FROM master.formula WHERE formula IN ('YLD_CONT_TON6  =  ADJAYLD_KG_CONT4 * 10/HV_AREA_SQM');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('YLD_CONT_TON6_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('YLD_CONT_TON6');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('YLD_CONT_TON6_SCALE');
--rollback DROP FUNCTION master.formula_yld_cont_ton6;



--changeset postgres:add_YLD_CONT_TON7_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert YLD_CONT_TON7 Formula



-- YLD_CONT_TON7
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('YLD_CONT_TON7', 'YLD_TON7', 'YLD_TON7', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Grain Yield in ton/ha_for DSR', 'active', '1', 'Grain Yield in ton/ha_for DSR','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'YLD_CONT_TON7' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('YLD_CONT_TON7', 'YLD_TON7', 'YLD_TON7') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('YLD_CONT_TON7_METHOD', 'YLD_TON7 method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('YLD_CONT_TON7_SCALE', 'YLD_TON7 scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('YLD_CONT_TON7=ADJAYLD_KG_CONT*10/ADJHVAREA_CONT2', (SELECT id FROM master.variable WHERE abbrev = 'YLD_CONT_TON7'), (SELECT id FROM master.method WHERE abbrev = 'YLD_CONT_TON7_METHOD' limit 1), 'plot', 'master.formula_yld_cont_ton7(adjayld_kg_cont, adjhvarea_cont2)', NULL, 'create or replace function master.formula_yld_cont_ton7(
                adjayld_kg_cont float,
                adjhvarea_cont2 float
            ) returns float as
            $body$
            declare
                yld_cont_ton7 float;
              local_adjayld_kg_cont float;
              local_adjhvarea_cont2 float;
            begin
                
yld_cont_ton7=adjayld_kg_cont*10/adjhvarea_cont2;
                
                return round(yld_cont_ton7::numeric, 2);
            end
            $body$
            language plpgsql;', '2', '18', '2016-11-16 17:06:57.431355');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('ADJAYLD_KG_CONT','ADJHVAREA_CONT2') -- must be the same order as defined in the database function
    AND var.abbrev = 'YLD_CONT_TON7'
;

create or replace function master.formula_yld_cont_ton7(
                adjayld_kg_cont float,
                adjhvarea_cont2 float
            ) returns float as
            $body$
            declare
                yld_cont_ton7 float;
              local_adjayld_kg_cont float;
              local_adjhvarea_cont2 float;
            begin
                
yld_cont_ton7=adjayld_kg_cont*10/adjhvarea_cont2;
                
                return round(yld_cont_ton7::numeric, 2);
            end
            $body$
            language plpgsql;

select master.populate_order_number_for_master_formula_parameter();
       
       

--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('YLD_CONT_TON7');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'YLD_CONT_TON7=ADJAYLD_KG_CONT*10/ADJHVAREA_CONT2');
--rollback DELETE FROM master.formula WHERE formula IN ('YLD_CONT_TON7=ADJAYLD_KG_CONT*10/ADJHVAREA_CONT2');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('YLD_CONT_TON7_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('YLD_CONT_TON7');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('YLD_CONT_TON7_SCALE');
--rollback DROP FUNCTION master.formula_yld_cont_ton7;



--changeset postgres:add_YLD_CONT1_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3088 CB-DB: Insert YLD_CONT1 Formula



-- YLD_CONT1
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('YLD_CONT1', 'YLD_CONT1', 'YLD_CONT1', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Plot yield at 14% MC in kg/ha using adjusted harvest area', 'active', '1', 'Plot yield at 14% MC in kg/ha using adjusted harvest area','23.11')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'YLD_CONT1' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('YLD_CONT1', 'YLD_CONT1', 'YLD_CONT1') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('YLD_CONT1_METHOD', 'YLD_CONT1 method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('YLD_CONT1_SCALE', 'YLD_CONT1 scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('YLD_CONT1 = AYLD_CONT * MF_CONT * 10 / ADJHVAREA_CONT1', (SELECT id FROM master.variable WHERE abbrev = 'YLD_CONT1'), (SELECT id FROM master.method WHERE abbrev = 'YLD_CONT1_METHOD' limit 1), 'plot', 'master.formula_yld_cont1(ayld_cont, mf_cont, adjhvarea_cont1)', NULL, 'create or replace function master.formula_yld_cont1(
                ayld_cont float,
                mf_cont float,
                adjhvarea_cont1 float
            ) returns float as
            $body$
            declare
                yld_cont1 float;
            begin
                yld_cont1 = ayld_cont * mf_cont * 10 / adjhvarea_cont1;
                
                return round(yld_cont1::numeric, 4);
            end
            $body$
            language plpgsql;', '4', '6', '2014-09-18 13:05:25');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('AYLD_CONT','MF_CONT', 'ADJHVAREA_CONT1') -- must be the same order as defined in the database function
    AND var.abbrev = 'YLD_CONT1'
;

create or replace function master.formula_yld_cont1(
                ayld_cont float,
                mf_cont float,
                adjhvarea_cont1 float
            ) returns float as
            $body$
            declare
                yld_cont1 float;
            begin
                yld_cont1 = ayld_cont * mf_cont * 10 / adjhvarea_cont1;
                
                return round(yld_cont1::numeric, 4);
            end
            $body$
            language plpgsql; 

select master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('YLD_CONT1');
--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'YLD_CONT1 = AYLD_CONT * MF_CONT * 10 / ADJHVAREA_CONT1');
--rollback DELETE FROM master.formula WHERE formula IN ('YLD_CONT1 = AYLD_CONT * MF_CONT * 10 / ADJHVAREA_CONT1');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('YLD_CONT1_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('YLD_CONT1');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('YLD_CONT1_SCALE');
--rollback DROP FUNCTION master.formula_yld_cont1;