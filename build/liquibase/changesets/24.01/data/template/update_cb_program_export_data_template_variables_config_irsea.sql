--liquibase formatted sql

--changeset postgres:update_cb_program_export_data_template_variables_config_irsea context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6723 CB-EM: Request to add germplasm attribute information in the 'Export metadata and plot data' feature



UPDATE
    platform.config
SET
    config_value = '{
        "plot": [
            {
            "label": "Plot ID",
            "abbrev": "PLOT_ID",
            "required": "false",
            "attribute": "plotDbId",
            "is_selected": "true",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Plot Number",
            "abbrev": "PLOTNO",
            "required": "false",
            "attribute": "plotNumber",
            "is_selected": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Plot Code",
            "abbrev": "PLOT_CODE",
            "required": "false",
            "attribute": "plotCode",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Plot Type",
            "abbrev": "PLOT_TYPE",
            "required": "false",
            "attribute": "plotType",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Replication",
            "abbrev": "REP",
            "required": "false",
            "attribute": "rep",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Block Number",
            "abbrev": "BLOCK_NO_CONT",
            "required": "false",
            "attribute": "blockNumber",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "PA X",
            "abbrev": "PA_X",
            "required": "false",
            "attribute": "paX",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "PA Y",
            "abbrev": "PA_Y",
            "required": "false",
            "attribute": "paY",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            }
        ],
        "entry": [
            {
            "label": "Entry Number",
            "abbrev": "ENTRY_NUMBER",
            "required": "false",
            "attribute": "entryNumber",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Entry Code",
            "abbrev": "ENTRY_CODE",
            "required": "false",
            "attribute": "entryCode",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Entry Type",
            "abbrev": "ENTRY_TYPE",
            "required": "false",
            "attribute": "entryType",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Entry Class",
            "abbrev": "ENTRY_CLASS",
            "required": "false",
            "attribute": "entryClass",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Entry Role",
            "abbrev": "ENTRY_ROLE",
            "required": "false",
            "attribute": "entryRole",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            }
        ],
        "trait": [
            {
            "label": "Bl1 0-9",
            "abbrev": "BL_NURS_0_9",
            "required": "false",
            "attribute": "BL_NURS_0_9",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "YLD_CONT2",
            "abbrev": "YLD_CONT2",
            "required": "false",
            "attribute": "YLD_CONT2",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "YLD_TON",
            "abbrev": "YLD_CONT_TON",
            "required": "false",
            "attribute": "YLD_CONT_TON",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "TILLER_AVG",
            "abbrev": "TIL_AVE_CONT",
            "required": "false",
            "attribute": "TIL_AVE_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "NO OF SEED",
            "abbrev": "NO_OF_SEED",
            "required": "false",
            "attribute": "NO_OF_SEED",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Pacp 1-9",
            "abbrev": "PACP_SCOR_1_9",
            "required": "false",
            "attribute": "PACP_SCOR_1_9",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "FLW50",
            "abbrev": "FLW50",
            "required": "false",
            "attribute": "FLW50",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "CROSSING DATE",
            "abbrev": "DATE_CROSSED",
            "required": "false",
            "attribute": "DATE_CROSSED",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "TRANSFORMATION DATE",
            "abbrev": "TRANSFORMATION_DATE",
            "required": "false",
            "attribute": "TRANSFORMATION_DATE",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "HARVEST DATE",
            "abbrev": "HVDATE_CONT",
            "required": "false",
            "attribute": "HVDATE_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "RLODGINC CT PLNTPLOT",
            "abbrev": "RLODGINC_CT_PLNTPLOT",
            "required": "false",
            "attribute": "RLODGINC_CT_PLNTPLOT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "PSTANDHV CT PLNTPLOT",
            "abbrev": "PSTANDHV_CT_PLNTPLOT",
            "required": "false",
            "attribute": "PSTANDHV_CT_PLNTPLOT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "HV HILL",
            "abbrev": "HVHILL_CONT",
            "required": "false",
            "attribute": "HVHILL_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "TOTAL HILLS",
            "abbrev": "TOTAL_HILL_CONT",
            "required": "false",
            "attribute": "TOTAL_HILL_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "PSTANDTH CT PLNTPLOT",
            "abbrev": "PSTANDTH_CT_PLNTPLOT",
            "required": "false",
            "attribute": "PSTANDTH_CT_PLNTPLOT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "RLODGINC CMP PCT",
            "abbrev": "RLODGINC_CMP_PCT",
            "required": "false",
            "attribute": "RLODGINC_CMP_PCT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "ASI Trait",
            "abbrev": "ASI_TRAIT",
            "required": "false",
            "attribute": "ASI_TRAIT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Ht4",
            "abbrev": "HT4_CONT",
            "required": "false",
            "attribute": "HT4_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "CL5",
            "abbrev": "CML5_CONT",
            "required": "false",
            "attribute": "CML5_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "AYLD_G",
            "abbrev": "AYLD_CONT",
            "required": "false",
            "attribute": "AYLD_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Ht11",
            "abbrev": "HT11_CONT",
            "required": "false",
            "attribute": "HT11_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "HT_AVG",
            "abbrev": "HT_CONT",
            "required": "false",
            "attribute": "HT_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "PnL 5",
            "abbrev": "PNL5_CONT",
            "required": "false",
            "attribute": "PNL5_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "HT_AVG_ENT",
            "abbrev": "HT_CONT_ENT",
            "required": "false",
            "attribute": "HT_CONT_ENT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "HT",
            "abbrev": "HT_CAT",
            "required": "false",
            "attribute": "HT_CAT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Maturity date",
            "abbrev": "MAT_DATE_CONT",
            "required": "false",
            "attribute": "MAT_DATE_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Ht12",
            "abbrev": "HT12_CONT",
            "required": "false",
            "attribute": "HT12_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "BB GH 1-9 - SES5",
            "abbrev": "BB_SES5_GH_SCOR_1_9",
            "required": "false",
            "attribute": "BB_SES5_GH_SCOR_1_9",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "FLW DATE",
            "abbrev": "FLW_DATE_CONT",
            "required": "false",
            "attribute": "FLW_DATE_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Lg 1-9",
            "abbrev": "LG_SCOR_1_9",
            "required": "false",
            "attribute": "LG_SCOR_1_9",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Ht2",
            "abbrev": "HT2_CONT",
            "required": "false",
            "attribute": "HT2_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Grain Color",
            "abbrev": "GRAIN_COLOR",
            "required": "false",
            "attribute": "GRAIN_COLOR",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "FLW",
            "abbrev": "FLW_CONT",
            "required": "false",
            "attribute": "FLW_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "HT9",
            "abbrev": "HT9_CONT",
            "required": "false",
            "attribute": "HT9_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "HT8",
            "abbrev": "HT8_CONT",
            "required": "false",
            "attribute": "HT8_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "PnL 6",
            "abbrev": "PNL6_CONT",
            "required": "false",
            "attribute": "PNL6_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Ht6",
            "abbrev": "HT6_CONT",
            "required": "false",
            "attribute": "HT6_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Number of Ears Selected",
            "abbrev": "NO_OF_EARS",
            "required": "false",
            "attribute": "NO_OF_EARS",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "MC",
            "abbrev": "MC_CONT",
            "required": "false",
            "attribute": "MC_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Specific plant number selected",
            "abbrev": "SPECIFIC_PLANT",
            "required": "false",
            "attribute": "SPECIFIC_PLANT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "NO OF PLANTS SELECTED",
            "abbrev": "NO_OF_PLANTS",
            "required": "false",
            "attribute": "NO_OF_PLANTS",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "CL6",
            "abbrev": "CML6_CONT",
            "required": "false",
            "attribute": "CML6_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "HT10",
            "abbrev": "HT10_CONT",
            "required": "false",
            "attribute": "HT10_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "HT7",
            "abbrev": "HT7_CONT",
            "required": "false",
            "attribute": "HT7_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Ht5",
            "abbrev": "HT5_CONT",
            "required": "false",
            "attribute": "HT5_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "PANSEL",
            "abbrev": "PANNO_SEL",
            "required": "false",
            "attribute": "PANNO_SEL",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Ht3",
            "abbrev": "HT3_CONT",
            "required": "false",
            "attribute": "HT3_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Heterotic Group",
            "abbrev": "HETEROTIC_GROUP",
            "required": "false",
            "attribute": "HETEROTIC_GROUP",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Ht1",
            "abbrev": "HT1_CONT",
            "required": "false",
            "attribute": "HT1_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "HARV METH",
            "abbrev": "HV_METH_DISC",
            "required": "false",
            "attribute": "HV_METH_DISC",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            }
        ],
        "germplasm": [
            {
            "label": "Germplasm Code",
            "abbrev": "GERMPLASM_CODE",
            "required": "false",
            "attribute": "germplasmCode",
            "is_selected": "true",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Germplasm Name",
            "abbrev": "GERMPLASM_NAME",
            "required": "false",
            "attribute": "germplasmName",
            "is_selected": "true",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Germplasm State",
            "abbrev": "GERMPLASM_STATE",
            "required": "false",
            "attribute": "germplasmState",
            "is_selected": "true",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Taxonomy Name",
            "abbrev": "TAXONOMY_NAME",
            "required": "false",
            "attribute": "taxonomyName",
            "is_selected": "true",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Female Parent Germplasm Code",
            "abbrev": "GERMPLASM_CODE",
            "required": "false",
            "attribute": "femaleParentGermplasmCode",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Male Parent Germplasm Code",
            "abbrev": "GERMPLASM_CODE",
            "required": "false",
            "attribute": "maleParentGermplasmCode",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Parentage",
            "abbrev": "PARENTAGE",
            "required": "false",
            "attribute": "parentage",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Germplasm Type",
            "abbrev": "GERMPLASM_TYPE",
            "required": "false",
            "attribute": "germplasmType",
            "is_selected": "true",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Germplasm Name Type",
            "abbrev": "GERMPLASM_NAME_TYPE",
            "required": "false",
            "attribute": "germplasmNameType",
            "is_selected": "true",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "IP Status",
            "abbrev": "IP_STATUS",
            "required": "false",
            "attribute": "IP_STATUS",
            "is_selected": "true",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "MTA Status",
            "abbrev": "MTA_STATUS",
            "required": "false",
            "attribute": "MTA_STATUS",
            "is_selected": "true",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Germplasm Release Year",
            "abbrev": "GERMPLASM_RELEASE_YEAR",
            "required": "false",
            "attribute": "GERMPLASM_RELEASE_YEAR",
            "is_selected": "true",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Germplasm Year",
            "abbrev": "GERMPLASM_YEAR",
            "required": "false",
            "attribute": "GERMPLASM_YEAR",
            "is_selected": "true",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            }
        ],
        "experiment": [
            {
            "label": "Experiment Code",
            "abbrev": "EXPERIMENT_CODE",
            "required": "false",
            "attribute": "experimentCode",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Experiment Name",
            "abbrev": "EXPERIMENT_NAME",
            "required": "false",
            "attribute": "experimentName",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Experiment Year",
            "abbrev": "EXPERIMENT_YEAR",
            "required": "false",
            "attribute": "experimentYear",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Season",
            "abbrev": "SEASON",
            "required": "false",
            "attribute": "experimentSeason",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Stage Code",
            "abbrev": "STAGE_CODE",
            "required": "false",
            "attribute": "experimentStageCode",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Project",
            "abbrev": "PROJECT",
            "required": "false",
            "attribute": "experimentProject",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Pipeline",
            "abbrev": "PIPELINE",
            "required": "false",
            "attribute": "experimentPipeline",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Experiment Objective",
            "abbrev": "EXPERIMENT_OBJECTIVE",
            "required": "false",
            "attribute": "experimentObjective",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Experiment Type",
            "abbrev": "EXPERIMENT_TYPE",
            "required": "false",
            "attribute": "experimentType",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Experiment Subtype",
            "abbrev": "EXPERIMENT_SUB_TYPE",
            "required": "false",
            "attribute": "experimentSubtype",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Experiment Design Type",
            "abbrev": "EXPERIMENT_DESIGN_TYPE",
            "required": "false",
            "attribute": "experimentDesignType",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Experiment Status",
            "abbrev": "EXPERIMENT_STATUS",
            "required": "false",
            "attribute": "experimentStatus",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Experiment Steward",
            "abbrev": "EXPERIMENT_STEWARD",
            "required": "false",
            "attribute": "experimentSteward",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Planting Season",
            "abbrev": "PLANTING_SEASON",
            "required": "false",
            "attribute": "plantingSeason",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Market Segment",
            "abbrev": "MARKET_SEGMENT",
            "required": "false",
            "attribute": "MARKET_SEGMENT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Experiment Ecosystem",
            "abbrev": "ECOSYSTEM",
            "required": "false",
            "attribute": "EXPERIMENT_ECOSYSTEM",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Experiment Remarks",
            "abbrev": "REMARKS",
            "required": "false",
            "attribute": "experimentRemarks",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            }
        ],
        "occurrence": [
            {
            "label": "Occurrence Code",
            "abbrev": "OCCURRENCE_CODE",
            "required": "true",
            "attribute": "occurrenceCode",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Occurrence Name",
            "abbrev": "OCCURRENCE_NAME",
            "required": "false",
            "attribute": "occurrenceName",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Occurrence Number",
            "abbrev": "OCCURRENCE_NUMBER",
            "required": "false",
            "attribute": "occurrenceNumber",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Site Code",
            "abbrev": "SITE_CODE",
            "required": "true",
            "attribute": "siteCode",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Site Name",
            "abbrev": "SITE",
            "required": "false",
            "attribute": "siteName",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Location Code",
            "abbrev": "LOCATION_CODE",
            "required": "true",
            "attribute": "locationCode",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Location Name",
            "abbrev": "LOCATION_NAME",
            "required": "true",
            "attribute": "locationName",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Planting Date",
            "abbrev": "PLANTING_DATE",
            "required": "false",
            "attribute": "plantingDate",
            "is_selected": "false",
            "is_uploadable": "true",
            "should_retrieve_data_value": "true"
            },
            {
            "label": "Geospatial Coordinates",
            "abbrev": "GEOSPATIAL_COORDS",
            "required": "false",
            "attribute": "GEOSPATIAL_COORDS",
            "is_selected": "false",
            "is_uploadable": "true",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Longitude",
            "abbrev": "LONGITUDE",
            "required": "false",
            "attribute": "LONGITUDE",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Latitude",
            "abbrev": "LATITUDE",
            "required": "false",
            "attribute": "LATITUDE",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Occurrence Ecosystem",
            "abbrev": "ECOSYSTEM",
            "required": "false",
            "attribute": "OCCURRENCE_ECOSYSTEM",
            "is_selected": "true",
            "is_uploadable": "true",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Occurrence Description",
            "abbrev": "DESCRIPTION",
            "required": "false",
            "attribute": "occurrenceDescription",
            "is_selected": "true",
            "is_uploadable": "true",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Location Description",
            "abbrev": "DESCRIPTION",
            "required": "false",
            "attribute": "locationDescription",
            "is_selected": "true",
            "is_uploadable": "true",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Contact Person",
            "abbrev": "CONTCT_PERSON_CONT",
            "required": "false",
            "attribute": "CONTCT_PERSON_CONT",
            "is_selected": "false",
            "is_uploadable": "true",
            "should_retrieve_data_value": "false"
            }
        ],
        "planting_protocol": [
            {
            "label": "Crop Establishment",
            "abbrev": "ESTABLISHMENT",
            "required": "false",
            "attribute": "ESTABLISHMENT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Planting Type",
            "abbrev": "PLANTING_TYPE",
            "required": "false",
            "attribute": "PLANTING_TYPE",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Plot Type",
            "abbrev": "PLOT_TYPE",
            "required": "false",
            "attribute": "PLOT_TYPE",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "PLOT LN",
            "abbrev": "PLOT_LN",
            "required": "false",
            "attribute": "PLOT_LN",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Plot Width",
            "abbrev": "PLOT_WIDTH",
            "required": "false",
            "attribute": "PLOT_WIDTH",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Alley Length",
            "abbrev": "ALLEY_LENGTH",
            "required": "false",
            "attribute": "ALLEY_LENGTH",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Transplanting Date",
            "abbrev": "TRANS_DATE_CONT",
            "required": "false",
            "attribute": "TRANS_DATE_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Seeding Density",
            "abbrev": "SEEDING_RATE",
            "required": "false",
            "attribute": "SEEDING_RATE",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Planting Instructions",
            "abbrev": "PLANTING_INSTRUCTIONS",
            "required": "false",
            "attribute": "PLANTING_INSTRUCTIONS",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "BED WIDTH",
            "abbrev": "BED_WIDTH",
            "required": "false",
            "attribute": "BED_WIDTH",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "DISTANCE BET PLOTS IN M",
            "abbrev": "DISTANCE_BET_PLOTS_IN_M",
            "required": "false",
            "attribute": "DISTANCE_BET_PLOTS_IN_M",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "DIST BET HILLS",
            "abbrev": "DIST_BET_HILLS",
            "required": "false",
            "attribute": "DIST_BET_HILLS",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "DIST BET ROWS",
            "abbrev": "DIST_BET_ROWS",
            "required": "false",
            "attribute": "DIST_BET_ROWS",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "HILLS PER ROW CONT",
            "abbrev": "HILLS_PER_ROW_CONT",
            "required": "false",
            "attribute": "HILLS_PER_ROW_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "NO OF BEDS PER PLOT",
            "abbrev": "NO_OF_BEDS_PER_PLOT",
            "required": "false",
            "attribute": "NO_OF_BEDS_PER_PLOT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "PLOT AREA 1",
            "abbrev": "PLOT_AREA_1",
            "required": "false",
            "attribute": "PLOT_AREA_1",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "PLOT AREA 2",
            "abbrev": "PLOT_AREA_2",
            "required": "false",
            "attribute": "PLOT_AREA_2",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "PLOT AREA 13",
            "abbrev": "PLOT_AREA_3",
            "required": "false",
            "attribute": "PLOT_AREA_3",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "PLOT AREA 3",
            "abbrev": "PLOT_AREA_3",
            "required": "false",
            "attribute": "PLOT_AREA_3",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "PLOT LN 1",
            "abbrev": "PLOT_LN_1",
            "required": "false",
            "attribute": "PLOT_LN_1",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "PLOT WIDTH MAIZE BED",
            "abbrev": "PLOT_WIDTH_MAIZE_BED",
            "required": "false",
            "attribute": "PLOT_WIDTH_MAIZE_BED",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "PLOT WIDTH RICE",
            "abbrev": "PLOT_WIDTH_RICE",
            "required": "false",
            "attribute": "PLOT_WIDTH_RICE",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "PLOT WIDTH WHEAT FLAT",
            "abbrev": "PLOT_WIDTH_WHEAT_FLAT",
            "required": "false",
            "attribute": "PLOT_WIDTH_WHEAT_FLAT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "ROWS PER PLOT CONT",
            "abbrev": "ROWS_PER_PLOT_CONT",
            "required": "false",
            "attribute": "ROWS_PER_PLOT_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "PLOT AREA SQM CONT",
            "abbrev": "PLOT_AREA_SQM_CONT",
            "required": "false",
            "attribute": "PLOT_AREA_SQM_CONT",
            "is_selected": "true",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            }
        ],
        "management_protocol": [
            {
            "label": "Distance Between Rows",
            "abbrev": "DIST_BET_ROWS",
            "required": "false",
            "attribute": "DIST_BET_ROWS",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Rows per Plot",
            "abbrev": "ROWS_PER_PLOT_CONT",
            "required": "false",
            "attribute": "ROWS_PER_PLOT_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Seeding Date",
            "abbrev": "SEEDING_DATE_CONT",
            "required": "false",
            "attribute": "SEEDING_DATE_CONT",
            "is_selected": "false",
            "is_uploadable": "false",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Fertilizer Type - 1st Application",
            "abbrev": "FERT1_TYPE_DISC",
            "required": "false",
            "attribute": "FERT1_TYPE_DISC",
            "is_selected": "false",
            "is_uploadable": "true",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Fertilizer Date - 1st Application",
            "abbrev": "FERT1_DATE_CONT",
            "required": "false",
            "attribute": "FERT1_DATE_CONT",
            "is_selected": "false",
            "is_uploadable": "true",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Fertilizer Brand - 1st Application",
            "abbrev": "FERT1_BRAND",
            "required": "false",
            "attribute": "FERT1_BRAND",
            "is_selected": "false",
            "is_uploadable": "true",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Fertilizer Method - 1st Application",
            "abbrev": "FERT1_METH_DISC",
            "required": "false",
            "attribute": "FERT1_METH_DISC",
            "is_selected": "false",
            "is_uploadable": "true",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Fertilizer Type - 2nd Application",
            "abbrev": "FERT2_TYPE_DISC",
            "required": "false",
            "attribute": "FERT2_TYPE_DISC",
            "is_selected": "false",
            "is_uploadable": "true",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Fertilizer Date - 2nd Application",
            "abbrev": "FERT2_DATE_CONT",
            "required": "false",
            "attribute": "FERT2_DATE_CONT",
            "is_selected": "true",
            "is_uploadable": "true",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Fertilizer Brand - 2nd Application",
            "abbrev": "FERT2_BRAND",
            "required": "false",
            "attribute": "FERT2_BRAND",
            "is_selected": "false",
            "is_uploadable": "true",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Fertilizer Method - 2nd Application",
            "abbrev": "FERT2_METH_DISC",
            "required": "false",
            "attribute": "FERT2_METH_DISC",
            "is_selected": "false",
            "is_uploadable": "true",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Fertilizer Type - 3rd Application",
            "abbrev": "FERT3_TYPE_DISC",
            "required": "false",
            "attribute": "FERT3_TYPE_DISC",
            "is_selected": "false",
            "is_uploadable": "true",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Fertilizer Date - 3rd Application",
            "abbrev": "FERT3_DATE_CONT",
            "required": "false",
            "attribute": "FERT3_DATE_CONT",
            "is_selected": "false",
            "is_uploadable": "true",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Fertilizer Brand - 3rd Application",
            "abbrev": "FERT3_BRAND",
            "required": "false",
            "attribute": "FERT3_BRAND",
            "is_selected": "false",
            "is_uploadable": "true",
            "should_retrieve_data_value": "false"
            },
            {
            "label": "Fertilizer Method - 3rd Application",
            "abbrev": "FERT3_METH_DISC",
            "required": "false",
            "attribute": "FERT3_METH_DISC",
            "is_selected": "false",
            "is_uploadable": "true",
            "should_retrieve_data_value": "false"
            }
        ]
        }'
WHERE
    abbrev = 'CB_PROGRAM_IRSEA_ENTITY_EXPORT_DATA_TEMPLATE_VARIABLES_SETTINGS'



--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = '{
--rollback   "plot": [
--rollback     {
--rollback       "label": "Plot ID",
--rollback       "abbrev": "PLOT_ID",
--rollback       "required": "false",
--rollback       "attribute": "plotDbId",
--rollback       "is_selected": "true",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Plot Number",
--rollback       "abbrev": "PLOTNO",
--rollback       "required": "false",
--rollback       "attribute": "plotNumber",
--rollback       "is_selected": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Plot Code",
--rollback       "abbrev": "PLOT_CODE",
--rollback       "required": "false",
--rollback       "attribute": "plotCode",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Plot Type",
--rollback       "abbrev": "PLOT_TYPE",
--rollback       "required": "false",
--rollback       "attribute": "plotType",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Replication",
--rollback       "abbrev": "REP",
--rollback       "required": "false",
--rollback       "attribute": "rep",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Block Number",
--rollback       "abbrev": "BLOCK_NO_CONT",
--rollback       "required": "false",
--rollback       "attribute": "blockNumber",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "PA X",
--rollback       "abbrev": "PA_X",
--rollback       "required": "false",
--rollback       "attribute": "paX",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "PA Y",
--rollback       "abbrev": "PA_Y",
--rollback       "required": "false",
--rollback       "attribute": "paY",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     }
--rollback   ],
--rollback   "entry": [
--rollback     {
--rollback       "label": "Entry Number",
--rollback       "abbrev": "ENTRY_NUMBER",
--rollback       "required": "false",
--rollback       "attribute": "entryNumber",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Entry Code",
--rollback       "abbrev": "ENTRY_CODE",
--rollback       "required": "false",
--rollback       "attribute": "entryCode",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Entry Type",
--rollback       "abbrev": "ENTRY_TYPE",
--rollback       "required": "false",
--rollback       "attribute": "entryType",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Entry Class",
--rollback       "abbrev": "ENTRY_CLASS",
--rollback       "required": "false",
--rollback       "attribute": "entryClass",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Entry Role",
--rollback       "abbrev": "ENTRY_ROLE",
--rollback       "required": "false",
--rollback       "attribute": "entryRole",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     }
--rollback   ],
--rollback   "trait": [
--rollback     {
--rollback       "label": "Bl1 0-9",
--rollback       "abbrev": "BL_NURS_0_9",
--rollback       "required": "false",
--rollback       "attribute": "BL_NURS_0_9",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "YLD_CONT2",
--rollback       "abbrev": "YLD_CONT2",
--rollback       "required": "false",
--rollback       "attribute": "YLD_CONT2",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "YLD_TON",
--rollback       "abbrev": "YLD_CONT_TON",
--rollback       "required": "false",
--rollback       "attribute": "YLD_CONT_TON",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "TILLER_AVG",
--rollback       "abbrev": "TIL_AVE_CONT",
--rollback       "required": "false",
--rollback       "attribute": "TIL_AVE_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "NO OF SEED",
--rollback       "abbrev": "NO_OF_SEED",
--rollback       "required": "false",
--rollback       "attribute": "NO_OF_SEED",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Pacp 1-9",
--rollback       "abbrev": "PACP_SCOR_1_9",
--rollback       "required": "false",
--rollback       "attribute": "PACP_SCOR_1_9",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "FLW50",
--rollback       "abbrev": "FLW50",
--rollback       "required": "false",
--rollback       "attribute": "FLW50",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "CROSSING DATE",
--rollback       "abbrev": "DATE_CROSSED",
--rollback       "required": "false",
--rollback       "attribute": "DATE_CROSSED",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "TRANSFORMATION DATE",
--rollback       "abbrev": "TRANSFORMATION_DATE",
--rollback       "required": "false",
--rollback       "attribute": "TRANSFORMATION_DATE",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "HARVEST DATE",
--rollback       "abbrev": "HVDATE_CONT",
--rollback       "required": "false",
--rollback       "attribute": "HVDATE_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "RLODGINC CT PLNTPLOT",
--rollback       "abbrev": "RLODGINC_CT_PLNTPLOT",
--rollback       "required": "false",
--rollback       "attribute": "RLODGINC_CT_PLNTPLOT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "PSTANDHV CT PLNTPLOT",
--rollback       "abbrev": "PSTANDHV_CT_PLNTPLOT",
--rollback       "required": "false",
--rollback       "attribute": "PSTANDHV_CT_PLNTPLOT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "HV HILL",
--rollback       "abbrev": "HVHILL_CONT",
--rollback       "required": "false",
--rollback       "attribute": "HVHILL_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "TOTAL HILLS",
--rollback       "abbrev": "TOTAL_HILL_CONT",
--rollback       "required": "false",
--rollback       "attribute": "TOTAL_HILL_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "PSTANDTH CT PLNTPLOT",
--rollback       "abbrev": "PSTANDTH_CT_PLNTPLOT",
--rollback       "required": "false",
--rollback       "attribute": "PSTANDTH_CT_PLNTPLOT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "RLODGINC CMP PCT",
--rollback       "abbrev": "RLODGINC_CMP_PCT",
--rollback       "required": "false",
--rollback       "attribute": "RLODGINC_CMP_PCT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "ASI Trait",
--rollback       "abbrev": "ASI_TRAIT",
--rollback       "required": "false",
--rollback       "attribute": "ASI_TRAIT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Ht4",
--rollback       "abbrev": "HT4_CONT",
--rollback       "required": "false",
--rollback       "attribute": "HT4_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "CL5",
--rollback       "abbrev": "CML5_CONT",
--rollback       "required": "false",
--rollback       "attribute": "CML5_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "AYLD_G",
--rollback       "abbrev": "AYLD_CONT",
--rollback       "required": "false",
--rollback       "attribute": "AYLD_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Ht11",
--rollback       "abbrev": "HT11_CONT",
--rollback       "required": "false",
--rollback       "attribute": "HT11_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "HT_AVG",
--rollback       "abbrev": "HT_CONT",
--rollback       "required": "false",
--rollback       "attribute": "HT_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "PnL 5",
--rollback       "abbrev": "PNL5_CONT",
--rollback       "required": "false",
--rollback       "attribute": "PNL5_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "HT_AVG_ENT",
--rollback       "abbrev": "HT_CONT_ENT",
--rollback       "required": "false",
--rollback       "attribute": "HT_CONT_ENT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "HT",
--rollback       "abbrev": "HT_CAT",
--rollback       "required": "false",
--rollback       "attribute": "HT_CAT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Maturity date",
--rollback       "abbrev": "MAT_DATE_CONT",
--rollback       "required": "false",
--rollback       "attribute": "MAT_DATE_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Ht12",
--rollback       "abbrev": "HT12_CONT",
--rollback       "required": "false",
--rollback       "attribute": "HT12_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "BB GH 1-9 - SES5",
--rollback       "abbrev": "BB_SES5_GH_SCOR_1_9",
--rollback       "required": "false",
--rollback       "attribute": "BB_SES5_GH_SCOR_1_9",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "FLW DATE",
--rollback       "abbrev": "FLW_DATE_CONT",
--rollback       "required": "false",
--rollback       "attribute": "FLW_DATE_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Lg 1-9",
--rollback       "abbrev": "LG_SCOR_1_9",
--rollback       "required": "false",
--rollback       "attribute": "LG_SCOR_1_9",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Ht2",
--rollback       "abbrev": "HT2_CONT",
--rollback       "required": "false",
--rollback       "attribute": "HT2_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Grain Color",
--rollback       "abbrev": "GRAIN_COLOR",
--rollback       "required": "false",
--rollback       "attribute": "GRAIN_COLOR",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "FLW",
--rollback       "abbrev": "FLW_CONT",
--rollback       "required": "false",
--rollback       "attribute": "FLW_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "HT9",
--rollback       "abbrev": "HT9_CONT",
--rollback       "required": "false",
--rollback       "attribute": "HT9_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "HT8",
--rollback       "abbrev": "HT8_CONT",
--rollback       "required": "false",
--rollback       "attribute": "HT8_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "PnL 6",
--rollback       "abbrev": "PNL6_CONT",
--rollback       "required": "false",
--rollback       "attribute": "PNL6_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Ht6",
--rollback       "abbrev": "HT6_CONT",
--rollback       "required": "false",
--rollback       "attribute": "HT6_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Number of Ears Selected",
--rollback       "abbrev": "NO_OF_EARS",
--rollback       "required": "false",
--rollback       "attribute": "NO_OF_EARS",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "MC",
--rollback       "abbrev": "MC_CONT",
--rollback       "required": "false",
--rollback       "attribute": "MC_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Specific plant number selected",
--rollback       "abbrev": "SPECIFIC_PLANT",
--rollback       "required": "false",
--rollback       "attribute": "SPECIFIC_PLANT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "NO OF PLANTS SELECTED",
--rollback       "abbrev": "NO_OF_PLANTS",
--rollback       "required": "false",
--rollback       "attribute": "NO_OF_PLANTS",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "CL6",
--rollback       "abbrev": "CML6_CONT",
--rollback       "required": "false",
--rollback       "attribute": "CML6_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "HT10",
--rollback       "abbrev": "HT10_CONT",
--rollback       "required": "false",
--rollback       "attribute": "HT10_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "HT7",
--rollback       "abbrev": "HT7_CONT",
--rollback       "required": "false",
--rollback       "attribute": "HT7_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Ht5",
--rollback       "abbrev": "HT5_CONT",
--rollback       "required": "false",
--rollback       "attribute": "HT5_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "PANSEL",
--rollback       "abbrev": "PANNO_SEL",
--rollback       "required": "false",
--rollback       "attribute": "PANNO_SEL",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Ht3",
--rollback       "abbrev": "HT3_CONT",
--rollback       "required": "false",
--rollback       "attribute": "HT3_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Heterotic Group",
--rollback       "abbrev": "HETEROTIC_GROUP",
--rollback       "required": "false",
--rollback       "attribute": "HETEROTIC_GROUP",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Ht1",
--rollback       "abbrev": "HT1_CONT",
--rollback       "required": "false",
--rollback       "attribute": "HT1_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "HARV METH",
--rollback       "abbrev": "HV_METH_DISC",
--rollback       "required": "false",
--rollback       "attribute": "HV_METH_DISC",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     }
--rollback   ],
--rollback   "germplasm": [
--rollback     {
--rollback       "label": "Germplasm Code",
--rollback       "abbrev": "GERMPLASM_CODE",
--rollback       "required": "false",
--rollback       "attribute": "germplasmCode",
--rollback       "is_selected": "true",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Germplasm Name",
--rollback       "abbrev": "GERMPLASM_NAME",
--rollback       "required": "false",
--rollback       "attribute": "germplasmName",
--rollback       "is_selected": "true",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Germplasm State",
--rollback       "abbrev": "GERMPLASM_STATE",
--rollback       "required": "false",
--rollback       "attribute": "germplasmState",
--rollback       "is_selected": "true",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Taxonomy Name",
--rollback       "abbrev": "TAXONOMY_NAME",
--rollback       "required": "false",
--rollback       "attribute": "taxonomyName",
--rollback       "is_selected": "true",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Female Parent Germplasm Code",
--rollback       "abbrev": "GERMPLASM_CODE",
--rollback       "required": "false",
--rollback       "attribute": "femaleParentGermplasmCode",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Male Parent Germplasm Code",
--rollback       "abbrev": "GERMPLASM_CODE",
--rollback       "required": "false",
--rollback       "attribute": "maleParentGermplasmCode",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Parentage",
--rollback       "abbrev": "PARENTAGE",
--rollback       "required": "false",
--rollback       "attribute": "parentage",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Germplasm Type",
--rollback       "abbrev": "GERMPLASM_TYPE",
--rollback       "required": "false",
--rollback       "attribute": "germplasmType",
--rollback       "is_selected": "true",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Germplasm Name Type",
--rollback       "abbrev": "GERMPLASM_NAME_TYPE",
--rollback       "required": "false",
--rollback       "attribute": "germplasmNameType",
--rollback       "is_selected": "true",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     }
--rollback   ],
--rollback   "experiment": [
--rollback     {
--rollback       "label": "Experiment Code",
--rollback       "abbrev": "EXPERIMENT_CODE",
--rollback       "required": "false",
--rollback       "attribute": "experimentCode",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Experiment Name",
--rollback       "abbrev": "EXPERIMENT_NAME",
--rollback       "required": "false",
--rollback       "attribute": "experimentName",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Experiment Year",
--rollback       "abbrev": "EXPERIMENT_YEAR",
--rollback       "required": "false",
--rollback       "attribute": "experimentYear",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Season",
--rollback       "abbrev": "SEASON",
--rollback       "required": "false",
--rollback       "attribute": "experimentSeason",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Stage Code",
--rollback       "abbrev": "STAGE_CODE",
--rollback       "required": "false",
--rollback       "attribute": "experimentStageCode",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Project",
--rollback       "abbrev": "PROJECT",
--rollback       "required": "false",
--rollback       "attribute": "experimentProject",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Pipeline",
--rollback       "abbrev": "PIPELINE",
--rollback       "required": "false",
--rollback       "attribute": "experimentPipeline",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Experiment Objective",
--rollback       "abbrev": "EXPERIMENT_OBJECTIVE",
--rollback       "required": "false",
--rollback       "attribute": "experimentObjective",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Experiment Type",
--rollback       "abbrev": "EXPERIMENT_TYPE",
--rollback       "required": "false",
--rollback       "attribute": "experimentType",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Experiment Subtype",
--rollback       "abbrev": "EXPERIMENT_SUB_TYPE",
--rollback       "required": "false",
--rollback       "attribute": "experimentSubtype",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Experiment Design Type",
--rollback       "abbrev": "EXPERIMENT_DESIGN_TYPE",
--rollback       "required": "false",
--rollback       "attribute": "experimentDesignType",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Experiment Status",
--rollback       "abbrev": "EXPERIMENT_STATUS",
--rollback       "required": "false",
--rollback       "attribute": "experimentStatus",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Experiment Steward",
--rollback       "abbrev": "EXPERIMENT_STEWARD",
--rollback       "required": "false",
--rollback       "attribute": "experimentSteward",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Planting Season",
--rollback       "abbrev": "PLANTING_SEASON",
--rollback       "required": "false",
--rollback       "attribute": "plantingSeason",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Market Segment",
--rollback       "abbrev": "MARKET_SEGMENT",
--rollback       "required": "false",
--rollback       "attribute": "MARKET_SEGMENT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Experiment Ecosystem",
--rollback       "abbrev": "ECOSYSTEM",
--rollback       "required": "false",
--rollback       "attribute": "EXPERIMENT_ECOSYSTEM",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Experiment Remarks",
--rollback       "abbrev": "REMARKS",
--rollback       "required": "false",
--rollback       "attribute": "experimentRemarks",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     }
--rollback   ],
--rollback   "occurrence": [
--rollback     {
--rollback       "label": "Occurrence Code",
--rollback       "abbrev": "OCCURRENCE_CODE",
--rollback       "required": "true",
--rollback       "attribute": "occurrenceCode",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Occurrence Name",
--rollback       "abbrev": "OCCURRENCE_NAME",
--rollback       "required": "false",
--rollback       "attribute": "occurrenceName",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Occurrence Number",
--rollback       "abbrev": "OCCURRENCE_NUMBER",
--rollback       "required": "false",
--rollback       "attribute": "occurrenceNumber",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Site Code",
--rollback       "abbrev": "SITE_CODE",
--rollback       "required": "true",
--rollback       "attribute": "siteCode",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Site Name",
--rollback       "abbrev": "SITE",
--rollback       "required": "false",
--rollback       "attribute": "siteName",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Location Code",
--rollback       "abbrev": "LOCATION_CODE",
--rollback       "required": "true",
--rollback       "attribute": "locationCode",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Location Name",
--rollback       "abbrev": "LOCATION_NAME",
--rollback       "required": "true",
--rollback       "attribute": "locationName",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Planting Date",
--rollback       "abbrev": "PLANTING_DATE",
--rollback       "required": "false",
--rollback       "attribute": "plantingDate",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "true",
--rollback       "should_retrieve_data_value": "true"
--rollback     },
--rollback     {
--rollback       "label": "Geospatial Coordinates",
--rollback       "abbrev": "GEOSPATIAL_COORDS",
--rollback       "required": "false",
--rollback       "attribute": "GEOSPATIAL_COORDS",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "true",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Longitude",
--rollback       "abbrev": "LONGITUDE",
--rollback       "required": "false",
--rollback       "attribute": "LONGITUDE",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Latitude",
--rollback       "abbrev": "LATITUDE",
--rollback       "required": "false",
--rollback       "attribute": "LATITUDE",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Occurrence Ecosystem",
--rollback       "abbrev": "ECOSYSTEM",
--rollback       "required": "false",
--rollback       "attribute": "OCCURRENCE_ECOSYSTEM",
--rollback       "is_selected": "true",
--rollback       "is_uploadable": "true",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Occurrence Description",
--rollback       "abbrev": "DESCRIPTION",
--rollback       "required": "false",
--rollback       "attribute": "occurrenceDescription",
--rollback       "is_selected": "true",
--rollback       "is_uploadable": "true",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Location Description",
--rollback       "abbrev": "DESCRIPTION",
--rollback       "required": "false",
--rollback       "attribute": "locationDescription",
--rollback       "is_selected": "true",
--rollback       "is_uploadable": "true",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Contact Person",
--rollback       "abbrev": "CONTCT_PERSON_CONT",
--rollback       "required": "false",
--rollback       "attribute": "CONTCT_PERSON_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "true",
--rollback       "should_retrieve_data_value": "false"
--rollback     }
--rollback   ],
--rollback   "planting_protocol": [
--rollback     {
--rollback       "label": "Crop Establishment",
--rollback       "abbrev": "ESTABLISHMENT",
--rollback       "required": "false",
--rollback       "attribute": "ESTABLISHMENT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Planting Type",
--rollback       "abbrev": "PLANTING_TYPE",
--rollback       "required": "false",
--rollback       "attribute": "PLANTING_TYPE",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Plot Type",
--rollback       "abbrev": "PLOT_TYPE",
--rollback       "required": "false",
--rollback       "attribute": "PLOT_TYPE",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "PLOT LN",
--rollback       "abbrev": "PLOT_LN",
--rollback       "required": "false",
--rollback       "attribute": "PLOT_LN",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Plot Width",
--rollback       "abbrev": "PLOT_WIDTH",
--rollback       "required": "false",
--rollback       "attribute": "PLOT_WIDTH",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Alley Length",
--rollback       "abbrev": "ALLEY_LENGTH",
--rollback       "required": "false",
--rollback       "attribute": "ALLEY_LENGTH",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Transplanting Date",
--rollback       "abbrev": "TRANS_DATE_CONT",
--rollback       "required": "false",
--rollback       "attribute": "TRANS_DATE_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Seeding Density",
--rollback       "abbrev": "SEEDING_RATE",
--rollback       "required": "false",
--rollback       "attribute": "SEEDING_RATE",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Planting Instructions",
--rollback       "abbrev": "PLANTING_INSTRUCTIONS",
--rollback       "required": "false",
--rollback       "attribute": "PLANTING_INSTRUCTIONS",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "BED WIDTH",
--rollback       "abbrev": "BED_WIDTH",
--rollback       "required": "false",
--rollback       "attribute": "BED_WIDTH",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "DISTANCE BET PLOTS IN M",
--rollback       "abbrev": "DISTANCE_BET_PLOTS_IN_M",
--rollback       "required": "false",
--rollback       "attribute": "DISTANCE_BET_PLOTS_IN_M",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "DIST BET HILLS",
--rollback       "abbrev": "DIST_BET_HILLS",
--rollback       "required": "false",
--rollback       "attribute": "DIST_BET_HILLS",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "DIST BET ROWS",
--rollback       "abbrev": "DIST_BET_ROWS",
--rollback       "required": "false",
--rollback       "attribute": "DIST_BET_ROWS",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "HILLS PER ROW CONT",
--rollback       "abbrev": "HILLS_PER_ROW_CONT",
--rollback       "required": "false",
--rollback       "attribute": "HILLS_PER_ROW_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "NO OF BEDS PER PLOT",
--rollback       "abbrev": "NO_OF_BEDS_PER_PLOT",
--rollback       "required": "false",
--rollback       "attribute": "NO_OF_BEDS_PER_PLOT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "PLOT AREA 1",
--rollback       "abbrev": "PLOT_AREA_1",
--rollback       "required": "false",
--rollback       "attribute": "PLOT_AREA_1",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "PLOT AREA 2",
--rollback       "abbrev": "PLOT_AREA_2",
--rollback       "required": "false",
--rollback       "attribute": "PLOT_AREA_2",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "PLOT AREA 13",
--rollback       "abbrev": "PLOT_AREA_3",
--rollback       "required": "false",
--rollback       "attribute": "PLOT_AREA_3",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "PLOT AREA 3",
--rollback       "abbrev": "PLOT_AREA_3",
--rollback       "required": "false",
--rollback       "attribute": "PLOT_AREA_3",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "PLOT LN 1",
--rollback       "abbrev": "PLOT_LN_1",
--rollback       "required": "false",
--rollback       "attribute": "PLOT_LN_1",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "PLOT WIDTH MAIZE BED",
--rollback       "abbrev": "PLOT_WIDTH_MAIZE_BED",
--rollback       "required": "false",
--rollback       "attribute": "PLOT_WIDTH_MAIZE_BED",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "PLOT WIDTH RICE",
--rollback       "abbrev": "PLOT_WIDTH_RICE",
--rollback       "required": "false",
--rollback       "attribute": "PLOT_WIDTH_RICE",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "PLOT WIDTH WHEAT FLAT",
--rollback       "abbrev": "PLOT_WIDTH_WHEAT_FLAT",
--rollback       "required": "false",
--rollback       "attribute": "PLOT_WIDTH_WHEAT_FLAT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "ROWS PER PLOT CONT",
--rollback       "abbrev": "ROWS_PER_PLOT_CONT",
--rollback       "required": "false",
--rollback       "attribute": "ROWS_PER_PLOT_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "PLOT AREA SQM CONT",
--rollback       "abbrev": "PLOT_AREA_SQM_CONT",
--rollback       "required": "false",
--rollback       "attribute": "PLOT_AREA_SQM_CONT",
--rollback       "is_selected": "true",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     }
--rollback   ],
--rollback   "management_protocol": [
--rollback     {
--rollback       "label": "Distance Between Rows",
--rollback       "abbrev": "DIST_BET_ROWS",
--rollback       "required": "false",
--rollback       "attribute": "DIST_BET_ROWS",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Rows per Plot",
--rollback       "abbrev": "ROWS_PER_PLOT_CONT",
--rollback       "required": "false",
--rollback       "attribute": "ROWS_PER_PLOT_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Seeding Date",
--rollback       "abbrev": "SEEDING_DATE_CONT",
--rollback       "required": "false",
--rollback       "attribute": "SEEDING_DATE_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "false",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Fertilizer Type - 1st Application",
--rollback       "abbrev": "FERT1_TYPE_DISC",
--rollback       "required": "false",
--rollback       "attribute": "FERT1_TYPE_DISC",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "true",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Fertilizer Date - 1st Application",
--rollback       "abbrev": "FERT1_DATE_CONT",
--rollback       "required": "false",
--rollback       "attribute": "FERT1_DATE_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "true",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Fertilizer Brand - 1st Application",
--rollback       "abbrev": "FERT1_BRAND",
--rollback       "required": "false",
--rollback       "attribute": "FERT1_BRAND",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "true",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Fertilizer Method - 1st Application",
--rollback       "abbrev": "FERT1_METH_DISC",
--rollback       "required": "false",
--rollback       "attribute": "FERT1_METH_DISC",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "true",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Fertilizer Type - 2nd Application",
--rollback       "abbrev": "FERT2_TYPE_DISC",
--rollback       "required": "false",
--rollback       "attribute": "FERT2_TYPE_DISC",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "true",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Fertilizer Date - 2nd Application",
--rollback       "abbrev": "FERT2_DATE_CONT",
--rollback       "required": "false",
--rollback       "attribute": "FERT2_DATE_CONT",
--rollback       "is_selected": "true",
--rollback       "is_uploadable": "true",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Fertilizer Brand - 2nd Application",
--rollback       "abbrev": "FERT2_BRAND",
--rollback       "required": "false",
--rollback       "attribute": "FERT2_BRAND",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "true",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Fertilizer Method - 2nd Application",
--rollback       "abbrev": "FERT2_METH_DISC",
--rollback       "required": "false",
--rollback       "attribute": "FERT2_METH_DISC",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "true",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Fertilizer Type - 3rd Application",
--rollback       "abbrev": "FERT3_TYPE_DISC",
--rollback       "required": "false",
--rollback       "attribute": "FERT3_TYPE_DISC",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "true",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Fertilizer Date - 3rd Application",
--rollback       "abbrev": "FERT3_DATE_CONT",
--rollback       "required": "false",
--rollback       "attribute": "FERT3_DATE_CONT",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "true",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Fertilizer Brand - 3rd Application",
--rollback       "abbrev": "FERT3_BRAND",
--rollback       "required": "false",
--rollback       "attribute": "FERT3_BRAND",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "true",
--rollback       "should_retrieve_data_value": "false"
--rollback     },
--rollback     {
--rollback       "label": "Fertilizer Method - 3rd Application",
--rollback       "abbrev": "FERT3_METH_DISC",
--rollback       "required": "false",
--rollback       "attribute": "FERT3_METH_DISC",
--rollback       "is_selected": "false",
--rollback       "is_uploadable": "true",
--rollback       "should_retrieve_data_value": "false"
--rollback     }
--rollback   ]
--rollback }'
--rollback WHERE
--rollback     abbrev = 'CB_PROGRAM_IRSEA_ENTITY_EXPORT_DATA_TEMPLATE_VARIABLES_SETTINGS'

