--liquibase formatted sql

--changeset postgres:update_PLOT_AREA_HARVESTED_variable context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3148 CB-DB: Update type and data_level value of variable PLOT_AREA_HARVESTED



UPDATE master.variable
SET
 type = 'observation',
 data_level = 'plot, occurrence'
WHERE abbrev = 'PLOT_AREA_HARVESTED'
;



--rollback UPDATE master.variable
--rollback SET
--rollback  type = 'system',
--rollback  data_level = 'experiment, occurrence, location, plot'
--rollback WHERE abbrev = 'PLOT_AREA_HARVESTED'
--rollback ;