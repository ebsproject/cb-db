--liquibase formatted sql

--changeset postgres:update_formula_of_GW_CMP_KG context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3155 CB-DB: Update formula of GW_CMP_KG



UPDATE 
    master.formula 
SET
    data_level = (SELECT data_level from master.variable where abbrev = 'GW_CMP_KG'),
    function_name = 'master.formula_gw_cmp_kg_2(ew_m_kg)',
    database_formula = '
    CREATE OR REPLACE FUNCTION master.formula_gw_cmp_kg_2(
                ew_m_kg float
			) RETURNS float as
			$body$
			BEGIN
				RETURN ew_m_kg*08;
			END
			$body$
			language plpgsql;'
WHERE formula = 'GW_CMP_KG=EW_M_KG*0.8';



--rollback UPDATE master.formula SET data_level = 'plot', database_formula = 'create or replace function master.formula_gw_cmp_kg(ew_m_kg float) returns float as $body$ declare gw_cmp_kg float; local_gw_ew_m_kg float; begin gw_cmp_kg=(ayld_g_cont/hv_area_dsr_sqm)*10; return round(gw_cmp_kg::numeric, 3); end $body$ language plpgsql;' WHERE formula = 'GW_CMP_KG=EW_M_KG*0.8';



--changeset postgres:insert_formula_parameter_of_GW_CMP_KG context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3155 CB-DB: insert formula parameters of GW_CMP_KG



INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('EW_M_KG') -- must be the same order as defined in the database function
    AND var.abbrev = 'GW_CMP_KG'
;

SELECT master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.formula_parameter 
--rollback where formula_id in (SELECT id FROM master.formula WHERE formula = 'GW_CMP_KG=EW_M_KG*0.8');



--changeset postgres:update_function_of_GW_CMP_KG context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3155 CB-DB: update function of GW_CMP_KG



CREATE OR REPLACE FUNCTION master.formula_gw_cmp_kg_2(
    ew_m_kg float
) RETURNS float as
$body$
BEGIN
	RETURN ew_m_kg*08;
END
$body$
language plpgsql;



--rollback CREATE OR REPLACE FUNCTION master.formula_gw_cmp_kg(ew_m_kg float) RETURNS float as $body$ DECLARE gw_cmp_kg float; local_ew_m_kg float; BEGIN gw_cmp_kg=(ayld_g_cont/hv_area_dsr_sqm)*10; RETURN round(gw_cmp_kg::numeric, 3); END $body$ language plpgsql;