--liquibase formatted sql

--changeset postgres:5_populate_place_geospatial_object_for_BA context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3111: Populate place_geospatial_object for BA testing



INSERT INTO place.geospatial_object
(geospatial_object_code,geospatial_object_name,geospatial_object_type,geospatial_object_subtype,geospatial_coordinates,altitude,description,"parent_geospatial_object_id","root_geospatial_object_id",creator_id,coordinates)
 VALUES 
('PH_AN_RM-2023-WS-001','PH_AN_RM-2023-WS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH_AN_RM' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),NULL),
('IRRIHQ-2023-WS-006','IRRIHQ-2023-WS-006','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),NULL),
('PH_BO_UA-2023-WS-002','PH_BO_UA-2023-WS-002','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH_BO_UA' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),NULL),
('PH_NE_SM2-2023-WS-001','PH_NE_SM2-2023-WS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH_NE_SM2' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),NULL);



--rollback DELETE FROM
--rollback place.geospatial_object
--rollback WHERE
--rollback geospatial_object_code in (
--rollback 'PH_AN_RM-2023-WS-001',
--rollback 'IRRIHQ-2023-WS-006',
--rollback 'PH_BO_UA-2023-WS-002',
--rollback 'PH_NE_SM2-2023-WS-001'
--rollback );