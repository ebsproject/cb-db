--liquibase formatted sql

--changeset postgres:update_formula_of_GY_CMP_THA context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3153 CB-DB: Update old formula of GY_CMP_THA



UPDATE 
    master.formula 
SET
    data_level = (SELECT data_level from master.variable where abbrev = 'GY_CMP_THA'),
    function_name = 'master.formula_gy_cmp_tha_2(gw_cmp_kg, plot_area_harvested)',
    database_formula = '
    CREATE OR REPLACE FUNCTION master.formula_gy_cmp_tha_2(
                gw_cmp_kg float,
                plot_area_harvested float
            ) RETURNS float as
            $body$
            BEGIN    
              RETURN (gw_cmp_kg)*(10/plot_area_harvested);
            END
            $body$
            language plpgsql;'
WHERE formula = 'GY_CMP_THA = (GW_CMP_KG)*(10/PLOT_AREA_HARVESTED)';



--rollback UPDATE master.formula SET data_level = 'plot', function_name = 'master.formula_gy_cmp_tha(gw_m_kg,plot_area_harvested)', database_formula = 'CREATE OR REPLACE FUNCTION master.formula_gy_cmp_tha(gw_m_kg float, plot_area_harvested float ) RETURNS float as $body$ DECLARE gy_cmp_tha float; local_gw_m_kg float; local_plot_area_harvested float; BEGIN gy_cmp_tha = (gw_cmp_kg)*(10/plot_area_harvested); RETURN round(gy_cmp_tha::numeric, 3); END $body$ language plpgsql;' WHERE formula = 'GY_CMP_THA = (GW_CMP_KG)*(10/PLOT_AREA_HARVESTED)';



--changeset postgres:insert_formula_parameter_PLOT_AREA_HARVESTED context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3153 CB-DB: insert formula parameter PLOT_AREA_HARVESTED to old GY_CMP_THA formula



INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id, order_number)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id,
    2 AS order_number
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('PLOT_AREA_HARVESTED') -- must be the same order as defined in the database function
    AND var.abbrev = 'GY_CMP_THA';



--rollback DELETE FROM master.formula_parameter 
--rollback where formula_id in (SELECT id FROM master.formula WHERE formula = 'GY_CMP_THA = (GW_CMP_KG)*(10/PLOT_AREA_HARVESTED)') AND param_variable_id in (SELECT id from master.variable WHERE abbrev = 'PLOT_AREA_HARVESTED');



--changeset postgres:update_formula_parameter_GW_CMP_KG_of_GY_CMP_THA_old_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3153 CB-DB: update formula parameter GW_CMP_KG to second GY_CMP_THA formula



UPDATE
    master.formula_parameter
SET
    data_level = (SELECT data_level from master.variable where abbrev = 'GW_CMP_KG'),
    order_number = 1
WHERE formula_id in (SELECT id FROM master.formula WHERE formula = 'GY_CMP_THA = (GW_CMP_KG)*(10/PLOT_AREA_HARVESTED)') AND param_variable_id in (SELECT id from master.variable WHERE abbrev = 'GW_CMP_KG');



--rollback UPDATE
--rollback     master.formula_parameter
--rollback SET
--rollback     data_level = 'experiment, occurrence, location, plot',
--rollback     order_number = NULL
--rollback WHERE formula_id in (SELECT id FROM master.formula WHERE formula = 'GY_CMP_THA = (GW_CMP_KG)*(10/PLOT_AREA_HARVESTED)') AND param_variable_id in (SELECT id from master.variable WHERE abbrev = 'GW_CMP_KG');




--changeset postgres:update_function_of_GY_CMP_THA context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3153 CB-DB: update function of old GY_CMP_THA formula



DROP FUNCTION master.formula_gy_cmp_tha;

CREATE OR REPLACE FUNCTION master.formula_gy_cmp_tha_2(
    gw_cmp_kg float,
    plot_area_harvested float
) RETURNS float as
$body$
BEGIN    
    RETURN (gw_cmp_kg)*(10/plot_area_harvested);
END
$body$
language plpgsql;



--rollback DROP FUNCTION master.formula_gy_cmp_tha_2;
--rollback CREATE OR REPLACE FUNCTION master.formula_gy_cmp_tha(gw_m_kg float, plot_area_harvested float ) RETURNS float as $body$ DECLARE gy_cmp_tha float; local_gw_m_kg float; local_plot_area_harvested float; BEGIN gy_cmp_tha = (gw_cmp_kg)*(10/plot_area_harvested); RETURN round(gy_cmp_tha::numeric, 3); END $body$ language plpgsql;
