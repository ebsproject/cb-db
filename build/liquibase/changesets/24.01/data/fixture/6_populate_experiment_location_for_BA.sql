--liquibase formatted sql

--changeset postgres:6_populate_experiment_location_for_BA context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3111: Populate experiment_location for BA testing



INSERT INTO experiment.location
(location_code,location_name,location_status,location_type,description,steward_id,location_planting_date,location_harvest_date,geospatial_object_id,creator_id,location_year,season_id,site_id,field_id,location_number,remarks,entry_count,plot_count)
 VALUES 
('IRRIHQ-2023-WS-006','IRRIHQ-2023-WS-006','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRIHQ-2023-WS-006' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),2023,(SELECT id FROM tenant.season WHERE season_code='WS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRIHQ' LIMIT 1),NULL,6,NULL,0,170),
('PH_NE_SM2-2023-WS-001','PH_NE_SM2-2023-WS-001','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH_NE_SM2-2023-WS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),2023,(SELECT id FROM tenant.season WHERE season_code='WS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH_NE_SM2' LIMIT 1),NULL,1,NULL,0,170),
('PH_AN_RM-2023-WS-001','PH_AN_RM-2023-WS-001','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH_AN_RM-2023-WS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),2023,(SELECT id FROM tenant.season WHERE season_code='WS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH_AN_RM' LIMIT 1),NULL,1,NULL,0,170),
('PH_BO_UA-2023-WS-002','PH_BO_UA-2023-WS-002','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH_BO_UA-2023-WS-002' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),2023,(SELECT id FROM tenant.season WHERE season_code='WS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH_BO_UA' LIMIT 1),NULL,2,NULL,0,170);



--rollback DELETE FROM
--rollback experiment.location
--rollback WHERE
--rollback location_code in (
--rollback 'IRRIHQ-2023-WS-006',
--rollback 'PH_NE_SM2-2023-WS-001',
--rollback 'PH_AN_RM-2023-WS-001',
--rollback 'PH_BO_UA-2023-WS-002'
--rollback );