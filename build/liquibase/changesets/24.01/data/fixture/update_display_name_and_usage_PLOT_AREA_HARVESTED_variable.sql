--liquibase formatted sql

--changeset postgres:update_PLOT_AREA_HARVESTED_variable context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3148 CB-DB: Update display name and usage value of variable PLOT_AREA_HARVESTED



UPDATE master.variable
SET
 display_name = 'Harvested plot area',
 usage = 'occurrence'
WHERE abbrev = 'PLOT_AREA_HARVESTED'
;



--rollback UPDATE master.variable
--rollback SET
--rollback  display_name = 'Grain moisture',
--rollback  usage = 'application'
--rollback WHERE abbrev = 'PLOT_AREA_HARVESTED'
--rollback ;