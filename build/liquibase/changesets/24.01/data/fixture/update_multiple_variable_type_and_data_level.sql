--liquibase formatted sql

--changeset postgres:update_multiple_variable_type_and_data_level context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3148 CB-DB: Update type and data_level value of multiple variables



UPDATE master.variable
SET
 type = 'observation',
 usage = 'occurrence',
 data_level = 'plot'
WHERE abbrev in (
    'MF_CONT',
    'ADJAYLD_G_CONT',
    'ADJAYLD_KG_CONT',
    'ADJAYLD_KG_CONT4',
    'ADJYLD3_CONT',
    'DISEASE_INDEX',
    'HI_CONT',
    'MISS_HILL_CONT',
    'STRWWT_CONT',
    'STRWWTBM_CONT',
    'TOTBM_CONT_KG',
    'TOTBM_CONT1',
    'YLD_0_CONT1',
    'YLD_0_CONT2',
    'YLD_0_CONT3',
    'YLD_0_CONT4',
    'YLD_0_CONT5',
    'YLD_0_CONT6',
    'YLD_0_CONT7',
    'YLD_CONT_TON2',
    'YLD_CONT_TON5',
    'YLD_CONT_TON6',
    'YLD_CONT_TON7',
    'YLD_CONT1',
    'GYLD_DSR_KG',
    'HV_AREA_DSR_SQM',
    'HVHILL_PCT',
    'PLOT_AREA_DSR_SQM',
    'SUB_SURVIVAL_PCT',
    'TOT_PLTGAP_AREA_SQM',
    'YLD_CONT4',
    'YLD_DSR_TON',
    'GW_CMP_KG',
    'GY_CMP_THA',
    'GY_MOI15_CMP_THA',
    'GY_MOI13_5_CMP_THA',
    'GW_M_KG',
    'GW_SUB_M_KG',
    'EW_M_KG',
    'EW_SUB_M_KG',
    'SHELL_CMP_PCT',
    'GY_CALC_KGHA',
    'GY_CALC_GM2',
    'GY_CALC_DAHA',
    'PH_M_CM',
    'PH_M_M',
    'EMER_DATE_YMD',
    'EMER_DTO_DAY',
    'BOOT_DATEINIT_YMD',
    'BOOT_DTOINIT_DAY',
    'BOOT_DATE_YMD',
    'BOOT_DTO_DAY',
    'HD_DATE_YMD',
    'HD_DTO_DAY',
    'MAT_DATE_YMD',
    'MAT_DTO_DAY',
    'TSPL_DATE_YMD',
    'TSPL_DTO_DAY'
)
;



--rollback UPDATE master.variable
--rollback SET
--rollback  type = 'system',
--rollback  usage = 'application',
--rollback  data_level = 'experiment, occurrence, location, plot'
--rollback WHERE abbrev in (
--rollback     'MF_CONT',
--rollback     'ADJAYLD_G_CONT',
--rollback     'ADJAYLD_KG_CONT',
--rollback     'ADJAYLD_KG_CONT4',
--rollback     'ADJYLD3_CONT',
--rollback     'DISEASE_INDEX',
--rollback     'HI_CONT',
--rollback     'MISS_HILL_CONT',
--rollback     'STRWWT_CONT',
--rollback     'STRWWTBM_CONT',
--rollback     'TOTBM_CONT_KG',
--rollback     'TOTBM_CONT1',
--rollback     'YLD_0_CONT1',
--rollback     'YLD_0_CONT2',
--rollback     'YLD_0_CONT3',
--rollback     'YLD_0_CONT4',
--rollback     'YLD_0_CONT5',
--rollback     'YLD_0_CONT6',
--rollback     'YLD_0_CONT7',
--rollback     'YLD_CONT_TON2',
--rollback     'YLD_CONT_TON5',
--rollback     'YLD_CONT_TON6',
--rollback     'YLD_CONT_TON7',
--rollback     'YLD_CONT1',
--rollback     'GYLD_DSR_KG',
--rollback     'HV_AREA_DSR_SQM',
--rollback     'HVHILL_PCT',
--rollback     'PLOT_AREA_DSR_SQM',
--rollback     'SUB_SURVIVAL_PCT',
--rollback     'TOT_PLTGAP_AREA_SQM',
--rollback     'YLD_CONT4',
--rollback     'YLD_DSR_TON',
--rollback     'GW_CMP_KG',
--rollback     'GY_CMP_THA',
--rollback     'GY_MOI15_CMP_THA',
--rollback     'GY_MOI13_5_CMP_THA',
--rollback     'GW_M_KG',
--rollback     'GW_SUB_M_KG',
--rollback     'EW_M_KG',
--rollback     'EW_SUB_M_KG',
--rollback     'SHELL_CMP_PCT',
--rollback     'GY_CALC_KGHA',
--rollback     'GY_CALC_GM2',
--rollback     'GY_CALC_DAHA',
--rollback     'PH_M_CM',
--rollback     'PH_M_M',
--rollback     'EMER_DATE_YMD',
--rollback     'EMER_DTO_DAY',
--rollback     'BOOT_DATEINIT_YMD',
--rollback     'BOOT_DTOINIT_DAY',
--rollback     'BOOT_DATE_YMD',
--rollback     'BOOT_DTO_DAY',
--rollback     'HD_DATE_YMD',
--rollback     'HD_DTO_DAY',
--rollback     'MAT_DATE_YMD',
--rollback     'MAT_DTO_DAY',
--rollback     'TSPL_DATE_YMD',
--rollback     'TSPL_DTO_DAY'
--rollback )
--rollback ;