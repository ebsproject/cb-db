--liquibase formatted sql

--changeset postgres:add_variable_maize_EW_SUB_M_G context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3155 CB-DB: Insert EW_SUB_M_G variable



-- EW_SUB_M_G
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name, notes) 
    VALUES
        ('EW_SUB_M_G', 'EW_SUB_M_G', 'Ear weight subsample', 'float', false, 'observation', 'plot, occurrence', 'occurrence', 'Total weight of a subset of ears harvested from a plot measured in grams; used to calculate a shelling percentage per plot in combination with GW_SUB', 'active', '1', 'Ear weight subsample','24.01')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'EW_SUB_M_G' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('EW_SUB_M_G', 'Ear weight subsample', 'Ear weight subsample') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('EW_SUB_M_G_METHOD', 'Ear weight subsample method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('EW_SUB_M_G_SCALE', 'Ear weight subsample scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('EW_SUB_M_G');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('EW_SUB_M_G_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('EW_SUB_M_G');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('EW_SUB_M_G_SCALE');