--liquibase formatted sql

--changeset postgres:1_populate_experiment_experiment_for_BA context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3111: Populate experiment_experiment for BA testing



INSERT INTO experiment.experiment
(program_id,pipeline_id,stage_id,project_id,experiment_year,season_id,planting_season,experiment_code,experiment_name,experiment_objective,experiment_type,experiment_sub_type,experiment_sub_sub_type,experiment_design_type,experiment_status,description,steward_id,experiment_plan_id,creator_id,data_process_id,crop_id,remarks)
 VALUES 
((SELECT id FROM tenant.program WHERE program_code='IRSEA' LIMIT 1),NULL,(SELECT id FROM tenant.stage WHERE stage_code='IYT' LIMIT 1),NULL,2023,(SELECT id FROM tenant.season WHERE season_code='WS' LIMIT 1),NULL,(experiment.generate_code('experiment')),'Unspecified2023_Exp1Sample1',NULL,'Breeding Trial',NULL,NULL,'Unspecified','planted',NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),(SELECT id FROM master.item WHERE name='Breeding Trial' LIMIT 1),(SELECT id FROM tenant.crop WHERE crop_code='RICE' LIMIT 1),NULL);



--rollback DELETE FROM
--rollback experiment.experiment
--rollback WHERE
--rollback experiment_name = 'Unspecified2023_Exp1Sample1' 