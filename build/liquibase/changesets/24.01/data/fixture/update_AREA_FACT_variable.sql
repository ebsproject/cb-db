--liquibase formatted sql

--changeset postgres:update_AREA_FACT_variable context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3148 CB-DB: Update type, usage, and data_level value of variable AREA_FACT



UPDATE master.variable
SET
 type = 'observation',
 usage = 'occurrence',
 data_level = 'plot'
WHERE abbrev = 'AREA_FACT'
;



--rollback UPDATE master.variable
--rollback SET
--rollback  type = 'system',
--rollback  usage = 'application',
--rollback  data_level = 'experiment, occurrence, location, plot'
--rollback WHERE abbrev = 'AREA_FACT'
--rollback ;