--liquibase formatted sql

--changeset postgres:4_populate_experiment_occurrence_for_BA context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3111: Populate experiment_occurrence for BA testing



INSERT INTO experiment.occurrence
(occurrence_code,occurrence_name,occurrence_status,description,experiment_id,geospatial_object_id,creator_id,rep_count,site_id,field_id,occurrence_number,remarks,entry_count,plot_count,access_data)
 VALUES 
((experiment.generate_code('occurrence')),'Unspecified2023_Exp1Sample1-003','planted',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name='Unspecified2023_Exp1Sample1' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='PH_AN_RM' LIMIT 1),NULL,3,NULL,88,170,'{"program": {"101": {"addedBy": 494, "addedOn": "2023-11-20T13:19:50.416Z", "permission": "write"}}}'),
((experiment.generate_code('occurrence')),'Unspecified2023_Exp1Sample1-004','planted;trait data collected;trait data quality checked',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name='Unspecified2023_Exp1Sample1' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='PH_BO_UA' LIMIT 1),NULL,4,NULL,88,170,'{"program": {"101": {"addedBy": 494, "addedOn": "2023-11-20T13:19:55.090Z", "permission": "write"}}}'),
((experiment.generate_code('occurrence')),'Unspecified2023_Exp1Sample1-001','planted;trait data collected;trait data quality checked',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name='Unspecified2023_Exp1Sample1' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='IRRIHQ' LIMIT 1),NULL,1,NULL,88,170,'{"program": {"101": {"addedBy": 494, "addedOn": "2023-11-20T13:19:31.646Z", "permission": "write"}}}'),
((experiment.generate_code('occurrence')),'Unspecified2023_Exp1Sample1-002','planted;trait data collected;trait data quality checked',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name='Unspecified2023_Exp1Sample1' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='PH_NE_SM2' LIMIT 1),NULL,2,NULL,88,170,'{"program": {"101": {"addedBy": 494, "addedOn": "2023-11-20T13:19:43.403Z", "permission": "write"}}}');



--rollback DELETE FROM
--rollback experiment.occurrence
--rollback WHERE
--rollback experiment_id = (SELECT id FROM experiment.experiment WHERE experiment_name='Unspecified2023_Exp1Sample1' LIMIT 1);