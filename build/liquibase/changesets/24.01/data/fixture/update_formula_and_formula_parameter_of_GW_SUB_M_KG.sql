--liquibase formatted sql

--changeset postgres:update_formula_of_GW_SUB_M_KG context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3155 CB-DB: Update formula of GW_SUB_M_KG



UPDATE 
    master.formula 
SET
    data_level = (SELECT data_level from master.variable where abbrev = 'GW_SUB_M_KG'),
    function_name = 'master.formula_gw_sub_m_kg(gw_sub_m_g)',
    database_formula = '
    CREATE OR REPLACE FUNCTION master.formula_gw_sub_m_kg(
                gw_sub_m_g float
			) RETURNS float as
			$body$
			BEGIN
				RETURN gw_sub_m_g/1000;
			END
			$body$
			language plpgsql;'
WHERE formula = 'GW_SUB_M_KG = GW_SUB_M_G/1000';



--rollback UPDATE master.formula SET data_level = 'plot', database_formula = 'create or replace function master.formula_gw_sub_m_kg(gw_sub_m_g float) returns float as $body$ declare gw_sub_m_kg float; local_gw_sub_m_g float; begin gw_sub_m_kg=gw_sub_m_g/1000; return round(gw_sub_m_kg::numeric, 3); end $body$ language plpgsql;' WHERE formula = 'GW_SUB_M_KG = GW_SUB_M_G/1000';



--changeset postgres:insert_formula_parameter_of_GW_SUB_M_KG context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3155 CB-DB: insert formula parameters of GW_SUB_M_KG



INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('GW_SUB_M_G') -- must be the same order as defined in the database function
    AND var.abbrev = 'GW_SUB_M_KG'
;

SELECT master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.formula_parameter 
--rollback where formula_id in (SELECT id FROM master.formula WHERE formula = 'GW_SUB_M_KG = GW_SUB_M_G/1000');



--changeset postgres:update_function_of_GW_SUB_M_KG context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3155 CB-DB: update function of GW_M_KG



CREATE OR REPLACE FUNCTION master.formula_gw_sub_m_kg(
    gw_sub_m_g float
) RETURNS float as
$body$
BEGIN
	RETURN gw_sub_m_g/1000;
END
$body$
language plpgsql;



--rollback CREATE OR REPLACE FUNCTION master.formula_gw_sub_m_kg(gw_sub_m_g float) RETURNS float as $body$ DECLARE gw_sub_m_kg float; local_gw_sub_m_g float; BEGIN gw_sub_m_kg=gw_sub_m_g/1000; RETURN round(gw_sub_m_kg::numeric, 3); END $body$ language plpgsql;