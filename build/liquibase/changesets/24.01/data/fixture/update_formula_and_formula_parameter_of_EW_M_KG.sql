--liquibase formatted sql

--changeset postgres:update_formula_of_EW_M_KG context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3153 CB-DB: Update formula of EW_M_KG



UPDATE 
    master.formula 
SET
    data_level = (SELECT data_level from master.variable where abbrev = 'EW_M_KG'),
    function_name = 'master.formula_ew_m_kg(ew_m_g)',
    database_formula = '
    CREATE OR REPLACE FUNCTION master.formula_ew_m_kg(
                ew_m_g float
			) RETURNS float as
			$body$
			BEGIN
				RETURN ew_m_g/1000;
			END
			$body$
			language plpgsql;'
WHERE formula = 'EW_M_KG = EW_M_G/1000';



--rollback UPDATE master.formula SET data_level = 'plot', database_formula = 'create or replace function master.formula_ew_m_kg(ew_sub_m_g float) returns float as $body$ declare ew_m_kg float; local_ew_m_g float; begin ew_m_kg = ew_m_g/1000; return round(ew_m_kg::numeric, 3); end $body$ language plpgsql;' WHERE formula = 'EW_M_KG = EW_M_G/1000';



--changeset postgres:insert_formula_parameter_of_EW_M_KG context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3153 CB-DB: insert formula parameters of EW_M_KG



INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('EW_M_G') -- must be the same order as defined in the database function
    AND var.abbrev = 'EW_M_KG'
;

select master.populate_order_number_for_master_formula_parameter();

--rollback DELETE FROM master.formula_parameter 
--rollback where formula_id in (SELECT id FROM master.formula WHERE formula = 'EW_M_KG = EW_M_G/1000');



--changeset postgres:update_function_of_EW_M_KG context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3153 CB-DB: update function of EW_M_KG



CREATE OR REPLACE FUNCTION master.formula_ew_m_kg(
    ew_m_g float
) RETURNS float as
$body$
BEGIN
	RETURN ew_m_g/1000;
END
$body$
language plpgsql;



--rollback CREATE OR REPLACE FUNCTION master.formula_ew_m_kg(ew_m_g float) RETURNS float as $body$ DECLARE ew_m_kg float; local_ew_sub_m_g float; BEGIN ew_m_kg = ew_m_g/1000; RETURN round(ew_m_kg::numeric, 3); END $body$ language plpgsql;