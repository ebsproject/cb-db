--liquibase formatted sql

--changeset postgres:add_another_formula_of_GW_CMP_KG context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3155 CB-DB: Add another formula of GW_CMP_KG



INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id)
    VALUES ('GW_CMP_KG=EW_M_KG*(SHELL_CMP_PCT/100)', (SELECT id FROM master.variable WHERE abbrev = 'GW_CMP_KG'), (SELECT id FROM master.method WHERE abbrev = 'GW_CMP_KG_METHOD'), 'plot', 'master.formula_gw_cmp_kg_1(ew_m_kg, shell_cmp_pct)', NULL, '
    CREATE OR REPLACE FUNCTION master.formula_gw_cmp_kg_1(
				ew_m_kg float,
                shell_cmp_pct float
			) RETURNS float as
			$body$
			BEGIN
				RETURN ew_m_kg*(shell_cmp_pct/100);
			END
			$body$
			language plpgsql;', '3', '1');

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('EW_M_KG','SHELL_CMP_PCT') -- must be the same order as defined in the database function
    AND var.abbrev = 'GW_CMP_KG'
;

SELECT master.populate_order_number_for_master_formula_parameter();



CREATE OR REPLACE FUNCTION master.formula_gw_cmp_kg_1(
				ew_m_kg float,
                shell_cmp_pct float
			) returns float as
			$body$
			BEGIN
				return ew_m_kg*(shell_cmp_pct/100);
			end
			$body$
			language plpgsql;



--rollback DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'GW_CMP_KG=EW_M_KG*(SHELL_CMP_PCT/100)');
--rollback DELETE FROM master.formula WHERE formula IN ('GW_CMP_KG=EW_M_KG*(SHELL_CMP_PCT/100)');
--rollback DROP FUNCTION master.formula_gw_cmp_kg_1;