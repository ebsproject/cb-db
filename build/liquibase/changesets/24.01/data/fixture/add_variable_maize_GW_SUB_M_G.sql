--liquibase formatted sql

--changeset postgres:add_variable_maize_GW_SUB_M_G context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3153 CB-DB: Insert GW_SUB_M_G variable



-- GW_SUB_M_G
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('GW_SUB_M_G', 'GW_SUB_M_G', 'Grain weight subsample', 'float', false, 'observation', 'plot', 'occurrence', 'Total weight of shelled grain from a subset of ears harvested from a plot measured in grams; used to calculate a shelling percentage per plot', 'active', '1', 'Grain weight subsample','24.01')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'GW_SUB_M_G' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('GW_SUB_M_G', 'Grain weight subsample', 'Grain weight subsample') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('GW_SUB_M_G_METHOD', 'Grain weight subsample method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('GW_SUB_M_G_SCALE', 'Grain weight subsample scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('GW_SUB_M_G');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('GW_SUB_M_G_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('GW_SUB_M_G');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('GW_SUB_M_G_SCALE');