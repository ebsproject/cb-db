--liquibase formatted sql

--changeset postgres:update_formula_of_SHELL_CMP_PCT context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3155 CB-DB: Update formula of SHELL_CMP_PCT



UPDATE 
    master.formula 
SET
    data_level = (SELECT data_level from master.variable where abbrev = 'SHELL_CMP_PCT'),
    function_name = 'master.formula_shell_cmp_pct(gw_sub_m_kg, ew_sub_m_kg)',
    database_formula = '
    CREATE OR REPLACE FUNCTION master.formula_shell_cmp_pct(
                gw_sub_m_kg float,
                ew_sub_m_kg float
			) RETURNS float as
			$body$
			BEGIN
				RETURN (gw_sub_m_kg/ew_sub_m_kg)*100;
			END
			$body$
			language plpgsql;'
WHERE formula = 'SHELL_CMP_PCT=(GW_SUB_M_KG/EW_SUB_M_KG)*100';



--rollback UPDATE master.formula SET data_level = 'plot', database_formula = 'create or replace function master.formula_shell_cmp_pct(gw_sub_m_kg float; ew_sub_m_kg float) returns float as $body$ declare shell_cmp_pct float; local_gw_sub_m_kg float; local_ew_sub_m_kg float; begin shell_cmp_pct=(gw_sub_m_kg/ew_sub_m_kg)*100; return round(shell_cmp_pct::numeric, 3); end $body$ language plpgsql;' WHERE formula = 'SHELL_CMP_PCT=(GW_SUB_M_KG/EW_SUB_M_KG)*100';



--changeset postgres:insert_formula_parameter_of_SHELL_CMP_PCT context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3155 CB-DB: insert formula parameter of SHELL_CMP_PCT



INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('GW_SUB_M_KG','EW_SUB_M_KG') -- must be the same order as defined in the database function
    AND var.abbrev = 'SHELL_CMP_PCT'
;

SELECT master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.formula_parameter 
--rollback where formula_id in (SELECT id FROM master.formula WHERE formula = 'SHELL_CMP_PCT=(GW_SUB_M_KG/EW_SUB_M_KG)*100');



--changeset postgres:update_function_of_SHELL_CMP_PCT context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3155 CB-DB: update function of SHELL_CMP_PCT



CREATE OR REPLACE FUNCTION master.formula_shell_cmp_pct(
    gw_sub_m_kg float,
    ew_sub_m_kg float
) RETURNS float as
$body$
BEGIN
	RETURN (gw_sub_m_kg/ew_sub_m_kg)*100;
END
$body$
language plpgsql;



--rollback CREATE OR REPLACE FUNCTION master.formula_shell_cmp_pct(gw_sub_m_kg float, ew_sub_m_kg float) RETURNS float as $body$ DECLARE shell_cmp_pct float; local_gw_sub_m_kg float; local_ew_sub_m_kg float; BEGIN shell_cmp_pct=(gw_sub_m_kg/ew_sub_m_kg)*100; RETURN round(shell_cmp_pct::numeric, 3); END $body$ language plpgsql