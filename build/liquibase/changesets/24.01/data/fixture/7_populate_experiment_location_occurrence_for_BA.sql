--liquibase formatted sql

--changeset postgres:7_populate_experiment_location_occurrence_for_BA context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3111: Populate experiment_location_occurrence for BA testing



INSERT INTO experiment.location_occurrence_group
(location_id,occurrence_id,order_number,creator_id)
 VALUES 
((select id from experiment.location where location_code = 'PH_AN_RM-2023-WS-001'),(select id from experiment.occurrence where occurrence_name = 'Unspecified2023_Exp1Sample1-003'),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1)),
((select id from experiment.location where location_code = 'PH_BO_UA-2023-WS-002'),(select id from experiment.occurrence where occurrence_name = 'Unspecified2023_Exp1Sample1-004'),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1)),
((select id from experiment.location where location_code = 'IRRIHQ-2023-WS-006'),(select id from experiment.occurrence where occurrence_name = 'Unspecified2023_Exp1Sample1-001'),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1)),
((select id from experiment.location where location_code = 'PH_NE_SM2-2023-WS-001'),(select id from experiment.occurrence where occurrence_name = 'Unspecified2023_Exp1Sample1-002'),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1));



--rollback DELETE FROM
--rollback experiment.location_occurrence_group
--rollback WHERE
--rollback location_id IN
--rollback ( SELECT id FROM experiment.location
--rollback   WHERE 
--rollback   location_code in (
--rollback   'IRRIHQ-2023-WS-006',
--rollback   'PH_NE_SM2-2023-WS-001',
--rollback   'PH_AN_RM-2023-WS-001',
--rollback   'PH_BO_UA-2023-WS-002'
--rollback   )
--rollback );