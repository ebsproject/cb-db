--liquibase formatted sql

--changeset postgres:2_populate_experiment_entry_list_for_BA context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3111: Populate experiment_entry_list for BA testing



INSERT INTO experiment.entry_list
(entry_list_code,entry_list_name,description,entry_list_status,experiment_id,creator_id,entry_list_type,cross_count,experiment_year,season_id,site_id,crop_id,program_id)
 VALUES 
((experiment.generate_code('entry_list')),'Unspecified2023_Exp1Sample1',NULL,'completed',(SELECT id FROM experiment.experiment WHERE experiment_name='Unspecified2023_Exp1Sample1' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),'entry list',0,NULL,NULL,NULL,(SELECT id FROM tenant.crop WHERE crop_code='RICE' LIMIT 1),(SELECT id FROM tenant.program WHERE program_code='IRSEA' LIMIT 1));



--rollback DELETE FROM
--rollback experiment.entry_list
--rollback WHERE
--rollback entry_list_name = 'Unspecified2023_Exp1Sample1';