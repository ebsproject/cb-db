--liquibase formatted sql

--changeset postgres:update_GMOI_M_PCT_variable context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3148 CB-DB: Update name, type, and data_level value of variable GMOI_M_PCT



UPDATE master.variable
SET
 name = 'Grain moisture',
 type = 'observation',
 usage = 'occurrence',
 data_level = 'plot',
 description = 'Measured grain moisture percentage'
WHERE abbrev = 'GMOI_M_PCT'
;



--rollback UPDATE master.variable
--rollback SET
--rollback  name = 'Harvested plot area',
--rollback  type = 'system',
--rollback  usage = 'application',
--rollback  data_level = 'experiment, occurrence, location, plot',
--rollback  description = 'Measured Harvested plot area percentage'
--rollback WHERE abbrev = 'GMOI_M_PCT'
--rollback ;



--changeset postgres:update_GMOI_M_PCT_property context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3148 CB-DB: Update name and display_name value of GMOI_M_PCT property



UPDATE master.property
SET
 name = 'Grain moisture',
 display_name = 'Grain moisture'
WHERE abbrev = 'GMOI_M_PCT'
;



--rollback UPDATE master.property
--rollback SET
--rollback  name = 'Harvested plot area',
--rollback  display_name = 'Harvested plot area'
--rollback WHERE abbrev = 'GMOI_M_PCT'
--rollback ;



--changeset postgres:update_GMOI_M_PCT_method context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3148 CB-DB: Update name value of GMOI_M_PCT method



UPDATE master.method
SET
 name = 'Grain moisture method'
WHERE abbrev = 'GMOI_M_PCT_METHOD'
;



--rollback UPDATE master.method
--rollback SET
--rollback  name = 'Harvested plot area method'
--rollback WHERE abbrev = 'GMOI_M_PCT_METHOD'
--rollback ;



--changeset postgres:update_GMOI_M_PCT_scale context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3148 CB-DB: Update name value of GMOI_M_PCT scale



UPDATE master.scale
SET
 name = 'Grain moisture scale'
WHERE abbrev = 'GMOI_M_PCT_SCALE'
;



--rollback UPDATE master.method
--rollback SET
--rollback  name = 'Harvested plot area scale'
--rollback WHERE abbrev = 'GMOI_M_PCT_SCALE'
--rollback ;