--liquibase formatted sql

--changeset postgres:update_gm_coding_germplasm_name_config_WHEAT context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:1 SELECT CASE EXISTS(SELECT 1 FROM platform.config WHERE abbrev = 'GM_CROP_WHEAT_GERMPLASM_CODING_NAME_TYPE_CONFIG') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-3401: CB-GM: Germplasm coding UI does not display the list of germplasm name type to proceed for coding in WEE



UPDATE 
    platform.config
SET
    config_value = $${
            "germplasm_name_type": [
                "GERMPLASM_NAME_TYPE_HYBRID_CODE",
                "GERMPLASM_NAME_TYPE_LINE_CODE",
                "GERMPLASM_NAME_TYPE_LINE_NAME",
                "GERMPLASM_NAME_TYPE_ELITE_LINES",
                "GERMPLASM_NAME_TYPE_CROSS_NAME"
            ]
        }$$
WHERE
	abbrev = 'GM_CROP_WHEAT_GERMPLASM_CODING_NAME_TYPE_CONFIG'
;




--rollback UPDATE 
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback      "germplasm_name_type": [
--rollback          "GERMPLASM_NAME_TYPE_HYBRID_CODE",
--rollback          "GERMPLASM_NAME_TYPE_LINE_CODE",
--rollback          "GERMPLASM_NAME_TYPE_LINE_NAME",
--rollback          "GERMPLASM_NAME_TYPE_ELITE_LINES"
--rollback      ]
--rollback  }$$
--rollback WHERE
--rollback 	abbrev = 'GM_CROP_WHEAT_GERMPLASM_CODING_NAME_TYPE_CONFIG'
--rollback ;