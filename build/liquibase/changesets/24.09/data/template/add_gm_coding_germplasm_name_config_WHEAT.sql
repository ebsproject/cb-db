--liquibase formatted sql

--changeset postgres:add_gm_coding_germplasm_name_config_WHEAT.sql context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM platform.config WHERE abbrev = 'GM_CROP_WHEAT_GERMPLASM_CODING_NAME_TYPE_CONFIG') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-3401: CB-GM: Germplasm coding UI does not display the list of germplasm name type to proceed for coding in WEE



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'GM_CROP_WHEAT_GERMPLASM_CODING_NAME_TYPE_CONFIG',
        'Germplasm Manager Germplasm Coding GERMPLASM_NAME_TYPE WHEAT config',
        $${
            "germplasm_name_type": [
                "GERMPLASM_NAME_TYPE_HYBRID_CODE",
                "GERMPLASM_NAME_TYPE_LINE_CODE",
                "GERMPLASM_NAME_TYPE_LINE_NAME",
                "GERMPLASM_NAME_TYPE_ELITE_LINES"
            ]
        }$$,
        1,
        'Germplasm Manager Germplasm Coding GERMPLASM_NAME_TYPE options',
        (
            SELECT 
                id
            FROM
                tenant.person
            WHERE 
                person_name = 'EBS, Admin'
        ),
        'BDS-3401: CB-GM: Germplasm coding UI does not display the list of germplasm name type to proceed for coding in WEE - k.delarosa'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='GM_CROP_WHEAT_GERMPLASM_CODING_NAME_TYPE_CONFIG';