--liquibase formatted sql

--changeset postgres:update_enable_rbac_flag context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3413 Enable RBAC by default



UPDATE 
    platform.config
SET
    config_value = $$true$$
WHERE
	abbrev = 'ENABLE_RBAC_FLAG'
;



--rollback UPDATE 
--rollback     platform.config
--rollback SET
--rollback     config_value = $$false$$
--rollback WHERE
--rollback 	abbrev = 'ENABLE_RBAC_FLAG'
--rollback ;
