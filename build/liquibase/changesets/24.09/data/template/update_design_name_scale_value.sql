--liquibase formatted sql

--changeset postgres:update_design_name_scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3538 Update design name scale values


UPDATE 
    master.scale_value 
SET 
    value = $$Augmented$$
WHERE 
    abbrev = 'DESIGN_AUGMENTED_DESIGN' AND 
    scale_id = (
        SELECT 
            v.scale_id 
        FROM 
            master.variable v 
        WHERE 
            v.abbrev='DESIGN'
        )
;


UPDATE 
    master.scale_value 
SET 
    value = $$Incomplete Block$$
WHERE 
    abbrev = 'DESIGN_INCOMPLETE_BLOCK' AND 
    scale_id = (
        SELECT 
            v.scale_id 
        FROM 
            master.variable v 
        WHERE 
            v.abbrev='DESIGN'
        )
;



--rollback   UPDATE 
--rollback       master.scale_value 
--rollback   SET 
--rollback       value = $$Augmented Design$$
--rollback   WHERE 
--rollback       abbrev = 'DESIGN_AUGMENTED_DESIGN' AND 
--rollback       scale_id = (
--rollback           SELECT 
--rollback               v.scale_id 
--rollback           FROM 
--rollback               master.variable v 
--rollback           WHERE 
--rollback               v.abbrev='DESIGN'
--rollback           )
--rollback   ;
--rollback   
--rollback   
--rollback   UPDATE 
--rollback       master.scale_value 
--rollback   SET 
--rollback       value = $$Incomplete Block Design$$
--rollback   WHERE 
--rollback       abbrev = 'DESIGN_INCOMPLETE_BLOCK' AND 
--rollback       scale_id = (
--rollback           SELECT 
--rollback               v.scale_id 
--rollback           FROM 
--rollback               master.variable v 
--rollback           WHERE 
--rollback               v.abbrev='DESIGN'
--rollback           )
--rollback   ;