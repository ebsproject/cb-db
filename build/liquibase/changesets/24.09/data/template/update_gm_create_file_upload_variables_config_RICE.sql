--liquibase formatted sql

--changeset postgres:update_gm_create_file_upload_variables_config_RICE context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3294 CB-GM:  uploading germplasm for other crop in the germplasm CREATE has error



UPDATE 
    platform.config
SET
    config_value = $${
        "values": [
            {
                "name": "Germplasm Type",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "germplasm"
                    ]
                },
                "usage": "optional",
                "abbrev": "GERMPLASM_TYPE",
                "entity": "germplasm",
                "required": "false",
                "api_field": "germplasmType",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Program Code",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "seed"
                    ]
                },
                "usage": "optional",
                "abbrev": "PROGRAM_CODE",
                "entity": "seed",
                "required": "false",
                "api_field": "programDbId",
                "data_type": "string",
                "retrieve_db_id": "true",
                "retrieve_endpoint": "programs",
                "retrieve_api_value_field": "programDbId",
                "retrieve_api_search_field": "programCode"
            },
            {
                "name": "Harvest Date",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "seed"
                    ]
                },
                "usage": "optional",
                "abbrev": "HVDATE_CONT",
                "entity": "seed",
                "required": "false",
                "api_field": "harvestDate",
                "data_type": "date",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Harvest Method",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "seed"
                    ]
                },
                "usage": "optional",
                "abbrev": "HV_METH_DISC",
                "entity": "seed",
                "required": "false",
                "api_field": "harvestMethod",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Description",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "seed"
                    ]
                },
                "usage": "optional",
                "abbrev": "DESCRIPTION",
                "entity": "seed",
                "required": "false",
                "api_field": "description",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "MTA Status",
                "type": "attribute",
                "view": {
                    "visible": "false",
                    "entities": []
                },
                "usage": "optional",
                "abbrev": "MTA_STATUS",
                "entity": "seed",
                "required": "false",
                "api_field": "dataValue",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "IP Status",
                "type": "attribute",
                "view": {
                    "visible": "false",
                    "entities": []
                },
                "usage": "optional",
                "abbrev": "IP_STATUS",
                "entity": "seed",
                "required": "false",
                "api_field": "dataValue",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Source Organization",
                "type": "attribute",
                "view": {
                    "visible": "false",
                    "entities": []
                },
                "usage": "optional",
                "abbrev": "SOURCE_ORGANIZATION",
                "entity": "seed",
                "required": "false",
                "api_field": "dataValue",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Origin",
                "type": "attribute",
                "view": {
                    "visible": "false",
                    "entities": []
                },
                "usage": "optional",
                "abbrev": "ORIGIN",
                "entity": "seed",
                "required": "false",
                "api_field": "dataValue",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
                },
                {
                "name": "Source Study",
                "type": "attribute",
                "view": {
                    "visible": "false",
                    "entities": []
                },
                "usage": "optional",
                "abbrev": "SOURCE_STUDY",
                "entity": "seed",
                "required": "false",
                "api_field": "dataValue",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Source Harvest Year",
                "type": "attribute",
                "view": {
                    "visible": "false",
                    "entities": []
                },
                "usage": "optional",
                "abbrev": "SOURCE_HARV_YEAR",
                "entity": "seed",
                "required": "false",
                "api_field": "dataValue",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "MTA Number",
                "type": "attribute",
                "view": {
                    "visible": "false",
                    "entities": []
                },
                "usage": "optional",
                "abbrev": "MTA_NUMBER",
                "entity": "seed",
                "required": "false",
                "api_field": "dataValue",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Import Attribute",
                "type": "attribute",
                "view": {
                    "visible": "false",
                    "entities": []
                },
                "usage": "optional",
                "abbrev": "IMPORT_ATTRIBUTE",
                "entity": "seed",
                "required": "false",
                "api_field": "dataValue",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            }
        ]
    }$$
WHERE
	abbrev = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_RICE_DEFAULT'
;




--rollback UPDATE 
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback    "values": [
--rollback      {
--rollback        "name": "Germplasm Type",
--rollback        "type": "column",
--rollback        "view": {
--rollback          "visible": "true",
--rollback          "entities": [
--rollback            "germplasm"
--rollback          ]
--rollback        },
--rollback        "usage": "optional",
--rollback        "abbrev": "GERMPLASM_TYPE",
--rollback        "entity": "germplasm",
--rollback        "required": "false",
--rollback        "api_field": "germplasmType",
--rollback        "data_type": "string",
--rollback        "retrieve_db_id": "false",
--rollback        "retrieve_endpoint": "",
--rollback        "retrieve_api_value_field": "",
--rollback        "retrieve_api_search_field": ""
--rollback      },
--rollback      {
--rollback        "name": "Program Code",
--rollback        "type": "column",
--rollback        "view": {
--rollback          "visible": "true",
--rollback          "entities": [
--rollback            "seed"
--rollback          ]
--rollback        },
--rollback        "usage": "optional",
--rollback        "abbrev": "PROGRAM_CODE",
--rollback        "entity": "seed",
--rollback        "required": "false",
--rollback        "api_field": "programDbId",
--rollback        "data_type": "string",
--rollback        "retrieve_db_id": "true",
--rollback        "retrieve_endpoint": "programs",
--rollback        "retrieve_api_value_field": "programDbId",
--rollback        "retrieve_api_search_field": "programCode"
--rollback      },
--rollback      {
--rollback        "name": "Harvest Date",
--rollback        "type": "column",
--rollback        "view": {
--rollback          "visible": "true",
--rollback          "entities": [
--rollback            "seed"
--rollback          ]
--rollback        },
--rollback        "usage": "optional",
--rollback        "abbrev": "HVDATE_CONT",
--rollback        "entity": "seed",
--rollback        "required": "false",
--rollback        "api_field": "harvestDate",
--rollback        "data_type": "date",
--rollback        "retrieve_db_id": "false",
--rollback        "retrieve_endpoint": "",
--rollback        "retrieve_api_value_field": "",
--rollback        "retrieve_api_search_field": ""
--rollback      },
--rollback      {
--rollback        "name": "Harvest Method",
--rollback        "type": "column",
--rollback        "view": {
--rollback          "visible": "true",
--rollback          "entities": [
--rollback            "seed"
--rollback          ]
--rollback        },
--rollback        "usage": "optional",
--rollback        "abbrev": "HV_METH_DISC",
--rollback        "entity": "seed",
--rollback        "required": "false",
--rollback        "api_field": "harvestMethod",
--rollback        "data_type": "string",
--rollback        "retrieve_db_id": "false",
--rollback        "retrieve_endpoint": "",
--rollback        "retrieve_api_value_field": "",
--rollback        "retrieve_api_search_field": ""
--rollback      },
--rollback      {
--rollback        "name": "Description",
--rollback        "type": "column",
--rollback        "view": {
--rollback          "visible": "true",
--rollback          "entities": [
--rollback            "seed"
--rollback          ]
--rollback        },
--rollback        "usage": "optional",
--rollback        "abbrev": "DESCRIPTION",
--rollback        "entity": "seed",
--rollback        "required": "false",
--rollback        "api_field": "description",
--rollback        "data_type": "string",
--rollback        "retrieve_db_id": "false",
--rollback        "retrieve_endpoint": "",
--rollback        "retrieve_api_value_field": "",
--rollback        "retrieve_api_search_field": ""
--rollback      },
--rollback      {
--rollback        "name": "MTA Status",
--rollback        "type": "attribute",
--rollback        "view": {
--rollback          "visible": "false",
--rollback          "entities": []
--rollback        },
--rollback        "usage": "optional",
--rollback        "abbrev": "MTA_STATUS",
--rollback        "entity": "seed",
--rollback        "required": "false",
--rollback        "api_field": "dataValue",
--rollback        "data_type": "string",
--rollback        "retrieve_db_id": "false",
--rollback        "retrieve_endpoint": "",
--rollback        "retrieve_api_value_field": "",
--rollback        "retrieve_api_search_field": ""
--rollback      },
--rollback      {
--rollback        "name": "IP Status",
--rollback        "type": "attribute",
--rollback        "view": {
--rollback          "visible": "false",
--rollback          "entities": []
--rollback        },
--rollback        "usage": "optional",
--rollback        "abbrev": "IP_STATUS",
--rollback        "entity": "seed",
--rollback        "required": "false",
--rollback        "api_field": "dataValue",
--rollback        "data_type": "string",
--rollback        "retrieve_db_id": "false",
--rollback        "retrieve_endpoint": "",
--rollback        "retrieve_api_value_field": "",
--rollback        "retrieve_api_search_field": ""
--rollback      },
--rollback      {
--rollback        "name": "Source Organization",
--rollback        "type": "attribute",
--rollback        "view": {
--rollback          "visible": "false",
--rollback          "entities": []
--rollback        },
--rollback        "usage": "optional",
--rollback        "abbrev": "SOURCE_ORGANIZATION",
--rollback        "entity": "seed",
--rollback        "required": "false",
--rollback        "api_field": "dataValue",
--rollback        "data_type": "string",
--rollback        "retrieve_db_id": "false",
--rollback        "retrieve_endpoint": "",
--rollback        "retrieve_api_value_field": "",
--rollback        "retrieve_api_search_field": ""
--rollback      },
--rollback      {
--rollback        "name": "Origin",
--rollback        "type": "attribute",
--rollback        "view": {
--rollback          "visible": "false",
--rollback          "entities": []
--rollback        },
--rollback        "usage": "optional",
--rollback        "abbrev": "ORIGIN",
--rollback        "entity": "seed",
--rollback        "required": "false",
--rollback        "api_field": "dataValue",
--rollback        "data_type": "string",
--rollback        "retrieve_db_id": "false",
--rollback        "retrieve_endpoint": "",
--rollback        "retrieve_api_value_field": "",
--rollback        "retrieve_api_search_field": ""
--rollback      },
--rollback      {
--rollback        "name": "Source Study",
--rollback        "type": "attribute",
--rollback        "view": {
--rollback          "visible": "false",
--rollback          "entities": []
--rollback        },
--rollback        "usage": "optional",
--rollback        "abbrev": "SOURCE_STUDY",
--rollback        "entity": "seed",
--rollback        "required": "false",
--rollback        "api_field": "dataValue",
--rollback        "data_type": "string",
--rollback        "retrieve_db_id": "false",
--rollback        "retrieve_endpoint": "",
--rollback        "retrieve_api_value_field": "",
--rollback        "retrieve_api_search_field": ""
--rollback      },
--rollback      {
--rollback        "name": "Source Harvest Year",
--rollback        "type": "attribute",
--rollback        "view": {
--rollback          "visible": "false",
--rollback          "entities": []
--rollback        },
--rollback        "usage": "optional",
--rollback        "abbrev": "SOURCE_HARV_YEAR",
--rollback        "entity": "seed",
--rollback        "required": "false",
--rollback        "api_field": "dataValue",
--rollback        "data_type": "string",
--rollback        "retrieve_db_id": "false",
--rollback        "retrieve_endpoint": "",
--rollback        "retrieve_api_value_field": "",
--rollback        "retrieve_api_search_field": ""
--rollback      },
--rollback      {
--rollback        "name": "MTA Number",
--rollback        "type": "attribute",
--rollback        "view": {
--rollback          "visible": "false",
--rollback          "entities": []
--rollback        },
--rollback        "usage": "optional",
--rollback        "abbrev": "MTA_NUMBER",
--rollback        "entity": "seed",
--rollback        "required": "false",
--rollback        "api_field": "dataValue",
--rollback        "data_type": "string",
--rollback        "retrieve_db_id": "false",
--rollback        "retrieve_endpoint": "",
--rollback        "retrieve_api_value_field": "",
--rollback        "retrieve_api_search_field": ""
--rollback      },
--rollback      {
--rollback        "name": "Import Attribute",
--rollback        "type": "attribute",
--rollback        "view": {
--rollback          "visible": "false",
--rollback          "entities": []
--rollback        },
--rollback        "usage": "optional",
--rollback        "abbrev": "IMPORT_ATTRIBUTE",
--rollback        "entity": "seed",
--rollback        "required": "false",
--rollback        "api_field": "dataValue",
--rollback        "data_type": "string",
--rollback        "retrieve_db_id": "false",
--rollback        "retrieve_endpoint": "",
--rollback        "retrieve_api_value_field": "",
--rollback        "retrieve_api_search_field": ""
--rollback      },
--rollback      {
--rollback        "name": "Package Quantity",
--rollback        "type": "column",
--rollback        "view": {
--rollback          "visible": "true",
--rollback          "entities": [
--rollback            "package"
--rollback          ]
--rollback        },
--rollback        "usage": "required",
--rollback        "abbrev": "VOLUME",
--rollback        "entity": "package",
--rollback        "required": "true",
--rollback        "api_field": "packageQuantity",
--rollback        "data_type": "float",
--rollback        "retrieve_db_id": "false",
--rollback        "retrieve_endpoint": "",
--rollback        "retrieve_api_value_field": "",
--rollback        "retrieve_api_search_field": ""
--rollback      },
--rollback      {
--rollback        "name": "Package Unit",
--rollback        "type": "column",
--rollback        "view": {
--rollback          "visible": "true",
--rollback          "entities": [
--rollback            "package"
--rollback          ]
--rollback        },
--rollback        "usage": "required",
--rollback        "abbrev": "PACKAGE_UNIT",
--rollback        "entity": "package",
--rollback        "required": "true",
--rollback        "api_field": "packageUnit",
--rollback        "data_type": "string",
--rollback        "retrieve_db_id": "false",
--rollback        "retrieve_endpoint": "",
--rollback        "retrieve_api_value_field": "",
--rollback        "retrieve_api_search_field": ""
--rollback      }
--rollback    ]
--rollback  }$$
--rollback WHERE
--rollback 	abbrev = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_RICE_DEFAULT'
--rollback ;