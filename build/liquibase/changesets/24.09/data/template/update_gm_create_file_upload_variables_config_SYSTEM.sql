--liquibase formatted sql

--changeset postgres:update_gm_create_file_upload_variables_config_SYSTEM context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3294 CB-GM:  uploading germplasm for other crop in the germplasm CREATE has error



UPDATE 
    platform.config
SET
    config_value = $${
        "values": [
            {
                "name": "Germplasm Name",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "germplasm",
                    "seed",
                    "package"
                    ]
                },
                "usage": "required",
                "abbrev": "GERMPLASM_NAME",
                "entity": "germplasm",
                "required": "true",
                "api_field": "designation",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Germplasm Name Type",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "germplasm"
                    ]
                },
                "usage": "required",
                "abbrev": "GERMPLASM_NAME_TYPE",
                "entity": "germplasm",
                "required": "true",
                "api_field": "nameType",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Parentage",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "germplasm"
                    ]
                },
                "usage": "required",
                "abbrev": "PARENTAGE",
                "entity": "germplasm",
                "required": "true",
                "api_field": "parentage",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Generation",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "germplasm"
                    ]
                },
                "usage": "required",
                "abbrev": "GENERATION",
                "entity": "germplasm",
                "required": "true",
                "api_field": "generation",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Germplasm State",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "germplasm"
                    ]
                },
                "usage": "required",
                "abbrev": "GERMPLASM_STATE",
                "entity": "germplasm",
                "required": "true",
                "api_field": "state",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Taxon ID",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "germplasm"
                    ]
                },
                "usage": "required",
                "abbrev": "TAXON_ID",
                "entity": "germplasm",
                "required": "true",
                "api_field": "taxonomyDbId",
                "data_type": "string",
                "retrieve_db_id": "true",
                "retrieve_endpoint": "taxonomies",
                "retrieve_api_value_field": "taxonomyDbId",
                "retrieve_api_search_field": "taxonId"
            },
            {
                "name": "Seed Name",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "seed"
                    ]
                },
                "usage": "required",
                "abbrev": "SEED_NAME",
                "entity": "seed",
                "required": "true",
                "api_field": "seedName",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Package Label",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "package"
                    ]
                },
                "usage": "required",
                "abbrev": "PACKAGE_LABEL",
                "entity": "package",
                "required": "true",
                "api_field": "packageLabel",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Program",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "package"
                    ]
                },
                "usage": "required",
                "abbrev": "PROGRAM",
                "entity": "package",
                "required": "true",
                "api_field": "programDbId",
                "data_type": "string",
                "retrieve_db_id": "true",
                "retrieve_endpoint": "programs",
                "retrieve_api_value_field": "programDbId",
                "retrieve_api_search_field": "programCode"
            },
            {
                "name": "Package Status",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "package"
                    ]
                },
                "usage": "required",
                "abbrev": "PACKAGE_STATUS",
                "entity": "package",
                "required": "true",
                "api_field": "packageStatus",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Package Quantity",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "package"
                    ]
                },
                "usage": "required",
                "abbrev": "VOLUME",
                "entity": "package",
                "required": "true",
                "api_field": "packageQuantity",
                "data_type": "float",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            },
            {
                "name": "Package Unit",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "package"
                    ]
                },
                "usage": "required",
                "abbrev": "PACKAGE_UNIT",
                "entity": "package",
                "required": "true",
                "api_field": "packageUnit",
                "data_type": "string",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_value_field": "",
                "retrieve_api_search_field": ""
            }
        ]
    }$$
WHERE
	abbrev = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_SYSTEM_DEFAULT'
;




--rollback UPDATE 
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback      "values": [
--rollback          {
--rollback              "name": "Germplasm Name",
--rollback              "type": "column",
--rollback              "view": {
--rollback                  "visible": "true",
--rollback                  "entities": [
--rollback                  "germplasm",
--rollback                  "seed",
--rollback                  "package"
--rollback                  ]
--rollback              },
--rollback              "usage": "required",
--rollback              "abbrev": "GERMPLASM_NAME",
--rollback              "entity": "germplasm",
--rollback              "required": "true",
--rollback              "api_field": "designation",
--rollback              "data_type": "string",
--rollback              "retrieve_db_id": "false",
--rollback              "retrieve_endpoint": "",
--rollback              "retrieve_api_value_field": "",
--rollback              "retrieve_api_search_field": ""
--rollback          },
--rollback          {
--rollback              "name": "Germplasm Name Type",
--rollback              "type": "column",
--rollback              "view": {
--rollback                  "visible": "true",
--rollback                  "entities": [
--rollback                  "germplasm"
--rollback                  ]
--rollback              },
--rollback              "usage": "required",
--rollback              "abbrev": "GERMPLASM_NAME_TYPE",
--rollback              "entity": "germplasm",
--rollback              "required": "true",
--rollback              "api_field": "nameType",
--rollback              "data_type": "string",
--rollback              "retrieve_db_id": "false",
--rollback              "retrieve_endpoint": "",
--rollback              "retrieve_api_value_field": "",
--rollback              "retrieve_api_search_field": ""
--rollback          },
--rollback          {
--rollback              "name": "Parentage",
--rollback              "type": "column",
--rollback              "view": {
--rollback                  "visible": "true",
--rollback                  "entities": [
--rollback                  "germplasm"
--rollback                  ]
--rollback              },
--rollback              "usage": "required",
--rollback              "abbrev": "PARENTAGE",
--rollback              "entity": "germplasm",
--rollback              "required": "true",
--rollback              "api_field": "parentage",
--rollback              "data_type": "string",
--rollback              "retrieve_db_id": "false",
--rollback              "retrieve_endpoint": "",
--rollback              "retrieve_api_value_field": "",
--rollback              "retrieve_api_search_field": ""
--rollback          },
--rollback          {
--rollback              "name": "Generation",
--rollback              "type": "column",
--rollback              "view": {
--rollback                  "visible": "true",
--rollback                  "entities": [
--rollback                  "germplasm"
--rollback                  ]
--rollback              },
--rollback              "usage": "required",
--rollback              "abbrev": "GENERATION",
--rollback              "entity": "germplasm",
--rollback              "required": "true",
--rollback              "api_field": "generation",
--rollback              "data_type": "string",
--rollback              "retrieve_db_id": "false",
--rollback              "retrieve_endpoint": "",
--rollback              "retrieve_api_value_field": "",
--rollback              "retrieve_api_search_field": ""
--rollback          },
--rollback          {
--rollback              "name": "Germplasm State",
--rollback              "type": "column",
--rollback              "view": {
--rollback                  "visible": "true",
--rollback                  "entities": [
--rollback                  "germplasm"
--rollback                  ]
--rollback              },
--rollback              "usage": "required",
--rollback              "abbrev": "GERMPLASM_STATE",
--rollback              "entity": "germplasm",
--rollback              "required": "true",
--rollback              "api_field": "state",
--rollback              "data_type": "string",
--rollback              "retrieve_db_id": "false",
--rollback              "retrieve_endpoint": "",
--rollback              "retrieve_api_value_field": "",
--rollback              "retrieve_api_search_field": ""
--rollback          },
--rollback          {
--rollback              "name": "Taxon ID",
--rollback              "type": "column",
--rollback              "view": {
--rollback                  "visible": "true",
--rollback                  "entities": [
--rollback                  "germplasm"
--rollback                  ]
--rollback              },
--rollback              "usage": "required",
--rollback              "abbrev": "TAXON_ID",
--rollback              "entity": "germplasm",
--rollback              "required": "true",
--rollback              "api_field": "taxonomyDbId",
--rollback              "data_type": "string",
--rollback              "retrieve_db_id": "true",
--rollback              "retrieve_endpoint": "taxonomies",
--rollback              "retrieve_api_value_field": "taxonomyDbId",
--rollback              "retrieve_api_search_field": "taxonId"
--rollback          },
--rollback          {
--rollback              "name": "Seed Name",
--rollback              "type": "column",
--rollback              "view": {
--rollback                  "visible": "true",
--rollback                  "entities": [
--rollback                  "seed"
--rollback                  ]
--rollback              },
--rollback              "usage": "required",
--rollback              "abbrev": "SEED_NAME",
--rollback              "entity": "seed",
--rollback              "required": "true",
--rollback              "api_field": "seedName",
--rollback              "data_type": "string",
--rollback              "retrieve_db_id": "false",
--rollback              "retrieve_endpoint": "",
--rollback              "retrieve_api_value_field": "",
--rollback              "retrieve_api_search_field": ""
--rollback          },
--rollback          {
--rollback              "name": "Package Label",
--rollback              "type": "column",
--rollback              "view": {
--rollback                  "visible": "true",
--rollback                  "entities": [
--rollback                  "package"
--rollback                  ]
--rollback              },
--rollback              "usage": "required",
--rollback              "abbrev": "PACKAGE_LABEL",
--rollback              "entity": "package",
--rollback              "required": "true",
--rollback              "api_field": "packageLabel",
--rollback              "data_type": "string",
--rollback              "retrieve_db_id": "false",
--rollback              "retrieve_endpoint": "",
--rollback              "retrieve_api_value_field": "",
--rollback              "retrieve_api_search_field": ""
--rollback          },
--rollback          {
--rollback              "name": "Program",
--rollback              "type": "column",
--rollback              "view": {
--rollback                  "visible": "true",
--rollback                  "entities": [
--rollback                  "package"
--rollback                  ]
--rollback              },
--rollback              "usage": "required",
--rollback              "abbrev": "PROGRAM",
--rollback              "entity": "package",
--rollback              "required": "true",
--rollback              "api_field": "programDbId",
--rollback              "data_type": "string",
--rollback              "retrieve_db_id": "true",
--rollback              "retrieve_endpoint": "programs",
--rollback              "retrieve_api_value_field": "programDbId",
--rollback              "retrieve_api_search_field": "programCode"
--rollback          },
--rollback          {
--rollback              "name": "Package Status",
--rollback              "type": "column",
--rollback              "view": {
--rollback                  "visible": "true",
--rollback                  "entities": [
--rollback                  "package"
--rollback                  ]
--rollback              },
--rollback              "usage": "required",
--rollback              "abbrev": "PACKAGE_STATUS",
--rollback              "entity": "package",
--rollback              "required": "true",
--rollback              "api_field": "packageStatus",
--rollback              "data_type": "string",
--rollback              "retrieve_db_id": "false",
--rollback              "retrieve_endpoint": "",
--rollback              "retrieve_api_value_field": "",
--rollback              "retrieve_api_search_field": ""
--rollback          }
--rollback      ]
--rollback  }$$
--rollback WHERE
--rollback 	abbrev = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_SYSTEM_DEFAULT'
--rollback ;