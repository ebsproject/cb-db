--liquibase formatted sql

--changeset postgres:update_disable_rbac_flag context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3413 Disable RBAC by default



UPDATE 
    platform.config
SET
    config_value = $$false$$
WHERE
	abbrev = 'ENABLE_RBAC_FLAG'
;



--rollback UPDATE 
--rollback     platform.config
--rollback SET
--rollback     config_value = $$true$$
--rollback WHERE
--rollback 	abbrev = 'ENABLE_RBAC_FLAG'
--rollback ;
