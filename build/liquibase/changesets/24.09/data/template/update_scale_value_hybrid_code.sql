--liquibase formatted sql

--changeset postgres:update_scale_value_hybrid_code context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3405 CB GM: Update retrieval and consumption of CS sequences



UPDATE 
    master.scale_value
SET
    display_name = $$hybrid_code$$
WHERE
	abbrev = 'GERMPLASM_NAME_TYPE_HYBRID_CODE'
;



--rollback UPDATE 
--rollback     master.scale_value
--rollback SET
--rollback     display_name = $$Hybrid Code$$
--rollback WHERE
--rollback 	abbrev = 'GERMPLASM_NAME_TYPE_HYBRID_CODE'
--rollback ;
