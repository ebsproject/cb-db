--liquibase formatted sql

--changeset postgres:S24_updated_experiment_experiment_block_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-3040 Populate updated experiment_experiment_block



INSERT INTO experiment.experiment_block
(experiment_block_code,experiment_block_name,experiment_id,parent_experiment_block_id,order_number,block_type,no_of_blocks,no_of_ranges,no_of_cols,no_of_reps,plot_numbering_order,starting_corner,creator_id,is_void,notes)
 VALUES 
('VOIDED-423-423-134','Nursery block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F4-2020-DS-001' LIMIT 1),NULL,1,'parent',0,NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),True,NULL),
((SELECT experiment_code||'-EO-1' FROM experiment.experiment WHERE experiment_name = 'SYB-F4-2020-DS-001'),'Entry order block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F4-2020-DS-001' LIMIT 1),NULL,1,'parent',0,0.0,0.0,NULL,'Column serpentine','Top Left',(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'[]'),
('VOIDED-324-324-35','Nursery block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F1-2018-WS-001' LIMIT 1),NULL,1,'parent',0,NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),True,NULL),
((SELECT experiment_code||'-EO-1' FROM experiment.experiment WHERE experiment_name = 'SYB-F1-2018-WS-001'),'Entry order block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F1-2018-WS-001' LIMIT 1),NULL,1,'parent',0,0.0,0.0,NULL,'Column serpentine','Top Left',(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'[]'),
((SELECT experiment_code||'-EO-1' FROM experiment.experiment WHERE experiment_name = 'SYB-F2-2019-DS-001'),'Entry order block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F2-2019-DS-001' LIMIT 1),NULL,1,'parent',0,0.0,0.0,NULL,'Column serpentine','Top Left',(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'[]'),
('VOIDED-357-357-68','Nursery block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F2-2019-DS-001' LIMIT 1),NULL,1,'parent',0,NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),True,NULL),
((SELECT experiment_code||'-EO-1' FROM experiment.experiment WHERE experiment_name = 'SYB-F3-2019-WS-001'),'Entry order block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F3-2019-WS-001' LIMIT 1),NULL,1,'parent',0,0.0,0.0,NULL,'Column serpentine','Top Left',(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'[]'),
('VOIDED-390-390-101','Nursery block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F3-2019-WS-001' LIMIT 1),NULL,1,'parent',0,NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),True,NULL),
((SELECT experiment_code||'-EO-1' FROM experiment.experiment WHERE experiment_name = 'SYB-SEM-2018-DS-001'),'Entry order block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-SEM-2018-DS-001' LIMIT 1),NULL,1,'parent',0,0.0,0.0,NULL,'Column serpentine','Top Left',(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'[]'),
('VOIDED-555-555-235','Nursery block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-SEM-2018-DS-001' LIMIT 1),NULL,1,'parent',0,NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),True,NULL),
('VOIDED-555-555-234','Entry order block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-SEM-2018-DS-001' LIMIT 1),NULL,1,'parent',0,0.0,0.0,NULL,'Column serpentine','Top Left',(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),True,'[]'),
('VOIDED-555-555-233','Nursery block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-SEM-2018-DS-001' LIMIT 1),NULL,1,'parent',0,NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),True,NULL),
((SELECT experiment_code||'-EO-1' FROM experiment.experiment WHERE experiment_name = 'SYB-HB-2018-DS-001'),'Entry order block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-HB-2018-DS-001' LIMIT 1),NULL,1,'parent',0,0.0,0.0,NULL,'Column serpentine','Top Left',(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'[]'),
('VOIDED-291-291-2','Nursery block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-HB-2018-DS-001' LIMIT 1),NULL,1,'parent',0,NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),True,NULL);



--rollback DELETE FROM experiment.experiment_block WHERE experiment_id IN 
--rollback (SELECT id FROM experiment.experiment WHERE experiment_name IN (
--rollback   'SYB-F4-2020-DS-001',
--rollback   'SYB-F4-2020-DS-001',
--rollback   'SYB-F1-2018-WS-001',
--rollback   'SYB-F1-2018-WS-001',
--rollback   'SYB-F2-2019-DS-001',
--rollback   'SYB-F2-2019-DS-001',
--rollback   'SYB-F3-2019-WS-001',
--rollback   'SYB-F3-2019-WS-001',
--rollback   'SYB-SEM-2018-DS-001',
--rollback   'SYB-SEM-2018-DS-001',
--rollback   'SYB-SEM-2018-DS-001',
--rollback   'SYB-SEM-2018-DS-001',
--rollback   'SYB-HB-2018-DS-001',
--rollback   'SYB-HB-2018-DS-001'
--rollback ));