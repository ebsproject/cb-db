--liquibase formatted sql

--changeset postgres:S15_updated_tenant_protocol_fixture_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-3040 Populate updated tenant.protocol with Soybean fixture data



INSERT INTO tenant.protocol
	(protocol_code,protocol_name,protocol_type,description,program_id,creator_id,notes)
SELECT
    'MANAGEMENT_PROTOCOL_'||ex.experiment_code AS protocol_code,
	    'Management Protocol Exp'||substring(ex.experiment_code, 4) AS protocol_name,
	    'management' AS protocol_type,
	    null AS description,
	    ex.program_id,
        (SELECT CAST(id AS INTEGER) FROM tenant.person WHERE username='admin' LIMIT 1) AS creator_id,
        'BDS-3040 insert tenant.protocol' as notes
	FROM
	   	experiment.experiment ex
	WHERE 
        ex.experiment_name IN (
            'SYB-F4-2020-DS-001',
            'SYB-F1-2018-WS-001',
            'SYB-F2-2019-DS-001',
            'SYB-F3-2019-WS-001',
            'SYB-MET0-2023-DS-001',
            'SYB-SEM-2018-DS-001',
            'SYB-HB-2018-DS-001',
            'SYB-OYT-2021-DS-001',
            'SYB-AYT-2022-DS-001'
        )
UNION
SELECT
    'TRAIT_PROTOCOL_'||ex.experiment_code AS protocol_code,
	    'Trait Protocol Exp'||substring(ex.experiment_code, 4) AS protocol_name,
	    'trait' AS protocol_type,
	    null AS description,
	    ex.program_id,
        (SELECT CAST(id AS INTEGER) FROM tenant.person WHERE username='admin' LIMIT 1) AS creator_id,
        'BDS-3040 insert tenant.protocol' as notes
	FROM
	   	experiment.experiment ex
	WHERE 
        ex.experiment_name IN (
            'SYB-F4-2020-DS-001',
            'SYB-F1-2018-WS-001',
            'SYB-F2-2019-DS-001',
            'SYB-F3-2019-WS-001',
            'SYB-MET0-2023-DS-001',
            'SYB-SEM-2018-DS-001',
            'SYB-HB-2018-DS-001',
            'SYB-OYT-2021-DS-001',
            'SYB-AYT-2022-DS-001'
        )
UNION
SELECT
    'PLANTING_PROTOCOL_'||ex.experiment_code AS protocol_code,
	    'Planting Protocol Exp'||substring(ex.experiment_code, 4) AS protocol_name,
	    'planting' AS protocol_type,
	    null AS description,
	    ex.program_id,
        (SELECT CAST(id AS INTEGER) FROM tenant.person WHERE username='admin' LIMIT 1) AS creator_id,
        'BDS-3040 insert tenant.protocol' as notes
	FROM
	   	experiment.experiment ex
	WHERE 
        ex.experiment_name IN (
            'SYB-F4-2020-DS-001',
            'SYB-F1-2018-WS-001',
            'SYB-F2-2019-DS-001',
            'SYB-F3-2019-WS-001',
            'SYB-MET0-2023-DS-001',
            'SYB-SEM-2018-DS-001',
            'SYB-HB-2018-DS-001',
            'SYB-OYT-2021-DS-001',
            'SYB-AYT-2022-DS-001'
        )
UNION
SELECT
    'HARVEST_PROTOCOL_'||ex.experiment_code AS protocol_code,
	    'Harvest Protocol Exp'||substring(ex.experiment_code, 4) AS protocol_name,
	    'harvest' AS protocol_type,
	    null AS description,
	    ex.program_id,
        (SELECT CAST(id AS INTEGER) FROM tenant.person WHERE username='admin' LIMIT 1) AS creator_id,
        'BDS-3040 insert tenant.protocol' as notes
	FROM
	   	experiment.experiment ex
	WHERE 
         ex.experiment_name IN (
            'SYB-F4-2020-DS-001',
            'SYB-F1-2018-WS-001',
            'SYB-F2-2019-DS-001',
            'SYB-F3-2019-WS-001',
            'SYB-MET0-2023-DS-001',
            'SYB-SEM-2018-DS-001',
            'SYB-HB-2018-DS-001',
            'SYB-OYT-2021-DS-001',
            'SYB-AYT-2022-DS-001'
        )

--rollback DELETE FROM tenant.protocol
--rollback WHERE notes = 'BDS-3040 insert tenant.protocol';