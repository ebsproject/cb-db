--liquibase formatted sql

--changeset postgres:S11_updated_experiment_location_occurrence_group_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-3040 Populate updated experiment_location_occurrence_group



INSERT INTO experiment.location_occurrence_group
(location_id,occurrence_id,order_number,creator_id,is_void,notes)
 VALUES 
((SELECT id FROM experiment.location WHERE location_name ='SYB-PH_BO_UA-2022-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-AYT-2022-DS-001-004' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='SYB-IRRIHQ-2020-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-F4-2020-DS-001-001' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='SYB-PH_AN_RM-2022-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-AYT-2022-DS-001-003' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='SYB-IRRIHQ-2022-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-AYT-2022-DS-001-001' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='SYB-PH_NE_SM2-2022-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-AYT-2022-DS-001-002' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='SYB-PH_NE_SM2-2023-DS-003' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-MET0-2023-DS-001-002' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='SYB-IRRIHQ-2018-WS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-F1-2018-WS-001-001' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='SYB-IRRIHQ-2024-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-HB-2018-DS-001-001' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='SYB-PH_IB_SO-2023-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-MET0-2023-DS-001-005' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='SYB-PH_AN_RM-2023-DS-002' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-MET0-2023-DS-001-003' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='SYB-IRRIHQ-2021-DS-008' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-OYT-2021-DS-001-001' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='SYB-IRRIHQ-2019-WS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-F3-2019-WS-001-001' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='SYB-IRRIHQ-2023-DS-008' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-MET0-2023-DS-001-001' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='SYB-PH_NC_MD-2023-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-MET0-2023-DS-001-006' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='SYB-PH_NC_MD-2022-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-AYT-2022-DS-001-006' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='SYB-IRRIHQ-2018-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-SEM-2018-DS-001-001' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='SYB-IRRIHQ-2019-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-F2-2019-DS-001-001' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='SYB-PH_IB_SO-2022-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-AYT-2022-DS-001-005' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='SYB-PH_AN_RM-2021-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-OYT-2021-DS-001-003' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='SYB-PH_NE_SM2-2021-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-OYT-2021-DS-001-002' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='SYB-PH_BO_UA-2023-DS-002' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-MET0-2023-DS-001-004' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL);


--rollback DELETE FROM experiment.location_occurrence_group
--rollback WHERE occurrence_id IN (SELECT id FROM experiment.occurrence WHERE occurrence_name IN(
--rollback         'SYB-F4-2020-DS-001-001',
--rollback         'SYB-F1-2018-WS-001-001',
--rollback         'SYB-F2-2019-DS-001-001',
--rollback         'SYB-F3-2019-WS-001-001',
--rollback         'SYB-MET0-2023-DS-001-001',
--rollback         'SYB-MET0-2023-DS-001-002',
--rollback         'SYB-MET0-2023-DS-001-003',
--rollback         'SYB-MET0-2023-DS-001-005',
--rollback         'SYB-MET0-2023-DS-001-006',
--rollback         'SYB-MET0-2023-DS-001-004',
--rollback         'SYB-SEM-2018-DS-001-001',
--rollback         'SYB-HB-2018-DS-001-001',
--rollback         'SYB-OYT-2021-DS-001-002',
--rollback         'SYB-OYT-2021-DS-001-001',
--rollback         'SYB-OYT-2021-DS-001-003',
--rollback         'SYB-AYT-2022-DS-001-001',
--rollback         'SYB-AYT-2022-DS-001-004',
--rollback         'SYB-AYT-2022-DS-001-006',
--rollback         'SYB-AYT-2022-DS-001-005',
--rollback         'SYB-AYT-2022-DS-001-002',
--rollback         'SYB-AYT-2022-DS-001-003'
--rollback      )
--rollback );