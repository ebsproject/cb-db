--liquibase formatted sql

--changeset postgres:S6_updated_experiment_occurrence_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-3040 Populate updated experiment_occurrence



INSERT INTO experiment.occurrence
(occurrence_code,occurrence_name,occurrence_status,description,experiment_id,geospatial_object_id,creator_id,is_void,notes,rep_count,site_id,field_id,occurrence_number,remarks,entry_count,plot_count)
 VALUES 
((experiment.generate_code('occurrence')),'SYB-F4-2020-DS-001-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F4-2020-DS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,1.0,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),NULL,1,NULL,221,221),
((experiment.generate_code('occurrence')),'SYB-F1-2018-WS-001-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F1-2018-WS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,1.0,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),NULL,1,NULL,25,25),
((experiment.generate_code('occurrence')),'SYB-F2-2019-DS-001-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F2-2019-DS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,1.0,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),NULL,1,NULL,25,25),
((experiment.generate_code('occurrence')),'SYB-F3-2019-WS-001-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F3-2019-WS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,1.0,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),NULL,1,NULL,125,125),
((experiment.generate_code('occurrence')),'SYB-MET0-2023-DS-001-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-MET0-2023-DS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),NULL,1,NULL,56,112),
((experiment.generate_code('occurrence')),'SYB-MET0-2023-DS-001-002','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-MET0-2023-DS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NE_SM2' LIMIT 1),NULL,2,NULL,56,112),
((experiment.generate_code('occurrence')),'SYB-MET0-2023-DS-001-003','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-MET0-2023-DS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_AN_RM' LIMIT 1),NULL,3,NULL,56,112),
((experiment.generate_code('occurrence')),'SYB-MET0-2023-DS-001-005','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-MET0-2023-DS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_IB_SO' LIMIT 1),NULL,5,NULL,56,112),
((experiment.generate_code('occurrence')),'SYB-MET0-2023-DS-001-006','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-MET0-2023-DS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NC_MD' LIMIT 1),NULL,6,NULL,56,112),
((experiment.generate_code('occurrence')),'SYB-MET0-2023-DS-001-004','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-MET0-2023-DS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_BO_UA' LIMIT 1),NULL,4,NULL,56,112),
((experiment.generate_code('occurrence')),'SYB-SEM-2018-DS-001-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-SEM-2018-DS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,1.0,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),NULL,1,NULL,20,20),
((experiment.generate_code('occurrence')),'SYB-HB-2018-DS-001-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-HB-2018-DS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,1.0,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),NULL,1,NULL,10,10),
((experiment.generate_code('occurrence')),'SYB-OYT-2021-DS-001-002','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-OYT-2021-DS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NE_SM2' LIMIT 1),NULL,2,NULL,1406,1750),
((experiment.generate_code('occurrence')),'SYB-OYT-2021-DS-001-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-OYT-2021-DS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),NULL,1,NULL,1406,1750),
((experiment.generate_code('occurrence')),'SYB-OYT-2021-DS-001-003','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-OYT-2021-DS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_AN_RM' LIMIT 1),NULL,3,NULL,1406,1750),
((experiment.generate_code('occurrence')),'SYB-AYT-2022-DS-001-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-AYT-2022-DS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),NULL,1,NULL,286,572),
((experiment.generate_code('occurrence')),'SYB-AYT-2022-DS-001-004','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-AYT-2022-DS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_BO_UA' LIMIT 1),NULL,4,NULL,286,572),
((experiment.generate_code('occurrence')),'SYB-AYT-2022-DS-001-006','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-AYT-2022-DS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NC_MD' LIMIT 1),NULL,6,NULL,286,572),
((experiment.generate_code('occurrence')),'SYB-AYT-2022-DS-001-005','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-AYT-2022-DS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_IB_SO' LIMIT 1),NULL,5,NULL,286,572),
((experiment.generate_code('occurrence')),'SYB-AYT-2022-DS-001-002','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-AYT-2022-DS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NE_SM2' LIMIT 1),NULL,2,NULL,286,572),
((experiment.generate_code('occurrence')),'SYB-AYT-2022-DS-001-003','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-AYT-2022-DS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_AN_RM' LIMIT 1),NULL,3,NULL,286,572);



--rollback DELETE FROM experiment.occurrence WHERE occurrence_name IN (
--rollback      'SYB-F4-2020-DS-001-001',
--rollback      'SYB-F1-2018-WS-001-001',
--rollback      'SYB-F2-2019-DS-001-001',
--rollback      'SYB-F3-2019-WS-001-001',
--rollback      'SYB-MET0-2023-DS-001-001',
--rollback      'SYB-MET0-2023-DS-001-002',
--rollback      'SYB-MET0-2023-DS-001-003',
--rollback      'SYB-MET0-2023-DS-001-005',
--rollback      'SYB-MET0-2023-DS-001-006',
--rollback      'SYB-MET0-2023-DS-001-004',
--rollback      'SYB-SEM-2018-DS-001-001',
--rollback      'SYB-HB-2018-DS-001-001',
--rollback      'SYB-OYT-2021-DS-001-002',
--rollback      'SYB-OYT-2021-DS-001-001',
--rollback      'SYB-OYT-2021-DS-001-003',
--rollback      'SYB-AYT-2022-DS-001-001',
--rollback      'SYB-AYT-2022-DS-001-004',
--rollback      'SYB-AYT-2022-DS-001-006',
--rollback      'SYB-AYT-2022-DS-001-005',
--rollback      'SYB-AYT-2022-DS-001-002',
--rollback      'SYB-AYT-2022-DS-001-003'
--rollback  );