--liquibase formatted sql

--changeset postgres:S5_updated_place_geospatial_object_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-3040 Populate updated place_geospatial_object



INSERT INTO place.geospatial_object
(geospatial_object_code,geospatial_object_name,geospatial_object_type,geospatial_object_subtype,geospatial_coordinates,altitude,description,parent_geospatial_object_id,root_geospatial_object_id,creator_id,is_void,notes,coordinates)
 VALUES 
('SYB-IRRIHQ-2024-DS-001','IRRIHQ-2024-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('SYB-IRRIHQ-2018-WS-001','IRRIHQ-2018-WS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('SYB-IRRIHQ-2019-DS-001','IRRIHQ-2019-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('SYB-IRRIHQ-2019-WS-001','IRRIHQ-2019-WS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('SYB-IRRIHQ-2020-DS-001','IRRIHQ-2020-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('SYB-IRRIHQ-2018-DS-001','IRRIHQ-2018-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('SYB-IRRIHQ-2023-DS-008','IRRIHQ-2023-DS-008','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('SYB-IRRIHQ-2021-DS-008','IRRIHQ-2021-DS-008','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('SYB-PH_NE_SM2-2023-DS-003','PH_NE_SM2-2023-DS-003','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NE_SM2' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('SYB-PH_BO_UA-2023-DS-002','PH_BO_UA-2023-DS-002','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_BO_UA' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('SYB-PH_NE_SM2-2021-DS-001','PH_NE_SM2-2021-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NE_SM2' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('SYB-PH_AN_RM-2023-DS-002','PH_AN_RM-2023-DS-002','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_AN_RM' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('SYB-PH_NC_MD-2023-DS-001','PH_NC_MD-2023-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NC_MD' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('SYB-PH_AN_RM-2021-DS-001','PH_AN_RM-2021-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_AN_RM' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('SYB-PH_IB_SO-2023-DS-001','PH_IB_SO-2023-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_IB_SO' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('SYB-IRRIHQ-2022-DS-001','IRRIHQ-2022-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('SYB-PH_NE_SM2-2022-DS-001','PH_NE_SM2-2022-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NE_SM2' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('SYB-PH_AN_RM-2022-DS-001','PH_AN_RM-2022-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_AN_RM' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('SYB-PH_BO_UA-2022-DS-001','PH_BO_UA-2022-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_BO_UA' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('SYB-PH_IB_SO-2022-DS-001','PH_IB_SO-2022-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_IB_SO' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('SYB-PH_NC_MD-2022-DS-001','PH_NC_MD-2022-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NC_MD' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL);



--rollback DELETE FROM place.geospatial_object WHERE geospatial_object_code IN (
--rollback      'SYB-IRRIHQ-2024-DS-001',
--rollback      'SYB-IRRIHQ-2018-WS-001',
--rollback      'SYB-IRRIHQ-2019-DS-001',
--rollback      'SYB-IRRIHQ-2019-WS-001',
--rollback      'SYB-IRRIHQ-2020-DS-001',
--rollback      'SYB-IRRIHQ-2018-DS-001',
--rollback      'SYB-IRRIHQ-2023-DS-008',
--rollback      'SYB-IRRIHQ-2021-DS-008',
--rollback      'SYB-PH_NE_SM2-2023-DS-003',
--rollback      'SYB-PH_BO_UA-2023-DS-002',
--rollback      'SYB-PH_NE_SM2-2021-DS-001',
--rollback      'SYB-PH_AN_RM-2023-DS-002',
--rollback      'SYB-PH_NC_MD-2023-DS-001',
--rollback      'SYB-PH_AN_RM-2021-DS-001',
--rollback      'SYB-PH_IB_SO-2023-DS-001',
--rollback      'SYB-IRRIHQ-2022-DS-001',
--rollback      'SYB-PH_NE_SM2-2022-DS-001',
--rollback      'SYB-PH_AN_RM-2022-DS-001',
--rollback      'SYB-PH_BO_UA-2022-DS-001',
--rollback      'SYB-PH_IB_SO-2022-DS-001',
--rollback      'SYB-PH_NC_MD-2022-DS-001'
--rollback   );