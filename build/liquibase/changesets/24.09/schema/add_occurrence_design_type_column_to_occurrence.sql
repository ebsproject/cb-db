--liquibase formatted sql

--changeset postgres:add_occurrence_design_type_column_to_occurrence context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-3550 Add column occurrence_design_type to experiment.occurrence



ALTER TABLE experiment.occurrence
    ADD COLUMN occurrence_design_type varchar(64);

COMMENT ON COLUMN experiment.occurrence.occurrence_design_type
    IS 'Occurrence Design Type: Design type information of occurrence [DESIGN]';



--rollback ALTER TABLE experiment.occurrence DROP COLUMN occurrence_design_type;
