--liquibase formatted sql

--changeset postgres:add_all_param_required_to_master_formula context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-3140 CB-DB: Add new column master.formula.all_param_required to indicate required formula parameters



ALTER TABLE
	master.formula
ADD
	all_param_required boolean
NOT NULL DEFAULT true;

COMMENT ON COLUMN
	master.formula.all_param_required
IS
	'Indicator whether all formula parameters are required';



--rollback ALTER TABLE
--rollback 	master.formula
--rollback DROP COLUMN
--rollback 	all_param_required;