--liquibase formatted sql

--changeset postgres:update_function_for_other_names context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-3342 CB-GC: The transaction is still CODING ready but the proposed germplasm name is already reflected in the names page 



CREATE OR REPLACE FUNCTION germplasm.populate_other_names_for_germplasm_germplasm()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE 
    var_other_name varchar;
BEGIN
        IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
                SELECT 
                    string_agg(gn.name_value,';') INTO var_other_name
                FROM 
                    germplasm.germplasm g 
                JOIN
                    germplasm.germplasm_name gn 
                ON
                    gn.germplasm_id = g.id
                WHERE 
                    g.id = new.id
                AND
                    gn.is_void IS NOT TRUE    
                AND
                    gn.germplasm_name_status NOT IN ('draft')
                ;
                NEW.other_names = var_other_name;
        END IF;
    RETURN NEW;
END;
$function$
;

DROP TRIGGER IF EXISTS germplasm_populate_other_names_from_germplasm_tgr ON germplasm.germplasm;

CREATE TRIGGER germplasm_populate_other_names_from_germplasm_tgr BEFORE
INSERT OR UPDATE
    ON
    germplasm.germplasm FOR EACH ROW EXECUTE FUNCTION germplasm.populate_other_names_for_germplasm_germplasm()
;

CREATE OR REPLACE FUNCTION germplasm.populate_other_names_for_germplasm_germplasm_from_germplasm_germplasm_name()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE
    var_document varchar;
BEGIN
     
    IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
        UPDATE 
            germplasm.germplasm  
        SET 
            modification_timestamp = now() 
        WHERE 
            id
        IN
            (
                SELECT 
                    germplasm_id
                FROM
                    germplasm.germplasm_name g
                WHERE
                    germplasm_id = new.id
                    AND g.germplasm_name_status NOT IN ('draft')
            );
    END IF;
    
    RETURN NEW;
END;
$function$
;

DROP TRIGGER IF EXISTS germplasm_populate_other_names_from_germplasm_name_tgr ON germplasm.germplasm_name;

CREATE TRIGGER germplasm_populate_other_names_from_germplasm_name_tgr BEFORE
INSERT OR UPDATE
    ON
    germplasm.germplasm_name FOR EACH ROW EXECUTE FUNCTION germplasm.populate_other_names_for_germplasm_germplasm_from_germplasm_germplasm_name()
;



-- revert changes
--rollback  CREATE OR REPLACE FUNCTION germplasm.populate_other_names_for_germplasm_germplasm()
--rollback      RETURNS trigger
--rollback      LANGUAGE plpgsql
--rollback  AS $function$
--rollback  DECLARE 
--rollback      var_other_name varchar;
--rollback  BEGIN
--rollback          IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
--rollback                  SELECT 
--rollback                      string_agg(gn.name_value,';') INTO var_other_name
--rollback                  FROM 
--rollback                      germplasm.germplasm g 
--rollback                  JOIN
--rollback                      germplasm.germplasm_name gn 
--rollback                  ON
--rollback                      gn.germplasm_id = g.id
--rollback                  WHERE 
--rollback                      g.id = new.id
--rollback                  AND
--rollback                      gn.is_void IS NOT TRUE
--rollback                  ;
--rollback                  NEW.other_names = var_other_name;
--rollback          END IF;
--rollback      RETURN NEW;
--rollback  END;
--rollback  $function$
--rollback  ;

--rollback  CREATE OR REPLACE FUNCTION germplasm.populate_other_names_for_germplasm_germplasm_from_germplasm_germplasm_name()
--rollback      RETURNS trigger
--rollback      LANGUAGE plpgsql
--rollback  AS $function$
--rollback  DECLARE
--rollback      var_document varchar;
--rollback  BEGIN
     
--rollback      IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
--rollback          UPDATE 
--rollback              germplasm.germplasm  
--rollback          SET 
--rollback              modification_timestamp = now() 
--rollback          WHERE 
--rollback              id
--rollback          IN
--rollback              (
--rollback                  SELECT 
--rollback                      germplasm_id
--rollback                  FROM
--rollback                      germplasm.germplasm_name g
--rollback                  WHERE
--rollback                      germplasm_id = new.id
--rollback              );
--rollback      END IF;
    
--rollback      RETURN NEW;
--rollback  END;
--rollback  $function$
--rollback  ;