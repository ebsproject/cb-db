--liquibase formatted sql

--changeset postgres:insert_new_cross_method_scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-208 CB-HM DB: Update configs for COWPEA bi-parental cross harvest with Bulk



-- add scale value/s to a variable if not existing, else skip this changeset (use precondition to check if scale values exist)
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 0) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'CROSS_METHOD'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('bi-parental cross','Bi-parental Cross','Bi-parental Cross','BI_PARENTAL_CROSS','show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'CROSS_METHOD'
;



--rollback DELETE FROM master.scale_value WHERE abbrev = 'CROSS_METHOD_BI_PARENTAL_CROSS';