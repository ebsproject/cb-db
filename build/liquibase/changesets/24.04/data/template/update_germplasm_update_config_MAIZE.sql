--liquibase formatted sql

--changeset postgres:update_germplasm_update_config_MAIZE context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-188 CB-GM: Enable bulk update via Upload of GERMPLASM_STATE values



--update data browser config
UPDATE platform.config
SET
	config_value = $${
	"values": [
		{
			"type": "basic",
			"view": {
				"visible": "true",
				"entities": [
				"germplasm",
				"germplasm_attribute"
				]
			},
			"usage": "required",
			"abbrev": "GERMPLASM_CODE",
			"entity": "germplasm",
			"in_export": "true",
			"in_upload": "true",
			"in_browser": "true",
			"in_summary": "false",
			"api_resource": {
				"http_method": "POST",
				"search_filter": "germplasmCode",
				"search_params": "limit=1",
				"search_endpoint": "germplasm-search",
				"search_api_field": "germplasmDbId",
				"additional_filters": {}
			},
			"display_value": "Germplasm Code",
			"retrieve_db_id": "true"
		},
		{
			"type": "basic",
			"view": {
				"visible": "true",
				"entities": [
				"germplasm",
				"germplasm_attribute"
				]
			},
			"usage": "optional",
			"abbrev": "DESIGNATION",
			"entity": "germplasm",
			"in_export": "true",
			"in_upload": "true",
			"in_browser": "true",
			"in_summary": "false",
			"api_resource": {
				"http_method": "POST",
				"search_filter": "designation",
				"search_params": "limit=1",
				"search_endpoint": "germplasm-search",
				"search_api_field": "germplasmDbId",
				"additional_filters": {}
			},
			"display_value": "Designation",
			"retrieve_db_id": "true"
		},
		{
			"type": "basic",
			"view": {
				"visible": "true",
				"entities": [
				"germplasm"
				]
			},
			"usage": "optional",
			"abbrev": "GERMPLASM_STATE",
			"entity": "germplasm",
			"in_export": "true",
			"in_upload": "true",
			"in_browser": "true",
			"api_resource": {},
			"display_value": "Germplasm State",
			"retrieve_db_id": "false"
		},
		{
			"type": "attribute",
			"view": {
				"visible": "true",
				"entities": [
				"germplasm",
				"germplasm_attribute"
				]
			},
			"usage": "optional",
			"abbrev": "HETEROTIC_GROUP",
			"entity": "germplasm_attribute",
			"in_export": "true",
			"in_upload": "true",
			"in_browser": "true",
			"in_summary": "true",
			"api_resource": {
				"http_method": "GET",
				"search_filter": "",
				"search_params": "",
				"search_endpoint": "germplasm/{id}/attributes",
				"search_api_field": "",
				"additional_filters": {}
			},
			"display_value": "Heterotic Group",
			"retrieve_db_id": "true"
		},
		{
			"type": "attribute",
			"view": {
				"visible": "true",
				"entities": [
				"germplasm",
				"germplasm_attribute"
				]
			},
			"usage": "optional",
			"abbrev": "DH_FIRST_INCREASE_COMPLETED",
			"entity": "germplasm_attribute",
			"in_export": "true",
			"in_upload": "true",
			"in_browser": "true",
			"in_summary": "true",
			"api_resource": {
				"http_method": "GET",
				"search_filter": "",
				"search_params": "",
				"search_endpoint": "germplasm/{id}/attributes",
				"search_api_field": "",
				"additional_filters": {}
			},
			"display_value": "Double Haploid First Increase Completed",
			"retrieve_db_id": "true"
		},
		{
			"type": "attribute",
			"view": {
				"visible": "true",
				"entities": [
				"germplasm",
				"germplasm_attribute"
				]
			},
			"usage": "optional",
			"abbrev": "MTA_STATUS",
			"entity": "germplasm_attribute",
			"in_export": "true",
			"in_upload": "true",
			"in_browser": "true",
			"in_summary": "true",
			"api_resource": {
				"http_method": "POST",
				"search_filter": "mtaStatus",
				"search_params": "limit=1",
				"search_endpoint": "germplasm-mta-search",
				"search_api_field": "germplasmMtaDbId",
				"additional_filters": {}
			},
			"display_value": "MTA Status",
			"retrieve_db_id": "true"
		}
	]
	}$$
WHERE
	abbrev = 'GM_CROP_MAIZE_GERMPLASM_UPDATE_CONFIG'
;



--rollback UPDATE platform.config
--rollback SET
--rollback config_value = $${
--rollback  "values": [
--rollback  {
--rollback  	"type": "basic",
--rollback  	"view": {
--rollback  	"visible": "true",
--rollback  	"entities": [
--rollback  		"germplasm",
--rollback  		"germplasm_attribute"
--rollback  	]
--rollback  	},
--rollback  	"usage": "required",
--rollback  	"abbrev": "GERMPLASM_CODE",
--rollback  	"entity": "germplasm",
--rollback  	"in_export": "true",
--rollback  	"in_upload": "true",
--rollback  	"in_browser": "true",
--rollback  	"in_summary": "false",
--rollback  	"api_resource": {
--rollback  	"http_method": "POST",
--rollback  	"search_filter": "germplasmCode",
--rollback  	"search_params": "limit=1",
--rollback  	"search_endpoint": "germplasm-search",
--rollback  	"search_api_field": "germplasmDbId",
--rollback  	"additional_filters": {}
--rollback  	},
--rollback  	"display_value": "Germplasm Code",
--rollback  	"retrieve_db_id": "true"
--rollback  },
--rollback  {
--rollback  	"type": "basic",
--rollback  	"view": {
--rollback  	"visible": "true",
--rollback  	"entities": [
--rollback  		"germplasm",
--rollback  		"germplasm_attribute"
--rollback  	]
--rollback  	},
--rollback  	"usage": "optional",
--rollback  	"abbrev": "DESIGNATION",
--rollback  	"entity": "germplasm",
--rollback  	"in_export": "true",
--rollback  	"in_upload": "true",
--rollback  	"in_browser": "true",
--rollback  	"in_summary": "false",
--rollback  	"api_resource": {
--rollback  	"http_method": "POST",
--rollback  	"search_filter": "designation",
--rollback  	"search_params": "limit=1",
--rollback  	"search_endpoint": "germplasm-search",
--rollback  	"search_api_field": "germplasmDbId",
--rollback  	"additional_filters": {}
--rollback  	},
--rollback  	"display_value": "Designation",
--rollback  	"retrieve_db_id": "true"
--rollback  },
--rollback  {
--rollback  	"type": "attribute",
--rollback  	"view": {
--rollback  	"visible": "true",
--rollback  	"entities": [
--rollback  		"germplasm",
--rollback  		"germplasm_attribute"
--rollback  	]
--rollback  	},
--rollback  	"usage": "optional",
--rollback  	"abbrev": "HETEROTIC_GROUP",
--rollback  	"entity": "germplasm_attribute",
--rollback  	"in_export": "true",
--rollback  	"in_upload": "true",
--rollback  	"in_browser": "true",
--rollback  	"in_summary": "true",
--rollback  	"api_resource": {
--rollback  	"http_method": "GET",
--rollback  	"search_filter": "",
--rollback  	"search_params": "",
--rollback  	"search_endpoint": "germplasm/{id}/attributes",
--rollback  	"search_api_field": "",
--rollback  	"additional_filters": {}
--rollback  	},
--rollback  	"display_value": "Heterotic Group",
--rollback  	"retrieve_db_id": "true"
--rollback  },
--rollback  {
--rollback  	"type": "attribute",
--rollback  	"view": {
--rollback  	"visible": "true",
--rollback  	"entities": [
--rollback  		"germplasm",
--rollback  		"germplasm_attribute"
--rollback  	]
--rollback  	},
--rollback  	"usage": "optional",
--rollback  	"abbrev": "DH_FIRST_INCREASE_COMPLETED",
--rollback  	"entity": "germplasm_attribute",
--rollback  	"in_export": "true",
--rollback  	"in_upload": "true",
--rollback  	"in_browser": "true",
--rollback  	"in_summary": "true",
--rollback  	"api_resource": {
--rollback  	"http_method": "GET",
--rollback  	"search_filter": "",
--rollback  	"search_params": "",
--rollback  	"search_endpoint": "germplasm/{id}/attributes",
--rollback  	"search_api_field": "",
--rollback  	"additional_filters": {}
--rollback  	},
--rollback  	"display_value": "Double Haploid First Increase Completed",
--rollback  	"retrieve_db_id": "true"
--rollback  },
--rollback  {
--rollback  	"type": "attribute",
--rollback  	"view": {
--rollback  	"visible": "true",
--rollback  	"entities": [
--rollback  		"germplasm",
--rollback  		"germplasm_attribute"
--rollback  	]
--rollback  	},
--rollback  	"usage": "optional",
--rollback  	"abbrev": "MTA_STATUS",
--rollback  	"entity": "germplasm_attribute",
--rollback  	"in_export": "true",
--rollback  	"in_upload": "true",
--rollback  	"in_browser": "true",
--rollback  	"in_summary": "true",
--rollback  	"api_resource": {
--rollback  	"http_method": "POST",
--rollback  	"search_filter": "mtaStatus",
--rollback  	"search_params": "limit=1",
--rollback  	"search_endpoint": "germplasm-mta-search",
--rollback  	"search_api_field": "germplasmMtaDbId",
--rollback  	"additional_filters": {}
--rollback  	},
--rollback  	"display_value": "MTA Status",
--rollback  	"retrieve_db_id": "true"
--rollback  }
--rollback  ]
--rollback  }$$
--rollback WHERE
--rollback 	abbrev = 'GM_CROP_MAIZE_GERMPLASM_UPDATE_CONFIG'
--rollback ;