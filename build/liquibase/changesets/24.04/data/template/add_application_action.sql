--liquibase formatted sql

--changeset postgres:add_application_action context:template splitStatements:false rollbackSplitStatements:false
--preconditions OnFail:MARK_RAN
--precondition-sql-check expectedResult:0 SELECT COUNT(*) FROM platform.application_action WHERE module = 'dashboard' AND controller = 'default' AND action = 'index' AND application_id = (SELECT id FROM platform.application WHERE abbrev = 'DASHBOARD')
--comment: BDS-117 CB-Dashboard: Recently used tools - incorrect configure dashboard timestamp



INSERT INTO platform.application_action
	(application_id, module, controller, action, creator_id)
VALUES
	((SELECT id FROM platform.application WHERE abbrev = 'DASHBOARD'), 'dashboard', 'default', 'index', 1);



-- revert changes
--rollback DELETE FROM
--rollback     platform.application_action
--rollback WHERE
--rollback     module = 'dashboard' AND
--rollback     controller = 'default' AND
--rollback     action = 'index';