--liquibase formatted sql

--changeset postgres:add_deleteCrossList_threshold_in_platform_config context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-122: CB-EC: Add deleteCrossList threshold in EXPERIMENT_CREATION_BG_PROCESSING_THRESHOLD in platform.config



UPDATE platform.config
SET
	config_value = $$
{
  "createEntries": {
    "size": "500",
    "description": "Create entries of an Experiment"
  },
  "deleteEntries": {
    "size": "1000",
    "description": "Delete entries of an Experiment"
  },
  "updateEntries": {
    "size": "500",
    "description": "Update entries of an Experiment"
  },
  "inputListLimit": {
    "size": "1000",
    "description": "Input list size limit for validation for adding entries"
  },
  "updateCrossList": {
    "size": "1000",
    "description": "Update cross list records"
  },
  "deleteCrossList": {
    "size": "500",
    "description": "Delete cross list records"
  },
  "createOccurrences": {
    "size": "500",
    "description": "Create occurrence plot and/or planting instruction records of an Experiment"
  },
  "deleteOccurrences": {
    "size": "500",
    "description": "Delete occurrence plot and/or planting instruction records of an Experiment"
  },
  "reorderAllEntries": {
    "size": "200",
    "description": "Reorder all entries of an Experiment"
  },
  "renderCopyEntryList": {
    "size": "100",
    "description": "Limit for viewing experiments in copy entry list"
  },
  "exportCrossListLimit": {
    "size": "1000",
    "description": "Limit for exporting cross list records"
  },
  "renderAdvancedNursery": {
    "size": "50",
    "description": "Limit for viewing nurseries"
  }
}
$$
WHERE
	abbrev = 'EXPERIMENT_CREATION_BG_PROCESSING_THRESHOLD';



--rollback UPDATE platform.config
--rollback SET
--rollback 	config_value = $$
--rollback {
--rollback   "createEntries": {
--rollback     "size": "500",
--rollback     "description": "Create entries of an Experiment"
--rollback   },
--rollback   "deleteEntries": {
--rollback     "size": "1000",
--rollback     "description": "Delete entries of an Experiment"
--rollback   },
--rollback   "updateEntries": {
--rollback     "size": "500",
--rollback     "description": "Update entries of an Experiment"
--rollback   },
--rollback   "inputListLimit": {
--rollback     "size": "1000",
--rollback     "description": "Input list size limit for validation for adding entries"
--rollback   },
--rollback   "updateCrossList": {
--rollback     "size": "1000",
--rollback     "description": "Update cross list records"
--rollback   },
--rollback   "createOccurrences": {
--rollback     "size": "500",
--rollback     "description": "Create occurrence plot and/or planting instruction records of an Experiment"
--rollback   },
--rollback   "deleteOccurrences": {
--rollback     "size": "500",
--rollback     "description": "Delete occurrence plot and/or planting instruction records of an Experiment"
--rollback   },
--rollback   "reorderAllEntries": {
--rollback     "size": "200",
--rollback     "description": "Reorder all entries of an Experiment"
--rollback   },
--rollback   "renderCopyEntryList": {
--rollback     "size": "100",
--rollback     "description": "Limit for viewing experiments in copy entry list"
--rollback   },
--rollback   "exportCrossListLimit": {
--rollback     "size": "1000",
--rollback     "description": "Limit for exporting cross list records"
--rollback   },
--rollback   "renderAdvancedNursery": {
--rollback     "size": "50",
--rollback     "description": "Limit for viewing nurseries"
--rollback   }
--rollback }
--rollback $$
--rollback WHERE
--rollback 	abbrev = 'EXPERIMENT_CREATION_BG_PROCESSING_THRESHOLD';