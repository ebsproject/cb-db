--liquibase formatted sql

--changeset postgres:insert_BMS_LOCID_variable context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN
--precondition-sql-check expectedResult:0 SELECT COUNT(*) FROM master.variable WHERE abbrev='BMS_LOCID'
--comment: BDS-386 CB-DB: Insert BMS_LOCID variable record



--BMS_LOCID
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('BMS_LOCID', 'BMS LOCID', 'BMS LOCID', 'text', false, 'identification', 'geospatial_object', 'occurrence', 'A concatentation of a data migration code linked to a source Breeding Management System (BMS) database and the LOCID from the BMS location table that can be mapped to a geospatial_object in the EBS.', 'active', '1', 'BMS LOCID', '24.04')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'BMS_LOCID' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('BMS_LOCID', 'BMS LOCID', 'BMS LOCID') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('BMS_LOCID_METHOD', 'BMS LOCID method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('BMS_LOCID_SCALE', 'BMS LOCID scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('BMS_LOCID');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('BMS_LOCID_METHOD');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('BMS_LOCID_SCALE');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('BMS_LOCID');
