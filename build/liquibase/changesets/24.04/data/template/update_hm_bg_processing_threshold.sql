--liquibase formatted sql

--changeset postgres:update_hm_bg_processing_threshold context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-98 CB-HM DB: Update background processing threshold



--update hm background processing threshold
UPDATE platform.config
SET
	config_value = $${
        "createList": 
            {
                "size": "10000", 
                "description": "Create new germplasm, seed, or package list"
            }, 
        "deleteSeeds": 
            {
                "size": "0", 
                "description": "Delete seeds of plots or crosses"
            }, 
        "previewList": 
            {
                "size": "100", 
                "description": "Preview germplasm, seed, or package list"
            }, 
        "deletePackages": 
            {
                "size": "0", 
                "description": "Delete packages and related records"
            }, 
        "exportPackages": 
            {
                "size": "2000", 
                "description": "Export created germplasm, seed, and package records"
            }, 
        "deleteHarvestData": 
            {
                "size": "0", 
                "description": "Delete harvest data of plots or crosses"
            }, 
        "updateHarvestData": 
            {
                "size": "2500", 
                "description": "Update harvest data of plots or crosses"
            }
    }$$
WHERE
	abbrev = 'HARVEST_MANAGER_BG_PROCESSING_THRESHOLD'
;



--rollback UPDATE platform.config
--rollback SET
--rollback 	config_value = $${
--rollback         "createList": 
--rollback             {
--rollback                 "size": "10000", 
--rollback                 "description": "Create new germplasm, seed, or package list"
--rollback             }, 
--rollback         "deleteSeeds": 
--rollback             {
--rollback                 "size": "200", 
--rollback                 "description": "Delete seeds of plots or crosses"
--rollback             }, 
--rollback         "previewList": 
--rollback             {
--rollback                 "size": "100", 
--rollback                 "description": "Preview germplasm, seed, or package list"
--rollback             }, 
--rollback         "deletePackages": 
--rollback             {
--rollback                 "size": "200", 
--rollback                 "description": "Delete packages and related records"
--rollback             }, 
--rollback         "exportPackages": 
--rollback             {
--rollback                 "size": "2000", 
--rollback                 "description": "Export created germplasm, seed, and package records"
--rollback             }, 
--rollback         "deleteHarvestData": 
--rollback             {
--rollback                 "size": "2000", 
--rollback                 "description": "Delete harvest data of plots or crosses"
--rollback             }, 
--rollback         "updateHarvestData": 
--rollback             {
--rollback                 "size": "2500", 
--rollback                 "description": "Update harvest data of plots or crosses"
--rollback             }
--rollback     }$$
--rollback WHERE
--rollback 	abbrev = 'HARVEST_MANAGER_BG_PROCESSING_THRESHOLD'
--rollback ;