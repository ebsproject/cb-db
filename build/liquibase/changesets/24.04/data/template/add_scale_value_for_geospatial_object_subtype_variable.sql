--liquibase formatted sql

--changeset postgres:add_scale_value_for_geospatial_object_subtype_variable context:template splitStatements:false rollbackSplitStatements:false
--preconditions OnFail:MARK_RAN
--precondition-sql-check expectedResult:0 SELECT COUNT (*) FROM master.scale_value WHERE abbrev = 'GEOSPATIAL_OBJECT_SUBTYPE_GERMPLASM_COLLECTION_SITE'
--comment: BDS-608 CB-DB: Add scale value for the GEOSPATIAL_OBJECT_SUBTYPE variable



WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 0) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'GEOSPATIAL_OBJECT_SUBTYPE'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('germplasm collection site', 'germplasm collection site', 'germplasm collection site', 'GERMPLASM_COLLECTION_SITE', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'GEOSPATIAL_OBJECT_SUBTYPE'
;



--rollback DELETE FROM master.scale_value
--rollback WHERE abbrev in (
--rollback 	'GEOSPATIAL_OBJECT_SUBTYPE_GERMPLASM_COLLECTION_SITE'
--rollback 	);