--liquibase formatted sql

--changeset postgres:update_hm_cowpea_germplasm_name_pattern_config_003 context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-66 CB-HM DB: Update configs for COWPEA backcross harvest with Bulk



--update germplasm name config
UPDATE platform.config
SET
	config_value = $${
    "PLOT": {
        "default": {
            "not_fixed": {
                "default": {
                "default": [
                    {
                    "type": "field",
                    "entity": "plot",
                    "field_name": "germplasmDesignation",
                    "order_number": 0
                    }
                ],
                "single plant selection": [
                    {
                    "type": "field",
                    "entity": "plot",
                    "field_name": "germplasmDesignation",
                    "order_number": 0
                    },
                    {
                    "type": "delimiter",
                    "value": "-",
                    "order_number": 1
                    },
                    {
                    "type": "counter",
                    "order_number": 2
                    }
                ]
                }
            }
        }
    },
    "CROSS": {
        "default": {
            "default": {
                "default": {
                "default": [
                    {
                    "type": "free-text",
                    "value": "",
                    "order_number": 0
                    }
                ]
                }
            }
        },
        "bi-parental cross": {
            "default": {
                "default": {
                    "default": [
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "crossName",
                            "order_number": 0
                        }
                    ],
                    "bulk": [
                        {
                            "type": "free-text",
                            "value": "IT",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "harvestYearYY",
                            "order_number": 1
                        },
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "originSiteCode",
                            "order_number": 2
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 3
                        },
                        {
                            "type": "counter",
                            "order_number": 4
                        }
                    ]
                },
                "not_fixed": {
                    "default": [
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "crossName",
                            "order_number": 0
                        }
                    ],
                    "bulk": [
                        {
                            "type": "free-text",
                            "value": "IT",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "harvestYearYY",
                            "order_number": 1
                        },
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "originSiteCode",
                            "order_number": 2
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 3
                        },
                        {
                            "type": "counter",
                            "order_number": 4
                        }
                    ]
                }
            }
        },
        "backcross": {
            "default": {
                "default": {
                    "default": [
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "crossName",
                            "order_number": 0
                        }
                    ],
                    "bulk": [
                        {
                            "type": "field",
                            "entity": "nonRecurrentParentGermplasm",
                            "field_name": "designation",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "free-text",
                            "value": "B",
                            "order_number": 2
                        },
                        {
                            "type": "counter",
                            "order_number": 3
                        }
                    ]
                }
            }
        }
    },
    "harvest_mode": {
        "cross_method": {
        "germplasm_state": {
            "germplasm_type": {
            "harvest_method": [
                {
                "type": "free-text",
                "value": "ABC",
                "order_number": 0
                },
                {
                "type": "field",
                "entity": "<entity>",
                "field_name": "<field_name>",
                "order_number": 1
                },
                {
                "type": "delimiter",
                "value": "-",
                "order_number": 1
                },
                {
                "type": "counter",
                "order_number": 3
                },
                {
                "type": "db-sequence",
                "schema": "<schema>",
                "order_number": 4,
                "sequence_name": "<sequence_name>"
                },
                {
                "type": "free-text-repeater",
                "value": "ABC",
                "minimum": "2",
                "delimiter": "*",
                "order_number": 5
                }
            ]
            }
        }
        }
    }
    }$$
WHERE
	abbrev = 'HM_NAME_PATTERN_GERMPLASM_COWPEA_DEFAULT'
;



--rollback UPDATE platform.config
--rollback SET
--rollback 	config_value = $${
--rollback     "PLOT": {
--rollback         "default": {
--rollback         "not_fixed": {
--rollback             "default": {
--rollback             "default": [
--rollback                 {
--rollback                 "type": "field",
--rollback                 "entity": "plot",
--rollback                 "field_name": "germplasmDesignation",
--rollback                 "order_number": 0
--rollback                 }
--rollback             ],
--rollback             "single plant selection": [
--rollback                 {
--rollback                 "type": "field",
--rollback                 "entity": "plot",
--rollback                 "field_name": "germplasmDesignation",
--rollback                 "order_number": 0
--rollback                 },
--rollback                 {
--rollback                 "type": "delimiter",
--rollback                 "value": "-",
--rollback                 "order_number": 1
--rollback                 },
--rollback                 {
--rollback                 "type": "counter",
--rollback                 "order_number": 2
--rollback                 }
--rollback             ]
--rollback             }
--rollback         }
--rollback         }
--rollback     },
--rollback     "CROSS": {
--rollback         "default": {
--rollback             "default": {
--rollback                 "default": {
--rollback                 "default": [
--rollback                     {
--rollback                     "type": "free-text",
--rollback                     "value": "",
--rollback                     "order_number": 0
--rollback                     }
--rollback                 ]
--rollback                 }
--rollback             }
--rollback         },
--rollback         "bi-parental cross": {
--rollback             "default": {
--rollback                 "default": {
--rollback                     "default": [
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "cross",
--rollback                             "field_name": "crossName",
--rollback                             "order_number": 0
--rollback                         }
--rollback                     ],
--rollback                     "bulk": [
--rollback                         {
--rollback                             "type": "free-text",
--rollback                             "value": "IT",
--rollback                             "order_number": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "cross",
--rollback                             "field_name": "harvestYearYY",
--rollback                             "order_number": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "cross",
--rollback                             "field_name": "originSiteCode",
--rollback                             "order_number": 2
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "order_number": 3
--rollback                         },
--rollback                         {
--rollback                             "type": "counter",
--rollback                             "order_number": 4
--rollback                         }
--rollback                     ]
--rollback                 },
--rollback                 "not_fixed": {
--rollback                     "default": [
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "cross",
--rollback                             "field_name": "crossName",
--rollback                             "order_number": 0
--rollback                         }
--rollback                     ],
--rollback                     "bulk": [
--rollback                         {
--rollback                             "type": "free-text",
--rollback                             "value": "IT",
--rollback                             "order_number": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "cross",
--rollback                             "field_name": "harvestYearYY",
--rollback                             "order_number": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "cross",
--rollback                             "field_name": "originSiteCode",
--rollback                             "order_number": 2
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "order_number": 3
--rollback                         },
--rollback                         {
--rollback                             "type": "counter",
--rollback                             "order_number": 4
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             }
--rollback         }
--rollback     },
--rollback     "harvest_mode": {
--rollback         "cross_method": {
--rollback         "germplasm_state": {
--rollback             "germplasm_type": {
--rollback             "harvest_method": [
--rollback                 {
--rollback                 "type": "free-text",
--rollback                 "value": "ABC",
--rollback                 "order_number": 0
--rollback                 },
--rollback                 {
--rollback                 "type": "field",
--rollback                 "entity": "<entity>",
--rollback                 "field_name": "<field_name>",
--rollback                 "order_number": 1
--rollback                 },
--rollback                 {
--rollback                 "type": "delimiter",
--rollback                 "value": "-",
--rollback                 "order_number": 1
--rollback                 },
--rollback                 {
--rollback                 "type": "counter",
--rollback                 "order_number": 3
--rollback                 },
--rollback                 {
--rollback                 "type": "db-sequence",
--rollback                 "schema": "<schema>",
--rollback                 "order_number": 4,
--rollback                 "sequence_name": "<sequence_name>"
--rollback                 },
--rollback                 {
--rollback                 "type": "free-text-repeater",
--rollback                 "value": "ABC",
--rollback                 "minimum": "2",
--rollback                 "delimiter": "*",
--rollback                 "order_number": 5
--rollback                 }
--rollback             ]
--rollback             }
--rollback         }
--rollback         }
--rollback     }
--rollback     }$$
--rollback WHERE
--rollback 	abbrev = 'HM_NAME_PATTERN_GERMPLASM_COWPEA_DEFAULT'
--rollback ;