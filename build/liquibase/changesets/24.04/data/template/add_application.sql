--liquibase formatted sql

--changeset postgres:add_application context:template splitStatements:false rollbackSplitStatements:false
--preconditions OnFail:MARK_RAN
--precondition-sql-check expectedResult:0 SELECT COUNT(*) FROM platform.application WHERE abbrev = 'DASHBOARD'
--comment: BDS-117 CB-Dashboard: Recently used tools - incorrect configure dashboard timestamp



INSERT INTO platform.application
	(abbrev, label, action_label, icon, creator_id)
VALUES
	('DASHBOARD', 'Dashboard', 'View dashboard', 'dashboard', 1);



-- revert changes
--rollback DELETE FROM
--rollback     platform.application
--rollback WHERE
--rollback     abbrev = 'DASHBOARD';