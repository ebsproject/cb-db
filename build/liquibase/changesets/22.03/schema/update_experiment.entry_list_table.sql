--liquibase formatted sql

--changeset postgres:add_new_columns context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-942 Add new columns



-- add new columns
ALTER TABLE
    experiment.entry_list
ADD COLUMN
    parent_count integer,
ADD COLUMN
    cross_count integer,
ADD COLUMN
    experiment_year integer
;

COMMENT ON COLUMN experiment.entry_list.parent_count
    IS 'Parent Count: Total number of parents in the cross list [ENTLIST_PARENTCOUNT]';

COMMENT ON COLUMN experiment.entry_list.cross_count
    IS 'Cross Count: Total number of crosses created in the cross list [ENTLIST_CROSSCOUNT]';

COMMENT ON COLUMN experiment.entry_list.experiment_year
    IS 'Experiment Year: Year when the experiment or cross list is created [ENTLIST_YEAR]';



-- revert changes
--rollback ALTER TABLE
--rollback     experiment.entry_list
--rollback DROP COLUMN
--rollback     parent_count,
--rollback DROP COLUMN
--rollback     cross_count,
--rollback DROP COLUMN
--rollback     experiment_year
--rollback ;



--changeset postgres:add_new_reference_columns context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-942 Add new reference columns



-- add reference columns
ALTER TABLE
    experiment.entry_list
ADD COLUMN
    season_id integer,
ADD COLUMN
    site_id integer,
ADD COLUMN
    crop_id integer,
ADD COLUMN
    program_id integer
;

COMMENT ON COLUMN experiment.entry_list.season_id
    IS 'Season ID: Reference to the season when the cross list is created [ENTLIST_SEASON_ID]';

COMMENT ON COLUMN experiment.entry_list.site_id
    IS 'Site ID: Reference to the site (geospatial object) where the crosses are planted [ENTLIST_SITE_ID]';

COMMENT ON COLUMN experiment.entry_list.crop_id
    IS 'Crop ID: Reference to the crop of the cross list [ENTLIST_CROP_ID]';

COMMENT ON COLUMN experiment.entry_list.program_id
    IS 'Program ID: Reference to the program of the cross list [ENTLIST_PROG_ID]';



-- revert changes
--rollback ALTER TABLE
--rollback     experiment.entry_list
--rollback DROP COLUMN
--rollback     season_id,
--rollback DROP COLUMN
--rollback     site_id,
--rollback DROP COLUMN
--rollback     crop_id,
--rollback DROP COLUMN
--rollback     program_id
--rollback ;



--changeset postgres:add_foreign_key_constraints context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-942 Add foreign key constraints



-- add foreign key constraints
-- season_id
ALTER TABLE
    experiment.entry_list
ADD CONSTRAINT
    entry_list_season_id_fk
FOREIGN KEY (
        season_id
    )
REFERENCES
    tenant.season (
        id
    )
    ON UPDATE CASCADE
    ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT
    entry_list_season_id_fk
ON
    experiment.entry_list
IS
    'A cross list is created in a season. A season can have zero or more cross lists.'
;

-- site_id
ALTER TABLE
    experiment.entry_list
ADD CONSTRAINT
    entry_list_site_id_fk
FOREIGN KEY (
        site_id
    )
REFERENCES
    place.geospatial_object (
        id
    )
    ON UPDATE CASCADE
    ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT
    entry_list_site_id_fk
ON
    experiment.entry_list
IS
    'A cross list is created in a site. A site can have zero or more cross lists.#'
;

-- crop_id
ALTER TABLE
    experiment.entry_list
ADD CONSTRAINT
    entry_list_crop_id_fk
FOREIGN KEY (
        crop_id
    )
REFERENCES
    tenant.crop (
        id
    )
    ON UPDATE CASCADE
    ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT
    entry_list_crop_id_fk
ON
    experiment.entry_list
IS
    'A cross list consists of only one crop. A crop can be created with zero or more cross lists.'
;

-- program_id
ALTER TABLE
    experiment.entry_list
ADD CONSTRAINT
    entry_list_program_id_fk
FOREIGN KEY (
        program_id
    )
REFERENCES
    tenant.program (
        id
    )
    ON UPDATE CASCADE
    ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT
    entry_list_program_id_fk
ON
    experiment.entry_list
IS
    'A cross list belongs to a program. A program can have zero or more cross lists.'
;



-- revert changes
--rollback ALTER TABLE experiment.entry_list
--rollback     DROP CONSTRAINT entry_list_season_id_fk
--rollback ;
--rollback 
--rollback ALTER TABLE experiment.entry_list
--rollback     DROP CONSTRAINT entry_list_site_id_fk
--rollback ;
--rollback 
--rollback ALTER TABLE experiment.entry_list
--rollback     DROP CONSTRAINT entry_list_crop_id_fk
--rollback ;
--rollback 
--rollback ALTER TABLE experiment.entry_list
--rollback     DROP CONSTRAINT entry_list_program_id_fk
--rollback ;



--changeset postgres:add_indices context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-942 Add indices



-- add indices
CREATE INDEX entry_list_parent_count_idx
    ON experiment.entry_list USING btree (parent_count);

CREATE INDEX entry_list_cross_count_idx
    ON experiment.entry_list USING btree (cross_count);

CREATE INDEX entry_list_experiment_year_idx
    ON experiment.entry_list USING btree (experiment_year);

CREATE INDEX entry_list_season_id_idx
    ON experiment.entry_list USING btree (season_id);

CREATE INDEX entry_list_site_id_idx
    ON experiment.entry_list USING btree (site_id);

CREATE INDEX entry_list_crop_id_idx
    ON experiment.entry_list USING btree (crop_id);

CREATE INDEX entry_list_program_id_idx
    ON experiment.entry_list USING btree (program_id);



-- revert changes
--rollback DROP INDEX experiment.entry_list_parent_count_idx;
--rollback DROP INDEX experiment.entry_list_cross_count_idx;
--rollback DROP INDEX experiment.entry_list_experiment_year_idx;
--rollback DROP INDEX experiment.entry_list_season_id_idx;
--rollback DROP INDEX experiment.entry_list_site_id_idx;
--rollback DROP INDEX experiment.entry_list_crop_id_idx;
--rollback DROP INDEX experiment.entry_list_program_id_idx;



--changeset postgres:drop_not_null_on_experiment_id_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-942 Drop not null on experiment_id column



-- drop not null
ALTER TABLE
    experiment.entry_list
ALTER COLUMN
    experiment_id
    DROP NOT NULL
;



-- revert changes
--rollback ALTER TABLE
--rollback     experiment.entry_list
--rollback ALTER COLUMN
--rollback     experiment_id
--rollback     SET NOT NULL
--rollback ;



--changeset postgres:update_column_comments context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-942 Update column comments



-- update comments
COMMENT ON COLUMN experiment.entry_list.event_log
    IS 'Event Log: Historical transactions of the record [ENTLIST_EVENTLOG]';

COMMENT ON COLUMN experiment.entry_list.entry_list_type
    IS 'Entry List Type: Type of the entry list {entry list, cross list} [ENTLIST_TYPE]';



-- revert changes
--rollback COMMENT ON COLUMN experiment.entry_list.event_log
--rollback     IS '';
--rollback 
--rollback COMMENT ON COLUMN experiment.entry_list.entry_list_type
--rollback     IS 'Type of the entry list';



--changeset postgres:add_check_constraints context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-942 Add check constraints



-- add check constraint
ALTER TABLE experiment.entry_list
    ADD CONSTRAINT entry_list_entry_list_type_chk
    CHECK (entry_list_type = ANY(ARRAY['entry list', 'cross list', 'parent list']))
;

ALTER TABLE experiment.entry_list
    ADD CONSTRAINT entry_list_entry_list_status_chk
    CHECK (entry_list_status = ANY(ARRAY['draft', 'created', 'completed']))
;



-- revert changes
--rollback ALTER TABLE experiment.entry_list
--rollback     DROP CONSTRAINT entry_list_entry_list_type_chk
--rollback ;
--rollback
--rollback ALTER TABLE experiment.entry_list
--rollback     DROP CONSTRAINT entry_list_entry_list_status_chk
--rollback ;



--changeset postgres:add_default_values context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-942 Add default values



-- add default value to entry_list_type
ALTER TABLE
    experiment.entry_list
ALTER COLUMN
    entry_list_type
    SET DEFAULT 'entry list'
;

-- add default value to entry_list_status
ALTER TABLE
    experiment.entry_list
ALTER COLUMN
    entry_list_status
    SET DEFAULT 'draft'
;



-- revert changes
--rollback ALTER TABLE
--rollback     experiment.entry_list
--rollback ALTER COLUMN
--rollback     entry_list_type
--rollback     DROP DEFAULT
--rollback ;
--rollback 
--rollback ALTER TABLE
--rollback     experiment.entry_list
--rollback ALTER COLUMN
--rollback     entry_list_status
--rollback     DROP DEFAULT
--rollback ;
