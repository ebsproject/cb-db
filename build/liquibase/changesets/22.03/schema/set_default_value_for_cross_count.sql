--liquibase formatted sql

--changeset postgres:set_default_value_for_cross_count context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1018 Set default value for cross_count



ALTER TABLE experiment.entry_list ALTER COLUMN cross_count SET DEFAULT 0;



--rollback ALTER TABLE experiment.entry_list ALTER COLUMN cross_count DROP DEFAULT;