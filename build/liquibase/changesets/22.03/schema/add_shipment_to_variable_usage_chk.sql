--liquibase formatted sql

--changeset postgres:add_shipment_to_variable_usage_chk context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-823 Add shipment to variable_usage_chk



ALTER TABLE master.variable DROP CONSTRAINT variable_usage_chk;

ALTER TABLE master.variable
    ADD CONSTRAINT variable_usage_chk CHECK (usage::text = ANY (ARRAY['application', 'undefined', 'management', 'experiment', 'occurrence', 'experiment_manager', 'shipment']));



--rollback ALTER TABLE master.variable DROP CONSTRAINT variable_usage_chk;
--rollback 
--rollback ALTER TABLE master.variable
--rollback     ADD CONSTRAINT variable_usage_chk CHECK (usage::text = ANY (ARRAY['application', 'undefined', 'management', 'experiment', 'occurrence', 'experiment_manager']));