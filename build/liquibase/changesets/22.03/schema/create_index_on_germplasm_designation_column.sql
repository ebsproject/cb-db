--liquibase formatted sql

--changeset postgres:create_index_on_germplasm_designation_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1012 Create varchar_pattern_ops index on germplasm designation column



-- create index
CREATE INDEX germplasm_designation_idx
    ON germplasm.germplasm USING btree (designation varchar_pattern_ops);



-- revert changes
--rollback DROP INDEX germplasm.germplasm_designation_idx;
