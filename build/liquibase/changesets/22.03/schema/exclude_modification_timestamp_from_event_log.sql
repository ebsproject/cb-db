--liquibase formatted sql

--changeset postgres:exclude_modification_timestamp_from_event_log context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-400 Remove modification_timestamp changes from being saved to event_log



-- exclude modification_timestamp from event_log
CREATE OR REPLACE FUNCTION platform.log_record_event()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE
    var_new_event_log JSONB;
    var_action_type VARCHAR;
    var_actor_id INTEGER;
    var_row_data JSONB;
    var_new_data JSONB;
    var_diff_data JSONB;
    var_excluded_columns TEXT[] = ARRAY['event_log', 'document', 'access_data', 'modification_timestamp']::TEXT[];
BEGIN
    IF (TG_WHEN <> 'BEFORE' OR NOT(ARRAY[TG_OP] <@ ARRAY['INSERT', 'UPDATE'])) THEN
        RAISE EXCEPTION 'ERROR Trigger function log_record_event() may only run on BEFORE INSERT OR UPDATE';
    END IF;
    
    IF (TG_OP = 'INSERT') THEN
        var_action_type = 'INSERT';
        var_actor_id = NEW.creator_id;
        var_row_data = hstore_to_jsonb_loose(hstore(NEW.*) - var_excluded_columns);
        
        var_new_event_log = (
            format($$
                [{
                    "log_timestamp": "%1$s",
                    "action_type": "%2$s",
                    "actor_id": "%3$s",
                    "row_data": null,
                    "new_data": %4$s,
                    "transaction_id": "%5$s"
                }]
            $$,
                current_timestamp,
                var_action_type,
                var_actor_id,
                var_row_data::TEXT,
                txid_current()
            )::jsonb
        )::jsonb;
    ELSIF (TG_OP = 'UPDATE') THEN
        var_row_data = hstore_to_jsonb_loose((hstore(OLD.*) - hstore(NEW.*)) - var_excluded_columns);
        var_new_data = hstore_to_jsonb_loose(((hstore(NEW.*) - hstore(OLD.*))) - var_excluded_columns);
        
        IF ((var_row_data is null and var_new_data is null)
            or (var_row_data = '{}' and var_row_data = '{}')) then
            return NEW;
        end if;
        
        IF (OLD.is_void = false AND NEW.is_void = true) THEN
            var_action_type = 'VOID';
        ELSIF (OLD.is_void = true AND NEW.is_void = false) THEN
            var_action_type = 'RESTORE';
        ELSE
            var_action_type = 'UPDATE';
        END IF;
        
        IF (NEW.modifier_id IS NULL) THEN
            var_actor_id = 0;
        ELSE
            var_actor_id = NEW.modifier_id;
        END IF;
        
        NEW.modification_timestamp = CURRENT_TIMESTAMP;
        
        if (var_row_data is null) then
            var_row_data = '{}'::jsonb;
        end if;
        
        if (var_new_data is null) then
            var_new_data = '{}'::jsonb;
        end if;
        
        if (OLD.event_log is null) then
            var_new_event_log = 
                format($$
                    [{
                        "log_timestamp": "%1$s",
                        "action_type": "%2$s",
                        "actor_id": "%3$s",
                        "row_data": %4$s,
                        "new_data": %5$s,
                        "transaction_id": "%6$s"
                    }]
                $$,
                    current_timestamp,
                    var_action_type,
                    var_actor_id,
                    var_row_data::TEXT,
                    var_new_data::TEXT,
                    txid_current()
                )::jsonb
            ;
        else
            var_new_event_log = (OLD.event_log || (
                format($$
                    [{
                        "log_timestamp": "%1$s",
                        "action_type": "%2$s",
                        "actor_id": "%3$s",
                        "row_data": %4$s,
                        "new_data": %5$s,
                        "transaction_id": "%6$s"
                    }]
                $$,
                    current_timestamp,
                    var_action_type,
                    var_actor_id,
                    var_row_data::TEXT,
                    var_new_data::TEXT,
                    txid_current()
                )::jsonb
            ))::jsonb;
        end if;
    END IF;
    
    NEW.event_log = var_new_event_log;
    
    RETURN NEW;
END; $function$
;



-- revert changes
--rollback CREATE OR REPLACE FUNCTION platform.log_record_event()
--rollback     RETURNS trigger
--rollback     LANGUAGE plpgsql
--rollback AS $function$
--rollback DECLARE
--rollback     var_new_event_log JSONB;
--rollback     var_action_type VARCHAR;
--rollback     var_actor_id INTEGER;
--rollback     var_row_data JSONB;
--rollback     var_new_data JSONB;
--rollback     var_diff_data JSONB;
--rollback     var_excluded_columns TEXT[] = ARRAY['event_log', 'document', 'access_data']::TEXT[];
--rollback BEGIN
--rollback     IF (TG_WHEN <> 'BEFORE' OR NOT(ARRAY[TG_OP] <@ ARRAY['INSERT', 'UPDATE'])) THEN
--rollback         RAISE EXCEPTION 'ERROR Trigger function log_record_event() may only run on BEFORE INSERT OR UPDATE';
--rollback     END IF;
--rollback     
--rollback     IF (TG_OP = 'INSERT') THEN
--rollback         var_action_type = 'INSERT';
--rollback         var_actor_id = NEW.creator_id;
--rollback         var_row_data = hstore_to_jsonb_loose(hstore(NEW.*) - var_excluded_columns);
--rollback         
--rollback         var_new_event_log = (
--rollback             format($$
--rollback                 [{
--rollback                     "log_timestamp": "%1$s",
--rollback                     "action_type": "%2$s",
--rollback                     "actor_id": "%3$s",
--rollback                     "row_data": null,
--rollback                     "new_data": %4$s,
--rollback                     "transaction_id": "%5$s"
--rollback                 }]
--rollback             $$,
--rollback                 current_timestamp,
--rollback                 var_action_type,
--rollback                 var_actor_id,
--rollback                 var_row_data::TEXT,
--rollback                 txid_current()
--rollback             )::jsonb
--rollback         )::jsonb;
--rollback     ELSIF (TG_OP = 'UPDATE') THEN
--rollback         var_row_data = hstore_to_jsonb_loose((hstore(OLD.*) - hstore(NEW.*)) - var_excluded_columns);
--rollback         var_new_data = hstore_to_jsonb_loose(((hstore(NEW.*) - hstore(OLD.*))) - var_excluded_columns);
--rollback         
--rollback         IF ((var_row_data is null and var_new_data is null)
--rollback             or (var_row_data = '{}' and var_row_data = '{}')) then
--rollback             return NEW;
--rollback         end if;
--rollback         
--rollback         IF (OLD.is_void = false AND NEW.is_void = true) THEN
--rollback             var_action_type = 'VOID';
--rollback         ELSIF (OLD.is_void = true AND NEW.is_void = false) THEN
--rollback             var_action_type = 'RESTORE';
--rollback         ELSE
--rollback             var_action_type = 'UPDATE';
--rollback         END IF;
--rollback         
--rollback         IF (NEW.modifier_id IS NULL) THEN
--rollback             var_actor_id = 0;
--rollback         ELSE
--rollback             var_actor_id = NEW.modifier_id;
--rollback         END IF;
--rollback         
--rollback         NEW.modification_timestamp = CURRENT_TIMESTAMP;
--rollback         
--rollback         if (var_row_data is null) then
--rollback             var_row_data = '{}'::jsonb;
--rollback         end if;
--rollback         
--rollback         if (var_new_data is null) then
--rollback             var_new_data = '{}'::jsonb;
--rollback         end if;
--rollback         
--rollback         if (OLD.event_log is null) then
--rollback             var_new_event_log = 
--rollback                 format($$
--rollback                     [{
--rollback                         "log_timestamp": "%1$s",
--rollback                         "action_type": "%2$s",
--rollback                         "actor_id": "%3$s",
--rollback                         "row_data": %4$s,
--rollback                         "new_data": %5$s,
--rollback                         "transaction_id": "%6$s"
--rollback                     }]
--rollback                 $$,
--rollback                     current_timestamp,
--rollback                     var_action_type,
--rollback                     var_actor_id,
--rollback                     var_row_data::TEXT,
--rollback                     var_new_data::TEXT,
--rollback                     txid_current()
--rollback                 )::jsonb
--rollback             ;
--rollback         else
--rollback             var_new_event_log = (OLD.event_log || (
--rollback                 format($$
--rollback                     [{
--rollback                         "log_timestamp": "%1$s",
--rollback                         "action_type": "%2$s",
--rollback                         "actor_id": "%3$s",
--rollback                         "row_data": %4$s,
--rollback                         "new_data": %5$s,
--rollback                         "transaction_id": "%6$s"
--rollback                     }]
--rollback                 $$,
--rollback                     current_timestamp,
--rollback                     var_action_type,
--rollback                     var_actor_id,
--rollback                     var_row_data::TEXT,
--rollback                     var_new_data::TEXT,
--rollback                     txid_current()
--rollback                 )::jsonb
--rollback             ))::jsonb;
--rollback         end if;
--rollback     END IF;
--rollback     
--rollback     NEW.event_log = var_new_event_log;
--rollback     
--rollback     RETURN NEW;
--rollback END; $function$
--rollback ;
