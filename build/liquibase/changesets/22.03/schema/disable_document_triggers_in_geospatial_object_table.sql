--liquibase formatted sql

--changeset postgres:disable_document_triggers_in_geospatial_object_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1013 Disable document triggers in place.geospatial_object table



-- disable document triggers
ALTER TABLE place.geospatial_object
    DISABLE TRIGGER geospatial_object_update_occurrence_document_from_site_tgr
;

ALTER TABLE place.geospatial_object
    DISABLE TRIGGER geospatial_object_update_occurrence_document_from_field_tgr
;

ALTER TABLE place.geospatial_object
    DISABLE TRIGGER geospatial_object_update_location_document_from_site_tgr
;

ALTER TABLE place.geospatial_object
    DISABLE TRIGGER geospatial_object_update_location_document_from_field_tgr
;



-- revert changes
--rollback ALTER TABLE place.geospatial_object
--rollback     ENABLE TRIGGER geospatial_object_update_occurrence_document_from_site_tgr
--rollback ;
--rollback 
--rollback ALTER TABLE place.geospatial_object
--rollback     ENABLE TRIGGER geospatial_object_update_occurrence_document_from_field_tgr
--rollback ;
--rollback 
--rollback ALTER TABLE place.geospatial_object
--rollback     ENABLE TRIGGER geospatial_object_update_location_document_from_site_tgr
--rollback ;
--rollback 
--rollback ALTER TABLE place.geospatial_object
--rollback     ENABLE TRIGGER geospatial_object_update_location_document_from_field_tgr
--rollback ;
