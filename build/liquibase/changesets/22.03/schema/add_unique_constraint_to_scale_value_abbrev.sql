--liquibase formatted sql

--changeset postgres:add_unique_constraint_to_scale_value_abbrev context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-990 Add unique constraint to scale value abbrev



CREATE UNIQUE INDEX scale_value_abbrev_uidx
    ON master.scale_value (abbrev)
;

ALTER TABLE master.scale_value
    ADD CONSTRAINT scale_value_abbrev_ukey
    UNIQUE USING INDEX scale_value_abbrev_uidx
;



-- revert changes
--rollback ALTER TABLE master.scale_value
--rollback     DROP CONSTRAINT scale_value_abbrev_ukey
--rollback ;
