--liquibase formatted sql

--changeset postgres:create_experiment.parent_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-942 Create experiment.parent table



-- create table
CREATE TABLE experiment.parent (
    -- primary key column
    id serial NOT NULL,
    
    -- columns
    order_number integer NOT NULL,
    parent_role varchar(64) NOT NULL,
    parent_type varchar(64),
    description text,
    
    -- references
    entry_list_id integer NOT NULL,
    experiment_id integer NOT NULL,
    occurrence_id integer NOT NULL,
    entry_id integer NOT NULL,
    plot_id integer,
    
    -- audit columns
    remarks text,
    creation_timestamp timestamptz NOT NULL DEFAULT now(),
    creator_id integer NOT NULL,
    modification_timestamp timestamptz,
    modifier_id integer,
    notes text,
    is_void boolean NOT NULL DEFAULT FALSE,
    event_log jsonb,
    
    -- primary key constraint
    CONSTRAINT parent_id_pk PRIMARY KEY (id),
    
    -- audit foreign key constraints
    CONSTRAINT parent_creator_id_fk FOREIGN KEY (creator_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT parent_modifier_id_fk FOREIGN KEY (modifier_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT
);

-- constraints
-- entry_list_id + order_number
CREATE UNIQUE INDEX parent_entry_list_id_order_number_uidx
    ON experiment.parent (entry_list_id, order_number)
;

ALTER TABLE experiment.parent
    ADD CONSTRAINT parent_entry_list_id_order_number_unq
    UNIQUE USING INDEX parent_entry_list_id_order_number_uidx
;

-- entry_list_id
ALTER TABLE experiment.parent
    ADD CONSTRAINT parent_entry_list_id_fk
    FOREIGN KEY (entry_list_id)
    REFERENCES experiment.entry_list (id)
    ON UPDATE CASCADE ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT parent_entry_list_id_fk
    ON experiment.parent
    IS 'A parent is imported to a cross list. A cross list can have one or more imported parents.'
;

-- experiment_id
ALTER TABLE experiment.parent
    ADD CONSTRAINT parent_experiment_id_fk
    FOREIGN KEY (experiment_id)
    REFERENCES experiment.experiment (id)
    ON UPDATE CASCADE ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT parent_experiment_id_fk
    ON experiment.parent
    IS 'A parent is imported from an experiment. An experiment can have an entry imported as a parent in a cross list.'
;

-- occurrence_id
ALTER TABLE experiment.parent
    ADD CONSTRAINT parent_occurrence_id_fk
    FOREIGN KEY (occurrence_id)
    REFERENCES experiment.occurrence (id)
    ON UPDATE CASCADE ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT parent_occurrence_id_fk
    ON experiment.parent
    IS 'A parent is imported from an occurrence. An occurrence can have an entry imported as a parent in a cross list.'
;

-- entry_id
ALTER TABLE experiment.parent
    ADD CONSTRAINT parent_entry_id_fk
    FOREIGN KEY (entry_id)
    REFERENCES experiment.entry (id)
    ON UPDATE CASCADE ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT parent_entry_id_fk
    ON experiment.parent
    IS 'A parent is imported from an entry. An entry can be imported as a parent in a cross list.'
;

-- plot_id
ALTER TABLE experiment.parent
    ADD CONSTRAINT parent_plot_id_fk
    FOREIGN KEY (plot_id)
    REFERENCES experiment.plot (id)
    ON UPDATE CASCADE ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT parent_plot_id_fk
    ON experiment.parent
    IS 'A parent can be imported from a plot. A plot can be used as the source of an imported parent in a cross list.'
;

-- table comment
COMMENT ON TABLE experiment.parent
    IS 'Parent: List of parents in a cross list that are imported from different occurrences/experiments';

-- column comments
COMMENT ON COLUMN experiment.parent.order_number
    IS 'Order Number: Ordering of the parent in the cross list [PARENT_ORDERNO]';
COMMENT ON COLUMN experiment.parent.parent_role
    IS 'Parent Role: Role of the parent in the cross list [PARENT_ROLE]';
COMMENT ON COLUMN experiment.parent.parent_type
    IS 'Parent Type: Purpose or type of the parent [PARENT_TYPE]';
COMMENT ON COLUMN experiment.parent.description
    IS 'Description: Additional details or remarks about the parent [PARENT_DESC]';

-- audit column comments
COMMENT ON COLUMN experiment.parent.id
    IS 'Parent ID: Database identifier of the record [PARENT_ID]';
COMMENT ON COLUMN experiment.parent.remarks
    IS 'Remarks: Additional notes to record [PARENT_REMARKS]';
COMMENT ON COLUMN experiment.parent.creation_timestamp
    IS 'Creation Timestamp: Timestamp when the record was created [PARENT_CTSTAMP]';
COMMENT ON COLUMN experiment.parent.creator_id
    IS 'Creator ID: Reference to the person who created the record [PARENT_CPERSON]';
COMMENT ON COLUMN experiment.parent.modification_timestamp
    IS 'Modification Timestamp: Timestamp when the record was last modified [PARENT_MTSTAMP]';
COMMENT ON COLUMN experiment.parent.modifier_id
    IS 'Modifier ID: Reference to the person who last modified the record [PARENT_MPERSON]';
COMMENT ON COLUMN experiment.parent.notes
    IS 'Notes: Technical details about the record [PARENT_NOTES]';
COMMENT ON COLUMN experiment.parent.is_void
    IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [PARENT_ISVOID]';
COMMENT ON COLUMN experiment.parent.event_log
    IS 'Event Log: Event Log: Historical transactions of the record [PARENT_EVENTLOG]';

-- indices
CREATE INDEX parent_entry_list_id_order_number_idx
    ON experiment.parent USING btree (entry_list_id, order_number);
CREATE INDEX parent_entry_list_id_parent_role_idx
    ON experiment.parent USING btree (entry_list_id, parent_role);
CREATE INDEX parent_entry_list_id_parent_type_idx
    ON experiment.parent USING btree (entry_list_id, parent_type);
CREATE INDEX parent_entry_list_id_idx
    ON experiment.parent USING btree (entry_list_id);
CREATE INDEX parent_experiment_id_idx
    ON experiment.parent USING btree (experiment_id);
CREATE INDEX parent_occurrence_id_idx
    ON experiment.parent USING btree (occurrence_id);
CREATE INDEX parent_entry_id_idx
    ON experiment.parent USING btree (entry_id);
CREATE INDEX parent_plot_id_idx
    ON experiment.parent USING btree (plot_id);

-- audit indices
CREATE INDEX parent_creator_id_idx ON experiment.parent USING btree (creator_id);
CREATE INDEX parent_modifier_id_idx ON experiment.parent USING btree (modifier_id);
CREATE INDEX parent_is_void_idx ON experiment.parent USING btree (is_void);

-- triggers
CREATE TRIGGER parent_event_log_tgr BEFORE INSERT OR UPDATE
    ON experiment.parent FOR EACH ROW EXECUTE FUNCTION platform.log_record_event();



-- revert changes
--rollback DROP TABLE experiment.parent;
