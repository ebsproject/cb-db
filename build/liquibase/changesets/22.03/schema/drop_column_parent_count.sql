--liquibase formatted sql

--changeset postgres:drop_column_parent_count context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1018 Drop column parent_count



ALTER TABLE experiment.entry_list DROP COLUMN parent_count;



--rollback ALTER TABLE experiment.entry_list
--rollback     ADD COLUMN parent_count integer;
--rollback 
--rollback COMMENT ON COLUMN experiment.entry_list.parent_count
--rollback     IS 'Parent Count: Total number of parents in the cross list [ENTLIST_PARENTCOUNT]';