--liquibase formatted sql

--changeset postgres:add_new_reference_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-942 Add new reference column



-- add occurrence_id reference
ALTER TABLE
    germplasm.cross_parent
ADD COLUMN
    occurrence_id integer
;

COMMENT ON COLUMN germplasm.cross_parent.occurrence_id
    IS 'Occurrence ID: Reference to the occurrence of the parent [CPARENT_OCC_ID]';



-- revert changes
--rollback ALTER TABLE
--rollback     germplasm.cross_parent
--rollback DROP COLUMN
--rollback     occurrence_id
--rollback ;



--changeset postgres:add_foreign_key_constraint context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-942 Add foreign key constraint



-- add foreign key constraint
ALTER TABLE germplasm.cross_parent
    ADD CONSTRAINT cross_parent_occurrence_id_fk
    FOREIGN KEY (occurrence_id)
    REFERENCES experiment.occurrence (id)
    ON UPDATE CASCADE ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT cross_parent_occurrence_id_fk
    ON germplasm.cross_parent
    IS 'A parent in a cross can come from an occurrence. An occurrence can be the source of zero or more parents in a cross.'
;



-- revert changes
--rollback ALTER TABLE germplasm.cross_parent
--rollback     DROP CONSTRAINT cross_parent_occurrence_id_fk
--rollback ;



--changeset postgres:add_index context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-942 Add index



-- add index
CREATE INDEX cross_parent_occurrence_id_idx
    ON germplasm.cross_parent USING btree (occurrence_id);



-- revert changes
--rollback DROP INDEX germplasm.cross_parent_occurrence_id_idx;
