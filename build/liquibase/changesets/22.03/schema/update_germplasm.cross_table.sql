--liquibase formatted sql

--changeset postgres:add_entry_list_id_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-942 Add entry_list_id column



-- add column
ALTER TABLE
    germplasm.cross
ADD COLUMN
    entry_list_id integer
;

COMMENT ON COLUMN germplasm.cross.entry_list_id
    IS 'Entry List ID: Reference to the cross list where the cross is created [CROSS_ENTLIST_ID]';


-- revert changes
--rollback ALTER TABLE
--rollback     germplasm.cross
--rollback DROP COLUMN
--rollback     entry_list_id
--rollback ;



--changeset postgres:add_foreign_key_constraint context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-942 Add foreign key constraint



-- add foreign key constraint
ALTER TABLE
    germplasm.cross
ADD CONSTRAINT
    cross_entry_list_id_fk
FOREIGN KEY (
        entry_list_id
    )
REFERENCES
    experiment.entry_list (
        id
    )
    ON UPDATE CASCADE
    ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT
    cross_entry_list_id_fk
ON
    germplasm.cross
IS
    'A cross can be created from a cross list. A cross list can have zero or more crosses.'
;



-- revert changes
--rollback ALTER TABLE germplasm.cross
--rollback     DROP CONSTRAINT cross_entry_list_id_fk
--rollback ;



--changeset postgres:add_index context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-942 Add index



-- add index
CREATE INDEX cross_entry_list_id_idx
    ON germplasm.cross USING btree (entry_list_id);



-- revert changes
--rollback DROP INDEX germplasm.cross_entry_list_id_idx;
