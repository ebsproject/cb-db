--liquibase formatted sql

--changeset postgres:fix_harvest_status_of_plots_with_ready_harvests context:fixture splitStatements:false rollbackSplitStatements:false
--preconditions onFail:HALT onError:HALT
--precondition-sql-check expectedResult:1 SELECT CASE EXISTS(SELECT 1 FROM experiment.plot AS plot JOIN experiment.planting_instruction AS plantinst ON plantinst.plot_id = plot.id JOIN germplasm.germplasm AS ge ON ge.id = plantinst.germplasm_id JOIN LATERAL ( SELECT plotdata.* FROM experiment.plot_data AS plotdata JOIN master.variable AS hvdatevar ON hvdatevar.abbrev = 'HVDATE_CONT' WHERE plotdata.plot_id = plot.id AND plotdata.variable_id = hvdatevar.id AND plotdata.is_void = FALSE LIMIT 1 ) AS hvdatedata ON TRUE JOIN LATERAL ( SELECT plotdata.* FROM experiment.plot_data AS plotdata JOIN master.variable AS hvmethvar ON hvmethvar.abbrev = 'HV_METH_DISC' WHERE plotdata.plot_id = plot.id AND plotdata.variable_id = hvmethvar.id AND plotdata.data_value = 'Bulk' AND plotdata.is_void = FALSE LIMIT 1 ) AS hvmethdata ON TRUE WHERE ge.germplasm_state = 'fixed' AND plot.harvest_status = 'NO_HARVEST' LIMIT 1) WHEN TRUE THEN 1 ELSE 0 END;
--comment: DB-1049 Fix harvest_status of plots from fixture experiments
--validCheckSum: 8:b8d81734e86332042b2342271dbd9c21



-- update harvest_status = READY
PREPARE fix_plot_harvest_status(integer) AS
    WITH t_plot AS (
        SELECT
            plot.id AS plot_id
        FROM
            experiment.plot AS plot
            JOIN experiment.planting_instruction AS plantinst
                ON plantinst.plot_id = plot.id
            JOIN germplasm.germplasm AS ge
                ON ge.id = plantinst.germplasm_id
            JOIN LATERAL (
                SELECT
                    plotdata.*
                FROM
                    experiment.plot_data AS plotdata
                    JOIN master.variable AS hvdatevar
                        ON hvdatevar.abbrev = 'HVDATE_CONT'
                WHERE
                    plotdata.plot_id = plot.id
                    AND plotdata.variable_id = hvdatevar.id
                    AND plotdata.is_void = FALSE
                LIMIT
                    1
            ) AS hvdatedata
                ON TRUE
            JOIN LATERAL (
                SELECT
                    plotdata.*
                FROM
                    experiment.plot_data AS plotdata
                    JOIN master.variable AS hvmethvar
                        ON hvmethvar.abbrev = 'HV_METH_DISC'
                WHERE
                    plotdata.plot_id = plot.id
                    AND plotdata.variable_id = hvmethvar.id
                    AND plotdata.data_value = 'Bulk'
                    AND plotdata.is_void = FALSE
                LIMIT
                    1
            ) AS hvmethdata
                ON TRUE
        WHERE
            ge.germplasm_state = 'fixed'
            AND plot.harvest_status = 'NO_HARVEST'
        LIMIT
            $1
    )
    UPDATE
        experiment.plot
    SET
        harvest_status = 'READY',
        notes = 'DB-1049 plot'
    FROM
        t_plot AS t
    WHERE
        t.plot_id = plot.id
;

EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);

DEALLOCATE fix_plot_harvest_status;



-- revert changes
-- SELECT NULL;



--changeset postgres:fix_harvest_status_of_plots_with_ready_harvests_part_2 context:fixture splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:1 SELECT CASE EXISTS(SELECT 1 FROM experiment.plot AS plot JOIN experiment.planting_instruction AS plantinst ON plantinst.plot_id = plot.id JOIN germplasm.germplasm AS ge ON ge.id = plantinst.germplasm_id JOIN LATERAL ( SELECT plotdata.* FROM experiment.plot_data AS plotdata JOIN master.variable AS hvdatevar ON hvdatevar.abbrev = 'HVDATE_CONT' WHERE plotdata.plot_id = plot.id AND plotdata.variable_id = hvdatevar.id AND plotdata.is_void = FALSE LIMIT 1 ) AS hvdatedata ON TRUE JOIN LATERAL ( SELECT plotdata.* FROM experiment.plot_data AS plotdata JOIN master.variable AS hvmethvar ON hvmethvar.abbrev = 'HV_METH_DISC' WHERE plotdata.plot_id = plot.id AND plotdata.variable_id = hvmethvar.id AND plotdata.data_value = 'Bulk' AND plotdata.is_void = FALSE LIMIT 1 ) AS hvmethdata ON TRUE WHERE ge.germplasm_state = 'fixed' AND plot.harvest_status = 'NO_HARVEST' LIMIT 1) WHEN TRUE THEN 1 ELSE 0 END;
--comment: DB-1049 Fix harvest_status of plots from fixture experiments (part 2)
--validCheckSum: 8:38b7e2c79f5ead3c54e912e8acc8463c



-- update harvest_status = READY
PREPARE fix_plot_harvest_status(integer) AS
    WITH t_plot AS (
        SELECT
            plot.id AS plot_id
        FROM
            experiment.plot AS plot
            JOIN experiment.planting_instruction AS plantinst
                ON plantinst.plot_id = plot.id
            JOIN germplasm.germplasm AS ge
                ON ge.id = plantinst.germplasm_id
            JOIN LATERAL (
                SELECT
                    plotdata.*
                FROM
                    experiment.plot_data AS plotdata
                    JOIN master.variable AS hvdatevar
                        ON hvdatevar.abbrev = 'HVDATE_CONT'
                WHERE
                    plotdata.plot_id = plot.id
                    AND plotdata.variable_id = hvdatevar.id
                    AND plotdata.is_void = FALSE
                LIMIT
                    1
            ) AS hvdatedata
                ON TRUE
            JOIN LATERAL (
                SELECT
                    plotdata.*
                FROM
                    experiment.plot_data AS plotdata
                    JOIN master.variable AS hvmethvar
                        ON hvmethvar.abbrev = 'HV_METH_DISC'
                WHERE
                    plotdata.plot_id = plot.id
                    AND plotdata.variable_id = hvmethvar.id
                    AND plotdata.data_value = 'Bulk'
                    AND plotdata.is_void = FALSE
                LIMIT
                    1
            ) AS hvmethdata
                ON TRUE
        WHERE
            ge.germplasm_state = 'fixed'
            AND plot.harvest_status = 'NO_HARVEST'
        LIMIT
            $1
    )
    UPDATE
        experiment.plot
    SET
        harvest_status = 'READY',
        notes = 'DB-1049 plot'
    FROM
        t_plot AS t
    WHERE
        t.plot_id = plot.id
;

EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);

DEALLOCATE fix_plot_harvest_status;



-- revert changes
--rollback UPDATE
--rollback     experiment.plot
--rollback SET
--rollback     harvest_status = 'NO_HARVEST',
--rollback     notes = REPLACE(notes, 'DB-1049 plot', '')
--rollback WHERE
--rollback     notes LIKE '%DB-1049 plot%'
--rollback ;



--changeset postgres:fix_harvest_status_of_plots_with_ready_harvests_part_3 context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:1 SELECT CASE EXISTS(SELECT 1 FROM experiment.plot AS plot JOIN experiment.planting_instruction AS plantinst ON plantinst.plot_id = plot.id JOIN germplasm.germplasm AS ge ON ge.id = plantinst.germplasm_id JOIN LATERAL ( SELECT plotdata.* FROM experiment.plot_data AS plotdata JOIN master.variable AS hvdatevar ON hvdatevar.abbrev = 'HVDATE_CONT' WHERE plotdata.plot_id = plot.id AND plotdata.variable_id = hvdatevar.id AND plotdata.is_void = FALSE LIMIT 1 ) AS hvdatedata ON TRUE JOIN LATERAL ( SELECT plotdata.* FROM experiment.plot_data AS plotdata JOIN master.variable AS hvmethvar ON hvmethvar.abbrev = 'HV_METH_DISC' WHERE plotdata.plot_id = plot.id AND plotdata.variable_id = hvmethvar.id AND plotdata.data_value = 'Bulk' AND plotdata.is_void = FALSE LIMIT 1 ) AS hvmethdata ON TRUE WHERE ge.germplasm_state = 'fixed' AND plot.harvest_status = 'NO_HARVEST' LIMIT 1) WHEN TRUE THEN 1 ELSE 0 END;
--comment: DB-1049 Fix harvest_status of plots from fixture experiments (part 3)
--validCheckSum: 8:b8d81734e86332042b2342271dbd9c21



-- update harvest_status = READY
PREPARE fix_plot_harvest_status(integer) AS
    WITH t_plot AS (
        SELECT
            plot.id AS plot_id
        FROM
            experiment.plot AS plot
            JOIN experiment.planting_instruction AS plantinst
                ON plantinst.plot_id = plot.id
            JOIN germplasm.germplasm AS ge
                ON ge.id = plantinst.germplasm_id
            JOIN LATERAL (
                SELECT
                    plotdata.*
                FROM
                    experiment.plot_data AS plotdata
                    JOIN master.variable AS hvdatevar
                        ON hvdatevar.abbrev = 'HVDATE_CONT'
                WHERE
                    plotdata.plot_id = plot.id
                    AND plotdata.variable_id = hvdatevar.id
                    AND plotdata.is_void = FALSE
                LIMIT
                    1
            ) AS hvdatedata
                ON TRUE
            JOIN LATERAL (
                SELECT
                    plotdata.*
                FROM
                    experiment.plot_data AS plotdata
                    JOIN master.variable AS hvmethvar
                        ON hvmethvar.abbrev = 'HV_METH_DISC'
                WHERE
                    plotdata.plot_id = plot.id
                    AND plotdata.variable_id = hvmethvar.id
                    AND plotdata.data_value = 'Bulk'
                    AND plotdata.is_void = FALSE
                LIMIT
                    1
            ) AS hvmethdata
                ON TRUE
        WHERE
            ge.germplasm_state = 'fixed'
            AND plot.harvest_status = 'NO_HARVEST'
        LIMIT
            $1
    )
    UPDATE
        experiment.plot
    SET
        harvest_status = 'READY',
        notes = 'DB-1049 plot'
    FROM
        t_plot AS t
    WHERE
        t.plot_id = plot.id
;

EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);

DEALLOCATE fix_plot_harvest_status;



-- revert changes
-- SELECT NULL;



--changeset postgres:fix_harvest_status_of_plots_with_ready_harvests_part_4 context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:1 SELECT CASE EXISTS(SELECT 1 FROM experiment.plot AS plot JOIN experiment.planting_instruction AS plantinst ON plantinst.plot_id = plot.id JOIN germplasm.germplasm AS ge ON ge.id = plantinst.germplasm_id JOIN LATERAL ( SELECT plotdata.* FROM experiment.plot_data AS plotdata JOIN master.variable AS hvdatevar ON hvdatevar.abbrev = 'HVDATE_CONT' WHERE plotdata.plot_id = plot.id AND plotdata.variable_id = hvdatevar.id AND plotdata.is_void = FALSE LIMIT 1 ) AS hvdatedata ON TRUE JOIN LATERAL ( SELECT plotdata.* FROM experiment.plot_data AS plotdata JOIN master.variable AS hvmethvar ON hvmethvar.abbrev = 'HV_METH_DISC' WHERE plotdata.plot_id = plot.id AND plotdata.variable_id = hvmethvar.id AND plotdata.data_value = 'Bulk' AND plotdata.is_void = FALSE LIMIT 1 ) AS hvmethdata ON TRUE WHERE ge.germplasm_state = 'fixed' AND plot.harvest_status = 'NO_HARVEST' LIMIT 1) WHEN TRUE THEN 1 ELSE 0 END;
--comment: DB-1049 Fix harvest_status of plots from fixture experiments (part 4)
--validCheckSum: 8:b8d81734e86332042b2342271dbd9c21



-- update harvest_status = READY
PREPARE fix_plot_harvest_status(integer) AS
    WITH t_plot AS (
        SELECT
            plot.id AS plot_id
        FROM
            experiment.plot AS plot
            JOIN experiment.planting_instruction AS plantinst
                ON plantinst.plot_id = plot.id
            JOIN germplasm.germplasm AS ge
                ON ge.id = plantinst.germplasm_id
            JOIN LATERAL (
                SELECT
                    plotdata.*
                FROM
                    experiment.plot_data AS plotdata
                    JOIN master.variable AS hvdatevar
                        ON hvdatevar.abbrev = 'HVDATE_CONT'
                WHERE
                    plotdata.plot_id = plot.id
                    AND plotdata.variable_id = hvdatevar.id
                    AND plotdata.is_void = FALSE
                LIMIT
                    1
            ) AS hvdatedata
                ON TRUE
            JOIN LATERAL (
                SELECT
                    plotdata.*
                FROM
                    experiment.plot_data AS plotdata
                    JOIN master.variable AS hvmethvar
                        ON hvmethvar.abbrev = 'HV_METH_DISC'
                WHERE
                    plotdata.plot_id = plot.id
                    AND plotdata.variable_id = hvmethvar.id
                    AND plotdata.data_value = 'Bulk'
                    AND plotdata.is_void = FALSE
                LIMIT
                    1
            ) AS hvmethdata
                ON TRUE
        WHERE
            ge.germplasm_state = 'fixed'
            AND plot.harvest_status = 'NO_HARVEST'
        LIMIT
            $1
    )
    UPDATE
        experiment.plot
    SET
        harvest_status = 'READY',
        notes = 'DB-1049 plot'
    FROM
        t_plot AS t
    WHERE
        t.plot_id = plot.id
;

EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);

DEALLOCATE fix_plot_harvest_status;



-- revert changes
-- SELECT NULL;



--changeset postgres:fix_harvest_status_of_plots_with_ready_harvests_part_5 context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:1 SELECT CASE EXISTS(SELECT 1 FROM experiment.plot AS plot JOIN experiment.planting_instruction AS plantinst ON plantinst.plot_id = plot.id JOIN germplasm.germplasm AS ge ON ge.id = plantinst.germplasm_id JOIN LATERAL ( SELECT plotdata.* FROM experiment.plot_data AS plotdata JOIN master.variable AS hvdatevar ON hvdatevar.abbrev = 'HVDATE_CONT' WHERE plotdata.plot_id = plot.id AND plotdata.variable_id = hvdatevar.id AND plotdata.is_void = FALSE LIMIT 1 ) AS hvdatedata ON TRUE JOIN LATERAL ( SELECT plotdata.* FROM experiment.plot_data AS plotdata JOIN master.variable AS hvmethvar ON hvmethvar.abbrev = 'HV_METH_DISC' WHERE plotdata.plot_id = plot.id AND plotdata.variable_id = hvmethvar.id AND plotdata.data_value = 'Bulk' AND plotdata.is_void = FALSE LIMIT 1 ) AS hvmethdata ON TRUE WHERE ge.germplasm_state = 'fixed' AND plot.harvest_status = 'NO_HARVEST' LIMIT 1) WHEN TRUE THEN 1 ELSE 0 END;
--comment: DB-1049 Fix harvest_status of plots from fixture experiments (part 5)
--validCheckSum: 8:b8d81734e86332042b2342271dbd9c21



-- update harvest_status = READY
PREPARE fix_plot_harvest_status(integer) AS
    WITH t_plot AS (
        SELECT
            plot.id AS plot_id
        FROM
            experiment.plot AS plot
            JOIN experiment.planting_instruction AS plantinst
                ON plantinst.plot_id = plot.id
            JOIN germplasm.germplasm AS ge
                ON ge.id = plantinst.germplasm_id
            JOIN LATERAL (
                SELECT
                    plotdata.*
                FROM
                    experiment.plot_data AS plotdata
                    JOIN master.variable AS hvdatevar
                        ON hvdatevar.abbrev = 'HVDATE_CONT'
                WHERE
                    plotdata.plot_id = plot.id
                    AND plotdata.variable_id = hvdatevar.id
                    AND plotdata.is_void = FALSE
                LIMIT
                    1
            ) AS hvdatedata
                ON TRUE
            JOIN LATERAL (
                SELECT
                    plotdata.*
                FROM
                    experiment.plot_data AS plotdata
                    JOIN master.variable AS hvmethvar
                        ON hvmethvar.abbrev = 'HV_METH_DISC'
                WHERE
                    plotdata.plot_id = plot.id
                    AND plotdata.variable_id = hvmethvar.id
                    AND plotdata.data_value = 'Bulk'
                    AND plotdata.is_void = FALSE
                LIMIT
                    1
            ) AS hvmethdata
                ON TRUE
        WHERE
            ge.germplasm_state = 'fixed'
            AND plot.harvest_status = 'NO_HARVEST'
        LIMIT
            $1
    )
    UPDATE
        experiment.plot
    SET
        harvest_status = 'READY',
        notes = 'DB-1049 plot'
    FROM
        t_plot AS t
    WHERE
        t.plot_id = plot.id
;

EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);

DEALLOCATE fix_plot_harvest_status;



-- revert changes
-- SELECT NULL;



--changeset postgres:fix_harvest_status_of_plots_with_ready_harvests_part_6 context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:1 SELECT CASE EXISTS(SELECT 1 FROM experiment.plot AS plot JOIN experiment.planting_instruction AS plantinst ON plantinst.plot_id = plot.id JOIN germplasm.germplasm AS ge ON ge.id = plantinst.germplasm_id JOIN LATERAL ( SELECT plotdata.* FROM experiment.plot_data AS plotdata JOIN master.variable AS hvdatevar ON hvdatevar.abbrev = 'HVDATE_CONT' WHERE plotdata.plot_id = plot.id AND plotdata.variable_id = hvdatevar.id AND plotdata.is_void = FALSE LIMIT 1 ) AS hvdatedata ON TRUE JOIN LATERAL ( SELECT plotdata.* FROM experiment.plot_data AS plotdata JOIN master.variable AS hvmethvar ON hvmethvar.abbrev = 'HV_METH_DISC' WHERE plotdata.plot_id = plot.id AND plotdata.variable_id = hvmethvar.id AND plotdata.data_value = 'Bulk' AND plotdata.is_void = FALSE LIMIT 1 ) AS hvmethdata ON TRUE WHERE ge.germplasm_state = 'fixed' AND plot.harvest_status = 'NO_HARVEST' LIMIT 1) WHEN TRUE THEN 1 ELSE 0 END;
--comment: DB-1049 Fix harvest_status of plots from fixture experiments (part 6)
--validCheckSum: 8:b8d81734e86332042b2342271dbd9c21



-- update harvest_status = READY
PREPARE fix_plot_harvest_status(integer) AS
    WITH t_plot AS (
        SELECT
            plot.id AS plot_id
        FROM
            experiment.plot AS plot
            JOIN experiment.planting_instruction AS plantinst
                ON plantinst.plot_id = plot.id
            JOIN germplasm.germplasm AS ge
                ON ge.id = plantinst.germplasm_id
            JOIN LATERAL (
                SELECT
                    plotdata.*
                FROM
                    experiment.plot_data AS plotdata
                    JOIN master.variable AS hvdatevar
                        ON hvdatevar.abbrev = 'HVDATE_CONT'
                WHERE
                    plotdata.plot_id = plot.id
                    AND plotdata.variable_id = hvdatevar.id
                    AND plotdata.is_void = FALSE
                LIMIT
                    1
            ) AS hvdatedata
                ON TRUE
            JOIN LATERAL (
                SELECT
                    plotdata.*
                FROM
                    experiment.plot_data AS plotdata
                    JOIN master.variable AS hvmethvar
                        ON hvmethvar.abbrev = 'HV_METH_DISC'
                WHERE
                    plotdata.plot_id = plot.id
                    AND plotdata.variable_id = hvmethvar.id
                    AND plotdata.data_value = 'Bulk'
                    AND plotdata.is_void = FALSE
                LIMIT
                    1
            ) AS hvmethdata
                ON TRUE
        WHERE
            ge.germplasm_state = 'fixed'
            AND plot.harvest_status = 'NO_HARVEST'
        LIMIT
            $1
    )
    UPDATE
        experiment.plot
    SET
        harvest_status = 'READY',
        notes = 'DB-1049 plot'
    FROM
        t_plot AS t
    WHERE
        t.plot_id = plot.id
;

EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);

DEALLOCATE fix_plot_harvest_status;



-- revert changes
-- SELECT NULL;



--changeset postgres:fix_harvest_status_of_plots_with_ready_harvests_part_7 context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:1 SELECT CASE EXISTS(SELECT 1 FROM experiment.plot AS plot JOIN experiment.planting_instruction AS plantinst ON plantinst.plot_id = plot.id JOIN germplasm.germplasm AS ge ON ge.id = plantinst.germplasm_id JOIN LATERAL ( SELECT plotdata.* FROM experiment.plot_data AS plotdata JOIN master.variable AS hvdatevar ON hvdatevar.abbrev = 'HVDATE_CONT' WHERE plotdata.plot_id = plot.id AND plotdata.variable_id = hvdatevar.id AND plotdata.is_void = FALSE LIMIT 1 ) AS hvdatedata ON TRUE JOIN LATERAL ( SELECT plotdata.* FROM experiment.plot_data AS plotdata JOIN master.variable AS hvmethvar ON hvmethvar.abbrev = 'HV_METH_DISC' WHERE plotdata.plot_id = plot.id AND plotdata.variable_id = hvmethvar.id AND plotdata.data_value = 'Bulk' AND plotdata.is_void = FALSE LIMIT 1 ) AS hvmethdata ON TRUE WHERE ge.germplasm_state = 'fixed' AND plot.harvest_status = 'NO_HARVEST' LIMIT 1) WHEN TRUE THEN 1 ELSE 0 END;
--comment: DB-1049 Fix harvest_status of plots from fixture experiments (part 7)
--validCheckSum: 8:38b7e2c79f5ead3c54e912e8acc8463c



-- update harvest_status = READY
PREPARE fix_plot_harvest_status(integer) AS
    WITH t_plot AS (
        SELECT
            plot.id AS plot_id
        FROM
            experiment.plot AS plot
            JOIN experiment.planting_instruction AS plantinst
                ON plantinst.plot_id = plot.id
            JOIN germplasm.germplasm AS ge
                ON ge.id = plantinst.germplasm_id
            JOIN LATERAL (
                SELECT
                    plotdata.*
                FROM
                    experiment.plot_data AS plotdata
                    JOIN master.variable AS hvdatevar
                        ON hvdatevar.abbrev = 'HVDATE_CONT'
                WHERE
                    plotdata.plot_id = plot.id
                    AND plotdata.variable_id = hvdatevar.id
                    AND plotdata.is_void = FALSE
                LIMIT
                    1
            ) AS hvdatedata
                ON TRUE
            JOIN LATERAL (
                SELECT
                    plotdata.*
                FROM
                    experiment.plot_data AS plotdata
                    JOIN master.variable AS hvmethvar
                        ON hvmethvar.abbrev = 'HV_METH_DISC'
                WHERE
                    plotdata.plot_id = plot.id
                    AND plotdata.variable_id = hvmethvar.id
                    AND plotdata.data_value = 'Bulk'
                    AND plotdata.is_void = FALSE
                LIMIT
                    1
            ) AS hvmethdata
                ON TRUE
        WHERE
            ge.germplasm_state = 'fixed'
            AND plot.harvest_status = 'NO_HARVEST'
        LIMIT
            $1
    )
    UPDATE
        experiment.plot
    SET
        harvest_status = 'READY',
        notes = 'DB-1049 plot'
    FROM
        t_plot AS t
    WHERE
        t.plot_id = plot.id
;

EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);
EXECUTE fix_plot_harvest_status(100000);

DEALLOCATE fix_plot_harvest_status;



-- revert changes
--rollback UPDATE
--rollback     experiment.plot
--rollback SET
--rollback     harvest_status = 'NO_HARVEST',
--rollback     notes = REPLACE(notes, 'DB-1049 plot', '')
--rollback WHERE
--rollback     notes LIKE '%DB-1049 plot%'
--rollback ;
