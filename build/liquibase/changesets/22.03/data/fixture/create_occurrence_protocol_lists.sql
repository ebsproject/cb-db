--liquibase formatted sql

--changeset postgres:create_occurrence_management_protocol_lists context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-952 Create occurrence management protocol lists



-- create management protocol lists
INSERT INTO
    platform.list (
        abbrev, name, display_name, type, list_sub_type, entity_id, list_usage, status, is_active, creator_id, notes
    )
SELECT
    'MANAGEMENT_PROTOCOL_' || t_occ.occurrence_code AS abbrev,
    t_occ.occurrence_name || ' Management Protocol (' || t_occ.occurrence_code || ')' AS name,
    t_occ.occurrence_name || ' Management Protocol (' || t_occ.occurrence_code || ')' AS display_name,
    'variable' AS type,
    'management protocol' AS list_sub_type,
    enty.id AS entity_id,
    'working list' AS list_usage,
    'created' AS status,
    TRUE AS is_active,
    t_occ.creator_id AS creator_id,
    'DB-952 occurrence management protocol list' AS notes
FROM
    (
        SELECT
            expt.id AS experiment_id,
            expt.experiment_code,
            expt.experiment_name,
            occ.id AS occurrence_id,
            occ.occurrence_code,
            occ.occurrence_name,
            occ.creator_id,
            EXISTS(
                SELECT
                    *
                FROM
                    experiment.occurrence_data AS occdata
                    JOIN master.variable AS var
                        ON var.id = occdata.variable_id
                WHERE
                    occdata.occurrence_id = occ.id
                    AND var.abbrev = 'MANAGEMENT_PROTOCOL_LIST_ID'
                    AND occdata.is_void = FALSE
            ) AS has_protocol_list
        FROM
            experiment.experiment AS expt
            INNER JOIN experiment.occurrence AS occ
                ON occ.experiment_id = expt.id
        WHERE
            expt.experiment_status IN ('mapped', 'planted', 'completed', 'created')
            AND expt.is_void = FALSE
            AND occ.is_void = FALSE
    ) AS t_occ
    LEFT JOIN dictionary.entity AS enty
        ON enty.abbrev = 'VARIABLE'
WHERE
    NOT t_occ.has_protocol_list
;



-- revert changes
--rollback DELETE FROM
--rollback     platform.list AS lst
--rollback WHERE
--rollback     lst.notes = 'DB-952 occurrence management protocol list'
--rollback ;



--changeset postgres:create_experiment_management_protocols context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-952 Create experiment management protocols



-- create experiment management protocols
INSERT INTO
    tenant.protocol (
        protocol_code, protocol_name, protocol_type, program_id, creator_id, notes
    )
SELECT
    DISTINCT
    'MANAGEMENT_PROTOCOL_' || t_occ.experiment_code AS protocol_code,
    'Management Protocol ' || t_occ.experiment_code AS protocol_name,
    'management' AS protocol_type,
    t_occ.program_id,
    t_occ.creator_id,
    'DB-952 experiment management protocol' AS notes
FROM
    (
        SELECT
            expt.id AS experiment_id,
            expt.experiment_code,
            expt.experiment_name,
            expt.program_id,
            occ.id AS occurrence_id,
            occ.occurrence_code,
            occ.occurrence_name,
            occ.creator_id,
            EXISTS(
                SELECT
                    *
                FROM
                    experiment.occurrence_data AS occdata
                    JOIN master.variable AS var
                        ON var.id = occdata.variable_id
                WHERE
                    occdata.occurrence_id = occ.id
                    AND var.abbrev = 'MANAGEMENT_PROTOCOL_LIST_ID'
                    AND occdata.is_void = FALSE
            ) AS has_protocol_list,
            EXISTS(
                SELECT
                    *
                FROM
                    tenant.protocol AS prot
                WHERE
                    prot.protocol_code = 'MANAGEMENT_PROTOCOL_' || expt.experiment_code
                    AND prot.is_void = FALSE
            ) AS has_protocol
        FROM
            experiment.experiment AS expt
            INNER JOIN experiment.occurrence AS occ
                ON occ.experiment_id = expt.id
        WHERE
            expt.experiment_status IN ('mapped', 'planted', 'completed', 'created')
            AND expt.is_void = FALSE
    ) AS t_occ
WHERE
    NOT t_occ.has_protocol_list
    AND NOT t_occ.has_protocol
;



-- revert changes
--rollback DELETE FROM
--rollback     tenant.protocol AS prot
--rollback WHERE
--rollback     prot.notes = 'DB-952 experiment management protocol'
--rollback ;



--changeset postgres:link_management_protocol_lists_to_occurrences context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-952 Link management protocol lists to occurrences



-- link management protocol lists to occurrences
INSERT INTO
    experiment.occurrence_data (
        occurrence_id, variable_id, data_value, data_qc_code, creator_id, protocol_id, notes
    )
SELECT
    t_occ.occurrence_id,
    var2.id AS variable_id,
    lst.id AS data_value,
    'G' AS data_qc_code,
    t_occ.creator_id,
    prot.id AS protocol_id,
    'DB-952 occurrence management protocol list' AS notes
FROM (
        SELECT
            expt.id AS experiment_id,
            expt.experiment_code,
            expt.experiment_name,
            occ.id AS occurrence_id,
            occ.occurrence_code,
            occ.occurrence_name,
            occ.creator_id,
            EXISTS(
                SELECT
                    *
                FROM
                    experiment.occurrence_data AS occdata
                    JOIN master.variable AS var
                        ON var.id = occdata.variable_id
                WHERE
                    occdata.occurrence_id = occ.id
                    AND var.abbrev = 'MANAGEMENT_PROTOCOL_LIST_ID'
                    AND occdata.is_void = FALSE
            ) AS has_protocol_list
        FROM
            experiment.experiment AS expt
            INNER JOIN experiment.occurrence AS occ
                ON occ.experiment_id = expt.id
        WHERE
            expt.experiment_status IN ('mapped', 'planted', 'completed', 'created')
            AND expt.is_void = FALSE
            AND occ.is_void = FALSE
    ) AS t_occ
    LEFT JOIN dictionary.entity AS enty
        ON enty.abbrev = 'VARIABLE'
    LEFT JOIN platform.list AS lst
        ON lst.abbrev = 'MANAGEMENT_PROTOCOL_' || t_occ.occurrence_code
    LEFT JOIN tenant.protocol AS prot
        ON prot.protocol_code = 'MANAGEMENT_PROTOCOL_' || t_occ.experiment_code
    LEFT JOIN master.variable AS var2
        ON var2.abbrev = 'MANAGEMENT_PROTOCOL_LIST_ID'
WHERE
    lst.id IS NOT NULL
    AND NOT t_occ.has_protocol_list
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence_data AS occdata
--rollback WHERE
--rollback     occdata.notes = 'DB-952 occurrence management protocol list'
--rollback ;



--changeset postgres:create_occurrence_trait_protocol_lists context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-952 Create occurrence trait protocol lists



-- create trait protocol lists
INSERT INTO
    platform.list (
        abbrev, name, display_name, type, list_sub_type, entity_id, list_usage, status, is_active, creator_id, notes
    )
SELECT
    'TRAIT_PROTOCOL_' || t_occ.occurrence_code AS abbrev,
    t_occ.occurrence_name || ' Trait Protocol (' || t_occ.occurrence_code || ')' AS name,
    t_occ.occurrence_name || ' Trait Protocol (' || t_occ.occurrence_code || ')' AS display_name,
    'trait' AS type,
    'trait protocol' AS list_sub_type,
    enty.id AS entity_id,
    'working list' AS list_usage,
    'created' AS status,
    TRUE AS is_active,
    t_occ.creator_id AS creator_id,
    'DB-952 occurrence trait protocol list' AS notes
FROM
    (
        SELECT
            expt.id AS experiment_id,
            expt.experiment_code,
            expt.experiment_name,
            occ.id AS occurrence_id,
            occ.occurrence_code,
            occ.occurrence_name,
            occ.creator_id,
            EXISTS(
                SELECT
                    *
                FROM
                    experiment.occurrence_data AS occdata
                    JOIN master.variable AS var
                        ON var.id = occdata.variable_id
                WHERE
                    occdata.occurrence_id = occ.id
                    AND var.abbrev = 'TRAIT_PROTOCOL_LIST_ID'
                    AND occdata.is_void = FALSE
            ) AS has_protocol_list
        FROM
            experiment.experiment AS expt
            INNER JOIN experiment.occurrence AS occ
                ON occ.experiment_id = expt.id
        WHERE
            expt.experiment_status IN ('mapped', 'planted', 'completed', 'created')
            AND expt.is_void = FALSE
            AND occ.is_void = FALSE
    ) AS t_occ
    LEFT JOIN dictionary.entity AS enty
        ON enty.abbrev = 'TRAIT'
WHERE
    NOT t_occ.has_protocol_list
;



-- revert changes
--rollback DELETE FROM
--rollback     platform.list AS lst
--rollback WHERE
--rollback     lst.notes = 'DB-952 occurrence trait protocol list'
--rollback ;



--changeset postgres:create_experiment_trait_protocols context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-952 Create experiment trait protocols



-- create experiment trait protocols
INSERT INTO
    tenant.protocol (
        protocol_code, protocol_name, protocol_type, program_id, creator_id, notes
    )
SELECT
    DISTINCT
    'TRAIT_PROTOCOL_' || t_occ.experiment_code AS protocol_code,
    'Trait Protocol ' || t_occ.experiment_code AS protocol_name,
    'trait' AS protocol_type,
    t_occ.program_id,
    t_occ.creator_id,
    'DB-952 experiment trait protocol' AS notes
FROM
    (
        SELECT
            expt.id AS experiment_id,
            expt.experiment_code,
            expt.experiment_name,
            expt.program_id,
            occ.id AS occurrence_id,
            occ.occurrence_code,
            occ.occurrence_name,
            occ.creator_id,
            EXISTS(
                SELECT
                    *
                FROM
                    experiment.occurrence_data AS occdata
                    JOIN master.variable AS var
                        ON var.id = occdata.variable_id
                WHERE
                    occdata.occurrence_id = occ.id
                    AND var.abbrev = 'TRAIT_PROTOCOL_LIST_ID'
                    AND occdata.is_void = FALSE
            ) AS has_protocol_list,
            EXISTS(
                SELECT
                    *
                FROM
                    tenant.protocol AS prot
                WHERE
                    prot.protocol_code = 'TRAIT_PROTOCOL_' || expt.experiment_code
                    AND prot.is_void = FALSE
            ) AS has_protocol
        FROM
            experiment.experiment AS expt
            INNER JOIN experiment.occurrence AS occ
                ON occ.experiment_id = expt.id
        WHERE
            expt.experiment_status IN ('mapped', 'planted', 'completed', 'created')
            AND expt.is_void = FALSE
    ) AS t_occ
WHERE
    NOT t_occ.has_protocol_list
    AND NOT t_occ.has_protocol
;



-- revert changes
--rollback DELETE FROM
--rollback     tenant.protocol AS prot
--rollback WHERE
--rollback     prot.notes = 'DB-952 experiment trait protocol'
--rollback ;



--changeset postgres:link_trait_protocol_lists_to_occurrences context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-952 Link trait protocol lists to occurrences



-- link trait protocol lists to occurrences
INSERT INTO
    experiment.occurrence_data (
        occurrence_id, variable_id, data_value, data_qc_code, creator_id, protocol_id, notes
    )
SELECT
    t_occ.occurrence_id,
    var2.id AS variable_id,
    lst.id AS data_value,
    'G' AS data_qc_code,
    t_occ.creator_id,
    prot.id AS protocol_id,
    'DB-952 occurrence trait protocol list' AS notes
FROM (
        SELECT
            expt.id AS experiment_id,
            expt.experiment_code,
            expt.experiment_name,
            occ.id AS occurrence_id,
            occ.occurrence_code,
            occ.occurrence_name,
            occ.creator_id,
            EXISTS(
                SELECT
                    *
                FROM
                    experiment.occurrence_data AS occdata
                    JOIN master.variable AS var
                        ON var.id = occdata.variable_id
                WHERE
                    occdata.occurrence_id = occ.id
                    AND var.abbrev = 'TRAIT_PROTOCOL_LIST_ID'
                    AND occdata.is_void = FALSE
            ) AS has_protocol_list
        FROM
            experiment.experiment AS expt
            INNER JOIN experiment.occurrence AS occ
                ON occ.experiment_id = expt.id
        WHERE
            expt.experiment_status IN ('mapped', 'planted', 'completed', 'created')
            AND expt.is_void = FALSE
            AND occ.is_void = FALSE
    ) AS t_occ
    LEFT JOIN dictionary.entity AS enty
        ON enty.abbrev = 'TRAIT'
    LEFT JOIN platform.list AS lst
        ON lst.abbrev = 'TRAIT_PROTOCOL_' || t_occ.occurrence_code
    LEFT JOIN tenant.protocol AS prot
        ON prot.protocol_code = 'TRAIT_PROTOCOL_' || t_occ.experiment_code
    LEFT JOIN master.variable AS var2
        ON var2.abbrev = 'TRAIT_PROTOCOL_LIST_ID'
WHERE
    lst.id IS NOT NULL
    AND NOT t_occ.has_protocol_list
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence_data AS occdata
--rollback WHERE
--rollback     occdata.notes = 'DB-952 occurrence trait protocol list'
--rollback ;
