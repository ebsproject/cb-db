--liquibase formatted sql

--changeset postgres:generate_wtp_packages_from_bw_seeds context:fixture labels:stress splitStatements:false rollbackSplitStatements:false
--comment: DB-977 Generate WTP packages linked to BW seeds for testing



-- 1000 WTP packages linked to BW seeds
INSERT INTO
   germplasm.package (
       package_code,
       package_label,
       package_quantity,
       package_unit,
       package_status,
       seed_id,
       program_id,
       geospatial_object_id,
       facility_id,
       creator_id,
       notes
   )
SELECT
    germplasm.generate_code('package') AS package_code,
    ('WTP-' || sd.seed_name || t.package_label || gs.num) AS package_label,
    floor(random() * 300) AS package_quantity,
    t.package_unit,
    t.package_status,
    sd.id AS seed_id,
    prog2.id AS program_id,
    NULL AS geospatial_object_id,
    NULL AS facility_id,
    sd.creator_id,
    'DB-977 package' AS notes
FROM (
        VALUES
        ('-PKG', 'g', 'active')
    ) AS t (
        package_label,
        package_unit,
        package_status
    )
    JOIN tenant.crop AS crop
        ON crop.crop_code = 'WHEAT'
    JOIN tenant.program AS prog
        ON prog.program_code = 'BW'
    JOIN tenant.program AS prog2
        ON prog2.program_code = 'WTP'
    JOIN germplasm.germplasm AS ge
        ON ge.crop_id = crop.id
    JOIN germplasm.seed AS sd
        ON sd.germplasm_id = ge.id
        AND sd.program_id = prog.id,
    generate_series(1, 1) AS gs (num)
WHERE
    ge.germplasm_state = 'fixed'
ORDER BY
    RANDOM()
LIMIT
    1000
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.package AS pkg
--rollback USING
--rollback     tenant.program AS prog
--rollback WHERE
--rollback     pkg.program_id = prog.id
--rollback     AND prog.program_code = 'WTP'
--rollback     AND pkg.notes = 'DB-977 package'
--rollback ;



--changeset postgres:generate_bw_packages_from_wtp_seeds context:fixture labels:stress splitStatements:false rollbackSplitStatements:false
--comment: DB-977 Generate BW packages linked to WTP seeds for testing



-- 1000 BW packages linked to WTP seeds
INSERT INTO
   germplasm.package (
       package_code,
       package_label,
       package_quantity,
       package_unit,
       package_status,
       seed_id,
       program_id,
       geospatial_object_id,
       facility_id,
       creator_id,
       notes
   )
SELECT
    germplasm.generate_code('package') AS package_code,
    ('BW-' || sd.seed_name || t.package_label || gs.num) AS package_label,
    floor(random() * 300) AS package_quantity,
    t.package_unit,
    t.package_status,
    sd.id AS seed_id,
    prog2.id AS program_id,
    NULL AS geospatial_object_id,
    NULL AS facility_id,
    sd.creator_id,
    'DB-977 package' AS notes
FROM (
        VALUES
        ('-PKG', 'g', 'active')
    ) AS t (
        package_label,
        package_unit,
        package_status
    )
    JOIN tenant.crop AS crop
        ON crop.crop_code = 'WHEAT'
    JOIN tenant.program AS prog
        ON prog.program_code = 'WTP'
    JOIN tenant.program AS prog2
        ON prog2.program_code = 'BW'
    JOIN germplasm.germplasm AS ge
        ON ge.crop_id = crop.id
    JOIN germplasm.seed AS sd
        ON sd.germplasm_id = ge.id
        AND sd.program_id = prog.id,
    generate_series(1, 1) AS gs (num)
WHERE
    ge.germplasm_state = 'fixed'
ORDER BY
    RANDOM()
LIMIT
    1000
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.package AS pkg
--rollback USING
--rollback     tenant.program AS prog
--rollback WHERE
--rollback     pkg.program_id = prog.id
--rollback     AND prog.program_code = 'BW'
--rollback     AND pkg.notes = 'DB-977 package'
--rollback ;
