--liquibase formatted sql

--changeset postgres:update_ht_cont_is_computed_to_false context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-968 Update HT_CONT is_computed to false



UPDATE
    master.variable
SET
    is_computed = false
WHERE
    abbrev = 'HT_CONT'
;



--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     is_computed = true
--rollback WHERE
--rollback     abbrev = 'HT_CONT'
--rollback ;