--liquibase formatted sql

--changeset postgres:fix_duplicate_occurrence_numbers context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1021 Fix duplicate occurrence numbers



-- fix duplicate occurrence_number
WITH t_occurrence AS (
    SELECT
        occ.id AS occurrence_id,
        split_part(occ.occurrence_name, '-', 6)::integer AS correct_occurrence_number
    FROM
        experiment.experiment AS expt
        JOIN experiment.occurrence AS occ
            ON occ.experiment_id = expt.id
    WHERE
        expt.experiment_name IN (
            'IRSEA-AYT-2015-DS-001',
            'IRSEA-AYT-2016-DS-005',
            'IRSEA-AYT-2016-WS-006',
            'IRSEA-PYT-2016-WS-002',
            'IRSEA-AYT-2015-WS-007',
            'IRSEA-AYT-2015-DS-002',
            'IRSEA-AYT-2016-DS-001',
            'IRSEA-AYT-2015-WS-004'
        )
    ORDER BY
        expt.experiment_name,
        occ.occurrence_name
)
UPDATE
    experiment.occurrence AS occ
SET
    occurrence_number = t.correct_occurrence_number
FROM
    t_occurrence AS t
WHERE
    t.occurrence_id = occ.id
    AND t.correct_occurrence_number <> occ.occurrence_number
;



-- revert changes
--rollback UPDATE
--rollback     experiment.occurrence AS occ
--rollback SET
--rollback     occurrence_number = t.old_occurrence_number
--rollback FROM (
--rollback         VALUES
--rollback         ('IRSEA-AYT-2015-DS-001-001', 1),
--rollback         ('IRSEA-AYT-2015-DS-001-002', 1),
--rollback         ('IRSEA-AYT-2015-DS-001-003', 1),
--rollback         ('IRSEA-AYT-2015-DS-001-004', 1),
--rollback         ('IRSEA-AYT-2015-DS-002-001', 2),
--rollback         ('IRSEA-AYT-2015-DS-002-002', 2),
--rollback         ('IRSEA-AYT-2015-DS-002-003', 2),
--rollback         ('IRSEA-AYT-2015-DS-002-004', 2),
--rollback         ('IRSEA-AYT-2015-WS-004-001', 3),
--rollback         ('IRSEA-AYT-2015-WS-004-002', 3),
--rollback         ('IRSEA-AYT-2015-WS-004-003', 3),
--rollback         ('IRSEA-AYT-2015-WS-007-001', 2),
--rollback         ('IRSEA-AYT-2015-WS-007-002', 2),
--rollback         ('IRSEA-AYT-2015-WS-007-003', 2),
--rollback         ('IRSEA-AYT-2016-DS-001-001', 11),
--rollback         ('IRSEA-AYT-2016-DS-001-002', 3),
--rollback         ('IRSEA-AYT-2016-DS-001-003', 3),
--rollback         ('IRSEA-AYT-2016-DS-005-001', 2),
--rollback         ('IRSEA-AYT-2016-DS-005-002', 2),
--rollback         ('IRSEA-AYT-2016-WS-006-001', 2),
--rollback         ('IRSEA-AYT-2016-WS-006-002', 2),
--rollback         ('IRSEA-PYT-2016-WS-002-001', 2),
--rollback         ('IRSEA-PYT-2016-WS-002-002', 2)
--rollback     ) AS t (
--rollback         occurrence_name, old_occurrence_number
--rollback     )
--rollback WHERE
--rollback     t.occurrence_name = occ.occurrence_name
--rollback     AND t.old_occurrence_number <> occ.occurrence_number
--rollback ;
