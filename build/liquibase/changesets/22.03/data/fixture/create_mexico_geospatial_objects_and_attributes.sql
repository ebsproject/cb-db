--liquibase formatted sql

--changeset postgres:create_toluca_and_yaqui_fields context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-991 Create Toluca and Yaqui fields



-- create new geospatial objects
INSERT INTO
   place.geospatial_object (
       geospatial_object_name,
       geospatial_object_code,
       geospatial_object_type,
       geospatial_object_subtype,
       parent_geospatial_object_id,
       root_geospatial_object_id,
       creator_id
   )
SELECT
    t.geospatial_object_name,
    t.geospatial_object_code,
    t.geospatial_object_type,
    t.geospatial_object_subtype,
    pgeo.id AS parent_geospatial_object_id,
    pgeo.root_geospatial_object_id,
    pgeo.creator_id
FROM (
        VALUES
        ('Toluca Greenhouse', 'MX_M_MGH', 'field', 'breeding location', 'TOA'),
        ('Toluca Screen House', 'MX_M_MSH', 'field', 'breeding location', 'TOA'),
        ('Yaqui Greenhouse', 'MX_Y_YGH', 'field', 'breeding location', 'CON'),
        ('Yaqui Screen House', 'MX_Y_YSH', 'field', 'breeding location', 'CON')
    ) AS t (
        geospatial_object_name,
        geospatial_object_code,
        geospatial_object_type,
        geospatial_object_subtype,
        parent_geospatial_object_code
    ),
    place.geospatial_object AS pgeo
WHERE
    pgeo.geospatial_object_code = t.parent_geospatial_object_code
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object
--rollback WHERE
--rollback     geospatial_object_code IN (
--rollback         'MX_M_MGH', 'MX_M_MSH', 'MX_Y_YGH', 'MX_Y_YSH'
--rollback     )
--rollback ;



--changeset postgres:add_origin_site_codes_to_toluca_and_yaqui_fields context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-991 Add origin site codes to Toluca and Yaqui fields



-- add origin site codes
INSERT INTO
    place.geospatial_object_attribute (
        geospatial_object_id,
        variable_id,
        data_value,
        creator_id
    )
SELECT
    geo.id AS geospatial_object_id,
    var.id AS variable_id,
    t.origin_site_code AS data_value,
    geo.creator_id
FROM (
        VALUES
        ('MX_M_MGH', 'MGH'),
        ('MX_M_MSH', 'MSH'),
        ('MX_Y_YGH', 'YGH'),
        ('MX_Y_YSH', 'YSH')
    ) AS t (
        geospatial_object_code,
        origin_site_code
    ),
    place.geospatial_object AS geo,
    master.variable AS var
WHERE
    geo.geospatial_object_code = t.geospatial_object_code
    AND var.abbrev = 'ORIGIN_SITE_CODE'
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object_attribute AS geoattr
--rollback USING
--rollback     place.geospatial_object AS geo,
--rollback     master.variable AS var
--rollback WHERE
--rollback     geo.geospatial_object_code IN (
--rollback         'MX_M_MGH', 'MX_M_MSH', 'MX_Y_YGH', 'MX_Y_YSH'
--rollback     )
--rollback     AND var.abbrev = 'ORIGIN_SITE_CODE'
--rollback     AND geoattr.data_value IN ('MGH', 'MSH', 'YGH', 'YSH')
--rollback     AND geoattr.geospatial_object_id = geo.id
--rollback     AND geoattr.is_void = FALSE
--rollback ;
