--liquibase formatted sql

--changeset postgres:update_entry_list_type_of_test_lists context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-942 Update entry_list_type of test lists



-- update entry_list_type
UPDATE
    experiment.entry_list
SET
    entry_list_type = 'entry list'
WHERE
    entry_list_type NOT IN (
        'entry list',
        'cross list',
        'parent list'
    )
;


-- revert changes
--rollback UPDATE
--rollback     experiment.entry_list
--rollback SET
--rollback     entry_list_type = 'test list'
--rollback WHERE
--rollback     entry_list_name = 'BT1-15k-ENTRIES-AYT-2021-DS Entry List'
--rollback ;
