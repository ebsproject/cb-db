--liquibase formatted sql

--changeset postgres:create_test_cross_verification_stage context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-992 HM-DB: Create new Test-cross verification stage



-- create new TCV stage
INSERT INTO tenant.stage (
       stage_code, stage_name, description, creator_id
   )
SELECT
    'TCV' AS stage_code,
    'Test-cross verification' AS stage_name,
    'Test-cross verification' AS description,
    1 AS creator_id
;



-- revert changes
--rollback DELETE FROM tenant.stage WHERE stage_code = 'TCV';
