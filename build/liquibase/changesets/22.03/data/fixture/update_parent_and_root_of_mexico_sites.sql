--liquibase formatted sql

--changeset postgres:update_parent_and_root_of_mexico_sites context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-991 Update parent and root geospatial object of some Mexico sites



-- update parent and root geospatial object references
UPDATE
    place.geospatial_object AS geo
SET
    parent_geospatial_object_id = pgeo.id,
    root_geospatial_object_id = pgeo.id
FROM
    place.geospatial_object AS pgeo
WHERE
    geo.geospatial_object_code IN ('KIO', 'EBN', 'CON')
    AND pgeo.geospatial_object_code = 'MX'
;



-- revert changes
--rollback UPDATE
--rollback     place.geospatial_object AS geo
--rollback SET
--rollback     parent_geospatial_object_id = NULL,
--rollback     root_geospatial_object_id = NULL
--rollback WHERE
--rollback     geo.geospatial_object_code IN ('KIO', 'EBN', 'CON')
--rollback ;
