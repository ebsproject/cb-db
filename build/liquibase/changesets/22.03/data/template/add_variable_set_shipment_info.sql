--liquibase formatted sql

--changeset postgres:add_variable_set_shipment_info context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-823 Add variable set SHIPMENT_INFO



INSERT INTO
	master.variable_set (abbrev,name,creator_id) 
VALUES 
	('SHIPMENT_INFO','Shipment info',1);



--rollback DELETE FROM master.variable_set WHERE abbrev = 'SHIPMENT_INFO';