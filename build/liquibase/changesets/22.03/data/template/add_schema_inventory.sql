--liquibase formatted sql

--changeset postgres:add_schema_inventory context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-823 Add schema INVENTORY



INSERT INTO
    dictionary.schema (database_id,abbrev,name,comment,creator_id)
SELECT 
    t.database_id,
    t.abbrev,
    t.name,
    t.comment,
    1
FROM 
    (
        VALUES
            (
                (SELECT id FROM dictionary.database WHERE abbrev='CB'),
                'INVENTORY',
                'INVENTORY',
                'Inventory',
                'Inventory'
            )
    ) AS t (database_id,schema_id,abbrev,name,comment)
;



--rollback DELETE FROM dictionary.schema WHERE abbrev = 'INVENTORY';