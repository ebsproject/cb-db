--liquibase formatted sql

--changeset postgres:add_new_scale_values_to_hv_meth_disc_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-990 Add new scale values to HV_METH_DISC variable



-- add scale values to HV_METH_DISC variable
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 1) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'HV_METH_DISC'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('A-line Harvest', 'A-line Harvest', 'A-line Harvest', 'A_LINE_HARVEST', 'show'),
        ('H-Harvest', 'H-Harvest', 'H-Harvest', 'H_HARVEST', 'show'),
        ('Test-cross Harvest', 'Test-cross Harvest', 'Test-cross Harvest', 'TEST_CROSS_HARVEST', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'HV_METH_DISC'
;



-- revert changes
-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN (
--rollback         'HV_METH_DISC_A_LINE_HARVEST',
--rollback         'HV_METH_DISC_H_HARVEST',
--rollback         'HV_METH_DISC_TEST_CROSS_HARVEST'
--rollback     )
--rollback ;



--changeset postgres:add_new_scale_values_to_generation_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-990 Add new scale values to GENERATION variable



-- add scale values to GENERATION variable
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 1) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'GENERATION'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('Haploid', 'Haploid', 'Haploid', 'HAPLOID', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'GENERATION'
;



-- revert changes
-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN (
--rollback         'GENERATION_HAPLOID'
--rollback     )
--rollback ;



--changeset postgres:add_new_scale_values_to_germplasm_type_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-990 Add new scale values to GERMPLASM_TYPE variable



-- add scale values to GERMPLASM_TYPE variable
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 1) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'GERMPLASM_TYPE'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('haploid', 'haploid', 'haploid', 'HAPLOID', 'show'),
        ('CMS_line', 'CMS line', 'CMS_line', 'CMS_LINE', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'GERMPLASM_TYPE'
;



-- revert changes
-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN (
--rollback         'GERMPLASM_TYPE_HAPLOID',
--rollback         'GERMPLASM_TYPE_CMS_LINE'
--rollback     )
--rollback ;



--changeset postgres:add_new_scale_values_to_cross_method_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-990 Add new scale values to CROSS_METHOD variable



-- add scale values to CROSS_METHOD variable
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 1) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'CROSS_METHOD'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('CMS multiplication', 'CMS Multiplication', 'CMS multiplication', 'CMS_MULTIPLICATION', 'show'),
        ('test cross', 'Test Cross', 'test cross', 'TEST_CROSS', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'CROSS_METHOD'
;



-- revert changes
-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN (
--rollback         'CROSS_METHOD_CMS_MULTIPLICATION',
--rollback         'CROSS_METHOD_TEST_CROSS'
--rollback     )
--rollback ;


--changeset postgres:update_scale_value_of_cross_method_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-990 Update scale value of CROSS_METHOD variable



-- update scale value for maternal haploid induction of CROSS_METHOD variable
UPDATE
    master.scale_value
SET
    value = 'maternal haploid induction',
    display_name = 'maternal haploid induction',
    description = 'In a maternal haploid induction cross, a male haploid inducer parent is crossed to a female parent to induce a process that enables the female to produce haploid offspring that carry the maternal genome and cytoplasm.',
    abbrev = 'CROSS_METHOD_MATERNAL_HAPLOID_INDUCTION'
WHERE
    abbrev = 'CROSS_METHOD_HAPLOID_INDUCTION'
;



-- revert changes
--rollback UPDATE
--rollback     master.scale_value
--rollback SET
--rollback     value = 'haploid induction',
--rollback     display_name = 'haploid induction',
--rollback     description = 'In a haploid induction cross, a male haploid inducer parent is typically crossed to a female plant to induce a process that enables the female to produce haploid offspring',
--rollback     abbrev = 'CROSS_METHOD_HAPLOID_INDUCTION'
--rollback WHERE
--rollback     abbrev = 'CROSS_METHOD_MATERNAL_HAPLOID_INDUCTION'
--rollback ;
