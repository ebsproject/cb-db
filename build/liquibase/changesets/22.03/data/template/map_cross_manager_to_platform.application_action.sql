--liquibase formatted sql

--changeset postgres:map_cross_manager_to_platform.application_action context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-2813 Map cross manager to platform.application_action



INSERT INTO 
    platform.application_action (application_id, module, creator_id)
SELECT 
	id,
	'crossManager',
	1
FROM 
	platform.application
WHERE 
	abbrev = 'CROSS_MANAGER';



--rollback DELETE FROM platform.application_action WHERE application_id IN (SELECT id FROM platform.application WHERE abbrev = 'CROSS_MANAGER');