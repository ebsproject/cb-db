--liquibase formatted sql

--changeset postgres:update_lists_application_label context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-997 LM: Update tool name header



UPDATE
    platform.application
SET
    label = 'List Manager', 
    action_label = 'List Manager'
WHERE
    abbrev = 'LISTS'



--rollback UPDATE
--rollback    platform.application
--rollback SET
--rollback    label = 'Lists', 
--rollback    action_label = 'Lists'
--rollback WHERE
--rollback    abbrev = 'LISTS'
