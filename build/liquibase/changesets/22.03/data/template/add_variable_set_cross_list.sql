--liquibase formatted sql

--changeset postgres:add_variable_set_cross_list context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-989 Add variable set CROSS_LIST



INSERT INTO
	master.variable_set (abbrev,name,creator_id) 
VALUES 
	('CROSS_LIST','Cross List',1);



--rollback DELETE FROM master.variable_set WHERE abbrev = 'CROSS_LIST';