--liquibase formatted sql

--changeset postgres:insert_cross_manager_to_platform.application context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-988 Insert cross manager to platform.application



INSERT INTO 
    platform.application (abbrev, label, action_label, icon, creator_id)
VALUES
    ('CROSS_MANAGER', 'Cross manager', 'Cross manager', 'repeat', 1);



--rollback DELETE FROM platform.application WHERE abbrev = 'CROSS_MANAGER';
