--liquibase formatted sql

--changeset postgres:add_variable_requestor_country_id context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-823 Add variable REQUESTOR_COUNTRY_ID



DO $$
DECLARE
	var_property_id int;
	var_method_id int;
	var_scale_id int;
	var_variable_id int;
	var_variable_set_id int;
	var_variable_set_member_order_number int;
	var_count_property_id int;
	var_count_method_id int;
	var_count_scale_id int;
	var_count_variable_set_id int;
	var_count_variable_set_member_id int;
BEGIN

	--PROPERTY

	SELECT count(id) FROM master.property WHERE ABBREV = 'REQUESTOR_COUNTRY_ID' INTO var_count_property_id;
	IF var_count_property_id > 0 THEN
		SELECT id FROM master.property WHERE ABBREV = 'REQUESTOR_COUNTRY_ID' INTO var_property_id;
	ELSE
		INSERT INTO
			master.property (abbrev,name,display_name) 
		VALUES 
			('REQUESTOR_COUNTRY_ID','REQUESTOR_COUNTRY_ID','REQUESTOR_COUNTRY_ID') 
		RETURNING id INTO var_property_id;
	END IF;

	--METHOD

	SELECT count(id) FROM master.method WHERE ABBREV = 'REQUESTOR_COUNTRY_ID' INTO var_count_method_id;
	IF var_count_method_id > 0 THEN
		SELECT id FROM master.method WHERE ABBREV = 'REQUESTOR_COUNTRY_ID' INTO var_method_id;
	ELSE
		INSERT INTO
			master.method (abbrev,name,description,formula) 
		VALUES 
			('REQUESTOR_COUNTRY_ID','Requestor Country ID',NULL,NULL) 
		RETURNING id INTO var_method_id;
	END IF;

	--SCALE

	SELECT count(id) FROM master.scale WHERE ABBREV = 'REQUESTOR_COUNTRY_ID' INTO var_count_scale_id;
	IF var_count_scale_id > 0 THEN
		SELECT id FROM master.scale WHERE ABBREV = 'REQUESTOR_COUNTRY_ID' INTO var_scale_id;
	ELSE
		INSERT INTO
			master.scale (abbrev,name,unit,type,level) 
		VALUES 
			('REQUESTOR_COUNTRY_ID','Requestor Country ID',NULL,'discrete','nominal') 
		RETURNING id INTO var_scale_id;
	END IF;

	--VARIABLE

		INSERT INTO
			master.variable (abbrev,name,description,label,data_type,not_null,type,usage,status,data_level,display_name) 
		VALUES 
			('REQUESTOR_COUNTRY_ID','Requestor Country ID','Reference to the country where the shipment was requested from (linked to CRM)','REQUESTOR COUNTRY ID','integer','False','identification','shipment','active','shipment','Requestor Country ID') 
		RETURNING id INTO var_variable_id;

	--UPDATE PROPERTY_ID, METHOD_ID, SCALE_ID

	UPDATE master.variable SET property_id=var_property_id, method_id=var_method_id, scale_id=var_scale_id WHERE id=var_variable_id;

	--VARIABLE_SET_ID

	SELECT count(id) FROM master.variable_set WHERE ABBREV = 'SHIPMENT_INFO' INTO var_count_variable_set_id;
	IF var_count_variable_set_id > 0 THEN
		SELECT id FROM master.variable_set WHERE ABBREV = 'SHIPMENT_INFO' INTO var_variable_set_id;
	ELSE
		INSERT INTO
			master.variable_set (abbrev,name) 
		VALUES 
			('SHIPMENT_INFO','Shipment info') 
		RETURNING id INTO var_variable_set_id;
	END IF;

	--GET THE LAST ORDER NUMBER

	SELECT count(id) FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id INTO var_count_variable_set_member_id;
	IF var_count_variable_set_member_id > 0 THEN
		SELECT max(order_number)+1 FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id GROUP BY variable_set_id INTO var_variable_set_member_order_number;
	ELSE
		var_variable_set_member_order_number = 1;
	END IF;

	--ADD VARIABLE SET MEMBER

	INSERT INTO master.variable_set_member (variable_set_id, variable_id, order_number) VALUES (var_variable_set_id, var_variable_id,var_variable_set_member_order_number  );


END;
$$



--rollback DELETE FROM master.scale WHERE id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'REQUESTOR_COUNTRY_ID');

--rollback DELETE FROM master."property" WHERE id IN (SELECT property_id FROM master.variable WHERE abbrev = 'REQUESTOR_COUNTRY_ID');

--rollback DELETE FROM master.method WHERE id IN (SELECT method_id FROM master.variable WHERE abbrev = 'REQUESTOR_COUNTRY_ID');

--rollback DELETE FROM master.variable_set_member WHERE variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'REQUESTOR_COUNTRY_ID');

--rollback DELETE FROM master.variable_set WHERE abbrev = 'None';

--rollback DELETE FROM master.variable WHERE abbrev = 'REQUESTOR_COUNTRY_ID';
