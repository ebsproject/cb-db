--liquibase formatted sql

--changeset postgres:update_hm_rice_name_config_germplasm_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1048 HM: Add germplasm name pattern for backcross single seed numbering



--update germplasm name config
UPDATE platform.config
SET
	config_value = '{
        "PLOT": {
            "default": {
                "default": {
                    "default": [
                        {
                            "type": "free-text",
                            "value": "IR XXXXX",
                            "order_number": 0
                        }
                    ]
                },
                "progeny": {
                    "bulk": [
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "germplasmDesignation",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "free-text",
                            "value": "B",
                            "order_number": 2
                        }
                    ],
                    "default": [
                        {
                            "type": "free-text",
                            "value": "IR XXXXX",
                            "order_number": 0
                        }
                    ],
                    "plant-specific": [
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "germplasmDesignation",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 2
                        }
                    ],
                    "panicle selection": [
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "germplasmDesignation",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 2
                        }
                    ],
                    "single seed descent": [
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "germplasmDesignation",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "free-text",
                            "value": "B RGA",
                            "order_number": 2
                        }
                    ],
                    "single plant selection": [
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "germplasmDesignation",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 2
                        }
                    ]
                }
            }
        },
        "CROSS": {
            "default": {
                "default": {
                    "default": [
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "crossName",
                            "order_number": 0
                        }
                    ],
                    "single seed numbering": [
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "crossName",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": ":",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 2
                        }
                    ]
                }
            },
            "single cross": {
                "default": {
                    "bulk": [
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "crossName",
                            "order_number": 0
                        }
                    ],
                    "default": [
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "crossName",
                            "order_number": 0
                        }
                    ],
                    "single seed numbering": [
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "crossName",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": ":",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 2
                        }
                    ]
                }
            }
        },
        "harvest_mode": {
            "cross_method": {
                "germplasm_state/germplasm_type": {
                    "harvest_method": [
                        {
                            "type": "free-text",
                            "value": "ABC",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "<entity>",
                            "field_name": "<field_name>",
                            "order_number": 1
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 3
                        },
                        {
                            "type": "db-sequence",
                            "schema": "<schema>",
                            "order_number": 4,
                            "sequence_name": "<sequence_name>"
                        }
                    ]
                }
            }
        }
    }'
WHERE
	abbrev = 'HM_NAME_PATTERN_GERMPLASM_RICE_DEFAULT'
;



--rollback UPDATE platform.config
--rollback SET
--rollback 	config_value = '{
--rollback         "PLOT": {
--rollback             "default": {
--rollback                 "default": {
--rollback                     "default": [
--rollback                         {
--rollback                             "type": "free-text",
--rollback                             "value": "IR XXXXX",
--rollback                             "order_number": 0
--rollback                         }
--rollback                     ]
--rollback                 },
--rollback                 "progeny": {
--rollback                     "bulk": [
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "plot",
--rollback                             "field_name": "germplasmDesignation",
--rollback                             "order_number": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "order_number": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "free-text",
--rollback                             "value": "B",
--rollback                             "order_number": 2
--rollback                         }
--rollback                     ],
--rollback                     "default": [
--rollback                         {
--rollback                             "type": "free-text",
--rollback                             "value": "IR XXXXX",
--rollback                             "order_number": 0
--rollback                         }
--rollback                     ],
--rollback                     "plant-specific": [
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "plot",
--rollback                             "field_name": "germplasmDesignation",
--rollback                             "order_number": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "order_number": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "counter",
--rollback                             "order_number": 2
--rollback                         }
--rollback                     ],
--rollback                     "panicle selection": [
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "plot",
--rollback                             "field_name": "germplasmDesignation",
--rollback                             "order_number": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "order_number": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "counter",
--rollback                             "order_number": 2
--rollback                         }
--rollback                     ],
--rollback                     "single seed descent": [
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "plot",
--rollback                             "field_name": "germplasmDesignation",
--rollback                             "order_number": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "order_number": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "free-text",
--rollback                             "value": "B RGA",
--rollback                             "order_number": 2
--rollback                         }
--rollback                     ],
--rollback                     "single plant selection": [
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "plot",
--rollback                             "field_name": "germplasmDesignation",
--rollback                             "order_number": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "order_number": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "counter",
--rollback                             "order_number": 2
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             }
--rollback         },
--rollback         "CROSS": {
--rollback             "default": {
--rollback                 "default": {
--rollback                     "default": [
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "cross",
--rollback                             "field_name": "crossName",
--rollback                             "order_number": 0
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             "single cross": {
--rollback                 "default": {
--rollback                     "bulk": [
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "cross",
--rollback                             "field_name": "crossName",
--rollback                             "order_number": 0
--rollback                         }
--rollback                     ],
--rollback                     "default": [
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "cross",
--rollback                             "field_name": "crossName",
--rollback                             "order_number": 0
--rollback                         }
--rollback                     ],
--rollback                     "single seed numbering": [
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "cross",
--rollback                             "field_name": "crossName",
--rollback                             "order_number": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": ":",
--rollback                             "order_number": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "counter",
--rollback                             "order_number": 2
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             }
--rollback         },
--rollback         "harvest_mode": {
--rollback             "cross_method": {
--rollback                 "germplasm_state/germplasm_type": {
--rollback                     "harvest_method": [
--rollback                         {
--rollback                             "type": "free-text",
--rollback                             "value": "ABC",
--rollback                             "order_number": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "<entity>",
--rollback                             "field_name": "<field_name>",
--rollback                             "order_number": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "order_number": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "counter",
--rollback                             "order_number": 3
--rollback                         },
--rollback                         {
--rollback                             "type": "db-sequence",
--rollback                             "schema": "<schema>",
--rollback                             "order_number": 4,
--rollback                             "sequence_name": "<sequence_name>"
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             }
--rollback         }
--rollback     }'
--rollback WHERE
--rollback 	abbrev = 'HM_NAME_PATTERN_GERMPLASM_RICE_DEFAULT'
--rollback ;