--liquibase formatted sql

--changeset postgres:add_cm_and_domains_in_admin_space context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-988 Add CM and Domains in left menu config of admin space



UPDATE 
    platform.space 
SET 
    menu_data = 
    '
        {
		"left_menu_items": [{
			"name": "experiment-creation",
			"label": "Experiment creation",
			"appAbbrev": "EXPERIMENT_CREATION"
		}, {
			"name": "experiment-manager",
			"label": "Experiment manager",
			"appAbbrev": "OCCURRENCES"
		}, {
			"name": "cross-manger",
			"label": "Cross manager",
			"appAbbrev": "CROSS_MANAGER"
		}, {
			"name": "planting-instructions-manager",
			"label": "Planting instructions manager",
			"appAbbrev": "PLANTING_INSTRUCTIONS_MANAGER"
		}, {
			"name": "data-collection-qc-quality-control",
			"label": "Data collection",
			"appAbbrev": "QUALITY_CONTROL"
		}, {
			"name": "seeds-harvest-manager",
			"label": "Harvest manager",
			"appAbbrev": "HARVEST_MANAGER"
		}, {
			"name": "searchs-germplasm",
			"label": "Germplasm",
			"appAbbrev": "GERMPLASM_CATALOG"
		}, {
			"name": "search-seeds",
			"label": "Seed search",
			"appAbbrev": "FIND_SEEDS"
		}, {
			"name": "search-traits",
			"label": "Traits",
			"appAbbrev": "TRAITS"
		}, {
			"name": "list-manager",
			"label": "List manager",
			"appAbbrev": "LISTS"
		}, {
			"name": "shortcut-domains",
			"items": [{
					"name": "shortcut-domains-core-breeding",
					"label": "Core breeding",
					"envVariable": "CB_URL"
				},
				{
					"name": "shortcut-domains-service-management",
					"label": "Service Management",
					"envVariable": "CS_URL",
					"isNewTab": true
				},
				{
					"name": "shortcut-domains-core-system",
					"label": "Core system",
					"envVariable": "CS_URL",
					"isNewTab": true
				},
				{
					"name": "shortcut-domains-analytics",
					"label": "Analytics",
					"envVariable": "CS_URL",
					"isNewTab": true
				}
			],
			"label": "Shortcut domains"
		}],
		"main_menu_items": [{
			"icon": "help_outline",
			"name": "help",
			"items": [{
				"url": "https://ebsproject.atlassian.net/servicedesk/customer/portal/2",
				"icon": "headset_mic",
				"name": "help_support-portal",
				"label": "Support portal",
				"tooltip": "Go to EBS Service Desk portal"
			}, {
				"url": "https://cbapi-dev.ebsproject.org",
				"icon": "code",
				"name": "help_api",
				"label": "API",
				"tooltip": "Go to EBS CB-API Documentation"
			}],
			"label": "Help"
		}, {
			"icon": "folder_special",
			"name": "data-management",
			"items": [{
				"name": "data-management_seasons",
				"label": "Seasons",
				"appAbbrev": "MANAGE_SEASONS"
			}],
			"label": "Data management"
		}, {
			"icon": "settings",
			"name": "administration",
			"items": [{
				"name": "administration_users",
				"label": "Persons",
				"appAbbrev": "PERSONS"
			}],
			"label": "Administration"
		}]
	}
    '
WHERE abbrev = 'ADMIN';



--rollback UPDATE 
--rollback     platform.space
--rollback SET 
--rollback     menu_data = 
--rollback     '
--rollback         {
--rollback             "left_menu_items": [{
--rollback                 "label": "Experiment creation",
--rollback                 "appAbbrev": "EXPERIMENT_CREATION"
--rollback             }, {
--rollback                 "name": "experiment-manager",
--rollback                 "label": "Experiment manager",
--rollback                 "appAbbrev": "OCCURRENCES"
--rollback             }, {
--rollback                 "name": "planting-instructions-manager",
--rollback                 "label": "Planting instructions manager",
--rollback                 "appAbbrev": "PLANTING_INSTRUCTIONS_MANAGER"
--rollback             }, {
--rollback                 "name": "data-collection-qc-quality-control",
--rollback                 "label": "Data collection",
--rollback                 "appAbbrev": "QUALITY_CONTROL"
--rollback             }, {
--rollback                 "name": "seeds-harvest-manager",
--rollback                 "label": "Harvest manager",
--rollback                 "appAbbrev": "HARVEST_MANAGER"
--rollback             }, {
--rollback                 "name": "searchs-germplasm",
--rollback                 "label": "Germplasm",
--rollback                 "appAbbrev": "GERMPLASM_CATALOG"
--rollback             }, {
--rollback                 "name": "search-seeds",
--rollback                 "label": "Seed search",
--rollback                 "appAbbrev": "FIND_SEEDS"
--rollback             }, {
--rollback                 "name": "search-traits",
--rollback                 "label": "Traits",
--rollback                 "appAbbrev": "TRAITS"
--rollback             }, {
--rollback                 "name": "list-manager",
--rollback                 "label": "List manager",
--rollback                 "appAbbrev": "LISTS"
--rollback            }],
--rollback             "main_menu_items": [{
--rollback                 "icon": "help_outline",
--rollback                 "name": "help",
--rollback                 "items": [{
--rollback                     "url": "https://ebsproject.atlassian.net/servicedesk/customer/portals",
--rollback                     "icon": "headset_mic",
--rollback                     "name": "help_support-portal",
--rollback                     "label": "Support portal",
--rollback                     "tooltip": "Go to EBS Service Desk portal"
--rollback                 }, {
--rollback                     "url": "https://cbapi-dev.ebsproject.org",
--rollback                     "icon": "code",
--rollback                     "name": "help_api",
--rollback                     "label": "API",
--rollback                     "tooltip": "Go to B4R API Documentation"
--rollback                 }],
--rollback                 "label": "Help"
--rollback             }, {
--rollback                 "icon": "folder_special",
--rollback                 "name": "data-management",
--rollback                 "items": [{
--rollback                     "name": "data-management_seasons",
--rollback                     "label": "Seasons",
--rollback                     "appAbbrev": "MANAGE_SEASONS"
--rollback                 }],
--rollback                 "label": "Data management"
--rollback             }, {
--rollback                 "icon": "settings",
--rollback                 "name": "administration",
--rollback                 "items": [{
--rollback                     "name": "administration_users",
--rollback                     "label": "Persons",
--rollback                     "appAbbrev": "PERSONS"
--rollback                 }],
--rollback                 "label": "Administration"
--rollback             }]
--rollback         }
--rollback     '
--rollback WHERE
--rollback     abbrev = 'ADMIN';