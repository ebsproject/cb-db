--liquibase formatted sql

--changeset postgres:add_variable_shipment_tracking_status context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-823 Add variable SHIPMENT_TRACKING_STATUS



DO $$
DECLARE
	var_property_id int;
	var_method_id int;
	var_scale_id int;
	var_variable_id int;
	var_variable_set_id int;
	var_variable_set_member_order_number int;
	var_count_property_id int;
	var_count_method_id int;
	var_count_scale_id int;
	var_count_variable_set_id int;
	var_count_variable_set_member_id int;
BEGIN

	--PROPERTY

	SELECT count(id) FROM master.property WHERE ABBREV = 'SHIPMENT_TRACKING_STATUS' INTO var_count_property_id;
	IF var_count_property_id > 0 THEN
		SELECT id FROM master.property WHERE ABBREV = 'SHIPMENT_TRACKING_STATUS' INTO var_property_id;
	ELSE
		INSERT INTO
			master.property (abbrev,name,display_name) 
		VALUES 
			('SHIPMENT_TRACKING_STATUS','SHIPMENT_TRACKING_STATUS','SHIPMENT_TRACKING_STATUS') 
		RETURNING id INTO var_property_id;
	END IF;

	--METHOD

	SELECT count(id) FROM master.method WHERE ABBREV = 'SHIPMENT_TRACKING_STATUS' INTO var_count_method_id;
	IF var_count_method_id > 0 THEN
		SELECT id FROM master.method WHERE ABBREV = 'SHIPMENT_TRACKING_STATUS' INTO var_method_id;
	ELSE
		INSERT INTO
			master.method (abbrev,name,description,formula) 
		VALUES 
			('SHIPMENT_TRACKING_STATUS','Shipment Tracking Status',NULL,NULL) 
		RETURNING id INTO var_method_id;
	END IF;

	--SCALE

	SELECT count(id) FROM master.scale WHERE ABBREV = 'SHIPMENT_TRACKING_STATUS' INTO var_count_scale_id;
	IF var_count_scale_id > 0 THEN
		SELECT id FROM master.scale WHERE ABBREV = 'SHIPMENT_TRACKING_STATUS' INTO var_scale_id;
	ELSE
		INSERT INTO
			master.scale (abbrev,name,unit,type,level) 
		VALUES 
			('SHIPMENT_TRACKING_STATUS','Shipment Tracking Status',NULL,'categorical','nominal') 
		RETURNING id INTO var_scale_id;
	END IF;

	--SCALE VALUE

	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev, scale_value_status) 
	VALUES 
		(var_scale_id,'submitted',1,'submitted','submitted', 'SHIPMENT_TRACKING_STATUS_SUBMITTED', 'show');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev, scale_value_status) 
	VALUES 
		(var_scale_id,'processing',2,'processing','processing', 'SHIPMENT_TRACKING_STATUS_PROCESSING', 'show');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev, scale_value_status) 
	VALUES 
		(var_scale_id,'on hold',3,'on hold','on hold', 'SHIPMENT_TRACKING_STATUS_ON_HOLD', 'show');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev, scale_value_status) 
	VALUES 
		(var_scale_id,'ready',4,'ready','ready', 'SHIPMENT_TRACKING_STATUS_READY', 'show');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev, scale_value_status) 
	VALUES 
		(var_scale_id,'sent',5,'sent','sent', 'SHIPMENT_TRACKING_STATUS_SENT', 'show');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev, scale_value_status) 
	VALUES 
		(var_scale_id,'received',6,'received','received', 'SHIPMENT_TRACKING_STATUS_RECEIVED', 'show');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev, scale_value_status) 
	VALUES 
		(var_scale_id,'cancelled',7,'cancelled','cancelled', 'SHIPMENT_TRACKING_STATUS_CANCELLED', 'show');

	UPDATE master.scale SET min_value='submitted', max_value='cancelled' WHERE id=var_scale_id;

	--VARIABLE

		INSERT INTO
			master.variable (abbrev,name,description,label,data_type,not_null,type,usage,status,data_level,display_name) 
		VALUES 
			('SHIPMENT_TRACKING_STATUS','Shipment Tracking Status','Low-level status related to service requirements associated with a shipment {submitted, processing, on hold, ready, sent, received, cancelled}','SHIPMENT TRACKING STATUS','character varying','False','metadata','shipment','active','shipment','Shipment Tracking Status') 
		RETURNING id INTO var_variable_id;

	--UPDATE PROPERTY_ID, METHOD_ID, SCALE_ID

	UPDATE master.variable SET property_id=var_property_id, method_id=var_method_id, scale_id=var_scale_id WHERE id=var_variable_id;

	--VARIABLE_SET_ID

	SELECT count(id) FROM master.variable_set WHERE ABBREV = 'SHIPMENT_INFO' INTO var_count_variable_set_id;
	IF var_count_variable_set_id > 0 THEN
		SELECT id FROM master.variable_set WHERE ABBREV = 'SHIPMENT_INFO' INTO var_variable_set_id;
	ELSE
		INSERT INTO
			master.variable_set (abbrev,name) 
		VALUES 
			('SHIPMENT_INFO','Shipment info') 
		RETURNING id INTO var_variable_set_id;
	END IF;

	--GET THE LAST ORDER NUMBER

	SELECT count(id) FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id INTO var_count_variable_set_member_id;
	IF var_count_variable_set_member_id > 0 THEN
		SELECT max(order_number)+1 FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id GROUP BY variable_set_id INTO var_variable_set_member_order_number;
	ELSE
		var_variable_set_member_order_number = 1;
	END IF;

	--ADD VARIABLE SET MEMBER

	INSERT INTO master.variable_set_member (variable_set_id, variable_id, order_number) VALUES (var_variable_set_id, var_variable_id,var_variable_set_member_order_number  );


END;
$$



--rollback DELETE FROM master.scale_value WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'SHIPMENT_TRACKING_STATUS');

--rollback DELETE FROM master.scale WHERE id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'SHIPMENT_TRACKING_STATUS');

--rollback DELETE FROM master."property" WHERE id IN (SELECT property_id FROM master.variable WHERE abbrev = 'SHIPMENT_TRACKING_STATUS');

--rollback DELETE FROM master.method WHERE id IN (SELECT method_id FROM master.variable WHERE abbrev = 'SHIPMENT_TRACKING_STATUS');

--rollback DELETE FROM master.variable_set_member WHERE variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'SHIPMENT_TRACKING_STATUS');

--rollback DELETE FROM master.variable WHERE abbrev = 'SHIPMENT_TRACKING_STATUS';
