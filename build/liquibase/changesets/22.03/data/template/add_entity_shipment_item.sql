--liquibase formatted sql

--changeset postgres:add_entity_shipment_item context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-823 Add entity SHIPMENT_ITEM



INSERT INTO
    dictionary.table (database_id,schema_id,abbrev,name,comment,creator_id)
SELECT 
    t.database_id,
    t.schema_id,
    t.abbrev,
    t.name,
    t.comment,
    1
FROM 
    (
        VALUES
            (
                (SELECT id FROM dictionary.database WHERE abbrev='CB'),
                (SELECT id FROM dictionary.schema WHERE abbrev='INVENTORY'),
                'SHIPMENT_ITEM',
                'List of items included in a shipment transaction',
                'Shipment Item'
            )
    ) AS t (database_id,schema_id,abbrev,name,comment)
;


INSERT INTO
    dictionary.entity (abbrev,name,description,table_id,creator_id)
SELECT 
    t.abbrev,
    t.name,
    t.description,
    t.table_id,
    1
FROM 
    (
        VALUES
            (
                'SHIPMENT_ITEM',
                'Shipment Item',
                'List of items included in a shipment transaction',
                (SELECT id FROM dictionary.table WHERE abbrev='SHIPMENT_ITEM')
            )
    ) AS t (abbrev,name,description,table_id)
;



--rollback DELETE FROM dictionary.entity WHERE abbrev = 'SHIPMENT_ITEM';
--rollback DELETE FROM dictionary.table WHERE abbrev = 'SHIPMENT_ITEM';