--liquibase formatted sql

--changeset postgres:add_entity_shipment_file context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-823 Add entity SHIPMENT_FILE



INSERT INTO
    dictionary.table (database_id,schema_id,abbrev,name,comment,creator_id)
SELECT 
    t.database_id,
    t.schema_id,
    t.abbrev,
    t.name,
    t.comment,
    1
FROM 
    (
        VALUES
            (
                (SELECT id FROM dictionary.database WHERE abbrev='CB'),
                (SELECT id FROM dictionary.schema WHERE abbrev='INVENTORY'),
                'SHIPMENT_FILE',
                'Support documents and files attached to shipments ',
                'Shipment File'
            )
    ) AS t (database_id,schema_id,abbrev,name,comment)
;


INSERT INTO
    dictionary.entity (abbrev,name,description,table_id,creator_id)
SELECT 
    t.abbrev,
    t.name,
    t.description,
    t.table_id,
    1
FROM 
    (
        VALUES
            (
                'SHIPMENT_FILE',
                'Shipment File',
                'Support documents and files attached to shipments ',
                (SELECT id FROM dictionary.table WHERE abbrev='SHIPMENT_FILE')
            )
    ) AS t (abbrev,name,description,table_id)
;



--rollback DELETE FROM dictionary.entity WHERE abbrev = 'SHIPMENT_FILE';
--rollback DELETE FROM dictionary.table WHERE abbrev = 'SHIPMENT_FILE';