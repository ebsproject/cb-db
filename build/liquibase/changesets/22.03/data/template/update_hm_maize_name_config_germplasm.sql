--liquibase formatted sql

--changeset postgres:update_hm_maize_name_config_germplasm context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1043 HM: Add germplasm name config for maternal haploid induction



--update germplasm name config
UPDATE platform.config
SET
	config_value = '{
        "PLOT": {
            "default": {
                "fixed": {
                    "bulk": [
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "germplasmDesignation",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "free-text",
                            "value": "B",
                            "order_number": 2
                        }
                    ],
                    "default": [
                        {
                            "type": "free-text",
                            "value": "",
                            "order_number": 0
                        }
                    ]
                },
                "not_fixed": {
                    "bulk": [
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "germplasmDesignation",
                            "order_number": 1
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 2
                        },
                        {
                            "type": "free-text-repeater",
                            "value": "B",
                            "minimum": "2",
                            "delimiter": "*",
                            "order_number": 3
                        }
                    ],
                    "default": [
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "germplasmDesignation",
                            "order_number": 0
                        }
                    ],
                    "individual ear": [
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "germplasmDesignation",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 2
                        }
                    ]
                }
            }
        },
        "CROSS": {
            "default": {
                "default": {
                    "default": [
                        {
                            "type": "free-text",
                            "value": "",
                            "order_number": 0
                        }
                    ]
                }
            },
            "maternal haploid induction": {
                "default": {
                    "bulk": [
                        {
                            
                            "type": "field",
                            "entity": "femaleParentGermplasm",
                            "field_name": "designation",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "@",
                            "order_number": 1
                        },
                        {
                            "type": "free-text",
                            "value": "0",
                            "order_number": 2
                        }
                    ]
                }
            }
        },
        "harvest_mode": {
            "cross_method": {
                "germplasm_state/germplasm_type": {
                    "harvest_method": [
                        {
                            "type": "free-text",
                            "value": "ABC",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "<entity>",
                            "field_name": "<field_name>",
                            "order_number": 1
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 3
                        },
                        {
                            "type": "db-sequence",
                            "schema": "<schema>",
                            "order_number": 4,
                            "sequence_name": "<sequence_name>"
                        },
                        {
                            "type": "free-text-repeater",
                            "value": "ABC",
                            "minimum": "2",
                            "delimiter": "*",
                            "order_number": 5
                        }
                    ]
                }
            }
        }
    }'
WHERE
	abbrev = 'HM_NAME_PATTERN_GERMPLASM_MAIZE_DEFAULT'
;



--rollback UPDATE platform.config
--rollback SET
--rollback 	config_value = '{
--rollback         "PLOT": {
--rollback             "default": {
--rollback                 "fixed": {
--rollback                     "bulk": [
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "plot",
--rollback                             "field_name": "germplasmDesignation",
--rollback                             "order_number": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "order_number": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "free-text",
--rollback                             "value": "B",
--rollback                             "order_number": 2
--rollback                         }
--rollback                     ],
--rollback                     "default": [
--rollback                         {
--rollback                             "type": "free-text",
--rollback                             "value": "",
--rollback                             "order_number": 0
--rollback                         }
--rollback                     ]
--rollback                 },
--rollback                 "not_fixed": {
--rollback                     "bulk": [
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "plot",
--rollback                             "field_name": "germplasmDesignation",
--rollback                             "order_number": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "order_number": 2
--rollback                         },
--rollback                         {
--rollback                             "type": "free-text-repeater",
--rollback                             "value": "B",
--rollback                             "minimum": "2",
--rollback                             "delimiter": "*",
--rollback                             "order_number": 3
--rollback                         }
--rollback                     ],
--rollback                     "default": [
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "plot",
--rollback                             "field_name": "germplasmDesignation",
--rollback                             "order_number": 0
--rollback                         }
--rollback                     ],
--rollback                     "individual ear": [
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "plot",
--rollback                             "field_name": "germplasmDesignation",
--rollback                             "order_number": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "order_number": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "counter",
--rollback                             "order_number": 2
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             }
--rollback         },
--rollback         "CROSS": {
--rollback             "default": {
--rollback                 "default": {
--rollback                     "default": [
--rollback                         {
--rollback                             "type": "free-text",
--rollback                             "value": "",
--rollback                             "order_number": 0
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             }
--rollback         },
--rollback         "harvest_mode": {
--rollback             "cross_method": {
--rollback                 "germplasm_state/germplasm_type": {
--rollback                     "harvest_method": [
--rollback                         {
--rollback                             "type": "free-text",
--rollback                             "value": "ABC",
--rollback                             "order_number": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "<entity>",
--rollback                             "field_name": "<field_name>",
--rollback                             "order_number": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "order_number": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "counter",
--rollback                             "order_number": 3
--rollback                         },
--rollback                         {
--rollback                             "type": "db-sequence",
--rollback                             "schema": "<schema>",
--rollback                             "order_number": 4,
--rollback                             "sequence_name": "<sequence_name>"
--rollback                         },
--rollback                         {
--rollback                             "type": "free-text-repeater",
--rollback                             "value": "ABC",
--rollback                             "minimum": "2",
--rollback                             "delimiter": "*",
--rollback                             "order_number": 5
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             }
--rollback         }
--rollback     }'
--rollback WHERE
--rollback 	abbrev = 'HM_NAME_PATTERN_GERMPLASM_MAIZE_DEFAULT'
--rollback ;