--liquibase formatted sql

--changeset postgres:add_cm_basic_info_config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1041	CM-DB: Add changeset for CM_CROSS_LIST_BASIC_INFO config in platform.config



INSERT INTO
    platform.config(
		abbrev,
		name,
		config_value,
		rank,
		usage,
		creator_id,
		notes
	)
VALUES (
	'CM_CROSS_LIST_BASIC_INFO',
	'Cross Manager Basic Info tab configuration', 
	$${
		"Values": [
			{
				"field_label": "Cross List Name",
				"order_number": 1,
				"variable_abbrev": "CROSS_LIST_NAME",
				"required": "required",
				"disabled": false,
				"field_description": "Name of the cross list"
			},
			{
				"field_label": "Experiment Year",
				"order_number": 2,
				"variable_abbrev": "EXPERIMENT_YEAR",
				"required": "required",
				"disabled": false,
				"field_description": "Experiment Year"
			},
			{
				"field_label": "Description",
				"order_number": 3,
				"variable_abbrev": "DESCRIPTION",
				"disabled": false,
				"field_description": "Additional cross list details"
			},
			{
				"field_label": "Site",
				"order_number": 4,
				"variable_abbrev": "SITE",
				"required": "required",
				"disabled": false,
				"field_description": "Site where parents have been planted"
			},
			{
				"field_label": "Season",
				"order_number": 5,
				"variable_abbrev": "SEASON",
				"required": "required",
				"disabled": false,
				"field_description": "Season"
			}
		],
  		"Name": "Cross Manager Basic Info tab default fields"
  	}$$,
	1,
	'cross manager tool',
	1,
	'added by m.pasang'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'CM_CROSS_LIST_BASIC_INFO';
