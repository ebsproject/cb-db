--liquibase formatted sql

--changeset postgres:fix_api_message context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-965 Fix API message



UPDATE
    api.messages
SET
    message = 'You have provided invalid values for the sort field.'
WHERE
    code = 400057
;



--rollback UPDATE
--rollback     api.messages
--rollback SET
--rollback     message = 'You have provided invalues for the sort field.'
--rollback WHERE
--rollback     code = 400057
--rollback ;