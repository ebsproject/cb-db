--liquibase formatted sql

--changeset postgres:add_list_bg_process_threshold_config_to_platform_config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-994 LM-DB: Add new threshold values to background process threshold configuration



-- add list bg processthresholds config
INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'LISTS_BG_PROCESSING_THRESHOLD',
        'List Manager Background Processing Threshold',
        $$	
            {
                "mergeLists": {
                    "size": "500",
                    "description": "Merging of lists."
                },
                "splitList": {
                    "size": "500",
                    "description": "Splitting of list."
                },
                "reorderAllBySort": {
                    "size": "200",
                    "description": "Update order number of list members."
                }
            }
        $$,
        1,
        'list_manager',
        1,
        'DB-994 LM-DB: Add new threshold values to background process threshold configuration'
    );



--rollback DELETE FROM platform.config WHERE abbrev='LISTS_BG_PROCESSING_THRESHOLD';
