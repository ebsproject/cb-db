--liquibase formatted sql

--changeset postgres:update_cross_list_name_variable_to_entry_list_name context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-989 Update CROSS_LIST_NAME variable to ENTRY_LIST_NAME



-- update variable
UPDATE
    master.variable
SET
    abbrev = 'ENTRY_LIST_NAME',
    label = 'ENTRY LIST NAME',
    name = 'Entry List Name',
    display_name = 'Entry List Name',
    description = 'Name given to the entry list',
    not_null = true
WHERE
    abbrev = 'CROSS_LIST_NAME'
;



-- revert changes
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     abbrev = 'CROSS_LIST_NAME',
--rollback     label = 'CROSS LIST NAME',
--rollback     name = 'Cross List Name',
--rollback     display_name = 'Cross List Name',
--rollback     description = 'Cross List Name',
--rollback     not_null = false
--rollback WHERE
--rollback     abbrev = 'ENTRY_LIST_NAME'
--rollback ;



--changeset postgres:update_cross_list_name_method_to_entry_list_name context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-989 Update CROSS_LIST_NAME method to ENTRY_LIST_NAME
--validCheckSum: 8:6d618f3122d5cc406e60afc0eb86a64c



-- transferred from another changeset
-- update "property"
UPDATE
    master.property
SET
    abbrev = 'ENTRY_LIST_NAME',
    name = 'Entry List Name',
    display_name = 'Entry List Name'
WHERE
    abbrev = 'CROSS_LIST_NAME'
;

-- update method
UPDATE
    master.method
SET
    abbrev = 'ENTRY_LIST_NAME',
    name = 'Entry List Name'
WHERE
    abbrev = 'CROSS_LIST_NAME'
;



-- revert changes
--rollback UPDATE
--rollback     master.method
--rollback SET
--rollback     abbrev = 'CROSS_LIST_NAME',
--rollback     name = 'Cross List Name'
--rollback WHERE
--rollback     abbrev = 'ENTRY_LIST_NAME'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     master."property"
--rollback SET
--rollback     abbrev = 'CROSS_LIST_NAME',
--rollback     name = 'CROSS_LIST_NAME',
--rollback     display_name = 'CROSS_LIST_NAME'
--rollback WHERE
--rollback     abbrev = 'ENTRY_LIST_NAME'
--rollback ;



--changeset postgres:update_cross_list_name_scale_to_entry_list_name context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-989 Update CROSS_LIST_NAME scale to ENTRY_LIST_NAME



-- update scale
UPDATE
    master.scale
SET
    abbrev = 'ENTRY_LIST_NAME',
    name = 'Entry List Name'
WHERE
    abbrev = 'CROSS_LIST_NAME'
;



-- revert changes
--rollback UPDATE
--rollback     master.scale
--rollback SET
--rollback     abbrev = 'CROSS_LIST_NAME',
--rollback     name = 'Cross List Name'
--rollback WHERE
--rollback     abbrev = 'ENTRY_LIST_NAME'
--rollback ;
