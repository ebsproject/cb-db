--liquibase formatted sql

--changeset postgres:delete_seed_inventory_config_application context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1062 Delete Seed Inventory Config application



-- delete application_action
DELETE FROM
    platform.application_action AS appaxn
USING
    platform.application AS app
WHERE
    appaxn.application_id = app.id
    AND app.abbrev = 'SEED_INVENTORY_CONFIG'
;

-- delete application
DELETE FROM
    platform.application
WHERE
    abbrev = 'SEED_INVENTORY_CONFIG'
;



-- revert changes
--rollback INSERT INTO
--rollback     platform.application (
--rollback         abbrev, label, action_label, icon, description, creator_id
--rollback     )
--rollback VALUES (
--rollback         'SEED_INVENTORY_CONFIG', 'Seed inventory config', 'Seed inventory config', 'settings', 'A tool to manage configurations used for seed inventories.', 1
--rollback     )
--rollback ;
--rollback 
--rollback INSERT INTO
--rollback     platform.application_action (
--rollback         application_id, module, controller, creator_id
--rollback     )
--rollback SELECT
--rollback     app.id AS application_id,
--rollback     'seedInventory' AS module,
--rollback     'configuration' AS controller,
--rollback     1 AS creator_id
--rollback FROM
--rollback     platform.application AS app
--rollback WHERE
--rollback     app.abbrev = 'SEED_INVENTORY_CONFIG'
--rollback ;



--changeset postgres:delete_status_application context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1062 Delete Status application



-- delete application_action
DELETE FROM
    platform.application_action AS appaxn
USING
    platform.application AS app
WHERE
    appaxn.application_id = app.id
    AND app.abbrev = 'STATUS'
;

-- delete application
DELETE FROM
    platform.application
WHERE
    abbrev = 'STATUS'
;



-- revert changes
--rollback INSERT INTO
--rollback     platform.application (
--rollback         abbrev, label, action_label, icon, description, creator_id
--rollback     )
--rollback VALUES (
--rollback         'STATUS', 'Status', 'Browse status of EBS Core Breeding services', 'speed', 'This page provides information about the status for each of EBS Core Breeding services and how they are responding at the moment.', 1
--rollback     )
--rollback ;
--rollback 
--rollback INSERT INTO
--rollback     platform.application_action (
--rollback         application_id, module, controller, creator_id
--rollback     )
--rollback SELECT
--rollback     app.id AS application_id,
--rollback     'status' AS module,
--rollback     NULL AS controller,
--rollback     1 AS creator_id
--rollback FROM
--rollback     platform.application AS app
--rollback WHERE
--rollback     app.abbrev = 'STATUS'
--rollback ;
