--liquibase formatted sql

--changeset postgres:add_variable_pa_entry_working_list context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1038 Add variable PA_ENTRY_WORKING_LIST



DO $$
DECLARE
	var_property_id int;
	var_method_id int;
	var_scale_id int;
	var_variable_id int;
	var_variable_set_id int;
	var_variable_set_member_order_number int;
	var_count_property_id int;
	var_count_method_id int;
	var_count_scale_id int;
	var_count_variable_set_id int;
	var_count_variable_set_member_id int;
BEGIN
	--PROPERTY
	SELECT count(id) FROM master.property WHERE ABBREV = 'PA_ENTRY_WORKING_LIST' INTO var_count_property_id;
	IF var_count_property_id > 0 THEN
		SELECT id FROM master.property WHERE ABBREV = 'PA_ENTRY_WORKING_LIST' INTO var_property_id;
	ELSE
		INSERT INTO
			master.property (abbrev,display_name,name) 
		VALUES 
			('PA_ENTRY_WORKING_LIST','PA Entry Working List','PA Entry Working List') 
		RETURNING id INTO var_property_id;
	END IF;
	--METHOD
	SELECT count(id) FROM master.method WHERE ABBREV = 'PA_ENTRY_WORKING_LIST' INTO var_count_method_id;
	IF var_count_method_id > 0 THEN
		SELECT id FROM master.method WHERE ABBREV = 'PA_ENTRY_WORKING_LIST' INTO var_method_id;
	ELSE
		INSERT INTO
			master.method (name,abbrev,formula,description) 
		VALUES 
			('PA Entry Working List','PA_ENTRY_WORKING_LIST',NULL,NULL) 
		RETURNING id INTO var_method_id;
	END IF;
	--SCALE
	SELECT count(id) FROM master.scale WHERE ABBREV = 'PA_ENTRY_WORKING_LIST' INTO var_count_scale_id;
	IF var_count_scale_id > 0 THEN
		SELECT id FROM master.scale WHERE ABBREV = 'PA_ENTRY_WORKING_LIST' INTO var_scale_id;
	ELSE
		INSERT INTO
			master.scale (abbrev,type,name,unit,level) 
		VALUES 
			('PA_ENTRY_WORKING_LIST','continuous','PA Entry Working List',NULL,'nominal') 
		RETURNING id INTO var_scale_id;
	END IF;
	--VARIABLE
    INSERT INTO
        master.variable (status,display_name,name,data_type,description,label,not_null,abbrev,usage,type) 
    VALUES 
        ('active','PA Entry Working List','PA Entry Working List','json','PA Entry Working List','PA Entry Working List','False','PA_ENTRY_WORKING_LIST','application','system') 
    RETURNING id INTO var_variable_id;
	--UPDATE PROPERTY_ID, METHOD_ID, SCALE_ID
	UPDATE master.variable SET property_id=var_property_id, method_id=var_method_id, scale_id=var_scale_id WHERE id=var_variable_id;
END;
$$



--rollback DELETE FROM master.scale WHERE id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'PA_ENTRY_WORKING_LIST');

--rollback DELETE FROM master."property" WHERE id IN (SELECT property_id FROM master.variable WHERE abbrev = 'PA_ENTRY_WORKING_LIST');

--rollback DELETE FROM master.method WHERE id IN (SELECT method_id FROM master.variable WHERE abbrev = 'PA_ENTRY_WORKING_LIST');

--rollback DELETE FROM master.variable_set_member WHERE variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'PA_ENTRY_WORKING_LIST');

--rollback DELETE FROM master.variable WHERE abbrev = 'PA_ENTRY_WORKING_LIST';
