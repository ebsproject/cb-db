--liquibase formatted sql

--changeset postgres:add_input_list_limit_to_ss_process_threshold context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1008 SS: Add inputListLimit to bg process threshold config values



-- update browser column config
UPDATE
    platform.config
SET
    config_value = $$
        {
            "addWorkingList": {
                "size": "500",
                "description": "Update entries of an Experiment"
            },
            "deleteWorkingList": {
                "size": "500",
                "description": "Delete entries of an Experiment"
            },
            "inputListLimit": {
                "size": "500",
                "description": "Maximum number of input list items to be processed"
            }
        }
    $$
WHERE
    abbrev = 'FIND_SEEDS_BG_PROCESSING_THRESHOLD'
;



--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $$
--rollback         {
--rollback             "addWorkingList": {
--rollback                 "size": "500",
--rollback                 "description": "Update entries of an Experiment"
--rollback             },
--rollback             "deleteWorkingList": {
--rollback                 "size": "500",
--rollback                 "description": "Delete entries of an Experiment"
--rollback             }
--rollback         }
--rollback     $$
--rollback WHERE
--rollback     abbrev = 'FIND_SEEDS_BG_PROCESSING_THRESHOLD'
--rollback ;
