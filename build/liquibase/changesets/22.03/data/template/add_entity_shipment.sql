--liquibase formatted sql

--changeset postgres:add_entity_shipment context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-823 Add entity SHIPMENT



INSERT INTO
    dictionary.table (database_id,schema_id,abbrev,name,comment,creator_id)
SELECT 
    t.database_id,
    t.schema_id,
    t.abbrev,
    t.name,
    t.comment,
    1
FROM 
    (
        VALUES
            (
                (SELECT id FROM dictionary.database WHERE abbrev='CB'),
                (SELECT id FROM dictionary.schema WHERE abbrev='INVENTORY'),
                'SHIPMENT',
                'Incoming or outgoing shipment transactions',
                'Shipment'
            )
    ) AS t (database_id,schema_id,abbrev,name,comment)
;


INSERT INTO
    dictionary.entity (abbrev,name,description,table_id,creator_id)
SELECT 
    t.abbrev,
    t.name,
    t.description,
    t.table_id,
    1
FROM 
    (
        VALUES
            (
                'SHIPMENT',
                'Shipment',
                'Incoming or outgoing shipment transactions',
                (SELECT id FROM dictionary.table WHERE abbrev='SHIPMENT')
            )
    ) AS t (abbrev,name,description,table_id)
;



--rollback DELETE FROM dictionary.entity WHERE abbrev = 'SHIPMENT';
--rollback DELETE FROM dictionary.table WHERE abbrev = 'SHIPMENT';