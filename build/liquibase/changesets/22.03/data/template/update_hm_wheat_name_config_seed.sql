--liquibase formatted sql

--changeset postgres:update_hm_wheat_name_config_seed context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1040 HM: Update germplasm name and seed name configurations for WHEAT



--update germplasm name config
UPDATE platform.config
SET
	config_value = '{
        "PLOT": {
            "default": {
                "default": {
                    "default": {
                        "single_occurrence": [
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "fieldOriginSiteCode",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "experimentYearYY",
                                "order_number": 1
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "experimentSeasonCode",
                                "order_number": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 3
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "experimentName",
                                "order_number": 4
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 5
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "entryNumber",
                                "order_number": 6
                            }
                        ],
                        "multiple_occurrence": [
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "fieldOriginSiteCode",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "experimentYearYY",
                                "order_number": 1
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "experimentSeasonCode",
                                "order_number": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 3
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "occurrenceName",
                                "order_number": 4
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 5
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "entryNumber",
                                "order_number": 6
                            }
                        ]
                    },
                    "individual spike": {
                        "single_occurrence": [
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "fieldOriginSiteCode",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "experimentYearYY",
                                "order_number": 1
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "experimentSeasonCode",
                                "order_number": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 3
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "experimentName",
                                "order_number": 4
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 5
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "entryNumber",
                                "order_number": 6
                            },
                            {
                                "type": "delimiter",
                                "value": ":",
                                "order_number": 7
                            },
                            {
                                "type": "counter",
                                "order_number": 8
                            }
                        ],
                        "multiple_occurrence": [
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "fieldOriginSiteCode",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "experimentYearYY",
                                "order_number": 1
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "experimentSeasonCode",
                                "order_number": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 3
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "occurrenceName",
                                "order_number": 4
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 5
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "entryNumber",
                                "order_number": 6
                            },
                            {
                                "type": "delimiter",
                                "value": ":",
                                "order_number": 7
                            },
                            {
                                "type": "counter",
                                "order_number": 8
                            }
                        ]
                    },
                    "single plant selection": {
                        "single_occurrence": [
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "fieldOriginSiteCode",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "experimentYearYY",
                                "order_number": 1
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "experimentSeasonCode",
                                "order_number": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 3
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "experimentName",
                                "order_number": 4
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 5
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "entryNumber",
                                "order_number": 6
                            },
                            {
                                "type": "delimiter",
                                "value": ":",
                                "order_number": 7
                            },
                            {
                                "type": "counter",
                                "order_number": 8
                            }
                        ],
                        "multiple_occurrence": [
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "fieldOriginSiteCode",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "experimentYearYY",
                                "order_number": 1
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "experimentSeasonCode",
                                "order_number": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 3
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "occurrenceName",
                                "order_number": 4
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 5
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "entryNumber",
                                "order_number": 6
                            },
                            {
                                "type": "delimiter",
                                "value": ":",
                                "order_number": 7
                            },
                            {
                                "type": "counter",
                                "order_number": 8
                            }
                        ]
                    }
                }
            }
        },
        "CROSS": {
            "default": {
                "default": {
                    "default": {
                        "same_nursery": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "fieldOriginSiteCode",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentYearYY",
                                "order_number": 1
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentSeasonCode",
                                "order_number": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 3
                            },
                            {
                                "type": "field",
                                "entity": "femaleCrossParent",
                                "field_name": "sourceExperimentName",
                                "order_number": 4
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 5
                            },
                            {
                                "type": "field",
                                "entity": "femaleCrossParent",
                                "field_name": "entryNumber",
                                "order_number": 6
                            },
                            {
                                "type": "delimiter",
                                "value": "/",
                                "order_number": 7
                            },
                            {
                                "type": "field",
                                "entity": "maleCrossParent",
                                "field_name": "entryNumber",
                                "order_number": 8
                            }
                        ],
                        "different_nurseries": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "fieldOriginSiteCode",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentYearYY",
                                "order_number": 1
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentSeasonCode",
                                "order_number": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 3
                            },
                            {
                                "type": "field",
                                "entity": "femaleCrossParent",
                                "field_name": "sourceExperimentName",
                                "order_number": 4
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 5
                            },
                            {
                                "type": "field",
                                "entity": "femaleCrossParent",
                                "field_name": "entryNumber",
                                "order_number": 6
                            },
                            {
                                "type": "delimiter",
                                "value": "/",
                                "order_number": 7
                            },
                            {
                                "type": "field",
                                "entity": "maleCrossParent",
                                "field_name": "sourceExperimentName",
                                "order_number": 8
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 9
                            },
                            {
                                "type": "field",
                                "entity": "maleCrossParent",
                                "field_name": "entryNumber",
                                "order_number": 10
                            }
                        ]
                    }
                }
            },
            "backcross": {
                "default": {
                    "default": {
                        "same_nursery": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "fieldOriginSiteCode",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentYearYY",
                                "order_number": 1
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentSeasonCode",
                                "order_number": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 3
                            },
                            {
                                "type": "field",
                                "entity": "femaleCrossParent",
                                "field_name": "sourceExperimentName",
                                "order_number": 4
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 5
                            },
                            {
                                "type": "field",
                                "entity": "femaleCrossParent",
                                "field_name": "entryNumber",
                                "order_number": 6
                            },
                            {
                                "type": "delimiter",
                                "value": "/",
                                "order_number": 7
                            },
                            {
                                "type": "field",
                                "entity": "maleCrossParent",
                                "field_name": "entryNumber",
                                "order_number": 8
                            }
                        ],
                        "different_nurseries": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "fieldOriginSiteCode",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentYearYY",
                                "order_number": 1
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentSeasonCode",
                                "order_number": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 3
                            },
                            {
                                "type": "field",
                                "entity": "femaleCrossParent",
                                "field_name": "sourceExperimentName",
                                "order_number": 4
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 5
                            },
                            {
                                "type": "field",
                                "entity": "femaleCrossParent",
                                "field_name": "entryNumber",
                                "order_number": 6
                            },
                            {
                                "type": "delimiter",
                                "value": "/",
                                "order_number": 7
                            },
                            {
                                "type": "field",
                                "entity": "maleCrossParent",
                                "field_name": "sourceExperimentName",
                                "order_number": 8
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 9
                            },
                            {
                                "type": "field",
                                "entity": "maleCrossParent",
                                "field_name": "entryNumber",
                                "order_number": 10
                            }
                        ]
                    }
                }
            },
            "top cross": {
                "default": {
                    "default": {
                        "same_nursery": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "fieldOriginSiteCode",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentYearYY",
                                "order_number": 1
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentSeasonCode",
                                "order_number": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 3
                            },
                            {
                                "type": "field",
                                "entity": "femaleCrossParent",
                                "field_name": "sourceExperimentName",
                                "order_number": 4
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 5
                            },
                            {
                                "type": "field",
                                "entity": "femaleCrossParent",
                                "field_name": "entryNumber",
                                "order_number": 6
                            },
                            {
                                "type": "delimiter",
                                "value": "/",
                                "order_number": 7
                            },
                            {
                                "type": "field",
                                "entity": "maleCrossParent",
                                "field_name": "entryNumber",
                                "order_number": 8
                            }
                        ],
                        "different_nurseries": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "fieldOriginSiteCode",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentYearYY",
                                "order_number": 1
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentSeasonCode",
                                "order_number": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 3
                            },
                            {
                                "type": "field",
                                "entity": "femaleCrossParent",
                                "field_name": "sourceExperimentName",
                                "order_number": 4
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 5
                            },
                            {
                                "type": "field",
                                "entity": "femaleCrossParent",
                                "field_name": "entryNumber",
                                "order_number": 6
                            },
                            {
                                "type": "delimiter",
                                "value": "/",
                                "order_number": 7
                            },
                            {
                                "type": "field",
                                "entity": "maleCrossParent",
                                "field_name": "sourceExperimentName",
                                "order_number": 8
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 9
                            },
                            {
                                "type": "field",
                                "entity": "maleCrossParent",
                                "field_name": "entryNumber",
                                "order_number": 10
                            }
                        ]
                    }
                }
            },
            "single cross": {
                "default": {
                    "default": {
                        "same_nursery": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "fieldOriginSiteCode",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentYearYY",
                                "order_number": 1
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentSeasonCode",
                                "order_number": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 3
                            },
                            {
                                "type": "field",
                                "entity": "femaleCrossParent",
                                "field_name": "sourceExperimentName",
                                "order_number": 4
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 5
                            },
                            {
                                "type": "field",
                                "entity": "femaleCrossParent",
                                "field_name": "entryNumber",
                                "order_number": 6
                            },
                            {
                                "type": "delimiter",
                                "value": "/",
                                "order_number": 7
                            },
                            {
                                "type": "field",
                                "entity": "maleCrossParent",
                                "field_name": "entryNumber",
                                "order_number": 8
                            }
                        ],
                        "different_nurseries": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "fieldOriginSiteCode",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentYearYY",
                                "order_number": 1
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentSeasonCode",
                                "order_number": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 3
                            },
                            {
                                "type": "field",
                                "entity": "femaleCrossParent",
                                "field_name": "sourceExperimentName",
                                "order_number": 4
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 5
                            },
                            {
                                "type": "field",
                                "entity": "femaleCrossParent",
                                "field_name": "entryNumber",
                                "order_number": 6
                            },
                            {
                                "type": "delimiter",
                                "value": "/",
                                "order_number": 7
                            },
                            {
                                "type": "field",
                                "entity": "maleCrossParent",
                                "field_name": "sourceExperimentName",
                                "order_number": 8
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 9
                            },
                            {
                                "type": "field",
                                "entity": "maleCrossParent",
                                "field_name": "entryNumber",
                                "order_number": 10
                            }
                        ]
                    }
                }
            }
        },
        "harvest_mode": {
            "cross_method": {
                "germplasm_state/germplasm_type": {
                    "harvest_method": [
                        {
                            "type": "free-text",
                            "value": "ABC",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "<entity>",
                            "field_name": "<field_name>",
                            "order_number": 1
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 3
                        },
                        {
                            "type": "db-sequence",
                            "schema": "<schema>",
                            "order_number": 4,
                            "sequence_name": "<sequence_name>"
                        },
                        {
                            "type": "free-text-repeater",
                            "value": "ABC",
                            "minimum": "2",
                            "delimiter": "*",
                            "order_number": 5
                        }
                    ]
                }
            }
        }
    }'
WHERE
	abbrev = 'HM_NAME_PATTERN_SEED_WHEAT_DEFAULT'
;



--rollback UPDATE platform.config
--rollback SET
--rollback 	config_value = '{
--rollback         "PLOT": {
--rollback             "default": {
--rollback                 "default": {
--rollback                     "default": {
--rollback                         "single_occurrence": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "originSiteCode",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "experimentYearYY",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "experimentSeasonCode",
--rollback                                 "order_number": 2
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 3
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "experimentName",
--rollback                                 "order_number": 4
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 5
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "entryNumber",
--rollback                                 "order_number": 6
--rollback                             }
--rollback                         ],
--rollback                         "multiple_occurrence": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "originSiteCode",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "experimentYearYY",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "experimentSeasonCode",
--rollback                                 "order_number": 2
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 3
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "occurrenceName",
--rollback                                 "order_number": 4
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 5
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "entryNumber",
--rollback                                 "order_number": 6
--rollback                             }
--rollback                         ]
--rollback                     },
--rollback                     "individual spike": {
--rollback                         "single_occurrence": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "originSiteCode",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "experimentYearYY",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "experimentSeasonCode",
--rollback                                 "order_number": 2
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 3
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "experimentName",
--rollback                                 "order_number": 4
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 5
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "entryNumber",
--rollback                                 "order_number": 6
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": ":",
--rollback                                 "order_number": 7
--rollback                             },
--rollback                             {
--rollback                                 "type": "counter",
--rollback                                 "order_number": 8
--rollback                             }
--rollback                         ],
--rollback                         "multiple_occurrence": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "originSiteCode",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "experimentYearYY",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "experimentSeasonCode",
--rollback                                 "order_number": 2
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 3
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "occurrenceName",
--rollback                                 "order_number": 4
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 5
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "entryNumber",
--rollback                                 "order_number": 6
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": ":",
--rollback                                 "order_number": 7
--rollback                             },
--rollback                             {
--rollback                                 "type": "counter",
--rollback                                 "order_number": 8
--rollback                             }
--rollback                         ]
--rollback                     },
--rollback                     "single plant selection": {
--rollback                         "single_occurrence": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "originSiteCode",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "experimentYearYY",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "experimentSeasonCode",
--rollback                                 "order_number": 2
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 3
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "experimentName",
--rollback                                 "order_number": 4
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 5
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "entryNumber",
--rollback                                 "order_number": 6
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": ":",
--rollback                                 "order_number": 7
--rollback                             },
--rollback                             {
--rollback                                 "type": "counter",
--rollback                                 "order_number": 8
--rollback                             }
--rollback                         ],
--rollback                         "multiple_occurrence": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "originSiteCode",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "experimentYearYY",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "experimentSeasonCode",
--rollback                                 "order_number": 2
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 3
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "occurrenceName",
--rollback                                 "order_number": 4
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 5
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "entryNumber",
--rollback                                 "order_number": 6
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": ":",
--rollback                                 "order_number": 7
--rollback                             },
--rollback                             {
--rollback                                 "type": "counter",
--rollback                                 "order_number": 8
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             }
--rollback         },
--rollback         "CROSS": {
--rollback             "default": {
--rollback                 "default": {
--rollback                     "default": {
--rollback                         "same_nursery": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "originSiteCode",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "experimentYearYY",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "experimentSeasonCode",
--rollback                                 "order_number": 2
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 3
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "femaleCrossParent",
--rollback                                 "field_name": "sourceExperimentName",
--rollback                                 "order_number": 4
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 5
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "femaleCrossParent",
--rollback                                 "field_name": "entryNumber",
--rollback                                 "order_number": 6
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "/",
--rollback                                 "order_number": 7
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "maleCrossParent",
--rollback                                 "field_name": "entryNumber",
--rollback                                 "order_number": 8
--rollback                             }
--rollback                         ],
--rollback                         "different_nurseries": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "originSiteCode",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "experimentYearYY",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "experimentSeasonCode",
--rollback                                 "order_number": 2
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 3
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "femaleCrossParent",
--rollback                                 "field_name": "sourceExperimentName",
--rollback                                 "order_number": 4
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 5
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "femaleCrossParent",
--rollback                                 "field_name": "entryNumber",
--rollback                                 "order_number": 6
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "/",
--rollback                                 "order_number": 7
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "maleCrossParent",
--rollback                                 "field_name": "sourceExperimentName",
--rollback                                 "order_number": 8
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 9
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "maleCrossParent",
--rollback                                 "field_name": "entryNumber",
--rollback                                 "order_number": 10
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             },
--rollback             "backcross": {
--rollback                 "default": {
--rollback                     "default": {
--rollback                         "same_nursery": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "originSiteCode",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "experimentYearYY",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "experimentSeasonCode",
--rollback                                 "order_number": 2
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 3
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "femaleCrossParent",
--rollback                                 "field_name": "sourceExperimentName",
--rollback                                 "order_number": 4
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 5
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "femaleCrossParent",
--rollback                                 "field_name": "entryNumber",
--rollback                                 "order_number": 6
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "/",
--rollback                                 "order_number": 7
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "maleCrossParent",
--rollback                                 "field_name": "entryNumber",
--rollback                                 "order_number": 8
--rollback                             }
--rollback                         ],
--rollback                         "different_nurseries": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "originSiteCode",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "experimentYearYY",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "experimentSeasonCode",
--rollback                                 "order_number": 2
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 3
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "femaleCrossParent",
--rollback                                 "field_name": "sourceExperimentName",
--rollback                                 "order_number": 4
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 5
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "femaleCrossParent",
--rollback                                 "field_name": "entryNumber",
--rollback                                 "order_number": 6
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "/",
--rollback                                 "order_number": 7
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "maleCrossParent",
--rollback                                 "field_name": "sourceExperimentName",
--rollback                                 "order_number": 8
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 9
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "maleCrossParent",
--rollback                                 "field_name": "entryNumber",
--rollback                                 "order_number": 10
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             },
--rollback             "top cross": {
--rollback                 "default": {
--rollback                     "default": {
--rollback                         "same_nursery": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "originSiteCode",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "experimentYearYY",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "experimentSeasonCode",
--rollback                                 "order_number": 2
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 3
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "femaleCrossParent",
--rollback                                 "field_name": "sourceExperimentName",
--rollback                                 "order_number": 4
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 5
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "femaleCrossParent",
--rollback                                 "field_name": "entryNumber",
--rollback                                 "order_number": 6
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "/",
--rollback                                 "order_number": 7
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "maleCrossParent",
--rollback                                 "field_name": "entryNumber",
--rollback                                 "order_number": 8
--rollback                             }
--rollback                         ],
--rollback                         "different_nurseries": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "originSiteCode",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "experimentYearYY",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "experimentSeasonCode",
--rollback                                 "order_number": 2
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 3
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "femaleCrossParent",
--rollback                                 "field_name": "sourceExperimentName",
--rollback                                 "order_number": 4
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 5
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "femaleCrossParent",
--rollback                                 "field_name": "entryNumber",
--rollback                                 "order_number": 6
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "/",
--rollback                                 "order_number": 7
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "maleCrossParent",
--rollback                                 "field_name": "sourceExperimentName",
--rollback                                 "order_number": 8
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 9
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "maleCrossParent",
--rollback                                 "field_name": "entryNumber",
--rollback                                 "order_number": 10
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             },
--rollback             "single cross": {
--rollback                 "default": {
--rollback                     "default": {
--rollback                         "same_nursery": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "originSiteCode",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "experimentYearYY",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "experimentSeasonCode",
--rollback                                 "order_number": 2
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 3
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "femaleCrossParent",
--rollback                                 "field_name": "sourceExperimentName",
--rollback                                 "order_number": 4
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 5
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "femaleCrossParent",
--rollback                                 "field_name": "entryNumber",
--rollback                                 "order_number": 6
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "/",
--rollback                                 "order_number": 7
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "maleCrossParent",
--rollback                                 "field_name": "entryNumber",
--rollback                                 "order_number": 8
--rollback                             }
--rollback                         ],
--rollback                         "different_nurseries": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "originSiteCode",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "experimentYearYY",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "experimentSeasonCode",
--rollback                                 "order_number": 2
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 3
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "femaleCrossParent",
--rollback                                 "field_name": "sourceExperimentName",
--rollback                                 "order_number": 4
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 5
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "femaleCrossParent",
--rollback                                 "field_name": "entryNumber",
--rollback                                 "order_number": 6
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "/",
--rollback                                 "order_number": 7
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "maleCrossParent",
--rollback                                 "field_name": "sourceExperimentName",
--rollback                                 "order_number": 8
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 9
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "maleCrossParent",
--rollback                                 "field_name": "entryNumber",
--rollback                                 "order_number": 10
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             }
--rollback         },
--rollback         "harvest_mode": {
--rollback             "cross_method": {
--rollback                 "germplasm_state/germplasm_type": {
--rollback                     "harvest_method": [
--rollback                         {
--rollback                             "type": "free-text",
--rollback                             "value": "ABC",
--rollback                             "order_number": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "<entity>",
--rollback                             "field_name": "<field_name>",
--rollback                             "order_number": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "order_number": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "counter",
--rollback                             "order_number": 3
--rollback                         },
--rollback                         {
--rollback                             "type": "db-sequence",
--rollback                             "schema": "<schema>",
--rollback                             "order_number": 4,
--rollback                             "sequence_name": "<sequence_name>"
--rollback                         },
--rollback                         {
--rollback                             "type": "free-text-repeater",
--rollback                             "value": "ABC",
--rollback                             "minimum": "2",
--rollback                             "delimiter": "*",
--rollback                             "order_number": 5
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             }
--rollback         }
--rollback     }'
--rollback WHERE
--rollback 	abbrev = 'HM_NAME_PATTERN_SEED_WHEAT_DEFAULT'
--rollback ;