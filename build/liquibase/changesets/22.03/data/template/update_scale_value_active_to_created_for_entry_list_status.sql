--liquibase formatted sql

--changeset postgres:update_scale_value_active_to_created_for_entry_list_status context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1039 Update scale_value from active to created for ENTRY_LIST_STATUS



UPDATE
    master.scale_value
SET
    value = 'created', 
    description = 'created',
    display_name = 'created',
    abbrev = 'ENTRY_LIST_STATUS_CREATED'
WHERE
    abbrev = 'ENTRY_LIST_STATUS_ACTIVE'
;



--rollback UPDATE
--rollback     master.scale_value
--rollback SET
--rollback     value = 'active', 
--rollback     description = 'active',
--rollback     display_name = 'active',
--rollback     abbrev = 'ENTRY_LIST_STATUS_ACTIVE'
--rollback WHERE
--rollback     abbrev = 'ENTRY_LIST_STATUS_CREATED'
--rollback ;