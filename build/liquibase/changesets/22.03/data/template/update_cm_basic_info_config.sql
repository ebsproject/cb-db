--liquibase formatted sql

--changeset postgres:update_cm_basic_info_config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1057 CM-DB: Update basic info configuration



UPDATE
    platform.config
SET
    config_value =
    $${
        "Values": [
            {
                "disabled": false,
                "required": "required",
                "api_field": "entryListName",
                "field_label": "Cross List Name",
                "order_number": 1,
                "variable_abbrev": "ENTRY_LIST_NAME",
                "field_description": "Name of the cross list"
            },
            {
                "disabled": false,
                "required": "required",
                "api_field": "experimentYear",
                "field_label": "Experiment Year",
                "order_number": 2,
                "variable_abbrev": "EXPERIMENT_YEAR",
                "field_description": "Experiment Year"
            },
            {
                "disabled": false,
                "api_field": "description",
                "data_type": "text",
                "field_label": "Description",
                "order_number": 3,
                "target_column": "description",
                "variable_type": "identification",
                "variable_abbrev": "DESCRIPTION",
                "field_description": "Additional cross list details"
            },
            {
                "disabled": false,
                "required": "required",
                "api_field": "siteDbId",
                "field_label": "Site",
                "order_number": 4,
                "target_value": "geospatialObjectName",
                "target_column": "siteDbId",
                "variable_type": "identification",
                "variable_abbrev": "SITE",
                "api_resource_sort": "",
                "field_description": "Site where parents have been planted",
                "api_resource_filter": {
                    "geospatialObjectType": "site"
                },
                "api_resource_method": "POST",
                "api_resource_endpoint": "geospatial-objects-search",
                "secondary_target_column": "geospatialObjectDbId"
            },
            {
                "disabled": false,
                "required": "required",
                "api_field": "seasonDbId",
                "field_label": "Season",
                "order_number": 5,
                "target_value": "seasonCode",
                "target_column": "seasonDbId",
                "variable_type": "identification",
                "variable_abbrev": "SEASON",
                "api_resource_sort": "sort=seasonCode",
                "field_description": "Season",
                "api_resource_method": "GET",
                "api_resource_endpoint": "seasons"
            }
        ],
          "Name": "Cross Manager Basic Info tab default fields"
      }$$
WHERE
    abbrev = 'CM_CROSS_LIST_BASIC_INFO';



--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value =
--rollback     $${
--rollback         "Values": [
--rollback             {
--rollback                 "field_label": "Cross List Name",
--rollback                 "order_number": 1,
--rollback                 "variable_abbrev": "CROSS_LIST_NAME",
--rollback                 "required": "required",
--rollback                 "disabled": false,
--rollback                 "field_description": "Name of the cross list"
--rollback             },
--rollback             {
--rollback                 "field_label": "Experiment Year",
--rollback                 "order_number": 2,
--rollback                 "variable_abbrev": "EXPERIMENT_YEAR",
--rollback                 "required": "required",
--rollback                 "disabled": false,
--rollback                 "field_description": "Experiment Year"
--rollback             },
--rollback             {
--rollback                 "field_label": "Description",
--rollback                 "order_number": 3,
--rollback                 "variable_abbrev": "DESCRIPTION",
--rollback                 "disabled": false,
--rollback                 "field_description": "Additional cross list details"
--rollback             },
--rollback             {
--rollback                 "field_label": "Site",
--rollback                 "order_number": 4,
--rollback                 "variable_abbrev": "SITE",
--rollback                 "required": "required",
--rollback                 "disabled": false,
--rollback                 "field_description": "Site where parents have been planted"
--rollback             },
--rollback             {
--rollback                 "field_label": "Season",
--rollback                 "order_number": 5,
--rollback                 "variable_abbrev": "SEASON",
--rollback                 "required": "required",
--rollback                 "disabled": false,
--rollback                 "field_description": "Season"
--rollback             }
--rollback         ],
--rollback         "Name": "Cross Manager Basic Info tab default fields"
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'CM_CROSS_LIST_BASIC_INFO';
