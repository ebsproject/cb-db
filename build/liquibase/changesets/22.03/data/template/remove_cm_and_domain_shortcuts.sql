--liquibase formatted sql

--changeset postgres:remove_cm_and_domain_shortcuts context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1077 Remove CM and domain shortcuts



DELETE FROM platform.application WHERE abbrev = 'CROSS_MANAGER';

DELETE FROM platform.application_action WHERE application_id IN (SELECT id FROM platform.application WHERE abbrev = 'CROSS_MANAGER');



--rollback INSERT INTO 
--rollback     platform.application (abbrev, label, action_label, icon, creator_id)
--rollback VALUES
--rollback     ('CROSS_MANAGER', 'Cross manager', 'Cross manager', 'repeat', 1);

--rollback INSERT INTO 
--rollback     platform.application_action (application_id, module, creator_id)
--rollback SELECT 
--rollback 	id,
--rollback 	'crossManager',
--rollback 	1
--rollback FROM 
--rollback 	platform.application
--rollback WHERE 
--rollback 	abbrev = 'CROSS_MANAGER';