--liquibase formatted sql

--changeset postgres:update_hm_rice_name_config_cross_003 context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1052 HM: Update Rice browser config and cross name config to support cms multiplication harvest



--update germplasm name config
UPDATE platform.config
SET
	config_value = '{
        "PLOT": {
            "default": {
                "default": {
                    "default": [
                        {
                            "type": "free-text",
                            "value": "IR XXXXX",
                            "order_number": 0
                        }
                    ]
                }
            }
        },
        "CROSS": {
            "default": {
                "default": {
                    "bulk": [
                        {
                            "type": "free-text",
                            "value": "IR ",
                            "order_number": 0
                        },
                        {
                            "type": "db-sequence",
                            "schema": "germplasm",
                            "order_number": 1,
                            "sequence_name": "cross_number_seq"
                        }
                    ],
                    "default": [
                        {
                            "type": "free-text",
                            "value": "IR ",
                            "order_number": 0
                        },
                        {
                            "type": "db-sequence",
                            "schema": "germplasm",
                            "order_number": 1,
                            "sequence_name": "cross_number_seq"
                        }
                    ]
                }
            },
            "hybrid formation": {
                "default": {
                    "default": [
                        {
                            "type": "free-text",
                            "value": "IR ",
                            "order_number": 0
                        },
                        {
                            "type": "db-sequence",
                            "schema": "germplasm",
                            "order_number": 1,
                            "sequence_name": "cross_number_seq"
                        },
                        {
                            "type": "free-text",
                            "value": " H",
                            "order_number": 2
                        }
                    ]
                }
            },
            "cms multiplication": {
                "default": {
                    "default": [
                        {
                            "type": "free-text",
                            "value": "IR ",
                            "order_number": 0
                        },
                        {
                            "type": "db-sequence",
                            "schema": "germplasm",
                            "order_number": 1,
                            "sequence_name": "cross_number_seq"
                        },
                        {
                            "type": "free-text",
                            "value": " A",
                            "order_number": 2
                        }
                    ]
                }
            },
            "single cross": {
                "default": {
                    "bulk": [
                        {
                            "type": "free-text",
                            "value": "IR ",
                            "order_number": 0
                        },
                        {
                            "type": "db-sequence",
                            "schema": "germplasm",
                            "order_number": 1,
                            "sequence_name": "cross_number_seq"
                        }
                    ],
                    "default": [
                        {
                            "type": "free-text",
                            "value": "IR ",
                            "order_number": 0
                        },
                        {
                            "type": "db-sequence",
                            "schema": "germplasm",
                            "order_number": 1,
                            "sequence_name": "cross_number_seq"
                        }
                    ],
                    "single seed numbering": [
                        {
                            "type": "free-text",
                            "value": "IR ",
                            "order_number": 0
                        },
                        {
                            "type": "db-sequence",
                            "schema": "germplasm",
                            "order_number": 1,
                            "sequence_name": "cross_number_seq"
                        }
                    ]
                }
            }
        },
        "harvest_mode": {
            "cross_method": {
                "germplasm_state/germplasm_type": {
                    "harvest_method": [
                        {
                            "type": "free-text",
                            "value": "ABC",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "<entity>",
                            "field_name": "<field_name>",
                            "order_number": 1
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 3
                        },
                        {
                            "type": "db-sequence",
                            "schema": "<schema>",
                            "order_number": 4,
                            "sequence_name": "<sequence_name>"
                        }
                    ]
                }
            }
        }
    }'
WHERE
	abbrev = 'HM_NAME_PATTERN_CROSS_RICE_DEFAULT'
;



--rollback UPDATE platform.config
--rollback SET
--rollback 	config_value = '{
--rollback         "PLOT": {
--rollback             "default": {
--rollback                 "default": {
--rollback                     "default": [
--rollback                         {
--rollback                             "type": "free-text",
--rollback                             "value": "IR XXXXX",
--rollback                             "order_number": 0
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             }
--rollback         },
--rollback         "CROSS": {
--rollback             "default": {
--rollback                 "default": {
--rollback                     "bulk": [
--rollback                         {
--rollback                             "type": "free-text",
--rollback                             "value": "IR ",
--rollback                             "order_number": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "db-sequence",
--rollback                             "schema": "germplasm",
--rollback                             "order_number": 1,
--rollback                             "sequence_name": "cross_number_seq"
--rollback                         }
--rollback                     ],
--rollback                     "default": [
--rollback                         {
--rollback                             "type": "free-text",
--rollback                             "value": "IR ",
--rollback                             "order_number": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "db-sequence",
--rollback                             "schema": "germplasm",
--rollback                             "order_number": 1,
--rollback                             "sequence_name": "cross_name_seq"
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             "single cross": {
--rollback                 "default": {
--rollback                     "bulk": [
--rollback                         {
--rollback                             "type": "free-text",
--rollback                             "value": "IR ",
--rollback                             "order_number": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "db-sequence",
--rollback                             "schema": "germplasm",
--rollback                             "order_number": 1,
--rollback                             "sequence_name": "cross_number_seq"
--rollback                         }
--rollback                     ],
--rollback                     "default": [
--rollback                         {
--rollback                             "type": "free-text",
--rollback                             "value": "IR ",
--rollback                             "order_number": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "db-sequence",
--rollback                             "schema": "germplasm",
--rollback                             "order_number": 1,
--rollback                             "sequence_name": "cross_name_seq"
--rollback                         }
--rollback                     ],
--rollback                     "single seed numbering": [
--rollback                         {
--rollback                             "type": "free-text",
--rollback                             "value": "IR ",
--rollback                             "order_number": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "db-sequence",
--rollback                             "schema": "germplasm",
--rollback                             "order_number": 1,
--rollback                             "sequence_name": "cross_number_seq"
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             }
--rollback         },
--rollback         "harvest_mode": {
--rollback             "cross_method": {
--rollback                 "germplasm_state/germplasm_type": {
--rollback                     "harvest_method": [
--rollback                         {
--rollback                             "type": "free-text",
--rollback                             "value": "ABC",
--rollback                             "order_number": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "<entity>",
--rollback                             "field_name": "<field_name>",
--rollback                             "order_number": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "order_number": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "counter",
--rollback                             "order_number": 3
--rollback                         },
--rollback                         {
--rollback                             "type": "db-sequence",
--rollback                             "schema": "<schema>",
--rollback                             "order_number": 4,
--rollback                             "sequence_name": "<sequence_name>"
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             }
--rollback         }
--rollback     }'
--rollback WHERE
--rollback 	abbrev = 'HM_NAME_PATTERN_CROSS_RICE_DEFAULT'
--rollback ;