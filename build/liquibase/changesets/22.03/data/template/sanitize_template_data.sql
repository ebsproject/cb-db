--liquibase formatted sql

--changeset postgres:sanitize_template_data context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-883 Sanitize text values in template data



-- remove extra spaces
UPDATE api.messages SET message = TRIM(regexp_replace(message, '\s{2,}', ' ', 'g')) WHERE message IS NOT NULL AND (message ~ '\s{2,}' OR message LIKE ' %' OR message LIKE '% ');
UPDATE api.messages SET type = TRIM(regexp_replace(type, '\s{2,}', ' ', 'g')) WHERE type IS NOT NULL AND (type ~ '\s{2,}' OR type LIKE ' %' OR type LIKE '% ');
UPDATE dictionary.schema SET comment = TRIM(regexp_replace(comment, '\s{2,}', ' ', 'g')) WHERE comment IS NOT NULL AND (comment ~ '\s{2,}' OR comment LIKE ' %' OR comment LIKE '% ');
UPDATE master.instruction SET value = TRIM(regexp_replace(value, '\s{2,}', ' ', 'g')) WHERE value IS NOT NULL AND (value ~ '\s{2,}' OR value LIKE ' %' OR value LIKE '% ');
UPDATE master.item SET description = TRIM(regexp_replace(description, '\s{2,}', ' ', 'g')) WHERE description IS NOT NULL AND (description ~ '\s{2,}' OR description LIKE ' %' OR description LIKE '% ');
UPDATE master.item SET display_name = TRIM(regexp_replace(display_name, '\s{2,}', ' ', 'g')) WHERE display_name IS NOT NULL AND (display_name ~ '\s{2,}' OR display_name LIKE ' %' OR display_name LIKE '% ');
UPDATE master.item SET name = TRIM(regexp_replace(name, '\s{2,}', ' ', 'g')) WHERE name IS NOT NULL AND (name ~ '\s{2,}' OR name LIKE ' %' OR name LIKE '% ');
UPDATE master.method SET description = TRIM(regexp_replace(description, '\s{2,}', ' ', 'g')) WHERE description IS NOT NULL AND (description ~ '\s{2,}' OR description LIKE ' %' OR description LIKE '% ');
UPDATE master.method SET name = TRIM(regexp_replace(name, '\s{2,}', ' ', 'g')) WHERE name IS NOT NULL AND (name ~ '\s{2,}' OR name LIKE ' %' OR name LIKE '% ');
UPDATE master.method SET remarks = TRIM(regexp_replace(remarks, '\s{2,}', ' ', 'g')) WHERE remarks IS NOT NULL AND (remarks ~ '\s{2,}' OR remarks LIKE ' %' OR remarks LIKE '% ');
UPDATE master.property SET bibliographical_reference = TRIM(regexp_replace(bibliographical_reference, '\s{2,}', ' ', 'g')) WHERE bibliographical_reference IS NOT NULL AND (bibliographical_reference ~ '\s{2,}' OR bibliographical_reference LIKE ' %' OR bibliographical_reference LIKE '% ');
UPDATE master.property SET description = TRIM(regexp_replace(description, '\s{2,}', ' ', 'g')) WHERE description IS NOT NULL AND (description ~ '\s{2,}' OR description LIKE ' %' OR description LIKE '% ');
UPDATE master.property SET display_name = TRIM(regexp_replace(display_name, '\s{2,}', ' ', 'g')) WHERE display_name IS NOT NULL AND (display_name ~ '\s{2,}' OR display_name LIKE ' %' OR display_name LIKE '% ');
UPDATE master.property SET name = TRIM(regexp_replace(name, '\s{2,}', ' ', 'g')) WHERE name IS NOT NULL AND (name ~ '\s{2,}' OR name LIKE ' %' OR name LIKE '% ');
UPDATE master.property SET ontology_id = TRIM(regexp_replace(ontology_id, '\s{2,}', ' ', 'g')) WHERE ontology_id IS NOT NULL AND (ontology_id ~ '\s{2,}' OR ontology_id LIKE ' %' OR ontology_id LIKE '% ');
UPDATE master.property SET remarks = TRIM(regexp_replace(remarks, '\s{2,}', ' ', 'g')) WHERE remarks IS NOT NULL AND (remarks ~ '\s{2,}' OR remarks LIKE ' %' OR remarks LIKE '% ');
UPDATE master.record SET display_name = TRIM(regexp_replace(display_name, '\s{2,}', ' ', 'g')) WHERE display_name IS NOT NULL AND (display_name ~ '\s{2,}' OR display_name LIKE ' %' OR display_name LIKE '% ');
UPDATE master.record SET name = TRIM(regexp_replace(name, '\s{2,}', ' ', 'g')) WHERE name IS NOT NULL AND (name ~ '\s{2,}' OR name LIKE ' %' OR name LIKE '% ');
UPDATE master.scale SET description = TRIM(regexp_replace(description, '\s{2,}', ' ', 'g')) WHERE description IS NOT NULL AND (description ~ '\s{2,}' OR description LIKE ' %' OR description LIKE '% ');
UPDATE master.scale SET name = TRIM(regexp_replace(name, '\s{2,}', ' ', 'g')) WHERE name IS NOT NULL AND (name ~ '\s{2,}' OR name LIKE ' %' OR name LIKE '% ');
UPDATE master.scale SET remarks = TRIM(regexp_replace(remarks, '\s{2,}', ' ', 'g')) WHERE remarks IS NOT NULL AND (remarks ~ '\s{2,}' OR remarks LIKE ' %' OR remarks LIKE '% ');
UPDATE master.scale_value SET description = TRIM(regexp_replace(description, '\s{2,}', ' ', 'g')) WHERE description IS NOT NULL AND (description ~ '\s{2,}' OR description LIKE ' %' OR description LIKE '% ');
UPDATE master.scale_value SET display_name = TRIM(regexp_replace(display_name, '\s{2,}', ' ', 'g')) WHERE display_name IS NOT NULL AND (display_name ~ '\s{2,}' OR display_name LIKE ' %' OR display_name LIKE '% ');
UPDATE master.tooltip SET description = TRIM(regexp_replace(description, '\s{2,}', ' ', 'g')) WHERE description IS NOT NULL AND (description ~ '\s{2,}' OR description LIKE ' %' OR description LIKE '% ');
UPDATE master.tooltip SET value = TRIM(regexp_replace(value, '\s{2,}', ' ', 'g')) WHERE value IS NOT NULL AND (value ~ '\s{2,}' OR value LIKE ' %' OR value LIKE '% ');
UPDATE master.variable SET description = TRIM(regexp_replace(description, '\s{2,}', ' ', 'g')) WHERE description IS NOT NULL AND (description ~ '\s{2,}' OR description LIKE ' %' OR description LIKE '% ');
UPDATE master.variable SET display_name = TRIM(regexp_replace(display_name, '\s{2,}', ' ', 'g')) WHERE display_name IS NOT NULL AND (display_name ~ '\s{2,}' OR display_name LIKE ' %' OR display_name LIKE '% ');
UPDATE master.variable SET label = TRIM(regexp_replace(label, '\s{2,}', ' ', 'g')) WHERE label IS NOT NULL AND (label ~ '\s{2,}' OR label LIKE ' %' OR label LIKE '% ');
UPDATE master.variable SET name = TRIM(regexp_replace(name, '\s{2,}', ' ', 'g')) WHERE name IS NOT NULL AND (name ~ '\s{2,}' OR name LIKE ' %' OR name LIKE '% ');
UPDATE master.variable SET remarks = TRIM(regexp_replace(remarks, '\s{2,}', ' ', 'g')) WHERE remarks IS NOT NULL AND (remarks ~ '\s{2,}' OR remarks LIKE ' %' OR remarks LIKE '% ');
UPDATE master.variable_set SET description = TRIM(regexp_replace(description, '\s{2,}', ' ', 'g')) WHERE description IS NOT NULL AND (description ~ '\s{2,}' OR description LIKE ' %' OR description LIKE '% ');
UPDATE master.variable_set SET name = TRIM(regexp_replace(name, '\s{2,}', ' ', 'g')) WHERE name IS NOT NULL AND (name ~ '\s{2,}' OR name LIKE ' %' OR name LIKE '% ');
UPDATE master.variable_set SET remarks = TRIM(regexp_replace(remarks, '\s{2,}', ' ', 'g')) WHERE remarks IS NOT NULL AND (remarks ~ '\s{2,}' OR remarks LIKE ' %' OR remarks LIKE '% ');
UPDATE platform.application SET description = TRIM(regexp_replace(description, '\s{2,}', ' ', 'g')) WHERE description IS NOT NULL AND (description ~ '\s{2,}' OR description LIKE ' %' OR description LIKE '% ');
UPDATE platform.config SET name = TRIM(regexp_replace(name, '\s{2,}', ' ', 'g')) WHERE name IS NOT NULL AND (name ~ '\s{2,}' OR name LIKE ' %' OR name LIKE '% ');



-- revert changes
--rollback SELECT NULL;
