--liquibase formatted sql

--changeset postgres:update_experiment_status_to_match_with_location_status_part_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-27 Match experiment status with location status Part 2



-- match experiment status with occurrence status
UPDATE experiment.occurrence
SET occurrence_status = 'planted'
WHERE occurrence.id
IN (
    3377,
    3378,
    3379,
    3380,
    3381,
    3382,
    3383,
    3384,
    3385,
    3386,
    3387,
    3388,
    3389,
    3390,
    3391,
    3392
    )
;



--rollback UPDATE experiment.occurrence
--rollback SET occurrence_status = 'completed'
--rollback WHERE occurrence.id
--rollback IN (
--rollback     3377,
--rollback     3378,
--rollback     3379,
--rollback     3380,
--rollback     3381,
--rollback     3382,
--rollback     3383,
--rollback     3384,
--rollback     3385,
--rollback     3386,
--rollback     3387,
--rollback     3388,
--rollback     3389,
--rollback     3390,
--rollback     3391,
--rollback     3392
--rollback     )
--rollback ;