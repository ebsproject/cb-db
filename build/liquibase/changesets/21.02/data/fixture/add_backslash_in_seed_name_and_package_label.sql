--liquibase formatted sql

--changeset postgres:add_backslash_in_seed_name_and_package_label context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Add backslash in seed_name and package_label



UPDATE 
    germplasm.package
SET
    package_label=(REPLACE(package_label,'/','\'))
WHERE
    id 
IN
    (
        2271578,
        2271577,
        2271555,
        2271549,
        2271516,
        2271515,
        2269272,
        2269266,
        2269261,
        2269247
    );

UPDATE 
    germplasm.seed
SET
    seed_name=(REPLACE(seed_name,'/','\'))
WHERE
    id 
IN
    (
        1979631,
		1979645,
		1979650,
		1979656,
		1981899,
		1981900,
		1981933,
		1981939,
		1981961,
		1981962
    );



--rollback UPDATE 
--rollback     germplasm.package
--rollback SET
--rollback     package_label=(REPLACE(package_label,'\','/'))
--rollback WHERE
--rollback     id 
--rollback IN
--rollback     (
--rollback         2271578,
--rollback         2271577,
--rollback         2271555,
--rollback         2271549,
--rollback         2271516,
--rollback         2271515,
--rollback         2269272,
--rollback         2269266,
--rollback         2269261,
--rollback         2269247
--rollback     );

--rollback UPDATE 
--rollback     germplasm.seed
--rollback SET
--rollback     seed_name=(REPLACE(seed_name,'\','/'))
--rollback WHERE
--rollback     id 
--rollback IN
--rollback     (
--rollback         1979631,
--rollback 		1979645,
--rollback 		1979650,
--rollback 		1979656,
--rollback 		1981899,
--rollback 		1981900,
--rollback 		1981933,
--rollback 		1981939,
--rollback 		1981961,
--rollback 		1981962
--rollback     );