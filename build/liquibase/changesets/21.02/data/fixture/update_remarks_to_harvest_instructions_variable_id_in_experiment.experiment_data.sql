--liquibase formatted sql

--changeset postgres:update_remarks_to_harvest_instructions_variable_id_in_experiment.experiment_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-4 Update REMARKS to HARVEST INSTRUCTIONS variable_id in experiment.experiment_data



UPDATE 
	experiment.experiment_data
SET 
	variable_id=(SELECT id FROM master.variable WHERE abbrev='HARVEST_INSTRUCTIONS') 
WHERE 
	variable_id IN (SELECT id FROM master.variable WHERE abbrev='REMARKS') AND protocol_id IS NOT NULL;



--rollback UPDATE 
--rollback 	experiment.experiment_data
--rollback SET 
--rollback 	variable_id=(SELECT id FROM master.variable WHERE abbrev='REMARKS') 
--rollback WHERE 
--rollback 	variable_id IN (SELECT id FROM master.variable WHERE abbrev='HARVEST_INSTRUCTIONS') AND protocol_id IS NOT NULL;