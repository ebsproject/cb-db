--liquibase formatted sql

--changeset postgres:add_background_processing_scale_values_to_occurrence_status context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-31 Add background processing scale values to OCCURRENCE_STATUS



INSERT INTO 
    master.scale_value 
        (scale_id,value,order_number,description,display_name,abbrev,creator_id)
VALUES
    ((SELECT scale_id FROM master.variable WHERE abbrev='OCCURRENCE_STATUS'),'generate location in progress',6,'generate location in progress','generate location in progress','OCCURRENCE_STATUS_GENERATE_LOCATION_IN_PROGRESS',1),
    ((SELECT scale_id FROM master.variable WHERE abbrev='OCCURRENCE_STATUS'),'generate location failed',7,'generate location failed','generate location failed','OCCURRENCE_STATUS_GENERATE_LOCATION_FAILED',1),
    ((SELECT scale_id FROM master.variable WHERE abbrev='OCCURRENCE_STATUS'),'upload planting arrays in progress',8,'upload planting arrays in progress','upload planting arrays in progress','OCCURRENCE_STATUS_UPLOAD_PLANTING_ARRAYS_IN_PROGRESS',1),
    ((SELECT scale_id FROM master.variable WHERE abbrev='OCCURRENCE_STATUS'),'upload planting arrays failed',9,'upload planting arrays failed','upload planting arrays failed','OCCURRENCE_STATUS_UPLOAD_PLANTING_ARRAYS_FAILED',1)
;



--rollback DELETE FROM master.scale_value WHERE abbrev IN ('OCCURRENCE_STATUS_GENERATE_LOCATION_IN_PROGRESS','OCCURRENCE_STATUS_GENERATE_LOCATION_FAILED','OCCURRENCE_STATUS_UPLOAD_PLANTING_ARRAYS_IN_PROGRESS','OCCURRENCE_STATUS_UPLOAD_PLANTING_ARRAYS_FAILED');