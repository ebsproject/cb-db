--liquibase formatted sql

--changeset postgres:add_config_breeding_trial_pollination_protocol_act_val_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Add config BREEDING_TRIAL_POLLINATION_PROTOCOL_ACT_VAL to platform.config



INSERT INTO 
    platform.config (abbrev, name, config_value, rank, usage, creator_id, notes)
VALUES 
    (
        'BREEDING_TRIAL_POLLINATION_PROTOCOL_ACT_VAL',
        'Breeding Trial Pollination Protocol variables', 
        '{
            "Name": "Required experiment level pollination protocol variables for Breeding Trial data process",
            "Values": [
                {
                    "default": false,
                    "disabled": false,
                    "variable_abbrev": "POLLINATION_INSTRUCTIONS"
                }
            ]
        }'::json,
        1,
        'experiment_creation',
        1,
        'added by j.antonio'
    );



--rollback DELETE FROM platform.config WHERE abbrev='BREEDING_TRIAL_POLLINATION_PROTOCOL_ACT_VAL';