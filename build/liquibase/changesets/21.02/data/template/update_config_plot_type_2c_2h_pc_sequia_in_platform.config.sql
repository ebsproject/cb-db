--liquibase formatted sql

--changeset postgres:update_config_plot_type_2c_2h_pc_sequia_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update config PLOT_TYPE_2C_2H_PC_SEQUIA in platform.config



UPDATE 
    platform.config 
SET 
    config_value = 
'
{
	"Name": "Required experiment level protocol plot type variables for Plot type 2C/2H_PC_Sequia",
	"Values": [{
			"default": 2,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "NO_OF_BEDS_PER_PLOT"
		},
		{
			"unit": "m",
			"default": 2,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "NO_OF_ROWS_PER_BED"
		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"variable_abbrev": "BED_WIDTH"
		},
		{
			"unit": "m",
			"default": 1.6,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH_WHEAT_BED"
		},
		{
			"unit": "m",
			"default": 1.5,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_LN"
		},
		{
			"unit": "sqm",
			"default": 2.4,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_4"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}
' 
WHERE 
    abbrev = 'PLOT_TYPE_2C_2H_PC_SEQUIA';



--rollback UPDATE
--rollback 	platform.config
--rollback SET
--rollback 	config_value=
--rollback '
--rollback {
--rollback     "Name": "Required experiment level protocol plot type variables for Plot type 2C/2H_PC_Sequia",
--rollback     "Values": [
--rollback         {
--rollback             "default": 2,
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "field_label": "No of beds per plot",
--rollback             "variable_abbrev": "NO_OF_BEDS_PER_PLOT",
--rollback             "field_description": "Number of beds per plot"
--rollback         },
--rollback         {
--rollback             "unit": "m",
--rollback             "default": 2,
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "field_label": "No of rows per bed",
--rollback             "variable_abbrev": "NO_OF_ROWS_PER_BED",
--rollback             "field_description": "Number of rows per bed"
--rollback         },
--rollback         {
--rollback             "unit": "m",
--rollback             "default": false,
--rollback             "computed": "computed",
--rollback             "disabled": false,
--rollback             "required": "required",
--rollback             "field_label": "Bed width",
--rollback             "variable_abbrev": "BED_WIDTH",
--rollback             "field_description": "Bed width"
--rollback         },
--rollback         {
--rollback             "unit": "m",
--rollback             "default": 1.6,
--rollback             "computed": "computed",
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "field_label": "Plot width",
--rollback             "variable_abbrev": "PLOT_WIDTH_WHEAT_BED",
--rollback             "field_description": "Plot width"
--rollback         },
--rollback         {
--rollback             "unit": "m",
--rollback             "default": 1.5,
--rollback             "computed": "computed",
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "field_label": "Bed length",
--rollback             "variable_abbrev": "PLOT_LN",
--rollback             "field_description": "Plot length"
--rollback         },
--rollback         {
--rollback             "unit": "sqm",
--rollback             "default": 2.4,
--rollback             "computed": "computed",
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "field_label": "Plot area",
--rollback             "variable_abbrev": "PLOT_AREA_4",
--rollback             "field_description": "Plot area"
--rollback         },
--rollback         {
--rollback             "unit": "m",
--rollback             "default": false,
--rollback             "disabled": false,
--rollback             "required": false,
--rollback             "field_label": "Alley length",
--rollback             "variable_abbrev": "ALLEY_LENGTH",
--rollback             "field_description": "Alley length"
--rollback         },
--rollback         {
--rollback             "unit": "m",
--rollback             "default": false,
--rollback             "disabled": false,
--rollback             "required": false,
--rollback             "field_label": "Seeding Rate",
--rollback             "allow_new_val": true,
--rollback             "variable_abbrev": "SEEDING_RATE",
--rollback             "field_description": "Seeding Rate"
--rollback         }
--rollback     ]
--rollback }
--rollback '
--rollback WHERE
--rollback 	abbrev='PLOT_TYPE_2C_2H_PC_SEQUIA';