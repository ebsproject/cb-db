--liquibase formatted sql

--changeset postgres:add_config_hm_package_label_pattern_rice_default_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Add config HM_PACKAGE_LABEL_PATTERN_RICE_DEFAULT to platform.config



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_PACKAGE_LABEL_PATTERN_RICE_DEFAULT',
        'Harvest Manager Rice Package Label Pattern-Default',
$$			
		
{
	"default": {
	  "default" : {
        "default" : [
          {
            "type" : "field",
            "entity" : "plot",
            "field_name" : "plotCode",
            "order_number":0
          }
        ]
      }
    }
}
$$,
        1,
        'harvest_manager',
        1,
        'CORB-53 update config - j.bantay 2021-02-04');



--rollback DELETE FROM platform.config WHERE abbrev='HM_PACKAGE_LABEL_PATTERN_RICE_DEFAULT';