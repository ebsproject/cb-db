--liquibase formatted sql

--changeset postgres:add_config_hm_germplasm_name_pattern_rice_default_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Add config HM_GERMPLASM_NAME_PATTERN_RICE_DEFAULT to platform.config



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_GERMPLASM_NAME_PATTERN_RICE_DEFAULT',
        'Harvest Manager Rice Germplasm Name Pattern-Default',
$$			
        
{
    "default": {
      "default" : {
        "default" : [
          {
            "type" : "free-text",
            "value" : "IR XXXXX",
            "order_number" : 0
          }
        ]
      }
    },
    "breeding trial" : {
      "default" : {
        "default" : [
          {
            "type" : "free-text",
            "value" : "IR XXXXX",
            "order_number" : 0
          }
        ]
      }
    },
    "generation nursery" : {
      "default" : {
        "default" : [
          {
            "type" : "free-text",
            "value" : "IR XXXXX",
            "order_number" : 0
          }
        ]
      },
      "progeny" : {
        "default" : [
          {
            "type" : "free-text",
            "value" : "IR XXXXX",
            "order_number" : 0
          }
        ],
        "single plant selection" : [
          {
            "type" : "field",
            "entity" : "plot",
            "field_name" : "germplasmDesignation",
            "order_number":0
          },
          {
            "type" : "delimiter",
            "value" : "-",
            "order_number" : 1
          },
          {
            "type" : "counter",
            "order_number" : 2
          }
        ]
      }
    }
}
$$,
        1,
        'harvest_manager',
        1,
        'CORB-54 update config - j.bantay 2021-02-04');



--rollback DELETE FROM platform.config WHERE abbrev='HM_GERMPLASM_NAME_PATTERN_RICE_DEFAULT';