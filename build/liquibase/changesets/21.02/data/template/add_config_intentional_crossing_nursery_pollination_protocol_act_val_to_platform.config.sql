--liquibase formatted sql

--changeset postgres:add_config_intentional_crossing_nursery_pollination_protocol_act_val_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Add config INTENTIONAL_CROSSING_NURSERY_POLLINATION_PROTOCOL_ACT_VAL to platform.config



INSERT INTO 
    platform.config (abbrev, name, config_value, rank, usage, creator_id, notes)
VALUES 
    (
        'INTENTIONAL_CROSSING_NURSERY_POLLINATION_PROTOCOL_ACT_VAL',
         'Intentional Crossing Nursery Pollination Protocol variables', 
        '{
            "Name": "Required experiment level pollination protocol variables for Intentional Crossing Nursery data process",
            "Values": [
                {
                    "default": false,
                    "disabled": false,
                    "variable_abbrev": "POLLINATION_INSTRUCTIONS"
                }
            ]
        }'::json,
        1,
        'experiment_creation',
        1,
        'added by j.antonio'
    );



--rollback DELETE FROM platform.config WHERE abbrev='INTENTIONAL_CROSSING_NURSERY_POLLINATION_PROTOCOL_ACT_VAL';