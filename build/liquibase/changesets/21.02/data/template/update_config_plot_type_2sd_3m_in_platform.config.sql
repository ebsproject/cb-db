--liquibase formatted sql

--changeset postgres:update_config_plot_type_2sd_3m_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update config PLOT_TYPE_2SD_3M in platform.config



UPDATE
    platform.config
SET
    config_value =
'
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 2,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_BEDS_PER_PLOT"
        },
        {
            "unit": "m",
            "default": 2,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_ROWS_PER_BED"
        },
        {
            "unit": "m",
            "default": 0.4,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "BED_WIDTH"
        },
        {
            "unit": "m",
            "default": 0.8,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH_MAIZE_BED"
        },
        {
            "unit": "m",
            "default": 3,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_LN"
        },
        {
            "unit": "sqm",
            "default": 2.4,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_4"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
'
WHERE
    abbrev = 'PLOT_TYPE_2SD_3M';



--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value=
--rollback '
--rollback {
--rollback     "Name": "Required experiment level protocol plot type variables",
--rollback     "Values": [
--rollback         {
--rollback             "default": 2,
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "field_label": "No of beds per plot",
--rollback             "variable_abbrev": "NO_OF_BEDS_PER_PLOT",
--rollback             "field_description": "Number of beds per plot"
--rollback         },
--rollback         {
--rollback             "unit": "m",
--rollback             "default": 2,
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "field_label": "No of rows per bed",
--rollback             "variable_abbrev": "NO_OF_ROWS_PER_BED",
--rollback             "field_description": "Number of rows per bed"
--rollback         },
--rollback         {
--rollback             "unit": "m",
--rollback             "default": 0.4,
--rollback             "computed": "computed",
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "field_label": "Bed width",
--rollback             "variable_abbrev": "BED_WIDTH",
--rollback             "field_description": "Bed width"
--rollback         },
--rollback         {
--rollback             "unit": "m",
--rollback             "default": 0.8,
--rollback             "computed": "computed",
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "field_label": "Plot width",
--rollback             "variable_abbrev": "PLOT_WIDTH_MAIZE_BED",
--rollback             "field_description": "Plot width"
--rollback         },
--rollback         {
--rollback             "unit": "m",
--rollback             "default": 3,
--rollback             "computed": "computed",
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "field_label": "Bed length",
--rollback             "variable_abbrev": "PLOT_LN",
--rollback             "field_description": "Plot length"
--rollback         },
--rollback         {
--rollback             "unit": "sqm",
--rollback             "default": 2.4,
--rollback             "computed": "computed",
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "field_label": "Plot area",
--rollback             "variable_abbrev": "PLOT_AREA_4",
--rollback             "field_description": "Plot area"
--rollback         },
--rollback         {
--rollback             "unit": "m",
--rollback             "default": false,
--rollback             "disabled": false,
--rollback             "required": "required",
--rollback             "field_label": "Alley length",
--rollback             "variable_abbrev": "ALLEY_LENGTH",
--rollback             "field_description": "Alley length"
--rollback         },
--rollback         {
--rollback             "unit": "m",
--rollback             "default": false,
--rollback             "disabled": false,
--rollback             "required": false,
--rollback             "field_label": "Seeding Rate",
--rollback             "allow_new_val": true,
--rollback             "variable_abbrev": "SEEDING_RATE",
--rollback             "field_description": "Seeding Rate"
--rollback         }
--rollback     ]
--rollback }
--rollback '
--rollback WHERE
--rollback     abbrev='PLOT_TYPE_2SD_3M';