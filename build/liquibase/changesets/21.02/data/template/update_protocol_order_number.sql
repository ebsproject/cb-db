--liquibase formatted sql

--changeset postgres:update_breeding_trial_pollination_order_number context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update breeding trial pollination order number



UPDATE
    master.item_relation SET order_number = 2 
WHERE  
   root_id = (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_DATA_PROCESS')
AND 
    parent_id = (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_PROTOCOL_ACT')
AND 
    child_id = (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_POLLINATION_PROTOCOL_ACT');



--rollback UPDATE
--rollback     master.item_relation SET order_number = 2 
--rollback WHERE  
--rollback    root_id = (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_DATA_PROCESS')
--rollback AND 
--rollback     parent_id = (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_PROTOCOL_ACT')
--rollback AND 
--rollback     child_id = (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_POLLINATION_PROTOCOL_ACT');



--changeset postgres:update_breeding_trial_traits_order_number context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update breeding trial traits order number



UPDATE
    master.item_relation SET order_number = 3
WHERE  
   root_id = (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_DATA_PROCESS')
AND 
    parent_id = (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_PROTOCOL_ACT')
AND 
    child_id = (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_TRAITS_PROTOCOL_ACT');



--rollback UPDATE
--rollback     master.item_relation SET order_number = 2
--rollback WHERE  
--rollback    root_id = (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_DATA_PROCESS')
--rollback AND 
--rollback     parent_id = (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_PROTOCOL_ACT')
--rollback AND 
--rollback     child_id = (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_TRAITS_PROTOCOL_ACT');



--changeset postgres:update_breeding_trial_management_order_number context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update breeding trial management order number



UPDATE
    master.item_relation SET order_number = 4
WHERE  
   root_id = (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_DATA_PROCESS')
AND 
    parent_id = (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_PROTOCOL_ACT')
AND 
    child_id = (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_MANAGEMENT_PROTOCOL_ACT');



--rollback UPDATE
--rollback     master.item_relation SET order_number = 4
--rollback WHERE  
--rollback    root_id = (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_DATA_PROCESS')
--rollback AND 
--rollback     parent_id = (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_PROTOCOL_ACT')
--rollback AND 
--rollback     child_id = (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_MANAGEMENT_PROTOCOL_ACT');



--changeset postgres:update_breeding_trial_harvest_order_number context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update breeding trial harvest order number



UPDATE
    master.item_relation SET order_number = 5
WHERE  
   root_id = (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_DATA_PROCESS')
AND 
    parent_id = (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_PROTOCOL_ACT')
AND 
    child_id = (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_HARVEST_PROTOCOL_ACT');



--rollback UPDATE
--rollback     master.item_relation SET order_number = 3
--rollback WHERE  
--rollback    root_id = (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_DATA_PROCESS')
--rollback AND 
--rollback     parent_id = (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_PROTOCOL_ACT')
--rollback AND 
--rollback     child_id = (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_HARVEST_PROTOCOL_ACT');



--changeset postgres:update_intentional_crossing_nursery_pollination_order_number context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update intentional crossing nursery pollination order number



UPDATE
    master.item_relation SET order_number = 2
WHERE  
   root_id = (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS')
AND 
    parent_id = (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT')
AND 
    child_id = (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_POLLINATION_PROTOCOL_ACT');



--rollback UPDATE
--rollback     master.item_relation SET order_number = 2
--rollback WHERE  
--rollback    root_id = (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS')
--rollback AND 
--rollback     parent_id = (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT')
--rollback AND 
--rollback     child_id = (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_POLLINATION_PROTOCOL_ACT');



--changeset postgres:update_intentional_crossing_nursery_traits_order_number context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update intentional crossing nursery traits order number



UPDATE
    master.item_relation SET order_number = 3
WHERE  
   root_id = (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS')
AND 
    parent_id = (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT')
AND 
    child_id = (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_TRAITS_PROTOCOL_ACT');



--rollback UPDATE
--rollback     master.item_relation SET order_number = 2
--rollback WHERE  
--rollback    root_id = (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS')
--rollback AND 
--rollback     parent_id = (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT')
--rollback AND 
--rollback     child_id = (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_TRAITS_PROTOCOL_ACT');



--changeset postgres:update_intentional_crossing_nursery_management_order_number context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update intentional crossing nursery management order number



UPDATE
    master.item_relation SET order_number = 3
WHERE  
   root_id = (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS')
AND 
    parent_id = (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT')
AND 
    child_id = (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_MANAGEMENT_PROTOCOL_ACT');



--rollback UPDATE
--rollback     master.item_relation SET order_number = 4
--rollback WHERE  
--rollback    root_id = (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS')
--rollback AND 
--rollback     parent_id = (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT')
--rollback AND 
--rollback     child_id = (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_MANAGEMENT_PROTOCOL_ACT');



--changeset postgres:update_intentional_crossing_nursery_harvest_order_number context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update intentional crossing nursery harvest order number



UPDATE
    master.item_relation SET order_number = 5
WHERE  
   root_id = (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS')
AND 
    parent_id = (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT')
AND 
    child_id = (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT');



--rollback UPDATE
--rollback     master.item_relation SET order_number = 3
--rollback WHERE  
--rollback    root_id = (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS')
--rollback AND 
--rollback     parent_id = (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT')
--rollback AND 
--rollback     child_id = (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT');



--changeset postgres:update_generation_nursery_pollination_order_number context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update generation nursery pollination order number



UPDATE
    master.item_relation SET order_number = 2
WHERE  
   root_id = (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_DATA_PROCESS')
AND 
    parent_id = (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_PROTOCOL_ACT')
AND 
    child_id = (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_POLLINATION_PROTOCOL_ACT');



--rollback UPDATE
--rollback     master.item_relation SET order_number = 2
--rollback WHERE  
--rollback    root_id = (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_DATA_PROCESS')
--rollback AND 
--rollback     parent_id = (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_PROTOCOL_ACT')
--rollback AND 
--rollback     child_id = (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_POLLINATION_PROTOCOL_ACT');



--changeset postgres:update_generation_nursery_traits_order_number context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update generation nursery traits order number



UPDATE
    master.item_relation SET order_number = 3
WHERE  
   root_id = (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_DATA_PROCESS')
AND 
    parent_id = (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_PROTOCOL_ACT')
AND 
    child_id = (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_TRAITS_PROTOCOL_ACT');



--rollback UPDATE
--rollback     master.item_relation SET order_number = 2
--rollback WHERE  
--rollback    root_id = (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_DATA_PROCESS')
--rollback AND 
--rollback     parent_id = (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_PROTOCOL_ACT')
--rollback AND 
--rollback     child_id = (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_TRAITS_PROTOCOL_ACT');



--changeset postgres:update_generation_nursery_management_order_number context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update generation nursery management order number



UPDATE
    master.item_relation SET order_number = 4
WHERE  
   root_id = (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_DATA_PROCESS')
AND 
    parent_id = (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_PROTOCOL_ACT')
AND 
    child_id = (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_MANAGEMENT_PROTOCOL_ACT');



--rollback UPDATE
--rollback     master.item_relation SET order_number = 4
--rollback WHERE  
--rollback    root_id = (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_DATA_PROCESS')
--rollback AND 
--rollback     parent_id = (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_PROTOCOL_ACT')
--rollback AND 
--rollback     child_id = (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_MANAGEMENT_PROTOCOL_ACT');



--changeset postgres:update_generation_nursery_harvest_order_number context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update generation nursery harvest order number



UPDATE
    master.item_relation SET order_number = 5
WHERE  
   root_id = (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_DATA_PROCESS')
AND 
    parent_id = (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_PROTOCOL_ACT')
AND 
    child_id = (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_HARVEST_PROTOCOL_ACT');



--rollback UPDATE
--rollback     master.item_relation SET order_number = 3
--rollback WHERE  
--rollback    root_id = (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_DATA_PROCESS')
--rollback AND 
--rollback     parent_id = (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_PROTOCOL_ACT')
--rollback AND 
--rollback     child_id = (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_HARVEST_PROTOCOL_ACT');



--changeset postgres:update_cross_parent_nursery_pi_pollination_order_number context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update cross parent nursery pi pollination order number



UPDATE
    master.item_relation SET order_number = 2
WHERE  
   root_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_DATA_PROCESS')
AND 
    parent_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_PROTOCOL_ACT')
AND 
    child_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_POLLINATION_PROTOCOL_ACT');



--rollback UPDATE
--rollback     master.item_relation SET order_number = 2
--rollback WHERE  
--rollback    root_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_DATA_PROCESS')
--rollback AND 
--rollback     parent_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_PROTOCOL_ACT')
--rollback AND 
--rollback     child_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_POLLINATION_PROTOCOL_ACT');



--changeset postgres:update_cross_parent_nursery_pi_traits_order_number context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update cross parent nursery pi traits order number



UPDATE
    master.item_relation SET order_number = 3
WHERE  
   root_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_DATA_PROCESS')
AND 
    parent_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_PROTOCOL_ACT')
AND 
    child_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_TRAITS_PROTOCOL_ACT');



--rollback UPDATE
--rollback     master.item_relation SET order_number = 2
--rollback WHERE  
--rollback    root_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_DATA_PROCESS')
--rollback AND 
--rollback     parent_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_PROTOCOL_ACT')
--rollback AND 
--rollback     child_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_TRAITS_PROTOCOL_ACT');



--changeset postgres:update_cross_parent_nursery_pi_management_order_number context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update cross parent nursery pi management order number



UPDATE
    master.item_relation SET order_number = 4
WHERE  
   root_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_DATA_PROCESS')
AND 
    parent_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_PROTOCOL_ACT')
AND 
    child_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_MANAGEMENT_PROTOCOL_ACT');



--rollback UPDATE
--rollback     master.item_relation SET order_number = 4
--rollback WHERE  
--rollback    root_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_DATA_PROCESS')
--rollback AND 
--rollback     parent_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_PROTOCOL_ACT')
--rollback AND 
--rollback     child_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_MANAGEMENT_PROTOCOL_ACT');



--changeset postgres:update_cross_parent_nursery_pi_harvest_order_number context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update cross parent nursery pi harvest order number



UPDATE
    master.item_relation SET order_number = 5
WHERE  
   root_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_DATA_PROCESS')
AND 
    parent_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_PROTOCOL_ACT')
AND 
    child_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_HARVEST_PROTOCOL_ACT');



--rollback UPDATE
--rollback     master.item_relation SET order_number = 3
--rollback WHERE  
--rollback    root_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_DATA_PROCESS')
--rollback AND 
--rollback     parent_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_PROTOCOL_ACT')
--rollback AND 
--rollback     child_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_HARVEST_PROTOCOL_ACT');



--changeset postgres:update_cross_parent_nursery_pii_pollination_order_number context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update cross parent nursery pii pollination order number



UPDATE
    master.item_relation SET order_number = 2
WHERE  
   root_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS')
AND 
    parent_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PROTOCOL_ACT')
AND 
    child_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_POLLINATION_PROTOCOL_ACT');



--rollback UPDATE
--rollback     master.item_relation SET order_number = 2
--rollback WHERE  
--rollback    root_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS')
--rollback AND 
--rollback     parent_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PROTOCOL_ACT')
--rollback AND 
--rollback     child_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_POLLINATION_PROTOCOL_ACT');



--changeset postgres:update_cross_parent_nursery_pii_traits_order_number context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update cross parent nursery pii traits order number



UPDATE
    master.item_relation SET order_number = 3
WHERE  
   root_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS')
AND 
    parent_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PROTOCOL_ACT')
AND 
    child_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_TRAITS_PROTOCOL_ACT');



--rollback UPDATE
--rollback     master.item_relation SET order_number = 2
--rollback WHERE  
--rollback    root_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS')
--rollback AND 
--rollback     parent_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PROTOCOL_ACT')
--rollback AND 
--rollback     child_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_TRAITS_PROTOCOL_ACT');



--changeset postgres:update_cross_parent_nursery_pii_management_order_number context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update cross parent nursery pii management order number



UPDATE
    master.item_relation SET order_number = 4
WHERE  
   root_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS')
AND 
    parent_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PROTOCOL_ACT')
AND 
    child_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_MANAGEMENT_PROTOCOL_ACT');



--rollback UPDATE
--rollback     master.item_relation SET order_number = 4
--rollback WHERE  
--rollback    root_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS')
--rollback AND 
--rollback     parent_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PROTOCOL_ACT')
--rollback AND 
--rollback     child_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_MANAGEMENT_PROTOCOL_ACT');



--changeset postgres:update_cross_parent_nursery_pii_harvest_order_number context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update cross parent nursery pii harvest order number



UPDATE
    master.item_relation SET order_number = 5
WHERE  
   root_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS')
AND 
    parent_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PROTOCOL_ACT')
AND 
    child_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_HARVEST_PROTOCOL_ACT');



--rollback UPDATE
--rollback     master.item_relation SET order_number = 3
--rollback WHERE  
--rollback    root_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS')
--rollback AND 
--rollback     parent_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PROTOCOL_ACT')
--rollback AND 
--rollback     child_id = (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_HARVEST_PROTOCOL_ACT');