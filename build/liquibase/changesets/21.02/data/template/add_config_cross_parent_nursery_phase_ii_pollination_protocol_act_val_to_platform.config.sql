--liquibase formatted sql

--changeset postgres:add_config_cross_parent_nursery_phase_ii_pollination_protocol_act_val_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Add config CROSS_PARENT_NURSERY_PHASE_II_POLLINATION_PROTOCOL_ACT_VAL to platform.config



INSERT INTO 
  platform.config (abbrev, name, config_value, rank, usage, creator_id, notes)
VALUES 
  (
    'CROSS_PARENT_NURSERY_PHASE_II_POLLINATION_PROTOCOL_ACT_VAL',
    'Cross Parent Nursery Phase II Pollination Protocol variables', 
    '{
        "Name": "Required experiment level pollination protocol variables for Cross Parent Nursery Phase II data process",
        "Values": [
          {
                "default": false,
                "disabled": false,
                "variable_abbrev": "POLLINATION_INSTRUCTIONS"
            }
        ]
    }'::json,
    1,
    'experiment_creation',
    1,
    'added by j.antonio'
  );



--rollback DELETE FROM platform.config WHERE abbrev='CROSS_PARENT_NURSERY_PHASE_II_POLLINATION_PROTOCOL_ACT_VAL';