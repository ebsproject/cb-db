--liquibase formatted sql

--changeset postgres:update_config_plot_type_6ss_5m_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update config PLOT_TYPE_6SS_5M in platform.config



UPDATE
    platform.config
SET
    config_value =
'
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 6,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "ROWS_PER_PLOT_CONT"
            
        },
        {
            "unit": "m",
            "default": 0.18,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "DIST_BET_ROWS"
            
        },
        {
            "unit": "m",
            "default": 0.4,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "DISTANCE_BET_PLOTS_IN_M"
            
        },
        {
            "unit": "m",
            "default": 1.3,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "PLOT_WIDTH"
            
        },
        {
            "unit": "m",
            "default": 5,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "PLOT_LN_1"
            
        },
        {
            "unit": "sqm",
            "default": 6.5,
            "disabled": true,
            "required": "required",
            
             "variable_abbrev": "PLOT_AREA_2"
            
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "ALLEY_LENGTH"
            
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            
            "allow_new_val": true,
             "variable_abbrev": "SEEDING_RATE"
            
        },
           {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
'
WHERE
    abbrev = 'PLOT_TYPE_6SS_5M';



--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value=
--rollback '
--rollback {
--rollback     "Name": "Required experiment level protocol plot type variables",
--rollback     "Values": [
--rollback         {
--rollback             "default": 6,
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "field_label": "Rows per plot",
--rollback             "variable_abbrev": "ROWS_PER_PLOT_CONT",
--rollback             "field_description": "Number of rows per plot"
--rollback         },
--rollback         {
--rollback             "unit": "m",
--rollback             "default": 0.18,
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "field_label": "Dist. bet. rows",
--rollback             "variable_abbrev": "DIST_BET_ROWS",
--rollback             "field_description": "Distance between rows"
--rollback         },
--rollback         {
--rollback             "unit": "m",
--rollback             "default": 0.4,
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "field_label": "Dist. bet. plots",
--rollback             "variable_abbrev": "DISTANCE_BET_PLOTS_IN_M",
--rollback             "field_description": "Distance betweem plots"
--rollback         },
--rollback         {
--rollback             "unit": "m",
--rollback             "default": 1.3,
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "field_label": "Plot width",
--rollback             "variable_abbrev": "PLOT_WIDTH",
--rollback             "field_description": "Plot width"
--rollback         },
--rollback         {
--rollback             "unit": "m",
--rollback             "default": 5,
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "field_label": "Plot length",
--rollback             "variable_abbrev": "PLOT_LN_1",
--rollback             "field_description": "Plot length"
--rollback         },
--rollback         {
--rollback             "unit": "sqm",
--rollback             "default": 6.5,
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "field_label": "Plot area",
--rollback             "variable_abbrev": "PLOT_AREA_2",
--rollback             "field_description": "Plot area"
--rollback         },
--rollback         {
--rollback             "unit": "m",
--rollback             "default": false,
--rollback             "disabled": false,
--rollback             "required": "required",
--rollback             "field_label": "Alley length",
--rollback             "variable_abbrev": "ALLEY_LENGTH",
--rollback             "field_description": "Alley length"
--rollback         },
--rollback         {
--rollback             "unit": "m",
--rollback             "default": false,
--rollback             "disabled": false,
--rollback             "required": false,
--rollback             "field_label": "Seeding Rate",
--rollback             "allow_new_val": true,
--rollback             "variable_abbrev": "SEEDING_RATE",
--rollback             "field_description": "Seeding Rate"
--rollback         }
--rollback     ]
--rollback }
--rollback '
--rollback WHERE
--rollback     abbrev='PLOT_TYPE_6SS_5M';