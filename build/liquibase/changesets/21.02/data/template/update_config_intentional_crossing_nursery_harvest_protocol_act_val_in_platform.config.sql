--liquibase formatted sql

--changeset postgres:update_config_intentional_crossing_nursery_harvest_protocol_act_val_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update config INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT_VAL in platform.config



UPDATE 
    platform.config 
SET 
    config_value = '{
        "Name": "Required experiment level harvest protocol variables for Intentional Crossing Nursery data process",
        "Values": [{
                "default": false,
                "disabled": false,
                "variable_abbrev": "HV_METH_DISC"
            },{
                "default": false,
                "disabled": false,
                "variable_abbrev": "HARVEST_INSTRUCTIONS"
            }
        ]
    }' 
WHERE 
    abbrev = 'INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT_VAL';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "Name": "Required experiment level harvest protocol variables for Intentional Crossing Nursery data process",
--rollback             "Values": [
--rollback                 {
--rollback                     "default": false,
--rollback                     "disabled": false,
--rollback                     "variable_abbrev": "HV_METH_DISC"
--rollback                 },
--rollback                 {
--rollback                     "default": false,
--rollback                     "disabled": false,
--rollback                     "variable_abbrev": "REMARKS"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE 
--rollback     abbrev = 'INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT_VAL';