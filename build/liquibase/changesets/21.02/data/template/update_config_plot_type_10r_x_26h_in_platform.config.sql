--liquibase formatted sql

--changeset postgres:update_config_plot_type_10r_x_26h_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update config PLOT_TYPE_10R_X_26H in platform.config



UPDATE
    platform.config
SET
    config_value =
'
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 10,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "ROWS_PER_PLOT_CONT"
        },
        {
            "default": 26,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "HILLS_PER_ROW_CONT"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "DIST_BET_ROWS"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "DIST_BET_HILLS"
        },
        {
            "unit": "m",
            "default": 5,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_LN_1"
        },
        {
            "unit": "m",
            "default": 2,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH_RICE"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_1"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
'
WHERE
    abbrev = 'PLOT_TYPE_10R_X_26H';



--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value=
--rollback '
--rollback {
--rollback     "Name": "Required experiment level protocol plot type variables",
--rollback     "Values": [
--rollback         {
--rollback             "default": 10,
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "field_label": "Rows per plot",
--rollback             "variable_abbrev": "ROWS_PER_PLOT_CONT",
--rollback             "field_description": "Number of rows per plot"
--rollback         },
--rollback         {
--rollback             "default": 26,
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "field_label": "Hills per row",
--rollback             "variable_abbrev": "HILLS_PER_ROW_CONT",
--rollback             "field_description": "Number of hills per row"
--rollback         },
--rollback         {
--rollback             "unit": "cm",
--rollback             "default": 20,
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "field_label": "Dist. bet. rows",
--rollback             "variable_abbrev": "DIST_BET_ROWS",
--rollback             "field_description": "Distance between rows"
--rollback         },
--rollback         {
--rollback             "unit": "cm",
--rollback             "default": 20,
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "field_label": "Dist. bet. hills",
--rollback             "variable_abbrev": "DIST_BET_HILLS",
--rollback             "field_description": "Distance between hills"
--rollback         },
--rollback         {
--rollback             "unit": "m",
--rollback             "default": 5,
--rollback             "computed": "computed",
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "field_label": "Plot length",
--rollback             "variable_abbrev": "PLOT_LN_1",
--rollback             "field_description": "Plot length"
--rollback         },
--rollback         {
--rollback             "unit": "m",
--rollback             "default": 2,
--rollback             "computed": "computed",
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "field_label": "Plot width",
--rollback             "variable_abbrev": "PLOT_WIDTH_RICE",
--rollback             "field_description": "Plot width"
--rollback         },
--rollback         {
--rollback             "unit": "sqm",
--rollback             "default": false,
--rollback             "computed": "computed",
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "field_label": "Plot Area",
--rollback             "variable_abbrev": "PLOT_AREA_1",
--rollback             "field_description": "Plot Area"
--rollback         },
--rollback         {
--rollback             "unit": "m",
--rollback             "default": false,
--rollback             "disabled": false,
--rollback             "required": "required",
--rollback             "field_label": "Alley length",
--rollback             "variable_abbrev": "ALLEY_LENGTH",
--rollback             "field_description": "Alley length"
--rollback         },
--rollback         {
--rollback             "unit": "m",
--rollback             "default": false,
--rollback             "disabled": false,
--rollback             "required": false,
--rollback             "field_label": "Seeding Rate",
--rollback             "allow_new_val": true,
--rollback             "variable_abbrev": "SEEDING_RATE",
--rollback             "field_description": "Seeding Rate"
--rollback         }
--rollback     ]
--rollback }
--rollback '
--rollback WHERE
--rollback     abbrev='PLOT_TYPE_10R_X_26H';