--liquibase formatted sql

--changeset postgres:update_variable_type_in_master.variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-3 Update variable type in master.variable



UPDATE
    master.variable
SET
    type = 'metadata'
WHERE 
    abbrev 
IN
    ('HARVEST_INSTRUCTIONS','POLLINATION_INSTRUCTIONS');



--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     type = 'identification'
--rollback WHERE 
--rollback     abbrev 
--rollback IN
--rollback     ('HARVEST_INSTRUCTIONS','POLLINATION_INSTRUCTIONS');