--liquibase formatted sql

--changeset postgres:add_planted_scale_value_for_location_status context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-29 Add planted scale value for LOCATION_STATUS



INSERT INTO 
    master.scale_value 
        (scale_id,value,order_number,description,display_name,abbrev,creator_id)
VALUES
    ((SELECT scale_id FROM master.variable WHERE abbrev='LOCATION_STATUS'),'mapped',4,'mapped','mapped','LOCATION_STATUS_MAPPED',1),
    ((SELECT scale_id FROM master.variable WHERE abbrev='LOCATION_STATUS'),'planted',5,'planted','planted','LOCATION_STATUS_PLANTED',1);



--rollback DELETE FROM master.scale_value WHERE abbrev IN ('LOCATION_STATUS_MAPPED','LOCATION_STATUS_PLANTED');