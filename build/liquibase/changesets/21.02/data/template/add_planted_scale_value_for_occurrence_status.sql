--liquibase formatted sql

--changeset postgres:add_planted_scale_value_for_occurrence_status context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-29 Add planted scale value for OCCURRENCE_STATUS



INSERT INTO 
    master.scale_value 
        (scale_id,value,order_number,description,display_name,abbrev,creator_id)
VALUES
    ((SELECT scale_id FROM master.variable WHERE abbrev='OCCURRENCE_STATUS'),'planted',5,'planted','planted','OCCURRENCE_STATUS_PLANTED',1);



--rollback DELETE FROM master.scale_value WHERE abbrev='OCCURRENCE_STATUS_PLANTED';