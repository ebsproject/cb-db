--liquibase formatted sql

--changeset postgres:update_config_harvest_manager_plot_browser_config_rice_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update config HARVEST_MANAGER_PLOT_BROWSER_CONFIG_RICE in platform.config



UPDATE 
    platform.config 
SET 
    config_value = 
'
{
    "name": "HARVEST_MANAGER_PLOT_BROWSER_CONFIG_RICE",
    "values": [
        {
            "germplasm_states": {
                "fixed": {
                    "methods": [
                        "Bulk"
                    ],
                    "display_columns": [
                        "harvestDate",
                        "harvestMethod"
                    ]
                },
                "not_fixed": {
                    "methods": [
                        "Bulk",
                        "Single Plant Selection"
                    ],
                    "display_columns": [
                        "harvestDate",
                        "harvestMethod",
                        "numericVar"
                    ]
                }
            }
        }
    ]
}
' 
WHERE abbrev = 'HARVEST_MANAGER_PLOT_BROWSER_CONFIG_RICE';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback '
--rollback {
--rollback     "name": "HARVEST_MANAGER_PLOT_BROWSER_CONFIG_RICE",
--rollback     "values": [
--rollback         {
--rollback             "germplasm_states": {
--rollback                 "fixed": {
--rollback                     "methods": [
--rollback                         "Bulk"
--rollback                     ],
--rollback                     "display_columns": [
--rollback                         "harvestDate",
--rollback                         "harvestMethod"
--rollback                     ]
--rollback                 },
--rollback                 "not_fixed": {
--rollback                     "methods": [
--rollback                         "Bulk",
--rollback                         "Bulk fixedline",
--rollback                         "Panicle Selection",
--rollback                         "Single Plant Selection",
--rollback                         "Single Plant Selection and Bulk",
--rollback                         "plant-specific",
--rollback                         "Plant-Specific and Bulk",
--rollback                         "Single Seed Descent"
--rollback                     ],
--rollback                     "display_columns": [
--rollback                         "harvestDate",
--rollback                         "harvestMethod",
--rollback                         "numericVar"
--rollback                     ]
--rollback                 }
--rollback             }
--rollback         }
--rollback     ]
--rollback }
--rollback '
--rollback WHERE abbrev = 'HARVEST_MANAGER_PLOT_BROWSER_CONFIG_RICE';