--liquibase formatted sql

--changeset postgres:update_usage_of_variables_used_in_management_in_the_variable_table_part_2 context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-13 Identify variables used in management and update their usage in the variable table Part 2



-- change
UPDATE
    master.variable AS var
SET
    usage = 'management'
WHERE
    var.abbrev IN (
        'SEEDING_DATE_CONT',
        'DIST_BET_ROWS',
        'ROWS_PER_PLOT_CONT',
        'FERT1_METH_DISC',
        'FERT1_TYPE_DISC',
        'FERT1_DATE_CONT',
        'FERT1_BRAND',
        'FERT2_METH_DISC',
        'FERT2_TYPE_DISC',
        'FERT2_DATE_CONT',
        'FERT2_BRAND',
        'FERT3_METH_DISC',
        'FERT3_TYPE_DISC',
        'FERT3_DATE_CONT',
        'FERT3_BRAND'
    )
    AND var.is_void = FALSE
;



-- rollback
--rollback UPDATE
--rollback     master.variable AS var
--rollback SET
--rollback     usage = 'study'
--rollback WHERE
--rollback     var.abbrev IN (
--rollback         'SEEDING_DATE_CONT',
--rollback         'DIST_BET_ROWS',
--rollback         'ROWS_PER_PLOT_CONT',
--rollback         'FERT1_METH_DISC',
--rollback         'FERT1_TYPE_DISC',
--rollback         'FERT1_DATE_CONT',
--rollback         'FERT1_BRAND',
--rollback         'FERT2_METH_DISC',
--rollback         'FERT2_TYPE_DISC',
--rollback         'FERT2_DATE_CONT',
--rollback         'FERT2_BRAND',
--rollback         'FERT3_METH_DISC',
--rollback         'FERT3_TYPE_DISC',
--rollback         'FERT3_DATE_CONT',
--rollback         'FERT3_BRAND'
--rollback     )
--rollback     AND var.is_void = FALSE
--rollback ;