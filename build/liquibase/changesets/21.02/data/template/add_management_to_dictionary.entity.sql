--liquibase formatted sql

--changeset postgres:add_management_to_dictionary.entity context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-28 Add management to dictionary.entity



INSERT INTO
    dictionary.entity
        (abbrev,name,description,table_id,creator_id)
VALUES
    (
        'MANAGEMENT',
        'Management',
        'Management',
        (SELECT id FROM dictionary.table WHERE abbrev='VARIABLE'),
        1
    );



--rollback DELETE FROM dictionary.entity WHERE abbrev='MANAGEMENT';