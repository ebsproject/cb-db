--liquibase formatted sql

--changeset postgres:update_config_hm_seed_name_pattern_rice_default_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update config HM_SEED_NAME_PATTERN_RICE_DEFAULT in platform.config



UPDATE 
    platform.config 
SET 
    config_value = 
'
{
	"default": {
	  "default" : {
        "default" : [
          {
            "type" : "field",
            "entity" : "germplasm",
            "field_name" : "designation",
            "order_number":0
          }
        ]
      }
    }
}
' 
WHERE abbrev = 'HM_SEED_NAME_PATTERN_RICE_DEFAULT';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback '
--rollback {
--rollback     "bulk": [
--rollback         {
--rollback             "type": "field",
--rollback             "label": "Source Nursery Site Code",
--rollback             "order_number": 0,
--rollback             "plotInfoField": "nurserySiteCode"
--rollback         },
--rollback         {
--rollback             "type": "field",
--rollback             "label": "Experiment Year - YY",
--rollback             "order_number": 1,
--rollback             "plotInfoField": "experimentYearYY"
--rollback         },
--rollback         {
--rollback             "type": "field",
--rollback             "label": "Source Season Code",
--rollback             "order_number": 2,
--rollback             "plotInfoField": "seasonCode"
--rollback         },
--rollback         {
--rollback             "type": "delimeter",
--rollback             "value": "/",
--rollback             "order_number": 3
--rollback         },
--rollback         {
--rollback             "type": "field",
--rollback             "label": "Occurrence Code",
--rollback             "order_number": 4,
--rollback             "plotInfoField": "occurrenceCode"
--rollback         },
--rollback         {
--rollback             "type": "delimeter",
--rollback             "value": "/",
--rollback             "order_number": 5
--rollback         },
--rollback         {
--rollback             "type": "field",
--rollback             "label": "Source Entry Number",
--rollback             "order_number": 6,
--rollback             "plotInfoField": "entryNumber"
--rollback         }
--rollback     ],
--rollback     "default": [
--rollback         {
--rollback             "type": "field",
--rollback             "label": "Source Nursery Site Code",
--rollback             "order_number": 0,
--rollback             "plotInfoField": "nurserySiteCode"
--rollback         },
--rollback         {
--rollback             "type": "field",
--rollback             "label": "Experiment Year - YY",
--rollback             "order_number": 1,
--rollback             "plotInfoField": "experimentYearYY"
--rollback         },
--rollback         {
--rollback             "type": "field",
--rollback             "label": "Source Season Code",
--rollback             "order_number": 2,
--rollback             "plotInfoField": "seasonCode"
--rollback         },
--rollback         {
--rollback             "type": "delimeter",
--rollback             "value": "/",
--rollback             "order_number": 3
--rollback         },
--rollback         {
--rollback             "type": "field",
--rollback             "label": "Occurrence Code",
--rollback             "order_number": 4,
--rollback             "plotInfoField": "occurrenceCode"
--rollback         },
--rollback         {
--rollback             "type": "delimeter",
--rollback             "value": "/",
--rollback             "order_number": 5
--rollback         },
--rollback         {
--rollback             "type": "field",
--rollback             "label": "Source Entry Number",
--rollback             "order_number": 6,
--rollback             "plotInfoField": "entryNumber"
--rollback         }
--rollback     ],
--rollback     "individual_ear": [
--rollback         {
--rollback             "type": "field",
--rollback             "label": "Source Nursery Site Code",
--rollback             "order_number": 0,
--rollback             "plotInfoField": "nurserySiteCode"
--rollback         },
--rollback         {
--rollback             "type": "field",
--rollback             "label": "Experiment Year - YY",
--rollback             "order_number": 1,
--rollback             "plotInfoField": "experimentYearYY"
--rollback         },
--rollback         {
--rollback             "type": "field",
--rollback             "label": "Source Season Code",
--rollback             "order_number": 2,
--rollback             "plotInfoField": "seasonCode"
--rollback         },
--rollback         {
--rollback             "type": "delimeter",
--rollback             "value": "/",
--rollback             "order_number": 3
--rollback         },
--rollback         {
--rollback             "type": "field",
--rollback             "label": "Occurrence Code",
--rollback             "order_number": 4,
--rollback             "plotInfoField": "occurrenceCode"
--rollback         },
--rollback         {
--rollback             "type": "delimeter",
--rollback             "value": "-",
--rollback             "order_number": 5
--rollback         },
--rollback         {
--rollback             "type": "counter",
--rollback             "order_number": 6
--rollback         }
--rollback     ]
--rollback }
--rollback '
--rollback WHERE abbrev = 'HM_SEED_NAME_PATTERN_RICE_DEFAULT';