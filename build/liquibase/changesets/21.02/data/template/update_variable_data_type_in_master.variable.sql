--liquibase formatted sql

--changeset postgres:update_variable_data_type_in_master.variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-4 Update variable data_type in master.variable



UPDATE
    master.variable
SET
    data_type = 'text'
WHERE 
    abbrev 
IN
    ('POLLINATION_INSTRUCTIONS','HARVEST_INSTRUCTIONS','PLANTING_INSTRUCTIONS');



--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     data_type = 'character varying'
--rollback WHERE 
--rollback     abbrev 
--rollback IN
--rollback     ('POLLINATION_INSTRUCTIONS','HARVEST_INSTRUCTIONS','PLANTING_INSTRUCTIONS');