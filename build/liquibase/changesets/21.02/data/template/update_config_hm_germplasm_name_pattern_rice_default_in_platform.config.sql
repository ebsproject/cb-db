--liquibase formatted sql

--changeset postgres:update_config_hm_germplasm_name_pattern_rice_default_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-16 Update config HM_GERMPLASM_NAME_PATTERN_RICE_DEFAULT in platform.config



UPDATE 
    platform.config 
SET 
    config_value = 
'
{
	"default": {
	  "default" : {
        "default" : [
          {
            "type" : "free-text",
            "value" : "IR XXXXX",
            "order_number" : 0
          }
        ]
      }
    },
    "breeding trial" : {
      "default" : {
        "default" : [
          {
            "type" : "free-text",
            "value" : "IR XXXXX",
            "order_number" : 0
          }
        ]
      },
      "progeny" : {
        "default" : [
          {
            "type" : "free-text",
            "value" : "IR XXXXX",
            "order_number" : 0
          }
        ],
        "bulk" : [
          {
            "type" : "field",
            "entity" : "plot",
            "field_name" : "germplasmDesignation",
            "order_number":0
          },
          {
            "type" : "delimiter",
            "value" : "-",
            "order_number" : 1
          },
          {
            "type" : "free-text",
            "value" : "B",
            "order_number" : 2
          }
        ]
      }
    },
    "generation nursery" : {
      "default" : {
        "default" : [
          {
            "type" : "free-text",
            "value" : "IR XXXXX",
            "order_number" : 0
          }
        ]
      },
      "progeny" : {
        "default" : [
          {
            "type" : "free-text",
            "value" : "IR XXXXX",
            "order_number" : 0
          }
        ],
        "bulk" : [
          {
            "type" : "field",
            "entity" : "plot",
            "field_name" : "germplasmDesignation",
            "order_number":0
          },
          {
            "type" : "delimiter",
            "value" : "-",
            "order_number" : 1
          },
          {
            "type" : "free-text",
            "value" : "B",
            "order_number" : 2
          }
        ],
        "single plant selection" : [
          {
            "type" : "field",
            "entity" : "plot",
            "field_name" : "germplasmDesignation",
            "order_number":0
          },
          {
            "type" : "delimiter",
            "value" : "-",
            "order_number" : 1
          },
          {
            "type" : "counter",
            "order_number" : 2
          }
        ]
      }
    }
}
'
WHERE 
    abbrev = 'HM_GERMPLASM_NAME_PATTERN_RICE_DEFAULT';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback '
--rollback {
--rollback     "default": {
--rollback         "default": {
--rollback             "default": [
--rollback                 {
--rollback                     "type": "free-text",
--rollback                     "value": "IR XXXXX",
--rollback                     "order_number": 0
--rollback                 }
--rollback             ]
--rollback         }
--rollback     },
--rollback     "breeding trial": {
--rollback         "default": {
--rollback             "default": [
--rollback                 {
--rollback                     "type": "free-text",
--rollback                     "value": "IR XXXXX",
--rollback                     "order_number": 0
--rollback                 }
--rollback             ]
--rollback         }
--rollback     },
--rollback     "generation nursery": {
--rollback         "default": {
--rollback             "default": [
--rollback                 {
--rollback                     "type": "free-text",
--rollback                     "value": "IR XXXXX",
--rollback                     "order_number": 0
--rollback                 }
--rollback             ]
--rollback         },
--rollback         "progeny": {
--rollback             "default": [
--rollback                 {
--rollback                     "type": "free-text",
--rollback                     "value": "IR XXXXX",
--rollback                     "order_number": 0
--rollback                 }
--rollback             ],
--rollback             "single plant selection": [
--rollback                 {
--rollback                     "type": "field",
--rollback                     "entity": "plot",
--rollback                     "field_name": "germplasmDesignation",
--rollback                     "order_number": 0
--rollback                 },
--rollback                 {
--rollback                     "type": "delimiter",
--rollback                     "value": "-",
--rollback                     "order_number": 1
--rollback                 },
--rollback                 {
--rollback                     "type": "counter",
--rollback                     "order_number": 2
--rollback                 }
--rollback             ]
--rollback         }
--rollback     }
--rollback }
--rollback '
--rollback WHERE 
--rollback     abbrev = 'HM_GERMPLASM_NAME_PATTERN_RICE_DEFAULT';