--liquibase formatted sql

--changeset postgres:add_management_protocol_type_to_platform.list_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-8 Add “management protocol” type to platform.list table



ALTER TABLE 
    platform.list 
DROP CONSTRAINT 
    list_type_chk;

ALTER TABLE 
    platform.list
ADD CONSTRAINT 
    list_type_chk CHECK ((type)::text = ANY (ARRAY['seed'::text, 'plot'::text, 'location'::text, 'germplasm'::text, 'study'::text, 'trait'::text, 'trait protocol'::text, 'package'::text, 'management protocol'::text]))



--rollback ALTER TABLE 
--rollback     platform.list 
--rollback DROP CONSTRAINT 
--rollback     list_type_chk;
--rollback 
--rollback ALTER TABLE 
--rollback     platform.list
--rollback ADD CONSTRAINT 
--rollback     list_type_chk CHECK ((type)::text = ANY (ARRAY['seed'::text, 'plot'::text, 'location'::text, 'germplasm'::text, 'study'::text, 'trait'::text, 'trait protocol'::text, 'package'::text]));