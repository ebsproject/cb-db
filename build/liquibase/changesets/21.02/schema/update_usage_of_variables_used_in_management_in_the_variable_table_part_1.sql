--liquibase formatted sql

--changeset postgres:update_usage_of_variables_used_in_management_in_the_variable_table_part_1 context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-13 Identify variables used in management and update their usage in the variable table Part 1



-- change
-- PART 1 Add “management” usage variable to master.variable table
ALTER TABLE
    master.variable
DROP CONSTRAINT
    variable_usage_chk;

ALTER TABLE
    master.variable
ADD CONSTRAINT
    variable_usage_chk CHECK (((usage)::text = ANY (ARRAY[('application'::character varying)::text, ('study'::character varying)::text, ('undefined'::character varying)::text, ('management'::character varying)::text])));



-- rollback
--rollback ALTER TABLE
--rollback     master.variable
--rollback DROP CONSTRAINT
--rollback     variable_usage_chk;
--rollback 
--rollback ALTER TABLE
--rollback     master.variable
--rollback ADD CONSTRAINT
--rollback     variable_usage_chk CHECK (((usage)::text = ANY (ARRAY[('application'::character varying)::text, ('study'::character varying)::text, ('undefined'::character varying)::text])));