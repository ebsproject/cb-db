--liquibase formatted sql

--changeset postgres:12_populate_tenant_protocol_rice_for_htaf context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2769 Populate tenant protocol Rice for HTAF



INSERT INTO
    tenant.protocol (
        protocol_code, protocol_name, protocol_type, program_id, creator_id
    )
SELECT
    CONCAT(UPPER(ptc.protocol_type),'_PROTOCOL_',UPPER(exp.experiment_name)) AS protocol_code,
    CONCAT(INITCAP(ptc.protocol_type),' Protocol ',UPPER(exp.experiment_name)) AS protocol_name,
    ptc.protocol_type AS protocol_type,
    exp.program_id AS program_id,
    exp.creator_id AS creator_id
FROM 
(
    SELECT 
        experiment_name,
        program_id,
        creator_id
    FROM
        experiment.experiment
    WHERE
        experiment_name = 'HTAF2023_RICE_AYT'
) AS exp,
(
    VALUES
        ('trait'),
        ('management'),
        ('planting'),
        ('harvest')
) AS ptc (protocol_type)



--rollback DELETE FROM 
--rollback     tenant.protocol 
--rollback WHERE 
--rollback     protocol_code 
--rollback IN 
--rollback     (
--rollback         SELECT
--rollback             CONCAT(UPPER(ptc.protocol_type),'_PROTOCOL_',UPPER(exp.experiment_name)) AS protocol_code
--rollback         FROM 
--rollback         (
--rollback             SELECT 
--rollback                 experiment_name,
--rollback                 program_id,
--rollback                 creator_id
--rollback             FROM
--rollback                 experiment.experiment
--rollback             WHERE
--rollback                 experiment_name = 'HTAF2023_RICE_AYT'
--rollback         ) AS exp,
--rollback         (
--rollback             VALUES
--rollback                 ('trait'),
--rollback                 ('management'),
--rollback                 ('planting'),
--rollback                 ('harvest')
--rollback         ) AS ptc (protocol_type)
--rollback 
--rollback     )
--rollback ;
