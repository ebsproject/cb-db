--liquibase formatted sql

--changeset postgres:8_populate_plot_rice_for_htaf context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2769 Populate plot Rice for HTAF



WITH t1 AS (
    SELECT
        eo.id AS occurrence_id,
        loc.id AS location_id,
        ee.id AS entry_id,
        'plot' AS plot_type,
        generate_series(1,2) AS rep,
        ee.creator_id
    FROM
        experiment.experiment exp
    INNER JOIN
        experiment.entry_list el
    ON
        el.experiment_id = exp.id
    INNER JOIN
        experiment.entry ee
    ON
        ee.entry_list_id = el.id
    INNER JOIN
        experiment.occurrence eo
    ON
        eo.experiment_id = exp.id
    INNER JOIN
        experiment.location_occurrence_group elo
    ON
        elo.occurrence_id = eo.id
    INNER JOIN
        experiment.location loc
    ON
        elo.location_id = loc.id
    WHERE
        exp.experiment_name = 'HTAF2023_RICE_AYT'
    ORDER BY
        rep,
        ee.id
), t2 AS (
    SELECT
        *,
        ROW_NUMBER() OVER() AS plot_code
    FROM
        t1 t
), t3 AS (
    SELECT 
        occurrence_id,
        location_id, 
        entry_id,
        plot_code, 
        plot_code AS plot_number, 
        plot_type, 
        rep,
        1 AS design_x,
        plot_code AS design_y,
        1 AS pa_x,
        plot_code AS pa_y,
        1 AS field_x,
        plot_code AS field_y,
        rep AS block_number,
        'active' AS plot_status,
        'Q' AS plot_qc_code,
        creator_id,
        'NO_HARVEST' AS harvest_status
    FROM
        t2 t
)
INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, pa_x, pa_y, field_x, field_y,
       block_number, plot_status, plot_qc_code, creator_id, harvest_status
   )
SELECT t.* FROM t3 t;



-- revert changes OCC
--rollback DELETE FROM
--rollback     experiment.plot AS plot
--rollback USING
--rollback 	experiment.occurrence AS occ
--rollback WHERE
--rollback 	plot.occurrence_id = occ.id
--rollback AND 
--rollback 	occ.experiment_id IN (
--rollback 	SELECT 
--rollback 		id
--rollback 	FROM
--rollback 		experiment.experiment
--rollback 	WHERE
--rollback 		experiment_name='HTAF2023_RICE_AYT'
--rollback 	)
--rollback ;
