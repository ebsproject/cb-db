--liquibase formatted sql

--changeset postgres:2_populate_entry_list_maize_for_htaf context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2771 Populate entry_list Maize for HTAF



INSERT INTO 
    experiment.entry_list
        (
            entry_list_code,entry_list_name,description,entry_list_status,experiment_id,creator_id,
            entry_list_type,cross_count,experiment_year,season_id,site_id,crop_id,program_id
        )
SELECT
    entry_list_code,entry_list_name,description,entry_list_status,experiment_id,creator_id,
    entry_list_type,cross_count,experiment_year::int,season_id::int,site_id::int,crop_id,program_id
FROM
    (
        VALUES
            (
                (experiment.generate_code('entry_list')),
                'HTAF2023_MAIZE_KE Entry List',
                NULL,
                'completed',
                (SELECT id FROM experiment.experiment WHERE experiment_name='HTAF2023_MAIZE_KE'),
                (SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),
                'entry list',
                0,
                NULL,
                NULL,
                NULL,
                (SELECT id FROM tenant.crop WHERE crop_name='MAIZE'),
                (SELECT id FROM tenant.program WHERE program_name='IRSEA')
            )
    )
     t (entry_list_code,entry_list_name,description,entry_list_status,experiment_id,creator_id,entry_list_type,cross_count,experiment_year,season_id,site_id,crop_id,program_id)
;



--rollback DELETE FROM
--rollback    experiment.entry_list
--rollback WHERE
--rollback    entry_list_name
--rollback IN
--rollback    (
--rollback        'HTAF2023_MAIZE_KE Entry List'
--rollback    )
--rollback ;