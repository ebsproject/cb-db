--liquibase formatted sql

--changeset postgres:3_populate_entry_rice_for_htaf context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2769 Populate entry Rice for HTAF



INSERT INTO
   experiment.entry (
       entry_code, entry_number, entry_name, entry_type, entry_class, package_id, entry_role, entry_status,
       entry_list_id, germplasm_id, seed_id, creator_id, is_void
   )
SELECT 
    ROW_NUMBER() OVER() AS entry_code,
    ROW_NUMBER() OVER() AS entry_number,
    gg.designation AS entry_name,
    'test' AS entry_type,
    NULL AS entry_class,
    gp.id AS package_id,
    NULL AS entry_role,
    'active' AS entry_status,
    el.id AS entry_list_id,
    gg.id AS germplasm_id,
    gs.id AS seed_id,
    ee.creator_id AS creator_id,
    FALSE AS is_void
FROM 
    germplasm.germplasm gg
INNER JOIN
    germplasm.seed gs
ON
    gs.germplasm_id = gg.id
INNER JOIN
    germplasm.package gp
ON
    gp.seed_id = gs.id
INNER JOIN
    experiment.experiment ee
ON
    ee.experiment_name = 'HTAF2023_RICE_AYT'
INNER JOIN
    experiment.entry_list el
ON
    el.experiment_id = ee.id
WHERE
    gg.designation
IN
    (
        'IR 97346',
        'IR 97367',
        'IR 98463',
        'IR 98464',
        'IR 100029',
        'IR 100112',
        'IR 100121',
        'IR 100122',
        'IR 100722',
        'IR 100723',
        'IR 96915-B RGA-B RGA-B RGA',
        'IR 97367-B RGA-B RGA-B RGA',
        'IR 98492-B-B RGA-B RGA',
        'IR 98594-B RGA-B RGA',
        'IR 99983-B RGA',
        'IR 100007-B RGA',
        'IR 100109-B RGA',
        'IR 100112-B RGA',
        'IR 100121-B RGA',
        'IR 98590-B RGA-B RGA',
        'IR 99983',
        'IR 99997-B',
        'IR 100007',
        'IR 100029-B RGA',
        'IR 100043-B',
        'IR 100090-B',
        'IR 100109',
        'IR 100110-B RGA',
        'IR 100110',
        'IR 100124',
        'IR 100136-B',
        'IR 100148-B',
        'IR 100160-B',
        'IR 96922-B RGA-B RGA-B RGA',
        'IR 96950',
        'IR 100122-B RGA',
        'IR 100124-B RGA',
        'IR 100171-B',
        'IR 100722-B RGA',
        'IR 100723-B RGA',
        'IR 103715-B',
        'IR 103761-B',
        'IR14D104',
        'IR14D154',
        'IR14D155',
        'IR 64',
        'IRRI 154',
        'IR03A550',
        'IRRI 193',
        'IR05N412',
        'IR05N419',
        'IR06N155',
        'IR07A179',
        'IR09A116',
        'IRRI 216',
        'IR09A220',
        'IR09A228',
        'IR09N190',
        'IR09N473',
        'IR09N503',
        'IR09N522',
        'IR09N534',
        'IR09N536',
        'IR09N538',
        'IR10A155',
        'IR10A231',
        'IR10F336',
        'IR10F365',
        'IR10F559',
        'IR10N108',
        'IR10N225',
        'IR10N237',
        'IR10N238',
        'IR10N259',
        'IR10N271',
        'IR10N305',
        'IR11A282',
        'IR11A303',
        'IR11A306',
        'IR11N121',
        'IR11N202',
        'IR12A181',
        'IR12A207',
        'IR12N135',
        'IR12N253',
        'IRBL9-W[RL] SPK2',
        'OR 142-99',
        'PR 36831-31-1-1-1-1-1',
        'PR 36933-B-1-36-1-1-1',
        'PR 37139-3-1-3-1-2-1',
        'PR 37246-2-3-2-1-1-2-1',
        'PR 37704-2B-6-1-2-1-1',
        'PR 37866-1B-1-4',
        'PR 37921-B-3-2-2',
        'PR 37921-B-3-4-2-1-2',
        'PR 37939-3B-1-2',
        'PR 37942-3B-5-3-2',
        'PR 37951-3B-37-1-2',
        'PR 37952-B-4-1-3',
        'PR 37957-B-3-3-1-1-1'
    )
;



-- revert changes
--rollback DELETE FROM
--rollback 	experiment.entry AS e
--rollback USING
--rollback 	experiment.entry_list AS el
--rollback INNER JOIN
--rollback 	experiment.experiment AS ee
--rollback ON
--rollback 	el.experiment_id = ee.id
--rollback WHERE
--rollback  	ee.experiment_name='HTAF2023_RICE_AYT'
--rollback AND
--rollback 	e.entry_list_id = el.id;