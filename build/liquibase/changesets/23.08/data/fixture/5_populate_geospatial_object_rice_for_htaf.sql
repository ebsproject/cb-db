--liquibase formatted sql

--changeset postgres:5_populate_geospatial_object_rice_for_htaf context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2769 Populate geospatial_object Rice for HTAF



INSERT INTO
    place.geospatial_object (
        geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, parent_geospatial_object_id, root_geospatial_object_id
    )
SELECT 
    CONCAT(site.geospatial_object_code,'-',exp.experiment_name) AS geospatial_object_code,
    CONCAT(tp.program_code,'-',ts.stage_code,'-',experiment_year,'-',tss.season_code,'-00',ROW_NUMBER() OVER()) AS geospatial_object_name,
    'planting area' AS geospatial_object_type,
    'breeding location' AS geospatial_object_subtype,
    exp.creator_id AS creator_id,
    field.id AS parent_geospatial_object_id,
    site.id AS root_geospatial_object_id
FROM
    experiment.experiment AS exp
INNER JOIN
    tenant.stage ts
ON
    ts.id = exp.stage_id
JOIN
    tenant.season tss
ON
    tss.id = exp.season_id
INNER JOIN
    tenant.program tp
ON
    exp.program_id = tp.id,
(
        VALUES
            ('IRRI, Los Baños, Laguna, Philippines','400')
) AS t (site, field)
INNER JOIN
    place.geospatial_object site
ON
    site.geospatial_object_name = t.site
INNER JOIN
    place.geospatial_object field
ON
    field.geospatial_object_name = t.field
WHERE
    experiment_name='HTAF2023_RICE_AYT'
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object
--rollback WHERE
--rollback     geospatial_object_name IN (
--rollback          SELECT 
--rollback              CONCAT(tp.program_code,'-',ts.stage_code,'-',experiment_year,'-',tss.season_code,'-00',ROW_NUMBER() OVER()) AS geospatial_object_name
--rollback          FROM
--rollback              experiment.experiment AS exp
--rollback          INNER JOIN
--rollback              tenant.stage ts
--rollback          ON
--rollback              ts.id = exp.stage_id
--rollback          JOIN
--rollback              tenant.season tss
--rollback          ON
--rollback              tss.id = exp.season_id
--rollback          INNER JOIN
--rollback              tenant.program tp
--rollback          ON
--rollback              exp.program_id = tp.id,
--rollback          (
--rollback                  VALUES
--rollback                      ('IRRI, Los Baños, Laguna, Philippines','400')
--rollback          ) AS t (site, field)
--rollback          INNER JOIN
--rollback              place.geospatial_object site
--rollback          ON
--rollback              site.geospatial_object_name = t.site
--rollback          INNER JOIN
--rollback              place.geospatial_object field
--rollback          ON
--rollback              field.geospatial_object_name = t.field
--rollback          WHERE
--rollback              experiment_name='HTAF2023_RICE_AYT'
--rollback );