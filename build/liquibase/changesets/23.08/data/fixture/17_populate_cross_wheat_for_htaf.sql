--liquibase formatted sql

--changeset postgres:17_populate_cross_wheat_for_htaf context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2770 Populate cross Wheat for HTAF



INSERT INTO
   germplasm.cross (
       cross_name,
       cross_method,
       germplasm_id,
       experiment_id,
       creator_id
   )
SELECT
    (entf.entry_name || '/' || entm.entry_name) AS cross_name,
    'SIMPLE CROSS' AS cross_method,
    NULL AS germplasm_id,
    expt.id AS experiment_id,
    person.id AS creator_id
FROM
    experiment.experiment AS expt
    INNER JOIN experiment.entry_list AS entlist
        ON expt.id = entlist.experiment_id
    INNER JOIN experiment.entry AS entf
        ON entf.entry_list_id = entlist.id
    INNER JOIN experiment.entry AS entm
        ON entm.entry_list_id = entlist.id
    INNER JOIN tenant.person AS person
        ON person.username = 'admin'
WHERE
    expt.experiment_name = 'HTAF2023_WHEAT_AYT'
    --AND entf.entry_number BETWEEN 1 AND 50
    --AND entm.entry_number BETWEEN 51 AND 100
ORDER BY
    entf.entry_number,
    entm.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross AS crs
--rollback USING
--rollback     experiment.experiment AS expt
--rollback WHERE
--rollback     crs.experiment_id = expt.id
--rollback     AND expt.experiment_name = 'HTAF2023_WHEAT_AYT'
--rollback ;