--liquibase formatted sql

--changeset postgres:18_populate_experiment_protocol_rice_for_htaf context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2769 Populate experiment protocol Rice for HTAF



INSERT INTO
    experiment.experiment_protocol (
        experiment_id, protocol_id, order_number, creator_id
    )
SELECT 
    ee.id experiment_id,
    tpr.id protocol_id,
    row_number () over () order_number,
    psn.id creator_id
FROM 
    tenant.protocol tpr
INNER JOIN
    experiment.experiment ee
ON
    ee.experiment_name = 'HTAF2023_RICE_AYT'
INNER JOIN
    tenant.person psn
ON
    psn.username = 'admin'
WHERE 
    tpr.protocol_code ILIKE (SELECT concat('%',ee.experiment_name ,'%'))
ORDER BY
    tpr.id
;



--rollback DELETE FROM experiment.experiment_protocol WHERE experiment_id = (SELECT id FROM experiment.experiment WHERE experiment_name='HTAF2023_RICE_AYT');
