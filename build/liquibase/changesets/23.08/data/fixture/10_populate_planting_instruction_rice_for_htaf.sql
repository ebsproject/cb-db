--liquibase formatted sql

--changeset postgres:10_populate_planting_instruction_rice_for_htaf context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2769 Populate planting instruction for HTAF



INSERT INTO
   experiment.planting_instruction (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status, entry_id,
       plot_id, germplasm_id, seed_id, creator_id
   )
SELECT
    ee.entry_code,
    ee.entry_number,
    ee.entry_name,
    ee.entry_type,
    ee.entry_role,
    ee.entry_status,
    ee.id AS entry_id,
    plt.id AS plot_id,
    ee.germplasm_id,
    ee.seed_id,
    exp.creator_id
FROM
    experiment.experiment exp
INNER JOIN
    experiment.entry_list el
ON
    el.experiment_id = exp.id
INNER JOIN
    experiment.entry ee
ON
    ee.entry_list_id = el.id
INNER JOIN
    experiment.plot plt
ON
    plt.entry_id = ee.id
WHERE
    exp.experiment_name = 'HTAF2023_RICE_AYT'
ORDER BY
    ee.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.planting_instruction AS plantinst
--rollback USING
--rollback      experiment.experiment exp
--rollback INNER JOIN
--rollback     experiment.entry_list el
--rollback ON
--rollback     el.experiment_id = exp.id
--rollback INNER JOIN
--rollback     experiment.entry ee
--rollback ON
--rollback     ee.entry_list_id = el.id
--rollback INNER JOIN
--rollback     experiment.plot plt
--rollback ON
--rollback     plt.entry_id = ee.id
--rollback WHERE
--rollback     exp.experiment_name = 'HTAF2023_RICE_AYT'
--rollback AND
--rollback     plantinst.entry_id = ee.id
--rollback AND
--rollback     plantinst.plot_id = plt.id
--rollback ;