--liquibase formatted sql

--changeset postgres:3_populate_entry_maize_for_htaf context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2771 Populate Entry Maize for HTAF



INSERT INTO
   experiment.entry (
       entry_code, entry_number, entry_name, entry_type, entry_class, package_id, entry_role, entry_status,
       entry_list_id, germplasm_id, seed_id, creator_id, is_void
   )
SELECT 
    ROW_NUMBER() OVER() AS entry_code,
    ROW_NUMBER() OVER() AS entry_number,
    gg.designation AS entry_name,
    'test' AS entry_type,
    NULL AS entry_class,
    gp.id AS package_id,
    NULL AS entry_role,
    'active' AS entry_status,
    el.id AS entry_list_id,
    gg.id AS germplasm_id,
    gs.id AS seed_id,
    ee.creator_id AS creator_id,
    FALSE AS is_void
FROM 
    germplasm.germplasm gg
INNER JOIN
    germplasm.seed gs
ON
    gs.germplasm_id = gg.id
INNER JOIN
    germplasm.package gp
ON
    gp.seed_id = gs.id
INNER JOIN
    experiment.experiment ee
ON
    ee.experiment_name = 'HTAF2023_MAIZE_KE'
INNER JOIN
    experiment.entry_list el
ON
    el.experiment_id = ee.id
WHERE
    gg.designation
IN
    (
        '(ABDHL0089/ABDHL120668)@195-B',
        '(ABDHL0089/ABDHL120668)@19-B',
        '(ABDHL0089/ABDHL120918)@200-B',
        '(ABDHL0089/ABDHL120918)@52-B',
        '(ABDHL0089/ABDHL120918)@63-B',
        '(ABDHL0089/ABLTI0136)@220-B',
        'ABDHL0221',
        'ABDHL120312',
        'ABDHL120918',
        'ABDHL163943',
        'ABDHL164302',
        'ABDHL164665',
        'ABDHL164672',
        'ABDHL165891',
        'ABLMARSI0022',
        'ABLMARSI0037',
        'ABLTI0137',
        '(ABLTI0139/ABDHL120918)@246-B',
        '(ABLTI0139/ABDHL120918)@299-B',
        '(ABLTI0139/ABDHL120918)@373-B',
        '(ABLTI0139/ABDHL120918)@393-B',
        '(ABLTI0139/ABDHL120918)@479-B',
        '(ABLTI0139/ABDHL120918)@553-B',
        '(ABLTI0139/ABDHL120918)@653-B',
        '(ABLTI0139/ABDHL120918)@95-B',
        '(ABLTI0139/ABLTI0335)@107-B',
        '(ABLTI0139/ABLTI0335)@14-B',
        '(ABLTI0139/CML543)@140-B',
        'ABLTI0330',
        'ABSBL10020',
        'ABSBL10060',
        '(CML494/OFP9)-12-2-1-1-1-B*4',
        '(CML494/OFP9)-12-2-1-1-2-B*5',
        'EEL822/LMC243',
        'EEL895/IBL444',
        '(FBM239/LLTR)-B-29-1',
        '(FBM239/LLTR)-B-55-1',
        '(FBM239/LLTR)-B-74-1',
        '(FBM239/LLTR)-B-79-1',
        '(FBM239/LLTR)-B-93-1',
        '(FBM239/LLTR)-B-97-1',
        '(FBM239/LLTR)-B-98-1',
        '(FBM239/RHHB9)-B-109-1',
        '(FBM239/RHHB9)-B-123-1',
        '(FBM239/RHHB9)-B-125-1',
        '(FBM239/RHHB9)-B-126-1',
        '(FBM239/RHHB9)-B-131-1',
        '(FBM239/RHHB9)-B-135-1',
        '(FBM239/RHHB9)-B-158-1',
        '(FBM239/RHHB9)-B-171-1',
        '(FBM239/RHM52)-B-134-1',
        '(FBM239/RHM52)-B-17-1',
        '(FBM239/RHM52)-B-39-1',
        '(FBM239/RHM52)-B-79-1',
        '(FBM239/RHT11)-B-101-1',
        '(FBM239/RHT11)-B-107-1',
        '(FBM239/RHT11)-B-13-1',
        '(FBM239/RHT11)-B-22-1',
        '(FBM239/RHT11)-B-71-1',
        '(FBM239/RHT11)-B-82-1',
        '(FBM239/RHT11)-B-89-1',
        '(HTL437/ABHB9)-B-108-1-1-B',
        '(HTL437/ABHB9)-B-119-1-1-B',
        '(HTL437/ABHB9)-B-120-1-1-B',
        '(HTL437/ABHB9)-B-168-1-1-B',
        '(HTL437/ABHB9)-B-168-1-2-B',
        '(HTL437/ABHB9)-B-17-1-2-B',
        '(HTL437/ABHB9)-B-183-1-2-B',
        '(HTL437/ABHB9)-B-31-1-1-B',
        '(HTL437/ABHB9)-B-31-1-2-B',
        '(HTL437/ABHB9)-B-3-1-1-B',
        '(HTL437/ABHB9)-B-73-1-2-B',
        '(HTL437/ABP38)-B-157-1-1-B',
        '(HTL437/ABP38)-B-157-1-2-B',
        '(HTL437/ABP38)-B-178-1-1-B',
        '(HTL437/ABP38)-B-178-1-2-B',
        '(HTL437/ABP38)-B-188-1-2-B',
        '(HTL437/ABP38)-B-189-1-2-B',
        '(HTL437/ABW52)-B-145-1-1-B',
        '(HTL437/ABW52)-B-17-1-2-B',
        '(HTL437/ABW52)-B-3-1-1-B',
        '(HTL437/ABW52)-B-3-1-2-B',
        '(HTL437/ABW52)-B-71-1-1-B',
        '(HTL437/ABW52//HTL437)-96-1-1-1-B',
        '(HTL437/ABW52//HTL437)-96-1-1-2-B',
        '(HTL437/ABW52//HTL437)-98-1-1-1-B',
        '(HTL437/ABW52//HTL437)-98-1-1-2-B',
        '(HTL437/RK132)-B-51-1-2-B',
        '(HTL437/RK132)-B-86-1-1-B',
        '(HTL437/RK132)-B-89-1-2-B',
        '(HTL437/RK132)-B-9-1-1-B',
        'LMC243/LMC266',
        'LMC266/EEL895',
        'LMC266/LMC269',
        'LMC266/MKL0333',
        '(LTL812/ABT11)-B-16-1-1-B',
        '(LTL812/ABT11)-B-16-1-2-B',
        '(LTL812/ABT11)-B-51-1-2-B',
        '(LTL812/ABT11)-B-8-1-2-B',
        '(LTL812/ABW52)-B-113-1-2-B'
    );



-- revert changes
--rollback DELETE FROM
--rollback 	experiment.entry AS e
--rollback USING
--rollback 	experiment.entry_list AS el
--rollback INNER JOIN
--rollback 	experiment.experiment AS ee
--rollback ON
--rollback 	el.experiment_id = ee.id
--rollback WHERE
--rollback  	ee.experiment_name='HTAF2023_MAIZE_KE'
--rollback AND
--rollback 	e.entry_list_id = el.id;