--liquibase formatted sql

--changeset postgres:9_populate_plot_data_maize_for_htaf context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2771 Populate Populate Plot Data Maize for HTAF



INSERT INTO
    experiment.plot_data
        (plot_id, variable_id, data_value, data_qc_code, creator_id)

SELECT
    plt.id plot_id,
    mv.id variable_id,
    'Bulk' data_value,
    'Q' data_qc_code,
    exp.creator_id
FROM
    experiment.experiment exp
INNER JOIN
    experiment.entry_list el
ON
    el.experiment_id = exp.id
INNER JOIN
    experiment.occurrence eo
ON
    eo.experiment_id = exp.id
INNER JOIN
    experiment.location_occurrence_group elo
ON
    elo.occurrence_id = eo.id
INNER JOIN
    experiment.location loc
ON
    elo.location_id = loc.id
INNER JOIN
    experiment.plot plt
ON
    plt.occurrence_id = eo.id
INNER JOIN
    master.variable mv
ON
    mv.abbrev='HV_METH_DISC'
WHERE
    exp.experiment_name = 'HTAF2023_MAIZE_KE'

UNION ALL

SELECT
    plt.id plot_id,
    mv.id variable_id,
    '2021-06-09' data_value,
    'Q' data_qc_code,
    exp.creator_id
FROM
    experiment.experiment exp
INNER JOIN
    experiment.entry_list el
ON
    el.experiment_id = exp.id
INNER JOIN
    experiment.occurrence eo
ON
    eo.experiment_id = exp.id
INNER JOIN
    experiment.location_occurrence_group elo
ON
    elo.occurrence_id = eo.id
INNER JOIN
    experiment.location loc
ON
    elo.location_id = loc.id
INNER JOIN
    experiment.plot plt
ON
    plt.occurrence_id = eo.id
INNER JOIN
    master.variable mv
ON
    mv.abbrev='HVDATE_CONT'
WHERE
    exp.experiment_name = 'HTAF2023_MAIZE_KE'
;



--rollback DELETE FROM
--rollback     experiment.plot_data AS pltd
--rollback USING
--rollback 	experiment.plot AS plt,
--rollback 	experiment.occurrence AS occ
--rollback WHERE
--rollback 	plt.occurrence_id = occ.id
--rollback AND
--rollback 	pltd.plot_id = plt.id
--rollback AND 
--rollback 	occ.experiment_id IN (
--rollback 	SELECT 
--rollback 		id
--rollback 	FROM
--rollback 		experiment.experiment
--rollback 	WHERE
--rollback 		experiment_name='HTAF2023_MAIZE_KE'
--rollback 	)
--rollback ;