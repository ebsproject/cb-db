--liquibase formatted sql

--changeset postgres:4_populate_occurrence_rice_for_htaf context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2769 Populate occurrence Rice for HTAF



INSERT INTO
    experiment.occurrence (
        occurrence_code,
        occurrence_name,
        occurrence_status,
        experiment_id,
        site_id,
        field_id,
        rep_count,
        occurrence_number,
        creator_id
    )
SELECT 
    experiment.generate_code('occurrence') AS occurrence_code,
    CONCAT(tp.program_code,'-',ts.stage_code,'-',experiment_year,'-',tss.season_code,'-00',ROW_NUMBER() OVER()) AS occurrence_name,
    'planted' AS occurrence_status,
    exp.id AS experiment_id,
    site.id AS site_id,
    field.id AS field_id,
    1 AS rep_count,
    1 AS occurrence_number,
    exp.creator_id AS creator_id
FROM
    experiment.experiment AS exp
INNER JOIN
    tenant.stage ts
ON
    ts.id = exp.stage_id
JOIN
    tenant.season tss
ON
    tss.id = exp.season_id
INNER JOIN
    tenant.program tp
ON
    exp.program_id = tp.id,
(
        VALUES
            ('IRRI, Los Baños, Laguna, Philippines','400')
) AS t (site, field)
INNER JOIN
    place.geospatial_object site
ON
    site.geospatial_object_name = t.site
INNER JOIN
    place.geospatial_object field
ON
    field.geospatial_object_name = t.field
WHERE
    experiment_name='HTAF2023_RICE_AYT'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence
--rollback WHERE
--rollback     experiment_id IN (SELECT id FROM experiment.experiment WHERE experiment_name='HTAF2023_RICE_AYT')
--rollback ;
