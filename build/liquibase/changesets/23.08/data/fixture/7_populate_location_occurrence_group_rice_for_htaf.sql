--liquibase formatted sql

--changeset postgres:7_populate_location_occurrence_group_rice_for_htaf context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2769 Populate location_occurrence_group Rice for HTAF



INSERT INTO 
    experiment.location_occurrence_group (
        location_id,occurrence_id,order_number,creator_id
    )
SELECT
    loc.id AS location_id,
    occ.id AS occurrence_id,
    ROW_NUMBER() OVER() AS order_number,
    exp.creator_id AS creator_id
FROM
    experiment.experiment exp
INNER JOIN
    tenant.season tss
ON
    tss.id = exp.season_id
INNER JOIN
    tenant.stage ts
ON
    ts.id = exp.stage_id
INNER JOIN
    tenant.program tp
ON
    exp.program_id = tp.id
INNER JOIN
    experiment.occurrence occ
ON
    occ.experiment_id = exp.id
INNER JOIN
    experiment.location loc
ON
    loc.location_name = CONCAT(tp.program_code,'-',ts.stage_code,'-',experiment_year,'-',tss.season_code,'-001')
WHERE
    exp.experiment_name = 'HTAF2023_RICE_AYT'
ORDER BY
    occ.id
;



--rollback DELETE FROM 
--rollback     experiment.location_occurrence_group
--rollback WHERE
--rollback     occurrence_id 
--rollback IN 
--rollback     (
--rollback      SELECT
--rollback          occ.id AS occurrence_id
--rollback      FROM
--rollback          experiment.experiment exp
--rollback      INNER JOIN
--rollback          tenant.season tss
--rollback      ON
--rollback          tss.id = exp.season_id
--rollback      INNER JOIN
--rollback          tenant.stage ts
--rollback      ON
--rollback          ts.id = exp.stage_id
--rollback      INNER JOIN
--rollback          tenant.program tp
--rollback      ON
--rollback          exp.program_id = tp.id
--rollback      INNER JOIN
--rollback          experiment.occurrence occ
--rollback      ON
--rollback          occ.experiment_id = exp.id
--rollback      INNER JOIN
--rollback          experiment.location loc
--rollback      ON
--rollback          loc.location_name = CONCAT(tp.program_code,'-',ts.stage_code,'-',experiment_year,'-',tss.season_code,'-001')
--rollback      WHERE
--rollback          exp.experiment_name = 'HTAF2023_RICE_AYT'
--rollback      ORDER BY
--rollback          occ.id
--rollback     )
--rollback ;