--liquibase formatted sql

--changeset postgres:6_populate_location_maize_for_htaf context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2771 Populate Location Maize for HTAF



INSERT INTO
   experiment.location (
       location_code, location_name, location_status, location_type,
       location_year, season_id, location_number, site_id, field_id,
       steward_id, geospatial_object_id, creator_id
   )
SELECT 
    CONCAT(site.geospatial_object_code,'-',exp.experiment_name) AS location_code,
    CONCAT(tp.program_code,'-',ts.stage_code,'-',experiment_year,'-',tss.season_code,'-00',ROW_NUMBER() OVER()) AS location_name,
    'planted' AS location_status,
    'planting area' AS location_type,
    exp.experiment_year AS location_year,
    tss.id AS season_id ,
    1 AS location_number,
    site.id AS site_id,
    field.id AS field_id,
    exp.creator_id AS steward_id,
    pgo.id AS geospatial_object_id,
    exp.creator_id AS creator_id
FROM
    experiment.experiment AS exp
JOIN
    tenant.season tss
ON
    tss.id = exp.season_id
INNER JOIN
    tenant.stage ts
ON
    ts.id = exp.stage_id
INNER JOIN
    tenant.program tp
ON
    exp.program_id = tp.id
INNER JOIN
    place.geospatial_object pgo
ON
    pgo.geospatial_object_name = CONCAT(tp.program_code,'-',ts.stage_code,'-',experiment_year,'-',tss.season_code,'-001'),
(
        VALUES
            ('IRRI, Los Baños, Laguna, Philippines','400')
) AS t (site, field)
INNER JOIN
    place.geospatial_object site
ON
    site.geospatial_object_name = t.site
INNER JOIN
    place.geospatial_object field
ON
    field.geospatial_object_name = t.field

WHERE
    exp.experiment_name='HTAF2023_MAIZE_KE'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location
--rollback WHERE
--rollback     location_name IN (
--rollback          SELECT 
--rollback              CONCAT(tp.program_code,'-',ts.stage_code,'-',exp.experiment_year,'-',tss.season_code,'-00',ROW_NUMBER() OVER()) AS location_name
--rollback          FROM
--rollback              experiment.experiment AS exp
--rollback          INNER JOIN
--rollback              tenant.stage ts
--rollback          ON
--rollback              ts.id = exp.stage_id
--rollback          JOIN
--rollback              tenant.season tss
--rollback          ON
--rollback              tss.id = exp.season_id
--rollback          INNER JOIN
--rollback              tenant.program tp
--rollback          ON
--rollback              exp.program_id = tp.id,
--rollback          (
--rollback                  VALUES
--rollback                      ('IRRI, Los Baños, Laguna, Philippines','400')
--rollback          ) AS t (site, field)
--rollback          INNER JOIN
--rollback              place.geospatial_object site
--rollback          ON
--rollback              site.geospatial_object_name = t.site
--rollback          INNER JOIN
--rollback              place.geospatial_object field
--rollback          ON
--rollback              field.geospatial_object_name = t.field
--rollback          WHERE
--rollback              experiment_name='HTAF2023_MAIZE_KE'
--rollback );