--liquibase formatted sql

--changeset postgres:13_populate_experiment_data_maize_for_htaf context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2771 Populate Experiment Data Maize for HTAF



INSERT INTO
    experiment.experiment_data (
        experiment_id, variable_id, data_value, data_qc_code, protocol_id, creator_id
    )
SELECT
    exp.id AS experiment_id,
    mv.id AS variable_id,
    exp_data.data_value AS data_value,
    'N' AS data_qc_code,
    tp.id AS protocol_id,
    exp.creator_id AS creator_id
FROM	
    experiment.experiment AS exp,
    (
        VALUES
            ('FIRST_PLOT_POSITION_VIEW','Top Left',NULL),
            ('TRAIT_PROTOCOL_LIST_ID','1393','trait'),
            ('MANAGEMENT_PROTOCOL_LIST_ID','1394','management'),
            ('ESTABLISHMENT','transplanted','planting'),
            ('PLANTING_TYPE','Flat','planting'),
            ('PLOT_TYPE','6R','planting'),
            ('ROWS_PER_PLOT_CONT','6','planting'),
            ('DIST_BET_ROWS','20','planting'),
            ('PLOT_WIDTH','1.2','planting'),
            ('PLOT_LN','20','planting'),
            ('PLOT_AREA_2','24','planting'),
            ('ALLEY_LENGTH','1','planting'),
            ('SEEDING_RATE','Normal','planting'),
            ('PROTOCOL_TARGET_LEVEL','occurrence',NULL),
            ('HV_METH_DISC','Bulk','harvest')
    ) AS exp_data (variable_abbrev, data_value,protocol_type)
    INNER JOIN
        master.variable mv
    ON
        mv.abbrev = exp_data.variable_abbrev
    LEFT JOIN
        tenant.protocol tp
    ON
        tp.protocol_code = (CONCAT(UPPER(exp_data.protocol_type),'_PROTOCOL_',UPPER('HTAF2023_MAIZE_KE')))
WHERE
    exp.experiment_name='HTAF2023_MAIZE_KE'
;



--rollback DELETE FROM experiment.experiment_data WHERE experiment_id = (SELECT id FROM experiment.experiment WHERE experiment_name='HTAF2023_MAIZE_KE');
