--liquibase formatted sql

--changeset postgres:11_populate_experiment_design_for_htaf context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2770 Populate experiment design Wheat for HTAF



INSERT INTO
   experiment.experiment_design (
       occurrence_id, design_id, plot_id, block_type, block_value,
       block_level_number, creator_id, block_name
   )
SELECT
    eo.id AS occurrence_id,
    ep.rep AS design_id,
    ep.id AS plot_id,
    'replication block',
    ep.rep AS block_value,
    ep.rep AS block_level_number,
    exp.creator_id AS creator_id,
    'replicate' AS block_name
FROM 
    experiment.experiment exp
INNER JOIN
    experiment.occurrence eo
ON
    eo.experiment_id = exp.id
INNER JOIN
    experiment.plot ep
ON
    ep.occurrence_id = eo.id
WHERE 
    exp.experiment_name='HTAF2023_WHEAT_AYT'
;



--rollback DELETE FROM 
--rollback     experiment.experiment_design ee
--rollback USING 
--rollback     experiment.occurrence eo,
--rollback     experiment.experiment exp
--rollback WHERE
--rollback     eo.experiment_id = exp.id
--rollback AND
--rollback     ee.occurrence_id = eo.id
--rollback AND
--rollback     experiment_name = 'HTAF2023_WHEAT_AYT'
--rollback ;