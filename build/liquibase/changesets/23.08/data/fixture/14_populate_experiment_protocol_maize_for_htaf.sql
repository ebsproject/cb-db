--liquibase formatted sql

--changeset postgres:14_populate_experiment_protocol_maize_for_htaf context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2771 Populate Experiment Protocol Maize for HTAF



INSERT INTO
    experiment.experiment_protocol (
        experiment_id, protocol_id, order_number, creator_id
    )
SELECT
    exp.id AS experiment_id,
    tp.id AS protocol_id,
    ROW_NUMBER() OVER() AS order_number,
    exp.creator_id AS creator_id
FROM 	
    experiment.experiment AS exp,
    (
        VALUES
            ('trait'),
            ('management'),
            ('planting'),
            ('harvest')
    ) AS t (protocol_type)
    INNER JOIN
        tenant.protocol tp
    ON
        tp.protocol_code = (CONCAT(UPPER(t.protocol_type),'_PROTOCOL_',UPPER('HTAF2023_MAIZE_KE')))
WHERE	
    exp.experiment_name='HTAF2023_MAIZE_KE'
;



--rollback DELETE FROM experiment.experiment_protocol WHERE experiment_id = (SELECT id FROM experiment.experiment WHERE experiment_name='HTAF2023_MAIZE_KE');