--liquibase formatted sql

--changeset postgres:17_populate_germplasm_cross_parent_rice_for_htaf context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2769 Populate germplasm cross parent Rice for HTAF



INSERT INTO
   germplasm.cross_parent (
       cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, creator_id
   )
SELECT
    t.*
FROM (
        SELECT
            crs.id AS cross_id,
            ent.germplasm_id,
            ent.seed_id,
            'female' AS parent_role,
            1 AS order_number,
            expt.id AS experiment_id,
            ent.id AS entry_id,
            person.id AS creator_id
        FROM
            germplasm.CROSS AS crs
            INNER JOIN experiment.experiment AS expt
                ON crs.experiment_id = expt.id
            INNER JOIN experiment.entry_list AS entlist
                ON entlist.experiment_id = expt.id
            INNER JOIN experiment.entry AS ent
                ON ent.entry_list_id = entlist.id
            INNER JOIN tenant.person AS person
                ON person.username = 'admin'
        WHERE
            expt.experiment_name = 'HTAF2023_RICE_AYT'
            AND ent.entry_number BETWEEN 1 AND 25
            AND crs.cross_name ILIKE ent.entry_name || '/%'
        UNION ALL
            SELECT
                crs.id AS cross_id,
                ent.germplasm_id,
                ent.seed_id,
                'male' AS parent_role,
                2 AS order_number,
                expt.id AS experiment_id,
                ent.id AS entry_id,
                person.id AS creator_id
            FROM
                germplasm.CROSS AS crs
                INNER JOIN experiment.experiment AS expt
                    ON crs.experiment_id = expt.id
                INNER JOIN experiment.entry_list AS entlist
                    ON entlist.experiment_id = expt.id
                INNER JOIN experiment.entry AS ent
                    ON ent.entry_list_id = entlist.id
                INNER JOIN tenant.person AS person
                    ON person.username = 'admin'
            WHERE
                expt.experiment_name = 'HTAF2023_RICE_AYT'
                AND ent.entry_number BETWEEN 26 AND 50
                AND crs.cross_name ILIKE '%/' || ent.entry_name
    ) AS t
ORDER BY
    t.cross_id,
    t.order_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross_parent AS crspar
--rollback USING
--rollback     germplasm.cross AS crs
--rollback     INNER JOIN experiment.experiment AS expt
--rollback         ON crs.experiment_id = expt.id
--rollback WHERE
--rollback     crspar.cross_id = crs.id
--rollback     AND expt.experiment_name = 'HTAF2023_RICE_AYT'
--rollback ;
