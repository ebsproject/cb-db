--liquibase formatted sql

--changeset postgres:add_em_planting_instructions_configurations context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5830 CB-EM UI: Request to make the columns of information customizable for downloading the planting instructions file from the location basic information page
--comment: CORB-6183 CB-DB: Add configurations for planting instructions (Occurrence and Location levels)



-- add em download planting instruction files configs
INSERT INTO platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'DOWNLOAD_PLANTING_INSTRUCTION_FILES_DEFAULT',
        'Download Planting Instruction Files Default',
        '{
            "occurrence": [
                {
                    "attribute": "experimentDbId",
                    "abbrev": "EXPERIMENT_ID"
                },
                {
                    "attribute": "programCode",
                    "abbrev": "PROGRAM_CODE"
                },
                {
                    "attribute": "stageCode",
                    "abbrev": "STAGE_CODE"
                },
                {
                    "attribute": "experimentYear",
                    "abbrev": "EXPERIMENT_YEAR"
                },
                {
                    "attribute": "seasonCode",
                    "abbrev": "SEASON_CODE"
                },
                {
                    "attribute": "experimentCode",
                    "abbrev": "EXPERIMENT_CODE"
                },
                {
                    "attribute": "experimentName",
                    "abbrev": "EXPERIMENT_NAME"
                },
                {
                    "attribute": "experimentType",
                    "abbrev": "EXPERIMENT_TYPE"
                },
                {
                    "attribute": "experimentDesignType",
                    "abbrev": "EXPERIMENT_DESIGN_TYPE"
                },
                {
                    "attribute": "experimentSteward",
                    "abbrev": "EXPERIMENT_STEWARD"
                },
                {
                    "attribute": "occurrenceDbId",
                    "abbrev": "OCCURRENCE_ID"
                },
                {
                    "attribute": "occurrenceCode",
                    "abbrev": "OCCURRENCE_CODE"
                },
                {
                    "attribute": "occurrenceName",
                    "abbrev": "OCCURRENCE_NAME"
                },
                {
                    "attribute": "occurrenceSiteCode",
                    "abbrev": "OCCURRENCE_SITE_CODE"
                },
                {
                    "attribute": "occurrenceFieldCode",
                    "abbrev": "OCCURRENCE_FIELD_CODE"
                },
                {
                    "attribute": "plotDbId",
                    "abbrev": "PLOT_ID"
                },
                {
                    "attribute": "plotCode",
                    "abbrev": "PLOT_CODE"
                },
                {
                    "attribute": "plotNumber",
                    "abbrev": "PLOT_NUMBER"
                },
                {
                    "attribute": "plotType",
                    "abbrev": "PLOT_TYPE"
                },
                {
                    "attribute": "designX",
                    "abbrev": "DESIGN_X"
                },
                {
                    "attribute": "designY",
                    "abbrev": "DESIGN_Y"
                },
                {
                    "attribute": "paX",
                    "abbrev": "PA_X"
                },
                {
                    "attribute": "paY",
                    "abbrev": "PA_Y"
                },
                {
                    "attribute": "plantingInstructionDbId",
                    "abbrev": "PLANTING_INSTRUCTION_ID"
                },
                {
                    "attribute": "entryDbId",
                    "abbrev": "ENTRY_ID"
                },
                {
                    "attribute": "entryCode",
                    "abbrev": "ENTRY_CODE"
                },
                {
                    "attribute": "entryNumber",
                    "abbrev": "ENTRY_NUMBER"
                },
                {
                    "attribute": "entryName",
                    "abbrev": "GERMPLASM_NAME"
                },
                {
                    "attribute": "germplasmCode",
                    "abbrev": "GERMPLASM_CODE"
                },
                {
                    "attribute": "entryType",
                    "abbrev": "ENTRY_TYPE"
                },
                {
                    "attribute": "entryRole",
                    "abbrev": "ENTRY_ROLE"
                },
                {
                    "attribute": "packageDbId",
                    "abbrev": "PACKAGE_ID"
                },
                {
                    "attribute": "packageLabel",
                    "abbrev": "PACKAGE_LABEL"
                }
            ],
            "location": [
                {
                    "attribute": "experimentDbId",
                    "abbrev": "EXPERIMENT_ID"
                },
                {
                    "attribute": "programCode",
                    "abbrev": "PROGRAM_CODE"
                },
                {
                    "attribute": "stageCode",
                    "abbrev": "STAGE_CODE"
                },
                {
                    "attribute": "experimentYear",
                    "abbrev": "EXPERIMENT_YEAR"
                },
                {
                    "attribute": "seasonCode",
                    "abbrev": "SEASON_CODE"
                },
                {
                    "attribute": "experimentCode",
                    "abbrev": "EXPERIMENT_CODE"
                },
                {
                    "attribute": "experimentName",
                    "abbrev": "EXPERIMENT_NAME"
                },
                {
                    "attribute": "experimentType",
                    "abbrev": "EXPERIMENT_TYPE"
                },
                {
                    "attribute": "experimentDesignType",
                    "abbrev": "EXPERIMENT_DESIGN_TYPE"
                },
                {
                    "attribute": "experimentSteward",
                    "abbrev": "EXPERIMENT_STEWARD"
                },
                {
                    "attribute": "occurrenceDbId",
                    "abbrev": "OCCURRENCE_ID"
                },
                {
                    "attribute": "occurrenceCode",
                    "abbrev": "OCCURRENCE_CODE"
                },
                {
                    "attribute": "occurrenceName",
                    "abbrev": "OCCURRENCE_NAME"
                },
                {
                    "attribute": "occurrenceSiteCode",
                    "abbrev": "OCCURRENCE_SITE_CODE"
                },
                {
                    "attribute": "occurrenceFieldCode",
                    "abbrev": "OCCURRENCE_FIELD_CODE"
                },
                {
                    "attribute": "locationDbId",
                    "abbrev": "LOCATION_ID"
                },
                {
                    "attribute": "locationCode",
                    "abbrev": "LOCATION_CODE"
                },
                {
                    "attribute": "locationName",
                    "abbrev": "LOCATION_NAME"
                },
                {
                    "attribute": "locationType",
                    "abbrev": "LOCATION_TYPE"
                },
                {
                    "attribute": "siteCode",
                    "abbrev": "SITE_CODE"
                },
                {
                    "attribute": "fieldCode",
                    "abbrev": "FIELD_CODE"
                },
                {
                    "attribute": "plotDbId",
                    "abbrev": "PLOT_ID"
                },
                {
                    "attribute": "plotCode",
                    "abbrev": "PLOT_CODE"
                },
                {
                    "attribute": "plotNumber",
                    "abbrev": "PLOT_NUMBER"
                },
                {
                    "attribute": "plotType",
                    "abbrev": "PLOT_TYPE"
                },
                {
                    "attribute": "designX",
                    "abbrev": "DESIGN_X"
                },
                {
                    "attribute": "designY",
                    "abbrev": "DESIGN_Y"
                },
                {
                    "attribute": "paX",
                    "abbrev": "PA_X"
                },
                {
                    "attribute": "paY",
                    "abbrev": "PA_Y"
                },
                {
                    "attribute": "plantingInstructionDbId",
                    "abbrev": "PLANTING_INSTRUCTION_ID"
                },
                {
                    "attribute": "entryDbId",
                    "abbrev": "ENTRY_ID"
                },
                {
                    "attribute": "entryCode",
                    "abbrev": "ENTRY_CODE"
                },
                {
                    "attribute": "entryNumber",
                    "abbrev": "ENTRY_NUMBER"
                },
                {
                    "attribute": "entryName",
                    "abbrev": "GERMPLASM_NAME"
                },
                {
                    "attribute": "germplasmCode",
                    "abbrev": "GERMPLASM_CODE"
                },
                {
                    "attribute": "entryType",
                    "abbrev": "ENTRY_TYPE"
                },
                {
                    "attribute": "entryRole",
                    "abbrev": "ENTRY_ROLE"
                },
                {
                    "attribute": "packageDbId",
                    "abbrev": "PACKAGE_ID"
                },
                {
                    "attribute": "packageLabel",
                    "abbrev": "PACKAGE_LABEL"
                }
            ]
        }',
        1,
        'planting_instructions_manager',
        1,
        'CORB-5830 CB-EM UI: Request to make the columns of information customizable for downloading the planting instructions file from the location basic information page'
    ),
    (
        'DOWNLOAD_PLANTING_INSTRUCTION_FILES_IRSEA',
        'Download Planting Instruction Files IRSEA',
        '{
            "occurrence": [
                {
                    "attribute": "experimentDbId",
                    "abbrev": "EXPERIMENT_ID"
                },
                {
                    "attribute": "programCode",
                    "abbrev": "PROGRAM_CODE"
                },
                {
                    "attribute": "stageCode",
                    "abbrev": "STAGE_CODE"
                },
                {
                    "attribute": "experimentYear",
                    "abbrev": "EXPERIMENT_YEAR"
                },
                {
                    "attribute": "seasonCode",
                    "abbrev": "SEASON_CODE"
                },
                {
                    "attribute": "experimentCode",
                    "abbrev": "EXPERIMENT_CODE"
                },
                {
                    "attribute": "experimentName",
                    "abbrev": "EXPERIMENT_NAME"
                },
                {
                    "attribute": "experimentType",
                    "abbrev": "EXPERIMENT_TYPE"
                },
                {
                    "attribute": "experimentDesignType",
                    "abbrev": "EXPERIMENT_DESIGN_TYPE"
                },
                {
                    "attribute": "experimentSteward",
                    "abbrev": "EXPERIMENT_STEWARD"
                },
                {
                    "attribute": "occurrenceDbId",
                    "abbrev": "OCCURRENCE_ID"
                },
                {
                    "attribute": "occurrenceCode",
                    "abbrev": "OCCURRENCE_CODE"
                },
                {
                    "attribute": "occurrenceName",
                    "abbrev": "OCCURRENCE_NAME"
                },
                {
                    "attribute": "occurrenceSiteCode",
                    "abbrev": "OCCURRENCE_SITE_CODE"
                },
                {
                    "attribute": "occurrenceFieldCode",
                    "abbrev": "OCCURRENCE_FIELD_CODE"
                },
                {
                    "attribute": "plotDbId",
                    "abbrev": "PLOT_ID"
                },
                {
                    "attribute": "plotCode",
                    "abbrev": "PLOT_CODE"
                },
                {
                    "attribute": "plotNumber",
                    "abbrev": "PLOT_NUMBER"
                },
                {
                    "attribute": "plotType",
                    "abbrev": "PLOT_TYPE"
                },
                {
                    "attribute": "designX",
                    "abbrev": "DESIGN_X"
                },
                {
                    "attribute": "designY",
                    "abbrev": "DESIGN_Y"
                },
                {
                    "attribute": "paX",
                    "abbrev": "PA_X"
                },
                {
                    "attribute": "paY",
                    "abbrev": "PA_Y"
                },
                {
                    "attribute": "plantingInstructionDbId",
                    "abbrev": "PLANTING_INSTRUCTION_ID"
                },
                {
                    "attribute": "entryDbId",
                    "abbrev": "ENTRY_ID"
                },
                {
                    "attribute": "entryCode",
                    "abbrev": "ENTRY_CODE"
                },
                {
                    "attribute": "entryNumber",
                    "abbrev": "ENTRY_NUMBER"
                },
                {
                    "attribute": "entryName",
                    "abbrev": "GERMPLASM_NAME"
                },
                {
                    "attribute": "germplasmCode",
                    "abbrev": "GERMPLASM_CODE"
                },
                {
                    "attribute": "entryType",
                    "abbrev": "ENTRY_TYPE"
                },
                {
                    "attribute": "entryRole",
                    "abbrev": "ENTRY_ROLE"
                },
                {
                    "attribute": "packageDbId",
                    "abbrev": "PACKAGE_ID"
                },
                {
                    "attribute": "packageLabel",
                    "abbrev": "PACKAGE_LABEL"
                }
            ],
            "location": [
                {
                    "attribute": "experimentDbId",
                    "abbrev": "EXPERIMENT_ID"
                },
                {
                    "attribute": "programCode",
                    "abbrev": "PROGRAM_CODE"
                },
                {
                    "attribute": "stageCode",
                    "abbrev": "STAGE_CODE"
                },
                {
                    "attribute": "experimentYear",
                    "abbrev": "EXPERIMENT_YEAR"
                },
                {
                    "attribute": "seasonCode",
                    "abbrev": "SEASON_CODE"
                },
                {
                    "attribute": "experimentCode",
                    "abbrev": "EXPERIMENT_CODE"
                },
                {
                    "attribute": "experimentName",
                    "abbrev": "EXPERIMENT_NAME"
                },
                {
                    "attribute": "experimentType",
                    "abbrev": "EXPERIMENT_TYPE"
                },
                {
                    "attribute": "experimentDesignType",
                    "abbrev": "EXPERIMENT_DESIGN_TYPE"
                },
                {
                    "attribute": "experimentSteward",
                    "abbrev": "EXPERIMENT_STEWARD"
                },
                {
                    "attribute": "occurrenceDbId",
                    "abbrev": "OCCURRENCE_ID"
                },
                {
                    "attribute": "occurrenceCode",
                    "abbrev": "OCCURRENCE_CODE"
                },
                {
                    "attribute": "occurrenceName",
                    "abbrev": "OCCURRENCE_NAME"
                },
                {
                    "attribute": "occurrenceSiteCode",
                    "abbrev": "OCCURRENCE_SITE_CODE"
                },
                {
                    "attribute": "occurrenceFieldCode",
                    "abbrev": "OCCURRENCE_FIELD_CODE"
                },
                {
                    "attribute": "locationDbId",
                    "abbrev": "LOCATION_ID"
                },
                {
                    "attribute": "locationCode",
                    "abbrev": "LOCATION_CODE"
                },
                {
                    "attribute": "locationName",
                    "abbrev": "LOCATION_NAME"
                },
                {
                    "attribute": "locationType",
                    "abbrev": "LOCATION_TYPE"
                },
                {
                    "attribute": "siteCode",
                    "abbrev": "SITE_CODE"
                },
                {
                    "attribute": "fieldCode",
                    "abbrev": "FIELD_CODE"
                },
                {
                    "attribute": "plotDbId",
                    "abbrev": "PLOT_ID"
                },
                {
                    "attribute": "plotCode",
                    "abbrev": "PLOT_CODE"
                },
                {
                    "attribute": "plotNumber",
                    "abbrev": "PLOT_NUMBER"
                },
                {
                    "attribute": "plotType",
                    "abbrev": "PLOT_TYPE"
                },
                {
                    "attribute": "designX",
                    "abbrev": "DESIGN_X"
                },
                {
                    "attribute": "designY",
                    "abbrev": "DESIGN_Y"
                },
                {
                    "attribute": "paX",
                    "abbrev": "PA_X"
                },
                {
                    "attribute": "paY",
                    "abbrev": "PA_Y"
                },
                {
                    "attribute": "plantingInstructionDbId",
                    "abbrev": "PLANTING_INSTRUCTION_ID"
                },
                {
                    "attribute": "entryDbId",
                    "abbrev": "ENTRY_ID"
                },
                {
                    "attribute": "entryCode",
                    "abbrev": "ENTRY_CODE"
                },
                {
                    "attribute": "entryNumber",
                    "abbrev": "ENTRY_NUMBER"
                },
                {
                    "attribute": "entryName",
                    "abbrev": "GERMPLASM_NAME"
                },
                {
                    "attribute": "germplasmCode",
                    "abbrev": "GERMPLASM_CODE"
                },
                {
                    "attribute": "entryType",
                    "abbrev": "ENTRY_TYPE"
                },
                {
                    "attribute": "entryRole",
                    "abbrev": "ENTRY_ROLE"
                },
                {
                    "attribute": "packageDbId",
                    "abbrev": "PACKAGE_ID"
                },
                {
                    "attribute": "packageLabel",
                    "abbrev": "PACKAGE_LABEL"
                }
            ]
        }',
        1,
        'planting_instructions_manager',
        1,
        'CORB-5830 CB-EM UI: Request to make the columns of information customizable for downloading the planting instructions file from the location basic information page'
    ),
    (
        'DOWNLOAD_PLANTING_INSTRUCTION_FILES_COLLABORATOR',
        'Download Planting Instruction Files Collaborator',
        '{
            "occurrence": [
                {
                    "attribute": "experimentDbId",
                    "abbrev": "EXPERIMENT_ID"
                },
                {
                    "attribute": "programCode",
                    "abbrev": "PROGRAM_CODE"
                },
                {
                    "attribute": "stageCode",
                    "abbrev": "STAGE_CODE"
                },
                {
                    "attribute": "experimentYear",
                    "abbrev": "EXPERIMENT_YEAR"
                },
                {
                    "attribute": "seasonCode",
                    "abbrev": "SEASON_CODE"
                },
                {
                    "attribute": "experimentCode",
                    "abbrev": "EXPERIMENT_CODE"
                },
                {
                    "attribute": "experimentName",
                    "abbrev": "EXPERIMENT_NAME"
                },
                {
                    "attribute": "experimentType",
                    "abbrev": "EXPERIMENT_TYPE"
                },
                {
                    "attribute": "experimentDesignType",
                    "abbrev": "EXPERIMENT_DESIGN_TYPE"
                },
                {
                    "attribute": "experimentSteward",
                    "abbrev": "EXPERIMENT_STEWARD"
                },
                {
                    "attribute": "occurrenceDbId",
                    "abbrev": "OCCURRENCE_ID"
                },
                {
                    "attribute": "occurrenceCode",
                    "abbrev": "OCCURRENCE_CODE"
                },
                {
                    "attribute": "occurrenceName",
                    "abbrev": "OCCURRENCE_NAME"
                },
                {
                    "attribute": "occurrenceSiteCode",
                    "abbrev": "OCCURRENCE_SITE_CODE"
                },
                {
                    "attribute": "occurrenceFieldCode",
                    "abbrev": "OCCURRENCE_FIELD_CODE"
                },
                {
                    "attribute": "plotDbId",
                    "abbrev": "PLOT_ID"
                },
                {
                    "attribute": "plotCode",
                    "abbrev": "PLOT_CODE"
                },
                {
                    "attribute": "plotNumber",
                    "abbrev": "PLOT_NUMBER"
                },
                {
                    "attribute": "plotType",
                    "abbrev": "PLOT_TYPE"
                },
                {
                    "attribute": "designX",
                    "abbrev": "DESIGN_X"
                },
                {
                    "attribute": "designY",
                    "abbrev": "DESIGN_Y"
                },
                {
                    "attribute": "paX",
                    "abbrev": "PA_X"
                },
                {
                    "attribute": "paY",
                    "abbrev": "PA_Y"
                },
                {
                    "attribute": "plantingInstructionDbId",
                    "abbrev": "PLANTING_INSTRUCTION_ID"
                },
                {
                    "attribute": "entryDbId",
                    "abbrev": "ENTRY_ID"
                },
                {
                    "attribute": "entryCode",
                    "abbrev": "ENTRY_CODE"
                },
                {
                    "attribute": "entryNumber",
                    "abbrev": "ENTRY_NUMBER"
                },
                {
                    "attribute": "entryName",
                    "abbrev": "GERMPLASM_NAME"
                },
                {
                    "attribute": "germplasmCode",
                    "abbrev": "GERMPLASM_CODE"
                },
                {
                    "attribute": "entryType",
                    "abbrev": "ENTRY_TYPE"
                },
                {
                    "attribute": "entryRole",
                    "abbrev": "ENTRY_ROLE"
                },
                {
                    "attribute": "packageDbId",
                    "abbrev": "PACKAGE_ID"
                },
                {
                    "attribute": "packageLabel",
                    "abbrev": "PACKAGE_LABEL"
                }
            ],
            "location": [
                {
                    "attribute": "experimentDbId",
                    "abbrev": "EXPERIMENT_ID"
                },
                {
                    "attribute": "programCode",
                    "abbrev": "PROGRAM_CODE"
                },
                {
                    "attribute": "stageCode",
                    "abbrev": "STAGE_CODE"
                },
                {
                    "attribute": "experimentYear",
                    "abbrev": "EXPERIMENT_YEAR"
                },
                {
                    "attribute": "seasonCode",
                    "abbrev": "SEASON_CODE"
                },
                {
                    "attribute": "experimentCode",
                    "abbrev": "EXPERIMENT_CODE"
                },
                {
                    "attribute": "experimentName",
                    "abbrev": "EXPERIMENT_NAME"
                },
                {
                    "attribute": "experimentType",
                    "abbrev": "EXPERIMENT_TYPE"
                },
                {
                    "attribute": "experimentDesignType",
                    "abbrev": "EXPERIMENT_DESIGN_TYPE"
                },
                {
                    "attribute": "experimentSteward",
                    "abbrev": "EXPERIMENT_STEWARD"
                },
                {
                    "attribute": "occurrenceDbId",
                    "abbrev": "OCCURRENCE_ID"
                },
                {
                    "attribute": "occurrenceCode",
                    "abbrev": "OCCURRENCE_CODE"
                },
                {
                    "attribute": "occurrenceName",
                    "abbrev": "OCCURRENCE_NAME"
                },
                {
                    "attribute": "occurrenceSiteCode",
                    "abbrev": "OCCURRENCE_SITE_CODE"
                },
                {
                    "attribute": "occurrenceFieldCode",
                    "abbrev": "OCCURRENCE_FIELD_CODE"
                },
                {
                    "attribute": "locationDbId",
                    "abbrev": "LOCATION_ID"
                },
                {
                    "attribute": "locationCode",
                    "abbrev": "LOCATION_CODE"
                },
                {
                    "attribute": "locationName",
                    "abbrev": "LOCATION_NAME"
                },
                {
                    "attribute": "locationType",
                    "abbrev": "LOCATION_TYPE"
                },
                {
                    "attribute": "siteCode",
                    "abbrev": "SITE_CODE"
                },
                {
                    "attribute": "fieldCode",
                    "abbrev": "FIELD_CODE"
                },
                {
                    "attribute": "plotDbId",
                    "abbrev": "PLOT_ID"
                },
                {
                    "attribute": "plotCode",
                    "abbrev": "PLOT_CODE"
                },
                {
                    "attribute": "plotNumber",
                    "abbrev": "PLOT_NUMBER"
                },
                {
                    "attribute": "plotType",
                    "abbrev": "PLOT_TYPE"
                },
                {
                    "attribute": "designX",
                    "abbrev": "DESIGN_X"
                },
                {
                    "attribute": "designY",
                    "abbrev": "DESIGN_Y"
                },
                {
                    "attribute": "paX",
                    "abbrev": "PA_X"
                },
                {
                    "attribute": "paY",
                    "abbrev": "PA_Y"
                },
                {
                    "attribute": "plantingInstructionDbId",
                    "abbrev": "PLANTING_INSTRUCTION_ID"
                },
                {
                    "attribute": "entryDbId",
                    "abbrev": "ENTRY_ID"
                },
                {
                    "attribute": "entryCode",
                    "abbrev": "ENTRY_CODE"
                },
                {
                    "attribute": "entryNumber",
                    "abbrev": "ENTRY_NUMBER"
                },
                {
                    "attribute": "entryName",
                    "abbrev": "GERMPLASM_NAME"
                },
                {
                    "attribute": "germplasmCode",
                    "abbrev": "GERMPLASM_CODE"
                },
                {
                    "attribute": "entryType",
                    "abbrev": "ENTRY_TYPE"
                },
                {
                    "attribute": "entryRole",
                    "abbrev": "ENTRY_ROLE"
                },
                {
                    "attribute": "packageDbId",
                    "abbrev": "PACKAGE_ID"
                },
                {
                    "attribute": "packageLabel",
                    "abbrev": "PACKAGE_LABEL"
                }
            ]
        }',
        1,
        'planting_instructions_manager',
        1,
        'CORB-5830 CB-EM UI: Request to make the columns of information customizable for downloading the planting instructions file from the location basic information page'
    )



--rollback DELETE FROM platform.config WHERE abbrev IN (
--rollback     'DOWNLOAD_PLANTING_INSTRUCTION_FILES_DEFAULT',
--rollback     'DOWNLOAD_PLANTING_INSTRUCTION_FILES_IRSEA',
--rollback     'DOWNLOAD_PLANTING_INSTRUCTION_FILES_COLLABORATOR',
--rollback );