--liquibase formatted sql

--changeset postgres:add_cm_bg_process_threshold_config_to_platform_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3085 CM-DB: Store threshold values in the configuration



-- add cm bg process thresholds config
INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'CM_BG_PROCESSING_THRESHOLD',
        'Cross Manager Background Processing Threshold',
        $$	
            {
                "validateCrosses": {
                    "size": "1500",
                    "description": "Validation of crosses."
                },
                "deleteCrosses": {
                    "size": "1000",
                    "description": "Deletion of crosses."
                }
            }
        $$,
        1,
        'cross_manager',
        1,
        'CORB-3085 CM-DB: Store threshold values in the configuration'
    );



--rollback DELETE FROM platform.config WHERE abbrev='CM_BG_PROCESSING_THRESHOLD';