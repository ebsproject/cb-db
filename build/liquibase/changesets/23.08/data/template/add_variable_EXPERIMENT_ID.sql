--liquibase formatted sql

--changeset postgres:add_variable_EXPERIMENT_ID context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM master.variable WHERE abbrev = 'EXPERIMENT_ID') WHEN TRUE THEN 1 ELSE 0 END;
--comment: CORB-6184 Add new variables to master.variable



-- create variable if not existing, else skip this changeset (use precondition to check if variable exists)
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name) 
    VALUES 
        ('EXPERIMENT_ID', 'EXPERIMENT ID', 'Experiment ID', 'integer', false, 'system', 'experiment, occurrence, location', 'application', 'Experiment ID', 'active', '1', 'Experiment ID')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'EXPERIMENT_ID' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('EXPERIMENT_ID', 'Experiment ID', 'Experiment ID') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('EXPERIMENT_ID_METHOD', 'Experiment ID method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('EXPERIMENT_ID_SCALE', 'Experiment ID scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'EXPERIMENT_ID'
--rollback     AND t.scale_id = var.scale_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.scale AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'EXPERIMENT_ID'
--rollback     AND t.id = var.scale_id
--rollback ;
--rollback DELETE FROM
--rollback     master.property AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'EXPERIMENT_ID'
--rollback     AND t.id = var.scale_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.method AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'EXPERIMENT_ID'
--rollback     AND t.id = var.method_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.variable AS t
--rollback WHERE
--rollback     t.abbrev = 'EXPERIMENT_ID'
--rollback ;