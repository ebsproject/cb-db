--liquibase formatted sql

--changeset postgres:update_em_planting_instruction_files_collaborator_configuration context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5830 CB-EM UI: Request to make the columns of information customizable for downloading the planting instructions file from the location basic information page
--comment: CORB-6183 CB-DB: Add configurations for planting instructions (Occurrence and Location levels)



-- update em download planting instruction files collaborator config
UPDATE
	platform.config
SET
	config_value = '{
	  "location": [
		{
		  "abbrev": "EXPERIMENT_ID",
		  "attribute": "experimentDbId"
		},
		{
		  "abbrev": "PROGRAM_CODE",
		  "attribute": "programCode"
		},
		{
		  "abbrev": "STAGE_CODE",
		  "attribute": "stageCode"
		},
		{
		  "abbrev": "EXPERIMENT_YEAR",
		  "attribute": "experimentYear"
		},
		{
		  "abbrev": "SEASON_CODE",
		  "attribute": "seasonCode"
		},
		{
		  "abbrev": "EXPERIMENT_CODE",
		  "attribute": "experimentCode"
		},
		{
		  "abbrev": "EXPERIMENT_NAME",
		  "attribute": "experimentName"
		},
		{
		  "abbrev": "EXPERIMENT_TYPE",
		  "attribute": "experimentType"
		},
		{
		  "abbrev": "EXPERIMENT_DESIGN_TYPE",
		  "attribute": "experimentDesignType"
		},
		{
		  "abbrev": "EXPERIMENT_STEWARD",
		  "attribute": "experimentSteward"
		},
		{
		  "abbrev": "OCCURRENCE_ID",
		  "attribute": "occurrenceDbId"
		},
		{
		  "abbrev": "OCCURRENCE_CODE",
		  "attribute": "occurrenceCode"
		},
		{
		  "abbrev": "OCCURRENCE_NAME",
		  "attribute": "occurrenceName"
		},
		{
		  "abbrev": "OCCURRENCE_SITE_CODE",
		  "attribute": "occurrenceSiteCode"
		},
		{
		  "abbrev": "OCCURRENCE_FIELD_CODE",
		  "attribute": "occurrenceFieldCode"
		},
		{
		  "abbrev": "LOCATION_ID",
		  "attribute": "locationDbId"
		},
		{
		  "abbrev": "LOCATION_CODE",
		  "attribute": "locationCode"
		},
		{
		  "abbrev": "LOCATION_NAME",
		  "attribute": "locationName"
		},
		{
		  "abbrev": "LOCATION_TYPE",
		  "attribute": "locationType"
		},
		{
		  "abbrev": "SITE_CODE",
		  "attribute": "siteCode"
		},
		{
		  "abbrev": "FIELD_CODE",
		  "attribute": "fieldCode"
		},
		{
		  "abbrev": "PLOT_ID",
		  "attribute": "plotDbId"
		},
		{
		  "abbrev": "PLOT_CODE",
		  "attribute": "plotCode"
		},
		{
		  "abbrev": "PLOT_NUMBER",
		  "attribute": "plotNumber"
		},
		{
		  "abbrev": "PLOT_TYPE",
		  "attribute": "plotType"
		},
		{
		  "abbrev": "DESIGN_X",
		  "attribute": "designX"
		},
		{
		  "abbrev": "DESIGN_Y",
		  "attribute": "designY"
		},
		{
		  "abbrev": "PA_X",
		  "attribute": "paX"
		},
		{
		  "abbrev": "PA_Y",
		  "attribute": "paY"
		},
		{
		  "abbrev": "PLANTING_INSTRUCTION_ID",
		  "attribute": "plantingInstructionDbId"
		},
		{
		  "abbrev": "ENTRY_ID",
		  "attribute": "entryDbId"
		},
		{
		  "abbrev": "ENTRY_CODE",
		  "attribute": "entryCode"
		},
		{
		  "abbrev": "ENTRY_NUMBER",
		  "attribute": "entryNumber"
		},
		{
		  "abbrev": "GERMPLASM_CODE",
		  "attribute": "germplasmCode"
		},
		{
		  "abbrev": "ENTRY_TYPE",
		  "attribute": "entryType"
		},
		{
		  "abbrev": "ENTRY_ROLE",
		  "attribute": "entryRole"
		},
		{
		  "abbrev": "PACKAGE_ID",
		  "attribute": "packageDbId"
		},
		{
		  "abbrev": "PACKAGE_LABEL",
		  "attribute": "packageLabel"
		}
	  ],
	  "occurrence": [
		{
		  "abbrev": "EXPERIMENT_ID",
		  "attribute": "experimentDbId"
		},
		{
		  "abbrev": "PROGRAM_CODE",
		  "attribute": "programCode"
		},
		{
		  "abbrev": "STAGE_CODE",
		  "attribute": "stageCode"
		},
		{
		  "abbrev": "EXPERIMENT_YEAR",
		  "attribute": "experimentYear"
		},
		{
		  "abbrev": "SEASON_CODE",
		  "attribute": "seasonCode"
		},
		{
		  "abbrev": "EXPERIMENT_CODE",
		  "attribute": "experimentCode"
		},
		{
		  "abbrev": "EXPERIMENT_NAME",
		  "attribute": "experimentName"
		},
		{
		  "abbrev": "EXPERIMENT_TYPE",
		  "attribute": "experimentType"
		},
		{
		  "abbrev": "EXPERIMENT_DESIGN_TYPE",
		  "attribute": "experimentDesignType"
		},
		{
		  "abbrev": "EXPERIMENT_STEWARD",
		  "attribute": "experimentSteward"
		},
		{
		  "abbrev": "OCCURRENCE_ID",
		  "attribute": "occurrenceDbId"
		},
		{
		  "abbrev": "OCCURRENCE_CODE",
		  "attribute": "occurrenceCode"
		},
		{
		  "abbrev": "OCCURRENCE_NAME",
		  "attribute": "occurrenceName"
		},
		{
		  "abbrev": "OCCURRENCE_SITE_CODE",
		  "attribute": "occurrenceSiteCode"
		},
		{
		  "abbrev": "OCCURRENCE_FIELD_CODE",
		  "attribute": "occurrenceFieldCode"
		},
		{
		  "abbrev": "PLOT_ID",
		  "attribute": "plotDbId"
		},
		{
		  "abbrev": "PLOT_CODE",
		  "attribute": "plotCode"
		},
		{
		  "abbrev": "PLOT_NUMBER",
		  "attribute": "plotNumber"
		},
		{
		  "abbrev": "PLOT_TYPE",
		  "attribute": "plotType"
		},
		{
		  "abbrev": "DESIGN_X",
		  "attribute": "designX"
		},
		{
		  "abbrev": "DESIGN_Y",
		  "attribute": "designY"
		},
		{
		  "abbrev": "PA_X",
		  "attribute": "paX"
		},
		{
		  "abbrev": "PA_Y",
		  "attribute": "paY"
		},
		{
		  "abbrev": "PLANTING_INSTRUCTION_ID",
		  "attribute": "plantingInstructionDbId"
		},
		{
		  "abbrev": "ENTRY_ID",
		  "attribute": "entryDbId"
		},
		{
		  "abbrev": "ENTRY_CODE",
		  "attribute": "entryCode"
		},
		{
		  "abbrev": "ENTRY_NUMBER",
		  "attribute": "entryNumber"
		},
		{
		  "abbrev": "GERMPLASM_CODE",
		  "attribute": "germplasmCode"
		},
		{
		  "abbrev": "ENTRY_TYPE",
		  "attribute": "entryType"
		},
		{
		  "abbrev": "ENTRY_ROLE",
		  "attribute": "entryRole"
		},
		{
		  "abbrev": "PACKAGE_ID",
		  "attribute": "packageDbId"
		},
		{
		  "abbrev": "PACKAGE_LABEL",
		  "attribute": "packageLabel"
		}
	  ]
	}'
WHERE
	abbrev = 'DOWNLOAD_PLANTING_INSTRUCTION_FILES_COLLABORATOR';



--rollback UPDATE
--rollback 	platform.config
--rollback SET
--rollback 	config_value = '{
--rollback 	  "location": [
--rollback 		{
--rollback 		  "abbrev": "EXPERIMENT_ID",
--rollback 		  "attribute": "experimentDbId"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "PROGRAM_CODE",
--rollback 		  "attribute": "programCode"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "STAGE_CODE",
--rollback 		  "attribute": "stageCode"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "EXPERIMENT_YEAR",
--rollback 		  "attribute": "experimentYear"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "SEASON_CODE",
--rollback 		  "attribute": "seasonCode"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "EXPERIMENT_CODE",
--rollback 		  "attribute": "experimentCode"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "EXPERIMENT_NAME",
--rollback 		  "attribute": "experimentName"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "EXPERIMENT_TYPE",
--rollback 		  "attribute": "experimentType"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "EXPERIMENT_DESIGN_TYPE",
--rollback 		  "attribute": "experimentDesignType"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "EXPERIMENT_STEWARD",
--rollback 		  "attribute": "experimentSteward"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "OCCURRENCE_ID",
--rollback 		  "attribute": "occurrenceDbId"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "OCCURRENCE_CODE",
--rollback 		  "attribute": "occurrenceCode"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "OCCURRENCE_NAME",
--rollback 		  "attribute": "occurrenceName"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "OCCURRENCE_SITE_CODE",
--rollback 		  "attribute": "occurrenceSiteCode"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "OCCURRENCE_FIELD_CODE",
--rollback 		  "attribute": "occurrenceFieldCode"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "LOCATION_ID",
--rollback 		  "attribute": "locationDbId"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "LOCATION_CODE",
--rollback 		  "attribute": "locationCode"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "LOCATION_NAME",
--rollback 		  "attribute": "locationName"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "LOCATION_TYPE",
--rollback 		  "attribute": "locationType"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "SITE_CODE",
--rollback 		  "attribute": "siteCode"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "FIELD_CODE",
--rollback 		  "attribute": "fieldCode"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "PLOT_ID",
--rollback 		  "attribute": "plotDbId"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "PLOT_CODE",
--rollback 		  "attribute": "plotCode"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "PLOT_NUMBER",
--rollback 		  "attribute": "plotNumber"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "PLOT_TYPE",
--rollback 		  "attribute": "plotType"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "DESIGN_X",
--rollback 		  "attribute": "designX"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "DESIGN_Y",
--rollback 		  "attribute": "designY"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "PA_X",
--rollback 		  "attribute": "paX"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "PA_Y",
--rollback 		  "attribute": "paY"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "PLANTING_INSTRUCTION_ID",
--rollback 		  "attribute": "plantingInstructionDbId"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "ENTRY_ID",
--rollback 		  "attribute": "entryDbId"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "ENTRY_CODE",
--rollback 		  "attribute": "entryCode"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "ENTRY_NUMBER",
--rollback 		  "attribute": "entryNumber"
--rollback 		},
--rollback         {
--rollback             "attribute": "entryName",
--rollback             "abbrev": "GERMPLASM_NAME"
--rollback         },
--rollback 		{
--rollback 		  "abbrev": "GERMPLASM_CODE",
--rollback 		  "attribute": "germplasmCode"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "ENTRY_TYPE",
--rollback 		  "attribute": "entryType"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "ENTRY_ROLE",
--rollback 		  "attribute": "entryRole"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "PACKAGE_ID",
--rollback 		  "attribute": "packageDbId"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "PACKAGE_LABEL",
--rollback 		  "attribute": "packageLabel"
--rollback 		}
--rollback 	  ],
--rollback 	  "occurrence": [
--rollback 		{
--rollback 		  "abbrev": "EXPERIMENT_ID",
--rollback 		  "attribute": "experimentDbId"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "PROGRAM_CODE",
--rollback 		  "attribute": "programCode"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "STAGE_CODE",
--rollback 		  "attribute": "stageCode"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "EXPERIMENT_YEAR",
--rollback 		  "attribute": "experimentYear"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "SEASON_CODE",
--rollback 		  "attribute": "seasonCode"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "EXPERIMENT_CODE",
--rollback 		  "attribute": "experimentCode"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "EXPERIMENT_NAME",
--rollback 		  "attribute": "experimentName"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "EXPERIMENT_TYPE",
--rollback 		  "attribute": "experimentType"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "EXPERIMENT_DESIGN_TYPE",
--rollback 		  "attribute": "experimentDesignType"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "EXPERIMENT_STEWARD",
--rollback 		  "attribute": "experimentSteward"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "OCCURRENCE_ID",
--rollback 		  "attribute": "occurrenceDbId"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "OCCURRENCE_CODE",
--rollback 		  "attribute": "occurrenceCode"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "OCCURRENCE_NAME",
--rollback 		  "attribute": "occurrenceName"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "OCCURRENCE_SITE_CODE",
--rollback 		  "attribute": "occurrenceSiteCode"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "OCCURRENCE_FIELD_CODE",
--rollback 		  "attribute": "occurrenceFieldCode"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "PLOT_ID",
--rollback 		  "attribute": "plotDbId"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "PLOT_CODE",
--rollback 		  "attribute": "plotCode"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "PLOT_NUMBER",
--rollback 		  "attribute": "plotNumber"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "PLOT_TYPE",
--rollback 		  "attribute": "plotType"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "DESIGN_X",
--rollback 		  "attribute": "designX"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "DESIGN_Y",
--rollback 		  "attribute": "designY"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "PA_X",
--rollback 		  "attribute": "paX"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "PA_Y",
--rollback 		  "attribute": "paY"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "PLANTING_INSTRUCTION_ID",
--rollback 		  "attribute": "plantingInstructionDbId"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "ENTRY_ID",
--rollback 		  "attribute": "entryDbId"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "ENTRY_CODE",
--rollback 		  "attribute": "entryCode"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "ENTRY_NUMBER",
--rollback 		  "attribute": "entryNumber"
--rollback 		},
--rollback         {
--rollback             "attribute": "entryName",
--rollback             "abbrev": "GERMPLASM_NAME"
--rollback         },
--rollback 		{
--rollback 		  "abbrev": "GERMPLASM_CODE",
--rollback 		  "attribute": "germplasmCode"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "ENTRY_TYPE",
--rollback 		  "attribute": "entryType"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "ENTRY_ROLE",
--rollback 		  "attribute": "entryRole"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "PACKAGE_ID",
--rollback 		  "attribute": "packageDbId"
--rollback 		},
--rollback 		{
--rollback 		  "abbrev": "PACKAGE_LABEL",
--rollback 		  "attribute": "packageLabel"
--rollback 		}
--rollback 	  ]
--rollback 	}'
--rollback WHERE
--rollback 	abbrev = 'DOWNLOAD_PLANTING_INSTRUCTION_FILES_COLLABORATOR';