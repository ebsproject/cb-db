--liquibase formatted sql

--changeset postgres:disable_documentation_trigger_for_location context:schema splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2786 Disable document trigger in location table



ALTER TABLE experiment.location
    DISABLE TRIGGER location_update_location_document_tgr;
;



--rollback     ALTER TABLE experiment.location
--rollback     ENABLE TRIGGER location_update_location_document_tgr;