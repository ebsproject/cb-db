--liquibase formatted sql

--changeset postgres:disable_documentation_trigger_for_occurrence context:schema splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2786 Disable document trigger in occurrence table



ALTER TABLE experiment.occurrence
    DISABLE TRIGGER occurrence_update_location_document_from_occurrence_tgr;
ALTER TABLE experiment.occurrence
    DISABLE TRIGGER occurrence_update_occurrence_document_tgr;
;



--rollback     ALTER TABLE experiment.occurrence
--rollback     ENABLE TRIGGER occurrence_update_location_document_from_occurrence_tgr;
--rollback     ALTER TABLE experiment.occurrence
--rollback     ENABLE TRIGGER occurrence_update_occurrence_document_tgr;