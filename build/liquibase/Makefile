# Copyright (C) 2024 Enterprise Breeding System
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# SPDX-License-Identifier: GPL-3.0-or-later

# CB-DB Makefile

# Variables named in UPPERCASE format are static and/or are defined automatically
# Variables named in lowercase format are provided during execution of a make target.
# Examples below use the .properties files saved in home/ sub-folders (without the .properties file extension).
# 	e.g. make lb-status env=home/fixture
#        make lb-update-one env=home/fixture
#        make lb-update-count env=home/fixture n=10
#        make lb-rollback-count env=home/fixture n=10
# Set DEFAULT_ENV to use a default properties file and avoid adding env=xxx to the make targets

# Load variables in .env file (replace when needed)
include config/.env
.EXPORT_ALL_VARIABLES:

# Set default values
env ?= $(DEFAULT_ENV)
PGTT_EXEC ?= pg_timetable

# Define version and sprint numbers
VERSION := $(version)
SPRINT := $(shell echo $(VERSION)| cut -c 1-5)

# Define current date and timestamp values
CURDATE := $(shell date +%y.%m.%d)
CURTSTAMP := $(shell date +%y.%m.%d.%H%M%S)

# Define text colors
DANGER := $(shell tput setaf 1)
SUCCESS := $(shell tput setaf 2)
WARNING := $(shell tput setaf 3)
PRIMARY := $(shell tput setaf 4)
SECONDARY := $(shell tput setaf 5)
INFO := $(shell tput setaf 6)
DO := $(shell tput sgr0)

# Initialize database version (make initialize version=YY.MM.DD)
initialize:
	@echo "$(PRIMARY)> Initializing database version...$(DO)"

	@echo "$(INFO)> Creating folders...$(DO)"
	mkdir -p changesets/$(SPRINT)/{data,schema,tag}
	mkdir -p changesets/$(SPRINT)/data/{template,fixture}

	@echo "$(INFO)> Adding .gitkeep...$(DO)"
	touch changesets/$(SPRINT)/schema/.gitkeep
	touch changesets/$(SPRINT)/data/{template,fixture}/.gitkeep

	@echo "$(INFO)> Copying changelog and tag files...$(DO)"
	cp templates/db-changelog-database-version-sprint.xml changelogs/db-changelog-database-version-$(SPRINT).xml
	cp templates/version-tag.xml changesets/$(SPRINT)/tag/$(VERSION)-tag.xml

	@echo "$(INFO)> Updating sprint and version placeholders...$(DO)"
	sed -i'.bak' 's/\"SPRINT\"/$(SPRINT)/g' changelogs/db-changelog-database-version-$(SPRINT).xml
	sed -i'.bak' 's/\"VERSION\"/$(VERSION)/g' changelogs/db-changelog-database-version-$(SPRINT).xml
	sed -i'.bak' 's/\"VERSION\"/\"$(VERSION)\"/g' changesets/$(SPRINT)/tag/$(VERSION)-tag.xml

	@echo "$(INFO)> Cleaning up...$(DO)"
	rm changelogs/db-changelog-database-version-$(SPRINT).xml.bak
	rm changesets/$(SPRINT)/tag/$(VERSION)-tag.xml.bak

	@echo "$(WARNING)> NOTE: Include version changelog to changelogs/db-changelog-master.xml$(DO)"

	@echo "$(SUCCESS)> Initializing database version... DONE!$(DO)"

# Release database version (make release version=YY.MM.DD)
release:
	@echo "$(PRIMARY)> Releasing database version...$(DO)"

	sed -i'.bak' 's/\| INCLUDE CHANGESETS ABOVE THIS LINE THANK YOU! ################## --\>/--\>/g' changelogs/db-changelog-database-version-$(SPRINT).xml
	sed -i'.bak' 's/\<\!--TAG//g' changelogs/db-changelog-database-version-$(SPRINT).xml
	sed -i'.bak' 's/TAG--\>//g' changelogs/db-changelog-database-version-$(SPRINT).xml

	@echo "$(SUCCESS)> Releasing database version... DONE!$(DO)"

# Initialize hotfix version (make initialize-hotfix version=YY.MM.DD)
initialize-hotfix:
	@echo "$(INFO)> Copying hotfix changelog file...$(DO)"
	cp templates/db-changelog-database-version-hotfix.xml changelogs/hotfixes/db-changelog-database-version-$(SPRINT).xml

	@echo "$(WARNING)> NOTE: Include hotfix changelog to changelogs/db-changelog-database-version-$(SPRINT).xml$(DO)"

	@echo "$(SUCCESS)> Initializing database hotfix version... DONE!$(DO)"

# Check liquibase status
lb-status:
	./liquibase --defaultsFile=$(CONFIG_DIR)/$(env).properties status --verbose

# Apply all changesets
lb-update:
	./liquibase --defaultsFile=$(CONFIG_DIR)/$(env).properties update

# Apply n changesets
lb-update-count:
	./liquibase --defaultsFile=$(CONFIG_DIR)/$(env).properties updateCount $(n)

# Apply 1 changeset
lb-update-one:
	./liquibase --defaultsFile=$(CONFIG_DIR)/$(env).properties updateCount 1

# Rollback n changesets
lb-rollback-count:
	./liquibase --defaultsFile=$(CONFIG_DIR)/$(env).properties rollbackCount $(n)

# Rollback 1 changeset
lb-rollback-one:
	./liquibase --defaultsFile=$(CONFIG_DIR)/$(env).properties rollbackCount 1

# Rollback 1 changeset
lb-clear-checksums:
	./liquibase --defaultsFile=$(CONFIG_DIR)/$(env).properties clear-checksums

# Release liquibase locks
lb-release-locks:
	./liquibase --defaultsFile=$(CONFIG_DIR)/$(env).properties releaseLocks

# Update to a specific version (make sure env is using the master changelog)
lb-update-to:
	./liquibase --defaultsFile=$(CONFIG_DIR)/$(env).properties update-to-tag $(version)

# Rollback to a specific version (make sure env is using the master changelog)
lb-rollback-to:
	./liquibase --defaultsFile=$(CONFIG_DIR)/$(env).properties rollback $(version)

# Export changelog to CSV
lb-export-changelog:
	psql -U $(CB_DB_USER) -h $(CB_DB_HOST) -p $(CB_DB_PUBLISHED_PORT) -c "\copy (SELECT * FROM public.databasechangelog ORDER BY orderexecuted DESC) TO STDOUT WITH (FORMAT CSV, HEADER TRUE, FORCE_QUOTE *, QUOTE '\"')" $(db) > $(CONFIG_DIR)/temp/changelog_$(db)_$(CURDATE).csv

# Terminate all database transactions
pg-terminate:
	psql -U $(CB_DB_USER) -h $(CB_DB_HOST) -p $(CB_DB_PUBLISHED_PORT) -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pid <> pg_backend_pid() AND pg_stat_activity.datname IN ('$(db)')" postgres

# Create a new database
pg-create:
	createdb -U $(CB_DB_USER) -h $(CB_DB_HOST) -p $(CB_DB_PUBLISHED_PORT) $(db)

# Drop an existing database
pg-drop:
	dropdb -U $(CB_DB_USER) -h $(CB_DB_HOST) -p $(CB_DB_PUBLISHED_PORT) $(db)

# Rename an existing database
pg-rename:
	psql -U $(CB_DB_USER) -h $(CB_DB_HOST) -p $(CB_DB_PUBLISHED_PORT) -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pid <> pg_backend_pid() AND pg_stat_activity.datname IN ('$(from)')" postgres
	psql -U $(CB_DB_USER) -h $(CB_DB_HOST) -p $(CB_DB_PUBLISHED_PORT) -c "ALTER DATABASE $(from) RENAME TO $(to)" postgres

# Dump database
pg-dump:
	pg_dump -U $(CB_DB_USER) -h $(CB_DB_HOST) -p $(CB_DB_PUBLISHED_PORT) -xO $(db) | gzip > $(CONFIG_DIR)/temp/$(db)_$(CURDATE).sql.gz

# Dump database schema only
pg-dump-schema:
	pg_dump -U $(CB_DB_USER) -h $(CB_DB_HOST) -p $(CB_DB_PUBLISHED_PORT) -sxO $(db) | gzip > $(CONFIG_DIR)/temp/$(db)_schema_$(CURDATE).sql.gz

# Dump database data only
pg-dump-data:
	pg_dump -U $(CB_DB_USER) -h $(CB_DB_HOST) -p $(CB_DB_PUBLISHED_PORT) -axO $(db) | gzip > $(CONFIG_DIR)/temp/$(db)_schema_$(CURDATE).sql.gz

# Restore database from dump with logs
pg-restore:
	gunzip < $(dump) | psql -U $(CB_DB_USER) -h $(CB_DB_HOST) -p $(CB_DB_PUBLISHED_PORT) $(db) > $(CONFIG_DIR)/temp/restore_$(db)_$(CURTSTAMP).log

# Restore database from dump without logs
pg-restore-silent:
	gunzip < $(dump) | psql -U $(CB_DB_USER) -h $(CB_DB_HOST) -p $(CB_DB_PUBLISHED_PORT) $(db) > /dev/null

# Copy an existing database
pg-copy:
	psql -U $(CB_DB_USER) -h $(CB_DB_HOST) -p $(CB_DB_PUBLISHED_PORT) -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pid <> pg_backend_pid() AND pg_stat_activity.datname IN ('$(from)', '$(to)')" postgres
	createdb -U $(CB_DB_USER) -h $(CB_DB_HOST) -p $(CB_DB_PUBLISHED_PORT) -T $(from) $(to)

# Drop an existing database and create it again using a copy from another database
pg-refresh:
	psql -U $(CB_DB_USER) -h $(CB_DB_HOST) -p $(CB_DB_PUBLISHED_PORT) -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pid <> pg_backend_pid() AND pg_stat_activity.datname IN ('$(from)', '$(to)')" postgres
	dropdb -U $(CB_DB_USER) -h $(CB_DB_HOST) -p $(CB_DB_PUBLISHED_PORT) --if-exists $(to)
	createdb -U $(CB_DB_USER) -h $(CB_DB_HOST) -p $(CB_DB_PUBLISHED_PORT) -T $(from) $(to)

# Start local database (for other non-docker instances)
pg-start:
	pg_ctl -D $(DB_DATA) -l $(CONFIG_DIR)/postgresql.log start

# Stop local database (for other non-docker instances)
pg-stop:
	pg_ctl -D $(DB_DATA) -l $(CONFIG_DIR)/postgresql.log stop

# Restart local database (for other non-docker instances)
pg-restart:
	pg_ctl -D $(DB_DATA) -l $(CONFIG_DIR)/postgresql.log restart

# Start local PostgreSQL server (requires homebrew in Mac)
pg-brew-start:
	brew services start $(BREW_PG)

# Stop local PostgreSQL server (requires homebrew in Mac)
pg-brew-stop:
	brew services stop $(BREW_PG)

# Restart local PostgreSQL server (requires homebrew in Mac)
pg-brew-restart:
	brew services restart $(BREW_PG)

# View running database transactions (requires pg_activity)
pg-monitor:
	pg_activity -U $(CB_DB_USER) -h $(CB_DB_HOST) -p $(CB_DB_PUBLISHED_PORT) -d $(CB_DB_NAME)

# Run an ad hoc SQL query (make pg-run query="SELECT 1")
pg-run:
	@psql -U $(CB_DB_USER) -h $(CB_DB_HOST) -p $(CB_DB_PUBLISHED_PORT) -c "$(query)" $(CB_DB_NAME)

# Run an ad hoc SQL query (make pg-run query="SELECT 1")
pg-run-file:
	@psql -U $(CB_DB_USER) -h $(CB_DB_HOST) -p $(CB_DB_PUBLISHED_PORT) $(CB_DB_NAME) < $(file)

# Run manually the pg_timetable executable file (make pgtt-run)
pgtt-run:
	./$(PGTT_EXEC) --host=$(CB_DB_HOST) --port=$(CB_DB_PUBLISHED_PORT) --dbname=$(CB_DB_NAME) --clientname=$(PGTT_CLIENTNAME) --user=$(PGTT_PGUSER) --password=$(PGTT_PGPASSWORD) --log-file=pgtt.log

# Add scheduled job to pg_timetable service (make pgtt-add job="test" schedule="*/5 * * * *" command="SELECT 'I run every 5 minutes'")
pgtt-add:
	@psql -U $(CB_DB_USER) -h $(CB_DB_HOST) -p $(CB_DB_PUBLISHED_PORT) -c "SELECT timetable.add_job('$(job)', '$(schedule)', '$(command)')" $(CB_DB_NAME)

# Remove a scheduled job from pg_timetable service (make pgtt-delete job="test")
pgtt-delete:
	@psql -U $(CB_DB_USER) -h $(CB_DB_HOST) -p $(CB_DB_PUBLISHED_PORT) -c "SELECT timetable.delete_job('$(job)')" $(CB_DB_NAME)

# Delete merged local branches
delete-local-branches:
	git branch --merged | egrep -v "(^\*|master|release|develop)" | xargs git branch -d
