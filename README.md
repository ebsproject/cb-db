# Core Breeding Database

This is the database module of the Core Breeding Applications. You'll find all the database scripts and binaries in this repository.

This project has been fully containerized. The commands to help you use this container is outlined below. When you run the container, the following will be done for you:

1. Ubuntu base image with utility tools installed
2. Postgres 13 installed and configured
3. Database engine tuned to run in a modest server
4. Database user created based on the passed variable or created from the default
5. Database created based on the passed variable or created from the default
6. Liquibase migration against the created database - effectively giving you the latest SM schema. Note that you can override the default contexts if you need fixture data, etc.

## Contents of this repository

#### Liquibase in EBS

There are too many ways you can use Liquibase for database versioning and change control, we have put up a [guideline across EBS as to how a database project should be structured](https://ebsproject.atlassian.net/wiki/spaces/DB/pages/29006528708/EBS+Database+Project+Structure). If you are contributing to this repository, it is imperative that you read the linked document and conform to the standards we've put in place.

#### Dockerfile and config.sh
Contains all the containerization steps. The config.sh is the entrypoint and you'll find all the database provisioning in there.

#### Build/liquibase directory
Contains all the liquibase scripts and binaries.

#### Build/util
Contains all utility scripts to make automation easier


## Using this container

Usage can be classified into two types: database development and general usage. The following environment variables/parameters are available (shown below with their respective default values) and can be set during `docker run` invocation or overriden via environment variables on docker compose or swarm:


```bash
postgres_local_auth_method=trust
postgres_host_auth_method=trust
postgres_listen_address=*
db_user=ebsuser
db_pass=ebsuser
db_name=cb
pg_driver=postgresql.jar
lq_contexts=schema,template
lq_labels=""
```

### Docker Compose Alternative

In case you just want to get a DB container with all the defaults and don't want to deal with all the docker commands mentioned below, this one's for you. Go into the `deploy` directory:

* To start the cb-db container/service:
```bash
docker compose -f deploy/cb-db.yml up -d
```
* To stop the cb-db container/service
```bash
docker compose -f deploy/cb-db.yml down
```


### Database Development

As mentioned above, the container will set up and configure everything you need. So you can focus on just writing SQL or database scripts. As long as they are in the build directory, the container will pick it up.

**Steps**


* Make sure your repository is up to date with remote (ie. `git pull --all`)
* Write your code, ex. for liquibase, make sure the SQL files are in build/liquibase/changesets directory and specified in a changelog XML (see [Database Management Guideline](https://ebsproject.atlassian.net/wiki/spaces/DB/pages/104235022/Database+Change+Management))
* Build the image. Make sure you are in the root directory of this repository, then run

```bash
docker build --force-rm=true -t cb-db .
```

* If the build succeeds, you should now have the docker image locally. You can then start and initialize the container. You have two options depending on wether or not you want the database data to persist. Change variable values as you see fit (-v).
	* Persist data across docker runs:
	```bash
	docker run --detach --name cb-db -h cb-db -p 5433:5432 --health-cmd="pg_isready -U postgres || exit 1" -e "db_name=cbfixturedb" -e "db_user=ebsuser" -e "lq_contexts=schema,template,fixture" -e "lq_labels=""" -v postgres_etc:/etc/postgresql -v postgres_log:/var/log/postgresql -v postgres_lib:/var/lib/postgresql -it cb-db:latest
	```
	* Do not persist data (whenever container is removed via `docker rm`, the data goes away with it):
	```bash
	docker run --detach --name cb-db -h cb-db -p 5433:5432 --health-cmd="pg_isready -U postgres || exit 1" -e "db_name=cbfixturedb" -e "db_user=ebsuser" -e "lq_contexts=schema,template,fixture" -e "lq_labels=""" -it cb-db:latest
	```

* Wait a minute or two. Feel free to check the status of the schema migration via `docker logs cb-db`.
* You now have a running Postgres 13 on port 5433 with all the latest changes. You can either connect to it to port 5433 from outside the container, or go inside the container and check via psql

```bash
docker exec -ti cb-db bash
su - postgres
psql
```

* Lastly, you have the option to either **keep the container running** as long as you're making your database changes, then invoking liquibase within the container to test. This way you save time by not having to rebuild the image everytime. Once you are happy with your work, push your liquibase changesets to this repository.

>Note that the example commands above creates a schema with fixture data. If you don't want that, modify the liquibase context being passed, ie. remove the `fixture` context

### General Usage

Typically, for general usage, you do not need to modify any database scripts or add new SQL. So the steps are simpler. You don't even need to pull this repository.

#### Get the official docker image from **EBSProject Dockerhub**

> The official nightly build tag is **dev**. Release images are tagged according to version numbers. Check the Dockerhub Repository for current tags. [CB-DB Dockerhub](https://hub.docker.com/r/ebsproject/cb-db)

**Steps**


* Run the container. You have two options depending on wether or not you want the database data to persist.

* Persist data across docker runs:
```bash
docker run --detach --name cb-db -h cb-db -p 5433:5432 --health-cmd="pg_isready -U postgres || exit 1" -e "db_name=cbfixturedb" -e "db_user=ebsuser" -e "lq_contexts=schema,template,fixture" -e "lq_labels=""" -v postgres_etc:/etc/postgresql -v postgres_log:/var/log/postgresql -v postgres_lib:/var/lib/postgresql -it ebsproject/cb-db:dev
```
* Do not persist data (whenever container is removed via `docker rm`, the data goes away with it):
```bash
docker run --detach --name cb-db -h cb-db -p 5433:5432 --health-cmd="pg_isready -U postgres || exit 1" -e "db_name=cbfixturedb" -e "db_user=ebsuser" -e "lq_contexts=schema,template,fixture" -e "lq_labels=""" -it ebsproject/cb-db:dev
```
* Wait a minute or two. Feel free to check the status of the schema migration via `docker logs sm-db`.

* You now have a running Postgres 13 on port 5433 with all the latest changes. You can either connect to it to port 5433 from outside the container, or go inside the container and check via psql

```bash
docker exec -ti cb-db bash
su - postgres
psql
```

> The example commands above will create the container off of the nightly build (tag=dev). Change the tag as needed.


# References

## Changeset

- `filename.sql` = short summary of the changeset (e.g. `insert_new_config.sql`)
- `{changeset_summary}` = summary of changes (no spaces, separated by underscores, e.g. `insert_new_config`)
- `{changeset_type}` = type of change (e.g. `context:template`)
    - `schema`: changes in the database objects (e.g. add column, add table, remove constraint, update function, etc.)
    - `template`: data changes that persist in all instances and are necessary in using the system (e.g. variables, configs, default values and codes, etc.)
    - `fixture`: data changes that are used for development, testing, and demo purposes (e.g. sample experiments and made-up germplasm records)
- `{apply_changes}` = actions to apply changes (add comments if needed, e.g. `INSERT INTO platform.config VALUES (...);`)
- `{rollback_changes}` = revert changes applied above, prepended by `--rollback `
- `DB-XXX` = Jira issue reference (e.g. `DB-123`)

### Format: `changesets/YY.MM/.../filename.sql`
```sql
--liquibase formatted sql

--changeset postgres:{changeset_summary} context:{changeset_type} splitStatements:false rollbackSplitStatements:false
--comment: DB-XXX Summary of changes

{apply_changes}

-- rollback changes
--rollback {rollback_changes}
```

### Example: `changesets/21.06/data/template/insert_new_config.sql`
```sql
--liquibase formatted sql

--changeset postgres:insert_new_config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-123 Insert new config

INSERT INTO platform.config VALUES (...);

-- rollback changes
--rollback DELETE FROM platform.config WHERE ...;
```

## Changelog

- `DB-XXX` = Jira issue reference
- `YY.MM` = sprint folder
- `<include file="..." />` = relative path to the changeset file

### Format: `changelogs/db-changelog-database-version-YY.MM.xml`
```xml
<?xml version="1.0" encoding="UTF-8"?>
<databaseChangeLog xmlns="http://www.liquibase.org/xml/ns/dbchangelog" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-4.3.xsd">

    <!-- DB-XXX Summary of schema changes -->
    <include file="changesets/YY.MM/schema/apply_schema_changes.sql" />

    <!-- DB-XXX Summary of template data changes -->
    <include file="changesets/YY.MM/data/template/apply_template_data_changes.sql" />

    <!-- DB-XXX Summary of fixture data changes -->
    <include file="changesets/YY.MM/data/fixture/apply_fixture_data_changes.sql" />

</databaseChangeLog>
```

### Example: `changelogs/db-changelog-database-version-21.06.xml`
```xml
<?xml version="1.0" encoding="UTF-8"?>
<databaseChangeLog xmlns="http://www.liquibase.org/xml/ns/dbchangelog" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-4.3.xsd">

    <!-- DB-123 Insert new config -->
    <include file="changesets/21.06/data/template/insert_new_config.sql" />

</databaseChangeLog>
```

> Tip: Use VSCode to automatically write the changeset template to your SQL file by simply typing --liquibase and hitting Tab/Enter.

## Structure
```
.
├── .vscode
│   └── cb-db.code-snippets
├── build
│   └── liquibase
│       ├── bin
│       │   └── ...
│       ├── changelogs
│       │   ├── ...
│       │   ├── db-changelog-database-version-YY.MM.xml <----------- Sprint YY.MM changelog
│       │   └── db-changelog-master.xml
│       ├── changesets
│       │   ├── ...
│       │   ├── YY.MM <--------------------------------------------- Sprint YY.MM changesets
│       │   │   ├── data
│       │   │   │   ├── fixture <----------------------------------- Fixture data changes
│       │   │   │   │   ├── insert_fixture_data.sql
│       │   │   │   │   ├── update_fixture_data.sql
│       │   │   │   │   ├── delete_fixture_data.sql
│       │   │   │   │   └── ...
│       │   │   │   └── template <---------------------------------- Template data changes
│       │   │   │       ├── insert_template_data.sql
│       │   │   │       ├── update_template_data.sql
│       │   │   │       ├── delete_template_data.sql
│       │   │   │       └── ...
│       │   │   ├── schema <---------------------------------------- Schema changes
│       │   │   │   ├── insert_schema_object.sql
│       │   │   │   ├── update_schema_object.sql
│       │   │   │   ├── delete_schema_object.sql
│       │   │   │   └── ...
│       │   │   └── tag
│       │   │       └── dev-tag.xml
│       │   └── baseline
│       │       ├── data
│       │       │   ├── fixture
│       │       │   │   └── ...
│       │       │   └── template
│       │       │       └── ...
│       │       ├── schema
│       │       │    └── ...
│       │       └── tag
│       │            └── baseline-tag.xml
│       ├── lib
│       │   └── ...
│       ├── LICENSE.txt
│       ├── liquibase
│       ├── liquibase-fixture.properties
│       ├── liquibase-template.properties
│       ├── liquibase.bat
│       └── liquibase.jar
├── deploy
│   ├── .env
│   └── cb-db.yml
├── design
│   ├── ...
│   └── cb-db.dbs
├── test
│   ├── archive
│   │   └── ...
│   ├── config
│   │   ├── __init__.py
│   │   ├── config_parser.py
│   │   ├── config.ini
│   │   └── schema.ini
│   ├── lib
│   │   ├── __init__.py
│   │   └── connection_manager.py
│   ├── reference
│   │   └── ...
│   ├── Dockerfile
│   ├── requirements.txt
│   └── ...
├── .gitignore
├── config.sh
├── Dockerfile
└── README.md
```
