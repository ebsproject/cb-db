#!/bin/bash
#author: Kevin Palis <kdp44@cornell.edu> & Gene Romuga <g.romuga@irri.org> & Argem Flores <a.flores@irri.org>

# create database cluster
sudo pg_createcluster --locale=en_US.utf-8 13 main
sudo pg_dropcluster --stop 9.6 main 

#configure postgres for access
sudo sed -i "s/local   all             all                                     peer/local   all             all                                     $postgres_local_auth_method/" /etc/postgresql/13/main/pg_hba.conf
sudo sed -i "s/host    all             all             127\.0\.0\.1\/32            md5/host    all             all             0\.0\.0\.0\/0            	$postgres_host_auth_method/" /etc/postgresql/13/main/pg_hba.conf
sudo sed -i "s/\#listen_addresses = '$db_host'/listen_addresses = '$postgres_listen_address'/" /etc/postgresql/13/main/postgresql.conf

# Clean up apt cache and package lists
apt-get clean
rm -rf /var/lib/apt/lists/*

#Postgres tuning - configure as needed depending on the server the database is on!
sudo echo -e "default_statistics_target = $default_statistics_target
random_page_cost = $random_page_cost
effective_cache_size = $effective_cache_size
max_parallel_workers_per_gather = $max_parallel_workers_per_gather
max_parallel_workers = $max_parallel_workers
track_activity_query_size = $track_activity_query_size
max_connections = $max_connections
idle_in_transaction_session_timeout = $idle_in_transaction_session_timeout
shared_preload_libraries = $shared_preload_libraries
pg_stat_statements.max = $pg_stat_statements_max
pg_stat_statements.track = $pg_stat_statements_track
" >> /etc/postgresql/13/main/postgresql.conf

#setup pg_cron
sudo echo -e "shared_preload_libraries = 'pg_cron'
cron.database_name = 'cb'
cron.timezone = 'UTC'
" >> /etc/postgresql/13/main/postgresql.conf

#restart for the config and tuning to take effect
service postgresql restart

#wait added to ensure postgres is up
sleep 20

echo "Creating the default database user..."
sudo -u postgres psql -tAc "SELECT 1 FROM pg_roles WHERE rolname='$db_user'" | grep -q 1 || sudo -u postgres psql -c "create user $db_user with superuser password '$db_pass' valid until 'infinity';"

#creates the database ONLY if doesn't exist
echo "Creating the database..."

# create new template database with C collation and UTF-8 encoding
echo "ALTER DATABASE template1 is_template=false\gexec" | sudo -u postgres psql
echo "DROP DATABASE template1\gexec" | sudo -u postgres psql
echo "CREATE DATABASE template1 WITH OWNER = $db_user ENCODING = 'UTF8' TABLESPACE = pg_default LC_COLLATE = 'C' LC_CTYPE = 'C' CONNECTION LIMIT = -1 TEMPLATE template0\gexec" | sudo -u postgres psql
echo "GRANT ALL ON DATABASE template1 TO $db_user\gexec" | sudo -u postgres psql
echo "ALTER DATABASE template1 is_template=true\gexec" | sudo -u postgres psql
echo "\gexec" | sudo -u postgres psql

# create default cb database
echo "SELECT 'CREATE DATABASE $db_name TEMPLATE template1 OWNER $db_user' WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = '$db_name')\gexec" | sudo -u postgres psql
echo "GRANT ALL ON DATABASE $db_name TO $db_user\gexec" | sudo -u postgres psql

# create pg_timetable scheduler user
echo "Creating the pg_timetable database user..."
sudo -u postgres psql -tAc "SELECT 1 FROM pg_roles WHERE rolname='$PGTT_PGUSER'" | grep -q 1 || sudo -u postgres psql -c "create role $PGTT_PGUSER password '$PGTT_PGPASSWORD' login valid until 'infinity';"
echo "GRANT CREATE ON DATABASE $db_name TO $PGTT_PGUSER\gexec" | sudo -u postgres psql

# create extensions
echo "Creating database extensions..."
echo "CREATE EXTENSION IF NOT EXISTS pg_stat_statements\gexec" | sudo -u postgres psql -d $db_name
echo "CREATE EXTENSION IF NOT EXISTS postgis\gexec" | sudo -u postgres psql -d $db_name

export PATH=$PATH:$PWD
export _JAVA_OPTIONS="-Xmx2g"

cd build/liquibase

# uncomment to debug
#  echo "nohup ./pg_timetable --host=$db_host --port=$db_port --dbname=$db_name --clientname=$pgtt_clientname --user=$PGTT_PGUSER --password=$PGTT_PGPASSWORD &>/dev/null &"
#  echo bash liquibase --username=$db_user --password=$db_pass --url=jdbc:postgresql://$db_host:$db_port/$db_name --driver=org.postgresql.Driver --classpath="bin/drivers/$pg_driver" --hub-mode=off --secure-parsing=false --changeLogFile=changelogs/db-changelog-master.xml --contexts="$lq_contexts" --labels="$lq_labels" update

# run pg_timetable to schedule recurring database tasks
echo "Starting pg_timetable service..."

# run new pg_timetable service in the background
nohup ./pg_timetable --host=$db_host --port=$db_port --dbname=$db_name --clientname=$pgtt_clientname --log-file=pgtt.log &>/dev/null &

# run liquibase to apply database changes
echo "Starting liquibase migration..."
bash liquibase --username=$db_user --password=$db_pass --url=jdbc:postgresql://$db_host:$db_port/$db_name --driver=org.postgresql.Driver --classpath="bin/drivers/$pg_driver" --hub-mode=off --secure-parsing=false --changeLogFile=changelogs/db-changelog-master.xml --contexts="$lq_contexts" --labels="$lq_labels" update

# grant read-write privileges to pg_timetable service
cat config/pgtt_init.sql | sudo -u postgres psql -d $db_name

# run pytest
cd ../../tests
echo "Starting DB tests..."
rm config/config.ini

cat <<EOT >> config/config.ini
[CB-DB]
host = $db_host
port = $db_port
username = $db_user
password = $db_pass
database = $db_name
EOT

pytest -k $target_test -v > result.log
echo "DB Testing done..."
echo "Test Summary"
FAILED_TESTS=`grep -c "FAILED" result.log`
PASSED_TESTS=`grep -c "PASSED" result.log`
echo "*** DB Test Summary ***"
echo "Passed: ${PASSED_TESTS}"
echo "Failed: ${FAILED_TESTS}"
echo "***********************"
echo "Check result.log for the comprehensive summary"

/bin/bash
