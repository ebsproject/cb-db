# Copyright (C) 2024 Enterprise Breeding System
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# SPDX-License-Identifier: GPL-3.0-or-later

#author: Kevin Palis <kdp44@cornell.edu> & Gene Romuga <g.romuga@irri.org>

FROM ubuntu:22.04 AS builder
#update and install utility packages
RUN apt-get update -y && apt-get install -y \
 gnupg2 \
 wget \
 sudo \
 software-properties-common
EXPOSE 22 5432


#copy the entrypoint/config file and make sure it can execute
COPY config.sh /root
RUN chmod 755 /root/config.sh

#install Java so we can run liquibase
# RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 0xB1998361219BD9C9 && \
# apt-add-repository 'deb http://repos.azulsystems.com/ubuntu stable main' && \
# apt install -y zulu-13
# RUN apt install -y default-jre
RUN apt-get update && apt-get install -y openjdk-18-jre-headless

#Create the file repository configuration
#Import the repository signing key
RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list && \
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - && \
apt-get -y update

# install C locale with utf-8
RUN apt-get install -y locales && rm -rf /var/lib/apt/lists/* \
 && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LC_CTYPE en_US.UTF-8
ENV LC_COLLATE en_US.UTF-8

#Install Postgresql13
RUN apt-get -y update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
postgresql-13 \
postgresql-client-13

# Install PostGIS, PostgreSQL PostGIS, and Cron extension
RUN apt-get install -y --no-install-recommends \
postgis \
postgresql-13-postgis-3 \
postgresql-13-cron

# install make and htop
RUN apt install -y make
RUN apt install -y htop

# copy test files
COPY /test /tests

# install python 
RUN add-apt-repository ppa:deadsnakes/ppa
RUN apt-get install -y python3.8 \
    python3-pip \
    libpq-dev
RUN pip3 install -r /tests/requirements.txt

#set all environment variables needed to initialize the database - these can all be overriden during container run
ENV db_host=localhost
ENV db_port=5432
ENV db_name=cb
ENV db_user=ebsuser
ENV db_pass=ebsuser

ENV target_test=.

ENV lq_contexts=schema,template
ENV lq_labels=NULL
ENV lq_skip_update=FALSE

ENV postgres_local_auth_method=md5
ENV postgres_host_auth_method=md5
ENV postgres_listen_address=*
ENV pg_driver=postgresql.jar
ENV default_statistics_target=100
ENV random_page_cost=1.1
ENV effective_cache_size=32GB
ENV max_parallel_workers_per_gather=4
ENV max_parallel_workers=10
ENV track_activity_query_size=64kB
ENV max_connections=800
ENV idle_in_transaction_session_timeout=3min
ENV shared_preload_libraries=pg_stat_statements
ENV pg_stat_statements_max=10000
ENV pg_stat_statements_track=all

ENV pgtt_clientname=worker001
ENV PGTT_PGUSER=scheduler
ENV PGTT_PGPASSWORD=5ch3du13r

COPY build build

ENTRYPOINT ["/root/config.sh"]

FROM liquibase/liquibase:4.20 AS liquibase

COPY --from=builder /build/liquibase/changelogs /liquibase/changelog/changelogs
COPY --from=builder /build/liquibase/changesets /liquibase/changelog/changesets
COPY liquibase_config.sh /liquibase/liquibase_config.sh

WORKDIR /liquibase


CMD ["bash", "liquibase_config.sh"]