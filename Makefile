# Copyright (C) 2024 Enterprise Breeding System
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# SPDX-License-Identifier: GPL-3.0-or-later

# CB-DB Makefile (root)

# Variables named in UPPERCASE format are static and/or are defined automatically
# Variables named in lowercase format are provided during execution of a make target.

# Load variables in .env file (replace when needed)
include ./deploy/.env
.EXPORT_ALL_VARIABLES:

# Set default values
env ?= cb-db
container ?= deploy_cb-db_1

# Builds, (re)creates, starts, and attaches to containers for a service
up:
	docker-compose -f deploy/$(env).yml up -d
# Stops containers and removes containers, networks, volumes, and images created by `up`
down:
	docker-compose -f deploy/$(env).yml down

# Run a command in a running container
enter:
	docker exec -it `docker ps --filter "name=$(container)" --format {{.ID}}` /bin/bash

# Build an image from a Dockerfile
build-image:
	docker build -t $(CB_DB_IMAGE):$(CB_DB_IMAGE_TAG) -f Dockerfile .

# Run a command in a new container
run-temp:
	docker run \
		-p $(CB_DB_HOST):$(CB_DB_TARGET_PORT):$(CB_DB_PUBLISHED_PORT) \
		-it $(CB_DB_IMAGE):$(CB_DB_IMAGE_TAG) \
		/bin/bash

# Run a command in a new container with the current working directory as the mounted host volume
run-mount:
	docker run \
		-v "$$(pwd)":/cb-db \
		-p $(CB_DB_HOST):$(CB_DB_TARGET_PORT):$(CB_DB_PUBLISHED_PORT) \
		-it $(CB_DB_IMAGE):$(CB_DB_IMAGE_TAG) \
		/bin/bash

# Remove one or more images based on the given reference image and tag
remove-image:
	docker rmi -f `docker image ls -f reference=$(CB_DB_IMAGE):$(CB_DB_IMAGE_TAG) --format {{.ID}}`
